﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Vector3 UnityEngine.WheelHit::get_point()
extern void WheelHit_get_point_m950E6B764A768CE147DB57C1CAE81D288391E5E8 (void);
// 0x00000002 UnityEngine.Vector3 UnityEngine.WheelHit::get_normal()
extern void WheelHit_get_normal_mBC99D67AD92266C0B5EBCF8C2FC771BAE34E9D12 (void);
// 0x00000003 System.Single UnityEngine.WheelHit::get_forwardSlip()
extern void WheelHit_get_forwardSlip_m8F11089F49F1049C5D993A0E360E8738D7E600FC (void);
// 0x00000004 System.Single UnityEngine.WheelHit::get_sidewaysSlip()
extern void WheelHit_get_sidewaysSlip_m6AA5854F880C9BCDA98A22DBB694408954BB0CED (void);
// 0x00000005 System.Single UnityEngine.WheelCollider::get_radius()
extern void WheelCollider_get_radius_mDB04C14DAB0BB1D5D7A2D160C5EE3F159CE3FA26 (void);
// 0x00000006 System.Void UnityEngine.WheelCollider::set_radius(System.Single)
extern void WheelCollider_set_radius_mC075EA6709309EBBBE43F13F02C677024263B170 (void);
// 0x00000007 System.Void UnityEngine.WheelCollider::set_suspensionDistance(System.Single)
extern void WheelCollider_set_suspensionDistance_m086CDF09680B525B3936FC519D050C942448FA09 (void);
// 0x00000008 UnityEngine.JointSpring UnityEngine.WheelCollider::get_suspensionSpring()
extern void WheelCollider_get_suspensionSpring_m2C5E9C26DAB6CD1114B51464EA5428AEA20AB805 (void);
// 0x00000009 System.Void UnityEngine.WheelCollider::set_suspensionSpring(UnityEngine.JointSpring)
extern void WheelCollider_set_suspensionSpring_mE6A9289CB03402ACA06222E5914C28EC9EB7F2E9 (void);
// 0x0000000A System.Void UnityEngine.WheelCollider::set_mass(System.Single)
extern void WheelCollider_set_mass_m7991E5E529D8CA73AC4C194CD297E9D3C5841260 (void);
// 0x0000000B System.Void UnityEngine.WheelCollider::set_motorTorque(System.Single)
extern void WheelCollider_set_motorTorque_mFE7962DF8003D10BA646545E56F0A6B3ED8803DA (void);
// 0x0000000C System.Void UnityEngine.WheelCollider::set_brakeTorque(System.Single)
extern void WheelCollider_set_brakeTorque_mDAFB1032B6B3AD4C8103869D64807596774C1B54 (void);
// 0x0000000D System.Single UnityEngine.WheelCollider::get_steerAngle()
extern void WheelCollider_get_steerAngle_mE954E7540E1ACDE5F5FE0D3A32388F6A22378CF4 (void);
// 0x0000000E System.Void UnityEngine.WheelCollider::set_steerAngle(System.Single)
extern void WheelCollider_set_steerAngle_m1E46602E8B77EB747B1EA44D84B5EC99F86BB968 (void);
// 0x0000000F System.Single UnityEngine.WheelCollider::get_rpm()
extern void WheelCollider_get_rpm_m5127D5BE0A627C29F38CFD8194C3F2470D8008B1 (void);
// 0x00000010 System.Void UnityEngine.WheelCollider::GetWorldPose(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void WheelCollider_GetWorldPose_m227D45061C7734F3ED4A43B7F89605A398BE8BB5 (void);
// 0x00000011 System.Boolean UnityEngine.WheelCollider::GetGroundHit(UnityEngine.WheelHit&)
extern void WheelCollider_GetGroundHit_mDCD55AC74710E685DBA37F02C904B8F8F3E59DB9 (void);
// 0x00000012 System.Void UnityEngine.WheelCollider::get_suspensionSpring_Injected(UnityEngine.JointSpring&)
extern void WheelCollider_get_suspensionSpring_Injected_mBDD18FD4188F66F45BB4B1519C2C0D962E0E2F34 (void);
// 0x00000013 System.Void UnityEngine.WheelCollider::set_suspensionSpring_Injected(UnityEngine.JointSpring&)
extern void WheelCollider_set_suspensionSpring_Injected_m159C1046CCF601F21004354FF2CF6ED952AB249B (void);
static Il2CppMethodPointer s_methodPointers[19] = 
{
	WheelHit_get_point_m950E6B764A768CE147DB57C1CAE81D288391E5E8,
	WheelHit_get_normal_mBC99D67AD92266C0B5EBCF8C2FC771BAE34E9D12,
	WheelHit_get_forwardSlip_m8F11089F49F1049C5D993A0E360E8738D7E600FC,
	WheelHit_get_sidewaysSlip_m6AA5854F880C9BCDA98A22DBB694408954BB0CED,
	WheelCollider_get_radius_mDB04C14DAB0BB1D5D7A2D160C5EE3F159CE3FA26,
	WheelCollider_set_radius_mC075EA6709309EBBBE43F13F02C677024263B170,
	WheelCollider_set_suspensionDistance_m086CDF09680B525B3936FC519D050C942448FA09,
	WheelCollider_get_suspensionSpring_m2C5E9C26DAB6CD1114B51464EA5428AEA20AB805,
	WheelCollider_set_suspensionSpring_mE6A9289CB03402ACA06222E5914C28EC9EB7F2E9,
	WheelCollider_set_mass_m7991E5E529D8CA73AC4C194CD297E9D3C5841260,
	WheelCollider_set_motorTorque_mFE7962DF8003D10BA646545E56F0A6B3ED8803DA,
	WheelCollider_set_brakeTorque_mDAFB1032B6B3AD4C8103869D64807596774C1B54,
	WheelCollider_get_steerAngle_mE954E7540E1ACDE5F5FE0D3A32388F6A22378CF4,
	WheelCollider_set_steerAngle_m1E46602E8B77EB747B1EA44D84B5EC99F86BB968,
	WheelCollider_get_rpm_m5127D5BE0A627C29F38CFD8194C3F2470D8008B1,
	WheelCollider_GetWorldPose_m227D45061C7734F3ED4A43B7F89605A398BE8BB5,
	WheelCollider_GetGroundHit_mDCD55AC74710E685DBA37F02C904B8F8F3E59DB9,
	WheelCollider_get_suspensionSpring_Injected_mBDD18FD4188F66F45BB4B1519C2C0D962E0E2F34,
	WheelCollider_set_suspensionSpring_Injected_m159C1046CCF601F21004354FF2CF6ED952AB249B,
};
extern void WheelHit_get_point_m950E6B764A768CE147DB57C1CAE81D288391E5E8_AdjustorThunk (void);
extern void WheelHit_get_normal_mBC99D67AD92266C0B5EBCF8C2FC771BAE34E9D12_AdjustorThunk (void);
extern void WheelHit_get_forwardSlip_m8F11089F49F1049C5D993A0E360E8738D7E600FC_AdjustorThunk (void);
extern void WheelHit_get_sidewaysSlip_m6AA5854F880C9BCDA98A22DBB694408954BB0CED_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x06000001, WheelHit_get_point_m950E6B764A768CE147DB57C1CAE81D288391E5E8_AdjustorThunk },
	{ 0x06000002, WheelHit_get_normal_mBC99D67AD92266C0B5EBCF8C2FC771BAE34E9D12_AdjustorThunk },
	{ 0x06000003, WheelHit_get_forwardSlip_m8F11089F49F1049C5D993A0E360E8738D7E600FC_AdjustorThunk },
	{ 0x06000004, WheelHit_get_sidewaysSlip_m6AA5854F880C9BCDA98A22DBB694408954BB0CED_AdjustorThunk },
};
static const int32_t s_InvokerIndices[19] = 
{
	2250,
	2250,
	2231,
	2231,
	2231,
	1858,
	1858,
	2192,
	1822,
	1858,
	1858,
	1858,
	2231,
	1858,
	2231,
	897,
	1538,
	1778,
	1778,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_VehiclesModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VehiclesModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VehiclesModule_CodeGenModule = 
{
	"UnityEngine.VehiclesModule.dll",
	19,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_VehiclesModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
