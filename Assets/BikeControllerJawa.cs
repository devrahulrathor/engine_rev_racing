using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BikeControllerJawa : MonoBehaviour
{
    public GameObject BikeZX10;
    private Rigidbody bikejawa;
    public Slider RightTurnSlider;
    public Slider LeftTurnSlider;

    public int isteer = 0;  // -1 Left, 1 Right Constrolled from Touch Arrows
    public WheelCollider WColForward;
    public WheelCollider WColBack;

    public float maxMotorTorque = 400;

    private WheelData[] wheels;
    public Transform wheelF;
    public Transform wheelB;

    private float forwardspeed = 10;
    public Slider accelpedal;
    public float maxSteeringAngle = 0.00020f; // maximum steer angle the wheel can have // 20 initial value
    float steering;
    float maxleanangle = 40.0f;
    float angletilt = 0;  //Tilt Angle while steering
    float anglelean = 0;   // Lean Angle while cornering
    float danglelean = 25;  // Delta Lean angle every Update  
    int rightarrowengaged = 0, leftarrowengaged = 0;
    private int ivehiclecrashed = 0;  // Detect Vehicle Crash mode, 1 when crashed

    public class WheelData
    {

        public WheelData(Transform transform, WheelCollider collider)
        {
            wheelTransform = transform;
            wheelCollider = collider;
            wheelStartPos = transform.transform.localPosition;
        }

        public Transform wheelTransform;
        public WheelCollider wheelCollider;
        public Vector3 wheelStartPos;
        public float rotation = 0f;
    }



    // Start is called before the first frame update
    void Start()
    {
        bikejawa = this.GetComponent<Rigidbody>();

        wheels = new WheelData[2];
        wheels[0] = new WheelData(wheelF, WColForward);
        wheels[1] = new WheelData(wheelB, WColBack);
    }

    //Collosion Detection
    void OnCollisionEnter(Collision collision)
    {
        //Ouput the Collision to the console
        Debug.Log("Collision : " + collision.gameObject.name);
        if (collision.gameObject.name != "Line001" && collision.gameObject.name != "Line001_2" && collision.gameObject.name != "Road_Track_A_01" && collision.gameObject.name != "Track_C_002" && collision.gameObject.name != "Track_B_Mesh" && Mathf.Abs(bikejawa.velocity.x) > 200)
        {
            ivehiclecrashed = 1;
            //       BikeZX10.SetActive(false);

        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //      detectCollision();   // Avoid Bouncing off and tipping over on collision
        //      steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        //     Debug.Log("Steering=");
        //     Debug.Log(steering);
        //           bikejawa.velocity = accelpedal.value *  (forwardspeed* Vector3.left + (steering/100) * Vector3.forward);  // Forward


        //        Debug.Log("bikejawa Velocity=");
        //            Debug.Log(bikejawa.velocity.x);

        //Detect when there is a collision starting

        if (ivehiclecrashed == 0 && AudioEngineDrag.gear > 0)
        {
            //        bikejawa.velocity = accelpedal.value * (transform.right * -forwardspeed + transform.forward * (steering / 100));
            bikejawa.velocity = transform.right * AudioEngineDrag.bikespeed * 0.5f * -5;
            //    bikejawa.AddForce( 10000, 0, 0, ForceMode.Impulse);
            //       updateWheels();
            //      wheelB.Rotate(0f, WColBack.rpm / 60 * 360 * Time.deltaTime, 0f);
            //       WColBack.motorTorque = maxMotorTorque * 1;

            //               Debug.Log("bikejawa Velocity=");
            //               Debug.Log(bikejawa.velocity);
            // BikeTilting while steering
            //Right
            //        if (Input.GetAxis("Horizontal") < 1 && Input.GetAxis("Horizontal") > 0)
            if (isteer == 1)
            //     if (Input.GetKey(KeyCode.RightArrow))
            {

                //   steering = maxSteeringAngle * 0.1f;
                //        transform.Rotate((-bikejawa.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);

                //      Debug.Log("Right********************************Angle Lean=");
                //      Debug.Log(anglelean);
                //        Debug.Log("maxleanangle=");
                //      Debug.Log(-maxleanangle);
                if (anglelean > -maxleanangle)  //
                {

                    anglelean = anglelean + (-danglelean * Time.deltaTime);
                    //      BikeZX10.transform.Rotate(0, 0, -30 * Time.deltaTime);   // Leaning Bike
                    BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                                                                                     //     Debug.Log("Right Angle Lean=");
                                                                                     //     Debug.Log(anglelean);
                                                                                     //     steering = maxSteeringAngle * 30 * Time.deltaTime;
                }
                steering = (maxSteeringAngle * danglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                Debug.Log("Right Steering Angle =");
                Debug.Log(steering);
                transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa

                angletilt = angletilt + 1f * steering * Time.deltaTime;



                //      rightarrowengaged = 1;
                //       Debug.Log("Negativ eAngle Lean=");
                //       Debug.Log(anglelean);
            }

            //Left
            //     else if (Input.GetAxis("Horizontal") > -1 && Input.GetAxis("Horizontal") < 0)
            else if (isteer == -1)
            //     else if (Input.GetKey(KeyCode.LeftArrow))
            {
                //   steering = maxSteeringAngle * -0.1f;
                //          transform.Rotate((-bikejawa.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);


                if (anglelean < maxleanangle)  //
                {
                    //       Debug.Log("Left********************************Angle Lean=");
                    //       Debug.Log(anglelean);
                    anglelean = anglelean + (danglelean * Time.deltaTime);
                    BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                }
                steering = -(maxSteeringAngle * danglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa

                angletilt = angletilt - 1f * steering * Time.deltaTime;




                //    leftarrowengaged = 1;
                //      Debug.Log("Positive Angle Lean=");
                //      Debug.Log(anglelean);
            }


            else
            {
                //             Debug.Log("Entered recover steering left");
                if (anglelean >= 5)
                {
                    //               Debug.Log("Entered steering left");
                    steering = -maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent bikejawa
                    BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                    anglelean = anglelean - (danglelean * Time.deltaTime);
                }
                if (anglelean <= -5)
                {
                    steering = maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent bikejawa
                    BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                    anglelean = anglelean + (danglelean * Time.deltaTime);
                }
            }

            if (angletilt > 0 && rightarrowengaged != 1)
            {
                //        transform.Rotate(-0.5f * steering * Time.deltaTime, 0, 0);
            }
            if (angletilt < 0 && rightarrowengaged != 1)
            {
                //      transform.Rotate(0.5f * steering * Time.deltaTime, 0, 0);
            }
            //    bikejawa.AddForce(accelpedal.value * forwardspeed * (Vector3.forward + Vector3.back / 2));

            //     bikejawa.velocity = accelpedal.value * forwardspeed * (Vector3.left);  // Forward

            //     bikejawa.velocity = accelpedal.value * forwardspeed * (Vector3.left + steering* Vector3.forward);  // Forward
        } // Crashed Brace
    }

    private void updateWheels()
    {
        float delta = Time.fixedDeltaTime;

        foreach (WheelData w in wheels)
        {
            WheelHit hit;

            //    Vector3 localPos = w.wheelTransform.localPosition;
            if (w.wheelCollider.GetGroundHit(out hit))
            {
                //      localPos.y -= Vector3.Dot(w.wheelTransform.position - hit.point, transform.up) - wheelRadius;
                //      w.wheelTransform.localPosition = localPos;
            }
            else
            {
                //     localPos.y = w.wheelStartPos.y;
            }

            w.rotation = Mathf.Repeat(w.rotation + delta * w.wheelCollider.rpm * 360.0f / 60.0f, 360f);
            //     WColForward.steerAngle = Mathf.Clamp(1, -1, 1) * maxSteeringAngle;
            w.wheelTransform.localRotation = Quaternion.Euler(w.rotation, 0, 0);
            //      w.wheelTransform.rotation = Quaternion.Euler(w.rotation, 0, 0); ;
        }
    }

    public void LeftSteeron()
    {
        isteer = -1;
    }
    public void LeftSteeroff()
    {
        isteer = 0;
    }
    public void RightSteeron()
    {
        isteer = 1;
    }
    public void RightSteeroff()
    {
        isteer = 0;
    }

    public void RightTurnVal()
    {
  //      danglelean = Right;
    }
}