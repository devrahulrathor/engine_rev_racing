﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterGear : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngine.itrate == 10000)
        {
            //      RPMCal();
            // AudioEngine.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
              Debug.Log("RevLimiterStop Gear");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngine.ivibrating == 1)
        {
            //          AudioEngine.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (AudioEngine.ivibrator == 1)
            //       {
            if (AudioEngine.rpm1 == 15000) AudioEngine.rpm1 = 14000;
            else AudioEngine.rpm1 = 15000;
            //          else if (AudioEngine.rpm1 == 14000) AudioEngine.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //      audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}
