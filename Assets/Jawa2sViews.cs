﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jawa2sViews : MonoBehaviour
{

    public GameObject Jawa2sGarageButton;
    public GameObject Jawa2sRoadButton;
    public GameObject Jawa2sTunnelButton;
 //   public GameObject Jawa2sViewBackImage;
  //  public GameObject Jawa2sViewForwardArrow;
  //  public GameObject Jawa2sViewBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject Jawa2sExhaustOptions;
 //   public GameObject Jawa2sViewOptions;
//    public GameObject Jawa2sPopsCracklesButton2;


    public int iJawa2sview = 0;   // Appear/Disapperar Counter
    public int iJawa2sSwipeCounter = 1;   // Decide the View Option Active: 1 Garage Active, 2 Road Active
    int iJawa2sLocation = 0;

    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void Locations()  // 
    {



        if (iJawa2sLocation == 0)
        {
            Jawa2sGarageButton.SetActive(true);
            Jawa2sRoadButton.SetActive(true);
            Jawa2sTunnelButton.SetActive(true);
            Jawa2sExhaustOptions.SetActive(false);
  //          Jawa2sPopsCracklesButton2.SetActive(false);
  //          Jawa2sViewOptions.SetActive(false);
            iJawa2sLocation = 1;
        }
        else if (iJawa2sLocation == 1)
        {
            Jawa2sGarageButton.SetActive(false);
            Jawa2sRoadButton.SetActive(false);
            Jawa2sTunnelButton.SetActive(false);
            Jawa2sExhaustOptions.SetActive(true);
     //       Jawa2sPopsCracklesButton2.SetActive(true);
     //       Jawa2sViewOptions.SetActive(true);
            iJawa2sLocation = 0;
        }
    }

    public void LocationsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        Jawa2sGarageButton.SetActive(false);
        Jawa2sRoadButton.SetActive(false);
        Jawa2sTunnelButton.SetActive(false);
   //     Jawa2sPopsCracklesButton2.SetActive(false);
        iJawa2sLocation = 0;

    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iJawa2sview == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            //          audio.Play();
            Jawa2sGarageButton.SetActive(true);
            Jawa2sRoadButton.SetActive(true);
            //        ZX10RViewBackImage.SetActive(true);


            iJawa2sview = 1;
        }
        else if (iJawa2sview == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            //        audio.Play();
            Jawa2sGarageButton.SetActive(true);
            Jawa2sRoadButton.SetActive(true);
            //       ZX10RViewBackImage.SetActive(false);
            iJawa2sview = 0;
        }

    }


    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
}

