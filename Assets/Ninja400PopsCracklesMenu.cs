﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja400PopsCracklesMenu : MonoBehaviour
{
    int iNinja400PopsCrackles = 0;
    // Other Game Options Appear/Disappear
    public GameObject Ninja400ExhaustOptions;
    public GameObject Ninja400LocationOptions;
    public GameObject Ninja400ViewsButton;  // Deactivate Exhaust Button when Console View

    public GameObject PopsCracklesButton1;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Ninja400RPopsCracklesMenu()
    {
        if (iNinja400PopsCrackles == 0)
        {
            PopsCracklesButton1.SetActive(true);
            //        DefaultView.SetActive(true);
            //        ConsoleView.SetActive(true);

            Ninja400ExhaustOptions.SetActive(false);
            Ninja400LocationOptions.SetActive(false);
            Ninja400ViewsButton.SetActive(false);
            iNinja400PopsCrackles = 1;
        }
        else if (iNinja400PopsCrackles == 1)
        {
            PopsCracklesButton1.SetActive(false);
            //          DefaultView.SetActive(false);
            //          ConsoleView.SetActive(false);

            Ninja400ExhaustOptions.SetActive(true);
            Ninja400LocationOptions.SetActive(true);
            Ninja400ViewsButton.SetActive(true);
            iNinja400PopsCrackles = 0;
        }
    }
    public void Ninja400PopsCracklesDropDownDeactivate()  // From Main Options Button on top
    {
        PopsCracklesButton1.SetActive(false);
        iNinja400PopsCrackles = 0;
    }

}
