using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExhaustPop0Corvette : MonoBehaviour
{
    public GameObject locallight;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExhaustPop()
    {
        StartCoroutine("ExhaustPop2");  //ExhaustPop after Delay
                                        //    var exp = GetComponent<ParticleSystem>();
                                        //    exp.Play();


        //             Destroy(gameObject, exp.duration);
    }
    private IEnumerator ExhaustPop2()
    {
        yield return new WaitForSeconds(AudioEngineCorvette.dtcrackle);
        var exp = GetComponent<ParticleSystem>();
        exp.Play();
        locallight.SetActive(true);
        StartCoroutine("Switchofflocallight");

        //             Destroy(gameObject, exp.duration);
    }

    private IEnumerator Switchofflocallight()
    {
        yield return new WaitForSeconds(0.08f);
        locallight.SetActive(false);
    }



}

