﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10ROptionsButton : MonoBehaviour
{
    public GameObject ZX10RViewsButton;
    public GameObject ZX10RExhaustButton;
    public GameObject ZX10ROptionButtonBackground;
    public GameObject ZX10RCameraViewButton;
    public GameObject ZX10RPopsCracklesButton;



    public int iZX10Roptions = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    public void AllOptionButtonsActive()  // 
    {
        if (iZX10Roptions == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            ZX10RViewsButton.SetActive(true);
            ZX10RExhaustButton.SetActive(true);
      //      ZX10ROptionButtonBackground.SetActive(true);
            ZX10RCameraViewButton.SetActive(true);
            ZX10RPopsCracklesButton.SetActive(true);
            iZX10Roptions = 1;

        }
        else if (iZX10Roptions == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            ZX10RViewsButton.SetActive(false);
            ZX10RExhaustButton.SetActive(false);
     //       ZX10ROptionButtonBackground.SetActive(false);
            ZX10RCameraViewButton.SetActive(false);
            ZX10RPopsCracklesButton.SetActive(false);
            iZX10Roptions = 0;

        }

    }


    }
