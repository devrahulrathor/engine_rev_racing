﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jawa2sExhaustOptions : MonoBehaviour
{
    public GameObject Jawa2sStockExhaust;
//    public GameObject Jawa2sAkra1Exhaust;
 //   public GameObject Jawa2sBMSRevoExhaust;
 //   public GameObject Jawa2sExhaustForwardArrow;
 //   public GameObject Jawa2sExhaustBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject Jawa2sLocationOptions;
//    public GameObject Jawa2sViewOptions;
 //   public GameObject Jawa2sPopsCrackles;

    public GameObject CantswitchExhaustNote;

    //   public GameObject ZX10RExhaustBackImage;
    public int iJawa2sExhaust = 0;   // Appear/Disapperar Counter
//    public int iJawa2sExhaustSwipeCounter = 1;   // Decide the Exhaust Option Active: 1 Garage Active, 2 Road Active

    public GameObject Jawa2sMainCameraView;
    //  public GameObject Jawa2sRConsoleCameraView;  // Camera Objects

    public static int iJawa2sExhaust1 = 0;   // Appear/Disapperar Counter


    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Update()
    {

      
    }


    public void Exhausts()  // 
    {


        if (iJawa2sExhaust1 == 0)
        {
            Jawa2sStockExhaust.SetActive(true);
      //      Jawa2sAkra1Exhaust.SetActive(true);
            //         Jawa2sBMSRevoExhaust.SetActive(true);
            //         ZX10RLocationOptions.SetActive(false);
     //       Jawa2sViewOptions.SetActive(false);
            Jawa2sLocationOptions.SetActive(false);
     //       Jawa2sPopsCrackles.SetActive(false);
            //       ZX10MainCameraView.SetActive(true);
            //       ZX10RConsoleCameraView.SetActive(false);

            iJawa2sExhaust1 = 1;
        }
        else if (iJawa2sExhaust1 == 1)
        {
            Jawa2sStockExhaust.SetActive(false);
       //     Jawa2sAkra1Exhaust.SetActive(false);
            //       Jawa2sBMSRevoExhaust.SetActive(false);
            //           ZX10RLocationOptions.SetActive(true);
      //      Jawa2sViewOptions.SetActive(true);
            Jawa2sLocationOptions.SetActive(true);
       //     Jawa2sPopsCrackles.SetActive(true);
            iJawa2sExhaust1 = 0;
        }
    }

    public void ExhaustsDeactivatefromOptions()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        Jawa2sStockExhaust.SetActive(false);
     //   Jawa2sAkra1Exhaust.SetActive(false);
        //     Jawa2sAkra1CarbonExhaust.SetActive(false);
        //           ZX10RLocationOptions.SetActive(true);

        iJawa2sExhaust1 = 0;
    }

    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
    public void ExhaustCantChangeNote()
    {
        if (AudioEngineNinja250.istartfromtouch == 2)
        {
            CantswitchExhaustNote.SetActive(true);
            StartCoroutine("ExhaustCantChangeNoteoff");
        }
    }

    private IEnumerator ExhaustCantChangeNoteoff()
    {
        yield return new WaitForSeconds(2f);
        //   image.color = Color.white;
        CantswitchExhaustNote.SetActive(false);

    }

}
