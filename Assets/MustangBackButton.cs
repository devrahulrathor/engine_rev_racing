﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MustangBackButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void BackButton()
    {
        if (AudioEngineMustang.itrate != -1) AudioEngineMustang.istartfromtouch = 3;
        SceneManager.LoadScene(0); // Car Options1 Main Screen
    }
}

