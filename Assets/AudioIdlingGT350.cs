﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingGT350 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineMustang.irpm == 0)
        {
            AudioEngineMustang.rpm1 = AudioEngineMustang.rpmidling;
        }
    }

    public void Idling()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngineMustang.idlingvolGT350;
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       idlingvolGT350 = 0;


    }

    double idlingvolGT350 = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        idlingvolGT350 = idlingvolGT350 - 0.1;
        if (idlingvolGT350 > 0) audio.volume = (float)idlingvolGT350;
        else
        {

            AudioEngineMustang.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();
   
        idlingvolGT350 = idlingvolGT350 + 0.1;
        if (idlingvolGT350 < 1) audio.volume = (float)idlingvolGT350;
        else
        {
            audio.volume = 1;
            AudioEngineMustang.ifadeinidling = 0;

            //            audio.Stop();
        }

    }


    public void IdlingReverbRoad()
    {
        Debug.Log("Inside Road Appear");
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }




}


