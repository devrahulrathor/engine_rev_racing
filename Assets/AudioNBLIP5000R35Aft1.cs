﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP5000R35Aft1 : MonoBehaviour
{
    public static float dt5000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR35.itrate == 5004)
        {
            AudioEngineR35.rpm1 = 5000;
            //           Debug.Log(AudioEngineR35.rpm1);
        }
        LowPassFilter();
    }
    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip5000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineR35.rpm1 >= 4000 && AudioEngineR35.rpm1 < 5000)
        {
            audio.Play();
        }
        else if (AudioEngineR35.rpm1 >= 3000 && AudioEngineR35.rpm1 < 4000)
        {
            //       dt5000 = 0.052f;
            dt5000 = 0.042f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {
            dt5000 = 0.185f;
            //         dt5000 = 0.167f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (AudioEngineR35.rpm1 < 2000)
            //      dt5000 = 0.212f;
            dt5000 = 0.418f;
        audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        audio.volume = 1;

        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip5000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip5000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip5000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}



