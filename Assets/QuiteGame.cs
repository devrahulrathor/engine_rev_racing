﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuiteGame : MonoBehaviour
{
    // Start is called before the first frame update



    public void QuitGame()
    {
        Debug.LogError("Calling Back Quit Game");
        PlayerPrefs.SetInt("SplashScreen", 0);
        Application.Quit();
    }
}
