﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuracanOptionsButton : MonoBehaviour
{
    public GameObject HuracanViewsButton;
    public GameObject HuracanExhaustButton;
    public GameObject HuracanOptionButtonBackground;
    public GameObject HuracanCameraViewButton;



    public int iHuracanoptions = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AllOptionButtonsActive()  // 
    {
        if (iHuracanoptions == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            HuracanViewsButton.SetActive(true);
            HuracanExhaustButton.SetActive(true);
            //      ZX10ROptionButtonBackground.SetActive(true);
      //      HuracanCameraViewButton.SetActive(true);
            iHuracanoptions = 1;

        }
        else if (iHuracanoptions == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            HuracanViewsButton.SetActive(false);
            HuracanExhaustButton.SetActive(false);
            //       ZX10ROptionButtonBackground.SetActive(false);
     //       HuracanCameraViewButton.SetActive(false);
            iHuracanoptions = 0;

        }

    }


}

