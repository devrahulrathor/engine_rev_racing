using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000ZX25Drag : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX25Drag.itrate == 5005)
        {
            SoundEngineZX25Drag.rpm1 = 6000;
            //           Debug.Log(SoundEngineZX25Drag.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineZX25Drag.rpm1 >= 5000 && SoundEngineZX25Drag.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (SoundEngineZX25Drag.rpm1 >= 4000 && SoundEngineZX25Drag.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.047F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 3000 && SoundEngineZX25Drag.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.091F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 2000 && SoundEngineZX25Drag.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.134F);
        }
        else if (SoundEngineZX25Drag.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.251F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
    }
}

