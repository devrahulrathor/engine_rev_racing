﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000Drag1 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (AudioEngineDrag.itrate == 5006)
        {
            AudioEngineDrag.rpm1 = 7000;
            //           Debug.Log(AudioEngineDrag.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineDrag.rpm1 >= 6000 && AudioEngineDrag.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (AudioEngineDrag.rpm1 >= 5000 && AudioEngineDrag.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.012F);
        }
        else if (AudioEngineDrag.rpm1 >= 4000 && AudioEngineDrag.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.032F);
        }
        else if (AudioEngineDrag.rpm1 >= 3000 && AudioEngineDrag.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.084F);
        }
        else if (AudioEngineDrag.rpm1 >= 2000 && AudioEngineDrag.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.148F);
        }


        else if (AudioEngineDrag.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.244F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineDrag.ReverbXoneMix;
    }
}
