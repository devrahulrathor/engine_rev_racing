using System;
using System.Collections;
using System.Globalization;
using CompleteProject;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiControllerDrag : MonoBehaviour
{
    public GameObject ConfirmQuitNote;
    public GameObject FuelOverWarningNote, FuelLowWarningNote;
    public GameObject FuelLowRedCross;
    
    int iFuellowRedCross = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnPressedBackButton0()
    {
        Time.timeScale = 0;
        AudioListener.pause = true;
        ConfirmQuitNote.SetActive(true);
    }
    public void ConfirmQuiteNoteDecativate()
    {
        ConfirmQuitNote.SetActive(false);
        Time.timeScale = 1;
        AudioListener.pause = false;
    }
    public void OnPressedBackButton()
    {

        Time.timeScale = 1;
        AudioListener.pause = false;
        //    AudioController.Instance.button.Play();
        if (MainControllerDrag.ivehicle == 1)
        {
            if (SoundEngineZX25Drag.itrate != -1) MainControllerDrag.istartfromtouch = 3;
        }
        else if(MainControllerDrag.ivehicle == 2) {
            if (SoundEngineBullet500Drag.itrate != -1) MainControllerDrag.istartfromtouch = 3;
        }
        MainControllerDrag.ikeyinfromtouch = 0;
        //  SceneManager.LoadScene("MainMenu 1");


        // Reset Factory Settings
        PlayerPrefs.SetInt("Pops", 0);  // Pops Disabled
        PlayerPrefs.SetInt("TurnControl", 1);  // Steering Buttons Enabled
       


        SceneManager.LoadScene(0);  // Track1 Scene
    }

    public void FuelLowWarningNoteActivate()
    {
        //   Time.timeScale = 0;
        //   AudioListener.pause = true;
     //   FuelLowRedCross.SetActive(true);
        StartCoroutine("FuelLowRedCrossBlink");
        //  FuelLowWarningNote.SetActive(true);
    }
    public void FuelLowWarningNoteShow()
    {
        if (MainControllerDrag.iFuellowWarning == 1)  // Enable clicking of Fuel Icon only when Fuel Level Low
        {
            Time.timeScale = 0;
            AudioListener.pause = true;
            FuelLowWarningNote.SetActive(true);
        }
    }

    public void FuelLowWarningNoteDecativate()
    {
        FuelLowWarningNote.SetActive(false);
        Time.timeScale = 1;
        AudioListener.pause = false;
    }

    private IEnumerator FuelLowRedCrossBlink()
    {
     //   Debug.Log("Inside Fuel low RedCrossBlink0");
        while (MainControllerDrag.iFuellowWarning == 1)
       {
            yield return new WaitForSeconds(0.6f);
        //    Debug.Log("Inside Fuel low RedCrossBlink1");
            if (iFuellowRedCross == 0)
            {
                FuelLowRedCross.SetActive(true);
                iFuellowRedCross = 1;
            
        }
            else if(iFuellowRedCross == 1)
            {
                FuelLowRedCross.SetActive(false);
                iFuellowRedCross = 0;
            
        }
        }
      
    }

    public void FuelOverWarningNoteActivate()
    {
        Time.timeScale = 0;
        AudioListener.pause = true;
        FuelOverWarningNote.SetActive(true);
    }

    public void FuelOverWarningNoteDecativate()
    {
    //    Debug.Log("Fuel Refilled OverNOte Deactivate");
        FuelOverWarningNote.SetActive(false);
        Time.timeScale = 1;
        AudioListener.pause = false;
    }

    public void WatchAdFuelTank()
    {
        Debug.Log("Fuel Refilled");
     //   MainControllerDrag.fuelLevel = 100;  // Fuel Tank Refilled 100 %
        MainControllerDrag.distance = 0;  // Fuel Tank Refilled 100 % after distance(ODO)reset to zero
                                          //  FuelOverWarningNote.SetActive(false);
                                          //   Time.timeScale = 1;
        MainControllerDrag.iFuellowWarning = 0;
        MainControllerDrag.iFuelOverWarning = 0;
        FuelOverWarningNoteDecativate();
        FuelLowWarningNoteDecativate();
        //   iFuellowRedCross = -1;
        StopCoroutine("FuelLowRedCrossBlink");
        FuelLowRedCross.SetActive(false);



    }

}
