﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZX10RExhaustOptions : MonoBehaviour
{
    public GameObject ZX10RStockExhaust;
    public GameObject ZX10RAkra1Exhaust;
    public GameObject ZX10RAkra1CarbonExhaust;
    public GameObject ZX10RExhaustForwardArrow;
    public GameObject ZX10RExhaustBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject ZX10RLocationOptions;
    public GameObject ZX10RViewOptions;
    public GameObject ZX10RPopsCracklesButton;

    public GameObject CantswitchExhaustNote;

    //   public GameObject ZX10RExhaustBackImage;
    public int iZX10RExhaust = 0;   // Appear/Disapperar Counter
    public int iZX10RExhaustSwipeCounter = 1;   // Decide the Exhaust Option Active: 1 Garage Active, 2 Road Active

    public GameObject ZX10MainCameraView, ZX10RConsoleCameraView;  // Camera Objects

    public static int iZX10RExhaust1 = 0;   // Appear/Disapperar Counter


    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Update()
    {

        if (ZX10CameraViewChange.iZX10CameraSwitch == 1)  // Deactivate Button for Exhaust View Change when Console View
        {

            //       GetComponent<Button>().interactable = false;
        }
        else if (ZX10CameraViewChange.iZX10CameraSwitch == 0)
        {

            //              GetComponent<Button>().interactable = true;
        }
        if (iZX10RExhaust1 == 110)  // Disabled
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iZX10RExhaustSwipeCounter == 2)
                {
                    ZX10RExhaustForwardArrow.SetActive(true);
                    ZX10RExhaustBackArrow.SetActive(true);
                }
                else if (iZX10RExhaustSwipeCounter == 3)
                {
                    ZX10RExhaustForwardArrow.SetActive(false);
                    ZX10RExhaustBackArrow.SetActive(true);
                }
                else if (iZX10RExhaustSwipeCounter == 1)
                {
                    ZX10RExhaustForwardArrow.SetActive(true);
                    ZX10RExhaustBackArrow.SetActive(false);
                }
                if (pos.y > 400 && pos.y < 600 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iZX10RExhaustSwipeCounter == 1 && iswipeenable == 1)
                        {
                            ZX10RStockExhaust.SetActive(false);
                            ZX10RAkra1Exhaust.SetActive(true);
                            iZX10RExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iZX10RExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            ZX10RAkra1Exhaust.SetActive(false);
                            ZX10RAkra1CarbonExhaust.SetActive(true);
                            iZX10RExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                    if (touchZero.deltaPosition.x > 10)
                    {

                        if (iZX10RExhaustSwipeCounter == 3 && iswipeenable == 1)
                        {
                            //        ZX10RTBR1Exhaust.SetActive(false);
                            ZX10RAkra1Exhaust.SetActive(true);
                            ZX10RAkra1CarbonExhaust.SetActive(false);
                            //               ZX10RStockExhaust.SetActive(false);
                            iZX10RExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iZX10RExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            ZX10RAkra1Exhaust.SetActive(false);
                            ZX10RStockExhaust.SetActive(true);
                            iZX10RExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }


    public void Exhausts()  // 
    {


        if (iZX10RExhaust1 == 0)
        {
            ZX10RStockExhaust.SetActive(true);
            ZX10RAkra1Exhaust.SetActive(true);
            //            ZX10RAkra1CarbonExhaust.SetActive(true);   // TBR Muffler
            //         ZX10RLocationOptions.SetActive(false);
            ZX10RViewOptions.SetActive(false);
            ZX10RLocationOptions.SetActive(false);
            ZX10RPopsCracklesButton.SetActive(false);

            //       ZX10MainCameraView.SetActive(true);
            //       ZX10RConsoleCameraView.SetActive(false);

            iZX10RExhaust1 = 1;
        }
        else if (iZX10RExhaust1 == 1)
        {
            ZX10RStockExhaust.SetActive(false);
            ZX10RAkra1Exhaust.SetActive(false);
            ZX10RAkra1CarbonExhaust.SetActive(false);
            //           ZX10RLocationOptions.SetActive(true);
            ZX10RViewOptions.SetActive(true);
            ZX10RLocationOptions.SetActive(true);
            ZX10RPopsCracklesButton.SetActive(true);
            iZX10RExhaust1 = 0;
        }
        CantswitchExhaustNote.SetActive(false);
    }

    public void ExhaustsDeactivatefromOptions()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        ZX10RStockExhaust.SetActive(false);
        ZX10RAkra1Exhaust.SetActive(false);
        ZX10RAkra1CarbonExhaust.SetActive(false);
        //           ZX10RLocationOptions.SetActive(true);
        CantswitchExhaustNote.SetActive(false);
        iZX10RExhaust1 = 0;
    }

    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }


    public void ExhaustCantChangeNote()
    {
        if (AudioEngine.istartfromtouch == 2)
        {
            CantswitchExhaustNote.SetActive(true);
            StartCoroutine("ExhaustCantChangeNoteoff");
        }
    }

    private IEnumerator ExhaustCantChangeNoteoff()
    {
        yield return new WaitForSeconds(2f);
        //   image.color = Color.white;
        CantswitchExhaustNote.SetActive(false);

    }
}
