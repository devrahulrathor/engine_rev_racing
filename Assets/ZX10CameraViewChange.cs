﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10CameraViewChange : MonoBehaviour
{
    public static float ConsoleCameraAngle = 0.0f, ConsoleCameraAngleStep = 0.9f;
     public static int iZX10CameraSwitch = 0; // To Switch Camera from Default to Console 
    int iZX10CameraSwitch2 = 0; // To Switch Camera from Default to Console
    public GameObject ZX10MainCameraView, ZX10RConsoleCameraView;  // Camera Objects
    public GameObject RPMConsole, ConsoleBackground, Image360;   //Partillly take off Console Buttons
    public GameObject DefaultView, ConsoleView;  // GUI Images

    // Other Game Options Appear/Disappear
    public GameObject ZX10RExhaustOptions;
    public GameObject ZX10RLocationOptions;
    public GameObject ExhaustButton;  // Deactivate Exhaust Button when Console View
    public GameObject ZX10RPopsCracklesButton;  // Deactivate Pops Crackles Button

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //     ZX10MainCameraView.transform.position = Vector3.Lerp(ZX10MainCameraView.transform.position, ZX10RConsoleCameraView.transform.position, 0.5f * Time.deltaTime);
        //      do {
        //         if (ZX10MainCameraView.transform.position.x >= -10 && ZX10MainCameraView.transform.position.y >= -10)
        //         {
        //            ZX10MainCameraView.transform.Rotate(0, -ConsoleCameraAngleStep, 0);
        //             ConsoleCameraAngle = ConsoleCameraAngle + ConsoleCameraAngleStep;
        //         }
        //     } while (ConsoleCameraAngle <= 90.0f);
    }





    public void ZX10ViewChange()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        if (iZX10CameraSwitch == 0)
        {
            ZX10MainCameraView.SetActive(false);
            ZX10RConsoleCameraView.SetActive(true);
            ConsoleView.SetActive(true);
            DefaultView.SetActive(false);
            RPMConsole.SetActive(false);
            ConsoleBackground.SetActive(false);
            Image360.SetActive(false);

            
            iZX10CameraSwitch = 1;
        }
        else if (iZX10CameraSwitch == 1)
        {
            ZX10MainCameraView.SetActive(true);
            ZX10RConsoleCameraView.SetActive(false);
            DefaultView.SetActive(true);
            ConsoleView.SetActive(false);
            RPMConsole.SetActive(true);
            ConsoleBackground.SetActive(true);
            Image360.SetActive(true);
         
            iZX10CameraSwitch = 0;
        }
    }
    //      do
    //      {
    //          ZX10MainCameraView.transform.position = Vector3.Lerp(ZX10MainCameraView.transform.position, ZX10RConsoleCameraView.transform.position, 0.004f * Time.deltaTime);
    //         ZX10MainCameraView.transform.Rotate(0, -ConsoleCameraAngleStep, 0);
    //        ConsoleCameraAngle = ConsoleCameraAngle + ConsoleCameraAngleStep;
    //    } while (ConsoleCameraAngle <= 9000.0f);



    public void ZX10ViewToggle()
    {
        if (iZX10CameraSwitch2 == 0 )
        {
            DefaultView.SetActive(true);
            ConsoleView.SetActive(true);

            ZX10RExhaustOptions.SetActive(false);
            ZX10RLocationOptions.SetActive(false);
            ZX10RPopsCracklesButton.SetActive(false);
            iZX10CameraSwitch2 = 1;
        }
        else if (iZX10CameraSwitch2 == 1)
        {
            DefaultView.SetActive(false);
            ConsoleView.SetActive(false);

            ZX10RExhaustOptions.SetActive(true);
            ZX10RLocationOptions.SetActive(true);
            ZX10RPopsCracklesButton.SetActive(true);
            iZX10CameraSwitch2 = 0;
        }


    }

    public void CameraViewsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {
        DefaultView.SetActive(false);
        ConsoleView.SetActive(false);
        iZX10CameraSwitch2 = 0;
    }

        public void ZX10DefaultView()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);


        ZX10MainCameraView.SetActive(true);
        ZX10RConsoleCameraView.SetActive(false);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
        RPMConsole.SetActive(true);
        ConsoleBackground.SetActive(true);

        iZX10CameraSwitch = 0;

    }

    public void ZX10ConsoleView()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        iZX10CameraSwitch = 1;
        ZX10MainCameraView.SetActive(false);
        ZX10RConsoleCameraView.SetActive(true);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
        RPMConsole.SetActive(false);
        ConsoleBackground.SetActive(false);



    }

}
