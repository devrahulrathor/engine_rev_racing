﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja250Views : MonoBehaviour
{
    public GameObject Ninja250GarageButton;
    public GameObject Ninja250RoadButton;
    public int iNinja250view = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iNinja250view == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Ninja250GarageButton.SetActive(true);
            Ninja250RoadButton.SetActive(true);
           
        
            iNinja250view = 1;
        }
        else if(iNinja250view == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Ninja250GarageButton.SetActive(false);
            Ninja250RoadButton.SetActive(false);
            iNinja250view = 0;
        }

    }

        }
