using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;

public class MainControllerS1000RR : MonoBehaviour
{

    double startTick;
    public SoundEngineS1000RR SES1000RR;  // Refernce of Sound Engine
    public GameObject Huracan, Aventador; //Physical Car Appera/Disapper

    public static int ivehicle = 1; // Vehicle Variable: 1 Huracan, 2-Aventador
    

    public int ikeyin = 0;    // Ignition Key in Counter
    public static int ikeyinfromtouch = 0;
    public static int istart = -100;    // Start Counter
    public static int istartfromtouch = 0;
    public int istartdisable = 0; // Disable for 1.5sec after first start, 0-enabled, 1-disabled
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs                                                           
    public int iRPMIdleSettle = 0; // Counter for Console RPM Warm up and settle-0-3000-Idling RPM

    public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 
    public static int idlingvolsport = 0;  // Counter to set idling volume(sport Mode) 1 when engine starting 

    public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = 0;
 //   public static int iblipcanstart = 1;  // Counter for Blip Start
//    public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 0;

    int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


  //  public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 2000;
//    public float rpmdisplayafterstart = 1.4f;  // Time in seconds after which Idling RPM start post Startup
//    public static int rpmblipconst;  // TO Stop Blip at different RPMs
    public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public static float sliderval;
    public static float slidervalold;

    // Fade in out counters
    public static int ifadeoutnfsr = 0, ifadeinnfsr = 0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)

    public static int ifadeinidling = 0, ifadeoutidling = 0;
    public static int ifadeinidlingsport = 0, ifadeoutidlingsport = 0;
    public static int ifadeoutsli = 0, ifadeoutfsi1 = 0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters
    public GameObject GarageView;   // Appearance/Disappearance of Garage
    public static float ReverbXoneMix = 1.09f;   // Reverberation Value
    public GameObject RoadView;   // Appearance/Disappearance of Road
    public GameObject TunnelView;   // Appearance/Disappearance of Road

    public BinSaveLoad BinarySaveLoad;     // Gam Obj to Save Load Consumables in Binary

    // Exhaust/Drive Mode Counters
    public int exhaustDropDown = 0; // 0 Dropdown up, 1 Dropdown down
    public int iexhaust = 0;  // 0 for Stock, 1 for Akra1,  
                              //    public Button Exhaustbutton;  // Exhaust Option Button
    public GameObject Exhaustbutton;
    public GameObject ExhaustStockbutton;
    public GameObject StockExhaustTick;
    public GameObject ExhaustAkra1button;
    public GameObject Akra1ExhaustTick;
    public GameObject SportModePurPanel;    // Panel on SportMode GUI to grey out, disappears when purchased


    // GamrObject for RPMConsole1 to Enlarge
    public GameObject RPMConsole1;
    public int iRPMConsole = 0;  // 1:Enlarge, 0:Reduce

    //Backfire Section
    public static float dtcrackle = 0.4f;  // Delay on Backfire dtcrackle8000
    public ExhaustPopHuracan AVExhaustPop;
    public ExhaustPopHuracan1 AVExhaustPop1;
    public ExhaustPopHuracan2 AVExhaustPop2;
    public ExhaustPopHuracan3 AVExhaustPop3;
    public LighExhaustPop LightExhaustPop;


      public ExhaustPopAventadorL AVExhaustPopAventadorL;
     public ExhaustPopAventadorR  AVExhaustPopAventadorR;

    public TailLightLeftEmission TailLEmission;
    public TailLightRightEmission TailREmission;



    //Store Section
    public static int HuracanSportMode = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs


    // Headlight Object
    public HeadlightHuracan Hlight;
    public TailLightsHuracan Rlight;
    public GameObject HlightAventador, TaillightAventador;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 


    // Paid Options
    public AudioScriptCrackle8000S1000RR ACrackle8000;  // Crackle 8000
    public AudioScriptCrackle6000S1000RR ACrackle6000;  // Crackle 6000
    public AudioScriptCrackle4000S1000RR ACrackle4000;  // Crackle 4000
    public static int icrackle = 0, icrackleold = 0;                            // Enable Crackle 

    int ineedle = 0;
    int[] rpmidlesettleed = { 500, 800, 1000, 1500, 1800, 2000, 2200, 2500, 2800, 3000, 3200, 3000, 2800, 2800, 2500, 2500, 2200, 2200, 2200, 2000, 2000, 2000, 2000 };  // RPM Vlues for RPM settle down

    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No


    //RateUs Variables
    public int irevcountrateus = 0;  // Counter to decide when user rates when the count is greated than irevcountmaxrateus
    public int irevcountmaxrateus = 50;
    int irateusloopdisable = 0;
    int irateusdialogdisable = 0;

    [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, ModeSport, ModeNormal, RPM0, RPM200, RPM500, RPM800, RPM1000, RPM1100, RPM1500, RPM1800, RPM2000, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;

    void Awake()
    {
        Application.targetFrameRate = 1000;
    }
    // Use this for initialization
    void Start()
    {
        //       PlayerPrefs.SetInt("nonconsumableval101", 0);
        //      HuracanSportMode = PlayerPrefs.GetInt("nonconsumableval101");
        BinarySaveLoad.SaveHuracanMode(2);
    }

    // Update is called once per frame
    void Update()
    {
        //       slidervalold = sliderval;
        //      Debug.log(sliderval);

        //      trate = (sliderval - slidervalold);
        //        throttlediff = (sliderval * 150 - rpm1);
        // Starting Module
        //      if (Input.GetKeyDown("space") || istart == 0)

        // Section to enable Purchsed Sport Mode from PLaystore
        if (HuracanSportMode == 0) BinarySaveLoad.LoadHuracanMode();  // Binary Load Cnsumable Data
                                                                      // HuracanSportMode = PlayerPrefs.GetInt("nonconsumableval101");
        if (HuracanSportMode == 1)
        {
            if (SportModePurPanel.activeInHierarchy == true)
            {
                {
                    SportModePurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                         //                   Debug.Log("Aftermarket Exhaust 1 Purchased");
                }
            }
        }
        //   PlayerPrefs.SetInt("RevCounttoRate", 0);
        //  PlayerPrefs.SetInt("RateUsPopUpDisabled", 0);
        // Rate Us Module
        if (rpm1 > 4000 && irateusloopdisable == 0)
        {
            //      Debug.Log("INside Fiesta Rateus");
            irateusloopdisable = 1;
            irevcountrateus = PlayerPrefs.GetInt("RevCounttoRate");
            irevcountrateus = irevcountrateus + 1;
            PlayerPrefs.SetInt("RevCounttoRate", irevcountrateus);
            irateusdialogdisable = PlayerPrefs.GetInt("RateUsPopUpDisabled");  // 1 If permanently disabled
            if (irevcountrateus > irevcountmaxrateus && irateusdialogdisable == 0)
            {

                RateGame.Instance.ShowRatePopup();
                irateusdialogdisable = 1;
                PlayerPrefs.SetInt("RateUsPopUpDisabled", 1);
            }
        }
        if (rpm1 < 4000) irateusloopdisable = 0;

        // RateUs End


        // Ingnition On Module
        if (ikeyinfromtouch == 1)
        {
            ikeyinfromtouch = 2;
            //         NeutralButtonOff.enabled = true;

            // Ignition Key Rotate to On Position
            IgnitionKeyOn.enabled = true;
            IgnitionKeyOff.enabled = false;


            RedButtonOn.enabled = true;
            iconsolestartup = 1;  // Console Startup Loop Enabler
                                  //  AStartup.Startup();
        //    SES1000RR.StartupSound();
            NeutralButtonOff.enabled = false;
            NeutralButtonOn.enabled = true;
            StartCoroutine("ConsoleStartup");

            // Car Lights On
            //          TailLEmission.TailLightEmissionOn();
            //          TailREmission.TailLightEmissionOn();



        }


   //     Debug.Log("@@@@@@@@@@@@@@@@@@ RPM in Main Controller 1 =");
   //     Debug.Log(rpm1);


        // Start Module
        if ((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1)
        {
            // Enable Console neutral Buttons
            //        NeutralButtonOff.enabled = false;
            //        NeutralButtonOn.enabled = true;

            //        istart = 1;
            StartCoroutine("Startistart1");  // Starting the Loop for Events
            istartfromtouch = 2;

            gear = 0;
            SES1000RR.StartingfromMainController();  // Call for Staring in Sound Engine
       //     AStarting.Starting();
            // Module for startup rpm change increase and reduce 0-3000-2000
            iRPMIdleSettle = 1; // Idle-Warm Up RPM enabled
            ineedle = 0;
            StartCoroutine("RPMIdleSettle");
            // Module for startup ends

            itrate = 1;  // Enable SLI
            if (icrackle == 0) SoundEngineS1000RR.idlingvol = 1;
            if (icrackle == 1) SoundEngineS1000RR.idlingvolsport = 1;
            istartdisable = 1;    // Disable Start Button Function StartStopfromTouch till 1.5 sec till Idling starts
            StartCoroutine("StartIdling");
            SES1000RR.TriggerIdlingfromMainController();  // Start Idling sound in Sound Engine
            //       irpm = 0;

        }
   //     Debug.Log("@@@@@@@@@@@@@@@@@@ RPM in Main Controller 2 =");
   //     Debug.Log(rpm1);
        // Shuttin Module
        if ((Input.GetKeyDown("space") && istart == 1) || istartfromtouch == 3)
        {
            // Enable Console neutral Buttons
            //       NeutralButtonOff.enabled = true;
            //       NeutralButtonOn.enabled = false;
            

            itrate = -1;
            SES1000RR.ShuttingfromMainController();  // Call for Shutting on Sound Engine
            istart = 0;
            istartfromtouch = 0;

            //       itrate = -1;
            //      SoundEngineZX10.irpm = -1;  // For stopping RPM at Idling

            // Stop all sounds

            ivibrating = 0; // Stop Vibrating after Rev Limter Stops

            //      SoundEngineZX10.rpm1 = 0;
            Debug.Log("RPM in Shutting 1");
            Debug.Log(SoundEngineZX10.rpm1);
           
          



            rpm1 = 0;
            RemoveRPMConsole();
            RPM0.enabled = true;

            // Car Lights Off
            //         TailLEmission.TailLightEmissionOff();
            //         TailREmission.TailLightEmissionOff();
        }
   //     Debug.Log("@@@@@@@@@@@@@@@@@@ RPM in Main Controller 3 =");
   //     Debug.Log(rpm1);
        // Throttle Spring back Module

        if (istart == 1)
        {
            // Touch Release Module
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
                        //                Debug.Log("Touch Begun");
                        //             message = "Begun ";
                        break;
                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                        mSlider.value = 0.0f;

                        break;
                }
            }
            // Mouse Release Module
            if (Input.GetMouseButtonUp(0))
            {

              Debug.Log("Pressed primary button.");
                mSlider.value = 0.0f;
            }
        }


        // Throttle  Events


        rpm1 = SoundEngineS1000RR.rpm1;  // Reading RPM from SoundEngine
   //     Debug.Log("RPM in MainCOntroller 4=");
   //     Debug.Log(rpm1);
                                      //       Debug.Log("RPM in MinController");
                                      //       Debug.Log(SoundEngineZX10.rpm1);
                                      //    Debug.Log("RPM in MainController");
                                      //    Debug.Log(SoundEngineZX10.rpm1);
        trate = (sliderval - slidervalold);   // Calculating throttle rate (of change) to decide appropriate sound cycle in Sound Engine
        throttlediff = (sliderval * 250 - (rpm1-1)); // Calculating throttle difference to decide appropriate sound cycle in Sound Engine

        //Refresh Throttle Consoles
        slidervalold = sliderval;  // Assign Slidervalue to Slidrold
        

        if (rpm1 != 0)
        {
            RemoveRPMConsole();
            RPMConsoleUpdate();
        }
        // Mode Change Idling Changei

        if ((itrate == 1) && icrackle != icrackleold)   //then change idling sound
        {
          
        }


        // Idling Pulsating Needle








        


    }
    // Coroutines***************************************

    public void RPMConsoleUpdate()
    {
        // Idling Pulsating Needle
     
            if (irpm == 0)
            {
                if (iidleneedlemoved == 0)
                {
                    int randidle = Random.Range(0, 10);
                    if (randidle >= 5)
                    {
                        RPM1000.enabled = true;
                        RPM1100.enabled = false;
                    }
                    else if (randidle == 0)
                    {
                        iidleneedlemoved = 1;
                        //        StartCoroutine("EnableIdleRPMNeedleMove");
                        //                   yield return new WaitForSeconds(0.25f);
                        RPM1100.enabled = true;
                        RPM1000.enabled = false;
                    }
                }
            }
           if (rpm1 == 0) RPM0.enabled = true;
        
            if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
            if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
            if (rpm1 >= 1500 && rpm1 < 2000) RPM1500.enabled = true;
            if (rpm1 >= 2000 && rpm1 < 2200) RPM2000.enabled = true;
            if (rpm1 >= 2200 && rpm1 < 2500) RPM2200.enabled = true;
            if (rpm1 >= 2500 && rpm1 < 2800) RPM2500.enabled = true;
            if (rpm1 >= 2800 && rpm1 < 3000) RPM2800.enabled = true;
            if (rpm1 >= 3000 && rpm1 < 3200) RPM3000.enabled = true;
            if (rpm1 >= 3200 && rpm1 < 3500) RPM3500.enabled = true;
            if (rpm1 >= 3500 && rpm1 < 3800) RPM3800.enabled = true;
            if (rpm1 >= 3800 && rpm1 < 4000) RPM4000.enabled = true;
            if (rpm1 >= 4000 && rpm1 < 4200) RPM4200.enabled = true;
            if (rpm1 >= 4200 && rpm1 < 4500) RPM4500.enabled = true;
            if (rpm1 >= 4500 && rpm1 < 4800) RPM4800.enabled = true;
            if (rpm1 >= 4800 && rpm1 < 5000) RPM5000.enabled = true;
            if (rpm1 >= 5000 && rpm1 < 5200) RPM5200.enabled = true;
            if (rpm1 >= 5200 && rpm1 < 5500) RPM5500.enabled = true;
            if (rpm1 >= 5500 && rpm1 < 6000) RPM6000.enabled = true;
            if (rpm1 >= 6000 && rpm1 < 6500) RPM6500.enabled = true;
            if (rpm1 >= 6500 && rpm1 < 7000) RPM7000.enabled = true;
            if (rpm1 >= 7000 && rpm1 < 7500) RPM7500.enabled = true;
            if (rpm1 >= 7500 && rpm1 < 8000) RPM8000.enabled = true;
            if (rpm1 >= 8000 && rpm1 < 8500) RPM8500.enabled = true;
            if (rpm1 >= 8500 && rpm1 < 9000) RPM9000.enabled = true;
            if (rpm1 >= 9000 && rpm1 < 9500) RPM9500.enabled = true;
            if (rpm1 >= 9500 && rpm1 < 10000) RPM10000.enabled = true;
            if (rpm1 >= 10000 && rpm1 < 10500) RPM10500.enabled = true;
            if (rpm1 >= 10500 && rpm1 < 11000) RPM11000.enabled = true;
            if (rpm1 >= 11000 && rpm1 < 11500) RPM11500.enabled = true;
            if (rpm1 >= 11500 && rpm1 < 12000) RPM12000.enabled = true;
            if (rpm1 >= 12000 && rpm1 < 12500) RPM12500.enabled = true;
            if (rpm1 >= 12500 && rpm1 < 13000) RPM13000.enabled = true;
            if (rpm1 >= 13000 && rpm1 < 13500) RPM13500.enabled = true;
            if (rpm1 >= 13500 && rpm1 < 14000) RPM14000.enabled = true;
            if (rpm1 >= 14000 && rpm1 < 14500) RPM14500.enabled = true;
            if (rpm1 >= 14500 && rpm1 <= 15000) RPM15000.enabled = true;
            //         if (rpm1 > 14000) RedlineButton.enabled = true;
            else if (rpm1 < 14000) RedlineButton.enabled = false;
        
    }

        // RPM Console Effects

        // Idling needle Enable Move
        private IEnumerator EnableIdleRPMNeedleMove()
    {
        yield return new WaitForSeconds(0.5f);
        iidleneedlemoved = 0;
    }

    IEnumerator Startistart1()
    {
        yield return new WaitForSeconds(1.7f);
        istart = 1;
    }



    // Start Idling
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(1.5f); // wait half a second
        istartdisable = 0;
   //    SES1000RR.StartIdlingfromMainController();  // Start Idling sound in Sound Engine

      
        // Red Console button Off
        RedButtonOn.enabled = false;

    }




   

    // Keyin from Touch
    public void KeyInFromTouch()
    {
        //      moveSpeed = newSpeed;

        if (ikeyinfromtouch == 0)
        {
            ikeyinfromtouch = 1;
            // Headlight on Module
            Hlight.HeadlightOn();
            Rlight.RearlightOn();
            HlightAventador.SetActive(true);
            TaillightAventador.SetActive(true);
        }

        else if (ikeyinfromtouch == 2 && istartfromtouch != 0)
        {
            ikeyinfromtouch = 0;
            istartfromtouch = 3;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            HlightAventador.SetActive(false);
            TaillightAventador.SetActive(false);
        }
        else if (ikeyinfromtouch == 2)
        {
            ikeyinfromtouch = 0;
            RedButtonOn.enabled = false;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            HlightAventador.SetActive(false);
            TaillightAventador.SetActive(false);
        }




    }
    // StartStop from Touch
    public void StartStopFromTouch()
    {
        //      moveSpeed = newSpeed;
        if (istartdisable == 0)
        {
            if (ikeyinfromtouch == 2)
            {
                if (istartfromtouch == 0) istartfromtouch = 1;
                else if (istartfromtouch == 2) istartfromtouch = 3;
            }
            // Make StartStopButton Dark
            StartStopButton.enabled = false;
            StartStopButtonPressed.enabled = true;
        }
        //       StartCoroutine("StartStopButtonAppear");
    }
    // ReAppear StartStopButton(Original)


    // ReAppear StartStopButton(Original)
    public void Start1StopButtonAppear()  // 
    {
        Debug.Log("Start ButtonAppear_________________________________________");
        StartStopButton.enabled = true;
        StartStopButtonPressed.enabled = false;

    }

    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        ReverbXoneMix = 0.00f;
        RoadView.SetActive(true); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage
        TunnelView.SetActive(false); // DisAppearance of Tunnel

        HeadlightReflectionR.SetActive(false);
        HeadlightReflectionL.SetActive(false);

        SES1000RR.RoadAppear();

    }

    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {

        ReverbXoneMix = 1.03f;
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(true); // Disapperance of Garage
        TunnelView.SetActive(false); // DisAppearance of Tunnel

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        SES1000RR.GarageAppear();
    }

    // DisAppearance of Racetrack Road/Garage
    public void TunnelAppear()  // 
    {
        ReverbXoneMix = 1.1f;
        TunnelView.SetActive(true); // Disapperance of Garage
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        SES1000RR.TunnelAppear();
    }



    //   public void ExhaustStock()  // 
    //  {
    //      if (istartfromtouch != 2)
    //      { // Only works when Engine is off
    //          iexhaust = 0;
    ///          //       exhaustDropDown = 0;
    //         StockMuffler.SetActive(true); // Disapperance of Stovk Muffler
    //         StockExhaustTick.SetActive(true); // Tick on Stock Ex Panel
    //         Akra1ExhaustTick.SetActive(false); // Tick vanish on Akra1 Ex Panel
    //         Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel

    //            StockMufflerShield.SetActive(true); // Disapperance of Stovk Muffler Shield
    //          AkraCarbonMuffler.SetActive(false); // Disapperance of Akra Carbon Muffler                                   // Akra1Muffler.SetActive(false); // Disapperance of Stovk Muffler
    //          AkraBlackMuffler.SetActive(false); // Disapperance of Stovk Muffler
    //          StockMufflerSoundSource.SetActive(true);  //Activate Akra1 Sound Sources
    //         Akra1MufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
    //     }

    // }
    //  public void ExhaustAkra1()  // 
    // {
    //     if (istartfromtouch != 2)
    //     { // Only works when Engine is off
    //         if (ZX10ExhaustAkra1 == 1)  // Only Activate when Akra1 Purchased
    //         {
    //             iexhaust = 1;
    //     exhaustDropDown = 0;
    //             StockExhaustTick.SetActive(false); // Disapperance of Stovk Muffler
    //             Akra1ExhaustTick.SetActive(true); // Tick on Stock Ex Panel
    //            Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel


    //          ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
    //        StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
    //        StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler Shield
    //   Akra1Muffler.SetActive(true); // Disapperance of Stovk Muffler
    //        AkraCarbonMuffler.SetActive(false); // Disapperance of Stovk Muffler
    //        AkraBlackMuffler.SetActive(true); // Disapperance of Stovk Muffler
    //        StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
    //        Akra1MufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
    //    }
    // }
    // }



    public void MoveRPMConsole1()
    {
        if (iRPMConsole == 0)
        {
            RPMConsole1.transform.position = new Vector3(1530, 760, 0);
            RPMConsole1.transform.localScale += new Vector3(0.35f, 0.3f, 0);

            //    RPMConsole1.SetActive(false);
            iRPMConsole = 1;
        }
        else if (iRPMConsole == 1)
        {
            //     RPMConsole1.SetActive(true);
            RPMConsole1.transform.localScale += new Vector3(-0.35f, -0.3f, 0);
            RPMConsole1.transform.position = new Vector3(900, 140, 0);
            iRPMConsole = 0;
        }
    }

    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        sliderval = sliderposition;

    }

    // GUI Events Functions******************************************
    public void VibratorOnOff()
    {
        if (ivibrator == 0) ivibrator = 1;
        else if (ivibrator == 1) ivibrator = 0;

    }


    // Vehicle Change Method
    public void VehicleHuracan()
    {
        ivehicle = 1;
        Aventador.SetActive(false);
        Huracan.SetActive(true);
    }
    public void VehicleAventador()
    {
        ivehicle = 2;
        Aventador.SetActive(true);
        Huracan.SetActive(false);
    }
    // Crackle/Pops Bangs OnOff  Sport/Normal Mode
    public void CrackleOnOff()
    {
        if (icrackle == 0)
        {
            icrackle = 1;
            ModeSport.enabled = true;
            ModeNormal.enabled = false;
        }
        else if (icrackle == 1)
        {
            icrackle = 0;
            ModeSport.enabled = false;
            ModeNormal.enabled = true;
        }
    }

    //  Exhaust Normal Mode
    public void ExhaustSportModeOn()
    {
        if (HuracanSportMode == 1)  // Only Activate when Akra1 Purchased
        {
            icrackle = 1;
            ModeSport.enabled = true;
            ModeNormal.enabled = false;
            StockExhaustTick.SetActive(false); // Tick on Stock Ex Panel
            Akra1ExhaustTick.SetActive(true); // Tick vanish on Akra1 Ex Panel
        }
    }
    //  Exhaust Normal Mode
    public void ExhaustNormalModeOn()
    {
        icrackle = 0;
        ModeSport.enabled = false;
        ModeNormal.enabled = true;
        //      StockMuffler.SetActive(true); // Disapperance of Stovk Muffler
        StockExhaustTick.SetActive(true); // Tick on Stock Ex Panel
        Akra1ExhaustTick.SetActive(false); // Tick vanish on Akra1 Ex Panel

    }


    // Console Startup Sequence************************************************************
    int rpmi = 1000;   // Counter for rpm change for Console Startup Sequence
                       //   float wait=0.1;
    private IEnumerator ConsoleStartup()
    {
        yield return new WaitForSeconds(1f);
        while (iconsolestartup == 1 || iconsolestartup == -1)
        {
            yield return new WaitForSeconds(0.02f); // wait half a second
     //       Debug.Log("INside Startup");
            RemoveRPMConsole();
            //     if (rpmi <= 7400)
            //     {
            //     if (iconsolestartup == 1 && rpmi <= 14500)  for superbike with 15000 RPM limit
            if (iconsolestartup == 1 && rpmi <= 8000)   //for supercar with 9000 RPM limit 
            {
                rpmi = rpmi + iconsolestartuprate * 2;
            }
            else iconsolestartup = -1;
            if (iconsolestartup == -1 && rpmi >= 1000)
            {
                rpmi = rpmi - iconsolestartuprate * 2;
                if (rpmi <= 900)
                {
                
                    rpmi = 0;
                    iconsolestartup = 0;
                }
            }

            if (rpmi < 900) RPM0.enabled = true;
           
            if (rpmi >= 900 && rpmi < 1500) RPM1000.enabled = true;
            if (rpmi >= 1500 && rpmi < 2000) RPM1500.enabled = true;
            if (rpmi >= 2000 && rpmi < 2200) RPM2000.enabled = true;
            if (rpmi >= 2200 && rpmi < 2500) RPM2200.enabled = true;
            if (rpmi >= 2500 && rpmi < 2800) RPM2500.enabled = true;
            if (rpmi >= 2800 && rpmi < 3000) RPM2800.enabled = true;
            if (rpmi >= 3000 && rpmi < 3200) RPM3000.enabled = true;
            if (rpmi >= 3200 && rpmi < 3500) RPM3500.enabled = true;
            if (rpmi >= 3500 && rpmi < 3800) RPM3800.enabled = true;
            if (rpmi >= 3800 && rpmi < 4000) RPM4000.enabled = true;
            if (rpmi >= 4000 && rpmi < 4200) RPM4200.enabled = true;
            if (rpmi >= 4200 && rpmi < 4500) RPM4500.enabled = true;
            if (rpmi >= 4500 && rpmi < 4800) RPM4800.enabled = true;
            if (rpmi >= 4800 && rpmi < 5000) RPM5000.enabled = true;
            if (rpmi >= 5000 && rpmi < 5200) RPM5200.enabled = true;
            if (rpmi >= 5200 && rpmi < 5500) RPM5500.enabled = true;
            if (rpmi >= 5500 && rpmi < 6000) RPM6000.enabled = true;
            if (rpmi >= 6000 && rpmi < 6500) RPM6500.enabled = true;
            if (rpmi >= 6500 && rpmi < 7000) RPM7000.enabled = true;
            if (rpmi >= 7000 && rpmi < 7500) RPM7500.enabled = true;
            if (rpmi >= 7500 && rpmi < 8000) RPM8000.enabled = true;
            if (rpmi >= 8000 && rpmi < 8500) RPM8500.enabled = true;
            if (rpmi >= 8500 && rpmi < 9000) RPM9000.enabled = true;
            if (rpmi >= 9000 && rpmi < 9500) RPM9500.enabled = true;
            if (rpmi >= 9500 && rpmi < 10000) RPM10000.enabled = true;
            if (rpmi >= 10000 && rpmi < 10500) RPM10500.enabled = true;
            if (rpmi >= 10500 && rpmi < 11000) RPM11000.enabled = true;
            if (rpmi >= 11000 && rpmi < 11500) RPM11500.enabled = true;
            if (rpmi >= 11500 && rpmi < 12000) RPM12000.enabled = true;
            if (rpmi >= 12000 && rpmi < 12500) RPM12500.enabled = true;
            if (rpmi >= 12500 && rpmi < 13000) RPM13000.enabled = true;
            if (rpmi >= 13000 && rpmi < 13500) RPM13500.enabled = true;
            if (rpmi >= 13500 && rpmi < 14000) RPM14000.enabled = true;
            if (rpmi >= 14000 && rpmi < 14500) RPM14500.enabled = true;
            if (rpmi >= 14500 && rpmi < 15000) RPM15000.enabled = true;
        }
        //      newImage = Image.FromFile(@"C:/User Data/VS/Software/Bass/Combined-Project/Blank-Application/Images/" + rpmString + ".png");
    }

    private IEnumerator RPMIdleSettle()
    {
        yield return new WaitForSeconds(0.5f);

        while (iRPMIdleSettle == 1)
        {
            yield return new WaitForSeconds(0.04f);

            ineedle = ineedle + 1;
            rpmi = rpmidlesettleed[ineedle];
            //        Debug.Log("RPMi=");
            //        Debug.Log(rpmi);
            RemoveRPMConsole();
            if (rpmi == 0) RPM0.enabled = true;
            if (rpmi > 0 && rpmi < 300) RPM200.enabled = true;
            if (rpmi >= 300 && rpmi < 600) RPM500.enabled = true;
            if (rpmi >= 600 && rpmi < 900) RPM800.enabled = true;
            if (rpmi >= 900 && rpmi < 1100) RPM1000.enabled = true;
            if (rpmi >= 1100 && rpmi < 1600) RPM1500.enabled = true;
            if (rpmi >= 1600 && rpmi < 1900) RPM1800.enabled = true;
            if (rpmi >= 1900 && rpmi < 2100) RPM2000.enabled = true;
            if (rpmi >= 2100 && rpmi < 2300) RPM2200.enabled = true;
            if (rpmi >= 2300 && rpmi < 2600) RPM2500.enabled = true;
            if (rpmi >= 2600 && rpmi < 2900) RPM2800.enabled = true;
            if (rpmi >= 2900 && rpmi < 3100) RPM3000.enabled = true;
            if (rpmi >= 3100 && rpmi < 3300) RPM3200.enabled = true;
            if (ineedle >= 22) iRPMIdleSettle = 0;
        }

        //      RemoveRPMConsole();
    }

    // Console RPM Remove
    public void RemoveRPMConsole()
    {
        RPM0.enabled = false;
        RPM200.enabled = false;
        RPM500.enabled = false;
        RPM800.enabled = false;
        RPM1000.enabled = false;
        RPM1100.enabled = false;
        RPM1500.enabled = false;
        RPM1800.enabled = false;
        RPM2000.enabled = false;
        //      RPM2000.enabled = false;
        RPM2200.enabled = false;
        RPM2500.enabled = false;
        RPM2800.enabled = false;
        RPM3000.enabled = false;
        RPM3200.enabled = false;
        RPM3500.enabled = false;
        RPM3800.enabled = false;
        RPM4000.enabled = false;
        RPM4200.enabled = false;
        RPM4500.enabled = false;
        RPM4800.enabled = false;
        RPM5000.enabled = false;
        RPM5200.enabled = false;
        RPM5500.enabled = false;
        RPM6000.enabled = false;
        RPM6500.enabled = false;
        RPM7000.enabled = false;
        RPM7500.enabled = false;

        RPM8000.enabled = false;
        RPM8500.enabled = false;
        RPM9000.enabled = false;
        RPM9500.enabled = false;
        RPM10000.enabled = false;
        RPM10500.enabled = false;
        RPM11000.enabled = false;
        RPM11500.enabled = false;
        RPM12000.enabled = false;
        RPM12500.enabled = false;
        RPM13000.enabled = false;
        RPM13500.enabled = false;
        RPM14000.enabled = false;
        RPM14500.enabled = false;
        RPM15000.enabled = false;

    }
    // Exhaust Pops and Light
    public void Explode()
    {
        if (ivehicle == 1)
        {
            AVExhaustPop.ExhaustPop();
            AVExhaustPop1.ExhaustPop();
            AVExhaustPop2.ExhaustPop();
            AVExhaustPop3.ExhaustPop();
            LightExhaustPop.LightOnExhaust();
            StartCoroutine("LightOffExhaust");
        }
        else if(ivehicle == 2)
        {
            Debug.Log("Exhaust Popping");
            AVExhaustPopAventadorL.ExhaustPop();
            AVExhaustPopAventadorR.ExhaustPop();
        }
    }


    IEnumerator LightOffExhaust()
    {

        //          Debug.Log("Enter FadeoutSLR1***********************************2");
        yield return new WaitForSeconds(0.1f); // wait half a second
                                               //           ANFSR.NFSRFadeout();
        LightExhaustPop.LightOff2Exhaust();
    }


}

