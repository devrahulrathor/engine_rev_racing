using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingSTriple : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineSTriple.irpm == 0)
        {
            AudioEngineSTriple.rpm1 = AudioEngineSTriple.rpmidling;
        }
    }

    public void Idling()
    {
        //      Debug.Log("Inside Idling************************************************");
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngineSTriple.idlingvol;
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       Idlingvolume = 0;


    }

    double Idlingvolume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

            AudioEngineSTriple.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        Idlingvolume = Idlingvolume + 0.1;
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
        else
        {
            audio.volume = 1;
            AudioEngineSTriple.ifadeinidling = 0;

            //            audio.Stop();
        }

    }
    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}
