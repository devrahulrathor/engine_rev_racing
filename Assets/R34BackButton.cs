using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class R34BackButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void BackButton()
    {
        if (AudioEngineR34.itrate != -1) AudioEngineR34.istartfromtouch = 3;
        SceneManager.LoadScene(0); // Car Options1 Main Screen
    }
}
