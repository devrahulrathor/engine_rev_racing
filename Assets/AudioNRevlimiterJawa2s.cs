﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevlimiterJawa2s : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineJawa2s.itrate == 10000)
        {
            //      RPMCal();
            // AudioEngineJawa2s.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineJawa2s.ivibrating == 1)
        {

            //          AudioEngineJawa2s.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (AudioEngineJawa2s.ivibrator == 1)
            //       {
            if (AudioEngineJawa2s.rpm1 == 6000) AudioEngineJawa2s.rpm1 = 6500;
            else AudioEngineJawa2s.rpm1 = 6000;
            Debug.Log("RPM =");
            Debug.Log(AudioEngineJawa2s.rpm1);
            //          else if (AudioEngineJawa2s.rpm1 == 14000) AudioEngineJawa2s.rpm1 = 15000;
            //          }
        }

    }


    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineJawa2s.ReverbXoneMix;
    }

}

