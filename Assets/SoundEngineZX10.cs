using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using UnityEngine.SceneManagement;

public class SoundEngineZX10 : MonoBehaviour
{
    public MainControllerZX10 MCZX10;
    double startTick;

 //   public int ikeyin = 0;    // Ignition Key in Counter
//    public static int ikeyinfromtouch = 0;
 //   public int ikeyoffsound = 0; // Counter for activating key-off sound
    public static int istart = -100;    // Start Counter
 //   public static int istartfromtouch = 0;
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs

 //   public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 

 //   public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = -1;  // To let RPM be 0 before idling(Clash with AudioIdling Module where irpm =0 activates it
    public static int iblipcanstart = 1;  // Counter for Blip Start
    public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 0;

 //   int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 1100;
    public float rpmdisplayafterstart = 1.4f;  // Time in seconds after which Idling RPM start post Startup
    public static int rpmblipconst;  // TO Stop Blip at different RPMs
    public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public static float sliderval;
    public static float slidervalold;

    // Fade in out counters
    public static int ifadeoutnfsr = 0, ifadeinnfsr = 0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)

    public static int ifadeinidling = 0, ifadeoutidling = 0;
    public static int ifadeoutsli = 0, ifadeoutfsi1 = 0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters

    public static float ReverbXoneMix = 1.03f;   // Reverberation Value for Garage


    // Akrapovic Counters
//    public int exhaustDropDown = 0; // 0 Dropdown up, 1 Dropdown down
 //   public int MainControllerZX10.iexhaust = 0;  // 0 for Stock, 1 for Akra1,  
                              //    public Button Exhaustbutton;  // Exhaust Option Button
 





    // Backfire Section
    public ExhaustPopZX10R AVExhaustPop;
    public int iPopCrackle = 0;  // 0-off, 1-On
    public static float dtpop = 0.75f;  // Delay on Backfire 
 //   public GameObject PopsCracklesHeader;  // Main Menu for Pops Crackles
 //   public GameObject PopsCrackles1;       // Pos Crackles1 Button
 //   public GameObject PopsCrackles1Tick;       // Pos Crackles1 Button Tick
                                               // Paid Options
    public AudioScriptCrackle10000 ACrackle10000;  // Crackle 10000
    public AudioScriptCrackle8000 ACrackle8000;  // Crackle 10000

    //Timer Variables
    float StartTimer = 0;
    int istarttimer = 0;  // 1 at Start and 0 at Stop
    float startertime = 0;  // Time during which Starter operates -Less during warmedup engine
    float warmuptime = 20; // Seconds to warm-up

    public Camera Camera;


    public AudioStartupZX10 AStartup;
    public AudioStartdown AStartdown;
    public AudioStartingZX10 AStarting;
    public AudioShutting AShutting;

    public AudioIdlingZX10 AIdling;

    public AudioNBLIPZX10 ANBLIP;

    public AudioNBLIP2000ZX10 ANBLIP2000;
    public AudioNBLIP3000ZX10 ANBLIP3000;
    public AudioNBLIP4000ZX10 ANBLIP4000;
    public AudioNBLIP5000ZX10 ANBLIP5000;
    public AudioNBLIP6000ZX10 ANBLIP6000;
    public AudioNBLIP7000ZX10 ANBLIP7000;
    public AudioNBLIP10000ZX10 ANBLIP10000;
    public AudioNBLIP14000ZX10 ANBLIP14000;

    public AudioNBLIPC3000ZX10 ANBLIPC3000;
    public AudioNBLIPC4000ZX10 ANBLIPC4000;
    public AudioNBLIPC5500ZX10 ANBLIPC5500;
    public AudioNBLIPC6000ZX10 ANBLIPC6000;
    public AudioNBLIPC7500ZX10 ANBLIPC7500;
    public AudioNBLIPC10000ZX10 ANBLIPC10000;
    public AudioNBLIPC14000ZX10 ANBLIPC14000;
    public AudioNRevLimiterZX10 ANRevLimiter;
    public AudioNFSR15000ZX10 ANFSR15000;
    public AudioNFSR7500ZX10 ANFSR7500;
    public AudioNFSRZX10 ANFSR;
    public AudioNSLIZX10 ANSLI;
    public AudioNFSI1ZX10 ANFSI1;



    //  Akra1ZX10
    public AudioIdlingAkra1ZX10 AIdlingAkra1;
    public AudioNSLIAkra1ZX10 ANSLIAkra1;
    public AudioNFSI1Akra1ZX10 ANFSI1Akra1;
    public AudioNBLIPAkra1ZX10 ANBLIPAkra1;
    public AudioNBLIP2000Akra1ZX10 ANBLIP2000Akra1;
   public AudioNBLIP3000Akra1ZX10 ANBLIP3000Akra1;
    public AudioNBLIP4000Akra1ZX10 ANBLIP4000Akra1;
    public AudioNBLIP5000Akra1ZX10 ANBLIP5000Akra1;
    public AudioNBLIP6000Akra1ZX10 ANBLIP6000Akra1;
    public AudioNBLIP7000Akra1ZX10 ANBLIP7000Akra1;
    public AudioNBLIP10000Akra1ZX10 ANBLIP10000Akra1;
    public AudioNBLIP14000Akra1ZX10 ANBLIP14000Akra1;
    //   public AudioNRevLimiterAkra1 ANRevLimiterAkra1;
    public AudioNBLIPC3000Akra1ZX10 ANBLIPC3000Akra1;
    public AudioNBLIPC4000Akra1ZX10 ANBLIPC4000Akra1;
    public AudioNBLIPC5500Akra1ZX10 ANBLIPC5500Akra1;
    public AudioNBLIPC6000Akra1ZX10 ANBLIPC6000Akra1;
    public AudioNBLIPC7500Akra1ZX10 ANBLIPC7500Akra1;
    public AudioNBLIPC10000Akra1ZX10 ANBLIPC10000Akra1;
    public AudioNBLIPC14000Akra1ZX10 ANBLIPC14000Akra1;
    public AudioNRevLimiterAkra1ZX10 ANRevLimiterAkra1;
    public AudioNFSR15000Akra1ZX10 ANFSR15000Akra1;
    public AudioNFSRAkra1ZX10 ANFSRAkra1;


    // GEAR SECTION________________________________________________________________________________________________________________
    //   Gear1
    public static float bikespeed = 0;  // Bike SPEED
    public Text SpeedText;
    public Text GearText;


    public AudioNSLIGear1 ANSLIGear1;
    public AudioNFSI1Gear1 ANFSI1Gear1;

    public AudioNSLIGear2 ANSLIGear2;
    public AudioNFSI1Gear2 ANFSI1Gear2;

    public AudioNSLIGear3 ANSLIGear3;
    public AudioNFSI1Gear3 ANFSI1Gear3;

    public AudioNSLIGear4 ANSLIGear4;
    public AudioNFSI1Gear4 ANFSI1Gear4;

    public AudioNSLIGear5 ANSLIGear5;
    public AudioNFSI1Gear5 ANFSI1Gear5;

    public AudioNFSRGear1 ANFSRGear;
    public AudioNRevLimiterGear ANRevLimiterGear;

    //Launch Control

    public AudioLaunchC8000 ANLaunch8000;
    public AudioLaunchCConst8000 ANLaunchC8000;
    public AudioLaunchCReduce ANLaunchReduce;

    int revslaunch;   // Revs upto which engine revs
    int revslaunchdrop; // Revs to which rpm drops at SLI
                        //    public int GameMode = 0;  // 0- Race/Ride Mode 1- Drag Mode
    public int iclutchrelease = 0;
    int ilaunchCengaged = 0;  // 1 for 8000 RPM hold, 2 for 4000 RPM hold

    //Store Section
//    public static int ZX10ExhaustAkra1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs
    public static int ZX10ExhaustTBR1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval2 in Playerprefs

    // Bike Position
    public ZX10Rscript Bike;

    // Headlight Object
    public HeadLight Hlight;
    public RearLight Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 


    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No

    //RateUs Variables
    public int irevcountrateus = 0;  // Counter to decide when user rates when the count is greated than irevcountmaxrateus
    public int irevcountmaxrateus = 50;
 //   int irateusloopdisable = 0;
 //   int irateusdialogdisable = 0;

 
    void Awake()
    {
        Application.targetFrameRate = 1000;

    }
    // Use this for initialization
    void Start()
    {
        //       Bike.originalBikePosition = Bike.transform.position;// Get Original Position of Bike to be used to set to silencer view i
        //       Debug.Log(" Entered Bike Pos");


        // IAP Give the PlayerPrefs some values to send over to the next Scene
        //          PlayerPrefs.SetInt("nonconsumableval1", 0);
        //       Purchaser.OnInitialized();
   //     BinarySaveLoad.SaveZx10Akra1(2);
        //          PlayerPrefs.SetInt("nonconsumableval2", 0);

        //         ZX10ExhaustAkra1 =PlayerPrefs.GetInt("nonconsumableval1");
        //     BinarySaveLoad.LoadZX10Akra1();  // Binary Load Cnsumable Data
        //     ZX10ExhaustTBR1 = PlayerPrefs.GetInt("nonconsumableval2");


    }


    // Update is called once per frame
    void Update()
    {
         dtpop = Random.Range(0.45f, 0.8f);  // RAndom Variation of Exhhaust Pop delay 
        //       slidervalold = sliderval;
        //      Debug.log(sliderval);

        //      trate = (sliderval - slidervalold);
        //        throttlediff = (sliderval * 150 - rpm1);
        // Starting Module
        //      if (Input.GetKeyDown("space") || istart == 0)
        //           if(ZX10ExhaustAkra1 == 0) ZX10ExhaustAkra1 = PlayerPrefs.GetInt("nonconsumableval1");











        // Start Module


        // Shuttin Module


        // Throttle Spring back Module

      
        trate = (MainControllerZX10.sliderval - slidervalold);
     
        throttlediff = (MainControllerZX10.sliderval * 150 - MainControllerZX10.rpm1);
   //     Debug.Log("RPM=");
   //     Debug.Log(rpm1);

        // Throttle  Events
        if (istart == 1 && gear == 0 && ilaunchCengaged == 0)
        {
            //       trate = (sliderval - slidervalold);
            //       throttlediff = (sliderval * 150 - rpm1);
    //        Debug.Log("inside istart1");
            // SLI Module
            //               if(((trate > 0 && throttlediff > 0) && itrate == 1) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 ||itrate == 6006 || itrate == 6005  || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0))  {
            if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
            {
                           Debug.Log("Inside SLI______________________________________");
                // Select NSLI Sound Stock/Aftermarket
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANSLI.NSLI();
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                if (MainControllerZX10.iexhaust == 1)
                {
                    ANSLIAkra1.NSLI();
                    //________________________________________________
                    //         ANSLI.NSLI();
                    ANFSRAkra1.FSRStop();
                    //         ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000Akra1.BLIPC14000Stop();
                    ANBLIPC10000Akra1.BLIPC10000Stop();
                    ANBLIPC7500Akra1.BLIPC7500Stop();
                    ANBLIPC6000Akra1.BLIPC6000Stop();
                    ANBLIPC5500Akra1.BLIPC5500Stop();
                    ANBLIPC4000Akra1.BLIPC4000Stop();
                    ANBLIPC3000Akra1.BLIPC3000Stop();
                }
                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");
                if (itrate == 5001)
                {
                    //           ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
                    //           StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 2;    // Counter for FSI to start
                irpm = 1;      // Counter for RPM 
           //     MainControllerZX10.irpm1 = 1;
            }

            // FSI1 Module
            //            if (((trate > 2 || throttlediff > 2000) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))

            //               if (((trate > 2 || throttlediff > 2000 || rpm1 > 4500) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))
            if (((trate > 2 || throttlediff > 2000 || rpm1 > 15500) && itrate == 2 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
            // above FSI1 starts after rpm > 45000//

            {
                //         Debug.Log("Entering FSI1_________________________________________");
                // Select NSLI Sound Stock/Aftermarket
                if (MainControllerZX10.iexhaust == 0)
                {
                    //                Debug.Log("Entering FSI1 ex=0________________________________________");
                    ANFSI1.NFSI1();
                    ANSLI.NSLIStop();
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                else if (MainControllerZX10.iexhaust == 1)
                {
                    //          ANFSI1Akra1.NFSI1();
                    //          ANSLIAkra1.NSLIStop();
                    //          ANFSRAkra1.FSRStop();
                    //          ANFSR15000Akra1.NFSR15000Stop();
                    //          ANBLIPC14000Akra1.BLIPC14000Stop();
                    //          ANBLIPC10000Akra1.BLIPC10000Stop();
                    //          ANBLIPC7500Akra1.BLIPC7500Stop();
                    //          ANBLIPC6000Akra1.BLIPC6000Stop();
                    //          ANBLIPC5500Akra1.BLIPC5500Stop();
                    //          ANBLIPC4000Akra1.BLIPC4000Stop();
                    //          ANBLIPC3000Akra1.BLIPC3000Stop();
                }

                if (MainControllerZX10.iexhaust == 0)
                {


                    ifadeoutidling = 1;
                    fadeoutidlingtime = 0.01f;
                    StartCoroutine("FadeoutIdling");
                    if (itrate == 5001)
                    {
                        //        ifadeoutnfsr = 1;
                        //             StartCoroutine("FadeoutNFSR");
                        //        StartCoroutine("FadeoutBLIPCR7500");
                    }
                    //       AIdling.IdlingStop(); // Stop Idling

                    itrate = 3;    // Counter for FSI to start
                    irpm = 2;      // Counter for RPM 

                }


            }



            // Blip Module
            if ((trate > 10 || throttlediff > 12000) && throttlediff > 0 && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
            {
                //           Debug.Log("Trate=");
                //           Debug.Log(trate);

             //           Debug.Log("Inside Blip-------------------");
         //       Debug.Log("rpm1=");
         //       Debug.Log(rpm1);
                startTick = AudioSettings.dspTime;

                iblipcanstart = 0;
                irevlimitercanstart = 1;

                // Select NBLIP Sound Stock/Aftermarket
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANBLIP.Blip();
                    ANBLIP2000.Blip2000();
                    ANBLIP3000.Blip3000();
                    ANBLIP4000.Blip4000();
                    ANBLIP5000.Blip5000();
                    ANBLIP6000.Blip6000();
                    ANBLIP7000.Blip7000();
                    ANBLIP10000.Blip10000();
                    ANBLIP14000.Blip14000();
                    AIdling.IdlingStop(); // Stop Idling

                    //         ifadeoutidling = 1;
                    //         fadeoutidlingtime = 0.01f;
                    //         StartCoroutine("FadeoutIdling");
                    ANSLI.NSLIStop();
                    ANFSI1.NFSI1Stop();
                    //        ifadeoutsli = 1;
                    //        ifadeoutfsi1 = 1;
                    //         StartCoroutine("FadeoutNSLI");
                    //         StartCoroutine("FadeoutNFSI1");
                    //        ifadeoutnfsr = 1;
                    //        fadeoutfsrtime = 0.005f;
                    //          FadeoutNFSR();



                    //           ANFSR.FSRStop();

                    //         ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                else if (MainControllerZX10.iexhaust == 1)
                {
                    //               Debug.Log("Inside Blip MainControllerZX10.iexhaust 1");
                    ANBLIPAkra1.Blip();
                    ANBLIP2000Akra1.Blip2000();
                    ANBLIP3000Akra1.Blip3000();
                    ANBLIP4000Akra1.Blip4000();
                    ANBLIP5000Akra1.Blip5000();
                    ANBLIP6000Akra1.Blip6000();
                    ANBLIP7000Akra1.Blip7000();
                    ANBLIP10000Akra1.Blip10000();
                    ANBLIP14000Akra1.Blip14000();
                    AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
                    ANSLIAkra1.NSLIStop();
                    ANFSI1Akra1.NFSI1Stop();
                    //        ifadeoutsli = 1;
                    //        ifadeoutfsi1 = 1;
                    //         StartCoroutine("FadeoutNSLI");
                    //         StartCoroutine("FadeoutNFSI1");
                    //        ifadeoutnfsr = 1;
                    //        fadeoutfsrtime = 0.005f;
                    //          FadeoutNFSR();



                    //           ANFSR.FSRStop();

                    //         ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000Akra1.BLIPC14000Stop();
                    ANBLIPC10000Akra1.BLIPC10000Stop();
                    ANBLIPC7500Akra1.BLIPC7500Stop();
                    ANBLIPC6000Akra1.BLIPC6000Stop();
                    ANBLIPC5500Akra1.BLIPC5500Stop();
                    ANBLIPC4000Akra1.BLIPC4000Stop();
                    ANBLIPC3000Akra1.BLIPC3000Stop();
                }
                //________________________________________________

                //          ANBLIP.Blip();
                //                 ANBLIP2000.Blip2000();
                //         ANBLIP3000.Blip3000();
                //         ANBLIP4000.Blip4000();
                //         ANBLIP5000.Blip5000();
                //         ANBLIP6000.Blip6000();
                //         ANBLIP7000.Blip7000();
                //         ANBLIP10000.Blip10000();
                //         ANBLIP14000.Blip14000();
                //         ANFSR15000.NFSR15000();

                //           ANBLIPC3000.BLIPC3000();

                idlingvol = 0;   // Making Idling vol 0 so no silence upon start and idling


                //            NSLIAtrigger.SLIStop();

                //               Debug.Log("Blip ***************************************Blip");
                // Stop SLI
                // Stoo SLR
                itrate = 5000;  // Blip itrate
                                //         irpm = 5000;
                                //           rpmdisplay = rpm1;
            }

            if (((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 2 || itrate == 3)) && rpm1 >= 14000)
            //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
            {
                Debug.Log("Inside RevLimiter Loop---------------------------------------------");
                itrate = 10000;  // pre rev limit Trate
                                 //          EnableFSR15000afterRevlimiterStart();
                irevlimitercanstart = 0;
                ivibrating = 1;
                rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANRevLimiter.RevLimiter();
                    ANBLIP14000.Blip14000Stop();
                    ANBLIP.BlipStop();
                }
                else if (MainControllerZX10.iexhaust == 1)
                {
                    ANRevLimiterAkra1.RevLimiter();
                    ANBLIP14000Akra1.Blip14000Stop();
                    ANBLIPAkra1.BlipStop();
                    ANFSI1Akra1.NFSI1Stop();  // Test
                    ANSLIAkra1.NSLIStop();  // Test
                }

                // Mobile Vibration
                //            ivibrating = 1;
                //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                //          Handheld.Vibrate();
                StartCoroutine("RevLimiterVibrationOnOff");

            }

            if (((trate < 0 || throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            //           if (((trate < 0 && throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            //    Defunct          if (((trate < 0 || throttlediff < 0) && itrate == 5000 && rpm1 > 2000) || (itrate == 5000 && rpm1 >= 9000))
            {
                itrate = 5001;  // Counter for BlipC
                                //     itrate = -1;
                                //         iblipcanstop = 1;
                                //     }
                                //     if(iblipcanstop == 1) {

                startTick = AudioSettings.dspTime - startTick;
                //              Debug.Log("Delta Time=");
                //              Debug.Log(startTick);


                //                  InvokeRepeating("FadeoutBLIP", 0f, 0.01f);
                //                 StartCoroutine("Cancelinvoke"); 
                //             ANBLIP.BLIPFadeout();
                //             ifadeoutblip = 1;
                //             StartCoroutine("FadeoutBLIP");
                //          ANBLIP.BlipStop();

                //             if ((rpm1 > 2500 && rpm1 <= 3300) || (rpm1 > 3800 && rpm1 <= 4300) || (rpm1 > 5000 && rpm1 <= 5500) || (rpm1 > 5800 && rpm1 <= 6300) || (rpm1 > 6800 && rpm1 <= 7500))
                //             {

                //          Debug.Log("Inside trate < 0, rpm=");
                //             Debug.Log(rpm1);

                if (rpm1 > 10000 && rpm1 <= 15000)
                {
                    itrate = 6100;
                    ivibrating = 0; // Stop Vibrating after Rev Limter Stops
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANRevLimiter.RevLimiterStop();


                        ANFSR15000.NFSR15000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        if (iPopCrackle == 1)
                        {
                            Debug.Log("Inside Blipc15000------------------------");
                            ACrackle10000.Crackle10000();
                            StartCoroutine("Explode");
                        }


                    }

                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        //                      Debug.Log("Inside Blipc15000------------------------");
                        ANRevLimiterAkra1.RevLimiterStop();
                        ANFSR15000Akra1.NFSR15000();
                        ANBLIPAkra1.BlipStop();
                        //        ANBLIP14000.Blip14000Stop();
                        ANFSI1Akra1.NFSI1Stop();  // Test

                        if (iPopCrackle == 1)
                        {
                            ACrackle10000.Crackle10000();
                            StartCoroutine("Explode");
                        }
                    }
                }
                if (rpm1 > 6800 && rpm1 <= 10000)
                //             if (rpm1 > 7500 && rpm1 <= 10000)
                {
                    itrate = 6008;
                    //            iblipcanstop = 0;

                    if (MainControllerZX10.iexhaust == 0)
                    {

                        ANBLIPC14000.BLIPC14000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        if (iPopCrackle == 1)
                        {
                            Debug.Log("Inside Blipc10000------------------------");
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }
                    }

                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        Debug.Log("Inside Blipc14000");
                        ANBLIPC14000Akra1.BLIPC14000();
                        ANBLIPAkra1.BlipStop();
                        ANBLIP14000Akra1.Blip14000Stop();

                        if (iPopCrackle == 1)
                        {
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }
                    }

                    //       iblipcanstop = 0;
                }



                if (rpm1 > 6200 && rpm1 <= 6800)
                //              if (rpm1 > 6200 && rpm1 <= 7500)
                {
                    itrate = 6007;
                    //            iblipcanstop = 0;

                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANBLIPC10000.BLIPC10000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        if (iPopCrackle == 1)
                        {
                            Debug.Log("Inside Blipc10000------------------------");
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }
                    }

                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        //                     Debug.Log("Inside Blipc10000");
                        ANBLIPC10000Akra1.BLIPC10000();
                        ANBLIPAkra1.BlipStop();
                        ANBLIP14000Akra1.Blip14000Stop();
                        //            ANBLIP10000Akra1.Blip10000Stop();  //Test

                        {
                            Debug.Log("Inside Blipc10000------------------------");
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }

                    }
                    //       iblipcanstop = 0;
                }



                if (rpm1 > 5600 && rpm1 <= 6200)
                {
                    itrate = 6006;

                    rpmblipconst = rpm1;
                    blipcinterval = 5.347f;

                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANBLIPC7500.BLIPC7500();
                        //          iblipcanstop = 0;
                        ANBLIP.BlipStop();
                        //                ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }

                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        //                    Debug.Log("Inside Blipc7500");
                        ANBLIPC7500Akra1.BLIPC7500();
                        //          iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();
                        ANBLIP7000Akra1.Blip7000Stop();  //Test
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();
                    }
                    //       iblipcanstop = 0;
                }

                if (rpm1 > 5000 && rpm1 <= 5600)
                {
                    itrate = 6005;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANBLIPC6000.BLIPC6000();
                        //         iblipcanstop = 0;
                        ANBLIP.BlipStop();
                        //                ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }

                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        //                    Debug.Log("Inside Blipc6000");
                        ANBLIPC6000Akra1.BLIPC6000();
                        //         iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();
                        ANBLIP6000Akra1.Blip6000Stop();   //Test
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();
                    }

                    //       iblipcanstop = 0;
                }
                if (rpm1 > 4100 && rpm1 <= 5000)

                {

                    //            if ((startTick > 0.214 && startTick < 0.215) || (startTick > 0.221 && startTick < 0.222) || (startTick > 0.227 && startTick < 0.229) || (startTick > 0.234 && startTick < 0.236) || (startTick > 0.24 && startTick < 0.243) || (startTick > 0.247 && startTick < 0.249) || (startTick > 0.252 && startTick < 0.254)) iswitch = 0;
                    //            if (iswitch >= 0)
                    //            {
                    iswitch = 1;
                    itrate = 6004;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.345f;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANBLIPC5500.BLIPC5500();
                        //            iblipcanstop = 0;
                        ANBLIP.BlipStop();

                        //            ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }
                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        //                    Debug.Log("Inside Blipc5000");
                        ANBLIPC5500Akra1.BLIPC5500();
                        //            iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();

                        ANBLIP5000Akra1.Blip5000Stop();  //Test
                        ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();
                    }
                    //       iblipcanstop = 0;
                    //          }
                }
                //                         if (rpm1 > 3200 && rpm1 <= 4000 && ((startTick < 0.171 || startTick > 0.175) || (startTick < 0.161 || startTick > 0.166) || (startTick < 0.171 || startTick > 0.175)))
                if (rpm1 > 3000 && rpm1 <= 4100)
                {

                    //        if ((startTick >= 0.169 && startTick <= 0.17) || (startTick >= 0.178 && startTick <= 0.181) || (startTick >= 0.187 && startTick < 0.189) || (startTick >= 0.196 && startTick <= 0.197) || (startTick >= 0.203 && startTick <= 0.207) || (startTick >= 0.211 && startTick <= 0.215) || (startTick >= 0.217 && startTick <= 0.224)) iswitch = 0;
                    //                  if ((startTick > 0.171 && startTick < 0.175) || (startTick > 0.18 && startTick < 0.185) || (startTick > 0.19 && startTick < 0.192) || (startTick > 0.197 && startTick < 0.2) || (startTick > 0.205 && startTick < 0.209) || (startTick > 0.213 && startTick < 0.215) || (startTick > 0.22 && startTick < 0.223)) iswitch = 1;
                    //       if (iswitch >= 0)
                    //       {
                    iswitch = 1;
                    itrate = 6003;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.53f;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANBLIPC4000.BLIPC4000();

                        //           iblipcanstop = 0;
                        ANBLIP.BlipStop();


                        //               ANBLIP4000.Blip4000Stop();
                        //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                        if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }

                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        //                     Debug.Log("Inside Blipc4000");
                        ANBLIPC4000Akra1.BLIPC4000();

                        //           iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();


                        //               ANBLIP4000.Blip4000Stop();
                        //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                        if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                        ANBLIP4000Akra1.Blip4000Stop();
                        ANBLIP5000Akra1.Blip5000Stop();
                        ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();


                    }

                    //       iblipcanstop = 0;
                    //       }
                }
                //        if (rpm1 > 320 && rpm1 <= 3200 && startTick > 0.0f )
                if (rpm1 > 2000 && rpm1 <= 3000)
                //          if (startTick >= 0.178)
                {
                    //          if ((startTick >= 0.103 && startTick <= 0.107) || (startTick >= 0.111 && startTick <= 0.114) || (startTick >= 0.125 && startTick <= 0.129) || (startTick >= 0.137 && startTick <= 0.142) || (startTick >= 0.148 && startTick <= 0.152)) iswitch = 0;
                    //             if ((startTick > 0.121 && startTick < 0.122) || (startTick > 0.128 && startTick < 0.131) || (startTick > 0.133 && startTick < 0.135) || (startTick > 0.140 && startTick < 0.143) || (startTick > 0.152 && startTick < 0.156) || (startTick > 0.161 && startTick < 0.166)) iswitch = 1;
                    //         if (iswitch == 0) {
                    iswitch = 1;
                    itrate = 6002;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANBLIPC3000.BLIPC3000();

                        //      iblipcanstop = 0;
                        ANBLIP.BlipStop();


                        //                 ANBLIP3000.Blip3000Stop();
                        if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000.Blip4000Stop();
                        ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }
                    else if (MainControllerZX10.iexhaust == 1)
                    {
                        Debug.Log("Inside Blipc3000");
                        ANBLIPC3000Akra1.BLIPC3000();

                        //      iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();


                        //                 ANBLIP3000.Blip3000Stop();
                        if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000Akra1.Blip4000Stop();
                        ANBLIP4000Akra1.Blip4000Stop();
                        ANBLIP5000Akra1.Blip5000Stop();
                        ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();

                    }

                    //      }

                    //       iblipcanstop = 0;
                }




                StartCoroutine("EnableBlip");  // Enable blip after 200 ms of Blip 
                                               //         StartCoroutine("StartFSR");     // Enable FSR after Blipc ends
                                               //          StartCoroutine("StartFSR7500");     // Enable FSR after Blipc ends
                                               //          }
            }

            // FSR after SLI module
            if ((trate < -0.5 && throttlediff < 0) && (itrate == 2 || itrate == 3 || itrate == 10000) && rpm1 > 2000)
            {
                blipcinterval = 0;
                StartCoroutine("StartFSR");     // Enable FSR after Blipc ends

            }
            // Restart Idling
            if (rpm1 <= (rpmidling + 200) && (itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999))
            {
                //          Debug.Log("Pre Enter FadeoutSLR1 ***************************************8");

                //            iingearrestartidling = 0;


                itrate = 1;
                irpm = 0;
                if (MainControllerZX10.iexhaust == 0) AIdling.Idling();
                else if (MainControllerZX10.iexhaust == 1) AIdlingAkra1.IdlingAkra1();
                ifadeinidling = 1;
                StartCoroutine("FadeinIdling");
                //      StartCoroutine("StartIdling");
                //          ANFSR.FSRStop();
                rpm1 = rpmidling;
                //                ifadeoutnfsr = 1;
                //             fadeoutfsrtime = 0.01f;
                //         StartCoroutine("FadeoutNFSR");
                ANFSR.FSRStop();
                ANFSRAkra1.FSRStop();
                //          ANFSR15000.NFSR15000Stop();
                //          ANBLIPC14000.BLIPC14000Stop();
                //          ANBLIPC7500.BLIPC7500Stop();
                //          ANBLIPC6000.BLIPC6000Stop();
                //          ANBLIPC5500.BLIPC5500Stop();
                //          ANBLIPC4000.BLIPC4000Stop();
                //         ANBLIPC3000.BLIPC3000Stop();
                //     StartCoroutine("Setitrate1");
           
               
       //         idlingvol = 1;
       //         irpm = 0;
       //         MainControllerZX10.rpm1 = rpmidling;
       //         rpm1 = rpmidling;
             
            }

        }   // Neutral Section Over


        // Ingear Section for Racing *********************************************************************

        //        if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 12000))

        //Bikes Speed and Gear Print on Screen
        SpeedText.text = "Speed " + bikespeed;
        GearText.text = "Gear " + gear;


        //    Drag Racing Module
        // Launch Control/Throttle Hold
        if (ilaunchCengaged == 1 && itrate == 100)
        {
            ANLaunch8000.Launch8000();
            ANLaunchC8000.LaunchConst8000();
            StartCoroutine("LaunchRevRise");
            revslaunch = 8000;
            revslaunchdrop = 4000;

            AIdling.IdlingStop();
            irpm = 1000;
            //      rpm1 = 8000;

            ilaunchCengaged = 2;
        }
        if (ilaunchCengaged == 2 && clutchpos > 20)  // Clutch Release
        {
            Debug.Log("Inside Clutch Release");
            StartCoroutine("LaunchRevDrop");
            ANLaunchReduce.LaunchReduce();
            ANLaunchC8000.LaunchConst8000Stop();
            ilaunchCengaged = 30;  // To stop LaunchRevDrop to loop  
        }

        // SLI Module
        if (gear == 1 && ((trate > 0 && throttlediff > 0 && ilaunchCengaged == 0) || (throttlediff > 0 && ilaunchCengaged == 3 && iclutchrelease == 1)) && ((itrate == 100 && rpm1 < 12000) || (itrate == 49990)))
        {
            Debug.Log("Inside Gear1SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                ilaunchCengaged = 0;
                //       if (ilaunchCengaged == 2) StartCoroutine("LaunchRevDrop");
                ANSLIGear1.NSLI();

                AIdling.IdlingStop();
                ANFSRGear.FSRStop();
                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                ANFSR.FSRStop();
                ANLaunchC8000.LaunchConst8000Stop();
                ANLaunch8000.Launch8000Stop();

                //     ANFSI2.FSI2Stop();
                //     ANFSR15000Gear1.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();
            }
            if (MainControllerZX10.iexhaust == 1)
            {
                ANSLIAkra1.NSLI();
                //________________________________________________
                //         ANSLI.NSLI();
                ANFSRAkra1.FSRStop();
                //         ANFSR15000.NFSR15000Stop();
                ANBLIPC14000Akra1.BLIPC14000Stop();
                ANBLIPC10000Akra1.BLIPC10000Stop();
                ANBLIPC7500Akra1.BLIPC7500Stop();
                ANBLIPC6000Akra1.BLIPC6000Stop();
                ANBLIPC5500Akra1.BLIPC5500Stop();
                ANBLIPC4000Akra1.BLIPC4000Stop();
                ANBLIPC3000Akra1.BLIPC3000Stop();
            }
            ifadeoutidling = 1;
            fadeoutidlingtime = 0.01f;
            //         StartCoroutine("FadeoutIdlingG");

            //       AIdling.IdlingStop(); // Stop Idling

            itrate = 200;    // Counter for Gear1FSI to start
            irpm = 1;      // Counter for RPM 
        }
        // FSI1 Module
        //            if (((trate > 2 || throttlediff > 2000) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))

        //               if (((trate > 2 || throttlediff > 2000 || rpm1 > 4500) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))
        if (gear == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 200 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear1.NFSI1();
                ANSLIGear1.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();

                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                ANFSR.FSRStop();
                //     ANFSI2.FSI2Stop();
                //     ANFSR15000Gear1.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();
            }
            else if (MainControllerZX10.iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (MainControllerZX10.iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 300;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }


        // Gear2 Module____________________________________________________________________ 
        if (trate > 0 && throttlediff > 0 && gear == 2 && (itrate == 200 || itrate == 300 || itrate == 49990))
        {
            Debug.Log("Inside Gear2SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                ANSLIGear2.NSLI();
                ANSLIGear1.NSLIStop();
                ANFSI1Gear1.NFSI1Stop();
                ANFSRGear.FSRStop();
            }
            itrate = 202;   // Gear 2 (200 + 2)
        }
        //Gear2 FSI1 Module
        if (gear == 2 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 202 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 2 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear2.NFSI1();
                ANSLIGear2.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (MainControllerZX10.iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (MainControllerZX10.iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 302;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }

        //Gear 3 Module__________________________________________________________________________________________________

        if (trate > 0 && throttlediff > 0 && gear == 3 && (itrate == 202 || itrate == 302 || itrate == 49990))
        {
            Debug.Log("Inside Gear3SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                ANSLIGear3.NSLI();
                ANSLIGear2.NSLIStop();
                ANFSI1Gear2.NFSI1Stop();
                ANSLIGear1.NSLIStop();
                ANFSI1Gear1.NFSI1Stop();
                ANFSRGear.FSRStop();
            }
            itrate = 203;   // Gear 2 (200 + 2)
        }

        if (gear == 3 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 203 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 3 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear3.NFSI1();
                ANSLIGear3.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (MainControllerZX10.iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (MainControllerZX10.iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 303;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }


        //Gear 4 Module__________________________________________________________________________________________________

        if (trate > 0 && throttlediff > 0 && gear == 4 && (itrate == 203 || itrate == 303 || itrate == 49990))
        {
            Debug.Log("Inside Gear4SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                ANSLIGear4.NSLI();
                ANSLIGear3.NSLIStop();
                ANFSI1Gear3.NFSI1Stop();
                ANSLIGear2.NSLIStop();
                //           ANFSI1Gea21.NFSI1Stop();
                ANFSRGear.FSRStop();
            }
            itrate = 204;   // Gear 2 (200 + 2)
        }

        if (gear == 4 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 204 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 4 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear4.NFSI1();
                ANSLIGear4.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (MainControllerZX10.iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (MainControllerZX10.iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 304;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }


        //Gear 5 Module__________________________________________________________________________________________________

        if (trate > 0 && throttlediff > 0 && gear == 5 && (itrate == 204 || itrate == 304 || itrate == 49990))
        {
            Debug.Log("Inside Gear4SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                ANSLIGear5.NSLI();
                ANSLIGear4.NSLIStop();
                ANFSI1Gear4.NFSI1Stop();
                ANSLIGear3.NSLIStop();
                //           ANFSI1Gea21.NFSI1Stop();
                ANFSRGear.FSRStop();
            }
            itrate = 205;   // Gear 2 (200 + 2)
        }

        if (gear == 5 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 205 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 5 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (MainControllerZX10.iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear5.NFSI1();
                ANSLIGear5.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (MainControllerZX10.iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (MainControllerZX10.iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 305;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }



        // FSR after SLI module
        if ((trate < -0.5 && throttlediff < 0) && (itrate == 200 || itrate == 300 || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305 || itrate == 100000) && rpm1 > 3000)
        {
            Debug.Log("INside FSR Gear");
            blipcinterval = 0;
            StartCoroutine("StartFSRG1");     // Enable FSR after Blipc ends

        }
        // Restart Idling
        if (gear >= 0 && rpm1 <= (rpmidling + 500) && itrate == 49990)
        {
            //          Debug.Log("Pre Enter FadeoutSLR1 ***************************************8");

            //            iingearrestartidling = 0;

            gear = 0;
            itrate = 100;
            irpm = 0;
            if (MainControllerZX10.iexhaust == 0) AIdling.Idling();
            else if (MainControllerZX10.iexhaust == 1) AIdlingAkra1.IdlingAkra1();
            ifadeinidling = 1;
            StartCoroutine("FadeinIdling");
            //      StartCoroutine("StartIdling");
            //          ANFSR.FSRStop();
            rpm1 = rpmidling;
            //                ifadeoutnfsr = 1;
            //             fadeoutfsrtime = 0.01f;
            //         StartCoroutine("FadeoutNFSR");
            ANFSRGear.FSRStop();
            ANFSRAkra1.FSRStop();
            //          ANFSR15000.NFSR15000Stop();
            //          ANBLIPC14000.BLIPC14000Stop();
            //          ANBLIPC7500.BLIPC7500Stop();
            //          ANBLIPC6000.BLIPC6000Stop();
            //          ANBLIPC5500.BLIPC5500Stop();
            //          ANBLIPC4000.BLIPC4000Stop();
            //         ANBLIPC3000.BLIPC3000Stop();
            //     StartCoroutine("Setitrate1");

        }
        //InGear RevLimiter 
        if (gear > 0 && ((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 200 || itrate == 300) || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305) && rpm1 >= 14500)
        //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
        {
            Debug.Log("INside RevLimited Gear1***************************************");
            itrate = 100000;  // pre rev limit Trate
                              //          EnableFSR15000afterRevlimiterStart();
            irevlimitercanstart = 0;
            ivibrating = 1;
            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
            if (MainControllerZX10.iexhaust == 0)
            {
                ANRevLimiterGear.RevLimiter();
                ANSLIGear1.NSLIStop();
                ANFSI1Gear1.NFSI1Stop();
                ANSLIGear2.NSLIStop();
                ANFSI1Gear2.NFSI1Stop();
                ANSLIGear3.NSLIStop();
                ANFSI1Gear3.NFSI1Stop();
                ANSLIGear4.NSLIStop();
                ANFSI1Gear4.NFSI1Stop();
                ANSLIGear5.NSLIStop();
                ANFSI1Gear5.NFSI1Stop();
                //     ANFSI1Gear4.NFSI1Stop();
                //      ANBLIP14000.Blip14000Stop();
                //      ANBLIP.BlipStop();
            }
            else if (MainControllerZX10.iexhaust == 1)
            {
                ANRevLimiterAkra1.RevLimiter();
                ANBLIP14000Akra1.Blip14000Stop();
                ANBLIPAkra1.BlipStop();
                ANFSI1Akra1.NFSI1Stop();  // Test
                ANSLIAkra1.NSLIStop();  // Test
            }

            // Mobile Vibration
            //            ivibrating = 1;
            //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
            //          Handheld.Vibrate();
            StartCoroutine("RevLimiterVibrationOnOff");

        }




        // RPM Console Update 
      



        //*************************************************************************************
        slidervalold = MainControllerZX10.sliderval;  // Assign Slidervalue to Slidrold









    }
    // Coroutines***************************************

    // RPM Console Effects

    // Idling needle Enable Move
   

    IEnumerator Startistart1()
    {
        yield return new WaitForSeconds(0.4f);
        istart = 1;
    }








    IEnumerator EnableBlip()
    {
        yield return new WaitForSeconds(0.2f);
        iblipcanstart = 1;
        //      Debug.Log("Inside Enable Blip");
    }

    // Enable Trate for Revlimiter after 0.1s of Revlimiter Start so Reducing waits for 0.1 s
    private IEnumerator EnableFSR15000afterRevlimiterStart()
    {
        yield return new WaitForSeconds(0.1f);
        itrate = 10000;
    }

    IEnumerator StartFSR()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2 || itrate == 3)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
           //           Debug.Log("Inside FSR");
            ifadeinnfsr = 1;
            if (MainControllerZX10.iexhaust == 0) ANFSR.FSR();
            else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
            StartCoroutine("FadeinNFSR");

            if (iPopCrackle == 1 && rpm1 > 7000)
            {
                Debug.Log("Inside Blipc15000------------------------");
                ACrackle10000.Crackle10000();
                StartCoroutine("Explode");
            }

            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLI");
            StartCoroutine("FadeoutNFSI1");
            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;
            itrate = 4999;
        }


    }

    IEnumerator StartFSR7500()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            itrate = 4999;
            ANFSR7500.FSR7500();
            ANSLI.NSLIStop();
            //       iblipcanstart = 1;

        }


    }


    void FadeoutBLIP()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        //      while (ifadeoutblip == 1)
        //      {
        //          Debug.Log("Enter FadeoutSLR1***********************************2");
        //          yield return new WaitForSeconds(0.001f); // wait half a second

        ANBLIP.BLIPFadeout();
        //     }
    }

    IEnumerator Cancelinvoke()
    {


        yield return new WaitForSeconds(0.1f); // wait half a second
        CancelInvoke();

    }




    IEnumerator FadeinIdling()
    {
        //***************
        while (ifadeinidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (MainControllerZX10.iexhaust == 0) AIdling.IdlingFadein();
            else if (MainControllerZX10.iexhaust == 1)
            {
                //            Debug.Log("INside RestartIdling Akra1_____________________________________");
                AIdlingAkra1.IdlingAkra1Fadein();
            }
        }
    }

    IEnumerator FadeoutIdling()
    {
        //***************
        while (ifadeoutidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutidlingtime); // wait half a second
            if (MainControllerZX10.iexhaust == 0) AIdling.IdlingFadeout();
            else if (MainControllerZX10.iexhaust == 1) AIdlingAkra1.IdlingAkra1Fadeout();
        }
    }

    // Defunct************************
    IEnumerator FadeoutNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutfsrtime); // wait half a second
            ANFSR.NFSRFadeout();
            // Fadeout BlipFSRs
            ANFSR15000.BLIPCR15000Fadeout();
            ANBLIPC14000.BLIPCR14000Fadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
            ANBLIPC6000.BLIPCR6000Fadeout();
            ANBLIPC5500.BLIPCR5000Fadeout();
            ANBLIPC4000.BLIPCR4000Fadeout();
            ANBLIPC3000.BLIPCR3000Fadeout();
        }
    }

    IEnumerator FadeinNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (MainControllerZX10.iexhaust == 0) ANFSR.NFSRFadein();
            else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
    }

    IEnumerator FadeoutNSLI()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (MainControllerZX10.iexhaust == 0) ANSLI.NSLIFadeout();
            else if (MainControllerZX10.iexhaust == 1) ANSLIAkra1.NSLIFadeout();
        }
    }
    IEnumerator FadeoutNFSI1()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            //     ANFSI1.NFSI1Fadeout();
            if (MainControllerZX10.iexhaust == 0) ANFSI1.NFSI1Fadeout();
            else if (MainControllerZX10.iexhaust == 1) ANFSI1Akra1.NFSI1Fadeout();
        }
    }


    // ***************************************************

    IEnumerator FadeoutBLIPCR7500()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
                                                    //           ANFSR.NFSRFadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
        }
    }
    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator RevLimiterVibrationOnOff()
    {
        if (ivibrator == 1) //Handheld.Vibrate();
            while (ivibrating == 1)
            {
                yield return new WaitForSeconds(0.5f);
                if (ivibrator == 1)
                {
                    //          Handheld.Vibrate();
                }
            }

    }

  



  
    public void StartingfromMainController()
    {
        AStarting.Starting();
    }
        //     yield return new WaitForSeconds(startertime); // wait half a secon

        public void StartIdlingfromMainController()
    {
        //     yield return new WaitForSeconds(startertime); // wait half a secon

        
   //     istartfromtouch = 2;

        gear = 0;
       
       

        // Start Timer upon engine start to change idling speed
        //     StartTimer = 0.0f;
        istarttimer = 1; //Start Timer


        AStarting.StartingStop();
        StartCoroutine("Startistart1");  // Starting the Loop for Events
        itrate = 1;  // Enable SLI
        idlingvol = 1;
         irpm = 0;
        MainControllerZX10.rpm1 = rpmidling;
        rpm1 = rpmidling;
        if (MainControllerZX10.iexhaust == 0) AIdling.Idling();
        if (MainControllerZX10.iexhaust == 1) AIdlingAkra1.IdlingAkra1();
        Debug.Log("StartIdling in Sound ENgine");
  //     iblipcanstart = 0;
    }

    public void ShuttingfromMainController()
    {
        AShutting.Shutting();
        itrate = -1;
        istarttimer = 0; // Stop Timer
                         // Enable Console neutral Buttons
                         //       NeutralButtonOff.enabled = true;
                         //       NeutralButtonOn.enabled = false;
       
        istart = 0;
 //       istartfromtouch = 0;

      
        irpm = -1;  // For stopping RPM at Idling


        if (MainControllerZX10.iexhaust == 0)
        {
            AStarting.StartingStop();
            ANBLIP.BlipStop();
            ANSLI.NSLIStop();
            ANFSI1.NFSI1Stop();
            ANBLIP3000.Blip3000Stop();
            ANBLIP4000.Blip4000Stop();
            ANBLIP5000.Blip5000Stop();
            ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();
            AIdling.IdlingStop(); // Stop Idling
            AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
            ANFSR.FSRStop();
            ANFSR15000.NFSR15000Stop();
            ANBLIPC14000.BLIPC14000Stop();
            ANBLIPC10000.BLIPC10000Stop();
            ANBLIPC7500.BLIPC7500Stop();
            ANBLIPC6000.BLIPC6000Stop();
            ANBLIPC5500.BLIPC5500Stop();
            ANBLIPC4000.BLIPC4000Stop();
            ANBLIPC3000.BLIPC3000Stop();
            ANRevLimiter.RevLimiterStop();
        }
        if (MainControllerZX10.iexhaust == 1)
        {
            AStarting.StartingStop();
            ANBLIPAkra1.BlipStop();
            ANSLIAkra1.NSLIStop();
            ANFSI1Akra1.NFSI1Stop();
            ANBLIP3000Akra1.Blip3000Stop();
            ANBLIP4000Akra1.Blip4000Stop();
            ANBLIP5000Akra1.Blip5000Stop();
            ANBLIP6000Akra1.Blip6000Stop();
            ANBLIP7000Akra1.Blip7000Stop();
            ANBLIP10000Akra1.Blip10000Stop();
            ANBLIP14000Akra1.Blip14000Stop();

            AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
            ANFSRAkra1.FSRStop();
            ANFSR15000.NFSR15000Stop();
            ANBLIPC14000Akra1.BLIPC14000Stop();
            ANBLIPC10000Akra1.BLIPC10000Stop();
            ANBLIPC7500Akra1.BLIPC7500Stop();
            ANBLIPC6000Akra1.BLIPC6000Stop();
            ANBLIPC5500Akra1.BLIPC5500Stop();
            ANBLIPC4000Akra1.BLIPC4000Stop();
            ANBLIPC3000Akra1.BLIPC3000Stop();
            ANRevLimiterAkra1.RevLimiterStop();
        }
        rpm1 = 0;

    }

    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        ReverbXoneMix = 0.00f;
//        RoadView.SetActive(true); // Appearance of Road
//        GarageView.SetActive(false); // Disapperance of Garage
//        TunnelView.SetActive(false); // DisAppearance of Tunnel

//        HeadlightReflectionR.SetActive(false);
//        HeadlightReflectionL.SetActive(false);

        AIdling.IdlingReverbRoad();
        AStartup.StartupReverbRoad();
        AStarting.StartingReverbRoad();
        AShutting.ShuttingReverbRoad();
        ANRevLimiter.RevLimiterReverbRoad();
        //ANFSR.NFSRReverbRoad();
        ANSLI.NSLIReverbRoad();
        ANFSI1.NFSI1ReverbRoad();
        //   ANFSI2.NFSI2ReverbRoad();

        ANBLIP2000.Blip2000ReverbRoad();
        ANBLIP3000.Blip3000ReverbRoad();
        ANBLIP4000.Blip4000ReverbRoad();
        ANBLIP5000.Blip5000ReverbRoad();
        ANBLIP6000.Blip6000ReverbRoad();
        ANBLIP7000.Blip7000ReverbRoad();
        ANBLIP10000.Blip10000ReverbRoad();
        ANBLIP14000.Blip14000ReverbRoad();

        ANBLIPC3000.Blipc3000ReverbRoad();
        ANBLIPC4000.Blipc4000ReverbRoad();
        ANBLIPC5500.Blipc5000ReverbRoad();
        ANBLIPC6000.Blipc6000ReverbRoad();
        ANBLIPC7500.Blipc7500ReverbRoad();
        ANBLIPC10000.Blipc10000ReverbRoad();
        ANBLIPC14000.Blipc14000ReverbRoad();
        ANFSR15000.NFSR15000ReverbRoad();
        ANFSR.NFSRReverbRoad();
        // Akra 1
        AIdlingAkra1.IdlingAkra1ReverbRoad();
        ANRevLimiterAkra1.RevLimiterAkra1ReverbRoad();
        ANSLIAkra1.NSLIAkra1ReverbRoad();
        ANFSI1Akra1.NFSI1Akra1ReverbRoad();
        //   ANFSI2.NFSI2ReverbRoad();
        ANBLIP2000Akra1.Blip2000Akra1ReverbRoad();
        ANBLIP3000Akra1.Blip3000Akra1ReverbRoad();
        ANBLIP4000Akra1.Blip4000Akra1ReverbRoad();
        ANBLIP5000Akra1.Blip5000Akra1ReverbRoad();
        ANBLIP6000Akra1.Blip6000Akra1ReverbRoad();
        ANBLIP7000Akra1.Blip7000Akra1ReverbRoad();
        ANBLIP10000Akra1.Blip10000Akra1ReverbRoad();
        ANBLIP14000Akra1.Blip14000Akra1ReverbRoad();


        ANBLIPC3000Akra1.Blipc3000Akra1ReverbRoad();
        ANBLIPC4000Akra1.Blipc4000Akra1ReverbRoad();
        ANBLIPC5500Akra1.Blipc5000Akra1ReverbRoad();
        ANBLIPC6000Akra1.Blipc6000Akra1ReverbRoad();
        ANBLIPC7500Akra1.Blipc7500Akra1ReverbRoad();
        ANBLIPC10000Akra1.Blipc10000Akra1ReverbRoad();
        ANBLIPC14000Akra1.Blipc14000Akra1ReverbRoad();
        ANFSR15000Akra1.NFSR15000Akra1ReverbRoad();
        ANFSRAkra1.NFSRAkra1ReverbRoad();

        ACrackle10000.Crackle10000ReverbRoad();
        ACrackle8000.Crackle8000ReverbRoad();

    }



    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {
       ReverbXoneMix = 1.03f;
  //      RoadView.SetActive(false); // DisAppearance of Road
  //      TunnelView.SetActive(false); // DisAppearance of Tunnel
  //      GarageView.SetActive(true); // Disapperance of Garage

  //      HeadlightReflectionR.SetActive(true);
  //      HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000.Blip2000ReverbGarage();
        ANBLIP3000.Blip3000ReverbGarage();
        ANBLIP4000.Blip4000ReverbGarage();
        ANBLIP5000.Blip5000ReverbGarage();
        ANBLIP6000.Blip6000ReverbGarage();
        ANBLIP7000.Blip7000ReverbGarage();
        ANBLIP10000.Blip10000ReverbGarage();
        ANBLIP14000.Blip14000ReverbGarage();

        ANBLIPC3000.Blipc3000ReverbGarage();
        ANBLIPC4000.Blipc4000ReverbGarage();
        ANBLIPC5500.Blipc5000ReverbGarage();
        ANBLIPC6000.Blipc6000ReverbGarage();
        ANBLIPC7500.Blipc7500ReverbGarage();
        ANBLIPC10000.Blipc10000ReverbGarage();
        ANBLIPC14000.Blipc14000ReverbGarage();
        ANFSR15000.NFSR15000ReverbGarage();
        ANFSR.NFSRReverbGarage();

        // Akra1
        AIdlingAkra1.IdlingAkra1ReverbGarage();
        ANRevLimiterAkra1.RevLimiterAkra1ReverbGarage();
        ANSLIAkra1.NSLIAkra1ReverbGarage();
        ANFSI1Akra1.NFSI1Akra1ReverbGarage();
        //   ANFSI2.NFSI2ReverbRoad();
        ANBLIP2000Akra1.Blip2000Akra1ReverbGarage();
        ANBLIP3000Akra1.Blip3000Akra1ReverbGarage();
        ANBLIP4000Akra1.Blip4000Akra1ReverbGarage();
        ANBLIP5000Akra1.Blip5000Akra1ReverbGarage();
        ANBLIP6000Akra1.Blip6000Akra1ReverbGarage();
        ANBLIP7000Akra1.Blip7000Akra1ReverbGarage();
        ANBLIP10000Akra1.Blip10000Akra1ReverbGarage();
        ANBLIP14000Akra1.Blip14000Akra1ReverbGarage();

        ANBLIPC3000Akra1.Blipc3000Akra1ReverbGarage();
        ANBLIPC4000Akra1.Blipc4000Akra1ReverbGarage();
        ANBLIPC5500Akra1.Blipc5000Akra1ReverbGarage();
        ANBLIPC6000Akra1.Blipc6000Akra1ReverbGarage();
        ANBLIPC7500Akra1.Blipc7500Akra1ReverbGarage();
        ANBLIPC10000Akra1.Blipc10000Akra1ReverbGarage();
        ANBLIPC14000Akra1.Blipc14000Akra1ReverbGarage();
        ANFSR15000Akra1.NFSR15000Akra1ReverbGarage();
        ANFSRAkra1.NFSRAkra1ReverbGarage();

        ACrackle10000.Crackle10000ReverbGarage();
        ACrackle8000.Crackle8000ReverbGarage();
    }


    // DisAppearance of Racetrack Road/Garage
    public void TunnelAppear()  // 
    {
        ReverbXoneMix = 1.1f;
   //     TunnelView.SetActive(true); // Disapperance of Garage
    //    RoadView.SetActive(false); // Appearance of Road
   //     GarageView.SetActive(false); // Disapperance of Garage

    //    HeadlightReflectionR.SetActive(true);
    //    HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000.Blip2000ReverbGarage();
        ANBLIP3000.Blip3000ReverbGarage();
        ANBLIP4000.Blip4000ReverbGarage();
        ANBLIP5000.Blip5000ReverbGarage();
        ANBLIP6000.Blip6000ReverbGarage();
        ANBLIP7000.Blip7000ReverbGarage();
        ANBLIP10000.Blip10000ReverbGarage();
        ANBLIP14000.Blip14000ReverbGarage();

        ANBLIPC3000.Blipc3000ReverbGarage();
        ANBLIPC4000.Blipc4000ReverbGarage();
        ANBLIPC5500.Blipc5000ReverbGarage();
        ANBLIPC6000.Blipc6000ReverbGarage();
        ANBLIPC7500.Blipc7500ReverbGarage();
        ANBLIPC10000.Blipc10000ReverbGarage();
        ANBLIPC14000.Blipc14000ReverbGarage();
        ANFSR15000.NFSR15000ReverbGarage();
        ANFSR.NFSRReverbGarage();

        // Akra1
        AIdlingAkra1.IdlingAkra1ReverbGarage();
        ANRevLimiterAkra1.RevLimiterAkra1ReverbGarage();
        ANSLIAkra1.NSLIAkra1ReverbGarage();
        ANFSI1Akra1.NFSI1Akra1ReverbGarage();
        //   ANFSI2.NFSI2ReverbRoad();
        ANBLIP2000Akra1.Blip2000Akra1ReverbGarage();
        ANBLIP3000Akra1.Blip3000Akra1ReverbGarage();
        ANBLIP4000Akra1.Blip4000Akra1ReverbGarage();
        ANBLIP5000Akra1.Blip5000Akra1ReverbGarage();
        ANBLIP6000Akra1.Blip6000Akra1ReverbGarage();
        ANBLIP7000Akra1.Blip7000Akra1ReverbGarage();
        ANBLIP10000Akra1.Blip10000Akra1ReverbGarage();
        ANBLIP14000Akra1.Blip14000Akra1ReverbGarage();

        ANBLIPC3000Akra1.Blipc3000Akra1ReverbGarage();
        ANBLIPC4000Akra1.Blipc4000Akra1ReverbGarage();
        ANBLIPC5500Akra1.Blipc5000Akra1ReverbGarage();
        ANBLIPC6000Akra1.Blipc6000Akra1ReverbGarage();
        ANBLIPC7500Akra1.Blipc7500Akra1ReverbGarage();
        ANBLIPC10000Akra1.Blipc10000Akra1ReverbGarage();
        ANBLIPC14000Akra1.Blipc14000Akra1ReverbGarage();
        ANFSR15000Akra1.NFSR15000Akra1ReverbGarage();
        ANFSRAkra1.NFSRAkra1ReverbGarage();


        ACrackle10000.Crackle10000ReverbGarage();
        ACrackle8000.Crackle8000ReverbGarage();

    }



    // Fn to Select Exhaust Pipe : 0- Stock, 1- Akra1
  
    




  


    // GUI Events Functions
    public void VibratorOnOff()
    {
        if (ivibrator == 0) ivibrator = 1;
        else if (ivibrator == 1) ivibrator = 0;

    }



    // Console RPM Remove
  

    // Exhaust Pop/Backfire
    void Explode()
    {
        AVExhaustPop.ExhaustPop();
    }

   
    //******* In Gear Routines
    // Keyin from Touch
    public void GearUp()
    {

        gear = gear + 1;

        if (gear == 1) itrate = 100;    // To enable Gear1 SLI
        else if (gear == 2)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                StartCoroutine("FadeinNFSRG");
                ANRevLimiterGear.RevLimiterStop();

                // Debug.Log("gear+1=22");
            }

            if (itrate == 200 || itrate == 300)
            {

                itrate = 202;
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANSLIGear2.NSLIGearChange();
                    ANSLIGear1.NSLIStop();
                    ANFSI1Gear1.NFSI1Stop();
                }

            }
        }
        else if (gear == 3)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                StartCoroutine("FadeinNFSRG");
                ANRevLimiterGear.RevLimiterStop();

                // Debug.Log("gear+1=22");
            }

            if (itrate == 202 || itrate == 302)
            {

                itrate = 203;
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANSLIGear3.NSLIGearChange();
                    ANSLIGear2.NSLIStop();
                    ANFSI1Gear2.NFSI1Stop();
                    ANSLIGear1.NSLIStop();
                    ANFSI1Gear1.NFSI1Stop();
                }

            }
        }

        else if (gear == 4)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                StartCoroutine("FadeinNFSRG");
                ANRevLimiterGear.RevLimiterStop();

                // Debug.Log("gear+1=22");
            }

            if (itrate == 203 || itrate == 303)
            {

                itrate = 204;
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANSLIGear4.NSLIGearChange();
                    ANSLIGear3.NSLIStop();
                    ANFSI1Gear3.NFSI1Stop();
                    ANSLIGear2.NSLIStop();
                    ANFSI1Gear2.NFSI1Stop();
                }

            }
        }

        else if (gear == 5)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                StartCoroutine("FadeinNFSRG");
                ANRevLimiterGear.RevLimiterStop();

                // Debug.Log("gear+1=22");
            }

            if (itrate == 204 || itrate == 304)
            {

                itrate = 205;
                if (MainControllerZX10.iexhaust == 0)
                {
                    ANSLIGear5.NSLIGearChange();
                    ANSLIGear4.NSLIStop();
                    ANFSI1Gear4.NFSI1Stop();
                    ANSLIGear3.NSLIStop();
                    ANFSI1Gear3.NFSI1Stop();
                }

            }
        }
    }

    public void GearDown()
    {
        if (gear > 0)
        {
            gear = gear - 1;
            if (gear == 1)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                    StartCoroutine("FadeinNFSRG");
                    ANRevLimiterGear.RevLimiterStop();

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 202 || itrate == 302)
                {

                    itrate = 200;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANSLIGear1.NSLIGearChange();
                        ANSLIGear2.NSLIStop();
                        ANFSI1Gear2.NFSI1Stop();
                    }

                }
            }
            else if (gear == 2)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                    StartCoroutine("FadeinNFSRG");
                    ANRevLimiterGear.RevLimiterStop();

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 203 || itrate == 303)
                {

                    itrate = 202;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANSLIGear2.NSLIGearChange();
                        ANSLIGear3.NSLIStop();
                        ANFSI1Gear3.NFSI1Stop();
                    }

                }
            }

            else if (gear == 3)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                    StartCoroutine("FadeinNFSRG");
                    ANRevLimiterGear.RevLimiterStop();

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 204 || itrate == 304)
                {

                    itrate = 203;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANSLIGear3.NSLIGearChange();
                        ANSLIGear4.NSLIStop();
                        ANFSI1Gear4.NFSI1Stop();
                    }

                }
            }

            else if (gear == 4)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
                    StartCoroutine("FadeinNFSRG");
                    ANRevLimiterGear.RevLimiterStop();

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 205 || itrate == 305)
                {

                    itrate = 204;
                    if (MainControllerZX10.iexhaust == 0)
                    {
                        ANSLIGear4.NSLIGearChange();
                        ANSLIGear5.NSLIStop();
                        ANFSI1Gear5.NFSI1Stop();
                    }

                }
            }

        }
    }
    public void LaunchControlEngage()
    {
        ilaunchCengaged = 1;
    }


    IEnumerator StartFSRG1()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        Debug.Log("INside FSRG1");
        if (itrate == 200 || itrate == 300 || itrate == 49990 || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305 || itrate == 100000)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
           //                Debug.Log("Inside FSR Gear$$$$$$$$$$$$$$$$$$$$$$$$$$  Gear=");
           //      Debug.Log(gear);

            ifadeinnfsr = 1;
            if (MainControllerZX10.iexhaust == 0) ANFSRGear.FSR();
            else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.FSR();
            StartCoroutine("FadeinNFSRG");
            ANRevLimiterGear.RevLimiterStop();
            if (iPopCrackle == 1 && rpm1 > 7000)
            {
                Debug.Log("Inside Blipc15000------------------------");
                ACrackle10000.Crackle10000();
                StartCoroutine("Explode");
            }

            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLIG");
            StartCoroutine("FadeoutNFSI1G");
            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;
            itrate = 49990;
        }
    }

    IEnumerator FadeinNFSRG()
    {
        //        Debug.Log("Gear ____Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (MainControllerZX10.iexhaust == 0) ANFSRGear.NFSRFadein();
            //       else if (MainControllerZX10.iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
    }
    IEnumerator FadeoutNSLIG()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (gear == 1)
            {
                if (MainControllerZX10.iexhaust == 0) ANSLIGear1.NSLIFadeout();
                else if (MainControllerZX10.iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 2)
            {
                if (MainControllerZX10.iexhaust == 0) ANSLIGear2.NSLIFadeout();
                else if (MainControllerZX10.iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 3)
            {
                if (MainControllerZX10.iexhaust == 0) ANSLIGear3.NSLIFadeout();
                else if (MainControllerZX10.iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 4)
            {
                if (MainControllerZX10.iexhaust == 0) ANSLIGear4.NSLIFadeout();
                else if (MainControllerZX10.iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 5)
            {
                if (MainControllerZX10.iexhaust == 0) ANSLIGear5.NSLIFadeout();
                else if (MainControllerZX10.iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }

        }
    }
    IEnumerator FadeoutNFSI1G()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            //     ANFSI1.NFSI1Fadeout();
            if (gear == 1)
            {
                if (MainControllerZX10.iexhaust == 0) ANFSI1Gear1.NFSI1Fadeout();
            }
            //    else if (MainControllerZX10.iexhaust == 1) ANFSI1Akra1.NFSI1Fadeout();
            if (gear == 2)
            {
                if (MainControllerZX10.iexhaust == 0) ANFSI1Gear2.NFSI1Fadeout();
            }
            if (gear == 3)
            {
                if (MainControllerZX10.iexhaust == 0) ANFSI1Gear3.NFSI1Fadeout();
            }
            if (gear == 4)
            {
                if (MainControllerZX10.iexhaust == 0) ANFSI1Gear4.NFSI1Fadeout();
            }
            if (gear == 5)
            {
                if (MainControllerZX10.iexhaust == 0) ANFSI1Gear5.NFSI1Fadeout();
            }
        }
    }

    private IEnumerator LaunchRevRise()
    {
        yield return new WaitForSeconds(0.1f);
        //   Debug.Log("inside LaunchRevs");
        while (rpm1 < revslaunch)
        {
            //     Debug.Log("rpm1=");
            //     Debug.Log(rpm1);
            yield return new WaitForSeconds(0.01f); // wait half a second
            rpm1 = rpm1 + 300;

            //     if (rpmi <= 7400)
            //     {

        }

    }
    private IEnumerator LaunchRevDrop()
    {
        yield return new WaitForSeconds(0.01f);
        Debug.Log("Launch Drop1 rpm1=");
        Debug.Log(rpm1);

        while (rpm1 > revslaunchdrop)
        {
            //       itrate = 100;
            Debug.Log("Launch Drop rpm1=");
            Debug.Log(rpm1);
            yield return new WaitForSeconds(0.01f); // wait half a second
            rpm1 = rpm1 - 300;

            //     if (rpmi <= 7400)
            //     {

        }

        //      yield return new WaitForSeconds(0.2f);
        ilaunchCengaged = 3;
        iclutchrelease = 1;
        //      trate = 1;  // TO start SLI
        itrate = 100;  //
    }

    public void StartupSound()
    {
        AStartup.Startup();
    }

    float clutchpos = 0;
    public void SliderClutch(float sliderpositionclutch)
    {
        //      moveSpeed = newSpeed;
        clutchpos = sliderpositionclutch;

    }


}
