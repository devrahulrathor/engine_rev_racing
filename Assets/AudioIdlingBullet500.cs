using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingBullet500 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineBullet500Drag.irpm == 0)
        {
            //         Debug.Log("Inside Idling Sub");
         //   SoundEngineBullet500Drag.rpm1 = SoundEngineBullet500Drag.rpmidling;

            MainControllerDrag.rpm1 = SoundEngineBullet500Drag.rpmidling;
            MainControllerDrag.bikespeed = 0;
        }
    }

    public void Idling()
    {
        Debug.Log("Inside AIdling Bullet 500************************************************");
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = SoundEngineBullet500Drag.idlingvol;
        Debug.Log("Inside Idling************************************************ Idlingvol=");
        Debug.Log(audio.volume);
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       Idlingvolume = 0;


    }

    double Idlingvolume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

            SoundEngineBullet500Drag.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        Idlingvolume = Idlingvolume + 0.1;
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
        else
        {
            audio.volume = 1;
            SoundEngineBullet500Drag.ifadeinidling = 0;

            //            audio.Stop();
        }

    }
    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineBullet500Drag.ReverbXoneMix;
    }
}



