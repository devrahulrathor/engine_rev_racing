﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPI410000Akra1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void Blip10000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineI4.rpm1 >= 7000 && AudioEngineI4.rpm1 < 8000)
        {
            audio.Play();
        }
        else if (AudioEngineI4.rpm1 >= 6000 && AudioEngineI4.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.026F);
        }
        else if (AudioEngineI4.rpm1 >= 5000 && AudioEngineI4.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.049F);
        }
        else if (AudioEngineI4.rpm1 >= 4000 && AudioEngineI4.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.089F);
        }
        else if (AudioEngineI4.rpm1 >= 3000 && AudioEngineI4.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.148F);
        }
        else if (AudioEngineI4.rpm1 >= 2000 && AudioEngineI4.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.202F);
        }


        else if (AudioEngineI4.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.299F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip10000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip10000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip10000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //    audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineI4.ReverbXoneMix;
    }
}

