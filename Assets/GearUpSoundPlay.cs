﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearUpSoundPlay : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GearupSound()
    {
        if (MainControllerDrag.gear == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();

            audio.Play();
            audio.Play(44100);
        }

    }
}