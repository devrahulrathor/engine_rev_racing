﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPC6000R34 : MonoBehaviour
{
    int[] posrpmarrayfsr6000 = { 6000, 6000, 6000, 5500, 5000, 4500, 3800, 3800, 3000, 2500, 2400, 1800, 1250, 1250, 1250 };

    double[] rpmposarrayfsr6000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineR34.itrate == 6005)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();
            //       Debug.Log("Inside RPMCal 6000");
            //      Debug.Log(rpm);
        }
        LowPassFilter();
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineR34.rpm1 = posrpmarrayfsr6000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineR34.rpm1 <= (AudioEngineR34.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }
    public void BLIPC6000()
    {
        //      Debug.Log("Inside BlipC6000");
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1;
        audio.Play();
        audio.Play(44100);
    }

    public void BLIPC6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }


    double Blip6000volume = 1.0;
    public void BLIPCR6000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip6000volume = Blip6000volume - 0.1;
        if (Blip6000volume > 0) audio.volume = (float)Blip6000volume;
        else
        {

            AudioEngineR34.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
        }
    }
    public void Blipc6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blipc6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR34.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}
