using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000STriple : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineSTriple.itrate == 5005)
        {
            AudioEngineSTriple.rpm1 = 6000;
            //           Debug.Log(AudioEngineSTriple.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineSTriple.rpm1 >= 5000 && AudioEngineSTriple.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineSTriple.rpm1 >= 4000 && AudioEngineSTriple.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.02F);
        }
        else if (AudioEngineSTriple.rpm1 >= 3000 && AudioEngineSTriple.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.072F);
        }
        else if (AudioEngineSTriple.rpm1 >= 2000 && AudioEngineSTriple.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.136F);
        }
        else if (AudioEngineSTriple.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.232F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}


