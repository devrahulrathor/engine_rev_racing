﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizeGT350OffNote : MonoBehaviour
{
    public GameObject CustomizeforGT350OffNote;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CustomizeforGT350OffNoteOn()
    {
        Debug.Log("1: Vehic eNo=");
        Debug.Log(MustangVehicleOptions.iMustangVehicle);
        if (MustangVehicleOptions.iMustangVehicle == 1)
        {
            Debug.Log("2");
            CustomizeforGT350OffNote.SetActive(true);
            StartCoroutine("CustomizeforGT350OffNote1Off");
        }
    }

    public void CustomizeforGT350OffNoteOff()
    {
      
            CustomizeforGT350OffNote.SetActive(false);
     
    }

    private IEnumerator CustomizeforGT350OffNote1Off()
    {
        yield return new WaitForSeconds(2f);
        {
            CustomizeforGT350OffNote.SetActive(false);
        }
    }

}
