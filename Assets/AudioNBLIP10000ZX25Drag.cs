using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP10000ZX25Drag : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void Blip10000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineZX25Drag.rpm1 >= 7000 && SoundEngineZX25Drag.rpm1 < 8000)
        {
            audio.Play();
        }
        else if (SoundEngineZX25Drag.rpm1 >= 6000 && SoundEngineZX25Drag.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.177F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 5000 && SoundEngineZX25Drag.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.330F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 4000 && SoundEngineZX25Drag.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.377F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 3000 && SoundEngineZX25Drag.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.421F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 2000 && SoundEngineZX25Drag.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.464F);
        }


        else if (SoundEngineZX25Drag.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.581F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip10000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip10000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip10000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //   audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
    }
}
