﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34PosCracklesMenu : MonoBehaviour
{
    int iR34PopsCrackles = 0;
    // Other Game Options Appear/Disappear
    //    public GameObject R34ExhaustOptions;
    public GameObject R34LocationOptions;
    //  public GameObject R34ViewsButton;  // Deactivate Exhaust Button when Console View

    public GameObject PopsCracklesButton1;
    public GameObject PopsCracklesDropDown;
    public static int iR34Customize = 0, iR34thisPopsCracklesbutton = 0;  // Appear/Disapperar Counter

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void R34RPopsCracklesMenu()
    {
        if (iR34PopsCrackles == 0)
        {
            PopsCracklesDropDown.SetActive(true);
            //        DefaultView.SetActive(true);
            //        ConsoleView.SetActive(true);

            //         R34ExhaustOptions.SetActive(false);
            R34LocationOptions.SetActive(false);
         
            //         R34ViewsButton.SetActive(false);
            iR34PopsCrackles = 1;
        }
        else if (iR34PopsCrackles == 1)
        {
            PopsCracklesDropDown.SetActive(false);
            //          DefaultView.SetActive(false);
            //          ConsoleView.SetActive(false);

            //       R34ExhaustOptions.SetActive(true);
            R34LocationOptions.SetActive(true);
            //      R34ViewsButton.SetActive(true);
            iR34PopsCrackles = 0;
        }
    }

    public void PopsCracklesDropDownDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        Debug.Log("INside Custoimze 2");
        PopsCracklesDropDown.SetActive(false);
    


        iR34PopsCrackles = 0;
    }
    public void PosCracklesButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        //       Debug.Log("CustomizeButton=");
        //       Debug.Log(iR34thisCustomizebutton);
        if (iR34thisPopsCracklesbutton == 0)
        {
            //        Debug.Log("De Acivating Cutomize");
            PopsCracklesButton1.SetActive(false);
            iR34thisPopsCracklesbutton = 1;
        }
        else if (iR34thisPopsCracklesbutton == 1)
        {
            //      Debug.Log("Acivating Cutomize");
            PopsCracklesButton1.SetActive(true);
            iR34thisPopsCracklesbutton = 0;
        }
    }
}
