﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP4000R35Aft1 : MonoBehaviour
{
    public static float dt4000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        LowPassFilter();
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip4000()
    {


        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineR35.rpm1 >= 3000 && AudioEngineR35.rpm1 < 4000)
        {

            audio.Play();

        }
        else if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {
            dt4000 = 0.143f;
            //   dt4000 = 0.192f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }
        else if (AudioEngineR35.rpm1 < 2000)
        {
            dt4000 = 0.376f;
            //   dt4000 = 0.192f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }
        //     else if (AudioEngineR35.rpm1 < 2000)
        //     {
        //         //      dt4000 = 0.161f;
        //         dt4000 = 0.101f;
        //         audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        //     }

        audio.volume = 1;
        //     audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip4000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip4000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip4000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}