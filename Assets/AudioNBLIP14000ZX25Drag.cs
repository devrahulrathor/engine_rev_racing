using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP14000ZX25Drag : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX25Drag.itrate == 5007)
        {
            //        SoundEngineZX25Drag.rpm1 = 14000;
            //           Debug.Log(SoundEngineZX25Drag.rpm1);
        }
    }


    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip14000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (SoundEngineZX25Drag.rpm1 >= 10000 && SoundEngineZX25Drag.rpm1 < 14000)
        {
            audio.Play();
        }
        else if (SoundEngineZX25Drag.rpm1 >= 7000 && SoundEngineZX25Drag.rpm1 < 10000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.234F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 6000 && SoundEngineZX25Drag.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.411F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 5000 && SoundEngineZX25Drag.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.564F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 4000 && SoundEngineZX25Drag.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.610F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 3000 && SoundEngineZX25Drag.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.655F);
        }


        else if (SoundEngineZX25Drag.rpm1 >= 2000 && SoundEngineZX25Drag.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.698F);
        }
        else if (SoundEngineZX25Drag.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.815F);
        audio.volume = 1;
        //    audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip14000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip14000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip14000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //    audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
    }
}


