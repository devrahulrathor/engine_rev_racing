﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R35PaintOptions : MonoBehaviour
{
    public GameObject thisPaintButton, ColorButtons;
    public static int iR35thisPaintbutton = 0;
    public static int iR35PaintOptionButtons = 0;
    // Start is called before the first frame update


    public void PaintButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        Debug.Log("VehicleButton=");
        //     Debug.Log(iR35thisVehiclebutton);
        if (iR35thisPaintbutton == 0)
        {

            thisPaintButton.SetActive(false);
            iR35thisPaintbutton = 1;
            //       ColorButtons.SetActive(false);
        }
        else if (iR35thisPaintbutton == 1)
        {
            thisPaintButton.SetActive(true);
            iR35thisPaintbutton = 0;
            //      ColorButtons.SetActive(true);
        }
    }


    public void PaintOptionButtonsDeactivate()  // 
    {
        ColorButtons.SetActive(false);
        iR35PaintOptionButtons = 0;
    }

    public void PaintOptionButtons()  // 
    {



        if (iR35PaintOptionButtons == 0)
        {
            ColorButtons.SetActive(true);
            iR35PaintOptionButtons = 1;
            R34Customizeoptions.iR34thisCustomizebutton = 0;
        }
        else if (iR35PaintOptionButtons == 1)
        {
            ColorButtons.SetActive(false);

            iR35PaintOptionButtons = 0;
        }
    }

}