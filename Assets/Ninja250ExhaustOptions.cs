﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ninja250ExhaustOptions : MonoBehaviour
{

    public GameObject Ninja250StockExhaust;
    public GameObject Ninja250Akra1Exhaust;
    public GameObject Ninja250BMSRevoExhaust;
    public GameObject Ninja250ExhaustForwardArrow;
    public GameObject Ninja250ExhaustBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject Ninja250LocationOptions;
    public GameObject Ninja250ViewOptions;
    public GameObject Ninja250PopsCrackles;

    public GameObject CantswitchExhaustNote;

    //   public GameObject ZX10RExhaustBackImage;
    public int iNinja250Exhaust = 0;   // Appear/Disapperar Counter
    public int iNinja250ExhaustSwipeCounter = 1;   // Decide the Exhaust Option Active: 1 Garage Active, 2 Road Active

    public GameObject Ninja250MainCameraView;
  //  public GameObject Ninja250RConsoleCameraView;  // Camera Objects

    public static int iNinja250Exhaust1 = 0;   // Appear/Disapperar Counter


    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Update()
    {

        if (Ninja250CameraViewChange.iNinja250CameraSwitch == 1)  // Deactivate Button for Exhaust View Change when Console View
        {

     //       GetComponent<Button>().interactable = false;
        }
        else if (ZX10CameraViewChange.iZX10CameraSwitch == 0)
        {

       //     GetComponent<Button>().interactable = true;
        }
        if (iNinja250Exhaust1 == 110)  // Disabled
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iNinja250ExhaustSwipeCounter == 2)
                {
                    Ninja250ExhaustForwardArrow.SetActive(true);
                    Ninja250ExhaustBackArrow.SetActive(true);
                }
                else if (iNinja250ExhaustSwipeCounter == 3)
                {
                    Ninja250ExhaustForwardArrow.SetActive(false);
                    Ninja250ExhaustBackArrow.SetActive(true);
                }
                else if (iNinja250ExhaustSwipeCounter == 1)
                {
                    Ninja250ExhaustForwardArrow.SetActive(true);
                    Ninja250ExhaustBackArrow.SetActive(false);
                }
                if (pos.y > 400 && pos.y < 600 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iNinja250ExhaustSwipeCounter == 1 && iswipeenable == 1)
                        {
                            Ninja250StockExhaust.SetActive(false);
                            Ninja250Akra1Exhaust.SetActive(true);
                            iNinja250ExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iNinja250ExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            Ninja250Akra1Exhaust.SetActive(false);
              //              Ninja250Akra1CarbonExhaust.SetActive(true);
                            iNinja250ExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                    if (touchZero.deltaPosition.x > 10)
                    {

                        if (iNinja250ExhaustSwipeCounter == 3 && iswipeenable == 1)
                        {
                            //        ZX10RTBR1Exhaust.SetActive(false);
                            Ninja250Akra1Exhaust.SetActive(true);
          //                  Ninja250Akra1CarbonExhaust.SetActive(false);
                            //               ZX10RStockExhaust.SetActive(false);
                            iNinja250ExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iNinja250ExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            Ninja250Akra1Exhaust.SetActive(false);
                            Ninja250StockExhaust.SetActive(true);
                            iNinja250ExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }


    public void Exhausts()  // 
    {


        if (iNinja250Exhaust1 == 0)
        {
            Ninja250StockExhaust.SetActive(true);
            Ninja250Akra1Exhaust.SetActive(true);
   //         Ninja250BMSRevoExhaust.SetActive(true);
            //         ZX10RLocationOptions.SetActive(false);
          Ninja250ViewOptions.SetActive(false);
            Ninja250LocationOptions.SetActive(false);
            Ninja250PopsCrackles.SetActive(false);
            //       ZX10MainCameraView.SetActive(true);
            //       ZX10RConsoleCameraView.SetActive(false);

            iNinja250Exhaust1 = 1;
        }
        else if (iNinja250Exhaust1 == 1)
        {
            Ninja250StockExhaust.SetActive(false);
            Ninja250Akra1Exhaust.SetActive(false);
    //       Ninja250BMSRevoExhaust.SetActive(false);
            //           ZX10RLocationOptions.SetActive(true);
            Ninja250ViewOptions.SetActive(true);
            Ninja250LocationOptions.SetActive(true);
            Ninja250PopsCrackles.SetActive(true);
            iNinja250Exhaust1 = 0;
        }
        CantswitchExhaustNote.SetActive(false);
    }

    public void ExhaustsDeactivatefromOptions()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        Ninja250StockExhaust.SetActive(false);
        Ninja250Akra1Exhaust.SetActive(false);
        //     Ninja250Akra1CarbonExhaust.SetActive(false);
        //           ZX10RLocationOptions.SetActive(true);
        CantswitchExhaustNote.SetActive(false);
        iNinja250Exhaust1 = 0;
    }

    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
    public void ExhaustCantChangeNote()
    {
        if (AudioEngineNinja250.istartfromtouch == 2)
        {
            CantswitchExhaustNote.SetActive(true);
            StartCoroutine("ExhaustCantChangeNoteoff");
        }
    }

    private IEnumerator ExhaustCantChangeNoteoff()
    {
        yield return new WaitForSeconds(2f);
        //   image.color = Color.white;
        CantswitchExhaustNote.SetActive(false);

    }

}
