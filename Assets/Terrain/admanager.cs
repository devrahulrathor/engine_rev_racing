using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
public class admanager : MonoBehaviour
{
    // Start is called before the first frame update
    private BannerView bannerad;
    private InterstitialAd interstitial;
    private RewardedAd rewardBasedVideoAd;
    void Start()
    {
        MobileAds.Initialize(InitializationStatus => { Debug.Log("loading ");  });
        requestbanner();
        RequestInterstitial();
        Requestrewarded();
    }
    private void requestbanner()
    {
        string adunitid= "ca-app-pub-3940256099942544/6300978111";
        this.bannerad = new BannerView(adunitid, AdSize.SmartBanner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        this.bannerad.LoadAd(request);
    }
    // Update is called once per frame
    void Update()
    {
        if (rewardBasedVideoAd.IsLoaded())
        {
            Debug.Log("reward loadded");
            //interstitial.Show();
        }
    }
    public void showadd()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            Debug.Log("not loadded");
        }
    }
    public void showaddreward()
    {
        if (rewardBasedVideoAd.IsLoaded())
        {
            rewardBasedVideoAd.Show();
        }
        else
        {
            Debug.Log(" reward not loadded");
        }
    }
    public void RequestInterstitial()
    {
#if UNITY_EDITOR
        string adUnitId = "ca-app-pub-3940256099942544/1033173712";
#elif UNITY_ANDROID
         string adUnitId = "ca-app-pub-3940256099942544/1033173712";
#elif UNITY_IPHONE
         string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
         string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }
    public void Requestrewarded()
    {
#if UNITY_EDITOR
        string adUnitId = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_ANDROID
         string adUnitId = "ca-app-pub-3940256099942544/1033173712";
#elif UNITY_IPHONE
         string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
         string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        rewardBasedVideoAd = new RewardedAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        rewardBasedVideoAd.LoadAd(request);
        rewardBasedVideoAd.OnAdClosed += RewardBasedVideoAd_OnAdClosed;
    }

    private void RewardBasedVideoAd_OnAdClosed(object sender, EventArgs e)
    {
        Debug.Log("adclosed ");
        Requestrewarded();
        //throw new NotImplementedException();
    }
}
