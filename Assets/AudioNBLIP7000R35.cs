﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000R35 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (AudioEngineR35.itrate == 5006)
        {
            AudioEngineR35.rpm1 = 7000;
            //           Debug.Log(AudioEngineR35.rpm1);
        }
        LowPassFilter();
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineR35.rpm1 >= 6000 && AudioEngineR35.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (AudioEngineR35.rpm1 >= 5000 && AudioEngineR35.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.062F);
        }
        else if (AudioEngineR35.rpm1 >= 4000 && AudioEngineR35.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.106F);
        }
        else if (AudioEngineR35.rpm1 >= 3000 && AudioEngineR35.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.213F);
        }
        else if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.272F);
        }


        else if (AudioEngineR35.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.315F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}

