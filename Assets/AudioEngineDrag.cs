﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using UnityEngine.SceneManagement;

public class AudioEngineDrag : MonoBehaviour
{


    double startTick;
    // Vehicle Selection Section
    public static int ivehicle = 3;     // 1-S1000RR, 2 Jawa, 3-Bullet500CH

    public int ikeyin = 0;    // Ignition Key in Counter
    public int ikeyinfromtouch = 0;
    public int ikeyoffsound = 0; // Counter for activating key-off sound
    public static int istart = -100;    // Start Counter
    public static int istartfromtouch = 0;
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs

    public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 

    public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = -1;
    public static int iblipcanstart = 1;  // Counter for Blip Start
    public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 0;

    int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 1100;
    public float rpmdisplayafterstart = 1.4f;  // Time in seconds after which Idling RPM start post Startup
    public static int rpmblipconst;  // TO Stop Blip at different RPMs
    public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public float sliderval;
    public float slidervalold;

    // Fade in out counters
    public static int ifadeoutnfsr = 0, ifadeinnfsr = 0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)

    public static int ifadeinidling = 0, ifadeoutidling = 0;
    public static int ifadeoutsli = 0, ifadeoutfsi1 = 0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters
    public GameObject GarageView;   // Appearance/Disappearance of Garage
    public static float ReverbXoneMix = 1.03f;   // Reverberation Value for Garage
    public GameObject RoadView;   // Appearance/Disappearance of Road
    public GameObject TunnelView;   // Appearance/Disappearance of Road

    // Akrapovic Counters
    public int exhaustDropDown = 0; // 0 Dropdown up, 1 Dropdown down
    public int iexhaust = 0;  // 0 for Stock, 1 for Akra1,  
                              //    public Button Exhaustbutton;  // Exhaust Option Button
    public GameObject Exhaustbutton;
    public GameObject ExhaustStockbutton;
    public GameObject StockExhaustTick;
    public GameObject ExhaustAkra1button;
    public GameObject Akra1ExhaustTick;
    public GameObject Akra1CarbonExhaustTick;
    public GameObject ExhaustAkra1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased
    public GameObject ExhaustTBR1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased

    public BinSaveLoad BinarySaveLoad;     // Gam Obj to Save Load Consumables in Binary

    public GameObject StockMuffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject StockMufflerShield;   // Appearance/Disappearance of Stock Muffler Shield
    public GameObject Akra1Muffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject AkraBlackMuffler;   // Appearance/Disappearance of AkrapovicBlack
    public GameObject AkraCarbonMuffler;   // Appearance/Disappearance of AkrapovicCarbon
    public GameObject StockMufflerSoundSource;  // Activate/Deactivate Stock Sound (Interference stop issue at Blipc10000 
    public GameObject Akra1MufflerSoundSource;  // Activate/Deactivate Akra1 Sound (Interference stop issue at Blipc10000 

    // GamrObject for RPMConsole1 to Enlarge
    public GameObject RPMConsole1;
    public int iRPMConsole = 0;  // 1:Enlarge, 0:Reduce

    // Backfire Section
    public ExhaustPopZX10R AVExhaustPop;
    public int iPopCrackleX = 1;  // 0-off, 1-On
    public static float dtpop = 0.75f;  // Delay on Backfire 
    public GameObject PopsCracklesHeader;  // Main Menu for Pops Crackles
    public GameObject PopsCrackles1;       // Pos Crackles1 Button
    public GameObject PopsCrackles1Tick;       // Pos Crackles1 Button Tick
                                               // Paid Options
    public AudioScriptCrackle10000 ACrackle10000;  // Crackle 10000
    public AudioScriptCrackle8000 ACrackle8000;  // Crackle 10000

    //Timer Variables
    float StartTimer = 0;
    int istarttimer = 0;  // 1 at Start and 0 at Stop
    float startertime = 0;  // Time during which Starter operates -Less during warmedup engine
    float warmuptime = 20; // Seconds to warm-up

    public Camera Camera;

    //S1000RR
    public AudioStartupDrag1 AStartup;
    public AudioStartdown AStartdown;
    public AudioStartingDrag1 AStarting;
    public AudioShuttingDrag1 AShutting;

    public AudioIdlingDrag1 AIdling;

    public AudioNBLIPDrag1 ANBLIP;

    public AudioNBLIP2000Drag1 ANBLIP2000;
    public AudioNBLIP3000Drag1 ANBLIP3000;
    public AudioNBLIP4000Drag1 ANBLIP4000;
    public AudioNBLIP5000Drag1 ANBLIP5000;
    public AudioNBLIP6000Drag1 ANBLIP6000;
    public AudioNBLIP7000Drag1 ANBLIP7000;
    public AudioNBLIP10000Drag1 ANBLIP10000;
    public AudioNBLIP14000Drag1 ANBLIP14000;

    public AudioNBLIPC3000Drag1 ANBLIPC3000;
    public AudioNBLIPC4000Drag1 ANBLIPC4000;
    public AudioNBLIPC5500Drag1 ANBLIPC5500;
    public AudioNBLIPC6000Drag1 ANBLIPC6000;
    public AudioNBLIPC7500Drag1 ANBLIPC7500;
    public AudioNBLIPC10000Drag1 ANBLIPC10000;
    public AudioNBLIPC14000Drag1 ANBLIPC14000;
    public AudioNRevLimiterDrag1 ANRevLimiter;
    public AudioNFSR15000Drag1 ANFSR15000;
    public AudioNFSR7500Drag1 ANFSR7500;
    public AudioNFSRDrag1 ANFSR;
    public AudioNSLIDrag1 ANSLI;
    public AudioNFSI1Drag1 ANFSI1;

    // Jawa
//    public AudioStartupJawaRace AStartupJawa;
    //   public AudioStartdown AStartdown;
    public AudioStartingJawaDrag1 AStartingJawa;
 //   public AudioShuttingJawaDrag1 AShuttingJawa;

    public AudioIdlingJawaDrag1 AIdlingJawa;
    public AudioNFSRJawaDrag1 ANFSRJawa;
    public AudioNSLIJawaDrag1 ANSLIJawa;
    public AudioNRevLimiterJawaDrag1 ANRevLimiterJawa;

    //Bullet 500
       public AudioStartingBullet500 AStartingBullet500;
    //   public AudioShuttingJawaDrag1 AShuttingJawa;

       public AudioIdlingBullet500 AIdlingBullet500;
      public AudioNFSRBullet500 ANFSRBullet500;
      public AudioNSLIBullet500 ANSLIBullet500;
      public AudioNRevLimiterBullet500 ANRevLimiterBullet500;


    //  Akra1
    public AudioIdlingAkra1 AIdlingAkra1;
    public AudioNSLIAkra1 ANSLIAkra1;
    public AudioNFSI1Akra1 ANFSI1Akra1;
    public AudioNBLIPAkra1 ANBLIPAkra1;
    public AudioNBLIP2000Akra1 ANBLIP2000Akra1;
    public AudioNBLIP3000Akra1 ANBLIP3000Akra1;
    public AudioNBLIP4000Akra1 ANBLIP4000Akra1;
    public AudioNBLIP5000Akra1 ANBLIP5000Akra1;
    public AudioNBLIP6000Akra1 ANBLIP6000Akra1;
    public AudioNBLIP7000Akra1 ANBLIP7000Akra1;
    public AudioNBLIP10000Akra1 ANBLIP10000Akra1;
    public AudioNBLIP14000Akra1 ANBLIP14000Akra1;
    //   public AudioNRevLimiterAkra1 ANRevLimiterAkra1;
    public AudioNBLIPC3000Akra1 ANBLIPC3000Akra1;
    public AudioNBLIPC4000Akra1 ANBLIPC4000Akra1;
    public AudioNBLIPC5500Akra1 ANBLIPC5500Akra1;
    public AudioNBLIPC6000Akra1 ANBLIPC6000Akra1;
    public AudioNBLIPC7500Akra1 ANBLIPC7500Akra1;
    public AudioNBLIPC10000Akra1 ANBLIPC10000Akra1;
    public AudioNBLIPC14000Akra1 ANBLIPC14000Akra1;
    public AudioNRevLimiterAkra1 ANRevLimiterAkra1;
    public AudioNFSR15000Akra1 ANFSR15000Akra1;
    public AudioNFSRAkra1 ANFSRAkra1;


    // GEAR SECTION________________________________________________________________________________________________________________
    //   Gear1
    public static float bikespeed = 0;  // Bike SPEED
    public Text SpeedText;
    public Text GearText;


    public AudioNSLIGear1Drag1 ANSLIGear1;
    public AudioNFSI1Gear1Drag1 ANFSI1Gear1;

    public AudioNSLIGear2Drag1 ANSLIGear2;
    public AudioNFSI1Gear2Drag1 ANFSI1Gear2;

    public AudioNSLIGear3Drag1 ANSLIGear3;
    public AudioNFSI1Gear3Drag1 ANFSI1Gear3;

    public AudioNSLIGear4Drag1 ANSLIGear4;
    public AudioNFSI1Gear4Drag1 ANFSI1Gear4;

    public AudioNSLIGear5Drag1 ANSLIGear5;
    public AudioNFSI1Gear5Drag1 ANFSI1Gear5;

    public AudioNFSRGear1Drag1 ANFSRGear;
    public AudioNRevLimiterGearDrag1 ANRevLimiterGear;


    // Jawa
   public AudioNSLIJawaGear1Drag1 ANSLIGear1Jawa;
    //    public AudioNFSI1Gear1Drag1 ANFSI1Gear1;
   public AudioNSLIJawaGear2Drag1 ANSLIGear2Jawa;
    public AudioNSLIJawaGear3Drag1 ANSLIGear3Jawa;
    public AudioNSLIJawaGear4Drag1 ANSLIGear4Jawa;
    public AudioNFSRJawaGear1Drag1 ANFSRGearJawa;
   public AudioNRevLimiterJawaGearDrag1 ANRevLimiterGearJawa;

    // Bullet 500
    public AudioNSLIBullet500Gear1 ANSLIGear1Bullet500;
    //    public AudioNFSI1Gear1Drag1 ANFSI1Bullet500;
    public AudioNSLIBullet500Gear2 ANSLIGear2Bullet500;
    public AudioNSLIBullet500Gear3 ANSLIGear3Bullet500;
    public AudioNSLIBullet500Gear4 ANSLIGear4Bullet500;
    public AudioNSLIBullet500Gear5 ANSLIGear5Bullet500;
    public AudioNFSRBullet500Gear1 ANFSRGearBullet500;
    public AudioNRevLimiterBullet500Gear ANRevLimiterGearBullet500;

    // Braking Section
  public AudioBrakingBullet500 ABrakingBullet500;
  public int ibrake = 0; // 1 When Pressed, 0, when not pressed, -1 when released 
    int ibrakingloop = 0;  // To enable Brakefromtouch activate once

    //Launch Control

    public AudioLaunchC8000Drag1 ANLaunch8000;
    public AudioLaunchCConst8000Drag1 ANLaunchC8000;
    public AudioLaunchCReduceDrag1 ANLaunchReduce;

    int revslaunch;   // Revs upto which engine revs
    int revslaunchdrop; // Revs to which rpm drops at SLI
                        //    public int GameMode = 0;  // 0- Race/Ride Mode 1- Drag Mode
    public int iclutchrelease = 0;
    int ilaunchCengaged = 0;  // 1 for 8000 RPM hold, 2 for 4000 RPM hold

    //Store Section
    public static int ZX10ExhaustAkra1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs
    public static int ZX10ExhaustTBR1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval2 in Playerprefs

    // Bike Position
    public ZX10Rscript Bike;

    // Headlight Object
    public HeadLight Hlight;
    public RearLight Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 


    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No


    [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, RPM0, RPM1000, RPM1100, RPM1500, RPM2000, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;
    public GameObject ConsoleRPMOff, ConsoleRPM0, ConsoleRPM1000, ConsoleRPM1100, ConsoleRPM1500, ConsoleRPM2000, ConsoleRPM2200, ConsoleRPM2500, ConsoleRPM2800, ConsoleRPM3000, ConsoleRPM3200, ConsoleRPM3500, ConsoleRPM3800, ConsoleRPM4000, ConsoleRPM4200, ConsoleRPM4500, ConsoleRPM4800, ConsoleRPM5000, ConsoleRPM5200, ConsoleRPM5500, ConsoleRPM6000, ConsoleRPM6500, ConsoleRPM7000, ConsoleRPM7500, ConsoleRPM7700, ConsoleRPM8000, ConsoleRPM8200, ConsoleRPM8500, ConsoleRPM8800, ConsoleRPM9000, ConsoleRPM9200, ConsoleRPM9500, ConsoleRPM9800, ConsoleRPM10000, ConsoleRPM10200, ConsoleRPM10500, ConsoleRPM10800, ConsoleRPM11000, ConsoleRPM11200, ConsoleRPM11500, ConsoleRPM11800, ConsoleRPM12000, ConsoleRPM12200, ConsoleRPM12500, ConsoleRPM12800, ConsoleRPM13000, ConsoleRPM13200, ConsoleRPM13500, ConsoleRPM13800, ConsoleRPM14000, ConsoleRPM14200, ConsoleRPM14500, ConsoleRPM14800, ConsoleRPM15000;

    void Awake()
    {
        Application.targetFrameRate = 1000;

    }
    // Use this for initialization
    void Start()
    {
        //       Bike.originalBikePosition = Bike.transform.position;// Get Original Position of Bike to be used to set to silencer view i
        //       Debug.Log(" Entered Bike Pos");


        // IAP Give the PlayerPrefs some values to send over to the next Scene
        //          PlayerPrefs.SetInt("nonconsumableval1", 0);
        //       Purchaser.OnInitialized();
        BinarySaveLoad.SaveZx10Akra1(2);
        //          PlayerPrefs.SetInt("nonconsumableval2", 0);

        //         ZX10ExhaustAkra1 =PlayerPrefs.GetInt("nonconsumableval1");
        //     BinarySaveLoad.LoadZX10Akra1();  // Binary Load Cnsumable Data
        //     ZX10ExhaustTBR1 = PlayerPrefs.GetInt("nonconsumableval2");


    }


    // Update is called once per frame
    void Update()
    {
       
        //       slidervalold = sliderval;
        //      Debug.log(sliderval);

        //      trate = (sliderval - slidervalold);
        //        throttlediff = (sliderval * 150 - rpm1);
        // Starting Module
        //      if (Input.GetKeyDown("space") || istart == 0)
        //           if(ZX10ExhaustAkra1 == 0) ZX10ExhaustAkra1 = PlayerPrefs.GetInt("nonconsumableval1");

        dtpop = Random.Range(0.45f, 0.8f);  // RAndom Variation of Exhhaust Pop delay 

        if (ZX10ExhaustAkra1 == 0) BinarySaveLoad.LoadZX10Akra1();  // Binary Load Cnsumable Data
        if (ZX10ExhaustAkra1 == 1)
        {
            if (ExhaustAkra1PurPanel.activeInHierarchy == true)
            {
                {
                    ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                            //                   Debug.Log("Aftermarket Exhaust 1 Purchased");
                }
            }
        }

        if (ZX10ExhaustTBR1 == 0) ZX10ExhaustTBR1 = PlayerPrefs.GetInt("nonconsumableval2");
        if (ZX10ExhaustTBR1 == 1)
        {
            if (ExhaustTBR1PurPanel.activeInHierarchy == true)
            {
                {
                    ExhaustTBR1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                           //                   Debug.Log("Aftermarket Exhaust 1 Purchased");
                }
            }
        }

   
        // Ingnition On Module
        if (ikeyinfromtouch == 1)
        {
            ikeyinfromtouch = 2;
            ikeyoffsound = 1;  // Activate Key-off sound
            //         NeutralButtonOff.enabled = true;

            // Ignition Key Rotate to On Position
            IgnitionKeyOn.enabled = true;
            IgnitionKeyOff.enabled = false;


            RedButtonOn.enabled = true;
            iconsolestartup = 1;  // Console Startup Loop Enabler
            AStartup.Startup();
            NeutralButtonOff.enabled = false;
            NeutralButtonOn.enabled = true;
            StartCoroutine("ConsoleStartup");

            // Headlight on Module
            //      Hlight.HeadlightOn();


        }
        // Key Off Sound 
        else if (ikeyinfromtouch == 2 && ikeyoffsound == 1 && istartfromtouch != 2)
        {
            ikeyoffsound = 0;

            //           AStartdown.Startdown();
        }



     

        // Start Module
        if ((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1)
        {
            // Enable Console neutral Buttons
            //        NeutralButtonOff.enabled = false;
            //        NeutralButtonOn.enabled = true;

            //        istart = 1;
            StartCoroutine("Startistart1");  // Starting the Loop for Events
            istartfromtouch = 2;

            gear = 0;
            if (ivehicle == 1) AStarting.Starting();
            if (ivehicle == 2) AStartingJawa.Starting();
            if (ivehicle == 3) AStartingBullet500.Starting();
            AStarting.Starting();
            itrate = 1;  // Enable SLI
            idlingvol = 1;
            StartCoroutine("StartIdling");

            // Start Timer upon engine start to change idling speed
            //     StartTimer = 0.0f;
            istarttimer = 1; //Start Timer

            //       irpm = 0;

        }
        // Time Increment After Start Engine
        if (istarttimer == 1) StartTimer += Time.deltaTime;
        //       elseif (istarttimer ==0) Start
        if (StartTimer < warmuptime) startertime = 0.65f;  // Changing Startertime after warm-up
        else startertime = 0.35f;

        //      Debug.Log("Time=");
        //       Debug.Log(StartTimer);

        // Shuttin Module
        if ((Input.GetKeyDown("space") && istart == 1) || istartfromtouch == 3)
        {
            istarttimer = 0; // Stop Timer
                             // Enable Console neutral Buttons
                             //       NeutralButtonOff.enabled = true;
                             //       NeutralButtonOn.enabled = false;
            AShutting.Shutting();
            istart = 0;
            istartfromtouch = 0;

            itrate = -1;
            irpm = -1;  // For stopping RPM at Idling

     //       Debug.Log("3 ++++++++++++++++++++++++++++++++Debug RPM=");
       //     Debug.Log(rpm1);


            if (ivehicle == 1)
            { // S1000RR
              // Stop all sounds
                if (iexhaust == 0)
                {
                    AStarting.StartingStop();
                    ANBLIP.BlipStop();
                    ANSLI.NSLIStop();
                    ANFSI1.NFSI1Stop();
                    ANBLIP3000.Blip3000Stop();
                    ANBLIP4000.Blip4000Stop();
                    ANBLIP5000.Blip5000Stop();
                    ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();
                    AIdling.IdlingStop(); // Stop Idling
                    AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                    ANRevLimiter.RevLimiterStop();
                }
                if (iexhaust == 1)
                {
                    AStarting.StartingStop();
                    ANBLIPAkra1.BlipStop();
                    ANSLIAkra1.NSLIStop();
                    ANFSI1Akra1.NFSI1Stop();
                    ANBLIP3000Akra1.Blip3000Stop();
                    ANBLIP4000Akra1.Blip4000Stop();
                    ANBLIP5000Akra1.Blip5000Stop();
                    ANBLIP6000Akra1.Blip6000Stop();
                    ANBLIP7000Akra1.Blip7000Stop();
                    ANBLIP10000Akra1.Blip10000Stop();
                    ANBLIP14000Akra1.Blip14000Stop();

                    AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
                    ANFSRAkra1.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000Akra1.BLIPC14000Stop();
                    ANBLIPC10000Akra1.BLIPC10000Stop();
                    ANBLIPC7500Akra1.BLIPC7500Stop();
                    ANBLIPC6000Akra1.BLIPC6000Stop();
                    ANBLIPC5500Akra1.BLIPC5500Stop();
                    ANBLIPC4000Akra1.BLIPC4000Stop();
                    ANBLIPC3000Akra1.BLIPC3000Stop();
                    ANRevLimiterAkra1.RevLimiterStop();
                }
            }
            if (ivehicle == 2)
            {
                AStartingJawa.StartingStop();
                //   ANBLIP.BlipStop();
                ANSLIJawa.NSLIStop();
                //     ANFSI1.NFSI1Stop();
                //    ANBLIP3000.Blip3000Stop();
                //    ANBLIP4000.Blip4000Stop();
                //    ANBLIP5000.Blip5000Stop();
                //    ANBLIP6000.Blip6000Stop();
                //    ANBLIP7000.Blip7000Stop();
                //     ANBLIP10000.Blip10000Stop();
                //    ANBLIP14000.Blip14000Stop();
                AIdlingJawa.IdlingStop(); // Stop Idling
                                            //    AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
                ANFSRJawa.FSRStop();
                //    ANFSR15000.NFSR15000Stop();
                //    ANBLIPC14000.BLIPC14000Stop();
                //    ANBLIPC10000.BLIPC10000Stop();
                //    ANBLIPC7500.BLIPC7500Stop();
                //    ANBLIPC6000.BLIPC6000Stop();
                //    ANBLIPC5500.BLIPC5500Stop();
                //    ANBLIPC4000.BLIPC4000Stop();
                //    ANBLIPC3000.BLIPC3000Stop();
                ANRevLimiterJawa.RevLimiterStop();
            }

            if (ivehicle == 3)
            {
                AStartingBullet500.StartingStop();
                //   ANBLIP.BlipStop();
                ANSLIBullet500.NSLIStop();
                //     ANFSI1.NFSI1Stop();
                //    ANBLIP3000.Blip3000Stop();
                //    ANBLIP4000.Blip4000Stop();
                //    ANBLIP5000.Blip5000Stop();
                //    ANBLIP6000.Blip6000Stop();
                //    ANBLIP7000.Blip7000Stop();
                //     ANBLIP10000.Blip10000Stop();
                //    ANBLIP14000.Blip14000Stop();
                AIdlingBullet500.IdlingStop(); // Stop Idling
                                               //    AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
                ANFSRBullet500.FSRStop();
                //    ANFSR15000.NFSR15000Stop();
                //    ANBLIPC14000.BLIPC14000Stop();
                //    ANBLIPC10000.BLIPC10000Stop();
                //    ANBLIPC7500.BLIPC7500Stop();
                //    ANBLIPC6000.BLIPC6000Stop();
                //    ANBLIPC5500.BLIPC5500Stop();
                //    ANBLIPC4000.BLIPC4000Stop();
                //    ANBLIPC3000.BLIPC3000Stop();
                ANRevLimiterBullet500.RevLimiterStop();
            }


            ivibrating = 0; // Stop Vibrating after Rev Limter Stops

            rpm1 = 0;
            RemoveRPMConsole();
            RPM0.enabled = true;
            ConsoleRPM0.SetActive(true);
      //      Debug.Log("4 ++++++++++++++++++++++++++++++++Debug RPM=");
      //      Debug.Log(rpm1);
        }

        // Throttle Spring back Module

        if (istart == 1)
        {
            // Touch Release Module
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
                        //                Debug.Log("Touch Begun");
                        //             message = "Begun ";
                        break;
                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                        if (gear == 0) mSlider.value = 0.0f;

                        break;
                }
            }
            // Mouse Release Module
            if (Input.GetMouseButtonUp(0))
            {

                //      Debug.Log("Pressed primary button.");
                if (gear == 0) mSlider.value = 0.0f;
            }
        }
        trate = (sliderval - slidervalold);
        throttlediff = (sliderval * 150 - rpm1);


     
        // Throttle  Events
        if (istart == 1 && gear == 0 && ilaunchCengaged == 0)
        {
            //       trate = (sliderval - slidervalold);
            //       throttlediff = (sliderval * 150 - rpm1);

            // SLI Module
            //               if(((trate > 0 && throttlediff > 0) && itrate == 1) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 ||itrate == 6006 || itrate == 6005  || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0))  {
            if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
            {
                //              Debug.Log("Inside SLI______________________________________");
                // Select NSLI Sound Stock/Aftermarket
                if (ivehicle == 1)   //S1000RR
                {
                    if (iexhaust == 0)
                    {
                        ANSLI.NSLI();
                        ANFSR.FSRStop();
                        ANFSR15000.NFSR15000Stop();
                        ANBLIPC14000.BLIPC14000Stop();
                        ANBLIPC10000.BLIPC10000Stop();
                        ANBLIPC7500.BLIPC7500Stop();
                        ANBLIPC6000.BLIPC6000Stop();
                        ANBLIPC5500.BLIPC5500Stop();
                        ANBLIPC4000.BLIPC4000Stop();
                        ANBLIPC3000.BLIPC3000Stop();
                    }
                    if (iexhaust == 1)
                    {
                        ANSLIAkra1.NSLI();
                        //________________________________________________
                        //         ANSLI.NSLI();
                        ANFSRAkra1.FSRStop();
                        //         ANFSR15000.NFSR15000Stop();
                        ANBLIPC14000Akra1.BLIPC14000Stop();
                        ANBLIPC10000Akra1.BLIPC10000Stop();
                        ANBLIPC7500Akra1.BLIPC7500Stop();
                        ANBLIPC6000Akra1.BLIPC6000Stop();
                        ANBLIPC5500Akra1.BLIPC5500Stop();
                        ANBLIPC4000Akra1.BLIPC4000Stop();
                        ANBLIPC3000Akra1.BLIPC3000Stop();
                    }
                }
                if (ivehicle == 2) // Jawa2s
                {
                    ANSLIJawa.NSLI();
                    AIdlingJawa.IdlingStop();
                    ANFSRJawa.FSRStop();
                }
                if (ivehicle == 3) // Jawa2s
                {
                    ANSLIBullet500.NSLI();
                    AIdlingBullet500.IdlingStop();
                    ANFSRBullet500.FSRStop();
                }
                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");
               
                if (itrate == 5001)
                {
                    //           ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
                    //           StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 2;    // Counter for FSI to start
                irpm = 1;      // Counter for RPM 
            }
        //    Debug.Log("Itrate = ");
        //    Debug.Log(itrate);

            // FSI1 Module
            //            if (((trate > 2 || throttlediff > 2000) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))

            //               if (((trate > 2 || throttlediff > 2000 || rpm1 > 4500) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))
                 if (((trate > 2 || throttlediff > 2000 || rpm1 > 15500) && itrate == 2 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
       //     if (((trate > 2 || throttlediff > 2000 || rpm1 > 15500) && itrate == 2 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < 12000) && (ivehicle == 1))


            // above FSI1 starts after rpm > 45000//

            {
                if (ivehicle != 2 && ivehicle != 3)
                {
                    Debug.Log("Entering FSI1_________________________________________");
                    // Select NSLI Sound Stock/Aftermarket
                    if (iexhaust == 0)
                    {
                        //                Debug.Log("Entering FSI1 ex=0________________________________________");
                        ANFSI1.NFSI1();
                        ANSLI.NSLIStop();
                        ANFSR.FSRStop();
                        ANFSR15000.NFSR15000Stop();
                        ANBLIPC14000.BLIPC14000Stop();
                        ANBLIPC10000.BLIPC10000Stop();
                        ANBLIPC7500.BLIPC7500Stop();
                        ANBLIPC6000.BLIPC6000Stop();
                        ANBLIPC5500.BLIPC5500Stop();
                        ANBLIPC4000.BLIPC4000Stop();
                        ANBLIPC3000.BLIPC3000Stop();
                    }
                    else if (iexhaust == 1)
                    {
                        //          ANFSI1Akra1.NFSI1();
                        //          ANSLIAkra1.NSLIStop();
                        //          ANFSRAkra1.FSRStop();
                        //          ANFSR15000Akra1.NFSR15000Stop();
                        //          ANBLIPC14000Akra1.BLIPC14000Stop();
                        //          ANBLIPC10000Akra1.BLIPC10000Stop();
                        //          ANBLIPC7500Akra1.BLIPC7500Stop();
                        //          ANBLIPC6000Akra1.BLIPC6000Stop();
                        //          ANBLIPC5500Akra1.BLIPC5500Stop();
                        //          ANBLIPC4000Akra1.BLIPC4000Stop();
                        //          ANBLIPC3000Akra1.BLIPC3000Stop();
                    }

                    if (iexhaust == 0)
                    {


                        ifadeoutidling = 1;
                        fadeoutidlingtime = 0.01f;
                        StartCoroutine("FadeoutIdling");
                        if (itrate == 5001)
                        {
                            //        ifadeoutnfsr = 1;
                            //             StartCoroutine("FadeoutNFSR");
                            //        StartCoroutine("FadeoutBLIPCR7500");
                        }
                        //       AIdling.IdlingStop(); // Stop Idling

                        itrate = 3;    // Counter for FSI to start
                        irpm = 2;      // Counter for RPM 

                    }
                }


            }



            // Blip Module
               if ((trate > 10 || throttlediff > 12000) && throttlediff > 0 && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
         //   if ((trate > 10 || throttlediff > 12000) && throttlediff > 0 && rpm1 < 11000 && iblipcanstart == 1 && istart == 1 && (ivehicle == 1))

            {
                if (ivehicle != 2 && ivehicle  != 3)
                {
                    //           Debug.Log("Trate=");
                    //           Debug.Log(trate);

                    //           Debug.Log("Inside Blip");
                    startTick = AudioSettings.dspTime;

                    iblipcanstart = 0;
                    irevlimitercanstart = 1;

                    // Select NBLIP Sound Stock/Aftermarket
                    if (iexhaust == 0)
                    {
                        ANBLIP.Blip();
                        ANBLIP2000.Blip2000();
                        ANBLIP3000.Blip3000();
                        ANBLIP4000.Blip4000();
                        ANBLIP5000.Blip5000();
                        ANBLIP6000.Blip6000();
                        ANBLIP7000.Blip7000();
                        ANBLIP10000.Blip10000();
                        ANBLIP14000.Blip14000();
                        AIdling.IdlingStop(); // Stop Idling

                        //         ifadeoutidling = 1;
                        //         fadeoutidlingtime = 0.01f;
                        //         StartCoroutine("FadeoutIdling");
                        ANSLI.NSLIStop();
                        ANFSI1.NFSI1Stop();
                        //        ifadeoutsli = 1;
                        //        ifadeoutfsi1 = 1;
                        //         StartCoroutine("FadeoutNSLI");
                        //         StartCoroutine("FadeoutNFSI1");
                        //        ifadeoutnfsr = 1;
                        //        fadeoutfsrtime = 0.005f;
                        //          FadeoutNFSR();



                        //           ANFSR.FSRStop();

                        //         ANFSR15000.NFSR15000Stop();
                        ANBLIPC14000.BLIPC14000Stop();
                        ANBLIPC10000.BLIPC10000Stop();
                        ANBLIPC7500.BLIPC7500Stop();
                        ANBLIPC6000.BLIPC6000Stop();
                        ANBLIPC5500.BLIPC5500Stop();
                        ANBLIPC4000.BLIPC4000Stop();
                        ANBLIPC3000.BLIPC3000Stop();
                    }
                    else if (iexhaust == 1)
                    {
                        //               Debug.Log("Inside Blip iexhaust 1");
                        ANBLIPAkra1.Blip();
                        ANBLIP2000Akra1.Blip2000();
                        ANBLIP3000Akra1.Blip3000();
                        ANBLIP4000Akra1.Blip4000();
                        ANBLIP5000Akra1.Blip5000();
                        ANBLIP6000Akra1.Blip6000();
                        ANBLIP7000Akra1.Blip7000();
                        ANBLIP10000Akra1.Blip10000();
                        ANBLIP14000Akra1.Blip14000();
                        AIdlingAkra1.IdlingAkra1Stop(); // Stop Idling
                        ANSLIAkra1.NSLIStop();
                        ANFSI1Akra1.NFSI1Stop();
                        //        ifadeoutsli = 1;
                        //        ifadeoutfsi1 = 1;
                        //         StartCoroutine("FadeoutNSLI");
                        //         StartCoroutine("FadeoutNFSI1");
                        //        ifadeoutnfsr = 1;
                        //        fadeoutfsrtime = 0.005f;
                        //          FadeoutNFSR();



                        //           ANFSR.FSRStop();

                        //         ANFSR15000.NFSR15000Stop();
                        ANBLIPC14000Akra1.BLIPC14000Stop();
                        ANBLIPC10000Akra1.BLIPC10000Stop();
                        ANBLIPC7500Akra1.BLIPC7500Stop();
                        ANBLIPC6000Akra1.BLIPC6000Stop();
                        ANBLIPC5500Akra1.BLIPC5500Stop();
                        ANBLIPC4000Akra1.BLIPC4000Stop();
                        ANBLIPC3000Akra1.BLIPC3000Stop();
                    }
                    //________________________________________________

                    //          ANBLIP.Blip();
                    //                 ANBLIP2000.Blip2000();
                    //         ANBLIP3000.Blip3000();
                    //         ANBLIP4000.Blip4000();
                    //         ANBLIP5000.Blip5000();
                    //         ANBLIP6000.Blip6000();
                    //         ANBLIP7000.Blip7000();
                    //         ANBLIP10000.Blip10000();
                    //         ANBLIP14000.Blip14000();
                    //         ANFSR15000.NFSR15000();

                    //           ANBLIPC3000.BLIPC3000();

                    idlingvol = 0;   // Making Idling vol 0 so no silence upon start and idling


                    //            NSLIAtrigger.SLIStop();

                    //               Debug.Log("Blip ***************************************Blip");
                    // Stop SLI
                    // Stoo SLR
                    itrate = 5000;  // Blip itrate
                                    //         irpm = 5000;
                                    //           rpmdisplay = rpm1;
                }
                }

       //          if (((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 2 || itrate == 3)) && rpm1 >= 14000)
            if (((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 2 || itrate == 3)) && ((rpm1 >= 14000 && ivehicle == 1) || (rpm1 >= 6000 && ivehicle == 2)|| (rpm1 >= 5100 && ivehicle == 3)))
            //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
            {
                itrate = 10000;  // pre rev limit Trate
                                 //          EnableFSR15000afterRevlimiterStart();
                irevlimitercanstart = 0;
                ivibrating = 1;
                if (ivehicle == 1)
                {
                    rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                    if (iexhaust == 0)
                    {
                        ANRevLimiter.RevLimiter();
                        ANBLIP14000.Blip14000Stop();
                        ANBLIP.BlipStop();
                    }
                    else if (iexhaust == 1)
                    {
                        ANRevLimiterAkra1.RevLimiter();
                        ANBLIP14000Akra1.Blip14000Stop();
                        ANBLIPAkra1.BlipStop();
                        ANFSI1Akra1.NFSI1Stop();  // Test
                        ANSLIAkra1.NSLIStop();  // Test
                    }
                }
                else if(ivehicle == 2)
                {
                    rpm1 = 6000;
                    ANRevLimiterJawa.RevLimiter();
                    ANSLIJawa.NSLIStop();  // Test
                                              //    ANBLIP14000.Blip14000Stop();
                                              //    ANBLIP.BlipStop();
                }
                else if (ivehicle == 3)
                {
                    rpm1 = 5500;
                    ANRevLimiterBullet500.RevLimiter();
                    ANSLIBullet500.NSLIStop();  // Test
                                           //    ANBLIP14000.Blip14000Stop();
                                           //    ANBLIP.BlipStop();
                }

                // Mobile Vibration
                //            ivibrating = 1;
                //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                //          Handheld.Vibrate();
                StartCoroutine("RevLimiterVibrationOnOff");

            }
      //      Debug.Log("ITRATE =");
    //        Debug.Log(itrate);
            //  if (((trate < 0 || throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            if (((trate < 0 || throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000) && (ivehicle == 1))
            //           if (((trate < 0 && throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
          
            {
         //       Debug.Log("Inside BLIPC");
                itrate = 5001;  // Counter for BlipC
                                //     itrate = -1;
                                //         iblipcanstop = 1;
                                //     }
                                //     if(iblipcanstop == 1) {

                startTick = AudioSettings.dspTime - startTick;
                //              Debug.Log("Delta Time=");
                //              Debug.Log(startTick);


                //                  InvokeRepeating("FadeoutBLIP", 0f, 0.01f);
                //                 StartCoroutine("Cancelinvoke"); 
                //             ANBLIP.BLIPFadeout();
                //             ifadeoutblip = 1;
                //             StartCoroutine("FadeoutBLIP");
                //          ANBLIP.BlipStop();

                //             if ((rpm1 > 2500 && rpm1 <= 3300) || (rpm1 > 3800 && rpm1 <= 4300) || (rpm1 > 5000 && rpm1 <= 5500) || (rpm1 > 5800 && rpm1 <= 6300) || (rpm1 > 6800 && rpm1 <= 7500))
                //             {

                //          Debug.Log("Inside trate < 0, rpm=");
                //             Debug.Log(rpm1);

                if (rpm1 > 10000 && rpm1 <= 15000)
                {
                    itrate = 6100;
                    ivibrating = 0; // Stop Vibrating after Rev Limter Stops
                    if (iexhaust == 0)
                    {
                        ANRevLimiter.RevLimiterStop();


                        ANFSR15000.NFSR15000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        if (iPopCrackleX == 1)
                        {
                            Debug.Log("Inside Blipc15000------------------------");
                            ACrackle10000.Crackle10000();
                            StartCoroutine("Explode");
                        }


                    }

                    else if (iexhaust == 1)
                    {
                        //                      Debug.Log("Inside Blipc15000------------------------");
                        ANRevLimiterAkra1.RevLimiterStop();
                        ANFSR15000Akra1.NFSR15000();
                        ANBLIPAkra1.BlipStop();
                        //        ANBLIP14000.Blip14000Stop();
                        ANFSI1Akra1.NFSI1Stop();  // Test

                        if (iPopCrackleX == 1)
                        {
                            ACrackle10000.Crackle10000();
                            StartCoroutine("Explode");
                        }
                    }
                }
                if (rpm1 > 7800 && rpm1 <= 10000)
                //             if (rpm1 > 7500 && rpm1 <= 10000)
                {
                    itrate = 6008;
                    //            iblipcanstop = 0;

                    if (iexhaust == 0)
                    {

                        ANBLIPC14000.BLIPC14000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        if (iPopCrackleX == 1)
                        {
                            Debug.Log("Inside Blipc10000------------------------");
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }
                    }

                    else if (iexhaust == 1)
                    {
                        Debug.Log("Inside Blipc14000");
                        ANBLIPC14000Akra1.BLIPC14000();
                        ANBLIPAkra1.BlipStop();
                        ANBLIP14000Akra1.Blip14000Stop();

                        if (iPopCrackleX == 1)
                        {
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }
                    }

                    //       iblipcanstop = 0;
                }



                if (rpm1 > 7000 && rpm1 <= 7800)
                //              if (rpm1 > 6200 && rpm1 <= 7500)
                {
                    itrate = 6007;
                    //            iblipcanstop = 0;

                    if (iexhaust == 0)
                    {
                        Debug.Log("Inside Blipc7000");
                        ANBLIPC10000.BLIPC10000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        if (iPopCrackleX == 1)
                        {
                            Debug.Log("Inside Blipc10000------------------------");
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }
                    }

                    else if (iexhaust == 1)
                    {
                        //                     Debug.Log("Inside Blipc10000");
                        ANBLIPC10000Akra1.BLIPC10000();
                        ANBLIPAkra1.BlipStop();
                        ANBLIP14000Akra1.Blip14000Stop();
                        //            ANBLIP10000Akra1.Blip10000Stop();  //Test

                        {
                            Debug.Log("Inside Blipc10000------------------------");
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        }

                    }
                    //       iblipcanstop = 0;
                }



                if (rpm1 > 6000 && rpm1 <= 7000)
                {
                    itrate = 6006;

                    rpmblipconst = rpm1;
                    blipcinterval = 5.347f;

                    if (iexhaust == 0)
                    {
                        Debug.Log("Inside Blipc7000");
                        ANBLIPC7500.BLIPC7500();
                        //          iblipcanstop = 0;
                        ANBLIP.BlipStop();
                        //                ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }

                    else if (iexhaust == 1)
                    {
                        //                    Debug.Log("Inside Blipc7500");
                        ANBLIPC7500Akra1.BLIPC7500();
                        //          iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();
                        ANBLIP7000Akra1.Blip7000Stop();  //Test
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();
                    }
                    //       iblipcanstop = 0;
                }

                if (rpm1 > 5000 && rpm1 <= 6000)
                {
                    itrate = 6005;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    if (iexhaust == 0)
                    {
                        Debug.Log("Inside Blipc6000");
                        ANBLIPC6000.BLIPC6000();
                        //         iblipcanstop = 0;
                        ANBLIP.BlipStop();
                        //                ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }

                    else if (iexhaust == 1)
                    {
                        //                    Debug.Log("Inside Blipc6000");
                        ANBLIPC6000Akra1.BLIPC6000();
                        //         iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();
                        ANBLIP6000Akra1.Blip6000Stop();   //Test
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();
                    }

                    //       iblipcanstop = 0;
                }
                if (rpm1 > 4100 && rpm1 <= 5000)

                {

                    //            if ((startTick > 0.214 && startTick < 0.215) || (startTick > 0.221 && startTick < 0.222) || (startTick > 0.227 && startTick < 0.229) || (startTick > 0.234 && startTick < 0.236) || (startTick > 0.24 && startTick < 0.243) || (startTick > 0.247 && startTick < 0.249) || (startTick > 0.252 && startTick < 0.254)) iswitch = 0;
                    //            if (iswitch >= 0)
                    //            {
                    iswitch = 1;
                    itrate = 6004;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.345f;
                    if (iexhaust == 0)
                    {
                        Debug.Log("Inside Blipc5000");

                        ANBLIPC5500.BLIPC5500();
                        //            iblipcanstop = 0;
                        ANBLIP.BlipStop();

                        //            ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }
                    else if (iexhaust == 1)
                    {
                        //                    Debug.Log("Inside Blipc5000");
                        ANBLIPC5500Akra1.BLIPC5500();
                        //            iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();

                        ANBLIP5000Akra1.Blip5000Stop();  //Test
                        ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();
                    }
                    //       iblipcanstop = 0;
                    //          }
                }
                //                         if (rpm1 > 3200 && rpm1 <= 4000 && ((startTick < 0.171 || startTick > 0.175) || (startTick < 0.161 || startTick > 0.166) || (startTick < 0.171 || startTick > 0.175)))
                if (rpm1 > 3000 && rpm1 <= 4100)
                {

                    //        if ((startTick >= 0.169 && startTick <= 0.17) || (startTick >= 0.178 && startTick <= 0.181) || (startTick >= 0.187 && startTick < 0.189) || (startTick >= 0.196 && startTick <= 0.197) || (startTick >= 0.203 && startTick <= 0.207) || (startTick >= 0.211 && startTick <= 0.215) || (startTick >= 0.217 && startTick <= 0.224)) iswitch = 0;
                    //                  if ((startTick > 0.171 && startTick < 0.175) || (startTick > 0.18 && startTick < 0.185) || (startTick > 0.19 && startTick < 0.192) || (startTick > 0.197 && startTick < 0.2) || (startTick > 0.205 && startTick < 0.209) || (startTick > 0.213 && startTick < 0.215) || (startTick > 0.22 && startTick < 0.223)) iswitch = 1;
                    //       if (iswitch >= 0)
                    //       {
                    iswitch = 1;
                    itrate = 6003;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.53f;
                    if (iexhaust == 0)
                    {
                        Debug.Log("Inside Blipc4000");
                        ANBLIPC4000.BLIPC4000();

                        //           iblipcanstop = 0;
                        ANBLIP.BlipStop();


                        //               ANBLIP4000.Blip4000Stop();
                        //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                        if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }

                    else if (iexhaust == 1)
                    {
                        //                     Debug.Log("Inside Blipc4000");
                        ANBLIPC4000Akra1.BLIPC4000();

                        //           iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();


                        //               ANBLIP4000.Blip4000Stop();
                        //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                        if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                        ANBLIP4000Akra1.Blip4000Stop();
                        ANBLIP5000Akra1.Blip5000Stop();
                        ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();


                    }

                    //       iblipcanstop = 0;
                    //       }
                }
                //        if (rpm1 > 320 && rpm1 <= 3200 && startTick > 0.0f )
                if (rpm1 > 2000 && rpm1 <= 3000)
                //          if (startTick >= 0.178)
                {
                    //          if ((startTick >= 0.103 && startTick <= 0.107) || (startTick >= 0.111 && startTick <= 0.114) || (startTick >= 0.125 && startTick <= 0.129) || (startTick >= 0.137 && startTick <= 0.142) || (startTick >= 0.148 && startTick <= 0.152)) iswitch = 0;
                    //             if ((startTick > 0.121 && startTick < 0.122) || (startTick > 0.128 && startTick < 0.131) || (startTick > 0.133 && startTick < 0.135) || (startTick > 0.140 && startTick < 0.143) || (startTick > 0.152 && startTick < 0.156) || (startTick > 0.161 && startTick < 0.166)) iswitch = 1;
                    //         if (iswitch == 0) {
                    iswitch = 1;
                    itrate = 6002;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    if (iexhaust == 0)
                    {
                        Debug.Log("Inside Blipc3000");
                        ANBLIPC3000.BLIPC3000();

                        //      iblipcanstop = 0;
                        ANBLIP.BlipStop();


                        //                 ANBLIP3000.Blip3000Stop();
                        if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000.Blip4000Stop();
                        ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();
                    }
                    else if (iexhaust == 1)
                    {
                        Debug.Log("Inside Blipc3000");
                        ANBLIPC3000Akra1.BLIPC3000();

                        //      iblipcanstop = 0;
                        ANBLIPAkra1.BlipStop();


                        //                 ANBLIP3000.Blip3000Stop();
                        if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000Akra1.Blip4000Stop();
                        ANBLIP4000Akra1.Blip4000Stop();
                        ANBLIP5000Akra1.Blip5000Stop();
                        ANBLIP6000Akra1.Blip6000Stop();
                        ANBLIP7000Akra1.Blip7000Stop();
                        ANBLIP10000Akra1.Blip10000Stop();
                        ANBLIP14000Akra1.Blip14000Stop();

                    }

                    //      }

                    //       iblipcanstop = 0;
                }




                StartCoroutine("EnableBlip");  // Enable blip after 200 ms of Blip 
                                               //         StartCoroutine("StartFSR");     // Enable FSR after Blipc ends
                                               //          StartCoroutine("StartFSR7500");     // Enable FSR after Blipc ends
                                               //          }
            }

            // FSR after SLI module
            //     if ((trate < -0.5 && throttlediff < 0) && (itrate == 2 || itrate == 3 || itrate == 10000) && rpm1 > 2000)
            if ((trate < -0.5 && throttlediff < 0) && (itrate == 2 || itrate == 3 || itrate == 10000) && rpm1 > 2000)
            {
         //       Debug.Log("Entered RevLimiterStop in FSR ");
                blipcinterval = 0;
                StartCoroutine("StartFSR");     // Enable FSR after Blipc ends

            }
     //       Debug.Log("RPM in Audio Engine = ");
     //       Debug.Log(rpm1);
            // Restart Idling
            if (rpm1 <= (rpmidling + 200) && (itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999))
            {
                //          Debug.Log("Pre Enter FadeoutSLR1 ***************************************8");

                //            iingearrestartidling = 0;


                itrate = 1;
                irpm = 0;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0) AIdling.Idling();
                    else if (iexhaust == 1) AIdlingAkra1.IdlingAkra1();
                }
                else if (ivehicle == 2)
                {
                    AIdlingJawa.Idling();
                }
                else if (ivehicle == 3)
                {
                    AIdlingBullet500.Idling();
                }
                ifadeinidling = 1;
                StartCoroutine("FadeinIdling");
                //      StartCoroutine("StartIdling");
                //          ANFSR.FSRStop();
                rpm1 = rpmidling;
                //                ifadeoutnfsr = 1;
                //             fadeoutfsrtime = 0.01f;
                //         StartCoroutine("FadeoutNFSR");
                ANFSR.FSRStop();
                ANFSRAkra1.FSRStop();
                ANFSRJawa.FSRStop();
                //          ANFSR15000.NFSR15000Stop();
                //          ANBLIPC14000.BLIPC14000Stop();
                //          ANBLIPC7500.BLIPC7500Stop();
                //          ANBLIPC6000.BLIPC6000Stop();
                //          ANBLIPC5500.BLIPC5500Stop();
                //          ANBLIPC4000.BLIPC4000Stop();
                //         ANBLIPC3000.BLIPC3000Stop();
                //     StartCoroutine("Setitrate1");

            }

        }   // Neutral Section Over

        // Start Automatic Gear Section for certain bikes - Jawa, Bullet 500,----***********************************______________________________
        if (ivehicle == 2)
        {
            if (itrate >= 200 && itrate < 49990)
            {
                if (gear == 1 && bikespeed > 17 && bikespeed < 37) GearUp();
                if (gear == 2 && bikespeed >= 37 && bikespeed < 66) GearUp();
                if (gear == 3 && bikespeed >= 66 && bikespeed < 1000) GearUp();
            }
            else if (itrate == 49990)
            {
                if (gear == 4 && bikespeed < 66 ) GearDown();
                if (gear == 3 && bikespeed <= 37 && bikespeed > 17) GearDown();
                if (gear == 2 && bikespeed <= 17) GearDown();
            }
        }
        //Bullet 500
        if (ivehicle == 3)
        {
            if (itrate >= 200 && itrate < 49990)
            {
                if (gear == 1 && bikespeed >= 30 && bikespeed < 50) GearUp();
                if (gear == 2 && bikespeed >= 50 && bikespeed < 80) GearUp();
                if (gear == 3 && bikespeed >= 80 && bikespeed < 100) GearUp();
                if (gear == 4 && bikespeed >= 100 && bikespeed < 1000) GearUp();
            }
            else if (itrate == 49990)
            {
                if (gear == 5 && bikespeed < 90) GearDown();
                if (gear == 4 && bikespeed < 70) GearDown();
                if (gear == 3 && bikespeed <= 50 && bikespeed > 30) GearDown();
                if (gear == 2 && bikespeed <= 30) GearDown();
            }
        }

        // End Automatic Gear Section for certain bikes - Jawa, Bullet 500,----***********************************______________________________



        // Ingear Section for Racing *********************************************************************

        //        if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 12000))

        //Bikes Speed and Gear Print on Screen
        SpeedText.text = "SPEED: " + bikespeed;
        GearText.text = "G: " + gear;


        //    Drag Racing Module
        // Launch Control/Throttle Hold
        if (ilaunchCengaged == 1 && itrate == 100)
        {
            ANLaunch8000.Launch8000();
            ANLaunchC8000.LaunchConst8000();
            StartCoroutine("LaunchRevRise");
            revslaunch = 8000;
            revslaunchdrop = 4000;

            AIdling.IdlingStop();
            irpm = 1000;
            //      rpm1 = 8000;

            ilaunchCengaged = 2;
        }
        if (ilaunchCengaged == 2 && clutchpos > 20)  // Clutch Release
        {
            Debug.Log("Inside Clutch Release");
            StartCoroutine("LaunchRevDrop");
            ANLaunchReduce.LaunchReduce();
            ANLaunchC8000.LaunchConst8000Stop();
            ilaunchCengaged = 30;  // To stop LaunchRevDrop to loop  
        }

        // SLI Module
        if (gear == 1 && ((trate > 0 && throttlediff > 0 && ilaunchCengaged == 0) || (throttlediff > 0 && ilaunchCengaged == 3 && iclutchrelease == 1)) && ((itrate == 100 && rpm1 < 12000) || (itrate == 49990)))
        {
            Debug.Log("Inside Gear1SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (ivehicle == 1)
            {
                if (iexhaust == 0)
                {
                    ilaunchCengaged = 0;
                    //       if (ilaunchCengaged == 2) StartCoroutine("LaunchRevDrop");
                    ANSLIGear1.NSLI();

                    AIdling.IdlingStop();
                    ANFSRGear.FSRStop();
                    ANSLI.NSLIStop();
                    ANFSI1.NFSI1Stop();
                    ANFSR.FSRStop();
                    ANLaunchC8000.LaunchConst8000Stop();
                    ANLaunch8000.Launch8000Stop();

                    //     ANFSI2.FSI2Stop();
                    //     ANFSR15000Gear1.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                if (iexhaust == 1)
                {
                    ANSLIAkra1.NSLI();
                    //________________________________________________
                    //         ANSLI.NSLI();
                    ANFSRAkra1.FSRStop();
                    //         ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000Akra1.BLIPC14000Stop();
                    ANBLIPC10000Akra1.BLIPC10000Stop();
                    ANBLIPC7500Akra1.BLIPC7500Stop();
                    ANBLIPC6000Akra1.BLIPC6000Stop();
                    ANBLIPC5500Akra1.BLIPC5500Stop();
                    ANBLIPC4000Akra1.BLIPC4000Stop();
                    ANBLIPC3000Akra1.BLIPC3000Stop();
                }
            }
            else if(ivehicle == 2)
            {
                ilaunchCengaged = 0;
                //       if (ilaunchCengaged == 2) StartCoroutine("LaunchRevDrop");
                ANSLIGear1Jawa.NSLI();

                AIdlingJawa.IdlingStop();
               ANFSRGearJawa.FSRStop();
                ANSLIJawa.NSLIStop();
           //     ANFSI1.NFSI1Stop();
                ANFSRJawa.FSRStop();
         //       ANLaunchC8000.LaunchConst8000Stop();
         //       ANLaunch8000.Launch8000Stop();
            }
            else if (ivehicle == 3)
            {
                ilaunchCengaged = 0;
                //       if (ilaunchCengaged == 2) StartCoroutine("LaunchRevDrop");
                ANSLIGear1Bullet500.NSLI();

                AIdlingBullet500.IdlingStop();
                ANFSRGearBullet500.FSRStop();
                ANSLIBullet500.NSLIStop();
                //     ANFSI1.NFSI1Stop();
                ANFSRBullet500.FSRStop();
                //       ANLaunchC8000.LaunchConst8000Stop();
                //       ANLaunch8000.Launch8000Stop();
            }
            ifadeoutidling = 1;
            fadeoutidlingtime = 0.01f;

            ibrakingloop = 0;  //Enable Braking when SLI Starts

            //         StartCoroutine("FadeoutIdlingG");

            //       AIdling.IdlingStop(); // Stop Idling

            itrate = 200;    // Counter for Gear1FSI to start
            irpm = 1;      // Counter for RPM 
        }
        // FSI1 Module
        //            if (((trate > 2 || throttlediff > 2000) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))

        //    if (gear == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 200 && rpm1 < 12000))
        if (gear == 1 && ivehicle == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 200 && rpm1 < 12000))  // ivehicle for S1000rr and not Jawa as noFSI1
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear1.NFSI1();
                ANSLIGear1.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();

                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                ANFSR.FSRStop();
                //     ANFSI2.FSI2Stop();
                //     ANFSR15000Gear1.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();
            }
            else if (iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 300;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }


        // Gear2 Module____________________________________________________________________ 
        if (trate > 0 && throttlediff > 0 && gear == 2 && (itrate == 200 || itrate == 300 || itrate == 49990))
        {
            Debug.Log("Inside Gear2SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (ivehicle == 1)
            {
                if (iexhaust == 0)
                {
                    ANSLIGear2.NSLI();
                    ANSLIGear1.NSLIStop();
                    ANFSI1Gear1.NFSI1Stop();
                    ANFSRGear.FSRStop();
                }
            }
            else if(ivehicle == 2)
            {
                ANSLIGear2Jawa.NSLI();
                ANSLIGear1Jawa.NSLIStop();
            //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearJawa.FSRStop();
            }
            else if (ivehicle == 3)
            {
                ANSLIGear2Bullet500.NSLI();
                ANSLIGear1Bullet500.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearBullet500.FSRStop();
            }
            itrate = 202;   // Gear 2 (200 + 2)
        }
        //Gear2 FSI1 Module
        if (gear == 2 && ivehicle == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 202 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 2 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear2.NFSI1();
                ANSLIGear2.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 302;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }

        //Gear 3 Module__________________________________________________________________________________________________

        if (trate > 0 && throttlediff > 0 && gear == 3 && (itrate == 202 || itrate == 302 || itrate == 49990))
        {
            Debug.Log("Inside Gear3SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if(ivehicle == 1) {
                if (iexhaust == 0)
                {
                    ANSLIGear3.NSLI();
                    ANSLIGear2.NSLIStop();
                    ANFSI1Gear2.NFSI1Stop();
                    ANSLIGear1.NSLIStop();
                    ANFSI1Gear1.NFSI1Stop();
                    ANFSRGear.FSRStop();
                }
            }
               else if (ivehicle == 2)
            {
                ANSLIGear3Jawa.NSLI();
                ANSLIGear2Jawa.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearJawa.FSRStop();
            }
            else if (ivehicle == 3)
            {
                ANSLIGear3Bullet500.NSLI();
                ANSLIGear2Bullet500.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearBullet500.FSRStop();
            }
            itrate = 203;   // Gear 2 (200 + 2)
        }

        if (gear == 3 && ivehicle == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 203 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 3 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear3.NFSI1();
                ANSLIGear3.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 303;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }


        //Gear 4 Module__________________________________________________________________________________________________

        if (trate > 0 && throttlediff > 0 && gear == 4 && (itrate == 203 || itrate == 303 || itrate == 49990))
        {
            Debug.Log("Inside Gear4SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (ivehicle == 1)
            {
                if (iexhaust == 0)
                {
                    ANSLIGear4.NSLI();
                    ANSLIGear3.NSLIStop();
                    ANFSI1Gear3.NFSI1Stop();
                    ANSLIGear2.NSLIStop();
                    //           ANFSI1Gea21.NFSI1Stop();
                    ANFSRGear.FSRStop();
                }
            }
            else if (ivehicle == 2)
            {
                ANSLIGear4Jawa.NSLI();
                ANSLIGear3Jawa.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearJawa.FSRStop();
            }
            else if (ivehicle == 3)
            {
                ANSLIGear4Bullet500.NSLI();
                ANSLIGear3Bullet500.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearBullet500.FSRStop();
            }
            itrate = 204;   // Gear 2 (200 + 2)
        }

        if (gear == 4 && ivehicle == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 204 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 4 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (iexhaust == 0)
            {
                //                Debug.Log("Entering FSI1 ex=0________________________________________");
                ANFSI1Gear4.NFSI1();
                ANSLIGear4.NSLIStop();
                ANFSRGear.FSRStop();
                AIdling.IdlingStop();


            }
            else if (iexhaust == 1)
            {
                //          ANFSI1Akra1.NFSI1();
                //          ANSLIAkra1.NSLIStop();
                //          ANFSRAkra1.FSRStop();
                //          ANFSR15000Akra1.NFSR15000Stop();
                //          ANBLIPC14000Akra1.BLIPC14000Stop();
                //          ANBLIPC10000Akra1.BLIPC10000Stop();
                //          ANBLIPC7500Akra1.BLIPC7500Stop();
                //          ANBLIPC6000Akra1.BLIPC6000Stop();
                //          ANBLIPC5500Akra1.BLIPC5500Stop();
                //          ANBLIPC4000Akra1.BLIPC4000Stop();
                //          ANBLIPC3000Akra1.BLIPC3000Stop();
            }

            if (iexhaust == 0)
            {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 304;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }


        }


        //Gear 5 Module__________________________________________________________________________________________________

        if (trate > 0 && throttlediff > 0 && gear == 5 && (itrate == 204 || itrate == 304 || itrate == 49990))
        {
            Debug.Log("Inside Gear4SLI ______________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (ivehicle == 1)
            {
                if (iexhaust == 0)
                {
                    ANSLIGear5.NSLI();
                    ANSLIGear4.NSLIStop();
                    ANFSI1Gear4.NFSI1Stop();
                    ANSLIGear3.NSLIStop();
                    //           ANFSI1Gea21.NFSI1Stop();
                    ANFSRGear.FSRStop();
                }
            }
            else if (ivehicle == 3)
            {
                Debug.Log("Entered Gear5 AudioEngine");
                ANSLIGear5Bullet500.NSLI();
                ANSLIGear4Bullet500.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                ANFSRGearBullet500.FSRStop();
            }
            itrate = 205;   // Gear 2 (200 + 2)
        }

        if (gear == 5 && ivehicle == 1 && ((trate > 4 || throttlediff > 4000 || rpm1 > 15500) && itrate == 205 && rpm1 < 12000))
        // above FSI1 starts after rpm > 45000//

        {
            Debug.Log("Gear 5 Entering FSI1_________________________________________");
            // Select NSLI Sound Stock/Aftermarket
            if (ivehicle == 1)
            {
                if (iexhaust == 0)
                {
                    //                Debug.Log("Entering FSI1 ex=0________________________________________");
                    ANFSI1Gear5.NFSI1();
                    ANSLIGear5.NSLIStop();
                    ANFSRGear.FSRStop();
                    AIdling.IdlingStop();


                }
                else if (iexhaust == 1)
                {
                    //          ANFSI1Akra1.NFSI1();
                    //          ANSLIAkra1.NSLIStop();
                    //          ANFSRAkra1.FSRStop();
                    //          ANFSR15000Akra1.NFSR15000Stop();
                    //          ANBLIPC14000Akra1.BLIPC14000Stop();
                    //          ANBLIPC10000Akra1.BLIPC10000Stop();
                    //          ANBLIPC7500Akra1.BLIPC7500Stop();
                    //          ANBLIPC6000Akra1.BLIPC6000Stop();
                    //          ANBLIPC5500Akra1.BLIPC5500Stop();
                    //          ANBLIPC4000Akra1.BLIPC4000Stop();
                    //          ANBLIPC3000Akra1.BLIPC3000Stop();
                }
            }
            else if (ivehicle == 3)
            {
                //   ANFSI1Gear5Bullet500.NFSI1();
          //      ANSLIGear5Bullet500.NSLIStop();
          //      ANFSRGear1Bullet500.FSRStop();
         //      AIdlingBullet500.IdlingStop();

            }

        //    if (iexhaust == 0)
          //  {


                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 305;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

       //     }


        }

        //Braking Module
        if(ibrake == 1 && itrate != -1000 && itrate != 1)
        {
            ABrakingBullet500.Braking();
            ANSLIGear5Bullet500.NSLIStop();
            ANSLIGear4Bullet500.NSLIStop();
            ANSLIGear3Bullet500.NSLIStop();
            ANSLIGear2Bullet500.NSLIStop();
            ANSLIGear1Bullet500.NSLIStop();
            //    ANFSI1Gear1.NFSI1Stop();
            ANFSRGearBullet500.FSRStop();
            ANRevLimiterGearBullet500.RevLimiterStop();
            itrate = -1000;  // This value for Braking
            ibrake = 0;
        }
        //Brake Release and FSR Start Module
        //Braking Module
        if (ibrake == -1 && itrate == -1000)
        {
            Debug.Log("************************************Brake Release");
            ABrakingBullet500.BrakingStop();
            itrate = -999;    //  Brake Release function
            StartCoroutine("StartFSRG1");

        }

            // FSR after SLI module
            if ((trate < -0.5 && throttlediff < 0) && (itrate == 200 || itrate == 300 || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305 || itrate == 100000) && rpm1 > 2500)
        {
            Debug.Log("INside FSR Gear");
            blipcinterval = 0;
            StartCoroutine("StartFSRG1");     // Enable FSR after Blipc ends

        }
        // Restart Idling
        if (gear > 0 && rpm1 <= (rpmidling + 500) && (itrate == 49990 || itrate == -1000))
        {
            //          Debug.Log("Pre Enter FadeoutSLR1 ***************************************8");

            //            iingearrestartidling = 0;

            gear = 0;
            //    itrate = 100;
            itrate = 1;

            irpm = 0;
            if (ivehicle == 1)
            {
                if (iexhaust == 0) AIdling.Idling();
                else if (iexhaust == 1) AIdlingAkra1.IdlingAkra1();
                ANFSRGear.FSRStop();
                ANFSRAkra1.FSRStop();
            } 
            else if (ivehicle == 2)
            {
                Debug.Log("Inside Jawa Idling");
                AIdlingJawa.Idling();
                ANFSRGearJawa.FSRStop();
            }
            else if (ivehicle == 3)
            {
                Debug.Log("Inside Jawa Idling");
                AIdlingBullet500.Idling();
                ANFSRGearBullet500.FSRStop();
                ABrakingBullet500.BrakingStop();
                bikespeed = 0;
            }
       //     ibrake = 1;  // EnableBlip Braking again after Vehicle Stops
            ifadeinidling = 1;
            StartCoroutine("FadeinIdling");
            //      StartCoroutine("StartIdling");
            //          ANFSR.FSRStop();
            rpm1 = rpmidling;
            //                ifadeoutnfsr = 1;
            //             fadeoutfsrtime = 0.01f;
            //         StartCoroutine("FadeoutNFSR");
           
            //          ANFSR15000.NFSR15000Stop();
            //          ANBLIPC14000.BLIPC14000Stop();
            //          ANBLIPC7500.BLIPC7500Stop();
            //          ANBLIPC6000.BLIPC6000Stop();
            //          ANBLIPC5500.BLIPC5500Stop();
            //          ANBLIPC4000.BLIPC4000Stop();
            //         ANBLIPC3000.BLIPC3000Stop();
            //     StartCoroutine("Setitrate1");

        }
        //InGear RevLimiter / Constant Top Speed
   //     if (gear > 0 && ((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 200 || itrate == 300) || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305) && rpm1 >= 14500)
            if (gear > 0 && ((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 200 || itrate == 300) || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305) && ((ivehicle == 1 && rpm1 >= 14500) || (ivehicle == 3 && bikespeed == 120)))

            //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
            {
                Debug.Log("INside RevLimited Gear1***************************************");
            itrate = 100000;  // pre rev limit Trate
                              //          EnableFSR15000afterRevlimiterStart();
            irevlimitercanstart = 0;
            ivibrating = 1;
            if (ivehicle == 1)
            {
                rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                if (iexhaust == 0)
                {
                    ANRevLimiterGear.RevLimiter();
                    ANSLIGear1.NSLIStop();
                    ANFSI1Gear1.NFSI1Stop();
                    ANSLIGear2.NSLIStop();
                    ANFSI1Gear2.NFSI1Stop();
                    ANSLIGear3.NSLIStop();
                    ANFSI1Gear3.NFSI1Stop();
                    ANSLIGear4.NSLIStop();
                    ANFSI1Gear4.NFSI1Stop();
                    ANSLIGear5.NSLIStop();
                    ANFSI1Gear5.NFSI1Stop();
                    //     ANFSI1Gear4.NFSI1Stop();
                    //      ANBLIP14000.Blip14000Stop();
                    //      ANBLIP.BlipStop();
                }
                else if (iexhaust == 1)
                {
                    ANRevLimiterAkra1.RevLimiter();
                    ANBLIP14000Akra1.Blip14000Stop();
                    ANBLIPAkra1.BlipStop();
                    ANFSI1Akra1.NFSI1Stop();  // Test
                    ANSLIAkra1.NSLIStop();  // Test
                }
            }
            else if (ivehicle == 3)
            {
               Debug.Log("INside RevLimited Gear1*************************************** Loop2");
                ANRevLimiterGearBullet500.RevLimiter();
                ANSLIGear5Bullet500.NSLIStop();
                bikespeed = 120f;

            }
                // Bullet 500

            // Mobile Vibration
            //            ivibrating = 1;
            //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
            //          Handheld.Vibrate();
            StartCoroutine("RevLimiterVibrationOnOff");

        }




        // RPM Console Update 
        RemoveRPMConsole();

        // Idling Pulsating Needle

        if (irpm == 0)
        {
            if (iidleneedlemoved == 0)
            {
                int randidle = Random.Range(0, 10);
                if (randidle >= 5)
                {
                    RPM1000.enabled = true;
                    RPM1100.enabled = false;
                }
                else if (randidle == 0)
                {
                    iidleneedlemoved = 1;
                    StartCoroutine("EnableIdleRPMNeedleMove");
                    //                   yield return new WaitForSeconds(0.25f);
                    RPM1100.enabled = true;
                    RPM1000.enabled = false;
                }
            }
        }
    
        if (rpm1 == 0) RPM0.enabled = true;
        if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
        if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
        if (rpm1 >= 1500 && rpm1 < 2000) RPM1500.enabled = true;
        if (rpm1 >= 2000 && rpm1 < 2200) RPM2000.enabled = true;
        if (rpm1 >= 2200 && rpm1 < 2500) RPM2200.enabled = true;
        if (rpm1 >= 2500 && rpm1 < 2800) RPM2500.enabled = true;
        if (rpm1 >= 2800 && rpm1 < 3000) RPM2800.enabled = true;
        if (rpm1 >= 3000 && rpm1 < 3200) RPM3000.enabled = true;
        if (rpm1 >= 3200 && rpm1 < 3500) RPM3500.enabled = true;
        if (rpm1 >= 3500 && rpm1 < 3800) RPM3800.enabled = true;
        if (rpm1 >= 3800 && rpm1 < 4000) RPM4000.enabled = true;
        if (rpm1 >= 4000 && rpm1 < 4200) RPM4200.enabled = true;
        if (rpm1 >= 4200 && rpm1 < 4500) RPM4500.enabled = true;
        if (rpm1 >= 4500 && rpm1 < 4800) RPM4800.enabled = true;
        if (rpm1 >= 4800 && rpm1 < 5000) RPM5000.enabled = true;
        if (rpm1 >= 5000 && rpm1 < 5200) RPM5200.enabled = true;
        if (rpm1 >= 5200 && rpm1 < 5500) RPM5500.enabled = true;
        if (rpm1 >= 5500 && rpm1 < 6000) RPM6000.enabled = true;
        if (rpm1 >= 6000 && rpm1 < 6500) RPM6500.enabled = true;
        if (rpm1 >= 6500 && rpm1 < 7000) RPM7000.enabled = true;
        if (rpm1 >= 7000 && rpm1 < 7500) RPM7500.enabled = true;
        if (rpm1 >= 7500 && rpm1 < 8000) RPM8000.enabled = true;
        if (rpm1 >= 8000 && rpm1 < 8500) RPM8500.enabled = true;
        if (rpm1 >= 8500 && rpm1 < 9000) RPM9000.enabled = true;
        if (rpm1 >= 9000 && rpm1 < 9500) RPM9500.enabled = true;
        if (rpm1 >= 9500 && rpm1 < 10000) RPM10000.enabled = true;
        if (rpm1 >= 10000 && rpm1 < 10500) RPM10500.enabled = true;
        if (rpm1 >= 10500 && rpm1 < 11000) RPM11000.enabled = true;
        if (rpm1 >= 11000 && rpm1 < 11500) RPM11500.enabled = true;
        if (rpm1 >= 11500 && rpm1 < 12000) RPM12000.enabled = true;
        if (rpm1 >= 12000 && rpm1 < 12500) RPM12500.enabled = true;
        if (rpm1 >= 12500 && rpm1 < 13000) RPM13000.enabled = true;
        if (rpm1 >= 13000 && rpm1 < 13500) RPM13500.enabled = true;
        if (rpm1 >= 13500 && rpm1 < 14000) RPM14000.enabled = true;
        if (rpm1 >= 14000 && rpm1 < 14500) RPM14500.enabled = true;
        if (rpm1 >= 14500 && rpm1 <= 15000) RPM15000.enabled = true;
        //         if (rpm1 > 14000) RedlineButton.enabled = true;
        else if (rpm1 < 14000) RedlineButton.enabled = false;

        // Console RPM Bars*****************************************************
        if (rpm1 > 1500 && irpm == 0)
        {
            //       ConsoleRPM500.SetActive(true);
            //      ConsoleRPM1000.SetActive(true);
            //      ConsoleRPM1100.SetActive(true);
        }

        if (rpm1 > 1500 && irpm != 0)
        {
            //      ConsoleRPM500.SetActive(true);
            //      ConsoleRPM1000.SetActive(true);
            //      ConsoleRPM1100.SetActive(true);
        }

        if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0)
        {
            //       ConsoleRPM500.SetActive(true);
            ConsoleRPM1000.SetActive(true);
        }
        if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0)
        {
            //             ConsoleRPM500.SetActive(true);
            ConsoleRPM1000.SetActive(true);
        }
        if (rpm1 == 0) ConsoleRPM0.SetActive(true);
        if (rpm1 >= 1500 && rpm1 < 2000) ConsoleRPM1500.SetActive(true);
        if (rpm1 >= 2000 && rpm1 < 2200) ConsoleRPM2000.SetActive(true);
        if (rpm1 >= 2200 && rpm1 < 2500) ConsoleRPM2200.SetActive(true);
        if (rpm1 >= 2500 && rpm1 < 2800) ConsoleRPM2500.SetActive(true);
        if (rpm1 >= 2800 && rpm1 < 3000) ConsoleRPM2800.SetActive(true);
        if (rpm1 >= 3000 && rpm1 < 3200) ConsoleRPM3000.SetActive(true);
        if (rpm1 >= 3200 && rpm1 < 3500) ConsoleRPM3500.SetActive(true);
        if (rpm1 >= 3500 && rpm1 < 3800) ConsoleRPM3800.SetActive(true);
        if (rpm1 >= 3800 && rpm1 < 4000) ConsoleRPM4000.SetActive(true);
        if (rpm1 >= 4000 && rpm1 < 4200) ConsoleRPM4200.SetActive(true);
        if (rpm1 >= 4200 && rpm1 < 4500) ConsoleRPM4500.SetActive(true);
        if (rpm1 >= 4500 && rpm1 < 4800) ConsoleRPM4800.SetActive(true);
        if (rpm1 >= 4800 && rpm1 < 5000) ConsoleRPM5000.SetActive(true);
        if (rpm1 >= 5000 && rpm1 < 5200) ConsoleRPM5200.SetActive(true);
        if (rpm1 >= 5200 && rpm1 < 5500) ConsoleRPM5500.SetActive(true);
        if (rpm1 >= 5500 && rpm1 < 6000) ConsoleRPM6000.SetActive(true);

        if (rpm1 >= 6000 && rpm1 < 6500) ConsoleRPM6500.SetActive(true);
        if (rpm1 >= 6500 && rpm1 < 7000) ConsoleRPM7000.SetActive(true);

        if (rpm1 >= 7000 && rpm1 < 7500) ConsoleRPM7500.SetActive(true);
        //      if(rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM7700.SetActive(true);
        if (rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM8000.SetActive(true);
        if (rpm1 >= 8000 && rpm1 < 8500) ConsoleRPM8500.SetActive(true);
        if (rpm1 >= 8500 && rpm1 < 9000) ConsoleRPM9000.SetActive(true);

        if (rpm1 >= 9000 && rpm1 < 9500) ConsoleRPM9500.SetActive(true);

        if (rpm1 >= 9500 && rpm1 < 10000) ConsoleRPM10000.SetActive(true);


        if (rpm1 >= 10000 && rpm1 < 10500) ConsoleRPM10500.SetActive(true);
        if (rpm1 >= 10500 && rpm1 < 11000) ConsoleRPM11000.SetActive(true);
        if (rpm1 >= 11000 && rpm1 < 11500) ConsoleRPM11500.SetActive(true);

        if (rpm1 >= 11500 && rpm1 < 12000) ConsoleRPM12000.SetActive(true);
        if (rpm1 >= 12000 && rpm1 < 12500) ConsoleRPM12500.SetActive(true);
        if (rpm1 >= 12500 && rpm1 < 13000) ConsoleRPM13000.SetActive(true);
        if (rpm1 >= 13000 && rpm1 < 13500) ConsoleRPM13500.SetActive(true);
        if (rpm1 >= 13500 && rpm1 < 14000) ConsoleRPM14000.SetActive(true);
        if (rpm1 >= 14000 && rpm1 < 14500) ConsoleRPM14500.SetActive(true);
        if (rpm1 >= 14500 && rpm1 <= 15000) ConsoleRPM15000.SetActive(true);


       


        //*************************************************************************************
        slidervalold = sliderval;  // Assign Slidervalue to Slidrold









    }
    // Coroutines***************************************

    // RPM Console Effects

    // Idling needle Enable Move
    private IEnumerator EnableIdleRPMNeedleMove()
    {
        yield return new WaitForSeconds(0.5f);
        iidleneedlemoved = 0;
    }

    IEnumerator Startistart1()
    {
        yield return new WaitForSeconds(0.5f);
        istart = 1;
    }



    // Start Idling
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(startertime); // wait half a secon


        AStarting.StartingStop();
    
        irpm = 0;
        rpm1 = rpmidling;
        if (ivehicle == 1)
        {
            if (iexhaust == 0) AIdling.Idling();
            if (iexhaust == 1) AIdlingAkra1.IdlingAkra1();
        }
        else if(ivehicle == 2)
        {
            AIdlingJawa.Idling();
        }
        else if (ivehicle == 3)
        {
            AIdlingBullet500.Idling();
        }
        // Red Console button Off
        RedButtonOn.enabled = false;
    }




    IEnumerator EnableBlip()
    {
        yield return new WaitForSeconds(0.2f);
        iblipcanstart = 1;
        //      Debug.Log("Inside Enable Blip");
    }

    // Enable Trate for Revlimiter after 0.1s of Revlimiter Start so Reducing waits for 0.1 s
    private IEnumerator EnableFSR15000afterRevlimiterStart()
    {
        yield return new WaitForSeconds(0.1f);
        itrate = 10000;
    }

    IEnumerator StartFSR()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
  //      if (itrate == 5001 || itrate == 2 || itrate == 3)  
            if (itrate == 5001 || itrate == 2 || itrate == 3 || (ivehicle == 2 && itrate == 10000) || (ivehicle == 3 && itrate == 10000))    // May 1 due to FSR not starting after RevLimiter
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
           //           Debug.Log("Inside FSR");
            ifadeinnfsr = 1;
            if (ivehicle == 1)
            {
                if (iexhaust == 0) ANFSR.FSR();
                else if (iexhaust == 1) ANFSRAkra1.FSR();
            }
            else if(ivehicle == 2)
            {
                ANFSRJawa.FSR();
                ANRevLimiterJawa.RevLimiterStop();
            }
            else if (ivehicle == 3)
            {
         //       Debug.Log("inside FSR Bullet");
                ANFSRBullet500.FSR();
                ANRevLimiterBullet500.RevLimiterStop();
            }
            StartCoroutine("FadeinNFSR");

            if (iPopCrackleX == 1 && rpm1 > 7000)
            {
                Debug.Log("Inside Blipc15000------------------------");
                ACrackle10000.Crackle10000();
                StartCoroutine("Explode");
            }

            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLI");
            StartCoroutine("FadeoutNFSI1");
            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;
            itrate = 4999;
        }


    }

    IEnumerator StartFSR7500()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            itrate = 4999;
            ANFSR7500.FSR7500();
            ANSLI.NSLIStop();
            //       iblipcanstart = 1;

        }


    }


    void FadeoutBLIP()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        //      while (ifadeoutblip == 1)
        //      {
        //          Debug.Log("Enter FadeoutSLR1***********************************2");
        //          yield return new WaitForSeconds(0.001f); // wait half a second

        ANBLIP.BLIPFadeout();
        //     }
    }

    IEnumerator Cancelinvoke()
    {


        yield return new WaitForSeconds(0.1f); // wait half a second
        CancelInvoke();

    }




    IEnumerator FadeinIdling()
    {
        //***************
        while (ifadeinidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (ivehicle == 1)
            {
                if (iexhaust == 0) AIdling.IdlingFadein();
                else if (iexhaust == 1)
                {
                    //            Debug.Log("INside RestartIdling Akra1_____________________________________");
                    AIdlingAkra1.IdlingAkra1Fadein();
                }
            }
            else if (ivehicle == 2)
            {
                AIdlingJawa.IdlingFadein();
            }
            else if (ivehicle == 3)
            {
                AIdlingBullet500.IdlingFadein();
            }
        }
    }

    IEnumerator FadeoutIdling()
    {
        //***************
        while (ifadeoutidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutidlingtime); // wait half a second
            if (ivehicle == 1)
            {
                if (iexhaust == 0) AIdling.IdlingFadeout();
                else if (iexhaust == 1) AIdlingAkra1.IdlingAkra1Fadeout();
            }
            else if (ivehicle == 2)
            {
                AIdlingJawa.IdlingFadeout();
            }
            else if (ivehicle == 3)
            {
                AIdlingBullet500.IdlingFadeout();
            }
        }
    }

    // Defunct************************
    IEnumerator FadeoutNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutfsrtime); // wait half a second
            ANFSR.NFSRFadeout();
            // Fadeout BlipFSRs
            ANFSR15000.BLIPCR15000Fadeout();
            ANBLIPC14000.BLIPCR14000Fadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
            ANBLIPC6000.BLIPCR6000Fadeout();
            ANBLIPC5500.BLIPCR5000Fadeout();
            ANBLIPC4000.BLIPCR4000Fadeout();
            ANBLIPC3000.BLIPCR3000Fadeout();
        }
    }

    IEnumerator FadeinNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (ivehicle == 1)
            {
                if (iexhaust == 0) ANFSR.NFSRFadein();
                else if (iexhaust == 1) ANFSRAkra1.NFSRFadein();
            }
            else if (ivehicle == 2)
            {
                ANFSRJawa.NFSRFadein();
            }
            else if (ivehicle == 3)
            {
                ANFSRBullet500.NFSRFadein();
            }
        }
    }

    IEnumerator FadeoutNSLI()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (ivehicle == 1)
            {
                if (iexhaust == 0) ANSLI.NSLIFadeout();
                else if (iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            else if (ivehicle == 2)
            {
                ANSLIJawa.NSLIFadeout();
            }
            else if (ivehicle == 3)
            {
                ANSLIBullet500.NSLIFadeout();
            }
        }
    }
    IEnumerator FadeoutNFSI1()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            //     ANFSI1.NFSI1Fadeout();
            if (ivehicle == 1)
            {
                if (iexhaust == 0) ANFSI1.NFSI1Fadeout();
                else if (iexhaust == 1) ANFSI1Akra1.NFSI1Fadeout();
            }
            else if (ivehicle == 2)
            {
          //      ANFSI1Jawa.NFSI1Fadeout();
            }
        }
    }


    // ***************************************************

    IEnumerator FadeoutBLIPCR7500()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
                                                    //           ANFSR.NFSRFadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
        }
    }
    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator RevLimiterVibrationOnOff()
    {
        if (ivibrator == 1) //Handheld.Vibrate();
            while (ivibrating == 1)
            {
                yield return new WaitForSeconds(0.5f);
                if (ivibrator == 1)
                {
                    //          Handheld.Vibrate();
                }
            }

    }

    // Keyin from Touch
    public void KeyInFromTouch()
    {
        //      moveSpeed = newSpeed;

        if (ikeyinfromtouch == 0)
        {
            ikeyinfromtouch = 1;
            // Headlight on Module
            Hlight.HeadlightOn();
            Rlight.RearlightOn();
            ConsoleRPM0.SetActive(true);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(false);
        }
        else if (ikeyinfromtouch == 2 && istartfromtouch != 0)
        {
            Debug.Log("Loop1");
            ikeyinfromtouch = 0;
            istartfromtouch = 3;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
            StartCoroutine("DelayRemoveConsoleRPM0");  //Due to inbility to remove Console RPM0 after directly shutting engine from key

        }
        else if (ikeyinfromtouch == 2)
        {
            ikeyinfromtouch = 0;
            //    Debug.Log("Loop2");
            RedButtonOn.enabled = false;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            ConsoleRPM0.SetActive(false);// COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);

        }




    }
    // StartStop from Touch
    //   public void StartStopFromTouch()
    //   {
    //      moveSpeed = newSpeed;
    //       if (ikeyinfromtouch == 2)
    //      {
    //          if (istartfromtouch == 0) istartfromtouch = 1;
    //          else if (istartfromtouch == 2) istartfromtouch = 3;
    //     }

    //  }
    // StartStop from Touch

    private IEnumerator DelayRemoveConsoleRPM0()
    {
        yield return new WaitForSeconds(0.2f);
        ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
    }

    public void StartStopFromTouch()
    {
        //      moveSpeed = newSpeed;
        if (ikeyinfromtouch == 2)
        {
            if (istartfromtouch == 0) istartfromtouch = 1;
            else if (istartfromtouch == 2) istartfromtouch = 3;
        }
        // Make StartStopButton Dark
        StartStopButton.enabled = false;
        StartStopButtonPressed.enabled = true;
        //       StartCoroutine("StartStopButtonAppear");
    }
    // ReAppear StartStopButton(Original)


    // ReAppear StartStopButton(Original)
    public void Start1StopButtonAppear()  // 
    {

        StartStopButton.enabled = true;
        StartStopButtonPressed.enabled = false;

    }

  
    // Braking Sub
    public void BrakeFromTouch()
    {
        Debug.Log("Brakefromtouchloop 1");

        if(ibrakingloop  == 0) ibrake = 1;
        Debug.Log("Brakefromtouchloop 2");
        ibrakingloop ++;
        }

    public void BrakeReleaseFromTouch()
    {

        Debug.Log("_______________________________________________Brake Release");
            ibrake = -1;
        ibrakingloop = 0;
    }

    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        ReverbXoneMix = 0.00f;
  //      RoadView.SetActive(true); // Appearance of Road
  //      GarageView.SetActive(false); // Disapperance of Garage
  //      TunnelView.SetActive(false); // DisAppearance of Tunnel

  //      HeadlightReflectionR.SetActive(false);
  //      HeadlightReflectionL.SetActive(false);

        AIdling.IdlingReverbRoad();
        AStartup.StartupReverbRoad();
        AStarting.StartingReverbRoad();
        AShutting.ShuttingReverbRoad();
        ANRevLimiter.RevLimiterReverbRoad();
        //ANFSR.NFSRReverbRoad();
        ANSLI.NSLIReverbRoad();
        ANFSI1.NFSI1ReverbRoad();
        //   ANFSI2.NFSI2ReverbRoad();

        ANBLIP2000.Blip2000ReverbRoad();
        ANBLIP3000.Blip3000ReverbRoad();
        ANBLIP4000.Blip4000ReverbRoad();
        ANBLIP5000.Blip5000ReverbRoad();
        ANBLIP6000.Blip6000ReverbRoad();
        ANBLIP7000.Blip7000ReverbRoad();
        ANBLIP10000.Blip10000ReverbRoad();
        ANBLIP14000.Blip14000ReverbRoad();

        ANBLIPC3000.Blipc3000ReverbRoad();
        ANBLIPC4000.Blipc4000ReverbRoad();
        ANBLIPC5500.Blipc5000ReverbRoad();
        ANBLIPC6000.Blipc6000ReverbRoad();
        ANBLIPC7500.Blipc7500ReverbRoad();
        ANBLIPC10000.Blipc10000ReverbRoad();
        ANBLIPC14000.Blipc14000ReverbRoad();
        ANFSR15000.NFSR15000ReverbRoad();
        ANFSR.NFSRReverbRoad();
        // Akra 1
        AIdlingAkra1.IdlingAkra1ReverbRoad();
        ANRevLimiterAkra1.RevLimiterAkra1ReverbRoad();
        ANSLIAkra1.NSLIAkra1ReverbRoad();
        ANFSI1Akra1.NFSI1Akra1ReverbRoad();
        //   ANFSI2.NFSI2ReverbRoad();
        ANBLIP2000Akra1.Blip2000Akra1ReverbRoad();
        ANBLIP3000Akra1.Blip3000Akra1ReverbRoad();
        ANBLIP4000Akra1.Blip4000Akra1ReverbRoad();
        ANBLIP5000Akra1.Blip5000Akra1ReverbRoad();
        ANBLIP6000Akra1.Blip6000Akra1ReverbRoad();
        ANBLIP7000Akra1.Blip7000Akra1ReverbRoad();
        ANBLIP10000Akra1.Blip10000Akra1ReverbRoad();
        ANBLIP14000Akra1.Blip14000Akra1ReverbRoad();


        ANBLIPC3000Akra1.Blipc3000Akra1ReverbRoad();
        ANBLIPC4000Akra1.Blipc4000Akra1ReverbRoad();
        ANBLIPC5500Akra1.Blipc5000Akra1ReverbRoad();
        ANBLIPC6000Akra1.Blipc6000Akra1ReverbRoad();
        ANBLIPC7500Akra1.Blipc7500Akra1ReverbRoad();
        ANBLIPC10000Akra1.Blipc10000Akra1ReverbRoad();
        ANBLIPC14000Akra1.Blipc14000Akra1ReverbRoad();
        ANFSR15000Akra1.NFSR15000Akra1ReverbRoad();
        ANFSRAkra1.NFSRAkra1ReverbRoad();

        //Bullet 500
        AIdlingBullet500.IdlingReverbRoad();
        //   AStartupBullet500.StartupReverbGarage();
        AStartingBullet500.StartingReverbRoad();
        //   AShuttingBullet500.ShuttingReverbGarage();
        ANRevLimiterBullet500.RevLimiterReverbRoad();
        // ANFSR.NFSRReverbGarage();
        ANSLIBullet500.NSLIReverbRoad();
        //    ANFSI1Bullet500.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();
        ANSLIGear1Bullet500.NSLIReverbRoad();
        ANSLIGear2Bullet500.NSLIReverbRoad();
        ANSLIGear3Bullet500.NSLIReverbRoad();
        ANSLIGear4Bullet500.NSLIReverbRoad();
        ANSLIGear5Bullet500.NSLIReverbRoad();
        ANFSRGearBullet500.NFSRReverbRoad();
        ANRevLimiterGearBullet500.RevLimiterReverbRoad();
        ANFSRBullet500.NFSRReverbRoad();



    //    ACrackle10000.Crackle10000ReverbRoad();
    //    ACrackle8000.Crackle8000ReverbRoad();

    }



    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {
        ReverbXoneMix = 1.03f;
        RoadView.SetActive(false); // DisAppearance of Road
        TunnelView.SetActive(false); // DisAppearance of Tunnel
        GarageView.SetActive(true); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000.Blip2000ReverbGarage();
        ANBLIP3000.Blip3000ReverbGarage();
        ANBLIP4000.Blip4000ReverbGarage();
        ANBLIP5000.Blip5000ReverbGarage();
        ANBLIP6000.Blip6000ReverbGarage();
        ANBLIP7000.Blip7000ReverbGarage();
        ANBLIP10000.Blip10000ReverbGarage();
        ANBLIP14000.Blip14000ReverbGarage();

        ANBLIPC3000.Blipc3000ReverbGarage();
        ANBLIPC4000.Blipc4000ReverbGarage();
        ANBLIPC5500.Blipc5000ReverbGarage();
        ANBLIPC6000.Blipc6000ReverbGarage();
        ANBLIPC7500.Blipc7500ReverbGarage();
        ANBLIPC10000.Blipc10000ReverbGarage();
        ANBLIPC14000.Blipc14000ReverbGarage();
        ANFSR15000.NFSR15000ReverbGarage();
        ANFSR.NFSRReverbGarage();

        // Akra1
        AIdlingAkra1.IdlingAkra1ReverbGarage();
        ANRevLimiterAkra1.RevLimiterAkra1ReverbGarage();
        ANSLIAkra1.NSLIAkra1ReverbGarage();
        ANFSI1Akra1.NFSI1Akra1ReverbGarage();
        //   ANFSI2.NFSI2ReverbRoad();
        ANBLIP2000Akra1.Blip2000Akra1ReverbGarage();
        ANBLIP3000Akra1.Blip3000Akra1ReverbGarage();
        ANBLIP4000Akra1.Blip4000Akra1ReverbGarage();
        ANBLIP5000Akra1.Blip5000Akra1ReverbGarage();
        ANBLIP6000Akra1.Blip6000Akra1ReverbGarage();
        ANBLIP7000Akra1.Blip7000Akra1ReverbGarage();
        ANBLIP10000Akra1.Blip10000Akra1ReverbGarage();
        ANBLIP14000Akra1.Blip14000Akra1ReverbGarage();

        ANBLIPC3000Akra1.Blipc3000Akra1ReverbGarage();
        ANBLIPC4000Akra1.Blipc4000Akra1ReverbGarage();
        ANBLIPC5500Akra1.Blipc5000Akra1ReverbGarage();
        ANBLIPC6000Akra1.Blipc6000Akra1ReverbGarage();
        ANBLIPC7500Akra1.Blipc7500Akra1ReverbGarage();
        ANBLIPC10000Akra1.Blipc10000Akra1ReverbGarage();
        ANBLIPC14000Akra1.Blipc14000Akra1ReverbGarage();
        ANFSR15000Akra1.NFSR15000Akra1ReverbGarage();
        ANFSRAkra1.NFSRAkra1ReverbGarage();

        ACrackle10000.Crackle10000ReverbGarage();
        ACrackle8000.Crackle8000ReverbGarage();
    }


    // DisAppearance of Racetrack Road/Garage
    public void TunnelAppear()  // 
    {
   //     ReverbXoneMix = 1.1f;
   //     TunnelView.SetActive(true); // Disapperance of Garage
   //     RoadView.SetActive(false); // Appearance of Road
   //     GarageView.SetActive(false); // Disapperance of Garage

//        HeadlightReflectionR.SetActive(true);
 //       HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000.Blip2000ReverbGarage();
        ANBLIP3000.Blip3000ReverbGarage();
        ANBLIP4000.Blip4000ReverbGarage();
        ANBLIP5000.Blip5000ReverbGarage();
        ANBLIP6000.Blip6000ReverbGarage();
        ANBLIP7000.Blip7000ReverbGarage();
        ANBLIP10000.Blip10000ReverbGarage();
        ANBLIP14000.Blip14000ReverbGarage();

        ANBLIPC3000.Blipc3000ReverbGarage();
        ANBLIPC4000.Blipc4000ReverbGarage();
        ANBLIPC5500.Blipc5000ReverbGarage();
        ANBLIPC6000.Blipc6000ReverbGarage();
        ANBLIPC7500.Blipc7500ReverbGarage();
        ANBLIPC10000.Blipc10000ReverbGarage();
        ANBLIPC14000.Blipc14000ReverbGarage();
        ANFSR15000.NFSR15000ReverbGarage();
        ANFSR.NFSRReverbGarage();

        // Akra1
        AIdlingAkra1.IdlingAkra1ReverbGarage();
        ANRevLimiterAkra1.RevLimiterAkra1ReverbGarage();
        ANSLIAkra1.NSLIAkra1ReverbGarage();
        ANFSI1Akra1.NFSI1Akra1ReverbGarage();
        //   ANFSI2.NFSI2ReverbRoad();
        ANBLIP2000Akra1.Blip2000Akra1ReverbGarage();
        ANBLIP3000Akra1.Blip3000Akra1ReverbGarage();
        ANBLIP4000Akra1.Blip4000Akra1ReverbGarage();
        ANBLIP5000Akra1.Blip5000Akra1ReverbGarage();
        ANBLIP6000Akra1.Blip6000Akra1ReverbGarage();
        ANBLIP7000Akra1.Blip7000Akra1ReverbGarage();
        ANBLIP10000Akra1.Blip10000Akra1ReverbGarage();
        ANBLIP14000Akra1.Blip14000Akra1ReverbGarage();

        ANBLIPC3000Akra1.Blipc3000Akra1ReverbGarage();
        ANBLIPC4000Akra1.Blipc4000Akra1ReverbGarage();
        ANBLIPC5500Akra1.Blipc5000Akra1ReverbGarage();
        ANBLIPC6000Akra1.Blipc6000Akra1ReverbGarage();
        ANBLIPC7500Akra1.Blipc7500Akra1ReverbGarage();
        ANBLIPC10000Akra1.Blipc10000Akra1ReverbGarage();
        ANBLIPC14000Akra1.Blipc14000Akra1ReverbGarage();
        ANFSR15000Akra1.NFSR15000Akra1ReverbGarage();
        ANFSRAkra1.NFSRAkra1ReverbGarage();


        //Bullet 500
        AIdlingBullet500.IdlingReverbGarage();
     //   AStartupBullet500.StartupReverbGarage();
        AStartingBullet500.StartingReverbGarage();
        //   AShuttingBullet500.ShuttingReverbGarage();
        ANRevLimiterBullet500.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLIBullet500.NSLIReverbGarage();
        //    ANFSI1Bullet500.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();
        ANSLIGear1Bullet500.NSLIReverbGarage();
        ANSLIGear2Bullet500.NSLIReverbGarage();
        ANSLIGear3Bullet500.NSLIReverbGarage();
        ANSLIGear4Bullet500.NSLIReverbGarage();
        ANSLIGear5Bullet500.NSLIReverbGarage();
        ANFSRGearBullet500.NFSRReverbGarage();
        ANRevLimiterGearBullet500.RevLimiterReverbGarage();
        ANFSRBullet500.NFSRReverbGarage();


   //     ACrackle10000.Crackle10000ReverbGarage();
   //     ACrackle8000.Crackle8000ReverbGarage();

    }



    // Fn to Select Exhaust Pipe : 0- Stock, 1- Akra1
    public void ExhaustMode()  // Defunct Not used as Default Exhaust Icon is Stock and it doesn't drop down on clicking Exhaust Button
    {
        if (istartfromtouch != 2)  // Only works when Engine is off
        {
            if (exhaustDropDown == 0)
            {
                ExhaustStockbutton.SetActive(true);
                ExhaustAkra1button.SetActive(true);
                exhaustDropDown = 1;
                //       istartfromtouch = 3;  // Shutting Down when Exhaust Menu Drops Down
                //        StartStopFromTouch();
                // Camera Position
                //       Camera.transform.Translate(0, -5, 0);
                //        Camera.transform.Rotate(0, 20, 0);

            }
            else if (exhaustDropDown == 1)
            {
                ExhaustStockbutton.SetActive(false);
                ExhaustAkra1button.SetActive(false);
                exhaustDropDown = 0;
            }
            if (iexhaust == 0)
            {

                //        iexhaust = 1;
            }
            else if (iexhaust == 1)
            {
                //    iexhaust = 0;
            }
            //      Exhaustbutton.image.overrideSprite = blockA_disable;
        }
    }

    public void ExhaustStock()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            iexhaust = 0;
            //       exhaustDropDown = 0;
            StockMuffler.SetActive(true); // Disapperance of Stovk Muffler
            StockExhaustTick.SetActive(true); // Tick on Stock Ex Panel
            Akra1ExhaustTick.SetActive(false); // Tick vanish on Akra1 Ex Panel
            Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel

            StockMufflerShield.SetActive(true); // Disapperance of Stovk Muffler Shield
            AkraCarbonMuffler.SetActive(false); // Disapperance of Akra Carbon Muffler                                   // Akra1Muffler.SetActive(false); // Disapperance of Stovk Muffler
            AkraBlackMuffler.SetActive(false); // Disapperance of Stovk Muffler
            StockMufflerSoundSource.SetActive(true);  //Activate Akra1 Sound Sources
            Akra1MufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
        }

    }
    public void ExhaustAkra1()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            if (ZX10ExhaustAkra1 == 1)  // Only Activate when Akra1 Purchased
            {
                iexhaust = 1;
                //     exhaustDropDown = 0;
                StockExhaustTick.SetActive(false); // Disapperance of Stovk Muffler
                Akra1ExhaustTick.SetActive(true); // Tick on Stock Ex Panel
                Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel


                //     ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
                StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler Shield
                                                     //   Akra1Muffler.SetActive(true); // Disapperance of Stovk Muffler
                AkraCarbonMuffler.SetActive(false); // Disapperance of Stovk Muffler
                AkraBlackMuffler.SetActive(true); // Disapperance of Stovk Muffler
                StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
                Akra1MufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
            }
        }
    }

    public void ExhaustAkra1Carbon()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            if (ZX10ExhaustTBR1 == 1)  // Only Activate when Akra1 Purchased
            {
                iexhaust = 1;
                //     exhaustDropDown = 0;
                StockExhaustTick.SetActive(false); // Disapperance of Stovk Muffler
                Akra1CarbonExhaustTick.SetActive(true); // Tick on Stock Ex Panel
                Akra1ExhaustTick.SetActive(false); // Tick on Stock Ex Panel

                //     ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                AkraBlackMuffler.SetActive(false); // Disapperance of Akra1 Muffler
                StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
                StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler Shield
                                                     //   Akra1Muffler.SetActive(true); // Disapperance of Stovk Muffler
                AkraCarbonMuffler.SetActive(true); // Disapperance of Stovk Muffler
                StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
                Akra1MufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
            }
        }
    }




    public void MoveRPMConsole1()
    {
        if (iRPMConsole == 0)
        {
            RPMConsole1.transform.position = new Vector3(1650, 740, 0);
            RPMConsole1.transform.localScale += new Vector3(15, 5, 0);

            //    RPMConsole1.SetActive(false);
            iRPMConsole = 1;
        }
        else if (iRPMConsole == 1)
        {
            //     RPMConsole1.SetActive(true);
            RPMConsole1.transform.localScale += new Vector3(-15, -5, 0);
            RPMConsole1.transform.position = new Vector3(1060, 130, 0);
            iRPMConsole = 0;
        }
    }
    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        sliderval = sliderposition;

    }

    // GUI Events Functions
    public void VibratorOnOff()
    {
        if (ivibrator == 0) ivibrator = 1;
        else if (ivibrator == 1) ivibrator = 0;

    }


    // Console Startup Sequence************************************************************
    int rpmi = 1000;   // Counter for rpm change for Console Startup Sequence
                       //   float wait=0.1;
    private IEnumerator ConsoleStartup()
    {
        yield return new WaitForSeconds(1f);
        while (iconsolestartup == 1 || iconsolestartup == -1)
        {
            yield return new WaitForSeconds(0.01f); // wait half a second

            RemoveRPMConsole();
            //     if (rpmi <= 7400)
            //     {
            if (iconsolestartup == 1 && rpmi <= 14500)
            {
                rpmi = rpmi + iconsolestartuprate * 2;
            }
            else iconsolestartup = -1;
            if (iconsolestartup == -1 && rpmi >= 1000)
            {
                rpmi = rpmi - iconsolestartuprate * 2;
                if (rpmi <= 900)
                {
                    //              RPM0.enabled = true;
                    rpmi = 0;
                    iconsolestartup = 0;
                }
            }

            if (rpmi < 900) RPM0.enabled = true;
            if (rpmi >= 900 && rpmi < 1500) RPM1000.enabled = true;
            if (rpmi >= 1500 && rpmi < 2000) RPM1500.enabled = true;
            if (rpmi >= 2000 && rpmi < 2200) RPM2000.enabled = true;
            if (rpmi >= 2200 && rpmi < 2500) RPM2200.enabled = true;
            if (rpmi >= 2500 && rpmi < 2800) RPM2500.enabled = true;
            if (rpmi >= 2800 && rpmi < 3000) RPM2800.enabled = true;
            if (rpmi >= 3000 && rpmi < 3200) RPM3000.enabled = true;
            if (rpmi >= 3200 && rpmi < 3500) RPM3500.enabled = true;
            if (rpmi >= 3500 && rpmi < 3800) RPM3800.enabled = true;
            if (rpmi >= 3800 && rpmi < 4000) RPM4000.enabled = true;
            if (rpmi >= 4000 && rpmi < 4200) RPM4200.enabled = true;
            if (rpmi >= 4200 && rpmi < 4500) RPM4500.enabled = true;
            if (rpmi >= 4500 && rpmi < 4800) RPM4800.enabled = true;
            if (rpmi >= 4800 && rpmi < 5000) RPM5000.enabled = true;
            if (rpmi >= 5000 && rpmi < 5200) RPM5200.enabled = true;
            if (rpmi >= 5200 && rpmi < 5500) RPM5500.enabled = true;
            if (rpmi >= 5500 && rpmi < 6000) RPM6000.enabled = true;
            if (rpmi >= 6000 && rpmi < 6500) RPM6500.enabled = true;
            if (rpmi >= 6500 && rpmi < 7000) RPM7000.enabled = true;
            if (rpmi >= 7000 && rpmi < 7500) RPM7500.enabled = true;
            if (rpmi >= 7500 && rpmi < 8000) RPM8000.enabled = true;
            if (rpmi >= 8000 && rpmi < 8500) RPM8500.enabled = true;
            if (rpmi >= 8500 && rpmi < 9000) RPM9000.enabled = true;
            if (rpmi >= 9000 && rpmi < 9500) RPM9500.enabled = true;
            if (rpmi >= 9500 && rpmi < 10000) RPM10000.enabled = true;
            if (rpmi >= 10000 && rpmi < 10500) RPM10500.enabled = true;
            if (rpmi >= 10500 && rpmi < 11000) RPM11000.enabled = true;
            if (rpmi >= 11000 && rpmi < 11500) RPM11500.enabled = true;
            if (rpmi >= 11500 && rpmi < 12000) RPM12000.enabled = true;
            if (rpmi >= 12000 && rpmi < 12500) RPM12500.enabled = true;
            if (rpmi >= 12500 && rpmi < 13000) RPM13000.enabled = true;
            if (rpmi >= 13000 && rpmi < 13500) RPM13500.enabled = true;
            if (rpmi >= 13500 && rpmi < 14000) RPM14000.enabled = true;
            if (rpmi >= 14000 && rpmi < 14500) RPM14500.enabled = true;
            if (rpmi >= 14500 && rpmi < 15000) RPM15000.enabled = true;


            // Console RPM Bars*****************************************************


            if (rpmi < 900)
            {
         //       Debug.Log("Activating Console 0 RPM");
                ConsoleRPM0.SetActive(true);
            }
            if (rpmi >= 900 && rpmi < 1500) ConsoleRPM1000.SetActive(true);
            if (rpmi >= 1500 && rpmi < 2000) ConsoleRPM1500.SetActive(true);

            if (rpmi >= 2000 && rpmi < 2200) ConsoleRPM2000.SetActive(true);
            if (rpmi >= 2200 && rpmi < 2500) ConsoleRPM2200.SetActive(true);
            if (rpmi >= 2500 && rpmi < 2800) ConsoleRPM2500.SetActive(true);
            if (rpmi >= 2800 && rpmi < 3000) ConsoleRPM2800.SetActive(true);
            if (rpmi >= 3000 && rpmi < 3200) ConsoleRPM3000.SetActive(true);
            if (rpmi >= 3200 && rpmi < 3500) ConsoleRPM3500.SetActive(true);
            if (rpmi >= 3500 && rpmi < 3800) ConsoleRPM3800.SetActive(true);
            if (rpmi >= 3800 && rpmi < 4000) ConsoleRPM4000.SetActive(true);
            if (rpmi >= 4000 && rpmi < 4200) ConsoleRPM4200.SetActive(true);
            if (rpmi >= 4200 && rpmi < 4500) ConsoleRPM4500.SetActive(true);
            if (rpmi >= 4500 && rpmi < 4800) ConsoleRPM4800.SetActive(true);
            if (rpmi >= 4800 && rpmi < 5000) ConsoleRPM5000.SetActive(true);
            if (rpmi >= 5000 && rpmi < 5200) ConsoleRPM5200.SetActive(true);
            if (rpmi >= 5200 && rpmi < 5500) ConsoleRPM5500.SetActive(true);
            if (rpmi >= 5500 && rpmi < 6000) ConsoleRPM6000.SetActive(true);

            if (rpmi >= 6000 && rpmi < 6500) ConsoleRPM6500.SetActive(true);
            if (rpmi >= 6500 && rpmi < 7000) ConsoleRPM7000.SetActive(true);

            if (rpmi >= 7000 && rpmi < 7500) ConsoleRPM7500.SetActive(true);
            //      if(rpmi >= 7500 && rpmi < 8000) ConsoleRPM7700.SetActive(true);
            if (rpmi >= 7500 && rpmi < 8000) ConsoleRPM8000.SetActive(true);
            if (rpmi >= 8000 && rpmi < 8500) ConsoleRPM8500.SetActive(true);
            if (rpmi >= 8500 && rpmi < 9000) ConsoleRPM9000.SetActive(true);

            if (rpmi >= 9000 && rpmi < 9500) ConsoleRPM9500.SetActive(true);

            if (rpmi >= 9500 && rpmi < 10000) ConsoleRPM10000.SetActive(true);


            if (rpmi >= 10000 && rpmi < 10500) ConsoleRPM10500.SetActive(true);
            if (rpmi >= 10500 && rpmi < 11000) ConsoleRPM11000.SetActive(true);
            if (rpmi >= 11000 && rpmi < 11500) ConsoleRPM11500.SetActive(true);

            if (rpmi >= 11500 && rpmi < 12000) ConsoleRPM12000.SetActive(true);
            if (rpmi >= 12000 && rpmi < 12500) ConsoleRPM12500.SetActive(true);
            if (rpmi >= 12500 && rpmi < 13000) ConsoleRPM13000.SetActive(true);
            if (rpmi >= 13000 && rpmi < 13500) ConsoleRPM13500.SetActive(true);
            if (rpmi >= 13500 && rpmi < 14000) ConsoleRPM14000.SetActive(true);
            if (rpmi >= 14000 && rpmi < 14500) ConsoleRPM14500.SetActive(true);
            if (rpmi >= 14500 && rpmi < 15000) ConsoleRPM15000.SetActive(true);





        }
        //      newImage = Image.FromFile(@"C:/User Data/VS/Software/Bass/Combined-Project/Blank-Application/Images/" + rpmString + ".png");
    }

    // Console RPM Remove
    public void RemoveRPMConsole()
    {
        RPM0.enabled = false;
       RPM1000.enabled = false;
        RPM1100.enabled = false;
        RPM1500.enabled = false;
        RPM2000.enabled = false;
        RPM2200.enabled = false;
        RPM2500.enabled = false;
        RPM2800.enabled = false;
        RPM3000.enabled = false;
        RPM3200.enabled = false;
        RPM3500.enabled = false;
        RPM3800.enabled = false;
        RPM4000.enabled = false;
        RPM4200.enabled = false;
        RPM4500.enabled = false;
        RPM4800.enabled = false;
        RPM5000.enabled = false;
        RPM5200.enabled = false;
        RPM5500.enabled = false;
        RPM6000.enabled = false;
        RPM6500.enabled = false;
        RPM7000.enabled = false;
        RPM7500.enabled = false;

        RPM8000.enabled = false;
        RPM8500.enabled = false;
        RPM9000.enabled = false;
        RPM9500.enabled = false;
        RPM10000.enabled = false;
        RPM10500.enabled = false;
        RPM11000.enabled = false;
        RPM11500.enabled = false;
        RPM12000.enabled = false;
        RPM12500.enabled = false;
        RPM13000.enabled = false;
        RPM13500.enabled = false;
        RPM14000.enabled = false;
        RPM14500.enabled = false;
        RPM15000.enabled = false;


        // RPM Console Section


        //        for (int irpmconsolexxx = 0; irpmconsolexxx < 15000; irpmconsolexxx = irpmconsolexxx + 100)
        //        {
        //            string name = $"ConsoleRPM " + "rpm1";
        //          ConsoleRPM500.name = "name";
        //          ConsoleRPM500.SetActive(false);
        //      }


        // Console View Bars
        //     ConsoleRPMOff.SetActive(false);
     
        ConsoleRPM0.SetActive(false);
        //      ConsoleRPM500.SetActive(false);      
        ConsoleRPM1000.SetActive(false);
        ConsoleRPM1500.SetActive(false);
        ConsoleRPM2000.SetActive(false);
        ConsoleRPM2200.SetActive(false);
        ConsoleRPM2500.SetActive(false);
        ConsoleRPM2800.SetActive(false);
        ConsoleRPM3000.SetActive(false);
        ConsoleRPM3200.SetActive(false);
        ConsoleRPM3500.SetActive(false);
        ConsoleRPM3800.SetActive(false);
        ConsoleRPM4000.SetActive(false);
        ConsoleRPM4200.SetActive(false);
        ConsoleRPM4500.SetActive(false);
        ConsoleRPM4800.SetActive(false);
        ConsoleRPM5000.SetActive(false);
        ConsoleRPM5200.SetActive(false);
        ConsoleRPM5500.SetActive(false);
        ConsoleRPM6000.SetActive(false);
        ConsoleRPM6500.SetActive(false);
        ConsoleRPM7000.SetActive(false);
        ConsoleRPM7500.SetActive(false);
        ConsoleRPM7700.SetActive(false);
        ConsoleRPM8000.SetActive(false);
        ConsoleRPM8200.SetActive(false);
        ConsoleRPM8500.SetActive(false);
        ConsoleRPM8800.SetActive(false);
        ConsoleRPM9000.SetActive(false);
        ConsoleRPM9200.SetActive(false);
        ConsoleRPM9500.SetActive(false);
        ConsoleRPM9800.SetActive(false);
        ConsoleRPM10000.SetActive(false);
        ConsoleRPM10200.SetActive(false);
        ConsoleRPM10500.SetActive(false);
        ConsoleRPM10800.SetActive(false);
        ConsoleRPM11000.SetActive(false);
        ConsoleRPM11200.SetActive(false);
        ConsoleRPM11500.SetActive(false);
        ConsoleRPM11800.SetActive(false);
        ConsoleRPM12000.SetActive(false);
        ConsoleRPM12200.SetActive(false);
        ConsoleRPM12500.SetActive(false);
        ConsoleRPM12800.SetActive(false);
        ConsoleRPM13000.SetActive(false);
        ConsoleRPM13200.SetActive(false);
        ConsoleRPM13500.SetActive(false);
        ConsoleRPM13800.SetActive(false);
        ConsoleRPM14000.SetActive(false);
        ConsoleRPM14200.SetActive(false);
        ConsoleRPM14500.SetActive(false);
        ConsoleRPM14800.SetActive(false);
        ConsoleRPM15000.SetActive(false);







        //     ConsoleRPM1100.SetActive(false);
        //     ConsoleRPM1500.SetActive(false);
        //     ConsoleRPM2000.SetActive(false);
        //     ConsoleRPM2200.SetActive(false);
        //     ConsoleRPM2500.SetActive(false);
        //     ConsoleRPM2800.SetActive(false);
        //     ConsoleRPM3000.SetActive(false);


    }

    // Exhaust Pop/Backfire
    void Explode()
    {
        AVExhaustPop.ExhaustPop();
    }

    public void PopCrackleEnable()  // Enable/Disable Pop Crackles
    {
        if (iPopCrackleX == 0)
        {
            iPopCrackleX = 1;
            PopsCrackles1Tick.SetActive(true);


        }
        else if (iPopCrackleX == 1)
        {
            iPopCrackleX = 0;
            PopsCrackles1Tick.SetActive(false);
        }
    }
    //******* In Gear Routines
    // Keyin from Touch
    public void GearUp()
    {

        gear = gear + 1;

        if (gear == 1) itrate = 100;    // To enable Gear1 SLI
        else if (gear == 2)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGear.RevLimiterStop();
                    
                }
                else if (ivehicle ==2)
                {
                    ANFSRGearJawa.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                  ANRevLimiterGearJawa.RevLimiterStop();
                }
                else if (ivehicle == 3)
                {
                    ANFSRGearBullet500.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGearBullet500.RevLimiterStop();
                }
                StartCoroutine("FadeinNFSRG");

                // Debug.Log("gear+1=22");
            }

            if (itrate == 200 || itrate == 300)
            {

                itrate = 202;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0)
                    {
                        ANSLIGear2.NSLIGearChange();
                        ANSLIGear1.NSLIStop();
                        ANFSI1Gear1.NFSI1Stop();
                    }
                }
                else if(ivehicle == 2) { 
                ANSLIGear2Jawa.NSLIGearChange();
                    ANSLIGear1Jawa.NSLIStop();
               //     ANFSI1Gear1.NFSI1Stop();
                }
                else if (ivehicle == 3)
                {
                    ANSLIGear2Bullet500.NSLIGearChange();
                    ANSLIGear1Bullet500.NSLIStop();
                    //     ANFSI1Gear1.NFSI1Stop();
                }

            }
        }
        else if (gear == 3)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGear.RevLimiterStop();
                }
                else if (ivehicle == 2 )
                {
                    ANFSRGearJawa.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGearJawa.RevLimiterStop();
                }
                else if (ivehicle == 3)
                {
                    ANFSRGearBullet500.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGearBullet500.RevLimiterStop();
                }
                StartCoroutine("FadeinNFSRG");
                

                // Debug.Log("gear+1=22");
            }

            if (itrate == 202 || itrate == 302)
            {

                itrate = 203;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0)
                    {
                        ANSLIGear3.NSLIGearChange();
                        ANSLIGear2.NSLIStop();
                        ANFSI1Gear2.NFSI1Stop();
                        ANSLIGear1.NSLIStop();
                        ANFSI1Gear1.NFSI1Stop();
                    }
                }
                else if (ivehicle == 2)
                {
                    ANSLIGear3Jawa.NSLIGearChange();
                    ANSLIGear2Jawa.NSLIStop();
                    ANSLIGear1Jawa.NSLIStop();
                //    ANFSI1Gear1.NFSI1Stop();
                    //     ANFSI1Gear1.NFSI1Stop();
                }
                else if (ivehicle == 3)
                {
                    ANSLIGear3Bullet500.NSLIGearChange();
                    ANSLIGear2Bullet500.NSLIStop();
                    ANSLIGear1Bullet500.NSLIStop();
                    //    ANFSI1Gear1.NFSI1Stop();
                    //     ANFSI1Gear1.NFSI1Stop();
                }

            }
        }

        else if (gear == 4)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGear.RevLimiterStop();
                }
                else if (ivehicle == 2)
                {
                    ANFSRGearJawa.FSRGearChange();
                  
                    ANRevLimiterGearJawa.RevLimiterStop();
                }
                else if (ivehicle == 3)
                {
                    ANFSRGearBullet500.FSRGearChange();

                    ANRevLimiterGearBullet500.RevLimiterStop();
                }
                StartCoroutine("FadeinNFSRG");
               

                // Debug.Log("gear+1=22");
            }

            if (itrate == 203 || itrate == 303)
            {

                itrate = 204;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0)
                    {
                        ANSLIGear4.NSLIGearChange();
                        ANSLIGear3.NSLIStop();
                        ANFSI1Gear3.NFSI1Stop();
                        ANSLIGear2.NSLIStop();
                        ANFSI1Gear2.NFSI1Stop();
                    }
                }
                else if (ivehicle == 2)
                {
                    ANSLIGear4Jawa.NSLIGearChange();
                    ANSLIGear3Jawa.NSLIStop();
                //    ANFSI1Gear3Jawa.NFSI1Stop();
                    ANSLIGear2Jawa.NSLIStop();
               //     ANFSI1Gear2.NFSI1Stop();
                }
                else if (ivehicle == 3)
                {
                    ANSLIGear4Bullet500.NSLIGearChange();
                    ANSLIGear3Bullet500.NSLIStop();
                    //    ANFSI1Gear3Jawa.NFSI1Stop();
                    ANSLIGear2Bullet500.NSLIStop();
                    //     ANFSI1Gear2.NFSI1Stop();
                }

            }
        }

        else if (gear == 5)
        {

            if (itrate == 49990)
            {

                blipcinterval = 0;
                ifadeinnfsr = 1;
                if (ivehicle == 1)
                {
                    if (iexhaust == 0) ANFSRGear.FSRGearChange();
                    //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                    ANRevLimiterGear.RevLimiterStop();
                }
                else if (ivehicle == 3)
                {
                    ANFSRGearBullet500.FSRGearChange();
            //        ANRevLimiterGear.RevLimiterStop();
                }
                    StartCoroutine("FadeinNFSRG");
               

                // Debug.Log("gear+1=22");
            }

            if (itrate == 204 || itrate == 304)
            {

                itrate = 205;
                if (iexhaust == 0)
                {
                    if (ivehicle == 1)
                    {
                        ANSLIGear5.NSLIGearChange();
                        ANSLIGear4.NSLIStop();
                        ANFSI1Gear4.NFSI1Stop();
                        ANSLIGear3.NSLIStop();
                        ANFSI1Gear3.NFSI1Stop();
                    }
                    else if(ivehicle == 3)
                    {
                      ANSLIGear5Bullet500.NSLIGearChange();
                        ANSLIGear4Bullet500.NSLIStop();
                     //   ANFSI1Gear3Bullet500.NFSI1Stop();
                        ANSLIGear3Bullet500.NSLIStop();
                        ANFSI1Gear3.NFSI1Stop();
                    }
                }

            }
        }
    }

    public void GearDown()
    {
        if (gear > 0)
        {
            gear = gear - 1;
            if (gear == 1)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0) ANFSRGear.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGear.RevLimiterStop();
                    }
                    else if (ivehicle == 2)
                    {
                        ANFSRGearJawa.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearJawa.RevLimiterStop();
                    }
                    else if (ivehicle == 3)
                    {
                        ANFSRGearBullet500.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearBullet500.RevLimiterStop();
                    }
                    StartCoroutine("FadeinNFSRG");
                    

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 202 || itrate == 302)
                {

                    itrate = 200;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0)
                        {
                            ANSLIGear1.NSLIGearChange();
                            ANSLIGear2.NSLIStop();
                            ANFSI1Gear2.NFSI1Stop();
                        }
                    }
                    else if (ivehicle == 2)
                    {
                        ANSLIGear1Jawa.NSLIGearChange();
                        ANSLIGear2Jawa.NSLIStop();
                    }
                    else if (ivehicle == 3)
                    {
                        ANSLIGear1Bullet500.NSLIGearChange();
                        ANSLIGear2Bullet500.NSLIStop();
                    }

                }
            }
            else if (gear == 2)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0) ANFSRGear.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGear.RevLimiterStop();
                    }
                    else if(ivehicle == 2)
                    {
                        ANFSRGearJawa.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearJawa.RevLimiterStop();
                    }
                    else if (ivehicle == 3)
                    {
                        ANFSRGearBullet500.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearBullet500.RevLimiterStop();
                    }
                    StartCoroutine("FadeinNFSRG");
                    

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 203 || itrate == 303)
                {

                    itrate = 202;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0)
                        {
                            ANSLIGear2.NSLIGearChange();
                            ANSLIGear3.NSLIStop();
                            ANFSI1Gear3.NFSI1Stop();
                        }
                    }
                    else if (ivehicle == 2)
                    {
                        ANSLIGear2Jawa.NSLIGearChange();
                        ANSLIGear3Jawa.NSLIStop();
                  //      ANFSI1Gear3.NFSI1Stop();
                    }

                    else if (ivehicle == 3)
                    {
                        ANSLIGear2Bullet500.NSLIGearChange();
                        ANSLIGear3Bullet500.NSLIStop();
                        //      ANFSI1Gear3.NFSI1Stop();
                    }
                }
            }

            else if (gear == 3)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0) ANFSRGear.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGear.RevLimiterStop();
                    }
                    else if(ivehicle == 2)
                    {

                        ANFSRGearJawa.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearJawa.RevLimiterStop();
                    }
                    else if (ivehicle == 3)
                    {

                        ANFSRGearBullet500.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearBullet500.RevLimiterStop();
                    }
                    StartCoroutine("FadeinNFSRG");
                    

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 204 || itrate == 304)
                {

                    itrate = 203;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0)
                        {
                            ANSLIGear3.NSLIGearChange();
                            ANSLIGear4.NSLIStop();
                            ANFSI1Gear4.NFSI1Stop();
                        }
                    }
                    else if (ivehicle == 2)
                    {
                        ANSLIGear3Jawa.NSLIGearChange();
                        ANSLIGear4Jawa.NSLIStop();
                    //    ANFSI1Gear4.NFSI1Stop();
                    }
                    else if (ivehicle == 2)
                    {
                        ANSLIGear3Bullet500.NSLIGearChange();
                        ANSLIGear4Bullet500.NSLIStop();
                        //    ANFSI1Gear4.NFSI1Stop();
                    }

                }
            }

            else if (gear == 4)
            {
                if (itrate == 49990)
                {

                    blipcinterval = 0;
                    ifadeinnfsr = 1;
                    if (ivehicle == 1)
                    {
                        if (iexhaust == 0) ANFSRGear.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGear.RevLimiterStop();
                    }
                    if (ivehicle == 3)
                    {
                         ANFSRGearBullet500.FSRGearChange();
                        //     else if (iexhaust == 1) ANFSRAkra1.FSR();
                        ANRevLimiterGearBullet500.RevLimiterStop();
                    }
                    StartCoroutine("FadeinNFSRG");
                    

                    // Debug.Log("gear+1=22");
                }


                if (itrate == 205 || itrate == 305)
                {

                    itrate = 204;
                    if(ivehicle == 1) {
                        if (iexhaust == 0)
                        {
                            ANSLIGear4.NSLIGearChange();
                            ANSLIGear5.NSLIStop();
                            ANFSI1Gear5.NFSI1Stop();
                        }
                        else if (ivehicle == 3)
                        {
                            ANSLIGear4Bullet500.NSLIGearChange();
                       //     ANSLIGear5Bullet500.NSLIStop();
                        //    ANFSI1Gear5Bullet500.NFSI1Stop();
                        }
                    }

                }
            }

        }
    }
    public void LaunchControlEngage()
    {
        ilaunchCengaged = 1;
    }


    IEnumerator StartFSRG1()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        Debug.Log("INside FSRG1");
        if (itrate == 200 || itrate == 300 || itrate == 49990 || itrate == 202 || itrate == 302 || itrate == 203 || itrate == 303 || itrate == 204 || itrate == 304 || itrate == 205 || itrate == 305 || itrate == 100000 || itrate == -999)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
                          Debug.Log("Inside FSR Gear$$$$$$$$$$$$$$$$$$$$$$$$$$  Gear=");
           //      Debug.Log(gear);

            ifadeinnfsr = 1;
            if (ivehicle == 1)
            {
                if (iexhaust == 0) ANFSRGear.FSR();
                else if (iexhaust == 1) ANFSRAkra1.FSR();
                ANRevLimiterGear.RevLimiterStop();
           //     ANRevLimiterGearJawa.RevLimiterStop();
            }
            else if (ivehicle == 2)
            {
                ANFSRGearJawa.FSR();
               
                ANRevLimiterGearJawa.RevLimiterStop();
            }
            else if (ivehicle == 3)
            {
                Debug.Log("Inside FSR Gear$$$$$$$$$$$$$$$$$$$$$$$$$$  Vehicle=3");
                ANFSRGearBullet500.FSR();

                ANRevLimiterGearBullet500.RevLimiterStop();
            }
            StartCoroutine("FadeinNFSRG");
            
            if (iPopCrackleX == 1 && rpm1 > 7000)
            {
                Debug.Log("Inside Blipc15000------------------------");
                ACrackle10000.Crackle10000();
                StartCoroutine("Explode");
            }

            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLIG");
            StartCoroutine("FadeoutNFSI1G");
            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;
            itrate = 49990;
        }
    }

    IEnumerator FadeinNFSRG()
    {
        //        Debug.Log("Gear ____Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (ivehicle == 1)
            {
                if (iexhaust == 0) ANFSRGear.NFSRFadein();
            }
            else if(ivehicle == 2)
            {
                ANFSRGearJawa.NFSRFadein();
            }
            else if (ivehicle == 3)
            {
                ANFSRGearBullet500.NFSRFadein();
            }
            //       else if (iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
    }
    IEnumerator FadeoutNSLIG()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            if (ivehicle == 1)
            {
                if (gear == 1)
                {
                    if (iexhaust == 0) ANSLIGear1.NSLIFadeout();
                    else if (iexhaust == 1) ANSLIAkra1.NSLIFadeout();
                }
              
            
            if (gear == 2)
            {
                if (iexhaust == 0) ANSLIGear2.NSLIFadeout();
                else if (iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 3)
            {
                if (iexhaust == 0) ANSLIGear3.NSLIFadeout();
                else if (iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 4)
            {
                if (iexhaust == 0) ANSLIGear4.NSLIFadeout();
                else if (iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
            if (gear == 5)
            {
                if (iexhaust == 0) ANSLIGear5.NSLIFadeout();
                else if (iexhaust == 1) ANSLIAkra1.NSLIFadeout();
            }
        }
            else if (ivehicle == 2)
            {
                ANSLIGear1Jawa.NSLIFadeout();
                ANSLIGear2Jawa.NSLIFadeout();
                ANSLIGear3Jawa.NSLIFadeout();
                ANSLIGear4Jawa.NSLIFadeout();
            }
            else if (ivehicle == 3)
            {
                ANSLIGear1Bullet500.NSLIFadeout();
                ANSLIGear2Bullet500.NSLIFadeout();
                ANSLIGear3Bullet500.NSLIFadeout();
                ANSLIGear4Bullet500.NSLIFadeout();
                ANSLIGear5Bullet500.NSLIFadeout();
            }

        }
    }
    IEnumerator FadeoutNFSI1G()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            //     ANFSI1.NFSI1Fadeout();
            if (ivehicle == 1)
            {
                if (gear == 1)
                {
                    if (iexhaust == 0) ANFSI1Gear1.NFSI1Fadeout();
                }
                //    else if (iexhaust == 1) ANFSI1Akra1.NFSI1Fadeout();
                if (gear == 2)
                {
                    if (iexhaust == 0) ANFSI1Gear2.NFSI1Fadeout();
                }
                if (gear == 3)
                {
                    if (iexhaust == 0) ANFSI1Gear3.NFSI1Fadeout();
                }
                if (gear == 4)
                {
                    if (iexhaust == 0) ANFSI1Gear4.NFSI1Fadeout();
                }
                if (gear == 5)
                {
                    if (iexhaust == 0) ANFSI1Gear5.NFSI1Fadeout();
                }
            }
            else if (ivehicle == 2 )
            {
              //  ANFSI1Gear1Jawa.NSLIFadeout();
              //  ANFSI1Gear2Jawa.NSLIFadeout();
              //  ANFSI1Gear3Jawa.NSLIFadeout();
            }
        }
    }

    private IEnumerator LaunchRevRise()
    {
        yield return new WaitForSeconds(0.1f);
        //   Debug.Log("inside LaunchRevs");
        while (rpm1 < revslaunch)
        {
            //     Debug.Log("rpm1=");
            //     Debug.Log(rpm1);
            yield return new WaitForSeconds(0.01f); // wait half a second
            rpm1 = rpm1 + 300;

            //     if (rpmi <= 7400)
            //     {

        }

    }
    private IEnumerator LaunchRevDrop()
    {
        yield return new WaitForSeconds(0.01f);
        Debug.Log("Launch Drop1 rpm1=");
        Debug.Log(rpm1);

        while (rpm1 > revslaunchdrop)
        {
            //       itrate = 100;
            Debug.Log("Launch Drop rpm1=");
            Debug.Log(rpm1);
            yield return new WaitForSeconds(0.01f); // wait half a second
            rpm1 = rpm1 - 300;

            //     if (rpmi <= 7400)
            //     {

        }

        //      yield return new WaitForSeconds(0.2f);
        ilaunchCengaged = 3;
        iclutchrelease = 1;
        //      trate = 1;  // TO start SLI
        itrate = 100;  //
    }



    float clutchpos = 0;
    public void SliderClutch(float sliderpositionclutch)
    {
        //      moveSpeed = newSpeed;
        clutchpos = sliderpositionclutch;

    }


}