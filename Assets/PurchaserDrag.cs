using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// Comment from huracan Test
// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
namespace CompleteProject
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class PurchaserDrag : MonoBehaviour, IStoreListener
    {
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        //     public GameObject PurchaserIndicator;
        // Product identifiers for all products capable of being purchased: 
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values 
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.
        public static string kProductIDConsumable = "consumable";
        public static string kProductIDNonConsumable = "nonconsumable";
        public static string kProductIDSubscription = "subscription";

        public BinSaveLoad BinarySaveLoad;     // Gam Obj to Save Load Consumables in Binary
        public BinSaveLoadDrag BinarySaveLoadDrag;     // Gam Obj to Save Load Consumables in Binary
        // Apple App Store-specific product identifier for the subscription product.
        private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

        // Google Play Store-specific product identifier subscription product.
        private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

        void Start()
        {
            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();

                Debug.Log("Inside Purchasing Restoring");
                //         RestorePlaystoreProducts("ss1.i4muffler1");  //self
                //       RestorePlaystoreProducts("s1.p2muffler1");
                //       RestorePlaystoreProducts("s2.i4muffler1");
                //       RestorePlaystoreProducts("sca.v10sportmode");
            }
        }


        //Self to restore playstore Purchased Items 
        public void RestorePlaystoreProducts(string productId)
        {
            Debug.Log("Inside Purchasing Restoring 1");
            Product product = m_StoreController.products.WithID(productId);
            if (product.receipt != null)
            {
                Debug.Log("Inside Purchasing Restoring 2");
                if (productId == "ss1.i4muffler1") BinarySaveLoad.SaveZx10Akra1(3);
                if (productId == "s1.p2muffler1") BinarySaveLoad.SaveCB400Akra1(3);
                if (productId == "s1.p2muffler1") BinarySaveLoad.SaveNinja250Yoshi1(3);
                if (productId == "sca.v10sportmode") BinarySaveLoad.SaveHuracanMode(3);
            }
        }
        // Self
        public void InitializePurchasing()
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                // ... we are done here.
                return;
            }

            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Add a product to sell / restore by way of its identifier, associating the general identifier
            // with its store-specific identifiers.
            builder.AddProduct(kProductIDConsumable, ProductType.Consumable);
            // Continue adding the non-consumable product.
            // ****          builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);
           
            
            // New Drag Moto Products

            //*************** Bikes
            // ZX25R
            builder.AddProduct("zx25r.i4", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
            builder.AddProduct("bullet500.single", ProductType.NonConsumable);    //** ZX10RAkra1-ss1


            // Exhausts

            // Riding Gear
            builder.AddProduct("ridinggear1", ProductType.NonConsumable);    //** ZX10RAkra1-ss1

            // Tracks
            builder.AddProduct("track1", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
            builder.AddProduct("track2", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
            builder.AddProduct("track3", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
            builder.AddProduct("track4", ProductType.NonConsumable);    //** ZX10RAkra1-ss1

            // OLD Motorbikes
            //    ZX10R 
            builder.AddProduct("ss1.i4muffler1", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
                                                                                //          builder.AddProduct("ss1.i4muffler1", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
                                                                                // Ninja400
            builder.AddProduct("s1.p2muffler1", ProductType.NonConsumable);    //** Ninja400 Yoshi1

            // CB400
            builder.AddProduct("s2.i4muffler1", ProductType.NonConsumable);    //** CB400400 Yoshi1

            // Jawa2s
            builder.AddProduct("jawa.stock1", ProductType.NonConsumable);    //** Jawa2s Stock1

            // SpeedTriple Arrow
            builder.AddProduct("speedtriple.arrow", ProductType.NonConsumable);    //** Jawa2s Stock1

            // Cars
            // Huracan
            builder.AddProduct("sca.v10sportmode", ProductType.NonConsumable);   // **  Huracan Sport Mode
                                                                                 //            builder.AddProduct("sca.v10sportmodetest", ProductType.NonConsumable);   // **  Test Huracan Sport Mode

            //Mustang
            builder.AddProduct("mustangv8.roush1", ProductType.NonConsumable);  //Roush1
            builder.AddProduct("mustangv8.borla1", ProductType.NonConsumable);   // Borla1

            // Nissan GTR R34      
            builder.AddProduct("nissangtr_r34_i6.stockexhaust", ProductType.NonConsumable);  //Roush1

            // Corvette Stingray
            builder.AddProduct("corvettestingrayv8.stockexhaust", ProductType.NonConsumable);  //Roush1

            // Nissan GTR Stock Titanium Exhaust
            builder.AddProduct("nissangtrr35_v6.titaniumexhaust", ProductType.NonConsumable);  //Roush1

            //          builder.AddProduct("SSAI4AMuffler1", ProductType.NonConsumable);    //** ZX10RAkra1
            //       builder.AddProduct("SSAI4AMuffler2", ProductType.NonConsumable);    //** ZX10RTBR1
            //       builder.AddProduct("SAI4AMuffler1", ProductType.NonConsumable);    //** CB400 Yoshi
            //       builder.AddProduct("SS1P2Muffler1", ProductType.NonConsumable);    //** Ninja400 Yoshi1
            //            builder.AddProduct("SS1P2Muffler1", ProductType.NonConsumable);    //** Ninja400 Yoshi1

            // OLD
            builder.AddProduct("ss1.i4muffler1test22", ProductType.NonConsumable);    //** ZX10RAkra1-ss1
            builder.AddProduct("nonconsumable", ProductType.NonConsumable);    //** ZX10RAkra1
            builder.AddProduct("nonconsumablezx10tb1", ProductType.NonConsumable);    //** ZX10RTBR1
            builder.AddProduct("nonconsumable2", ProductType.NonConsumable);   // **  Huracan Sport Mode
            builder.AddProduct("nonconsumableNinja250Yoshi1", ProductType.NonConsumable);    //** Ninja400 Yoshi1
            builder.AddProduct("nonconsumabletest2", ProductType.NonConsumable);    //** ZX10RAkra1Test
            builder.AddProduct("nonconsumabletest", ProductType.NonConsumable);    //** ZX10RAkra1Test
            builder.AddProduct("nonconsumabletestcb400yoshi", ProductType.NonConsumable);    //** CB400 yoshi

            builder.AddProduct("aftermarketextest2cb", ProductType.NonConsumable);    //** CB400 yoshi
            // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
            // if the Product ID was configured differently between Apple and Google stores. Also note that
            // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
            // must only be referenced here. 
            builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
                { kProductNameAppleSubscription, AppleAppStore.Name },
                { kProductNameGooglePlaySubscription, GooglePlay.Name },
            });

            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
            UnityPurchasing.Initialize(this, builder);
        }


        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }


        public void BuyConsumable()
        {
            // Buy the consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            BuyProductID(kProductIDConsumable);
        }


        // New Drag Moto Products
        public void BuyVehicleNonConsumable1()  // ZX10 Akra1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (MainMenuScript.ivehicle == 1)
            {
                if (MainMenuScript.IAPZX25 != 1)
                {
                    //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                    //       BuyProductID(kProductIDNonConsumable);

                            Debug.Log(" Inside ZX25 BuyNonCOnsumable");
                    //        BuyProductID("nonconsumablezx10tb1");
                    BuyProductID("zx25r.i4");
                }
            }
            else if (MainMenuScript.ivehicle == 2)
                {
                    if (MainMenuScript.IAPBullet500 != 1)
                    {
                        //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                        //       BuyProductID(kProductIDNonConsumable);

                        Debug.Log(" Inside Bullet500 BuyNonCOnsumable");
                        //        BuyProductID("nonconsumablezx10tb1");
                        BuyProductID("bullet500.single");
                    }
                }

        }

        public void BuyRidingGear1NonConsumable200()  // ZX10 Akra1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
           
                if (MainMenuScript.IAPRidingGear1 != 1)
                {
                    //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                    //       BuyProductID(kProductIDNonConsumable);

                    Debug.Log(" Inside Jacket1 BuyNonCOnsumable");
                    //        BuyProductID("nonconsumablezx10tb1");
                    BuyProductID("ridinggear1");
                }
            
        }
        public void BuyTracksNonConsumable300()  // ZX10 Akra1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.

            if (MainMenuScript.iTrack == 1 && MainMenuScript.IAPTrack1 != 1)
            {
                //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                //       BuyProductID(kProductIDNonConsumable);

                Debug.Log(" Inside Track1");
                //        BuyProductID("nonconsumablezx10tb1");
                BuyProductID("track1");
            }
            else if (MainMenuScript.iTrack == 2 && MainMenuScript.IAPTrack2 != 1)
            {
                //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                //       BuyProductID(kProductIDNonConsumable);

                Debug.Log(" Inside Track2");
                //        BuyProductID("nonconsumablezx10tb1");
                BuyProductID("track2");
            }
            if (MainMenuScript.iTrack == 3 && MainMenuScript.IAPTrack3 != 1)
            {
                //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                //       BuyProductID(kProductIDNonConsumable);

                Debug.Log(" Inside Track3");
                //        BuyProductID("nonconsumablezx10tb1");
                BuyProductID("track3");
            }
            if (MainMenuScript.iTrack == 4 && MainMenuScript.IAPTrack4 != 1)
            {
                //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                //       BuyProductID(kProductIDNonConsumable);

                Debug.Log(" Inside Track4");
                //        BuyProductID("nonconsumablezx10tb1");
                BuyProductID("track4");
            }
        }




        // Old EngineRev Products
        public void BuyNonConsumable1()  // ZX10 Akra1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngine.ZX10ExhaustAkra1 != 1)
            {
                //          PurchaserIndicator.SetActive(true); // Tick on Stock Ex Panel
                //       BuyProductID(kProductIDNonConsumable);

                //          Debug.Log(" Inside buynonconsumabe1");
                //        BuyProductID("nonconsumablezx10tb1");
                BuyProductID("ss1.i4muffler1");
            }

        }
        public void BuyNonConsumable2()  // ZX10TBR1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngine.ZX10ExhaustTBR1 != 1)
            {

                //        BuyProductID("nonconsumablezx10tb1");
                //           BuyProductID("nonconsumable");
            }
        }
        public void BuyNonConsumable3()  // HUracan Sport Mode
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineS1000RRAkra.HuracanSportMode != 1)
            {
                //         Debug.Log("Inside Purchaser3");
                BuyProductID("sca.v10sportmode");
            }
        }

        public void BuyNonConsumable4()  // Ninja400 Sport Mode
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineNinja250.Ninja250ExhaustYoshi1 != 1)
            {

                //        BuyProductID("nonconsumableNinja250Yoshi1");
                BuyProductID("s1.p2muffler1");
            }
        }

        public void BuyNonConsumable5()  // CB400 Akra/Yoshi Mode
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineI4.CB400Akra1 != 1)
            {

                //     BuyProductID("SAI4AMuffler1");
                BuyProductID("s2.i4muffler1");
            }
        }

        public void BuyNonConsumable6()  // Mustang Roush1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineMustang.MustangRoushPurchased != 1)
            {
                Debug.Log("Mustang Roush Purchased Start 1");
                BuyProductID("mustangv8.roush1");
            }
        }
        public void BuyNonConsumable7()  // Mustang Borla1
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineMustang.MustangBorla1Purchased != 1)
            {
                Debug.Log("Mustang Borla Purchased Start 1");
                BuyProductID("mustangv8.borla1");
            }
        }

        public void BuyNonConsumable8()  // Jawa2s Stock
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineJawa2s.Jawa2sExhaustStock != 1)
            {
                //         Debug.Log("Inside Purchaser3");
                BuyProductID("jawa.stock1");
            }
        }
        //  NISSAN GTR R34 Stock Exhaust
        public void BuyNonConsumable9()  // GTR R34 Stock Exhaust
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineR34.R34StockPurchased != 1)
            {
                //         Debug.Log("Inside Purchaser3");
                BuyProductID("nissangtr_r34_i6.stockexhaust");
            }
        }

        //  Corvette Stingray Staock Exhaust
        public void BuyNonConsumable10()  // GTR R34 Stock Exhaust
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineCorvette.CorvetteStockPurchased != 1)
            {
                Debug.Log("Inside Purchaser Corvette");
                BuyProductID("corvettestingrayv8.stockexhaust");
            }
        }

        public void BuyNonConsumable11()  // GTR R34 Stock Exhaust
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineR35.R35StockTitaniumPurchased != 1)
            {
                Debug.Log("Inside Purchaser Corvette");
                BuyProductID("nissangtrr35_v6.titaniumexhaust");
            }
        }


        //  SpeedTriple Arrow Exhaust
        public void BuyNonConsumable12()  // Speed Triple Exhaust
        {
            // Buy the non-consumable product using its general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            if (AudioEngineSTriple.STripleExhaustArrow != 1)
            {
                //     Debug.Log("Inside Purchaser Corvette");
                BuyProductID("speedtriple.arrow");
            }
        }

        public void BuySubscription()
        {
            // Buy the subscription product using its the general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            // Notice how we use the general product identifier in spite of this ID being mapped to
            // custom store-specific identifiers above.
            BuyProductID(kProductIDSubscription);
        }


        void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                // Self           if (product != null && product.availableToPurchase && product.receipt == null)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) => {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


        //  
        // --- IStoreListener
        //

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS---------------------------------");

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;


            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
        }



        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            // A consumable product has been purchased by this user.
            if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
            //    if (String.Equals(args.purchasedProduct.definition.id, "ss1.i4muffler1", StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
                //               ScoreManager.score += 100;
                //          AudioEngine.ZX10ExhaustAkra1 = 1;
                //     Debug.Log("Inside Purchase processing zx10");
                //    BinarySaveLoad.SaveZx10Akra1(1);

            }
            // Or ... a non-consumable product has been purchased by this user.
            //        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
            
            else if (String.Equals(args.purchasedProduct.definition.id, "zx25r.i4", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
         
                Debug.Log("INside ZX125 Process Purchase ------------------------------------------");
              BinarySaveLoadDrag.SaveZX25R(1);
             

            }

            else if (String.Equals(args.purchasedProduct.definition.id, "bullet500.single", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                Debug.Log("INside Bullet500 Process Purchase ------------------------------------------");
                BinarySaveLoadDrag.SaveBullet500(1);


            }

            else if (String.Equals(args.purchasedProduct.definition.id, "ridinggear1", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                Debug.Log("INside RidingGear1 Process Purchase ------------------------------------------");
                BinarySaveLoadDrag.SaveRidingGear1(1);


            }
            else if (String.Equals(args.purchasedProduct.definition.id, "track1", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                Debug.Log("INside RidingGear1 Process Purchase ------------------------------------------");
              BinarySaveLoadDrag.SaveTrack1(1);


            }
            else if (String.Equals(args.purchasedProduct.definition.id, "track2", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                Debug.Log("INside RidingGear1 Process Purchase ------------------------------------------");
                BinarySaveLoadDrag.SaveTrack2(1);


            }

            else if (String.Equals(args.purchasedProduct.definition.id, "track3", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                Debug.Log("INside RidingGear1 Process Purchase ------------------------------------------");
                BinarySaveLoadDrag.SaveTrack3(1);


            }

            else if (String.Equals(args.purchasedProduct.definition.id, "track4", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.

                Debug.Log("INside RidingGear1 Process Purchase ------------------------------------------");
           //     BinarySaveLoadDrag.SaveTrack4(1);


            }
            else if (String.Equals(args.purchasedProduct.definition.id, "ss1.i4muffler1", StringComparison.Ordinal))    // ZX10R Muffler1 
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene

                //              PlayerPrefs.SetInt("nonconsumableval1", 1);
                Debug.Log("INside ZX10R Process Purchase ------------------------------------------");
                BinarySaveLoad.SaveZx10Akra1(1);
                //   Debug.Log("INside ZX10R Purchaser 1");
                //

            }
            // else if (String.Equals(args.purchasedProduct.definition.id, "nonconsumablezx10tb1", StringComparison.Ordinal))
            else if (String.Equals(args.purchasedProduct.definition.id, "nonconsumable", StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                //       PlayerPrefs.SetInt("nonconsumableval2", 1);
                //          AudioEngine.ZX10ExhaustAkra1 = 1;
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "sca.v10sportmode", StringComparison.Ordinal))  // Huracan Sport Mode
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveHuracanMode(1);

                //        Debug.Log("INside huracan Purchaser1");
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "s1.p2muffler1", StringComparison.Ordinal))  // Ninja250 Yoshi1
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                //        PlayerPrefs.SetInt("nonconsumableval11", 1);

                BinarySaveLoad.SaveNinja250Yoshi1(1);
            }


            else if (String.Equals(args.purchasedProduct.definition.id, "s2.i4muffler1", StringComparison.Ordinal))  // CB400
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                //  PlayerPrefs.SetInt("nonconsumableval21", 1);
                BinarySaveLoad.SaveCB400Akra1(1);
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "mustangv8.roush1", StringComparison.Ordinal))  // Huracan Sport Mode
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveMustangRoush1(1);
                Debug.Log("Mustang Roush Purchased 2");
                //      Debug.Log("INside Mustang Roush Purchaser1");
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "mustangv8.borla1", StringComparison.Ordinal))  // Huracan Sport Mode
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveMustangBorla1(1);

                Debug.Log("Mustang Borla Purchased 2");
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "jawa.stock1", StringComparison.Ordinal))  // Huracan Sport Mode
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveJawa2sStock1(1);

                Debug.Log("INside Jawa2s ");
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "nissangtr_r34_i6.stockexhaust", StringComparison.Ordinal))  // Nissan GTR R34 Stock Exhaust
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveR34StockExhaust(1);

                Debug.Log("INside R34 Stock Exhaust");
            }
            else if (String.Equals(args.purchasedProduct.definition.id, "corvettestingrayv8.stockexhaust", StringComparison.Ordinal))  // Nissan GTR R34 Stock Exhaust
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveCorvetteStockExhaust(1);

                Debug.Log("INside Corvette Stock Exhaust");
            }

            else if (String.Equals(args.purchasedProduct.definition.id, "nissangtrr35_v6.titaniumexhaust", StringComparison.Ordinal))  // Nissan GTR R34 Stock Exhaust
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveR35StockTitaniumExhaust(1);

                //     Debug.Log("INside Corvette Stock Exhaust");
            }
            else if (String.Equals(args.purchasedProduct.definition.id, "speedtriple.arrow", StringComparison.Ordinal))  // Nissan GTR R34 Stock Exhaust
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                //Give the PlayerPrefs some values to send over to the next Scene
                // PlayerPrefs.SetInt("nonconsumableval101", 1);
                //          Debug.Log("INside huracan Purchaser0");
                BinarySaveLoad.SaveSTripleArrowExhaust(1);

                //    Debug.Log("INside Corvette Stock Exhaust");
            }




            // Or ... a subscription product has been purchased by this user.
            else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The subscription item has been successfully purchased, grant this to the player.
            }
            // Or ... an unknown product has been purchased by this user. Fill in additional products here....
            else
            {
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }
    }
}
