using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP4000STriple : MonoBehaviour
{
    public static float dt4000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip4000()
    {


        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineSTriple.rpm1 >= 3000 && AudioEngineSTriple.rpm1 < 4000)
        {

            audio.Play();

        }
        else if (AudioEngineSTriple.rpm1 >= 2000 && AudioEngineSTriple.rpm1 < 3000)
        {
        //    dt4000 = 0.096f;
            dt4000 = 0.059f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }
        else if (AudioEngineSTriple.rpm1 < 2000)
        {
          //  dt4000 = 0.161f;
            dt4000 = 0.203f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }

        audio.volume = 1;
        //     audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip4000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip4000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip4000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        // audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}

