using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using UnityEngine.SceneManagement;




public class MainControllerZX10 : MonoBehaviour
{

    double startTick;
    public SoundEngineZX10 SEZX10;  // Refernce of Sound Engine

    public int ikeyin = 0;    // Ignition Key in Counter
    public static int ikeyinfromtouch = 0;
    public int ikeyoffsound = 0; // Counter for activating key-off sound
    public static int istart = -100;    // Start Counter
    public static int istartfromtouch = 0;
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs

    public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 

    public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = -1;  // To let RPM be 0 before idling(Clash with AudioIdling Module where irpm =0 activates it
  //  public static int iblipcanstart = 1;  // Counter for Blip Start
  //  public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 0;

    int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


//    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 1100;
 //   public float rpmdisplayafterstart = 1.4f;  // Time in seconds after which Idling RPM start post Startup
  //  public static int rpmblipconst;  // TO Stop Blip at different RPMs
  //  public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public static float sliderval;
    public static float slidervalold;

    // Fade in out counters


    // View Counters Counters
    public GameObject GarageView;   // Appearance/Disappearance of Garage
 //   public static float ReverbXoneMix = 1.03f;   // Reverberation Value for Garage
    public GameObject RoadView;   // Appearance/Disappearance of Road
    public GameObject TunnelView;   // Appearance/Disappearance of Road

    // Akrapovic Counters
    public int exhaustDropDown = 0; // 0 Dropdown up, 1 Dropdown down
    public static int iexhaust = 0;  // 0 for Stock, 1 for Akra1,  
                              //    public Button Exhaustbutton;  // Exhaust Option Button
    public GameObject Exhaustbutton;
    public GameObject ExhaustStockbutton;
    public GameObject StockExhaustTick;
    public GameObject ExhaustAkra1button;
    public GameObject Akra1ExhaustTick;
    public GameObject Akra1CarbonExhaustTick;
    public GameObject ExhaustAkra1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased
    public GameObject ExhaustTBR1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased

    public BinSaveLoad BinarySaveLoad;     // Gam Obj to Save Load Consumables in Binary

    public GameObject StockMuffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject StockMufflerShield;   // Appearance/Disappearance of Stock Muffler Shield
    public GameObject Akra1Muffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject AkraBlackMuffler;   // Appearance/Disappearance of AkrapovicBlack
    public GameObject AkraCarbonMuffler;   // Appearance/Disappearance of AkrapovicCarbon
    public GameObject StockMufflerSoundSource;  // Activate/Deactivate Stock Sound (Interference stop issue at Blipc10000 
    public GameObject Akra1MufflerSoundSource;  // Activate/Deactivate Akra1 Sound (Interference stop issue at Blipc10000 



    // Backfire Section
    public ExhaustPopZX10R AVExhaustPop;
  //  public int iPopCrackle = 0;  // 0-off, 1-On
    public static float dtpop = 0.75f;  // Delay on Backfire 
    public GameObject PopsCracklesHeader;  // Main Menu for Pops Crackles
    public GameObject PopsCrackles1;       // Pos Crackles1 Button
    public GameObject PopsCrackles1Tick;       // Pos Crackles1 Button Tick
                                               // Paid Options
    public AudioScriptCrackle10000 ACrackle10000;  // Crackle 10000
    public AudioScriptCrackle8000 ACrackle8000;  // Crackle 10000

    //Timer Variables
    float StartTimer = 0;
    int istarttimer = 0;  // 1 at Start and 0 at Stop
    float startertime = 0;  // Time during which Starter operates -Less during warmedup engine
    float warmuptime = 20; // Seconds to warm-up

    public Camera Camera;


   




    // GEAR SECTION________________________________________________________________________________________________________________
    //   Gear1
    public static float bikespeed = 0;  // Bike SPEED
    public Text SpeedText;
    public Text GearText;


   

    //Launch Control

    public AudioLaunchC8000 ANLaunch8000;
    public AudioLaunchCConst8000 ANLaunchC8000;
    public AudioLaunchCReduce ANLaunchReduce;

    int revslaunch;   // Revs upto which engine revs
    int revslaunchdrop; // Revs to which rpm drops at SLI
                        //    public int GameMode = 0;  // 0- Race/Ride Mode 1- Drag Mode
    public int iclutchrelease = 0;
    int ilaunchCengaged = 0;  // 1 for 8000 RPM hold, 2 for 4000 RPM hold

    //Store Section
    public static int ZX10ExhaustAkra1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs
    public static int ZX10ExhaustTBR1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval2 in Playerprefs

    // Bike Position
    public ZX10Rscript Bike;

    // Headlight Object
    public HeadLight Hlight;
    public RearLight Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 


    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No

    //RateUs Variables
    public int irevcountrateus = 0;  // Counter to decide when user rates when the count is greated than irevcountmaxrateus
    public int irevcountmaxrateus = 50;
    int irateusloopdisable = 0;
    int irateusdialogdisable = 0;

    [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, RPM0, RPM1000, RPM1100, RPM1500, RPM2000, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;
    public GameObject ConsoleRPMOff, ConsoleRPM0, ConsoleRPM1000, ConsoleRPM1100, ConsoleRPM1500, ConsoleRPM2000, ConsoleRPM2200, ConsoleRPM2500, ConsoleRPM2800, ConsoleRPM3000, ConsoleRPM3200, ConsoleRPM3500, ConsoleRPM3800, ConsoleRPM4000, ConsoleRPM4200, ConsoleRPM4500, ConsoleRPM4800, ConsoleRPM5000, ConsoleRPM5200, ConsoleRPM5500, ConsoleRPM6000, ConsoleRPM6500, ConsoleRPM7000, ConsoleRPM7500, ConsoleRPM7700, ConsoleRPM8000, ConsoleRPM8200, ConsoleRPM8500, ConsoleRPM8800, ConsoleRPM9000, ConsoleRPM9200, ConsoleRPM9500, ConsoleRPM9800, ConsoleRPM10000, ConsoleRPM10200, ConsoleRPM10500, ConsoleRPM10800, ConsoleRPM11000, ConsoleRPM11200, ConsoleRPM11500, ConsoleRPM11800, ConsoleRPM12000, ConsoleRPM12200, ConsoleRPM12500, ConsoleRPM12800, ConsoleRPM13000, ConsoleRPM13200, ConsoleRPM13500, ConsoleRPM13800, ConsoleRPM14000, ConsoleRPM14200, ConsoleRPM14500, ConsoleRPM14800, ConsoleRPM15000;

    // Start is called before the first frame update
    void Start()
    {
        BinarySaveLoad.SaveZx10Akra1(0);
    }

    // Update is called once per frame
    void Update()
    {
       
//Load IAP Data to see if Product purchased or not
        if (ZX10ExhaustAkra1 == 0) BinarySaveLoad.LoadZX10Akra1();  // Binary Load Cnsumable Data
        if (ZX10ExhaustAkra1 == 1)
        {
            if (ExhaustAkra1PurPanel.activeInHierarchy == true)
            {
                {
                    ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                            //                   Debug.Log("Aftermarket Exhaust 1 Purchased");
                }
            }
        }

        if (ZX10ExhaustTBR1 == 0) ZX10ExhaustTBR1 = PlayerPrefs.GetInt("nonconsumableval2");
        if (ZX10ExhaustTBR1 == 1)
        {
            if (ExhaustTBR1PurPanel.activeInHierarchy == true)
            {
                {
                    ExhaustTBR1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                           //                   Debug.Log("Aftermarket Exhaust 1 Purchased");
                }
            }
        }
        //   PlayerPrefs.SetInt("RevCounttoRate", 0);
        //  PlayerPrefs.SetInt("RateUsPopUpDisabled", 0);
        // Rate Us Module- Pops when user revs beyond 4000 RPM and for irevcountermaxrateus time in entire game
        if (rpm1 > 4000 && irateusloopdisable == 0)
        {
            //      Debug.Log("INside Fiesta Rateus");
            irateusloopdisable = 1;
            irevcountrateus = PlayerPrefs.GetInt("RevCounttoRate");
            irevcountrateus = irevcountrateus + 1;
            PlayerPrefs.SetInt("RevCounttoRate", irevcountrateus);
            irateusdialogdisable = PlayerPrefs.GetInt("RateUsPopUpDisabled");  // 1 If permanently disabled
            if (irevcountrateus > irevcountmaxrateus && irateusdialogdisable == 0)
            {

                RateGame.Instance.ShowRatePopup();
                irateusdialogdisable = 1;
                PlayerPrefs.SetInt("RateUsPopUpDisabled", 1);
            }
        }
        if (rpm1 < 4000) irateusloopdisable = 0;

        // RateUs End

        // Ingnition On Module
        if (ikeyinfromtouch == 1)
        {
            ikeyinfromtouch = 2;
            ikeyoffsound = 1;  // Activate Key-off sound
            //         NeutralButtonOff.enabled = true;

            // Ignition Key Rotate to On Position
            IgnitionKeyOn.enabled = true;
            IgnitionKeyOff.enabled = false;


            RedButtonOn.enabled = true;
            iconsolestartup = 1;  // Console Startup Loop Enabler
                                  //   AStartup.Startup();
            SEZX10.StartupSound();
            NeutralButtonOff.enabled = false;
            NeutralButtonOn.enabled = true;
            StartCoroutine("ConsoleStartup");  // Lighing up of RPM Console when Key is pressed

            // Headlight on Module
            //      Hlight.HeadlightOn();


        }
        // Key Off Sound 
        else if (ikeyinfromtouch == 2 && ikeyoffsound == 1 && istartfromtouch != 2)
        {
            ikeyoffsound = 0;

            //           AStartdown.Startdown();
        }
        // Start Module
        if ((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1)
        {
            // Enable Console neutral Buttons
            //        NeutralButtonOff.enabled = false;
            //        NeutralButtonOn.enabled = true;

                    istart = 1;
       //     StartCoroutine("Startistart1");  // Starting the Loop for Events
            istartfromtouch = 2;

            gear = 0;
     //       AStarting.Starting();
           SEZX10.StartingfromMainController();  // Call for Staring in Sound Engine
   
            Debug.Log("inside Maincontroller Idling");
            StartCoroutine("StartIdling");

            // Start Timer upon engine start to change idling speed
            //     StartTimer = 0.0f;
            istarttimer = 1; //Start Timer

            //       irpm = 0;

        }
   //     Debug.Log("RPM=");
   //     Debug.Log(rpm1);
        // Module to change starter time depending upon the time engine has run (Time Increment After Start Engine)
        if (istarttimer == 1) StartTimer += Time.deltaTime;
        //       elseif (istarttimer ==0) Start
        if (StartTimer < warmuptime) startertime = 0.65f;  // Changing Startertime after warm-up
        else startertime = 0.35f;

        // Shuttin Module
        if ((Input.GetKeyDown("space") && istart == 1) || istartfromtouch == 3)
        {
            istarttimer = 0; // Stop Timer
                             // Enable Console neutral Buttons
                             //       NeutralButtonOff.enabled = true;
                             //       NeutralButtonOn.enabled = false;
                             //      AShutting.Shutting();
            SEZX10.ShuttingfromMainController();  // Call for Shutting on Sound Engine
            istart = 0;
            istartfromtouch = 0;

     //       itrate = -1;
      //      SoundEngineZX10.irpm = -1;  // For stopping RPM at Idling

            // Stop all sounds
           
            ivibrating = 0; // Stop Vibrating after Rev Limter Stops

      //      SoundEngineZX10.rpm1 = 0;
            Debug.Log("RPM in Shutting 1");
                Debug.Log(SoundEngineZX10.rpm1);
            RemoveRPMConsole();
            RPM0.enabled = true;
            ConsoleRPM0.SetActive(true);
        }

        // Throttle Spring back Module-reverts to zero position when  user leaves throttle

        if (istart == 1)
        {
            // Touch Release Module
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
                        //                Debug.Log("Touch Begun");
                        //             message = "Begun ";
                        break;
                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                        if (gear == 0) mSlider.value = 0.0f;

                        break;
                }
            }
            // Mouse Release Module
            if (Input.GetMouseButtonUp(0))
            {

                //      Debug.Log("Pressed primary button.");
                if (gear == 0) mSlider.value = 0.0f;
            }
        }
        rpm1 = SoundEngineZX10.rpm1;  // Reading RPM from SoundEngine
//       Debug.Log("RPM in MinController");
 //       Debug.Log(SoundEngineZX10.rpm1);
        //    Debug.Log("RPM in MainController");
        //    Debug.Log(SoundEngineZX10.rpm1);
        trate = (sliderval - slidervalold);   // Calculating throttle rate (of change) to decide appropriate sound cycle in Sound Engine
        throttlediff = (sliderval * 150 - rpm1); // Calculating throttle difference to decide appropriate sound cycle in Sound Engine

        //Refresh Throttle Consoles
        slidervalold = sliderval;  // Assign Slidervalue to Slidrold
        RemoveRPMConsole();
        RPMConsoleUpdate();   
    }


    public void RPMConsoleUpdate()
    {

        // Idling Pulsating Needle

        if (irpm == 0)
        {
            if (iidleneedlemoved == 0)
            {
                int randidle = Random.Range(0, 10);
                if (randidle >= 5)
                {
                    RPM1000.enabled = true;
                    RPM1100.enabled = false;
                }
                else if (randidle == 0)
                {
                    iidleneedlemoved = 1;
            //        StartCoroutine("EnableIdleRPMNeedleMove");
                    //                   yield return new WaitForSeconds(0.25f);
                    RPM1100.enabled = true;
                    RPM1000.enabled = false;
                }
            }
        }
        if (rpm1 == 0) RPM0.enabled = true;
        if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
        if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
        if (rpm1 >= 1500 && rpm1 < 2000) RPM1500.enabled = true;
        if (rpm1 >= 2000 && rpm1 < 2200) RPM2000.enabled = true;
        if (rpm1 >= 2200 && rpm1 < 2500) RPM2200.enabled = true;
        if (rpm1 >= 2500 && rpm1 < 2800) RPM2500.enabled = true;
        if (rpm1 >= 2800 && rpm1 < 3000) RPM2800.enabled = true;
        if (rpm1 >= 3000 && rpm1 < 3200) RPM3000.enabled = true;
        if (rpm1 >= 3200 && rpm1 < 3500) RPM3500.enabled = true;
        if (rpm1 >= 3500 && rpm1 < 3800) RPM3800.enabled = true;
        if (rpm1 >= 3800 && rpm1 < 4000) RPM4000.enabled = true;
        if (rpm1 >= 4000 && rpm1 < 4200) RPM4200.enabled = true;
        if (rpm1 >= 4200 && rpm1 < 4500) RPM4500.enabled = true;
        if (rpm1 >= 4500 && rpm1 < 4800) RPM4800.enabled = true;
        if (rpm1 >= 4800 && rpm1 < 5000) RPM5000.enabled = true;
        if (rpm1 >= 5000 && rpm1 < 5200) RPM5200.enabled = true;
        if (rpm1 >= 5200 && rpm1 < 5500) RPM5500.enabled = true;
        if (rpm1 >= 5500 && rpm1 < 6000) RPM6000.enabled = true;
        if (rpm1 >= 6000 && rpm1 < 6500) RPM6500.enabled = true;
        if (rpm1 >= 6500 && rpm1 < 7000) RPM7000.enabled = true;
        if (rpm1 >= 7000 && rpm1 < 7500) RPM7500.enabled = true;
        if (rpm1 >= 7500 && rpm1 < 8000) RPM8000.enabled = true;
        if (rpm1 >= 8000 && rpm1 < 8500) RPM8500.enabled = true;
        if (rpm1 >= 8500 && rpm1 < 9000) RPM9000.enabled = true;
        if (rpm1 >= 9000 && rpm1 < 9500) RPM9500.enabled = true;
        if (rpm1 >= 9500 && rpm1 < 10000) RPM10000.enabled = true;
        if (rpm1 >= 10000 && rpm1 < 10500) RPM10500.enabled = true;
        if (rpm1 >= 10500 && rpm1 < 11000) RPM11000.enabled = true;
        if (rpm1 >= 11000 && rpm1 < 11500) RPM11500.enabled = true;
        if (rpm1 >= 11500 && rpm1 < 12000) RPM12000.enabled = true;
        if (rpm1 >= 12000 && rpm1 < 12500) RPM12500.enabled = true;
        if (rpm1 >= 12500 && rpm1 < 13000) RPM13000.enabled = true;
        if (rpm1 >= 13000 && rpm1 < 13500) RPM13500.enabled = true;
        if (rpm1 >= 13500 && rpm1 < 14000) RPM14000.enabled = true;
        if (rpm1 >= 14000 && rpm1 < 14500) RPM14500.enabled = true;
        if (rpm1 >= 14500 && rpm1 <= 15000) RPM15000.enabled = true;
        //         if (rpm1 > 14000) RedlineButton.enabled = true;
        else if (rpm1 < 14000) RedlineButton.enabled = false;


        // Console RPM Bars*****************************************************
        if (rpm1 > 1500 && irpm == 0)
        {
            //       ConsoleRPM500.SetActive(true);
            //      ConsoleRPM1000.SetActive(true);
            //      ConsoleRPM1100.SetActive(true);
        }

        if (rpm1 > 1500 && irpm != 0)
        {
            //      ConsoleRPM500.SetActive(true);
            //      ConsoleRPM1000.SetActive(true);
            //      ConsoleRPM1100.SetActive(true);
        }

        if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0)
        {
            //       ConsoleRPM500.SetActive(true);
            ConsoleRPM1000.SetActive(true);
        }
        if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0)
        {
            //             ConsoleRPM500.SetActive(true);
            ConsoleRPM1000.SetActive(true);
        }
        if (rpm1 == 0 && ikeyinfromtouch != 0) ConsoleRPM0.SetActive(true);
        if (rpm1 >= 1500 && rpm1 < 2000) ConsoleRPM1500.SetActive(true);
        if (rpm1 >= 2000 && rpm1 < 2200) ConsoleRPM2000.SetActive(true);
        if (rpm1 >= 2200 && rpm1 < 2500) ConsoleRPM2200.SetActive(true);
        if (rpm1 >= 2500 && rpm1 < 2800) ConsoleRPM2500.SetActive(true);
        if (rpm1 >= 2800 && rpm1 < 3000) ConsoleRPM2800.SetActive(true);
        if (rpm1 >= 3000 && rpm1 < 3200) ConsoleRPM3000.SetActive(true);
        if (rpm1 >= 3200 && rpm1 < 3500) ConsoleRPM3500.SetActive(true);
        if (rpm1 >= 3500 && rpm1 < 3800) ConsoleRPM3800.SetActive(true);
        if (rpm1 >= 3800 && rpm1 < 4000) ConsoleRPM4000.SetActive(true);
        if (rpm1 >= 4000 && rpm1 < 4200) ConsoleRPM4200.SetActive(true);
        if (rpm1 >= 4200 && rpm1 < 4500) ConsoleRPM4500.SetActive(true);
        if (rpm1 >= 4500 && rpm1 < 4800) ConsoleRPM4800.SetActive(true);
        if (rpm1 >= 4800 && rpm1 < 5000) ConsoleRPM5000.SetActive(true);
        if (rpm1 >= 5000 && rpm1 < 5200) ConsoleRPM5200.SetActive(true);
        if (rpm1 >= 5200 && rpm1 < 5500) ConsoleRPM5500.SetActive(true);
        if (rpm1 >= 5500 && rpm1 < 6000) ConsoleRPM6000.SetActive(true);

        if (rpm1 >= 6000 && rpm1 < 6500) ConsoleRPM6500.SetActive(true);
        if (rpm1 >= 6500 && rpm1 < 7000) ConsoleRPM7000.SetActive(true);

        if (rpm1 >= 7000 && rpm1 < 7500) ConsoleRPM7500.SetActive(true);
        //      if(rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM7700.SetActive(true);
        if (rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM8000.SetActive(true);
        if (rpm1 >= 8000 && rpm1 < 8500) ConsoleRPM8500.SetActive(true);
        if (rpm1 >= 8500 && rpm1 < 9000) ConsoleRPM9000.SetActive(true);

        if (rpm1 >= 9000 && rpm1 < 9500) ConsoleRPM9500.SetActive(true);

        if (rpm1 >= 9500 && rpm1 < 10000) ConsoleRPM10000.SetActive(true);


        if (rpm1 >= 10000 && rpm1 < 10500) ConsoleRPM10500.SetActive(true);
        if (rpm1 >= 10500 && rpm1 < 11000) ConsoleRPM11000.SetActive(true);
        if (rpm1 >= 11000 && rpm1 < 11500) ConsoleRPM11500.SetActive(true);

        if (rpm1 >= 11500 && rpm1 < 12000) ConsoleRPM12000.SetActive(true);
        if (rpm1 >= 12000 && rpm1 < 12500) ConsoleRPM12500.SetActive(true);
        if (rpm1 >= 12500 && rpm1 < 13000) ConsoleRPM13000.SetActive(true);
        if (rpm1 >= 13000 && rpm1 < 13500) ConsoleRPM13500.SetActive(true);
        if (rpm1 >= 13500 && rpm1 < 14000) ConsoleRPM14000.SetActive(true);
        if (rpm1 >= 14000 && rpm1 < 14500) ConsoleRPM14500.SetActive(true);
        if (rpm1 >= 14500 && rpm1 <= 15000) ConsoleRPM15000.SetActive(true);

        //*************************************************************************************
        

    }
    public void ExhaustStock()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            iexhaust = 0;
            //       exhaustDropDown = 0;
            StockMuffler.SetActive(true); // Disapperance of Stovk Muffler
            StockExhaustTick.SetActive(true); // Tick on Stock Ex Panel
            Akra1ExhaustTick.SetActive(false); // Tick vanish on Akra1 Ex Panel
            Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel

            StockMufflerShield.SetActive(true); // Disapperance of Stovk Muffler Shield
            AkraCarbonMuffler.SetActive(false); // Disapperance of Akra Carbon Muffler                                   // Akra1Muffler.SetActive(false); // Disapperance of Stovk Muffler
            AkraBlackMuffler.SetActive(false); // Disapperance of Stovk Muffler
            StockMufflerSoundSource.SetActive(true);  //Activate Akra1 Sound Sources
            Akra1MufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
        }

    }
    public void ExhaustAkra1()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            if (MainControllerZX10.ZX10ExhaustAkra1 == 1)  // Only Activate when Akra1 Purchased
            {
                iexhaust = 1;
                //     exhaustDropDown = 0;
                StockExhaustTick.SetActive(false); // Disapperance of Stovk Muffler
                Akra1ExhaustTick.SetActive(true); // Tick on Stock Ex Panel
                Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel


                //     ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
                StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler Shield
                                                     //   Akra1Muffler.SetActive(true); // Disapperance of Stovk Muffler
                AkraCarbonMuffler.SetActive(false); // Disapperance of Stovk Muffler
                AkraBlackMuffler.SetActive(true); // Disapperance of Stovk Muffler
                StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
                Akra1MufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
            }
        }
    }

    public void ExhaustAkra1Carbon()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            if (ZX10ExhaustTBR1 == 1)  // Only Activate when Akra1 Purchased
            {
                iexhaust = 1;
                //     exhaustDropDown = 0;
                StockExhaustTick.SetActive(false); // Disapperance of Stovk Muffler
                Akra1CarbonExhaustTick.SetActive(true); // Tick on Stock Ex Panel
                Akra1ExhaustTick.SetActive(false); // Tick on Stock Ex Panel

                //     ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                AkraBlackMuffler.SetActive(false); // Disapperance of Akra1 Muffler
                StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
                StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler Shield
                                                     //   Akra1Muffler.SetActive(true); // Disapperance of Stovk Muffler
                AkraCarbonMuffler.SetActive(true); // Disapperance of Stovk Muffler
                StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
                Akra1MufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
            }
        }
    }

    // Keyin from Touch
    public void KeyInFromTouch()
    {
        //      moveSpeed = newSpeed;

        if (ikeyinfromtouch == 0)
        {
            ikeyinfromtouch = 1;
            // Headlight on Module
            Hlight.HeadlightOn();
            Rlight.RearlightOn();
            ConsoleRPM0.SetActive(true);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(false);
            Debug.Log("Ignition On");
        }
        else if (ikeyinfromtouch == 2 && istartfromtouch != 0)
        {
            Debug.Log("Loop1");
            ikeyinfromtouch = 0;
            istartfromtouch = 3;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
            StartCoroutine("DelayRemoveConsoleRPM0");  //Due to inbility to remove Console RPM0 after directly shutting engine from key
            Debug.Log("Ignition Off1");
        }
        else if (ikeyinfromtouch == 2)
        {
            ikeyinfromtouch = 0;
            //    Debug.Log("Loop2");
            RedButtonOn.enabled = false;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            ConsoleRPM0.SetActive(false);// COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
            Debug.Log("Ignition Off2");
        }

    }


    private IEnumerator DelayRemoveConsoleRPM0()
    {
        yield return new WaitForSeconds(0.2f);
        Debug.Log("Inside RPM0 Off");
        ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
    }
    public void StartStopFromTouch()
    {
        //      moveSpeed = newSpeed;
        if (ikeyinfromtouch == 2)
        {
            if (istartfromtouch == 0) istartfromtouch = 1;
            else if (istartfromtouch == 2) istartfromtouch = 3;
        }
        // Make StartStopButton Dark
        StartStopButton.enabled = false;
        StartStopButtonPressed.enabled = true;
        //       StartCoroutine("StartStopButtonAppear");
    }

    // ReAppear StartStopButton(Original)
    public void Start1StopButtonAppear()  // 
    {

        StartStopButton.enabled = true;
        StartStopButtonPressed.enabled = false;

    }
    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
     //   SoundEngineZX10.ReverbXoneMix = 0.00f;
        RoadView.SetActive(true); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage
        TunnelView.SetActive(false); // DisAppearance of Tunnel

        HeadlightReflectionR.SetActive(false);
        HeadlightReflectionL.SetActive(false);
        SEZX10.RoadAppear();
        

    }
    // DisAppearance of Racetrack Road/Garage
    public void TunnelAppear()  // 
    {
    //    SoundEngineZX10.ReverbXoneMix = 1.1f;
        TunnelView.SetActive(true); // Disapperance of Garage
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);
        SEZX10.TunnelAppear();
     
    }


    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {
    //    SoundEngineZX10.ReverbXoneMix = 1.03f;
        RoadView.SetActive(false); // DisAppearance of Road
        TunnelView.SetActive(false); // DisAppearance of Tunnel
        GarageView.SetActive(true); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);
        SEZX10.GarageAppear();
      
    }
    // Idling needle Enable Move
    private IEnumerator EnableIdleRPMNeedleMove()
    {
        yield return new WaitForSeconds(0.5f);
        iidleneedlemoved = 0;
    }
    IEnumerator Startistart1()
    {
        yield return new WaitForSeconds(0.5f);
        istart = 1;
    }



    // Start Idling  
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(startertime); // wait half a secon


        //     AStarting.StartingStop();
        SEZX10.StartIdlingfromMainController();  // Start Idling sound in Sound Engine
  //     irpm = 0;
//        rpm1 = rpmidling;
 //       if (MainControllerZX10.iexhaust == 0) AIdling.Idling();
 //       if (MainControllerZX10.iexhaust == 1) AIdlingAkra1.IdlingAkra1();
        // Red Console button Off
        RedButtonOn.enabled = false;
    }


   
    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        sliderval = sliderposition;

    }

    // Exhaust Pop/Backfire
    void Explode()
    {
        AVExhaustPop.ExhaustPop();
    }

    public void PopCrackleEnable()  // Enable/Disable Pop Crackles
    {
        if (SEZX10.iPopCrackle == 0)
        {
            Debug.Log("Pop Enable");
            SEZX10.iPopCrackle = 1;
            PopsCrackles1Tick.SetActive(true);


        }
        else if (SEZX10.iPopCrackle == 1)
        {
            Debug.Log("Pop EDisable");
            SEZX10.iPopCrackle = 0;
            PopsCrackles1Tick.SetActive(false);
        }
    }

    // Console RPM Remove
    public void RemoveRPMConsole()
    {
        RPM0.enabled = false;
        RPM1000.enabled = false;
        RPM1100.enabled = false;
        RPM1500.enabled = false;
        RPM2000.enabled = false;
        RPM2200.enabled = false;
        RPM2500.enabled = false;
        RPM2800.enabled = false;
        RPM3000.enabled = false;
        RPM3200.enabled = false;
        RPM3500.enabled = false;
        RPM3800.enabled = false;
        RPM4000.enabled = false;
        RPM4200.enabled = false;
        RPM4500.enabled = false;
        RPM4800.enabled = false;
        RPM5000.enabled = false;
        RPM5200.enabled = false;
        RPM5500.enabled = false;
        RPM6000.enabled = false;
        RPM6500.enabled = false;
        RPM7000.enabled = false;
        RPM7500.enabled = false;

        RPM8000.enabled = false;
        RPM8500.enabled = false;
        RPM9000.enabled = false;
        RPM9500.enabled = false;
        RPM10000.enabled = false;
        RPM10500.enabled = false;
        RPM11000.enabled = false;
        RPM11500.enabled = false;
        RPM12000.enabled = false;
        RPM12500.enabled = false;
        RPM13000.enabled = false;
        RPM13500.enabled = false;
        RPM14000.enabled = false;
        RPM14500.enabled = false;
        RPM15000.enabled = false;


        // RPM Console Section


        //        for (int irpmconsolexxx = 0; irpmconsolexxx < 15000; irpmconsolexxx = irpmconsolexxx + 100)
        //        {
        //            string name = $"ConsoleRPM " + "rpm1";
        //          ConsoleRPM500.name = "name";
        //          ConsoleRPM500.SetActive(false);
        //      }


        // Console View Bars
        //     ConsoleRPMOff.SetActive(false);
        ConsoleRPM0.SetActive(false);
        //      ConsoleRPM500.SetActive(false);      
        ConsoleRPM1000.SetActive(false);
        ConsoleRPM1500.SetActive(false);
        ConsoleRPM2000.SetActive(false);
        ConsoleRPM2200.SetActive(false);
        ConsoleRPM2500.SetActive(false);
        ConsoleRPM2800.SetActive(false);
        ConsoleRPM3000.SetActive(false);
        ConsoleRPM3200.SetActive(false);
        ConsoleRPM3500.SetActive(false);
        ConsoleRPM3800.SetActive(false);
        ConsoleRPM4000.SetActive(false);
        ConsoleRPM4200.SetActive(false);
        ConsoleRPM4500.SetActive(false);
        ConsoleRPM4800.SetActive(false);
        ConsoleRPM5000.SetActive(false);
        ConsoleRPM5200.SetActive(false);
        ConsoleRPM5500.SetActive(false);
        ConsoleRPM6000.SetActive(false);
        ConsoleRPM6500.SetActive(false);
        ConsoleRPM7000.SetActive(false);
        ConsoleRPM7500.SetActive(false);
        ConsoleRPM7700.SetActive(false);
        ConsoleRPM8000.SetActive(false);
        ConsoleRPM8200.SetActive(false);
        ConsoleRPM8500.SetActive(false);
        ConsoleRPM8800.SetActive(false);
        ConsoleRPM9000.SetActive(false);
        ConsoleRPM9200.SetActive(false);
        ConsoleRPM9500.SetActive(false);
        ConsoleRPM9800.SetActive(false);
        ConsoleRPM10000.SetActive(false);
        ConsoleRPM10200.SetActive(false);
        ConsoleRPM10500.SetActive(false);
        ConsoleRPM10800.SetActive(false);
        ConsoleRPM11000.SetActive(false);
        ConsoleRPM11200.SetActive(false);
        ConsoleRPM11500.SetActive(false);
        ConsoleRPM11800.SetActive(false);
        ConsoleRPM12000.SetActive(false);
        ConsoleRPM12200.SetActive(false);
        ConsoleRPM12500.SetActive(false);
        ConsoleRPM12800.SetActive(false);
        ConsoleRPM13000.SetActive(false);
        ConsoleRPM13200.SetActive(false);
        ConsoleRPM13500.SetActive(false);
        ConsoleRPM13800.SetActive(false);
        ConsoleRPM14000.SetActive(false);
        ConsoleRPM14200.SetActive(false);
        ConsoleRPM14500.SetActive(false);
        ConsoleRPM14800.SetActive(false);
        ConsoleRPM15000.SetActive(false);







        //     ConsoleRPM1100.SetActive(false);
        //     ConsoleRPM1500.SetActive(false);
        //     ConsoleRPM2000.SetActive(false);
        //     ConsoleRPM2200.SetActive(false);
        //     ConsoleRPM2500.SetActive(false);
        //     ConsoleRPM2800.SetActive(false);
        //     ConsoleRPM3000.SetActive(false);


    }

    // Console Startup Sequence************************************************************
    int rpmi = 1000;   // Counter for rpm change for Console Startup Sequence
                       //   float wait=0.1;
    private IEnumerator ConsoleStartup()
    {
        yield return new WaitForSeconds(1f);
        while (iconsolestartup == 1 || iconsolestartup == -1)
        {
            yield return new WaitForSeconds(0.01f); // wait half a second

            RemoveRPMConsole();
            //     if (rpmi <= 7400)
            //     {
            if (iconsolestartup == 1 && rpmi <= 14500)
            {
                rpmi = rpmi + iconsolestartuprate * 2;
            }
            else iconsolestartup = -1;
            if (iconsolestartup == -1 && rpmi >= 1000)
            {
                rpmi = rpmi - iconsolestartuprate * 2;
                if (rpmi <= 900)
                {
                    //              RPM0.enabled = true;
                    rpmi = 0;
                    iconsolestartup = 0;
                }
            }

            if (rpmi < 900) RPM0.enabled = true;
            if (rpmi >= 900 && rpmi < 1500) RPM1000.enabled = true;
            if (rpmi >= 1500 && rpmi < 2000) RPM1500.enabled = true;
            if (rpmi >= 2000 && rpmi < 2200) RPM2000.enabled = true;
            if (rpmi >= 2200 && rpmi < 2500) RPM2200.enabled = true;
            if (rpmi >= 2500 && rpmi < 2800) RPM2500.enabled = true;
            if (rpmi >= 2800 && rpmi < 3000) RPM2800.enabled = true;
            if (rpmi >= 3000 && rpmi < 3200) RPM3000.enabled = true;
            if (rpmi >= 3200 && rpmi < 3500) RPM3500.enabled = true;
            if (rpmi >= 3500 && rpmi < 3800) RPM3800.enabled = true;
            if (rpmi >= 3800 && rpmi < 4000) RPM4000.enabled = true;
            if (rpmi >= 4000 && rpmi < 4200) RPM4200.enabled = true;
            if (rpmi >= 4200 && rpmi < 4500) RPM4500.enabled = true;
            if (rpmi >= 4500 && rpmi < 4800) RPM4800.enabled = true;
            if (rpmi >= 4800 && rpmi < 5000) RPM5000.enabled = true;
            if (rpmi >= 5000 && rpmi < 5200) RPM5200.enabled = true;
            if (rpmi >= 5200 && rpmi < 5500) RPM5500.enabled = true;
            if (rpmi >= 5500 && rpmi < 6000) RPM6000.enabled = true;
            if (rpmi >= 6000 && rpmi < 6500) RPM6500.enabled = true;
            if (rpmi >= 6500 && rpmi < 7000) RPM7000.enabled = true;
            if (rpmi >= 7000 && rpmi < 7500) RPM7500.enabled = true;
            if (rpmi >= 7500 && rpmi < 8000) RPM8000.enabled = true;
            if (rpmi >= 8000 && rpmi < 8500) RPM8500.enabled = true;
            if (rpmi >= 8500 && rpmi < 9000) RPM9000.enabled = true;
            if (rpmi >= 9000 && rpmi < 9500) RPM9500.enabled = true;
            if (rpmi >= 9500 && rpmi < 10000) RPM10000.enabled = true;
            if (rpmi >= 10000 && rpmi < 10500) RPM10500.enabled = true;
            if (rpmi >= 10500 && rpmi < 11000) RPM11000.enabled = true;
            if (rpmi >= 11000 && rpmi < 11500) RPM11500.enabled = true;
            if (rpmi >= 11500 && rpmi < 12000) RPM12000.enabled = true;
            if (rpmi >= 12000 && rpmi < 12500) RPM12500.enabled = true;
            if (rpmi >= 12500 && rpmi < 13000) RPM13000.enabled = true;
            if (rpmi >= 13000 && rpmi < 13500) RPM13500.enabled = true;
            if (rpmi >= 13500 && rpmi < 14000) RPM14000.enabled = true;
            if (rpmi >= 14000 && rpmi < 14500) RPM14500.enabled = true;
            if (rpmi >= 14500 && rpmi < 15000) RPM15000.enabled = true;


            // Console RPM Bars*****************************************************


            if (rpmi < 900) ConsoleRPM0.SetActive(true);
            if (rpmi >= 900 && rpmi < 1500) ConsoleRPM1000.SetActive(true);
            if (rpmi >= 1500 && rpmi < 2000) ConsoleRPM1500.SetActive(true);

            if (rpmi >= 2000 && rpmi < 2200) ConsoleRPM2000.SetActive(true);
            if (rpmi >= 2200 && rpmi < 2500) ConsoleRPM2200.SetActive(true);
            if (rpmi >= 2500 && rpmi < 2800) ConsoleRPM2500.SetActive(true);
            if (rpmi >= 2800 && rpmi < 3000) ConsoleRPM2800.SetActive(true);
            if (rpmi >= 3000 && rpmi < 3200) ConsoleRPM3000.SetActive(true);
            if (rpmi >= 3200 && rpmi < 3500) ConsoleRPM3500.SetActive(true);
            if (rpmi >= 3500 && rpmi < 3800) ConsoleRPM3800.SetActive(true);
            if (rpmi >= 3800 && rpmi < 4000) ConsoleRPM4000.SetActive(true);
            if (rpmi >= 4000 && rpmi < 4200) ConsoleRPM4200.SetActive(true);
            if (rpmi >= 4200 && rpmi < 4500) ConsoleRPM4500.SetActive(true);
            if (rpmi >= 4500 && rpmi < 4800) ConsoleRPM4800.SetActive(true);
            if (rpmi >= 4800 && rpmi < 5000) ConsoleRPM5000.SetActive(true);
            if (rpmi >= 5000 && rpmi < 5200) ConsoleRPM5200.SetActive(true);
            if (rpmi >= 5200 && rpmi < 5500) ConsoleRPM5500.SetActive(true);
            if (rpmi >= 5500 && rpmi < 6000) ConsoleRPM6000.SetActive(true);

            if (rpmi >= 6000 && rpmi < 6500) ConsoleRPM6500.SetActive(true);
            if (rpmi >= 6500 && rpmi < 7000) ConsoleRPM7000.SetActive(true);

            if (rpmi >= 7000 && rpmi < 7500) ConsoleRPM7500.SetActive(true);
            //      if(rpmi >= 7500 && rpmi < 8000) ConsoleRPM7700.SetActive(true);
            if (rpmi >= 7500 && rpmi < 8000) ConsoleRPM8000.SetActive(true);
            if (rpmi >= 8000 && rpmi < 8500) ConsoleRPM8500.SetActive(true);
            if (rpmi >= 8500 && rpmi < 9000) ConsoleRPM9000.SetActive(true);

            if (rpmi >= 9000 && rpmi < 9500) ConsoleRPM9500.SetActive(true);

            if (rpmi >= 9500 && rpmi < 10000) ConsoleRPM10000.SetActive(true);


            if (rpmi >= 10000 && rpmi < 10500) ConsoleRPM10500.SetActive(true);
            if (rpmi >= 10500 && rpmi < 11000) ConsoleRPM11000.SetActive(true);
            if (rpmi >= 11000 && rpmi < 11500) ConsoleRPM11500.SetActive(true);

            if (rpmi >= 11500 && rpmi < 12000) ConsoleRPM12000.SetActive(true);
            if (rpmi >= 12000 && rpmi < 12500) ConsoleRPM12500.SetActive(true);
            if (rpmi >= 12500 && rpmi < 13000) ConsoleRPM13000.SetActive(true);
            if (rpmi >= 13000 && rpmi < 13500) ConsoleRPM13500.SetActive(true);
            if (rpmi >= 13500 && rpmi < 14000) ConsoleRPM14000.SetActive(true);
            if (rpmi >= 14000 && rpmi < 14500) ConsoleRPM14500.SetActive(true);
            if (rpmi >= 14500 && rpmi < 15000) ConsoleRPM15000.SetActive(true);





        }
        //      newImage = Image.FromFile(@"C:/User Data/VS/Software/Bass/Combined-Project/Blank-Application/Images/" + rpmString + ".png");
    }

}
