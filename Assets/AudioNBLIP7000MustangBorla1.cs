﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000MustangBorla1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (AudioEngineMustang.itrate == 5006)
        {
            AudioEngineMustang.rpm1 = 7000;
            //           Debug.Log(AudioEngineMustang.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineMustang.rpm1 >= 6000 && AudioEngineMustang.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (AudioEngineMustang.rpm1 >= 5000 && AudioEngineMustang.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.053F);
        }
        else if (AudioEngineMustang.rpm1 >= 4000 && AudioEngineMustang.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.214F);
        }
        else if (AudioEngineMustang.rpm1 >= 3000 && AudioEngineMustang.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.282F);
        }
        else if (AudioEngineMustang.rpm1 >= 2000 && AudioEngineMustang.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.282F);
        }


        else if (AudioEngineMustang.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.484F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }
}