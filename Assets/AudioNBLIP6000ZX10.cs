using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000ZX10 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX10.itrate == 5005)
        {
   //         SoundEngineZX10.rpm1 = 6000;
            //           Debug.Log(SoundEngineZX10.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineZX10.rpm1 >= 5000 && SoundEngineZX10.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (SoundEngineZX10.rpm1 >= 4000 && SoundEngineZX10.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.02F);
        }
        else if (SoundEngineZX10.rpm1 >= 3000 && SoundEngineZX10.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.072F);
        }
        else if (SoundEngineZX10.rpm1 >= 2000 && SoundEngineZX10.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.136F);
        }
        else if (SoundEngineZX10.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.232F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }
}


