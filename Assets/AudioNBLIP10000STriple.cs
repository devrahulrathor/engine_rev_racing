using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP10000STriple : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void Blip10000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineSTriple.rpm1 >= 7000 && AudioEngineSTriple.rpm1 < 8000)
        {
            audio.Play();
        }
        else if (AudioEngineSTriple.rpm1 >= 6000 && AudioEngineSTriple.rpm1 < 7000)
        {
       //     audio.PlayScheduled(AudioSettings.dspTime + 0.016F);
       //     audio.PlayScheduled(AudioSettings.dspTime + 0.052F);
            audio.PlayScheduled(AudioSettings.dspTime + 0.0F);
        }
        else if (AudioEngineSTriple.rpm1 >= 5000 && AudioEngineSTriple.rpm1 < 6000)
        {
      //      audio.PlayScheduled(AudioSettings.dspTime + 0.027F);
       //     audio.PlayScheduled(AudioSettings.dspTime + 0.098F);
            audio.PlayScheduled(AudioSettings.dspTime + 0.052F);
        }
        else if (AudioEngineSTriple.rpm1 >= 4000 && AudioEngineSTriple.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.048F);
       //     audio.PlayScheduled(AudioSettings.dspTime + 0.156F);
         //   audio.PlayScheduled(AudioSettings.dspTime + 0.098F);
        }
        else if (AudioEngineSTriple.rpm1 >= 3000 && AudioEngineSTriple.rpm1 < 4000)
        {
          audio.PlayScheduled(AudioSettings.dspTime + 0.099F);
        //    audio.PlayScheduled(AudioSettings.dspTime + 0.156F);
        }
        else if (AudioEngineSTriple.rpm1 >= 2000 && AudioEngineSTriple.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.156F);
         //   audio.PlayScheduled(AudioSettings.dspTime + 0.201F);
        }


        else if (AudioEngineSTriple.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.301F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip10000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip10000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip10000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //   audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}

