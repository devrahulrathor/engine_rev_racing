﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {


    public void ZX10RStock()
    {
        SceneManager.LoadScene(2);
    }

    public void Ninja250Stock()
    {
        SceneManager.LoadScene(3);
    }

    public void HuracanStock()
    {
        SceneManager.LoadScene(4);
    }

    public void AltoFiestaStock()
    {
        SceneManager.LoadScene(5);
    }

    //______________________________________________________

    public void Inline4Superbike()
    {
        SceneManager.LoadScene(1);
    }

    public void ZX10ROptions()
    {
        SceneManager.LoadScene(1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void Ninja250()
    {
        SceneManager.LoadScene(4);
    }

    public void Alto()
    {
        SceneManager.LoadScene(5);
    }

}
