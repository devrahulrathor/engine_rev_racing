﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailLightRightEmission : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TailLightEmissionOn()
    {
        var materials = GetComponent<Renderer>().materials;
        //      materials[3].SetColor("_EmissionColor", Color.blue);

        // Another thing to note is that Unity 5 uses the concept of shader keywords extensively.
        // So if your material is initially configured to be without emission, then in order to enable emission, you need to enable // the keyword.
        materials[0].EnableKeyword("_EMISSION");
    }

    public void TailLightEmissionOff()
    {
        var materials = GetComponent<Renderer>().materials;
        //      materials[3].SetColor("_EmissionColor", Color.blue);

        // Another thing to note is that Unity 5 uses the concept of shader keywords extensively.
        // So if your material is initially configured to be without emission, then in order to enable emission, you need to enable // the keyword.
        materials[0].DisableKeyword("_EMISSION");
    }

}
