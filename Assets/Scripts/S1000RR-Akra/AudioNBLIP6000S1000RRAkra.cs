﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000S1000RRAkra : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (SoundEngineS1000RR.itrate == 5005)
        {
            SoundEngineS1000RR.rpm1 = 6000;
            //           Debug.Log(SoundEngineS1000RR.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {

       
        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineS1000RR.rpm1 >= 5000 && SoundEngineS1000RR.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (SoundEngineS1000RR.rpm1 >= 4000 && SoundEngineS1000RR.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.044F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 3000 && SoundEngineS1000RR.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.151F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 2000 && SoundEngineS1000RR.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.21F);
        }
        else if (SoundEngineS1000RR.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.253F);
        audio.volume = 1;
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}

