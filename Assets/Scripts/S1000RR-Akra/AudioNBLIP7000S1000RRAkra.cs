﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000S1000RRAkra : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
   //         Debug.Log("Blip7000 is Playing***********************");
        }
            if (SoundEngineS1000RR.itrate == 5006)
        {
            SoundEngineS1000RR.rpm1 = 7000;
            //           Debug.Log(SoundEngineS1000RR.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {
        
        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineS1000RR.rpm1 >= 6000 && SoundEngineS1000RR.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (SoundEngineS1000RR.rpm1 >= 5000 && SoundEngineS1000RR.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.062F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 4000 && SoundEngineS1000RR.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.106F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 3000 && SoundEngineS1000RR.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.213F);
        }
            else if (SoundEngineS1000RR.rpm1 >= 2000 && SoundEngineS1000RR.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.272F);
        }


        else if (SoundEngineS1000RR.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.315F);
        audio.volume = 1;
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}
