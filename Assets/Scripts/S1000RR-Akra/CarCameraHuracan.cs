﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCameraHuracan : MonoBehaviour
{

    public GameObject TheCar;
    public float CarX;
    public float CarY;
    public float CarZ;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CarX = TheCar.transform.eulerAngles.x;
        CarY = TheCar.transform.eulerAngles.y;
        CarZ = TheCar.transform.eulerAngles.z;
    }

}