﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStartupS1000RRAkra : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Startup()
    {

        AudioSource audio = GetComponent<AudioSource>();

        audio.Play();
        audio.Play(44100);


    }

    public void StartupReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void StartupReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineS1000RRAkra.ReverbXoneMix;
    }
}
