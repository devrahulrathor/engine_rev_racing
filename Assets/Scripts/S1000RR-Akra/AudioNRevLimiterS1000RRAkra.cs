﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterS1000RRAkra : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
            if (SoundEngineS1000RR.itrate == 7000)
            {
            //      RPMCal();
            // SoundEngineS1000RR.rpm1 = 15000;
           
        }
     
	}

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }
    

    IEnumerator RevLimiterRPMFluctuation()
    {
      
        while (SoundEngineS1000RR.ivibrating == 1)
        {
  //          SoundEngineS1000RR.rpm1 = 15000;
            yield return new WaitForSeconds(0.08f);
     //       if (SoundEngineS1000RR.ivibrator == 1)
     //       {
                if (SoundEngineS1000RR.rpm1 == 8500) SoundEngineS1000RR.rpm1 = 8000;
                else SoundEngineS1000RR.rpm1 = 8500;
            //          else if (SoundEngineS1000RR.rpm1 == 14000) SoundEngineS1000RR.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}
