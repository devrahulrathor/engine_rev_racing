﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScriptCrackle4000S1000RR : MonoBehaviour {
//    float dtcrackle4000 = 0.14f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Crackle4000()
    {
        //      StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayScheduled(AudioSettings.dspTime + SoundEngineS1000RR.dtcrackle);
        //       audio.Play();
        audio.Play(44100);
    }

    public void Crackle4000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Crackle4000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}
