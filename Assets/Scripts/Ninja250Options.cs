﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ninja250Options : MonoBehaviour
{

    // Use this for initialization

    public void Ninja250()
    {
        SceneManager.LoadScene(3);
    }


    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void BackButton()
    {
        if (AudioEngineNinja250.itrate != -1) AudioEngineNinja250.istartfromtouch = 3;
        SceneManager.LoadScene(0);   // Motorbike Options
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
