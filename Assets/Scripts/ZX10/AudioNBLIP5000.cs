﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP5000 : MonoBehaviour {
    public static float dt5000 = 0;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngine.itrate == 5004)
        {
            AudioEngine.rpm1 = 5000;
            //           Debug.Log(AudioEngine.rpm1);
        }
    }
    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip5000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngine.rpm1 >= 4000 && AudioEngine.rpm1 < 5000)
        {
            audio.Play();
        }
        else if (AudioEngine.rpm1 >= 3000 && AudioEngine.rpm1 < 4000) {
            dt5000 = 0.052f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
            }
        else if (AudioEngine.rpm1 >= 2000 && AudioEngine.rpm1 < 3000) {
            dt5000 = 0.096f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (AudioEngine.rpm1 < 2000)
            dt5000 = 0.212f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        audio.volume = 1;
      
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip5000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip5000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip5000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //    audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}


