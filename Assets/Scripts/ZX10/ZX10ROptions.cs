﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZX10ROptions : MonoBehaviour
{

    // Use this for initialization

    public void ZX10RStock()
    {
        SceneManager.LoadScene(2);
    }

    public void ZX10RAkra()
    {
        SceneManager.LoadScene(5);
    }
    public void BackButton()
    {
        if (AudioEngine.itrate != -1) AudioEngine.istartfromtouch = 3;
        // Revert Factory Settings
     //   PlayerPrefs.SetInt("Pops", 0);  // Steering Buttons Enabled

        SceneManager.LoadScene(0);  // Motorbike Options
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}