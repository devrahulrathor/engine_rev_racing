﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000Alto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineAlto.itrate == 5005)
        {
            AudioEngineAlto.rpm1 = 6000;
            //           Debug.Log(AudioEngineAlto.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {

       
        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineAlto.rpm1 >= 5000 && AudioEngineAlto.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineAlto.rpm1 >= 4000 && AudioEngineAlto.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.02F);
        }
        else if (AudioEngineAlto.rpm1 >= 3000 && AudioEngineAlto.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.072F);
        }
        else if (AudioEngineAlto.rpm1 >= 2000 && AudioEngineAlto.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.136F);
        }
        else if (AudioEngineAlto.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.232F);
        audio.volume = 1;
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}

