﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNFSR15000Alto : MonoBehaviour {
    int[] posrpmarrayfsr15000 = { 14000, 15000, 15000, 12000, 10000, 10000, 10000, 10000, 9000, 9000, 7500, 7500, 6000, 5000, 5000, 5000, 4200, 3500, 3200, 3000, 2500, 2500, 2000, 1600, 1200, 1200 };

    double[] rpmposarrayfsr15000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineAlto.itrate == 6100)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineAlto.rpm1 = posrpmarrayfsr15000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineAlto.rpm1 <= (AudioEngineAlto.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }


    public void NFSR15000()
    {

        AudioSource audio = GetComponent<AudioSource>();
 //       audio.PlayScheduled(AudioSettings.dspTime + 0.504F);
       
            audio.Play();
        //      audio.Play(44100);
    }

    public void NFSR15000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    double Blip15000volume = 1.0;
    public void BLIPCR15000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip15000volume = Blip15000volume - 0.1;
        if (Blip15000volume > 0) audio.volume = (float)Blip15000volume;
        else
        {

            AudioEngineAlto.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
        }
    }


}
