﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingAlto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineAlto.irpm == 0)
        {
            AudioEngineAlto.rpm1 = AudioEngineAlto.rpmidling;
        }
    }

    public void Idling()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngineAlto.idlingvol;
       
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
 //       Idlingvolume = 0;
       
        
    }

    double Idlingvolume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

            AudioEngineAlto.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        Idlingvolume = Idlingvolume + 0.1;
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
        else
        {
            audio.volume = 1;
            AudioEngineAlto.ifadeinidling = 0;
            
            //            audio.Stop();
        }

    }
    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineAlto.ReverbXoneMix;
    }
}
