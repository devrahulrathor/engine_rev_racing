﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000Alto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
   //         Debug.Log("Blip7000 is Playing***********************");
        }
            if (AudioEngineAlto.itrate == 5006)
        {
            AudioEngineAlto.rpm1 = 7000;
            //           Debug.Log(AudioEngineAlto.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {
        
        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineAlto.rpm1 >= 6000 && AudioEngineAlto.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (AudioEngineAlto.rpm1 >= 5000 && AudioEngineAlto.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.012F);
        }
        else if (AudioEngineAlto.rpm1 >= 4000 && AudioEngineAlto.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.032F);
        }
        else if (AudioEngineAlto.rpm1 >= 3000 && AudioEngineAlto.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.084F);
        }
            else if (AudioEngineAlto.rpm1 >= 2000 && AudioEngineAlto.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.148F);
        }


        else if (AudioEngineAlto.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.244F);
        audio.volume = 1;
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

}
