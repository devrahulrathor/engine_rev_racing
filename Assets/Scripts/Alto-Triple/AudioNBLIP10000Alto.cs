﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP10000Alto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
  
    }

    public void Blip10000()
    {

        AudioSource audio = GetComponent<AudioSource>();
    
        if (AudioEngineAlto.rpm1 >= 7000 && AudioEngineAlto.rpm1 < 8000)
        {
            audio.Play();
        }
            else if (AudioEngineAlto.rpm1 >= 6000 && AudioEngineAlto.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.016F);
        }
        else if (AudioEngineAlto.rpm1 >= 5000 && AudioEngineAlto.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.027F);
        }
        else if (AudioEngineAlto.rpm1 >= 4000 && AudioEngineAlto.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.048F);
        }
        else if (AudioEngineAlto.rpm1 >= 3000 && AudioEngineAlto.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.099F);
        }
        else if (AudioEngineAlto.rpm1 >= 2000 && AudioEngineAlto.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.164F);
        }


        else if (AudioEngineAlto.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.26F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip10000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

}
