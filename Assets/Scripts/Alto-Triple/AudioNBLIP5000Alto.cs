﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP5000Alto : MonoBehaviour {
    public static float dt5000 = 0;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineAlto.itrate == 5004)
        {
            AudioEngineAlto.rpm1 = 5000;
            //           Debug.Log(AudioEngineAlto.rpm1);
        }
    }
    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip5000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineAlto.rpm1 >= 4000 && AudioEngineAlto.rpm1 < 5000)
        {
            audio.Play();
        }
        else if (AudioEngineAlto.rpm1 >= 3000 && AudioEngineAlto.rpm1 < 4000) {
            dt5000 = 0.052f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
            }
        else if (AudioEngineAlto.rpm1 >= 2000 && AudioEngineAlto.rpm1 < 3000) {
            dt5000 = 0.096f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (AudioEngineAlto.rpm1 < 2000)
            dt5000 = 0.212f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        audio.volume = 1;
      
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip5000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}


