﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP14000Alto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineAlto.itrate == 5007)
        {
    //        AudioEngineAlto.rpm1 = 14000;
            //           Debug.Log(AudioEngineAlto.rpm1);
        }
    }


    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip14000()
    {
       
        AudioSource audio = GetComponent<AudioSource>();
         if (AudioEngineAlto.rpm1 >= 8000 && AudioEngineAlto.rpm1 < 14000)
        {
            audio.Play();
        }
        else if (AudioEngineAlto.rpm1 >= 7000 && AudioEngineAlto.rpm1 < 8000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.031F);
        }
        else if (AudioEngineAlto.rpm1 >= 6000 && AudioEngineAlto.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.047F);
        }
        else if (AudioEngineAlto.rpm1 >= 5000 && AudioEngineAlto.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.058F);
        }
        else if (AudioEngineAlto.rpm1 >= 4000 && AudioEngineAlto.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.079F);
        }
        else if (AudioEngineAlto.rpm1 >= 3000 && AudioEngineAlto.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.130F);
        }


        else if (AudioEngineAlto.rpm1 >= 2000 && AudioEngineAlto.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.195F);
        }
        else if (AudioEngineAlto.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.291F);
        audio.volume = 1;
    //    audio.Play();
   //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip14000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}
