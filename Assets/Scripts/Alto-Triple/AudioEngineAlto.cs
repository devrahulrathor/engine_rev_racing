﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;

public class AudioEngineAlto : MonoBehaviour {

    double startTick;

    public int ikeyin = 0;    // Ignition Key in Counter
    public static int ikeyinfromtouch = 0;
    public static int istart = -100;    // Start Counter
    public static int istartfromtouch = 0;
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs

    public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 

    public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate=0;
    public static float throttlediff=0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = 0;
    public static int iblipcanstart = 1;  // Counter for Blip Start
    public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 1100;

    int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)
                             

    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 1200;
    
 //   public float rpmdisplayafterstart =1.4f;  // Time in seconds after which Idling RPM start post Startup
    public static int rpmblipconst;  // TO Stop Blip at different RPMs
    public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public float sliderval; 
    public float slidervalold;

    // Fade in out counters
    public static int ifadeoutnfsr = 0, ifadeinnfsr=0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)

    public static int ifadeinidling = 0, ifadeoutidling = 0;
    public static int ifadeoutsli=0, ifadeoutfsi1=0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters
    public GameObject GarageView;   // Appearance/Disappearance of Garage
    public static float ReverbXoneMix = 1.03f;   // Reverberation Value for Garage
    public GameObject RoadView;   // Appearance/Disappearance of Road
    public GameObject TunnelView;   // Appearance/Disappearance of Road


    // GamrObject for RPMConsole1 to Enlarge
    public GameObject RPMConsole1;
    public int iRPMConsole = 0;  // 1:Enlarge, 0:Reduce

    // Backfire Section
    public ExhaustPopFiesta AVExhaustPop;
    public int iPopCrackle2 = 0;  // 0-off, 1-On
    public static float dtpop = 0.75f;  // Delay on Backfire 
                                        //   public GameObject PopsCracklesHeader;  // Main Menu for Pops Crackles
    public GameObject PopsCrackles1;       // Pos Crackles1 Button
    public GameObject PopsCrackles1Tick;       // Pos Crackles1 Button Tick
                                               // Paid Options
    public AudioScriptFiestaCrackle10000 ACrackle10000;  // Crackle 10000
    public AudioScriptFiestaCrackle8000 ACrackle8000;  // Crackle 10000

    public AudioStartupAlto AStartup;
    public AudioStartingAlto AStarting;
    public AudioShuttingAlto AShutting;

    public AudioIdlingAlto AIdling;
    public AudioNBLIPAlto ANBLIP;
    public AudioNBLIP2000Ninja250 ANBLIP2000;
    public AudioNBLIP3000Alto ANBLIP3000;
    public AudioNBLIP4000Alto ANBLIP4000;
    public AudioNBLIP5000Alto ANBLIP5000;
    public AudioNBLIP6000Alto ANBLIP6000;
    public AudioNBLIP7000Alto ANBLIP7000;
    public AudioNBLIP10000Alto ANBLIP10000;
    public AudioNBLIP14000Alto ANBLIP14000;

    public AudioNBLIPC3000Alto ANBLIPC3000;
    public AudioNBLIPC4000Alto ANBLIPC4000;
    public AudioNBLIPC5500Alto ANBLIPC5500;
    public AudioNBLIPC6000Alto ANBLIPC6000;
    public AudioNBLIPC7500Alto ANBLIPC7500;
    public AudioNBLIPC10000Alto ANBLIPC10000;
    public AudioNBLIPC14000Alto ANBLIPC14000;
    public AudioNRevLimiterAlto ANRevLimiter;
    public AudioNFSR15000Alto ANFSR15000;
    public AudioNFSR7500Ninja250 ANFSR7500;
    public AudioNFSRAlto ANFSR;
    public AudioNSLIAlto ANSLI;
    public AudioNFSI1Alto ANFSI1;
    public AudioNFSI2Alto ANFSI2;

    // Headlight Object
    public HeadLights Hlight;
   public TailLight Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 

    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No

    //RateUs Variables
    public int irevcountrateus=0;  // Counter to decide when user rates when the count is greated than irevcountmaxrateus
    public int irevcountmaxrateus = 50;
    int irateusloopdisable = 0;
    int irateusdialogdisable = 0;


    [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, RPM0, RPM1000, RPM1100, RPM1500, RPM2000, RPM2100, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;

    void Awake()
    {
        Application.targetFrameRate = 1000;
    }
    // Use this for initialization
    void Start () {
    
    }

    // Update is called once per frame
    void Update()
    {
        //       slidervalold = sliderval;
        //      Debug.log(sliderval);

        //      trate = (sliderval - slidervalold);
        //        throttlediff = (sliderval * 150 - rpm1);
        // Starting Module
        //      if (Input.GetKeyDown("space") || istart == 0)

      //   PlayerPrefs.SetInt("RevCounttoRate", 0);
      //  PlayerPrefs.SetInt("RateUsPopUpDisabled", 0);
        // Rate Us Module
        if (rpm1 > 4000 && irateusloopdisable == 0)
        {
      //      Debug.Log("INside Fiesta Rateus");
            irateusloopdisable = 1;
            irevcountrateus = PlayerPrefs.GetInt("RevCounttoRate");
            irevcountrateus = irevcountrateus + 1;
            PlayerPrefs.SetInt("RevCounttoRate", irevcountrateus);
            irateusdialogdisable = PlayerPrefs.GetInt("RateUsPopUpDisabled");  // 1 If permanently disabled
            if (irevcountrateus > irevcountmaxrateus && irateusdialogdisable == 0)
            {
               
                RateGame.Instance.ShowRatePopup();
                irateusdialogdisable = 1;
                PlayerPrefs.SetInt("RateUsPopUpDisabled", 1);
            }
        }
        if(rpm1 < 4000) irateusloopdisable = 0;

        // RateUs End

        // Ingnition On Module
        if (ikeyinfromtouch == 1)
        {
            ikeyinfromtouch = 2;
            //         NeutralButtonOff.enabled = true;

            // Ignition Key Rotate to On Position
            IgnitionKeyOn.enabled = true;
            IgnitionKeyOff.enabled = false;


            RedButtonOn.enabled = true;
            iconsolestartup = 1;  // Console Startup Loop Enabler
            AStartup.Startup();
            NeutralButtonOff.enabled = false;
            NeutralButtonOn.enabled = true;
            StartCoroutine("ConsoleStartup");
        }
        
        



        // Start Module
        if ((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1 )
        {
            // Enable Console neutral Buttons
    //        NeutralButtonOff.enabled = false;
    //        NeutralButtonOn.enabled = true;
            
   //         istart = 1;
            StartCoroutine("Startistart1");  // Starting the Loop for Events


            istartfromtouch = 2;
            
            gear = 0;
            AStarting.Starting();
            itrate = 1;  // Enable SLI
            idlingvol = 1;
           
            StartCoroutine("StartIdling");
   //         irpm = 0;

        }

        // Shuttin Module
        if ((Input.GetKeyDown("space") && istart == 1) || istartfromtouch == 3)
        {
            // Enable Console neutral Buttons
     //       NeutralButtonOff.enabled = true;
     //       NeutralButtonOn.enabled = false;
            AShutting.Shutting();
            istart = 0;
            istartfromtouch = 0;
          
            itrate = -1;
           
            
            // Stop all sounds
            AStarting.StartingStop();
            ANBLIP.BlipStop();
            ANSLI.NSLIStop();
            ANFSI1.NFSI1Stop();
            ANBLIP3000.Blip3000Stop();
            ANBLIP4000.Blip4000Stop();
            ANBLIP5000.Blip5000Stop();
            ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();
            AIdling.IdlingStop(); // Stop Idling
            ANFSR.FSRStop();
            ANFSR15000.NFSR15000Stop();
            ANBLIPC14000.BLIPC14000Stop();
            ANBLIPC10000.BLIPC10000Stop();
            ANBLIPC7500.BLIPC7500Stop();
            ANBLIPC6000.BLIPC6000Stop();
            ANBLIPC5500.BLIPC5500Stop();
            ANBLIPC4000.BLIPC4000Stop();
            ANBLIPC3000.BLIPC3000Stop();
            ANRevLimiter.RevLimiterStop();

            StopCoroutine("StartIdling");
       //     StopCoroutine("Startistart1");
            ivibrating = 0; // Stop Vibrating after Rev Limter Stops

            rpm1 = 0;
            RemoveRPMConsole();
            RPM0.enabled = true;
        }

        // Throttle Spring back Module

        if (istart == 1)
        {
// Touch Release Module
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
        //                Debug.Log("Touch Begun");
           //             message = "Begun ";
                        break;
                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                                mSlider.value = 0.0f;
                     
                        break;
                }
            }
            // Mouse Release Module
            if (Input.GetMouseButtonUp(0))
           
            //      Debug.Log("Pressed primary button.");
            mSlider.value = 0.0f;
        }


            // Throttle  Events
            if (istart == 1 && gear == 0)
        {
            trate = (sliderval - slidervalold);
            throttlediff = (sliderval * 150 - rpm1);
   //         Debug.Log("Inside Throttle");
   //         Debug.Log(sliderval);

            // SLI Module
            //               if(((trate > 0 && throttlediff > 0) && itrate == 1) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 ||itrate == 6006 || itrate == 6005  || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0))  {
            if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 10000))
                {
       //             Debug.Log("Inside SLI");
                ANSLI.NSLI();
                ANFSR.FSRStop();
                ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();
                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                
                StopCoroutine("StartIdling");
                StartCoroutine("FadeoutIdling");
             
              
                

                if (itrate == 5001)
                {
         //           ifadeoutnfsr = 1;
       //             StartCoroutine("FadeoutNFSR");
         //           StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 2;    // Counter for FSI to start
                irpm = 1;      // Counter for RPM 
}

            // FSI1 Module
//            if (((trate > 2 || throttlediff > 2000) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))

 //               if (((trate > 2 || throttlediff > 2000 || rpm1 > 4500) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))
                if (((trate > 5 || throttlediff > 4000 || rpm1 > 20500) && itrate == 2 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
                // above FSI1 starts after rpm > 45000//

                {

                    ANFSI1.NFSI1();
                ANSLI.NSLIStop();
                ANFSR.FSRStop();
                ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();
                
                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");
                if (itrate == 5001)
                {
            //        ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
            //        StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 3;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }

            // Twin specific 3rd Throttle Mode
            if ((trate > 4000 || throttlediff > 120000) && itrate == 3 && throttlediff > 0 && rpm1 < 11000  && rpm1 > 2000 && iblipcanstart == 1 && istart == 1)
            {
                Debug.Log("Inside Blip(FSI2)------------------");
                ANFSI2.NFSI2();
                ANFSI1.NFSI1Stop();
                ANSLI.NSLIStop();
                ANFSR.FSRStop();
                ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();

                itrate = 4;
            }


                // Blip Module
   //             if ((trate > 10 || throttlediff > 12000) && throttlediff > 0  && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
                if ((trate > 1000 || throttlediff > 120000) && throttlediff > 0 && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
                {
                //           Debug.Log("Trate=");
                //           Debug.Log(trate);

                Debug.Log("Inside Blip---");
                startTick = AudioSettings.dspTime;

                iblipcanstart = 0;
                irevlimitercanstart = 1;

                         ANBLIP.Blip();
                       ANBLIP2000.Blip2000();
                ANBLIP3000.Blip3000();
                ANBLIP4000.Blip4000();
                ANBLIP5000.Blip5000();
                ANBLIP6000.Blip6000();
                ANBLIP7000.Blip7000();
                ANBLIP10000.Blip10000();
                ANBLIP14000.Blip14000();
                //         ANFSR15000.NFSR15000();

                //           ANBLIPC3000.BLIPC3000();

                idlingvol = 0;   // Making Idling vol 0 so no silence upon start and idling
                AIdling.IdlingStop(); // Stop Idling
                StopCoroutine("StartIdling");
                //         ifadeoutidling = 1;
                //         fadeoutidlingtime = 0.01f;
                //         StartCoroutine("FadeoutIdling");
                ANSLI.NSLIStop();
                 ANFSI1.NFSI1Stop();
        //        ifadeoutsli = 1;
        //        ifadeoutfsi1 = 1;
       //         StartCoroutine("FadeoutNSLI");
       //         StartCoroutine("FadeoutNFSI1");
        //        ifadeoutnfsr = 1;
        //        fadeoutfsrtime = 0.005f;
                //          FadeoutNFSR();



                //           ANFSR.FSRStop();

       //         ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                            ANBLIPC6000.BLIPC6000Stop();
                            ANBLIPC5500.BLIPC5500Stop();
                            ANBLIPC4000.BLIPC4000Stop();
                           ANBLIPC3000.BLIPC3000Stop();
            
                //            NSLIAtrigger.SLIStop();

                //               Debug.Log("Blip ***************************************Blip");
                // Stop SLI
                // Stoo SLR
                itrate = 5000;  // Blip itrate
       //         irpm = 5000;
     //           rpmdisplay = rpm1;
            }

            if (((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 2 || itrate == 3 || itrate == 4))  && rpm1 >= 5500)
     //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
            {
                 itrate = 10000;  // pre rev limit Trate
      //          EnableFSR15000afterRevlimiterStart();
                irevlimitercanstart = 0;
                ivibrating = 1;
                
                rpm1 = 6000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                ANRevLimiter.RevLimiter();
                ANBLIP14000.Blip14000Stop();
                ANBLIP.BlipStop();
                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                // Mobile Vibration
                //            ivibrating = 1;
                //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                //          Handheld.Vibrate();
                StartCoroutine("RevLimiterVibrationOnOff");
                
            }
            // Deactivated Blip FSR
   //         if (((trate < 0 || throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
                if (((trate < -10000 || throttlediff < -100000) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
                //              if (((trate < 0 || throttlediff < 0) && itrate == 5000 && rpm1 > 2000) || (itrate == 5000 && rpm1 >= 9000))
                {
                           itrate = 5001;  // Counter for BlipC
                //     itrate = -1;
       //         iblipcanstop = 1;
       //     }
       //     if(iblipcanstop == 1) {
                
                startTick = AudioSettings.dspTime-startTick;
                //              Debug.Log("Delta Time=");
                //              Debug.Log(startTick);


                //                  InvokeRepeating("FadeoutBLIP", 0f, 0.01f);
                //                 StartCoroutine("Cancelinvoke"); 
                //             ANBLIP.BLIPFadeout();
                //             ifadeoutblip = 1;
                //             StartCoroutine("FadeoutBLIP");
                //          ANBLIP.BlipStop();

                //             if ((rpm1 > 2500 && rpm1 <= 3300) || (rpm1 > 3800 && rpm1 <= 4300) || (rpm1 > 5000 && rpm1 <= 5500) || (rpm1 > 5800 && rpm1 <= 6300) || (rpm1 > 6800 && rpm1 <= 7500))
                //             {

                Debug.Log("Inside trate < 0, rpm=");
                Debug.Log(rpm1);

                if (rpm1 > 10000 && rpm1 <= 15000)
                {
                    itrate = 6100;
                    ANRevLimiter.RevLimiterStop();
                    ivibrating = 0; // Stop Vibrating after Rev Limter Stops

                    ANFSR15000.NFSR15000();
                    ANBLIP.BlipStop();
                    ANBLIP14000.Blip14000Stop();

                }
                if (rpm1 > 6800 && rpm1 <= 10000)
                {
                    itrate = 6008;
        //            iblipcanstop = 0;
                    
                    
                    ANBLIPC14000.BLIPC14000();
                    ANBLIP.BlipStop();
                    ANBLIP14000.Blip14000Stop();

                    //       iblipcanstop = 0;
                }


            
            if (rpm1 > 6200 && rpm1 <= 6800)
            {
                itrate = 6007;
                //            iblipcanstop = 0;


                ANBLIPC10000.BLIPC10000();
                ANBLIP.BlipStop();
                ANBLIP14000.Blip14000Stop();

                //       iblipcanstop = 0;
            }



            if (rpm1 > 5600 && rpm1 <= 6200)
                {
                    itrate = 6006;
                    rpmblipconst = rpm1;
                    blipcinterval = 5.347f;
                    ANBLIPC7500.BLIPC7500();
          //          iblipcanstop = 0;
                    ANBLIP.BlipStop();
                    //                ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();
                   
                    //       iblipcanstop = 0;
                }

                if (rpm1 > 5000 && rpm1 <= 5600)
                {
                    itrate = 6005;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    ANBLIPC6000.BLIPC6000();
           //         iblipcanstop = 0;
                    ANBLIP.BlipStop();
      //              ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();
                   
                    //       iblipcanstop = 0;
                }
                if (rpm1 > 4100 && rpm1 <= 5000)
                   
                {

        //            if ((startTick > 0.214 && startTick < 0.215) || (startTick > 0.221 && startTick < 0.222) || (startTick > 0.227 && startTick < 0.229) || (startTick > 0.234 && startTick < 0.236) || (startTick > 0.24 && startTick < 0.243) || (startTick > 0.247 && startTick < 0.249) || (startTick > 0.252 && startTick < 0.254)) iswitch = 0;
        //            if (iswitch >= 0)
        //            {
                        iswitch = 1;
                        itrate = 6004;
                        rpmblipconst = rpm1;
                        blipcinterval = 0.345f;
                        ANBLIPC5500.BLIPC5500();
                    //            iblipcanstop = 0;
                              ANBLIP.BlipStop();

        //            ANBLIP5000.Blip5000Stop();
                    ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();
                   
                    //       iblipcanstop = 0;
                    //          }
                }
                //                         if (rpm1 > 3200 && rpm1 <= 4000 && ((startTick < 0.171 || startTick > 0.175) || (startTick < 0.161 || startTick > 0.166) || (startTick < 0.171 || startTick > 0.175)))
                if (rpm1 > 3000 && rpm1 <= 4100)
                {

            //        if ((startTick >= 0.169 && startTick <= 0.17) || (startTick >= 0.178 && startTick <= 0.181) || (startTick >= 0.187 && startTick < 0.189) || (startTick >= 0.196 && startTick <= 0.197) || (startTick >= 0.203 && startTick <= 0.207) || (startTick >= 0.211 && startTick <= 0.215) || (startTick >= 0.217 && startTick <= 0.224)) iswitch = 0;
  //                  if ((startTick > 0.171 && startTick < 0.175) || (startTick > 0.18 && startTick < 0.185) || (startTick > 0.19 && startTick < 0.192) || (startTick > 0.197 && startTick < 0.2) || (startTick > 0.205 && startTick < 0.209) || (startTick > 0.213 && startTick < 0.215) || (startTick > 0.22 && startTick < 0.223)) iswitch = 1;
             //       if (iswitch >= 0)
             //       {
                        iswitch = 1;
                        itrate = 6003;
                        rpmblipconst = rpm1;
                        blipcinterval = 0.53f;
                            ANBLIPC4000.BLIPC4000();
                    
                    //           iblipcanstop = 0;
                    ANBLIP.BlipStop();


                    //               ANBLIP4000.Blip4000Stop();
                    //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                    if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                    ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();

                    
                    //       iblipcanstop = 0;
                    //       }
                }
                //        if (rpm1 > 320 && rpm1 <= 3200 && startTick > 0.0f )
                if (rpm1 > 2000 && rpm1 <= 3000)
          //          if (startTick >= 0.178)
                    {
          //          if ((startTick >= 0.103 && startTick <= 0.107) || (startTick >= 0.111 && startTick <= 0.114) || (startTick >= 0.125 && startTick <= 0.129) || (startTick >= 0.137 && startTick <= 0.142) || (startTick >= 0.148 && startTick <= 0.152)) iswitch = 0;
                    //             if ((startTick > 0.121 && startTick < 0.122) || (startTick > 0.128 && startTick < 0.131) || (startTick > 0.133 && startTick < 0.135) || (startTick > 0.140 && startTick < 0.143) || (startTick > 0.152 && startTick < 0.156) || (startTick > 0.161 && startTick < 0.166)) iswitch = 1;
           //         if (iswitch == 0) {
                            iswitch = 1;
                    itrate = 6002;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    ANBLIPC3000.BLIPC3000();
                   
                    //      iblipcanstop = 0;
                    ANBLIP.BlipStop();


                    //                 ANBLIP3000.Blip3000Stop();
                    if(startTick < (AudioNBLIP4000.dt4000-0.02f)) ANBLIP4000.Blip4000Stop();
                        ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();
                    
                    //      }

                    //       iblipcanstop = 0;
                }



                
                StartCoroutine("EnableBlip");  // Enable blip after 200 ms of Blip 
       //         StartCoroutine("StartFSR");     // Enable FSR after Blipc ends
      //          StartCoroutine("StartFSR7500");     // Enable FSR after Blipc ends
                //          }
            }

            // FSR after SLI module
            if (((trate < -0.5 && throttlediff < 0) && ( itrate == 2 || itrate == 3 || itrate  == 4 ) && rpm1 > 2000) || (throttlediff < 0 && itrate == 10000)) 
            {
                blipcinterval = 0;
     //           Debug.Log("Inside FSR-----------------------------");
                StartCoroutine("StartFSR");     // Enable FSR after Blipc ends

            }
                // Restart Idling
                if (rpm1 <= (rpmidling + 20) && (itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999))
            {
                //          Debug.Log("Pre Enter FadeoutSLR1 ***************************************8");

                //            iingearrestartidling = 0;
                Debug.Log("Inside RestartIdling----------------------------- Trate=");
                Debug.Log(trate);
                itrate = 1;
                irpm = 0;
                //           ANFSR.FSRStop();
                StopCoroutine("FadeinNFSR");
                StopCoroutine("FadeoutNFSR");
                StopCoroutine("FadeinIdling");
                StopCoroutine("FadeoutIdling");

                ifadeoutnfsr = 1;
                fadeoutfsrtime = 0.01f;
                StartCoroutine("FadeoutNFSR");

              
                AIdling.Idling();
               ifadeinidling = 1;
                StartCoroutine("FadeinIdling");
                //      StartCoroutine("StartIdling");
      //          ANFSR.FSRStop();
                rpm1 = rpmidling;
   //                
                //         
               
      //          ANFSR15000.NFSR15000Stop();
      //          ANBLIPC14000.BLIPC14000Stop();
      //          ANBLIPC7500.BLIPC7500Stop();
      //          ANBLIPC6000.BLIPC6000Stop();
      //          ANBLIPC5500.BLIPC5500Stop();
      //          ANBLIPC4000.BLIPC4000Stop();
       //         ANBLIPC3000.BLIPC3000Stop();
                //     StartCoroutine("Setitrate1");

            }

            
            // RPM Console Update 
            RemoveRPMConsole();

            // Idling Pulsating Needle
          
                   if (irpm == 0) {
                     if (iidleneedlemoved == 0)
                {
                    int randidle = Random.Range(0, 10);
                    if (randidle >= 5)
                    {
                //        RPM2000.enabled = true;
                //        RPM2100.enabled = false;

                        RPM1000.enabled = true;
                        RPM1100.enabled = false;
                    }
                    else if (randidle == 0)
                    {
                        iidleneedlemoved = 1;
                        StartCoroutine("EnableIdleRPMNeedleMove");
     //                   yield return new WaitForSeconds(0.25f);
              //          RPM2100.enabled = true;
              //          RPM2000.enabled = false;

                        RPM1100.enabled = true;
                        RPM1000.enabled = false;
                    }
                 }
                }
            if (rpm1 == 0) RPM0.enabled = true;
            if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
            if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
            if (rpm1 >= 1500 && rpm1 < 2000) RPM1500.enabled = true;
            if (rpm1 >= 2000 && rpm1 < 2200) RPM2000.enabled = true;
            if (rpm1 >= 2200 && rpm1 < 2500) RPM2200.enabled = true;
            if (rpm1 >= 2500 && rpm1 < 2800) RPM2500.enabled = true;
            if (rpm1 >= 2800 && rpm1 < 3000) RPM2800.enabled = true;
            if (rpm1 >= 3000 && rpm1 < 3200) RPM3000.enabled = true;
            if (rpm1 >= 3200 && rpm1 < 3500) RPM3500.enabled = true;
            if (rpm1 >= 3500 && rpm1 < 3800) RPM3800.enabled = true;
            if (rpm1 >= 3800 && rpm1 < 4000) RPM4000.enabled = true;
            if (rpm1 >= 4000 && rpm1 < 4200) RPM4200.enabled = true;
            if (rpm1 >= 4200 && rpm1 < 4500) RPM4500.enabled = true;
            if (rpm1 >= 4500 && rpm1 < 4800) RPM4800.enabled = true;
            if (rpm1 >= 4800 && rpm1 < 5000) RPM5000.enabled = true;
            if (rpm1 >= 5000 && rpm1 < 5200) RPM5200.enabled = true;
            if (rpm1 >= 5200 && rpm1 < 5500) RPM5500.enabled = true;
            if (rpm1 >= 5500 && rpm1 < 6000) RPM6000.enabled = true;
            if (rpm1 >= 6000 && rpm1 < 6500) RPM6500.enabled = true;
            if (rpm1 >= 6500 && rpm1 < 7000) RPM7000.enabled = true;
            if (rpm1 >= 7000 && rpm1 < 7500) RPM7500.enabled = true;
            if (rpm1 >= 7500 && rpm1 < 8000) RPM8000.enabled = true;
            if (rpm1 >= 8000 && rpm1 < 8500) RPM8500.enabled = true;
            if (rpm1 >= 8500 && rpm1 < 9000) RPM9000.enabled = true;
            if (rpm1 >= 9000 && rpm1 < 9500) RPM9500.enabled = true;
            if (rpm1 >= 9500 && rpm1 < 10000) RPM10000.enabled = true;
            if (rpm1 >= 10000 && rpm1 < 10500) RPM10500.enabled = true;
            if (rpm1 >= 10500 && rpm1 < 11000) RPM11000.enabled = true;
            if (rpm1 >= 11000 && rpm1 < 11500) RPM11500.enabled = true;
            if (rpm1 >= 11500 && rpm1 < 12000) RPM12000.enabled = true;
            if (rpm1 >= 12000 && rpm1 < 12500) RPM12500.enabled = true;
            if (rpm1 >= 12500 && rpm1 < 13000) RPM13000.enabled = true;
            if (rpm1 >= 13000 && rpm1 < 13500) RPM13500.enabled = true;
            if (rpm1 >= 13500 && rpm1 < 14000) RPM14000.enabled = true;
            if (rpm1 >= 14000 && rpm1 < 14500) RPM14500.enabled = true;
            if (rpm1 >= 14500 && rpm1 <= 15000) RPM15000.enabled = true;
   //         if (rpm1 > 14000) RedlineButton.enabled = true;
            else if (rpm1 < 14000) RedlineButton.enabled = false;

            slidervalold = sliderval;  // Assign Slidervalue to Slidrold
        }




        
      

}
    // Coroutines***************************************

    // RPM Console Effects

    // Idling needle Enable Move
       private IEnumerator EnableIdleRPMNeedleMove()
       {
        yield return new WaitForSeconds(0.5f);
        iidleneedlemoved = 0;
    }

    IEnumerator Startistart1()
    {
 //       yield return new WaitForSeconds(rpmdisplayafterstart);
        yield return new WaitForSeconds(0.5f);
        istart = 1;
        rpm1 = rpmidling;
    }

    // Start Idling
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(0.82f); // wait half a secon
        irpm = 0;
        rpm1 = rpmidling;
        AIdling.Idling();
        // Red Console button Off
        RedButtonOn.enabled = false;
    }




    IEnumerator EnableBlip()
    {
        yield return new WaitForSeconds(0.2f);
        iblipcanstart = 1;
        Debug.Log("Inside Enable Blip");
    }

    // Enable Trate for Revlimiter after 0.1s of Revlimiter Start so Reducing waits for 0.1 s
    private IEnumerator EnableFSR15000afterRevlimiterStart()
    {
        yield return new WaitForSeconds(0.1f);
        itrate = 10000;
    }

    IEnumerator StartFSR()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2 || itrate == 3 || itrate == 4 || itrate == 10000)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            ifadeinnfsr = 1;
            ANFSR.FSR();
            StartCoroutine("FadeinNFSR");
            ANRevLimiter.RevLimiterStop();

            if (iPopCrackle2 == 1 && rpm1 > 5000)
            {
        //        Debug.Log("Inside POP Crackle Ninja400");
                ACrackle10000.Crackle10000();
                StartCoroutine("Explode");
            }
            else if (iPopCrackle2 == 1 && rpm1 < 5000 && rpm1 > 4000)
            {
          //      Debug.Log("Inside POP Crackle Ninja400 2");
                ACrackle8000.Crackle8000();
                StartCoroutine("Explode");
            }

            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLI");
            StartCoroutine("FadeoutNFSI1");
          
            ANFSI2.NFSI2Stop();
            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;




            itrate = 4999;
        }
       

    }

    IEnumerator StartFSR7500()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            itrate = 4999;
            ANFSR7500.FSR7500();
            ANSLI.NSLIStop();
            //       iblipcanstart = 1;
            
        }


    }


    void FadeoutBLIP()  
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
  //      while (ifadeoutblip == 1)
  //      {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
  //          yield return new WaitForSeconds(0.001f); // wait half a second

            ANBLIP.BLIPFadeout();
   //     }
    }

    IEnumerator Cancelinvoke()
    {
        
     
            yield return new WaitForSeconds(0.1f); // wait half a second
        CancelInvoke();
       
    }




    IEnumerator FadeinIdling()
    {
       //***************
        while (ifadeinidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
           AIdling.IdlingFadein();
}
    }

    IEnumerator FadeoutIdling()
    {
        //***************
        while (ifadeoutidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutidlingtime); // wait half a second
            AIdling.IdlingFadeout();
        }
    }

    // Defunct************************
    IEnumerator FadeoutNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutfsrtime); // wait half a second
            ANFSR.NFSRFadeout();
            // Fadeout BlipFSRs
     //       ANFSR15000.BLIPCR15000Fadeout();
      //      ANBLIPC14000.BLIPCR14000Fadeout();
      //      ANBLIPC7500.BLIPCR7500Fadeout();
      //      ANBLIPC6000.BLIPCR6000Fadeout();
      //      ANBLIPC5500.BLIPCR5000Fadeout();
      //      ANBLIPC4000.BLIPCR4000Fadeout();
      //      ANBLIPC3000.BLIPCR3000Fadeout();
        }
    }

    IEnumerator FadeinNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            ANFSR.NFSRFadein();
        }
    }

    IEnumerator FadeoutNSLI()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
           ANSLI.NSLIFadeout();
            
        }
    }
    IEnumerator FadeoutNFSI1()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
           
            ANFSI1.NFSI1Fadeout();
        }
    }


    // ***************************************************

    IEnumerator FadeoutBLIPCR7500()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
 //           ANFSR.NFSRFadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
        }
    }
    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator RevLimiterVibrationOnOff()
    {
        if (ivibrator == 1) //Handheld.Vibrate();
        while (ivibrating == 1)
        {           
            yield return new WaitForSeconds(0.5f);
            if (ivibrator == 1)
            {
      //          Handheld.Vibrate();
            }
        }

    }

    // Keyin from Touch
    public void KeyInFromTouch()
    {
        //      moveSpeed = newSpeed;

        if (ikeyinfromtouch == 0)
        {
            ikeyinfromtouch = 1;
            // Headlight on Module
            Hlight.HeadlightOn();
            Rlight.RearlightOn();

        }
        else if (ikeyinfromtouch == 2 && istartfromtouch != 0)
        {
            ikeyinfromtouch = 0;
            istartfromtouch = 3;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;
           
            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();

        }
        else if (ikeyinfromtouch == 2)
        {
            ikeyinfromtouch = 0;
            RedButtonOn.enabled = false;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
        }




        }
    // StartStop from Touch
 //   public void StartStopFromTouch()
 //   {
        //      moveSpeed = newSpeed;
 //       if (ikeyinfromtouch == 2)
  //      {
  //          if (istartfromtouch == 0) istartfromtouch = 1;
  //       else if (istartfromtouch == 2) istartfromtouch = 3;
   //     }

    //}

    // StartStop from Touch
    public void StartStopFromTouch()
    {
        //      moveSpeed = newSpeed;
        if (ikeyinfromtouch == 2)
        {
            if (istartfromtouch == 0) istartfromtouch = 1;
            else if (istartfromtouch == 2) istartfromtouch = 3;
        }
        // Make StartStopButton Dark
        StartStopButton.enabled = false;
        StartStopButtonPressed.enabled = true;
        //       StartCoroutine("StartStopButtonAppear");
    }
    // ReAppear StartStopButton(Original)


    // ReAppear StartStopButton(Original)
    public void Start1StopButtonAppear()  // 
    {

        StartStopButton.enabled = true;
        StartStopButtonPressed.enabled = false;

    }


    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        ReverbXoneMix = 0.00f;
        RoadView.SetActive(true); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage
        TunnelView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(false);
        HeadlightReflectionL.SetActive(false);

        AIdling.IdlingReverbRoad();
        AStartup.StartupReverbRoad();
        AStarting.StartingReverbRoad();
        AShutting.ShuttingReverbRoad();
        ANRevLimiter.RevLimiterReverbRoad();
        ANFSR.NFSRReverbRoad();
        ANSLI.NSLIReverbRoad();
        ANFSI1.NFSI1ReverbRoad();
        ANFSI2.NFSI2ReverbRoad();

        ACrackle10000.Crackle10000ReverbRoad();
        ACrackle8000.Crackle8000ReverbRoad();
    }



    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {
        ReverbXoneMix = 1.03f;

        //   TunnelView.SetActive(false); // DisAppearance of Tunnel
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(true); // Disapperance of Garage
        TunnelView.SetActive(false); // Appearance of Road

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        ANFSI2.NFSI2ReverbGarage();

        ACrackle10000.Crackle10000ReverbGarage();
        ACrackle8000.Crackle8000ReverbGarage();

    }

    public void TunnelAppear()  // 
    {
        ReverbXoneMix = 1.1f;

        //   TunnelView.SetActive(false); // DisAppearance of Tunnel
        TunnelView.SetActive(true); // Appearance of Road
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        ANFSI2.NFSI2ReverbGarage();


        ACrackle10000.Crackle10000ReverbGarage();
        ACrackle8000.Crackle8000ReverbGarage();

    }

    public void MoveRPMConsole1()
    {
        if (iRPMConsole == 0)
        {
            RPMConsole1.transform.position = new Vector3(1550, 740, 0);
            RPMConsole1.transform.localScale += new Vector3(0.8f, 0.5f, 0);

            //    RPMConsole1.SetActive(false);
            iRPMConsole = 1;
        }
        else if (iRPMConsole == 1)
        {
            //     RPMConsole1.SetActive(true);
            RPMConsole1.transform.localScale += new Vector3(-0.8f, -0.5f, 0);
            RPMConsole1.transform.position = new Vector3(900, 130, 0);
            iRPMConsole = 0;
        }
    }

    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        sliderval = sliderposition;
  //      Debug.Log("Enter SliderVal");
   //     Debug.Log(sliderposition);
    }

    // GUI Events Functions
    public void VibratorOnOff()
    {
        if (ivibrator == 0) ivibrator = 1;
        else if(ivibrator == 1) ivibrator = 0;
        
    }


    // Console Startup Sequence************************************************************
    int rpmi = 1000;   // Counter for rpm change for Console Startup Sequence
                       //   float wait=0.1;
    private IEnumerator ConsoleStartup()
    {
        yield return new WaitForSeconds(1f);
        while (iconsolestartup == 1 || iconsolestartup == -1)
        {
            yield return new WaitForSeconds(0.03f); // wait half a second
            
            RemoveRPMConsole();
            //     if (rpmi <= 7400)
            //     {
            if (iconsolestartup == 1 && rpmi <= 6000)
            {
                rpmi = rpmi + iconsolestartuprate*2;
            }
            else iconsolestartup = -1;
            if (iconsolestartup == -1 && rpmi >= 1000)
            {
                rpmi = rpmi - iconsolestartuprate*2;
                if (rpmi <= 900)
                {
      //              RPM0.enabled = true;
                    rpmi = 0;
                    iconsolestartup = 0;
                }
            }

            if (rpmi < 900) RPM0.enabled = true;
            if (rpmi >= 900 && rpmi < 1500) RPM1000.enabled = true;
            if (rpmi >= 1500 && rpmi < 2000) RPM1500.enabled = true;
            if (rpmi >= 2000 && rpmi < 2200) RPM2000.enabled = true;
            if (rpmi >= 2200 && rpmi < 2500) RPM2200.enabled = true;
            if (rpmi >= 2500 && rpmi < 2800) RPM2500.enabled = true;
            if (rpmi >= 2800 && rpmi < 3000) RPM2800.enabled = true;
            if (rpmi >= 3000 && rpmi < 3200) RPM3000.enabled = true;
            if (rpmi >= 3200 && rpmi < 3500) RPM3500.enabled = true;
            if (rpmi >= 3500 && rpmi < 3800) RPM3800.enabled = true;
            if (rpmi >= 3800 && rpmi < 4000) RPM4000.enabled = true;
            if (rpmi >= 4000 && rpmi < 4200) RPM4200.enabled = true;
            if (rpmi >= 4200 && rpmi < 4500) RPM4500.enabled = true;
            if (rpmi >= 4500 && rpmi < 4800) RPM4800.enabled = true;
            if (rpmi >= 4800 && rpmi < 5000) RPM5000.enabled = true;
            if (rpmi >= 5000 && rpmi < 5200) RPM5200.enabled = true;
            if (rpmi >= 5200 && rpmi < 5500) RPM5500.enabled = true;
            if (rpmi >= 5500 && rpmi < 6000) RPM6000.enabled = true;
            if (rpmi >= 6000 && rpmi < 6500) RPM6500.enabled = true;
            if (rpmi >= 6500 && rpmi < 7000) RPM7000.enabled = true;
            if (rpmi >= 7000 && rpmi < 7500) RPM7500.enabled = true;
            if (rpmi >= 7500 && rpmi < 8000) RPM8000.enabled = true;
            if (rpmi >= 8000 && rpmi < 8500) RPM8500.enabled = true;
            if (rpmi >= 8500 && rpmi < 9000) RPM9000.enabled = true;
            if (rpmi >= 9000 && rpmi < 9500) RPM9500.enabled = true;
            if (rpmi >= 9500 && rpmi < 10000) RPM10000.enabled = true;
            if (rpmi >= 10000 && rpmi < 10500) RPM10500.enabled = true;
            if (rpmi >= 10500 && rpmi < 11000) RPM11000.enabled = true;
            if (rpmi >= 11000 && rpmi < 11500) RPM11500.enabled = true;
            if (rpmi >= 11500 && rpmi < 12000) RPM12000.enabled = true;
            if (rpmi >= 12000 && rpmi < 12500) RPM12500.enabled = true;
            if (rpmi >= 12500 && rpmi < 13000) RPM13000.enabled = true;
            if (rpmi >= 13000 && rpmi < 13500) RPM13500.enabled = true;
            if (rpmi >= 13500 && rpmi < 14000) RPM14000.enabled = true;
            if (rpmi >= 14000 && rpmi < 14500) RPM14500.enabled = true;
            if (rpmi >= 14500 && rpmi < 15000) RPM15000.enabled = true;
        }
        //      newImage = Image.FromFile(@"C:/User Data/VS/Software/Bass/Combined-Project/Blank-Application/Images/" + rpmString + ".png");
    }

    // Console RPM Remove
    public void RemoveRPMConsole()
    {
        RPM0.enabled = false;
        RPM1000.enabled = false;
        RPM1100.enabled = false;
        RPM1500.enabled = false;
        RPM2000.enabled = false;
        RPM2200.enabled = false;
        RPM2500.enabled = false;
        RPM2800.enabled = false;
        RPM3000.enabled = false;
        RPM3200.enabled = false;
        RPM3500.enabled = false;
        RPM3800.enabled = false;
        RPM4000.enabled = false;
        RPM4200.enabled = false;
        RPM4500.enabled = false;
        RPM4800.enabled = false;
        RPM5000.enabled = false;
        RPM5200.enabled = false;
        RPM5500.enabled = false;
        RPM6000.enabled = false;
        RPM6500.enabled = false;
        RPM7000.enabled = false;
        RPM7500.enabled = false;

        RPM8000.enabled = false;
        RPM8500.enabled = false;
        RPM9000.enabled = false;
        RPM9500.enabled = false;
        RPM10000.enabled = false;
        RPM10500.enabled = false;
        RPM11000.enabled = false;
        RPM11500.enabled = false;
        RPM12000.enabled = false;
        RPM12500.enabled = false;
        RPM13000.enabled = false;
        RPM13500.enabled = false;
        RPM14000.enabled = false;
        RPM14500.enabled = false;
        RPM15000.enabled = false;

    }


    // Exhaust Pop/Backfire
    void Explode()
    {
        AVExhaustPop.ExhaustPop();
    }

    public void PopCrackleEnable()  // Enable/Disable Pop Crackles
    {
        if (iPopCrackle2 == 0)
        {
            iPopCrackle2 = 1;
            Debug.Log("Pop Tick******************");
            PopsCrackles1Tick.SetActive(true);


        }
        else if (iPopCrackle2 == 1)
        {
            iPopCrackle2 = 0;
            PopsCrackles1Tick.SetActive(false);
        }
    }
}
