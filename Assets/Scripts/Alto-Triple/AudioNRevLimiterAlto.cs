﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterAlto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
            if (AudioEngineAlto.itrate == 10000)
            {
            //      RPMCal();
            // AudioEngineAlto.rpm1 = 15000;
           
        }
     
	}

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }
    

    IEnumerator RevLimiterRPMFluctuation()
    {
      
        while (AudioEngineAlto.ivibrating == 1)
        {
  //          AudioEngineAlto.rpm1 = 15000;
            yield return new WaitForSeconds(0.1f);
     //       if (AudioEngineAlto.ivibrator == 1)
     //       {
                if (AudioEngineAlto.rpm1 == 6000) AudioEngineAlto.rpm1 = 5500;
                else AudioEngineAlto.rpm1 = 6000;
            //          else if (AudioEngineAlto.rpm1 == 14000) AudioEngineAlto.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineAlto.ReverbXoneMix;
    }
}
