﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP14000Ninja250 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineNinja250.itrate == 5007)
        {
    //        AudioEngineNinja250.rpm1 = 14000;
            //           Debug.Log(AudioEngineNinja250.rpm1);
        }
    }


    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip14000()
    {
       
        AudioSource audio = GetComponent<AudioSource>();
         if (AudioEngineNinja250.rpm1 >= 8000 && AudioEngineNinja250.rpm1 < 14000)
        {
            audio.Play();
        }
        else if (AudioEngineNinja250.rpm1 >= 7000 && AudioEngineNinja250.rpm1 < 8000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.031F);
        }
        else if (AudioEngineNinja250.rpm1 >= 6000 && AudioEngineNinja250.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.047F);
        }
        else if (AudioEngineNinja250.rpm1 >= 5000 && AudioEngineNinja250.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.058F);
        }
        else if (AudioEngineNinja250.rpm1 >= 4000 && AudioEngineNinja250.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.079F);
        }
        else if (AudioEngineNinja250.rpm1 >= 3000 && AudioEngineNinja250.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.130F);
        }


        else if (AudioEngineNinja250.rpm1 >= 2000 && AudioEngineNinja250.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.195F);
        }
        else if (AudioEngineNinja250.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.291F);
        audio.volume = 1;
    //    audio.Play();
   //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip14000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}
