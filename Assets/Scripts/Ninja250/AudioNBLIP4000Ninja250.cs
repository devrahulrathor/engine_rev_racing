﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP4000Ninja250 : MonoBehaviour {
    public static float dt4000 = 0;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {


    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip4000()
    {
     

           AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineNinja250.rpm1 >= 3000 && AudioEngineNinja250.rpm1 < 4000)
        {

            audio.Play();

        }
        else if (AudioEngineNinja250.rpm1 >= 2000 && AudioEngineNinja250.rpm1 < 3000)
        {
            dt4000 = 0.096f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }
        else if (AudioEngineNinja250.rpm1 < 2000)
        {
            dt4000 = 0.161f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }
      
        audio.volume = 1;
   //     audio.Play();
   //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip4000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}
