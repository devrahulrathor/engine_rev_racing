﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000Ninja250 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioEngineNinja250.itrate == 5005)
        {
            AudioEngineNinja250.rpm1 = 6000;
            //           Debug.Log(AudioEngineNinja250.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {

       
        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineNinja250.rpm1 >= 5000 && AudioEngineNinja250.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineNinja250.rpm1 >= 4000 && AudioEngineNinja250.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.02F);
        }
        else if (AudioEngineNinja250.rpm1 >= 3000 && AudioEngineNinja250.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.072F);
        }
        else if (AudioEngineNinja250.rpm1 >= 2000 && AudioEngineNinja250.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.136F);
        }
        else if (AudioEngineNinja250.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.232F);
        audio.volume = 1;
    //    audio.Play();
    //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}

