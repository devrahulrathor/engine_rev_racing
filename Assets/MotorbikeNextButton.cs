﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MotorbikeNextButton : MonoBehaviour {

    // Use this for initialization

    public void NextButton()
    {
        SceneManager.LoadScene(1);  // Car Options Scene
    }

   
    public void HowToPLayButton()
    {
        SceneManager.LoadScene(6);  // Car Options Scene
    }

}
