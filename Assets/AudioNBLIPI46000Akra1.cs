﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPI46000Akra1 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineI4.itrate == 5005)
        {
            AudioEngineI4.rpm1 = 6000;
            //           Debug.Log(AudioEngineI4.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineI4.rpm1 >= 5000 && AudioEngineI4.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineI4.rpm1 >= 4000 && AudioEngineI4.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.04F);
        }
        else if (AudioEngineI4.rpm1 >= 3000 && AudioEngineI4.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.099F);
        }
        else if (AudioEngineI4.rpm1 >= 2000 && AudioEngineI4.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.153F);
        }
        else if (AudioEngineI4.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.251F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip6000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //     audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineI4.ReverbXoneMix;
    }
}


