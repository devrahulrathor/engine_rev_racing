using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class BinSaveLoadDrag : MonoBehaviour
{


    public static void Save()
    {
        //     BinaryFormatter bf = new BinaryFormatter();
        //      FileStream file = File.Open(Application.persistentDataPath + "/save.dat", FileMode.OpenOrCreate);
        //    SerializableSaveData serializableSaveData = new SerializableSaveData();
        //    bf.Serialize(file, serializableSaveData);
        //    file.Close();
        //Debug.Log (SaveData.currentLevel);
    }

    // ZX10R Section
 //   int izx10akra1 = 0;
    public void SaveZX25R(int a)
    {
        //
        Debug.Log("Inside SaveZX25 Binsave");
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/zx25r.dat")))

        {
            Debug.Log("File Exists ZX25");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/zx25r.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist ZX25");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/zx25r.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadZX25R()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/zx25r.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/zx25r.dat"), FileMode.Open);
            //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
            MainMenuScript.IAPZX25 = (int)bf.Deserialize(file);
            //          Debug.Log("ZX10Akra1=");
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }


    // Bullet500 Section
    //   int izx10akra1 = 0;
    public void SaveBullet500(int a)
    {
        //
        Debug.Log("Inside SaveBullet500 Binsave");
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/bullet500.dat")))

        {
            Debug.Log("File Exists Bullet500");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/bullet500.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist Bullet500");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/bullet500.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadBullet500()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/bullet500.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/bullet500.dat"), FileMode.Open);
            //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
            MainMenuScript.IAPBullet500 = (int)bf.Deserialize(file);
                     Debug.Log("IAPBullet500=" + MainMenuScript.IAPBullet500);
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    // Ridig Gear1 Section
    //   int izx10akra1 = 0;
    public void SaveRidingGear1(int a)
    {
        //
        Debug.Log("Inside SaveRidingGear1 Binsave");
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/ridinggear1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/ridinggear1.dat")))

        {
            Debug.Log("File Exists RidingGear1");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/ridinggear1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist RidingGear1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/ridinggear1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadRidingGear1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/ridinggear1.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/ridinggear1.dat"), FileMode.Open);
            //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
            MainMenuScript.IAPRidingGear1 = (int)bf.Deserialize(file);
            Debug.Log("IAPRidingGear1=" + MainMenuScript.IAPRidingGear1);
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    //Tracks Section
    //   int izx10akra1 = 0;
    public void SaveTrack1(int a)
    {
        //
        Debug.Log("Inside SaveRidingGear1 Binsave");
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/ridinggear1.dat", FileMode.OpenOrCreate);
       
            if (File.Exists(Path.Combine(Application.persistentDataPath + "/track1.dat")))

            {
                Debug.Log("File Exists RidingGear1");

                FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track1.dat"), FileMode.OpenOrCreate);
                //SaveData saveData = new SaveData();
                if (a == 0) nonconsumableval1 = 0;
                else if (a == 1) nonconsumableval1 = 1;
                else if (a == 3) nonconsumableval1 = 1;
                if (a != 2)
                {
                    bf.Serialize(file, nonconsumableval1);
                }

                file.Close();
            }
        
        else
        {
           
                Debug.Log("File doesn't Exist RidingGear1");
                FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track1.dat"), FileMode.OpenOrCreate);
                //SaveData saveData = new SaveData();

                if (a == 3 || a == 1) nonconsumableval1 = 1;

                else nonconsumableval1 = 0;
                bf.Serialize(file, nonconsumableval1);
                file.Close();
            }
        
    }

    public void LoadTrack1()
    {
        
      
            if (File.Exists(Path.Combine(Application.persistentDataPath + "/track1.dat")))
            {
                BinaryFormatter bf = new BinaryFormatter();
                //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
                FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track1.dat"), FileMode.Open);
                //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
                MainMenuScript.IAPTrack1 = (int)bf.Deserialize(file);
                Debug.Log("IAPRidingGear1=" + MainMenuScript.IAPTrack1);
                //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
                file.Close();
            }
            //      return true;
        
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    public void SaveTrack2(int a)
    {
        //
        Debug.Log("Inside SaveRidingGear1 Binsave");
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/ridinggear1.dat", FileMode.OpenOrCreate);

        if (File.Exists(Path.Combine(Application.persistentDataPath + "/track2.dat")))

        {
            Debug.Log("File Exists RidingGear1");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track2.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }

            file.Close();
        }

        else
        {

            Debug.Log("File doesn't Exist RidingGear1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track2.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }

    }

    public void LoadTrack2()
    {


        if (File.Exists(Path.Combine(Application.persistentDataPath + "/track2.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track2.dat"), FileMode.Open);
            //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
            MainMenuScript.IAPTrack2 = (int)bf.Deserialize(file);
            Debug.Log("IAPRidingGear1=" + MainMenuScript.IAPTrack1);
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
        }
        //      return true;

        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }


    public void SaveTrack3(int a)
    {
        //
        Debug.Log("Inside SaveRidingGear1 Binsave");
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/ridinggear1.dat", FileMode.OpenOrCreate);

        if (File.Exists(Path.Combine(Application.persistentDataPath + "/track3.dat")))

        {
            Debug.Log("File Exists RidingGear1");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track3.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }

            file.Close();
        }

        else
        {

            Debug.Log("File doesn't Exist RidingGear1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track3.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }

    }

    public void LoadTrack3()
    {


        if (File.Exists(Path.Combine(Application.persistentDataPath + "/track3.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/track3.dat"), FileMode.Open);
            //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
            MainMenuScript.IAPTrack3 = (int)bf.Deserialize(file);
            Debug.Log("IAPRidingGear1=" + MainMenuScript.IAPTrack1);
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
        }
        //      return true;

        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }
}