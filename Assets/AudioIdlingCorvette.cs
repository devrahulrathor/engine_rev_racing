using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingCorvette : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineCorvette.irpm == 0)
        {
            AudioEngineCorvette.rpm1 = AudioEngineCorvette.rpmidling;
        }
        LowPassFilter();
    }

    public void Idling()
    {
        Debug.Log("2 Inside RestartIdling *&*&*&*))))))))");
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngineCorvette.idlingvolStock;

  //      Debug.Log("4 IdlingVol=");
   //     Debug.Log(AudioEngineCorvette.idlingvolStock);
        audio.Play();
        audio.Play(44100);
    //   StartCoroutine("IdlingVolAmpup");
        if (audio.isPlaying)
        {
            Debug.Log("Audio PLaying");
        }


    }
    IEnumerator IdlingVolAmpup()  // Delayed Idling for the bug which stops Idling during BlipC3000
    {
        //***************
       
            yield return new WaitForSeconds(0.5f); // wait half a second
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1;
        
    }


    public void IdlingStop()
    {
        Debug.Log("Idling Stopped");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       idlingvolRoushume = 0;


    }

    public double idlingvolRoushume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        idlingvolRoushume = idlingvolRoushume - 0.1;
        if (idlingvolRoushume > 0) audio.volume = (float)idlingvolRoushume;
        else
        {

            AudioEngineCorvette.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        idlingvolRoushume = idlingvolRoushume + 0.1;
        if (idlingvolRoushume < 1) audio.volume = (float)idlingvolRoushume;
        else
        {
            audio.volume = 1;
            AudioEngineCorvette.ifadeinidling = 0;

            //            audio.Stop();
        }

    }


    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineCorvette.ReverbXoneMix;
    }

    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }


}
