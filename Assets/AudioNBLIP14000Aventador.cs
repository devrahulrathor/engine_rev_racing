using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP14000Aventador : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineS1000RR.itrate == 5007)
        {
            //        SoundEngineS1000RR.rpm1 = 14000;
            //           Debug.Log(SoundEngineS1000RR.rpm1);
        }
    }


    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip14000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (SoundEngineS1000RR.rpm1 >= 8000 && SoundEngineS1000RR.rpm1 < 14000)
        {
            audio.Play();
        }
        else if (SoundEngineS1000RR.rpm1 >= 7000 && SoundEngineS1000RR.rpm1 < 8000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.031F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 6000 && SoundEngineS1000RR.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.047F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 5000 && SoundEngineS1000RR.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.058F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 4000 && SoundEngineS1000RR.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.079F);
        }
        else if (SoundEngineS1000RR.rpm1 >= 3000 && SoundEngineS1000RR.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.130F);
        }


        else if (SoundEngineS1000RR.rpm1 >= 2000 && SoundEngineS1000RR.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.195F);
        }
        else if (SoundEngineS1000RR.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.291F);
        audio.volume = 1;
        //    audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip14000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip14000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip14000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}
