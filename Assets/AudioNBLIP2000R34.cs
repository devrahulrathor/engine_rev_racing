﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP2000R34 : MonoBehaviour
{
    int[] posrpmarrayblip2000 = { 1150, 1150, 1150, 1150, 1200, 1250, 1300, 1500, 1700, 1850 };
    double[] rpmposarrayblip2000 = { 0.01, 0.01, 0.01, 0.02, 0.05, 0.06, 0.07, 0.08, 0.08, 0.08, 0.08, 0.09, 0.09, 0.09, 0.09 };
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR34.rpm1 <= 2000)
        {
            //       RPMCal();
            //        Debug.Log("Inside 2000Blip irpmindex=");
            //        Debug.Log(irpmarrayindex);

        }
       LowPassFilter();
    }


    int irpmpos = 0;
    double posinsec;
    public void Blip2000()
    {
        //   AudioEngineR34.itrate = 5001;

        AudioSource audio = GetComponent<AudioSource>();


        //      irpmpos = (AudioEngineR34.rpm1 / 50) - 21;

        //      posinsec = rpmposarrayblip2000[irpmpos];
        //         textBox3.Text = posinsec.ToString();
        //       var Last10SecondsBytePos = Bass.BASS_ChannelSeconds2Bytes(streamslr1, durationInSeconds - 2);

        //      audio.time = (float)posinsec;
        //      irpmpos = (AudioEngineR34.rpm1 / 50) - 21;

        //      posinsec = rpmposarrayblip[irpmpos];
        //         textBox3.Text = posinsec.ToString();
        //       var Last10SecondsBytePos = Bass.BASS_ChannelSeconds2Bytes(streamslr1, durationInSeconds - 2);

        //      audio.time = (float)posinsec;
        //      audio.time = (float)posinsec;
        if (AudioEngineR34.rpm1 < 2000) audio.Play();
        audio.volume = 1;

        audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }


    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();
        irpmarrayindex = (int)((audio.time * 10000) / 100);

        //       rpm = posrpmarrayblip[irpmarrayindex];
        //         textBox2.Text = position.ToString();
        //       if (audio.time <= 0.32)
        //         if(AudioEngineR34.rpm1 <= 7000)
        //       {
        AudioEngineR34.rpm1 = posrpmarrayblip2000[irpmarrayindex];
        //       }
        //     else AudioEngineR34.rpm1 = 0;
        //     AudioEngineR34.itrate = -1;

    }

    public void Blip2000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip2000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip2000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR34.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
 
}
