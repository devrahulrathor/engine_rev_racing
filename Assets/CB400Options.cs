﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CB400Options : MonoBehaviour
{

    // Use this for initialization

    public void CB400()
    {
        SceneManager.LoadScene(7);
    }


    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void BackButton()
    {
        if (AudioEngineI4.itrate != -1) AudioEngineI4.istartfromtouch = 3;
        SceneManager.LoadScene(0);   // Motorbike Options
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
