﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingMustangBorla1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineMustang.irpm == 0)
        {
            AudioEngineMustang.rpm1 = AudioEngineMustang.rpmidling;
        }
    }

    public void Idling()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngineMustang.idlingvolBorla1;
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       idlingvolBorla1ume = 0;


    }

    double idlingvolBorla1ume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        idlingvolBorla1ume = idlingvolBorla1ume - 0.1;
        if (idlingvolBorla1ume > 0) audio.volume = (float)idlingvolBorla1ume;
        else
        {

            AudioEngineMustang.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        idlingvolBorla1ume = idlingvolBorla1ume + 0.1;
        if (idlingvolBorla1ume < 1) audio.volume = (float)idlingvolBorla1ume;
        else
        {
            audio.volume = 1;
            AudioEngineMustang.ifadeinidling = 0;

            //            audio.Stop();
        }

    }


    public void IdlingReverbRoad()
    {
        Debug.Log("Inside Road Appear");
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }




}


