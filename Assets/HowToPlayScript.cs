﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HowToPlayScript : MonoBehaviour
{
    public int iHowtoPlay = 0;   // Appear/Disapperar Counter
    public GameObject HowtoPlayWindow;
    public Button HowToPlayButton;

    // Use this for initialization

    public void NextButton()
    {
   //     SceneManager.LoadScene(1);  // Car Options Scene
    }

    public void HowToPLayButtonActivate()
    {
        HowtoPlayWindow.SetActive(true);
        ChangeButtonColor(1);
  //      MainMenuVehiclesScript.iMainMenubuttoncolor = 2;
        Debug.Log("Inside Howto play Color");
       
   //     MainMenuVehiclesScript.ChangeButtonColor();
    }
    public void HowToPLayButtonDeActivate()
    {
        HowtoPlayWindow.SetActive(false);
    }

    //    public void ChangeButtonColor()
    //    {
    //        ColorBlock colors = HowToPlayButton.colors;
    //        colors.normalColor = new Color32(0, 219, 213, 255);
    //   colors.normalColor = new Color(0, 0.2f, 0.5f, 0.8f);
    //    colors.highlightedColor = new Color32(255, 100, 100, 255);
    //        HowToPlayButton.colors = colors;
    //   }

    public void ChangeButtonColor(int a)
    {

        ColorBlock colors = HowToPlayButton.colors;
        //          colors.normalColor = new Color32(0, 219, 213, 255);
        if (a == 1)
        {
    //        Debug.Log("Change Colours to Green");
            colors.normalColor = new Color32(0, 219, 213, 255);  // Green Colour
            colors.highlightedColor = new Color32(0, 219, 213, 255);
            colors.pressedColor = new Color32(0, 219, 213, 255);
            colors.selectedColor = new Color32(0, 219, 213, 255);
        }
        else if (a == 2)
        {

            colors.normalColor = new Color32(225, 225, 225, 255);  //White Colour
            colors.highlightedColor = new Color32(225, 225, 225, 255);
            colors.pressedColor = new Color32(225, 225, 225, 255);
            colors.selectedColor = new Color32(225, 225, 225, 255);
        }
        //   colors.normalColor = new Color(0, 0.2f, 0.5f, 0.8f);
        //      colors.highlightedColor = new Color32(0, 219, 150, 255);
        HowToPlayButton.colors = colors;
    }



    // Defunct___________________________
    public void HowToPLayButton()
    {
        if (iHowtoPlay == 0)
        {
            //   SceneManager.LoadScene(7);  // Car Options Scene
            HowtoPlayWindow.SetActive(false);
            iHowtoPlay = 1;
        }
        else if (iHowtoPlay == 1)
        {
            HowtoPlayWindow.SetActive(true);
            iHowtoPlay = 0;
        }
    }
}
