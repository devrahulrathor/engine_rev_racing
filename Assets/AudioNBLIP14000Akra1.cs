﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP14000Akra1 : MonoBehaviour {

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngine.itrate == 5007)
        {
            //        AudioEngine.rpm1 = 14000;
            //           Debug.Log(AudioEngine.rpm1);
        }
    }


    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip14000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngine.rpm1 >= 8000 && AudioEngine.rpm1 < 14000)
        {
            audio.Play();
        }
        else if (AudioEngine.rpm1 >= 7000 && AudioEngine.rpm1 < 8000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.030F);
        }
        else if (AudioEngine.rpm1 >= 6000 && AudioEngine.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.042F);
        }
        else if (AudioEngine.rpm1 >= 5000 && AudioEngine.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.06F);
        }
        else if (AudioEngine.rpm1 >= 4000 && AudioEngine.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.087F);
        }
        else if (AudioEngine.rpm1 >= 3000 && AudioEngine.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.131F);
        }


        else if (AudioEngine.rpm1 >= 2000 && AudioEngine.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.173F);
        }
        else if (AudioEngine.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.228F);
        audio.volume = 1;
        //    audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip14000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip14000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip14000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
//       audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}
