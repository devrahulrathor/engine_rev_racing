using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP5000ZX25Drag : MonoBehaviour
{
    public static float dt5000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX25Drag.itrate == 5004)
        {
            SoundEngineZX25Drag.rpm1 = 5000;
            //           Debug.Log(SoundEngineZX25Drag.rpm1);
        }
    }
    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip5000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (SoundEngineZX25Drag.rpm1 >= 4000 && SoundEngineZX25Drag.rpm1 < 5000)
        {
            audio.Play();
        }
        else if (SoundEngineZX25Drag.rpm1 >= 3000 && SoundEngineZX25Drag.rpm1 < 4000)
        {
            dt5000 = 0.044f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 2000 && SoundEngineZX25Drag.rpm1 < 3000)
        {
            dt5000 = 0.088f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (SoundEngineZX25Drag.rpm1 < 2000)
            dt5000 = 0.204f;
        audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        audio.volume = 1;

        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip5000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip5000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip5000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //    audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
    }
}

