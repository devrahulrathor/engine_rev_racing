﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStartingJawa2s : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void Starting()
    {

        AudioSource audio = GetComponent<AudioSource>();

        audio.Play();
        audio.Play(44100);


    }

    public void StartingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.Stop();
    }
    public void StartingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void StartingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineJawa2s.ReverbXoneMix;
    }
}

