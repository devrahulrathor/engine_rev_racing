﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jawa2sOptionsButton : MonoBehaviour
{
    public GameObject Jawa2sViewsButton;
    public GameObject Jawa2sExhaustButton;
    //    public GameObject Ninj250OptionButtonBackground;
    //    public GameObject Jawa2sCameraViewButton;
    public GameObject Jawa2sCameraViewButton;
    public GameObject Jawa2sPopsCracklesButton;



    public int iJawa2soptions = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AllOptionButtonsActive()  // 
    {
        if (iJawa2soptions == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Jawa2sViewsButton.SetActive(true);
            Jawa2sExhaustButton.SetActive(true);
            //      ZX10ROptionButtonBackground.SetActive(true);
      //      Jawa2sCameraViewButton.SetActive(true);
      //      Jawa2sPopsCracklesButton.SetActive(true);
            iJawa2soptions = 1;

        }
        else if (iJawa2soptions == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Jawa2sViewsButton.SetActive(false);
            Jawa2sExhaustButton.SetActive(false);
     //       Jawa2sPopsCracklesButton.SetActive(false);
            //       ZX10ROptionButtonBackground.SetActive(false);
      //      Jawa2sCameraViewButton.SetActive(false);
            iJawa2soptions = 0;

        }

    }


}

