﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedArrow1Jawa2sScript : MonoBehaviour
{
    //  public Image image;
    public GameObject ImageKeyin, ImageStart, ImageThrottle, TextKeyin, TextStart, TextThrottle;
    int iImageKeyinactive = 1;
    int iImageStartactive = 1;
    int iImageThrottleactive = 1;
    int istart = 0;
    int iAnimateoff = 0;
    // Start is called before the first frame update
    void Start()
    {
//        PlayerPrefs.SetInt("IntroAnimation", 0);
        //   image = GetComponent<Image>();
        if (PlayerPrefs.GetInt("IntroAnimation") == 0)
        {
            iAnimateoff = 0;
            AnimateArrow1();
            PlayerPrefs.SetInt("IntroAnimation", 1);
        }
        else iAnimateoff = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineJawa2s.ikeyinfromtouch == 2 && istart == 0 && iAnimateoff == 0)
        {
            AnimateArrow2();
            istart = 1;

        }
        else if (AudioEngineJawa2s.istartfromtouch == 2 && istart == 1 && iAnimateoff == 0)
        {
            //       Debug.Log("INsideThrittle");
            AnimateArrow3();
            istart = 2;
        }
    }

    public void AnimateArrow1()
    {

        StartCoroutine("AnimateArrow1Sub");
        ///    image.color = Color.black;image
        //   matbody1.color = Color.Lerp(Color.red, Color.black, icolorshade);
    }

    public void AnimateArrow2()
    {

        StartCoroutine("AnimateArrow2Sub");

    }
    public void AnimateArrow3()
    {

        StartCoroutine("AnimateArrow3Sub");

    }

    private IEnumerator AnimateArrow1Sub()
    {
        yield return new WaitForSeconds(0.1f);
        //   image.color = Color.white;
        while (AudioEngineJawa2s.ikeyinfromtouch != 2)
        {
            TextKeyin.SetActive(true);
            //  Debug.Log("INside Color Change");
            yield return new WaitForSeconds(0.25f); // wait half a second
                                                    //   if (image.color == Color.white) image.color = Color.blue;
                                                    //   else image.color = Color.white;
            if (iImageKeyinactive == 1)
            {
                ImageKeyin.SetActive(false);
                //     Text.SetActive(false);
                iImageKeyinactive = 0;
            }
            else if (iImageKeyinactive == 0)
            {
                ImageKeyin.SetActive(true);
                //   Text.SetActive(true);
                iImageKeyinactive = 1;
            }
            //   else image.color = Color.white;

        }
        TextKeyin.SetActive(false);
        ImageKeyin.SetActive(false);
    }

    private IEnumerator AnimateArrow2Sub()
    {
        yield return new WaitForSeconds(0.1f);
        //   image.color = Color.white;

        while (AudioEngineJawa2s.istartfromtouch != 2)
        {

            //  Debug.Log("INside Color Change");
            yield return new WaitForSeconds(0.25f); // wait half a second
                                                    //   if (image.color == Color.white) image.color = Color.blue;
            TextStart.SetActive(true);                                      //   else image.color = Color.white;
            if (iImageStartactive == 1)
            {
                ImageStart.SetActive(false);
                //     Text.SetActive(false);
                iImageStartactive = 0;
            }
            else if (iImageStartactive == 0)
            {
                ImageStart.SetActive(true);
                //   Text.SetActive(true);
                iImageStartactive = 1;
            }
            //   else image.color = Color.white;

        }
        ImageStart.SetActive(false);
        TextStart.SetActive(false);
    }


    private IEnumerator AnimateArrow3Sub()
    {
        yield return new WaitForSeconds(0.1f);
        //   image.color = Color.white;

        while (AudioEngineJawa2s.itrate < 2)
        {

            //  Debug.Log("INside Color Change");
            yield return new WaitForSeconds(0.25f); // wait half a second
                                                    //   if (image.color == Color.white) image.color = Color.blue;
            TextThrottle.SetActive(true);                                      //   else image.color = Color.white;
            if (iImageThrottleactive == 1)
            {
                ImageThrottle.SetActive(false);
                //     Text.SetActive(false);
                iImageThrottleactive = 0;
            }
            else if (iImageThrottleactive == 0)
            {
                ImageThrottle.SetActive(true);
                //   Text.SetActive(true);
                iImageThrottleactive = 1;
            }
            //   else image.color = Color.white;

        }
        ImageThrottle.SetActive(false);
        TextThrottle.SetActive(false);
    }



}

