﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CB400PopsCracklesMenu : MonoBehaviour
{
    int iCB400PopsCrackles = 0;
    // Other Game Options Appear/Disappear
    public GameObject CB400ExhaustOptions;
    public GameObject CB400LocationOptions;
    public GameObject CB400ViewsButton;  // Deactivate Exhaust Button when Console View


    public GameObject PopsCracklesButton1;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CB400PopsCracklesMenu1()
    {
        if (iCB400PopsCrackles == 0)
        {
            PopsCracklesButton1.SetActive(true);
            //        DefaultView.SetActive(true);
            //        ConsoleView.SetActive(true);

            CB400ExhaustOptions.SetActive(false);
            CB400LocationOptions.SetActive(false);
    //        CB400ViewsButton.SetActive(false);
            iCB400PopsCrackles = 1;
        }
        else if (iCB400PopsCrackles == 1)
        {
            PopsCracklesButton1.SetActive(false);
            //          DefaultView.SetActive(false);
            //          ConsoleView.SetActive(false);

            CB400ExhaustOptions.SetActive(true);
            CB400LocationOptions.SetActive(true);
     //       CB400ViewsButton.SetActive(true);
            iCB400PopsCrackles = 0;
        }
    }
    public void CB400PopsCracklesDropDownDeactivate()  // From Main Options Button on top
    {
        PopsCracklesButton1.SetActive(false);
        iCB400PopsCrackles = 0;
    }

}
