﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MustangViews : MonoBehaviour
{
    public GameObject MustangGarageButton;
    public GameObject MustangRoadButton;
    public GameObject MustangTunnelButton;
    public GameObject MustangLocationButton;

    int iMustangLocation = 0;
    public int iMustangview = 0;   // Appear/Disapperar Counter
    public static int iMsutangthisLocationbutton = 0;
    public GameObject MustangDriveOptions;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Locations()  // 
    {



        if (iMustangLocation == 0)
        {
            MustangGarageButton.SetActive(true);
            MustangRoadButton.SetActive(true);
            MustangTunnelButton.SetActive(true);
     //       MustangDriveOptions.SetActive(false);
            //          MustangViewOptions.SetActive(false);
            iMustangLocation = 1;
            MustangCustomizeOptions.iMsutangthisCustomizebutton = 0;
        }
        else if (iMustangLocation == 1)
        {
           
            MustangGarageButton.SetActive(false);
            MustangRoadButton.SetActive(false);
            MustangTunnelButton.SetActive(false);
   //         MustangDriveOptions.SetActive(true);
            //        MustangViewOptions.SetActive(true);
            iMustangLocation = 0;
        }
    }
    public void LocationsDeactivate()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        MustangGarageButton.SetActive(false);
        MustangRoadButton.SetActive(false);
        MustangTunnelButton.SetActive(false);
  //      MustangLocationButton.SetActive(false);

        iMustangLocation = 0;

    }

    public void LocationsButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
 //       Debug.Log("ViewsButton=");
 //       Debug.Log(iMsutangthisLocationbutton);
        if (iMsutangthisLocationbutton == 0)
        {
   //         Debug.Log("deactivating Views");
            MustangLocationButton.SetActive(false);
            iMsutangthisLocationbutton = 1;
        }
        else if (iMsutangthisLocationbutton == 1)
        {
  //          Debug.Log("Activating Views");
            MustangLocationButton.SetActive(true);
            iMsutangthisLocationbutton = 0;
        }
    }


    public void GarageRoadButtonsActive()  // 
    {
        if (iMustangview == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            MustangGarageButton.SetActive(true);
            MustangRoadButton.SetActive(true);


            iMustangview = 1;
        }
        else if (iMustangview == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            MustangGarageButton.SetActive(false);
            MustangRoadButton.SetActive(false);
            iMustangview = 0;
        }

    }
}


