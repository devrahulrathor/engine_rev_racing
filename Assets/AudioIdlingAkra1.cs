﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingAkra1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (AudioEngine.irpm == 0)
        {
            AudioEngine.rpm1 = AudioEngine.rpmidling;
        }
    }

    public void IdlingAkra1()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngine.idlingvol;
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingAkra1Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       Idlingvolume = 0;


    }

    double Idlingvolume = 1.0;
    public void IdlingAkra1Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

            AudioEngine.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingAkra1Fadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        Idlingvolume = Idlingvolume + 0.1;
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
        else
        {
            audio.volume = 1;
            AudioEngine.ifadeinidling = 0;

            //            audio.Stop();
        }
    }

    public void IdlingAkra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingAkra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}
