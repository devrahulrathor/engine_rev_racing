﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP3000R35 : MonoBehaviour
{
    int[] posrpmarrayblip3000 = { 2000, 2300, 2500, 2700, 3000, 3200 };
    double[] rpmposarrayblip3000 = { 0.1, 0.1, 0.1, 0.11, 0.11, 0.11 };
    public static float dt3000 = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {
            //        RPMCal();
            //    Debug.Log("Inside 3000Blip irpmindex=");
            //            Debug.Log(irpmarrayindex);

        }
        LowPassFilter();
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip3000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {

            audio.Play();

        }
        else if (AudioEngineR35.rpm1 < 2000)
            //           dt3000 = 0.096f;
            dt3000 = 0.08f;
        audio.PlayScheduled(AudioSettings.dspTime + dt3000);
        //      Debug.Log("Inside 3000BlipPlay");
        audio.volume = 1;
        //       audio.Play();
        //       audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();
        irpmarrayindex = (int)((audio.time * 10000) / 100);

        //       rpm = posrpmarrayblip[irpmarrayindex];
        //         textBox2.Text = position.ToString();
        //       if (audio.time <= 0.32)
        //         if(AudioEngineR35.rpm1 <= 7000)
        //       {
        AudioEngineR35.rpm1 = posrpmarrayblip3000[irpmarrayindex];
        //       }
        //     else AudioEngineR35.rpm1 = 0;
        //     AudioEngineR35.itrate = -1;

    }

    public void Blip3000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.SetScheduledEndTime(1000);
        //    audio.Stop();
    }

    public void Blip3000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip3000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}

