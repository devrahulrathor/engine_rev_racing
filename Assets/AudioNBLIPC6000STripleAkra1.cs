using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPC6000STripleAkra1 : MonoBehaviour
{
 //   int[] posrpmarrayfsr6000 = { 7200, 7200, 6000, 5000, 4000, 2500, 2000, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1150, 1150, 1150 };

 //   double[] rpmposarrayfsr6000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };
    int[] posrpmarrayfsr6000 = { 6000, 6000, 6000, 5500, 5000, 4500, 3800, 3800, 3000, 2500, 2400, 1800, 1250, 1250, 1250 };

    double[] rpmposarrayfsr6000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineSTriple.itrate == 6005)
        {
            //          Debug.Log("INside BlipC5000(6000) update RPMCAL");
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineSTriple.rpm1 = posrpmarrayfsr6000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineSTriple.rpm1 <= (AudioEngineSTriple.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }
    public void BLIPC6000()
    {

        //     Debug.Log("INside BlipC5000(6000)");
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1;
        audio.Play();
        audio.Play(44100);
    }

    public void BLIPC6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }


    double Blip6000volume = 1.0;
    public void BLIPCR6000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip6000volume = Blip6000volume - 0.1;
        if (Blip6000volume > 0) audio.volume = (float)Blip6000volume;
        else
        {

            AudioEngineSTriple.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
        }
    }
    public void Blipc6000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blipc6000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //    audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}

