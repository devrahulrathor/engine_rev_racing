using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentRPMConsoleZX25 : MonoBehaviour
{
    public GameObject Needle;
    float danglerot = 0, anglerot = 0, anglerotidling = -11;
    int rpmold;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        danglerot = -(MainControllerDrag.rpm1 - rpmold) / 100;


        if (anglerot != anglerotidling && MainControllerDrag.rpm1 == MainControllerDrag.rpmidling)
        {
            //    Needle.transform.Rotate(0, 0, anglerot-anglerotidling);   // Leaning Bike
            danglerot = -anglerot + anglerotidling;
        }
        Needle.transform.Rotate(0, 0, danglerot);   // Leaning Bike
        anglerot = anglerot + danglerot;
        if (danglerot != 0)
        {
            //     Debug.Log("DagleRot=");
            //     Debug.Log(danglerot);
            //     Debug.Log("-----------------AngleRot=");
            //          Debug.Log(anglerot);
        }
        rpmold = MainControllerDrag.rpm1;
    }
}

