using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BikeControllerMother : MonoBehaviour
{
    public GameObject BikeZX10;
    public AudioEngineDrag AudioEngine;
    private Rigidbody bikejawa;
   
  //  public Rigidbody BikeLeftCollider;// Collider for Left Crashes
    public Slider RightTurnSlider;
    public Slider LeftTurnSlider;

    int i=0;
    int it = 0;
   float vehicleposition = 0;  //Tracking Vehhicle Position for Tunnel Sound Change
   float vehiclepositionold = 0;
    float vehicledirection = 0;  // +/- for Tunnel Sound
    int ivehicleintunnel = 0; 

    public int isteer = 0;  // -1 Left, 1 Right Constrolled from Touch Arrows
                            //    public WheelCollider WColForward;
                            //    public WheelCollider WColBack;

    public float maxMotorTorque = 400;

    private WheelData[] wheels;
    //  public Transform wheelF;
    //  public Transform wheelB;
    public int iaccelerometerTurnTest = 1;
    public float accelvaltest = 0;
    public float maxleananglemagnitude= 50;
    public Slider AcceltestSlider;


    private float forwardspeed = 10;
    public Slider accelpedal;
    public float maxSteeringAngle = 30f; // maximum steer angle the wheel can have // 35 initial value
    float steering;
    float maxleanangle = 40.0f;
    float maxleanangletemp;
    float angletilt = 0;  //Tilt Angle while steering
    float anglelean = 0;   // Lean Angle while cornering
    float danglelean = 50f;  // Delta Lean angle every Update  
    float dangleleanmultiplier = 50; // User control multiplier for lean angle change(high val means rapid leaning -Provide a user setting depending on Bike
    float devicetiltrate = 1; // To lean the bike as per the mobile rotation rate
    float inputaccelxold;  // To caclculate tilte rate
    int rightarrowengaged = 0, leftarrowengaged = 0;
    private int ivehiclecrashed = 0;  // Detect Vehicle Crash mode, 1 when crashed
    private int ivehiclecrashing = 0;  // Detect Vehicle Crash mode starts, 1 when crash starts to put the vehicle back on track after low speed collision

    //check the collision with object Strings
    String str = "GeeksforGeeks";
    String substr1 = "r_fence";  // Middle Metallic Divider
    String substr2 = "r_block";   // Side Concerte Wall
    //   String substr2 = "For";
    //Find location of Cebtre Crash Rails/ Side Concrete Boundaries
    int isiderailcollisionexit = 0;  // 1 if Vehicle left Guard Rail mesh Collider Trigget
    int icollisiondirection = 0; // -1 for Left Side Collision and 1 for Right

   


    // using String.Contains() Method
    // Console.WriteLine(str.Contains(substr1));


    public class WheelData
    {

        public WheelData(Transform transform, WheelCollider collider)
        {
            wheelTransform = transform;
            wheelCollider = collider;
            wheelStartPos = transform.transform.localPosition;
        }

        public Transform wheelTransform;
        public WheelCollider wheelCollider;
        public Vector3 wheelStartPos;
        public float rotation = 0f;
    }



    // Start is called before the first frame update
    void Start()
    {
        bikejawa = this.GetComponent<Rigidbody>();

        wheels = new WheelData[2];
        //     wheels[0] = new WheelData(wheelF, WColForward);
        //     wheels[1] = new WheelData(wheelB, WColBack);
        gameObject.tag = "Player";
    }

    //Collosion Detection
    void OnCollisionEnter(Collision collision)
    {
        //Ouput the Collision to the console
        Debug.Log("Collision : " + collision.gameObject.name);
    //    if (collision.gameObject.name != "Line001" && collision.gameObject.name != "Line001_2" && collision.gameObject.name != "Road_Track_A_01" && collision.gameObject.name != "Track_C_002" && collision.gameObject.name != "Track_B_Mesh" && Mathf.Abs(bikejawa.velocity.x) > 200)
    //    {
            //         ivehiclecrashed = 1;
            //       BikeZX10.SetActive(false);

    //    }
       if( collision.gameObject.name == "r_fence_cv")
        {
       //     Debug.Log("Collision with Ventral Divider");
        }

          Collider myCollider = collision.contacts[0].thisCollider;  // Generates Garbage
     //   Collider myCollider = collision.GetContact(0).Collider;  // Need to check this over above method
        //    Debug.Log("Collision at");
        //    Debug.Log(myCollider);

        if (myCollider.name == "Bike_Collider_Left" || myCollider.name == "Bike_Collider_Right")
        {
            if (myCollider.name == "Bike_Collider_Left") icollisiondirection = -1; 
            else if(myCollider.name == "Bike_Collider_Right") icollisiondirection = 1; 
            //      if (collision.contacts[0].otherCollider.transform.gameObject.name == "Bike_Collider_Left") { 
            AudioEngine.BrakeFromTouch();
            StartCoroutine("BrakeRelease"); //Brake Release to reduce speed to say 20 KMPh
            //   ivehiclecrashing = 1;
            if (Mathf.Abs(AudioEngineDrag.bikespeed) > 0 && Mathf.Abs(AudioEngineDrag.bikespeed) < 100)
            {
                       StopCoroutine("SlowSpeedSideRailCrash");
                  if(icollisiondirection == -1) transform.Rotate(0, 10, 0);  // Steering Parent bikejawa
                else if (icollisiondirection == 1) transform.Rotate(0, -10, 0);  // Steering Parent bikejawa
                StartCoroutine("SlowSpeedSideRailCrash");
            }
            Debug.Log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Bike Left Collider ");
        }
      

        if (collision.gameObject.name.Contains(substr1) || collision.gameObject.name.Contains(substr2))
        {
            //   var force = transform.position - collision.transform.position;
            // normalize force vector to get direction only and trim magnitude
            //   force.Normalize();
            //   bikejawa.AddForce(force * 50000);
        //    AudioEngine.BrakeFromTouch();

             Vector3 targetDirection = (transform.position - collision.transform.position).normalized;
            //  Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 10f, 0.0f);
            //bikejawa.transform.rotation = Quaternion.LookRotation(-newDirection);

            //output to log the position change
    //      Debug.Log("Bike Position=");
    //        Debug.Log(transform.position);
    //        Debug.Log("SideRail Position=");
    //        Debug.Log(collision.transform.position);
    //       if((bikejawa.transform.position.z - collision.transform.position.z) < 0)
    //        {
       //         Debug.Log("RIGHT Turn");
//            }

       //              else
       //             {
              //          Debug.Log("LEFT Turn");
         //          }
            //    Debug.Log("Target Direction=");
            //    Debug.Log(targetDirection);

            //          if (Vector3.Dot(transform.position, collision.transform.position) > 0)
            //             {
            //                 Debug.Log("RIGHT Turn");
            //             }

            // else
            // {
            //     Debug.Log("LEFT Turn");
            // }




        }
        if (collision.gameObject.name.Contains(substr1) || collision.gameObject.name.Contains(substr2))
        {
            //     if (isteer == 1) Debug.Log("___________________Right Crash");
            //      else if (isteer == -1) Debug.Log("___________________Left Crash");

            //     AudioEngine.BrakeFromTouch();
            //   Debug.Log("Collision with Ventral Divider" + collision.gameObject.name);
            //    bikejawa.angularVelocity = Vector3.zero;
            //  bikejawa.transform.position.z = 
            //      bikejawa.transform.position = new Vector3(1, 1, 1);
            //   ivehiclecrashing = 1;
            if (Mathf.Abs(AudioEngineDrag.bikespeed) > 0 && Mathf.Abs(AudioEngineDrag.bikespeed) < 100)
            {
                //       StopCoroutine("SlowSpeedSideRailCrash");
                //   transform.Rotate(0, 10, 0);  // Steering Parent bikejawa
                //        StartCoroutine("SlowSpeedSideRailCrash");
            }
        }


        // force is how forcefully we will push the player away from the enemy.
    //    float force = -1000;

        // If the object we hit is the enemy
        //     if (collision.gameObject.tag == "enemy")
        //     {
        // Calculate Angle Between the collision point and the player
        // how much the character should be knocked back
    //    var magnitude = 50000;
        // calculate force vector
        Vector3 dir = transform.position - collision.transform.position;
        // normalize force vector to get direction only and trim magnitude
        // dir.Normalize();
        //   bikejawa.AddForce(dir* magnitude);
        //     }

      
    }

    void OnCollisionExit(Collision collision)
    {
     //     Debug.Log("Exit Collision : " + collision.gameObject.name);
        // Gravity Modifier when Bike flies out of road so as to drop it back
     
        isiderailcollisionexit = 1;

    }
        // Update is called once per frame
        void FixedUpdate()
    {

        vehiclepositionold = vehicleposition;



      


        //    danglelean = maxleananglemagnitude * (1 + Mathf.Abs((Input.acceleration.x - inputaccelxold) / Time.deltaTime));

        //      detectCollision();   // Avoid Bouncing off and tipping over on collision
        //      steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        //     Debug.Log("Steering=");
        //     Debug.Log(steering);
        //           bikejawa.velocity = accelpedal.value *  (forwardspeed* Vector3.left + (steering/100) * Vector3.forward);  // Forward


        //        Debug.Log("bikejawa Velocity=");
        //            Debug.Log(bikejawa.velocity.x);

        //Detect when there is a collision starting
        //       AccelerometerTurnTest();
        //    ivehiclecrashed = 1;
        // Testing Vehicle Direction for Collision Sequence
        //     Debug.Log("Velocity x=");
        //     Debug.Log(bikejawa.velocity.x);
        //     Debug.Log("Velocity z=");
        //     Debug.Log(bikejawa.velocity.z);
        //output to log the position change
    //    Debug.Log("Bike Position=");
    //    Debug.Log(bikejawa.transform.position);
        


        //Module for Mobile Accelerometer
        danglelean = dangleleanmultiplier * (1 + Mathf.Abs((Input.acceleration.x - inputaccelxold) / Time.deltaTime));  // 50 for maxleananglemagnitude calibrated
   //     maxleanangle = Mathf.Abs(Input.acceleration.x * maxleananglemagnitude);
   //    if (Input.acceleration.x < -0.005) isteer = -1;
   //    else if (Input.acceleration.x > 0.005) isteer = 1;
       //Module for Accelerometer over

        if (ivehiclecrashed == 0 && ivehiclecrashing == 0 && AudioEngineDrag.gear > 0)
            {
            //        bikejawa.velocity = accelpedal.value * (transform.right * -forwardspeed + transform.forward * (steering / 100));
            bikejawa.velocity =  transform.right * AudioEngineDrag.bikespeed * 0.5f * -5;
         
            //    Debug.Log("Bike Vel=");
            //        Debug.Log(bikejawa.velocity.x);
            //    bikejawa.AddForce( 10000, 0, 0, ForceMode.Impulse);
            //       updateWheels();
            //      wheelB.Rotate(0f, WColBack.rpm / 60 * 360 * Time.deltaTime, 0f);
            //       WColBack.motorTorque = maxMotorTorque * 1;

            //               Debug.Log("bikejawa Velocity=");
            //               Debug.Log(bikejawa.velocity);

            //     maxleanangle = maxleanangletemp;
            // BikeTilting while steering
            //Right
            //        if (Input.GetAxis("Horizontal") < 1 && Input.GetAxis("Horizontal") > 0)
            //   if (Input.acceleration.x > 0) AccelerometerTurnVal();
            if (isteer == 1)
            //     if (Input.GetKey(KeyCode.RightArrow))
            {

                //   steering = maxSteeringAngle * 0.1f;
             //          transform.Rotate((-bikejawa.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);

                //      Debug.Log("Right********************************Angle Lean=");
                //      Debug.Log(anglelean);
                //        Debug.Log("maxleanangle=");
                //      Debug.Log(-maxleanangle);
                if (anglelean > -maxleanangle)  //
                {
                    if (Mathf.Abs(bikejawa.velocity.x) > 4 || Mathf.Abs(bikejawa.velocity.z) > 4)  {  // Lean and Steering starts after 4 KMph so at gear 1 at start no leaning
                    //    danglelean = 50 * (1+Mathf.Abs((Input.acceleration.x - inputaccelxold) / inputaccelxold));
                        anglelean = anglelean + (-danglelean * Time.deltaTime);
                        BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                  //      if (Mathf.Abs(bikejawa.velocity.x) > 30) BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                  //     else BikeZX10.transform.Rotate(0, 0, (-danglelean/1.5f) * Time.deltaTime);   // Leaning Bike
                                                                                              //     anglelean = anglelean + (maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);
                                                                                              //     BikeZX10.transform.Rotate(0, 0, maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);   // Leaning Bike
                    }
                                                                                                  //     Debug.Log("Right Angle Lean=");
                                                                                                  //     Debug.Log(anglelean);
                                                                                                  //     steering = maxSteeringAngle * 30 * Time.deltaTime;
                }
                steering = (maxSteeringAngle * -anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
             //   if(Mathf.Abs(bikejawa.velocity.x) < 30) steering = (maxSteeringAngle*1.5f * -anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);  // New Line
                Debug.Log("Right Steering Angle =");
                Debug.Log(steering);
                Debug.Log("Lean Angle =");
                Debug.Log(anglelean);
                transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa
             
                angletilt = angletilt + 1f * steering * Time.deltaTime;



                //      rightarrowengaged = 1;
                //       Debug.Log("Negativ eAngle Lean=");
                //       Debug.Log(anglelean);
            }

            //Left
            //     else if (Input.GetAxis("Horizontal") > -1 && Input.GetAxis("Horizontal") < 0)
            else if (isteer == -1)
            //     else if (Input.GetKey(KeyCode.LeftArrow))
            {
                //   steering = maxSteeringAngle * -0.1f;
                //          transform.Rotate((-bikejawa.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);


                if (anglelean < maxleanangle)  //
                {
                    //       Debug.Log("Left********************************Angle Lean=");
                    //       Debug.Log(anglelean);
                    if (Mathf.Abs(bikejawa.velocity.x) > 4 || Mathf.Abs(bikejawa.velocity.z) > 4)
                    {  // Lean and Steering starts after 4 KMph so at gear 1 at start no leaning

                             anglelean = anglelean + (danglelean * Time.deltaTime);
                        BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                     //   if (Mathf.Abs(bikejawa.velocity.x) > 30)  BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                     //       else BikeZX10.transform.Rotate(0, 0, (danglelean/1.5f) * Time.deltaTime);   // Test Line
                                                                                                 //     anglelean = anglelean + (maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);
                                                                                                 //     BikeZX10.transform.Rotate(0, 0, maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);   // Leaning Bike
                    }
                }
                steering = -(maxSteeringAngle * anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
            //    if (Mathf.Abs(bikejawa.velocity.x) < 30) steering = -(maxSteeringAngle* 1.5f * anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);  // Test Line
                Debug.Log("Left Steering Angle =");
                Debug.Log(steering);
                Debug.Log("Lean Angle =");
                Debug.Log(anglelean);

               transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa
         
                angletilt = angletilt - 1f * steering * Time.deltaTime;




                //    leftarrowengaged = 1;
                //      Debug.Log("Positive Angle Lean=");
                //      Debug.Log(anglelean);
            }

          else 
        //    else if(isteer == -10000)  // Mobile Accelerometer Module
            {
                //             Debug.Log("Entered recover steering left");
                if (anglelean >= 5)
                {
                    //               Debug.Log("Entered steering left");
                    //      steering = -maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent bikejawa
                    BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                    anglelean = anglelean - (danglelean * Time.deltaTime);
                    //New
                    steering = -(maxSteeringAngle * anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                    transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa
                }
                if (anglelean <= -5)
                {
                    //     steering = maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent bikejawa
                    BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                    anglelean = anglelean + (danglelean * Time.deltaTime);
                    //New
                    steering = (maxSteeringAngle * -anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                    transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa
                }
    //            Debug.Log("Recover Steering Angle =");
    //            Debug.Log(steering);
    //            Debug.Log("Recover Lean Angle =");
    //            Debug.Log(anglelean);
            }

            if (angletilt > 0 && rightarrowengaged != 1)
            {
                //        transform.Rotate(-0.5f * steering * Time.deltaTime, 0, 0);
            }
            if (angletilt < 0 && rightarrowengaged != 1)
            {
                //      transform.Rotate(0.5f * steering * Time.deltaTime, 0, 0);
            }
            //    bikejawa.AddForce(accelpedal.value * forwardspeed * (Vector3.forward + Vector3.back / 2));

            //     bikejawa.velocity = accelpedal.value * forwardspeed * (Vector3.left);  // Forward

            //     bikejawa.velocity = accelpedal.value * forwardspeed * (Vector3.left + steering* Vector3.forward);  // Forward
        } // Crashed Brace

        // Detect Tunnels
        //Detect Direction
        vehicleposition = bikejawa.transform.position.x;
        vehicledirection = vehicleposition - vehiclepositionold;
        //  Debug.Log("Vehicle Direction=");
        //  Debug.Log(vehicledirection);
        //      if (bikejawa.transform.position == new Vector3(1, 1, 1))

        if (vehicledirection < 0 && ivehicleintunnel != 1) it = 0;
        else if (vehicledirection > 0 && ivehicleintunnel != 1) it = 1;
        
            if (bikejawa.transform.position.x <= -12400 && bikejawa.transform.position.x > -13000 && it == 0)
            {
            Debug.Log("Inside Tunnel 1");
            if (vehicledirection < 0)
            {
                StartCoroutine("FadeinTunnelSound");
                ivehicleintunnel = 1;
            }
            else if (vehicledirection > 0)
            {
                StartCoroutine("FadeoutTunnelSound");
                ivehicleintunnel = 0;
            }
            it++;
        }
            if (bikejawa.transform.position.x <= -14502 && bikejawa.transform.position.x > -15400 && it == 1)
            {
            Debug.Log("Inside Tunnel 2");
            if (vehicledirection < 0)
            {
                StartCoroutine("FadeoutTunnelSound");
                ivehicleintunnel = 0;
            }
            else if (vehicledirection > 0)
            {
                StartCoroutine("FadeinTunnelSound");
                ivehicleintunnel = 1;
            }
            it--;
           
                }
      //  if (vehicledirection > 0) it = 1;
      //  else it = 0;

        if (bikejawa.transform.position.x <= -12400 && bikejawa.transform.position.x > -14502 && it == 0)
            {
       //     it++;  // Counter to decide if bike in tunnel , 1-in Tunnel, 0 out
                   //     bikejawa.transform.position = new Vector3(1, 1, 1);
                   //      AudioEngineDrag.ReverbXoneMix = 1.1f;
                   //   AudioEngine.TunnelAppear();
         //   Debug.Log("Tunnelin");
         //   StartCoroutine("FadeinTunnelSound");
        }
        if ((bikejawa.transform.position.x <= -14502 || bikejawa.transform.position.x > -12400) && it == 1)
        {
      //      it--;
            //     bikejawa.transform.position = new Vector3(1, 1, 1);
            //      AudioEngineDrag.ReverbXoneMix = 1.1f;
            //   AudioEngine.RoadAppear();
        //    Debug.Log("Tunnelout");
        //    Debug.Log("IT=");
        //    Debug.Log(it);
        //    StartCoroutine("FadeoutTunnelSound");
        }

        inputaccelxold = Input.acceleration.x;
    }


    IEnumerator FadeinTunnelSound()
    {
        i = 0;
        AudioEngineDrag.ReverbXoneMix = 0;
          //    Debug.Log("Tunnel Out SOund***************************************1");
        //Debug.Log("TunnelReverboneMix=***************************************1");
        //Debug.Log(AudioEngineDrag.ReverbXoneMix);
        while ( i  <= 11)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.05f); // wait half a second
            AudioEngineDrag.ReverbXoneMix = AudioEngineDrag.ReverbXoneMix + 0.1f;
            AudioEngine.TunnelAppear();
            i++;
            //       else if (iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
        AudioEngineDrag.ReverbXoneMix = 1.1f;
    }

  
   
    IEnumerator FadeoutTunnelSound()
    {
        i = 0;

       // Debug.Log("Tunnel In SOund***************************************1");
       // Debug.Log("TunnelReverboneMix=***************************************1");
       // Debug.Log(AudioEngineDrag.ReverbXoneMix);
        while (i <= 11)
        {
            
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.05f); // wait half a second
            AudioEngineDrag.ReverbXoneMix = AudioEngineDrag.ReverbXoneMix - 0.1f;
           AudioEngine.TunnelAppear();
            i++;
            //       else if (iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
        AudioEngineDrag.ReverbXoneMix = 0f;
    }


    private void updateWheels()
    {
        float delta = Time.fixedDeltaTime;

        foreach (WheelData w in wheels)
        {
            WheelHit hit;

            //    Vector3 localPos = w.wheelTransform.localPosition;
            if (w.wheelCollider.GetGroundHit(out hit))
            {
                //      localPos.y -= Vector3.Dot(w.wheelTransform.position - hit.point, transform.up) - wheelRadius;
                //      w.wheelTransform.localPosition = localPos;
            }
            else
            {
                //     localPos.y = w.wheelStartPos.y;
            }

            w.rotation = Mathf.Repeat(w.rotation + delta * w.wheelCollider.rpm * 360.0f / 60.0f, 360f);
            //     WColForward.steerAngle = Mathf.Clamp(1, -1, 1) * maxSteeringAngle;
            w.wheelTransform.localRotation = Quaternion.Euler(w.rotation, 0, 0);
            //      w.wheelTransform.rotation = Quaternion.Euler(w.rotation, 0, 0); ;
        }
    }

    public void LeftSteeron()
    {
        isteer = -1;
    }
    public void LeftSteeroff()
    {
        isteer = 0;
    }
    public void RightSteeron()
    {
        isteer = 1;
    }
    public void RightSteeroff()
    {
        isteer = 0;
    }

    public void RightTurnVal(float sliderposition)
    {

        if (sliderposition > 0) isteer = 1;
        else if (sliderposition < 0) isteer = -1;
        maxleanangle = sliderposition * isteer;




    }
    public void AccelerometerTurnVal()
    {
        Debug.Log("Accelerometer");
        if (Input.acceleration.x > 0) isteer = 1;
        else if (Input.acceleration.y < 0) isteer = -1;
        if (Input.acceleration.x > 40f)
        {
            maxleanangle = 40f;
        }
        else maxleanangle = Input.acceleration.x * 100 * isteer;





    }
    public void LeftTurnVal(float sliderposition)
    {
        isteer = -1;
        //   maxleanangle = sliderposition;
        accelvaltest = sliderposition;


    }
    public void AccelerometerTurnTest()
    {
        if (iaccelerometerTurnTest == 1 && ivehiclecrashed == 1)
        {
           BikeZX10.transform.Rotate(0, 0, Input.acceleration.x * maxleananglemagnitude * Time.deltaTime);   // Leaning Bike
       //     BikeZX10.transform.Rotate(0, 0, maxleananglemagnitude*accelvaltest * Time.deltaTime);   // Leaning Bike
        }
        else if (iaccelerometerTurnTest == 2)
        {
      //    BikeZX10.transform.Rotate(0, 0,  Input.acceleration.y * maxleananglemagnitude * Time.deltaTime);   // Leaning Bike
            
        }
        else if (iaccelerometerTurnTest == 3)
        {
        //    BikeZX10.transform.Rotate(0, 0, Input.acceleration.z * maxleananglemagnitude * Time.deltaTime);   // Leaning Bike
        }

    }
    public void AccelerometerTrunTestActivate()
    {
        if (ivehiclecrashed == 1) ivehiclecrashed = 0;
        else ivehiclecrashed = 1;
    }

    public void acceltestval1()
    {
        iaccelerometerTurnTest = 1;
    }
    public void acceltestval2()
    {
        iaccelerometerTurnTest = 2;
    }
    public void acceltestval3()
    {
        iaccelerometerTurnTest = 3;
    }
    public void AcceltestTurnVal(float sliderposition)
    {
 
    //    maxleananglemagnitude = sliderposition;


    }

    public void QuitGame()
    {
        Application.Quit();
    }

    int islscrash=0;
    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator SlowSpeedSideRailCrash()
    {
        //     if (ivibrator == 1) //Handheld.Vibrate();
        //         while (ivibrating == 1)
        Debug.Log(" Entered Low SPeed Crash Routine");
    //   if(anglelean > 0)  BikeZX10.transform.Rotate(0, 0, -anglelean);   // Leaning Bike
        for (i =1; i < 10; i++) 
            {
                yield return new WaitForSeconds(0.05f);
                if (islscrash == 1)
                {
                    transform.Rotate(0, 0.00f, 0);  // Steering Parent bikejawa
           //     BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
           if (icollisiondirection == -1) BikeZX10.transform.Rotate(0, 0, -1);   // Leaning Bike
                else if (icollisiondirection == 1) BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
                islscrash = 0;
                }
               else if (islscrash == 0)
                {
                //    transform.Rotate(0, 0.05f, 0);  // Steering Parent bikejawa
                //    BikeZX10.transform.Rotate(0, 0,  danglelean * Time.deltaTime);   // Leaning Bike
                if (icollisiondirection == -1) BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
                else if (icollisiondirection == 1) BikeZX10.transform.Rotate(0, 0, -1);   // Leaning Bike
                islscrash = 1;
                }
            }
  //      BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
    //    ivehiclecrashing = 0;
    //    transform.Rotate(0, 10, 0);  // Steering Parent bikejawa
   //     yield return new WaitForSeconds(0.05f);
   //     if(isiderailcollisionexit == 1)
   //     {
   //         transform.Rotate(0, -20, 0);  // Steering Parent bikejawa
   //         isiderailcollisionexit = 0;
   //     }

    }

    private IEnumerator BrakeRelease()
    {
        yield return new WaitForSeconds(0.5f);
        AudioEngine.BrakeReleaseFromTouch();
    }

    }
