﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jawa2sOptions : MonoBehaviour
{
    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void BackButton()
    {
        if (AudioEngineJawa2s.itrate != -1) AudioEngineJawa2s.istartfromtouch = 3;
        SceneManager.LoadScene(0);   // Motorbike Options
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
