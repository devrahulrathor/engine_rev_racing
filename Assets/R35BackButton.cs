using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class R35BackButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void BackButton()
    {
        if (AudioEngineR35.itrate != -1) AudioEngineR35.istartfromtouch = 3;
      //  AudioEngineR35.R35ExhaustMode = -1; 
        SceneManager.LoadScene(0); // Car Options1 Main Screen
    }
}