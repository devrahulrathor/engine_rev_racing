﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CB400ExhaustOptions : MonoBehaviour
{
    public GameObject CB400StockExhaust;
    public GameObject CB400Akra1Exhaust;
    public GameObject CB400Akra1CarbonExhaust;
    public GameObject CB400ExhaustForwardArrow;
    public GameObject CB400ExhaustBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject CB400LocationOptions;
    public GameObject CB400ViewOptions;
    public GameObject CB400PopsCracklesButton;

    public GameObject CantswitchExhaustNote;

    //   public GameObject CB400ExhaustBackImage;
    public int iCB400Exhaust = 0;   // Appear/Disapperar Counter
    public int iCB400ExhaustSwipeCounter = 1;   // Decide the Exhaust Option Active: 1 Garage Active, 2 Road Active

    public GameObject ZX10MainCameraView, CB400ConsoleCameraView;  // Camera Objects

    public static int iCB400Exhaust1 = 0;   // Appear/Disapperar Counter


    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Update()
    {

        if (ZX10CameraViewChange.iZX10CameraSwitch == 1)  // Deactivate Button for Exhaust View Change when Console View
        {

            GetComponent<Button>().interactable = false;
        }
        else if (ZX10CameraViewChange.iZX10CameraSwitch == 0)
        {

            GetComponent<Button>().interactable = true;
        }
        if (iCB400Exhaust1 == 110)  // Disabled
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iCB400ExhaustSwipeCounter == 2)
                {
                    CB400ExhaustForwardArrow.SetActive(true);
                    CB400ExhaustBackArrow.SetActive(true);
                }
                else if (iCB400ExhaustSwipeCounter == 3)
                {
                    CB400ExhaustForwardArrow.SetActive(false);
                    CB400ExhaustBackArrow.SetActive(true);
                }
                else if (iCB400ExhaustSwipeCounter == 1)
                {
                    CB400ExhaustForwardArrow.SetActive(true);
                    CB400ExhaustBackArrow.SetActive(false);
                }
                if (pos.y > 400 && pos.y < 600 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iCB400ExhaustSwipeCounter == 1 && iswipeenable == 1)
                        {
                            CB400StockExhaust.SetActive(false);
                            CB400Akra1Exhaust.SetActive(true);
                            iCB400ExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iCB400ExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            CB400Akra1Exhaust.SetActive(false);
                            CB400Akra1CarbonExhaust.SetActive(true);
                            iCB400ExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                    if (touchZero.deltaPosition.x > 10)
                    {

                        if (iCB400ExhaustSwipeCounter == 3 && iswipeenable == 1)
                        {
                            //        CB400TBR1Exhaust.SetActive(false);
                            CB400Akra1Exhaust.SetActive(true);
                            CB400Akra1CarbonExhaust.SetActive(false);
                            //               CB400StockExhaust.SetActive(false);
                            iCB400ExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iCB400ExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            CB400Akra1Exhaust.SetActive(false);
                            CB400StockExhaust.SetActive(true);
                            iCB400ExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }


    public void Exhausts()  // 
    {


        if (iCB400Exhaust1 == 0)
        {
            CB400StockExhaust.SetActive(true);
            CB400Akra1Exhaust.SetActive(true);
  //          CB400Akra1CarbonExhaust.SetActive(true);
            //         CB400LocationOptions.SetActive(false);
    //        CB400ViewOptions.SetActive(false);
            CB400LocationOptions.SetActive(false);
            CB400PopsCracklesButton.SetActive(false);

            //       ZX10MainCameraView.SetActive(true);
            //       CB400ConsoleCameraView.SetActive(false);

            iCB400Exhaust1 = 1;
        }
        else if (iCB400Exhaust1 == 1)
        {
            CB400StockExhaust.SetActive(false);
            CB400Akra1Exhaust.SetActive(false);
    //        CB400Akra1CarbonExhaust.SetActive(false);
            //           CB400LocationOptions.SetActive(true);
     //       CB400ViewOptions.SetActive(true);
            CB400LocationOptions.SetActive(true);
            CB400PopsCracklesButton.SetActive(true);
            iCB400Exhaust1 = 0;
        }
        CantswitchExhaustNote.SetActive(false);
    }

    public void ExhaustsDeactivatefromOptions()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        CB400StockExhaust.SetActive(false);
        CB400Akra1Exhaust.SetActive(false);
        //       CB400Akra1CarbonExhaust.SetActive(false);
        //           CB400LocationOptions.SetActive(true);
        CantswitchExhaustNote.SetActive(false);
        iCB400Exhaust1 = 0;
    }

    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
    public void ExhaustCantChangeNote()
    {
        if (AudioEngineI4.istartfromtouch == 2)
        {
            CantswitchExhaustNote.SetActive(true);
            StartCoroutine("ExhaustCantChangeNoteoff");
        }
    }

    private IEnumerator ExhaustCantChangeNoteoff()
    {
        yield return new WaitForSeconds(2f);
        //   image.color = Color.white;
        CantswitchExhaustNote.SetActive(false);

    }

}

