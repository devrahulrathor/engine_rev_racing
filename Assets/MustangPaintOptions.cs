﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MustangPaintOptions : MonoBehaviour
{
    public GameObject thisPaintButton, ColorButtons;
    public static int iMsutangthisPaintbutton = 0;
    public static int iMustangPaintOptionButtons = 0;
    // Start is called before the first frame update


    public void PaintButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        Debug.Log("VehicleButton=");
   //     Debug.Log(iMsutangthisVehiclebutton);
        if (iMsutangthisPaintbutton == 0)
        {

            thisPaintButton.SetActive(false);
            iMsutangthisPaintbutton = 1;
     //       ColorButtons.SetActive(false);
        }
        else if (iMsutangthisPaintbutton == 1)
        {
            thisPaintButton.SetActive(true);
            iMsutangthisPaintbutton = 0;
      //      ColorButtons.SetActive(true);
        }
    }


    public void PaintOptionButtonsDeactivate()  // 
    {
        ColorButtons.SetActive(false);
        iMustangPaintOptionButtons = 0;
    }

        public void PaintOptionButtons()  // 
    {


     
        if (iMustangPaintOptionButtons == 0)
        {
          ColorButtons.SetActive(true);
              iMustangPaintOptionButtons = 1;
            MustangCustomizeOptions.iMsutangthisCustomizebutton = 0;
        }
        else if (iMustangPaintOptionButtons == 1)
        {
          ColorButtons.SetActive(false);
         
            iMustangPaintOptionButtons = 0;
        }
    }

}
