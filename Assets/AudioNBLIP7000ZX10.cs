using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000ZX10 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (SoundEngineZX10.itrate == 5006)
        {
            SoundEngineZX10.rpm1 = 7000;
            //           Debug.Log(SoundEngineZX10.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineZX10.rpm1 >= 6000 && SoundEngineZX10.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (SoundEngineZX10.rpm1 >= 5000 && SoundEngineZX10.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.012F);
        }
        else if (SoundEngineZX10.rpm1 >= 4000 && SoundEngineZX10.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.032F);
        }
        else if (SoundEngineZX10.rpm1 >= 3000 && SoundEngineZX10.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.084F);
        }
        else if (SoundEngineZX10.rpm1 >= 2000 && SoundEngineZX10.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.148F);
        }


        else if (SoundEngineZX10.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.244F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }
}

