﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNSLIMustangGT350 : MonoBehaviour
{
    //    1100, 1400, 1800, 2200, 2500, 2700, 3000, 3300, 3400, 3500, 3800, 4100, 4400, 4700, 5000, 5200, 5400, 5600, 5800, 6000, 6300, 6600, 6900, 7200, 7400, 7500, 7500 };
    int[] posrpmarraysli = { 2000, 2600, 3000, 3400, 3800, 4200, 4600, 5000, 5400, 6600, 6800, 7500, 7500, 7500, 7500 };


  //    double[] rpmposarraysli = { 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.9, 0.9, 1, 1, 1, 1, 1, 1, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.7, 1.8, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2, 2, 2, 2, 2, 2.1, 2.1, 2.1, 2.1, 2.1, 2.1, 2.2, 2.2, 2.2, 2.2, 2.2, 2.2, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.4, 2.4, 2.4, 2.4, 2.4 };
    double[] rpmposarraysli = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 };



    int irpmarrayindex;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineMustang.itrate == 2)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }

    }
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();
        
        irpmarrayindex = (int)((audio.time * 1000) / 100);
        //      textBox2.Text = irpmarrayindex.ToString();

        AudioEngineMustang.rpm1 = posrpmarraysli[irpmarrayindex];
    }

    int irpmpos = 0;
    double posinsec;
    public void NSLI()
    {



        AudioSource audio = GetComponent<AudioSource>();
        //     AudioEngineMustang.rpm1 = AudioEngineMustang.rpmidling;
        irpmpos = (int)((AudioEngineMustang.rpm1 / 50) - 21);
  //      Debug.Log("RPM  in GT350 SLI=");
  //      Debug.Log(AudioEngineMustang.rpm1);
  //      Debug.Log("RPM Pos in GT350 SLI=");
  //      Debug.Log(irpmpos);
           if (irpmpos < 1) irpmpos = 1;  // Bug while irpmpos == -21 error
        //          textBox3.Text = irpmpos.ToString();     
        posinsec = rpmposarraysli[irpmpos];
        //          audio.volume = 1;   
        audio.time = (float)posinsec;
        audio.Play();
        audio.Play(44100);



    }
    public void NSLIStop()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.Stop();
    }

    double NSLIvolume = 1.0;
    public void NSLIFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        NSLIvolume = NSLIvolume - 0.1;
        if (NSLIvolume > 0) audio.volume = (float)NSLIvolume;
        else
        {

            AudioEngineMustang.ifadeoutsli = 0;
            audio.Stop();
            audio.volume = 1;
        }
    }

    public void NSLIReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void NSLIReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }
}


