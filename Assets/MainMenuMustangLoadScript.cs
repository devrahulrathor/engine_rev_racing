﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuMustangLoadScript : MonoBehaviour
{

    // Start is called before the first frame update
    public void MustangSceneLoad()
    {

        SceneManager.LoadScene(8);  // Mustang Scene

    }
}