using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentRPMConsoleSTriple : MonoBehaviour
{
    public GameObject Needle;
    float danglerot = 0, anglerot = 0, anglerotidling = 30;
    int rpmold;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if(AudioEngineSTriple.iconsolestartup == 1 || AudioEngineSTriple.iconsolestartup == -1)
        {
            danglerot = -(AudioEngineSTriple.rpmi - rpmold) / 35;
        }
        else danglerot = -(AudioEngineSTriple.rpm1 - rpmold) / 35;


        if (anglerot != anglerotidling && AudioEngineSTriple.rpm1 == AudioEngineSTriple.rpmidling)
        {
            //    Needle.transform.Rotate(0, 0, anglerot-anglerotidling);   // Leaning Bike
            danglerot = 1*(anglerot - anglerotidling);
        }
        Needle.transform.Rotate( 0, -danglerot/1.8f, -danglerot*0/2.2f, Space.Self);   // Leaning Bike
        anglerot = anglerot - danglerot;
        if (danglerot != 0)
        {
      //      Debug.Log("DagleRot=");
      //      Debug.Log(danglerot);
      //      Debug.Log("-----------------AngleRot=");
      //      Debug.Log(anglerot);
        }
       if (AudioEngineSTriple.iconsolestartup == 1 || AudioEngineSTriple.iconsolestartup == -1) rpmold = AudioEngineSTriple.rpmi;
       else rpmold = AudioEngineSTriple.rpm1;
    }
}

