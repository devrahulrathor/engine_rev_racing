﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBlip6000Mustang : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineMustang.itrate == 5005)
        {
            AudioEngineMustang.rpm1 = 6000;
            //           Debug.Log(AudioEngineMustang.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineMustang.rpm1 >= 5400 && AudioEngineMustang.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineMustang.rpm1 >= 4000 && AudioEngineMustang.rpm1 < 5400)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.199F);
        }
        else if (AudioEngineMustang.rpm1 >= 2000 && AudioEngineMustang.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.361F);
        }
   //     else if (AudioEngineMustang.rpm1 >= 2000 && AudioEngineMustang.rpm1 < 3000)
   //     {
   //         audio.PlayScheduled(AudioSettings.dspTime + 0.21F);
   //     }
        else if (AudioEngineMustang.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.552F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }
}


