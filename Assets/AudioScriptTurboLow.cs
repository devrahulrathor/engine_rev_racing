﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScriptTurboLow : MonoBehaviour
{
    private void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = Settings.turbovol;
    }
    public void Turbo()
    {
        //      StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        
        audio.PlayScheduled(AudioSettings.dspTime + AudioEngineMustang.dtcrackle);
        //       audio.Play();
        audio.Play(44100);
    }
    public void TurboStop()
    {
        //      StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayScheduled(AudioSettings.dspTime + AudioEngineMustang.dtcrackle);
        //       audio.Play();
        audio.Stop();
    }

    public void TurboReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void TurboReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }

}
