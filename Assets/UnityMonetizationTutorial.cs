﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class UnityMonetizationTutorial : MonoBehaviour, IUnityAdsListener
{


    string GooglePlay_ID = "3904057";
    bool testMode = true;
    string myPlacementId = "rewardedVideo";
    public Button myButton;

    // Start is called before the first frame update
    void Start()
    {
        //     myButton = GetComponent<Button>();
    //    myButton.interactable = false;

          Debug.Log("Ads Initialized");
        Advertisement.AddListener(this);
        Advertisement.Initialize(GooglePlay_ID, testMode);
    }
    void Update()
    {
        if (Advertisement.IsReady(myPlacementId))
        {
    //        Debug.Log("Advertisement is ready_______________________________________________ 1");
            myButton.interactable = true;
        }
        else
        {
            myButton.interactable = false;
            Debug.Log("Advertisement is not ready_______________________________________________ 1");
        }
        }

    public void DisplayInterstitialAD()
    {
        Advertisement.Show();
    }
    public void DisplayVideoAD()
    {
        //  Advertisement.Show(myPlacementId);
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady(myPlacementId))
        {
           
            Debug.Log("Advertisement is ready displaying_______________________________________________ 2");
            Advertisement.Show(myPlacementId);
        }
        else
        {
          Debug.Log("Rewarded video is not ready at the moment! Please try again later!");
        }
    }

   
    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            // Reward the user for watching the ad to completion.
       //     Debug.Log("You get Reward");
            AudioEngineR34.R34StockPurchased = 1;
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
            Debug.Log("You don't get Reward");
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.Log("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
        if (placementId == myPlacementId)
        {
            //        myButton.interactable = true;
            Debug.Log("OnUnity Ads Ready______________________________________ 0");
            myButton.interactable = true;
            // Optional actions to take when the placement becomes ready(For example, enable the rewarded ads button)
        }
       else  Debug.Log("OnUnity Ads Not Ready______________________________________ 0");
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }


}
