﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScriptCrackle6000Mustang : MonoBehaviour
{
    //    float dtcrackle6000 = 0.14f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Crackle6000()
    {
        //      StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayScheduled(AudioSettings.dspTime + AudioEngineMustang.dtcrackle);
        //       audio.Play();
        audio.Play(44100);
    }

    public void Crackle6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Crackle6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }
}
