﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuracanViews : MonoBehaviour
{

    public GameObject HuracanGarageButton;
    public GameObject HuracanRoadButton;
    public GameObject HuracanTunnelButton;
    int iHuracanLocation = 0;
    public int iHuracanview = 0;   // Appear/Disapperar Counter

    public GameObject HuracanDriveOptions;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Locations()  // 
    {



        if (iHuracanLocation == 0)
        {
            HuracanGarageButton.SetActive(true);
            HuracanRoadButton.SetActive(true);
            HuracanTunnelButton.SetActive(true);
            HuracanDriveOptions.SetActive(false);
            //          HuracanViewOptions.SetActive(false);
            iHuracanLocation = 1;
        }
        else if (iHuracanLocation == 1)
        {
            HuracanGarageButton.SetActive(false);
            HuracanRoadButton.SetActive(false);
            HuracanTunnelButton.SetActive(false);
            HuracanDriveOptions.SetActive(true);
            //        HuracanViewOptions.SetActive(true);
            iHuracanLocation = 0;
        }
    }
    public void LocationsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        HuracanGarageButton.SetActive(false);
        HuracanRoadButton.SetActive(false);
        HuracanTunnelButton.SetActive(false);

        iHuracanLocation = 0;

    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iHuracanview == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            HuracanGarageButton.SetActive(true);
            HuracanRoadButton.SetActive(true);


            iHuracanview = 1;
        }
        else if (iHuracanview == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            HuracanGarageButton.SetActive(false);
            HuracanRoadButton.SetActive(false);
            iHuracanview = 0;
        }

    }
}

