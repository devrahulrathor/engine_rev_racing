﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizeHelpButton : MonoBehaviour
{
    public GameObject CustomizeHelpMenu;
    int itoggle = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CustomizeHelpActivate()
    {
        if (itoggle == 0) {
            CustomizeHelpMenu.SetActive(true);
            itoggle = 1;

        }
        else if (itoggle == 1)
        {
            CustomizeHelpMenu.SetActive(false);
            itoggle = 0;
        }
    }
}
