using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP14000Akra1ZX10 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX10.itrate == 5007)
        {
            //        SoundEngineZX10.rpm1 = 14000;
            //           Debug.Log(SoundEngineZX10.rpm1);
        }
    }


    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip14000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (SoundEngineZX10.rpm1 >= 8000 && SoundEngineZX10.rpm1 < 14000)
        {
            audio.Play();
        }
        else if (SoundEngineZX10.rpm1 >= 7000 && SoundEngineZX10.rpm1 < 8000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.030F);
        }
        else if (SoundEngineZX10.rpm1 >= 6000 && SoundEngineZX10.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.042F);
        }
        else if (SoundEngineZX10.rpm1 >= 5000 && SoundEngineZX10.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.06F);
        }
        else if (SoundEngineZX10.rpm1 >= 4000 && SoundEngineZX10.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.087F);
        }
        else if (SoundEngineZX10.rpm1 >= 3000 && SoundEngineZX10.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.131F);
        }


        else if (SoundEngineZX10.rpm1 >= 2000 && SoundEngineZX10.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.173F);
        }
        else if (SoundEngineZX10.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.228F);
        audio.volume = 1;
        //    audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip14000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip14000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip14000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //       audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }
}