using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000ZX25Drag : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (SoundEngineZX25Drag.itrate == 5006)
        {
            SoundEngineZX25Drag.rpm1 = 7000;
            //           Debug.Log(SoundEngineZX25Drag.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineZX25Drag.rpm1 >= 6000 && SoundEngineZX25Drag.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (SoundEngineZX25Drag.rpm1 >= 5000 && SoundEngineZX25Drag.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.153F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 4000 && SoundEngineZX25Drag.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.2F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 3000 && SoundEngineZX25Drag.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.244F);
        }
        else if (SoundEngineZX25Drag.rpm1 >= 2000 && SoundEngineZX25Drag.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.288F);
        }


        else if (SoundEngineZX25Drag.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.404F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
    }
}

