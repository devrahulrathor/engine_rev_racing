using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Vespa2sBackButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void BackButton()
    {
        if (AudioEngineVespa2s.itrate != -1) AudioEngineVespa2s.istartfromtouch = 3;
        //  AudioEngineR35.R35ExhaustMode = -1; 
        SceneManager.LoadScene(0); // Car Options1 Main Screen
    }
}