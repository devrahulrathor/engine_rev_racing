using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPicker : MonoBehaviour
{
   
        public Color color;
        public Material mat;

        public void OnPickColor()
        {
            mat.color = new Color(color.r, color.g, color.b, color.a);
       //     ColorPickerObject.Instance.currentColor = color;
        //    AudioController.Instance.colorChoose.Play();
        }
    }

