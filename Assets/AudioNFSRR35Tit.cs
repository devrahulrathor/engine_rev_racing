using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNFSRR35Tit : MonoBehaviour
{
    //  int[] posrpmarrayfsr = { 7500, 6500, 5500, 4400, 4000, 3500, 3300, 3200, 2800, 2500, 2300, 2200, 2200, 2100, 2100, 2000, 2000, 2000, 2000 };
    int[] posrpmarrayfsr = { 7000, 620, 5900, 5500, 5000, 4600, 4200, 3800, 3400, 3000, 2700, 2500, 2300, 2100, 1900, 1700, 2000, 2000, 2000, 2000, 2000, 2000, 1900, 1800, 1800, 1800, 1800, 1800, 1800, 1600, 1400, 1200, 1000, 1100, 1100, 1100, 1100, 1100 };

    //   double[] rpmposarrayfsr = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

    //    double[] rpmposarrayfsr = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    double[] rpmposarrayfsr = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5 };





    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR35.itrate == 4999)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
        LowPassFilter();
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineR35.rpm1 = posrpmarrayfsr[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineR35.rpm1 <= (AudioEngineR35.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }

    int irpmpos = 0;
    double posinsec;
    public void FSR()
    {



        AudioSource audio = GetComponent<AudioSource>();
        irpmpos = (int)(141 - (AudioEngineR35.rpm1 / 50));

        //      Debug.Log(irpmpos);
        //    if (rpm >= 5000) irpmpos = 70;
        //          textBox3.Text = irpmpos.ToString();     
        posinsec = rpmposarrayfsr[irpmpos];
        //          audio.volume = 0;
        audio.time = (float)posinsec;
        //      audio.time = 0.1f;
        audio.Play();
        audio.Play(44100);
        //     AudioEngineR35.itrate = 4999;

        //     RPMmodetext.text = "Increasing0";
    }

    public void FSRStop()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.Stop();
        audio.volume = 0;
        NFSRvolume = 0;
    }


    double NFSRvolume = 1.0;
    public void NFSRFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        NFSRvolume = NFSRvolume - 0.1;
        if (NFSRvolume > 0) audio.volume = (float)NFSRvolume;
        else
        {

            AudioEngineR35.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 0;
            NFSRvolume = 0;
        }
    }

    public void NFSRFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        NFSRvolume = NFSRvolume + 0.1;
        if (NFSRvolume < 1) audio.volume = (float)NFSRvolume;
        else
        {
            audio.volume = 1;
            AudioEngineR35.ifadeinnfsr = 0;

        }

    }

    public void NFSRReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void NFSRReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}
