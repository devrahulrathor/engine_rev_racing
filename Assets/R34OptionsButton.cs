﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34OptionsButton : MonoBehaviour
{
    public GameObject R34LocationsButton;
    public GameObject R34VehicleButton;
    public GameObject R34CustomizeButton;
    public GameObject R34PopsCracklesButton;

    public GameObject R34CameraViewButton;



    public int iR34options = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AllOptionButtonsActive()  // 
    {
        if (iR34options == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            R34LocationsButton.SetActive(true);
            R34Views.iMsutangthisLocationbutton = 0;
//            R34VehicleButton.SetActive(true);
  //          R34VehicleOptions.iMsutangthisVehiclebutton = 0;
            R34CustomizeButton.SetActive(true);
            R34PopsCracklesButton.SetActive(true);
            R34PosCracklesMenu.iR34thisPopsCracklesbutton = 0;
            R34Customizeoptions.iR34thisCustomizebutton = 0;
            //      ZX10ROptionButtonBackground.SetActive(true);
            //      R34CameraViewButton.SetActive(true);
            iR34options = 1;
            Debug.Log("R34Option 1");

        }
        else if (iR34options == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            R34LocationsButton.SetActive(false);
            R34Views.iMsutangthisLocationbutton = 1;
  //          R34VehicleButton.SetActive(false);
   //         R34VehicleOptions.iR34thisVehiclebutton = 1;
            R34CustomizeButton.SetActive(false);
            R34PopsCracklesButton.SetActive(false);
            R34PosCracklesMenu.iR34thisPopsCracklesbutton = 1;
            R34Customizeoptions.iR34thisCustomizebutton = 1;

            //       ZX10ROptionButtonBackground.SetActive(false);
            //       R34CameraViewButton.SetActive(false);
            Debug.Log("R34Option 0");
            iR34options = 0;

        }

    }


}



