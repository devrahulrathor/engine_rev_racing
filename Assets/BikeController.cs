﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BikeController : MonoBehaviour
{
    public GameObject BikeZX10;
    private Rigidbody cube;
    private float forwardspeed = 10;
    public Slider accelpedal;
    public float maxSteeringAngle = 20f; // maximum steer angle the wheel can have
    float steering;
    float maxleanangle = 40.0f;
    float angletilt = 0;  //Tilt Angle while steering
    float anglelean = 0;   // Lean Angle while cornering
    int rightarrowengaged = 0, leftarrowengaged = 0;
    private int ivehiclecrashed=0;  // Detect Vehicle Crash mode, 1 when crashed

    // Start is called before the first frame update
    void Start()
    {
        cube = this.GetComponent<Rigidbody>();
    }

    //Collosion Detection
    void OnCollisionEnter(Collision collision)
    {
        //Ouput the Collision to the console
        Debug.Log("Collision : " + collision.gameObject.name);
        if (collision.gameObject.name != "Line001" && collision.gameObject.name != "Line001_2" && collision.gameObject.name != "Road_Track_A_01" && collision.gameObject.name != "Track_C_002"  && collision.gameObject.name != "Track_B_Mesh" && Mathf.Abs(cube.velocity.x) > 200)
        {
            ivehiclecrashed = 1;
            BikeZX10.SetActive(false);
         
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //      detectCollision();   // Avoid Bouncing off and tipping over on collision
        //      steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        //     Debug.Log("Steering=");
        //     Debug.Log(steering);
        //           cube.velocity = accelpedal.value *  (forwardspeed* Vector3.left + (steering/100) * Vector3.forward);  // Forward


   //        Debug.Log("Cube Velocity=");
   //            Debug.Log(cube.velocity.x);

        //Detect when there is a collision starting

        if (ivehiclecrashed == 0  && AudioEngineDrag.gear > 0)
        {
           //        cube.velocity = accelpedal.value * (transform.right * -forwardspeed + transform.forward * (steering / 100));
            cube.velocity= transform.right*AudioEngineDrag.bikespeed*-5;
    //               Debug.Log("Cube Velocity=");
    //               Debug.Log(cube.velocity);
            // BikeTilting while steering
            //Right
             if(Input.GetAxis("Horizontal") < 1 && Input.GetAxis("Horizontal") > 0)
       //     if (Input.GetKey(KeyCode.RightArrow))
            {

                //   steering = maxSteeringAngle * 0.1f;
                //           transform.Rotate((-cube.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);

          //      Debug.Log("Right********************************Angle Lean=");
          //      Debug.Log(anglelean);
                Debug.Log("maxleanangle=");
                Debug.Log(-maxleanangle);
                if (anglelean > -maxleanangle)  //
                {
                   
                    anglelean = anglelean + (-30 * Time.deltaTime);
                    BikeZX10.transform.Rotate(0, 0, -30 * Time.deltaTime);   // Leaning Bike
                    Debug.Log("Right Angle Lean=");
                    Debug.Log(anglelean);
                    //     steering = maxSteeringAngle * 30 * Time.deltaTime;
                    steering = (maxSteeringAngle * 30 * Time.deltaTime) * (1 - Mathf.Abs(cube.velocity.x) / 1500);
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent Cube
                }
                angletilt = angletilt + 1f * steering * Time.deltaTime;



                //      rightarrowengaged = 1;
                //       Debug.Log("Negativ eAngle Lean=");
                //       Debug.Log(anglelean);
            }

            //Left
            else if (Input.GetAxis("Horizontal") > -1 && Input.GetAxis("Horizontal") < 0)
       //     else if (Input.GetKey(KeyCode.LeftArrow))
            {
                //   steering = maxSteeringAngle * -0.1f;
                //          transform.Rotate((-cube.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);


                if (anglelean < maxleanangle)  //
                {
             //       Debug.Log("Left********************************Angle Lean=");
             //       Debug.Log(anglelean);
                    anglelean = anglelean + (30 * Time.deltaTime);
                    BikeZX10.transform.Rotate(0, 0, 30 * Time.deltaTime);   // Leaning Bike
                    steering = -(maxSteeringAngle * 30 * Time.deltaTime) * (1 - Mathf.Abs(cube.velocity.x) / 1500);
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent Cube
                }
                angletilt = angletilt - 1f * steering * Time.deltaTime;




                //    leftarrowengaged = 1;
                //      Debug.Log("Positive Angle Lean=");
                //      Debug.Log(anglelean);
            }


            else
            {
   //             Debug.Log("Entered recover steering left");
                if (anglelean >= 5)
                {
     //               Debug.Log("Entered steering left");
                    steering = -maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent Cube
                    BikeZX10.transform.Rotate(0, 0, -30 * Time.deltaTime);   // Leaning Bike
                    anglelean = anglelean - (30 * Time.deltaTime);
                }
                if (anglelean <= -5)
                {
                    steering = maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent Cube
                    BikeZX10.transform.Rotate(0, 0, 30 * Time.deltaTime);   // Leaning Bike
                    anglelean = anglelean + (30 * Time.deltaTime);
                }
            }

            if (angletilt > 0 && rightarrowengaged != 1)
            {
                //        transform.Rotate(-0.5f * steering * Time.deltaTime, 0, 0);
            }
            if (angletilt < 0 && rightarrowengaged != 1)
            {
                //      transform.Rotate(0.5f * steering * Time.deltaTime, 0, 0);
            }
            //    cube.AddForce(accelpedal.value * forwardspeed * (Vector3.forward + Vector3.back / 2));

            //     cube.velocity = accelpedal.value * forwardspeed * (Vector3.left);  // Forward

            //     cube.velocity = accelpedal.value * forwardspeed * (Vector3.left + steering* Vector3.forward);  // Forward
        } // Crashed Brace
    }
  

}