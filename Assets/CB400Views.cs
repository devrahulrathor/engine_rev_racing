﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CB400Views : MonoBehaviour
{
    public GameObject CB400GarageButton;
    public GameObject CB400RoadButton;
    public GameObject CB400TunnelButton;
    public GameObject CB400ViewBackImage;
    public GameObject CB400ViewForwardArrow;
    public GameObject CB400ViewBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject CB400ExhaustOptions;
    public GameObject CB400ViewOptions;
    public GameObject CB400PopsCracklesButton;


    public int iCB400view = 0;   // Appear/Disapperar Counter
    public int iZX10SwipeCounter = 1;   // Decide the View Option Active: 1 Garage Active, 2 Road Active
    int iCB400Location = 0;

    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (iCB400Location == 100)  // Deactivated
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iZX10SwipeCounter == 2)
                {
                    CB400ViewForwardArrow.SetActive(true);
                    CB400ViewBackArrow.SetActive(true);
                }
                else if (iZX10SwipeCounter == 3)
                {
                    CB400ViewForwardArrow.SetActive(false);
                    CB400ViewBackArrow.SetActive(true);
                }
                else if (iZX10SwipeCounter == 1)
                {
                    CB400ViewForwardArrow.SetActive(true);
                    CB400ViewBackArrow.SetActive(false);
                }

                if (pos.y > 600 && pos.y < 800 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iZX10SwipeCounter == 1 && iswipeenable == 1)
                        {
                            CB400RoadButton.SetActive(true);
                            CB400GarageButton.SetActive(false);
                            CB400TunnelButton.SetActive(false);
                            iZX10SwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iZX10SwipeCounter == 2 && iswipeenable == 1)
                        {
                            CB400TunnelButton.SetActive(true);
                            CB400RoadButton.SetActive(false);
                            iZX10SwipeCounter++;

                            //                 iswipeenable = 0;
                            //                 StartCoroutine("EnableSwipe");
                        }

                    }
                    if (touchZero.deltaPosition.x > 10)
                    {
                        if (iZX10SwipeCounter == 3 && iswipeenable == 1)
                        {
                            //                  //        CB400TBR1Exhaust.SetActive(false);
                            CB400RoadButton.SetActive(true);
                            CB400TunnelButton.SetActive(false);
                            iZX10SwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iZX10SwipeCounter == 2 && iswipeenable == 1)
                        {
                            CB400GarageButton.SetActive(true);
                            CB400RoadButton.SetActive(false);
                            //              CB400TunnelButton.SetActive(false);
                            iZX10SwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }

    public void Locations()  // 
    {



        if (iCB400Location == 0)
        {
            CB400GarageButton.SetActive(true);
            CB400RoadButton.SetActive(true);
            CB400TunnelButton.SetActive(true);
            CB400ExhaustOptions.SetActive(false);
   //         CB400ViewOptions.SetActive(false);
            CB400PopsCracklesButton.SetActive(false);
            iCB400Location = 1;
        }
        else if (iCB400Location == 1)
        {
            CB400GarageButton.SetActive(false);
            CB400RoadButton.SetActive(false);
            CB400TunnelButton.SetActive(false);
            CB400ExhaustOptions.SetActive(true);
    //        CB400ViewOptions.SetActive(true);
            CB400PopsCracklesButton.SetActive(true);
            iCB400Location = 0;
        }
    }

    public void LocationsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        CB400GarageButton.SetActive(false);
        CB400RoadButton.SetActive(false);
        CB400TunnelButton.SetActive(false);

        iCB400Location = 0;

    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iCB400view == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            //          audio.Play();
            CB400GarageButton.SetActive(true);
            CB400RoadButton.SetActive(true);
            //        CB400ViewBackImage.SetActive(true);


            iCB400view = 1;
        }
        else if (iCB400view == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            //        audio.Play();
            CB400GarageButton.SetActive(true);
            CB400RoadButton.SetActive(true);
            //       CB400ViewBackImage.SetActive(false);
            iCB400view = 0;
        }

    }


    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
}
