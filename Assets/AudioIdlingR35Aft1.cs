﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingR35Aft1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR35.irpm == 0)
        {
            AudioEngineR35.rpm1 = AudioEngineR35.rpmidling;
        }
        LowPassFilter();
    }

    public void Idling()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = AudioEngineR35.idlingvolAft1;

        Debug.Log("4 IdlingVol=");
        Debug.Log(AudioEngineR35.idlingvolAft1);
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       idlingvolRoushume = 0;


    }

    double idlingvolRoushume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        idlingvolRoushume = idlingvolRoushume - 0.1;
        if (idlingvolRoushume > 0) audio.volume = (float)idlingvolRoushume;
        else
        {

            AudioEngineR35.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        idlingvolRoushume = idlingvolRoushume + 0.1;
        if (idlingvolRoushume < 1) audio.volume = (float)idlingvolRoushume;
        else
        {
            audio.volume = 1;
            AudioEngineR35.ifadeinidling = 0;

            //            audio.Stop();
        }

    }


    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }

    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }


}


