using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterZX10 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (SoundEngineZX10.itrate == 10000)
        {
            //      RPMCal();
            // SoundEngineZX10.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        //      Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (SoundEngineZX10.ivibrating == 1)
        {
            //          SoundEngineZX10.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (SoundEngineZX10.ivibrator == 1)
            //       {
            if (SoundEngineZX10.rpm1 == 15000) SoundEngineZX10.rpm1 = 14000;
            else SoundEngineZX10.rpm1 = 15000;
            //          else if (SoundEngineZX10.rpm1 == 14000) SoundEngineZX10.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //      audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }
}

