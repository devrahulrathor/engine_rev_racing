﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja250CameraViewChange : MonoBehaviour
{
    public static float ConsoleCameraAngle = 0.0f, ConsoleCameraAngleStep = 0.9f;
    public static int iNinja250CameraSwitch = 0; // To Switch Camera from Default to Console 
    int iNinja250CameraSwitch2 = 0; // To Switch Camera from Default to Console
    public GameObject Ninja250MainCameraView, Ninja250ConsoleCameraView;  // Camera Objects
    public GameObject RPMConsole, ConsoleBackground, Image360;   //Partillly take off Console Buttons
    public GameObject DefaultView, ConsoleView;  // GUI Images

    // Other Game Options Appear/Disappear
    public GameObject Ninja250ExhaustOptions;
    public GameObject Ninja250LocationOptions;
    public GameObject ExhaustButton;  // Deactivate Exhaust Button when Console View
    public GameObject Ninja250PopsCracklesButton;  // Deactivate Pops Crackles Button

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //     ZX10MainCameraView.transform.position = Vector3.Lerp(ZX10MainCameraView.transform.position, ZX10RConsoleCameraView.transform.position, 0.5f * Time.deltaTime);
        //      do {
        //         if (ZX10MainCameraView.transform.position.x >= -10 && ZX10MainCameraView.transform.position.y >= -10)
        //         {
        //            ZX10MainCameraView.transform.Rotate(0, -ConsoleCameraAngleStep, 0);
        //             ConsoleCameraAngle = ConsoleCameraAngle + ConsoleCameraAngleStep;
        //         }
        //     } while (ConsoleCameraAngle <= 90.0f);
    }





    public void Ninja250ViewChange()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        if (iNinja250CameraSwitch == 0)
        {
            Ninja250MainCameraView.SetActive(false);
            Ninja250ConsoleCameraView.SetActive(true);
            ConsoleView.SetActive(true);
            DefaultView.SetActive(false);
            RPMConsole.SetActive(false);
            ConsoleBackground.SetActive(false);
            Image360.SetActive(false);


            iNinja250CameraSwitch = 1;
        }
        else if (iNinja250CameraSwitch == 1)
        {
            Ninja250MainCameraView.SetActive(true);
            Ninja250ConsoleCameraView.SetActive(false);
            DefaultView.SetActive(true);
            ConsoleView.SetActive(false);
            RPMConsole.SetActive(true);
            ConsoleBackground.SetActive(true);
            Image360.SetActive(true);

            iNinja250CameraSwitch = 0;
        }
    }
    //      do
    //      {
    //          ZX10MainCameraView.transform.position = Vector3.Lerp(ZX10MainCameraView.transform.position, ZX10RConsoleCameraView.transform.position, 0.004f * Time.deltaTime);
    //         ZX10MainCameraView.transform.Rotate(0, -ConsoleCameraAngleStep, 0);
    //        ConsoleCameraAngle = ConsoleCameraAngle + ConsoleCameraAngleStep;
    //    } while (ConsoleCameraAngle <= 9000.0f);



    public void Ninja250ViewToggle()
    {
        if (iNinja250CameraSwitch2 == 0)
        {
            DefaultView.SetActive(true);
            ConsoleView.SetActive(true);

            Ninja250ExhaustOptions.SetActive(false);
            Ninja250LocationOptions.SetActive(false);
            Ninja250PopsCracklesButton.SetActive(false);
            iNinja250CameraSwitch2 = 1;
        }
        else if (iNinja250CameraSwitch2 == 1)
        {
            DefaultView.SetActive(false);
            ConsoleView.SetActive(false);

            Ninja250ExhaustOptions.SetActive(true);
            Ninja250LocationOptions.SetActive(true);
            Ninja250PopsCracklesButton.SetActive(true);
            iNinja250CameraSwitch2 = 0;
        }


    }

    public void CameraViewsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {
        DefaultView.SetActive(false);
        ConsoleView.SetActive(false);
        iNinja250CameraSwitch2 = 0;
    }

    public void Ninja250DefaultView()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);


        Ninja250MainCameraView.SetActive(true);
        Ninja250ConsoleCameraView.SetActive(false);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
        RPMConsole.SetActive(true);
        ConsoleBackground.SetActive(true);

        iNinja250CameraSwitch = 0;

    }

    public void Ninja250ConsoleView()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        iNinja250CameraSwitch = 1;
        Ninja250MainCameraView.SetActive(false);
        Ninja250ConsoleCameraView.SetActive(true);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
        RPMConsole.SetActive(false);
        ConsoleBackground.SetActive(false);



    }

}
