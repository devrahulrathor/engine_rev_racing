using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNFSR7500ZX10 : MonoBehaviour
{
    //      int[] posrpmarrayfsr7500 = {  7000,6500,6000,5500,5000,4500,4000,3800,3500,3200,2700,2100,1200, 1200,1200, 1100, 1100, 1100, 1100, 1100, 1100 };
    int[] posrpmarrayfsr7500 = { 7500, 7000, 6500, 6000, 5000, 5000, 4200, 3800, 3300, 3000, 3000, 2500, 2200, 2000, 1900, 1700, 1600, 1500, 1300, 1250, 1200, 1150, 1100 };
    //   double[] rpmposarrayfsr7500 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX10.itrate == 4999)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        SoundEngineZX10.rpm1 = posrpmarrayfsr7500[irpmarrayindex];



    }


    int irpmpos = 0;
    double posinsec;
    public void FSR7500()
    {



        AudioSource audio = GetComponent<AudioSource>();
        //      irpmpos = (int)(151 - (SoundEngineZX10.rpm1 / 50));

        //      Debug.Log(irpmpos);
        //    if (rpm >= 5000) irpmpos = 70;
        //          textBox3.Text = irpmpos.ToString();     
        //      posinsec = rpmposarrayfsr7500[irpmpos];
        //     audio.volume = 1;
        //      audio.time = (float)posinsec;
        audio.time = 0.5f;
        audio.Play();
        audio.Play(44100);
        //     SoundEngineZX10.itrate = 4999;

        //     RPMmodetext.text = "Increasing0";
    }

    public void FSR7500Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.Stop();
    }


}