﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CB400OptionsButton : MonoBehaviour
{
    public GameObject CB400ViewsButton;
    public GameObject CB400ExhaustButton;
    public GameObject CB400OptionButtonBackground;
    public GameObject CB400CameraViewButton;
    public GameObject CB400PopsCracklesButton;



    public int iCB400options = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AllOptionButtonsActive()  // 
    {
        if (iCB400options == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            CB400ViewsButton.SetActive(true);
            CB400ExhaustButton.SetActive(true);
            //      CB400OptionButtonBackground.SetActive(true);
     //       CB400CameraViewButton.SetActive(true);
            CB400PopsCracklesButton.SetActive(true);
            iCB400options = 1;

        }
        else if (iCB400options == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            CB400ViewsButton.SetActive(false);
            CB400ExhaustButton.SetActive(false);
            //       CB400OptionButtonBackground.SetActive(false);
     //       CB400CameraViewButton.SetActive(false);
            CB400PopsCracklesButton.SetActive(false);
            iCB400options = 0;

        }

    }


}

