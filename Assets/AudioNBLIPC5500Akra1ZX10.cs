using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPC5500Akra1ZX10 : MonoBehaviour
{
    int[] posrpmarrayfsr5000 = { 6200, 6200, 6000, 5000, 4000, 2500, 2000, 1200, 1200, 1200, 1200, 1200, 1200, 1250, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1150, 1150, 1150 };

    double[] rpmposarrayfsr5000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //        Debug.Log("Blip5500 C is Playing***********************");
        //       Debug.Log(SoundEngineZX10.itrate);
        if (SoundEngineZX10.itrate == 6004)
        {
            //           Debug.Log("Blip5500 C is Playing***********************");
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        SoundEngineZX10.rpm1 = posrpmarrayfsr5000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (SoundEngineZX10.rpm1 <= (SoundEngineZX10.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }

    public void BLIPC5500()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1;
        audio.Play();
        audio.Play(44100);
    }

    public void BLIPC5500Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }


    double Blip5500volume = 1.0;
    public void BLIPCR5000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip5500volume = Blip5500volume - 0.1;
        if (Blip5500volume > 0) audio.volume = (float)Blip5500volume;
        else
        {

            SoundEngineZX10.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
            Blip5500volume = 1;
        }
    }
    public void Blipc5000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blipc5000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //     audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }

}

