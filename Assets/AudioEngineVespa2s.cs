using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;

public class AudioEngineVespa2s : MonoBehaviour
{
    double startTick;

    public int ikeyin = 0;    // Ignition Key in Counter
    public static int ikeyinfromtouch = 0;
    public static int istart = -100;    // Start Counter
    public static int istartfromtouch = 0;
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs
    public int iRPMIdleSettle = 0; // Counter for Console RPM Warm up and settle-0-3000-Idling RPM
    public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 

    public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = 0;
    public static int iblipcanstart = 1;  // Counter for Blip Start
    public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 1100;

    int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 1500;

    //   public float rpmdisplayafterstart =1.4f;  // Time in seconds after which Idling RPM start post Startup
    public static int rpmblipconst;  // TO Stop Blip at different RPMs
    public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public float sliderval;
    public float slidervalold;

    // Fade in out counters
    public static int ifadeoutnfsr = 0, ifadeinnfsr = 0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)

    public static int ifadeinidling = 0, ifadeoutidling = 0;
    public static int ifadeoutsli = 0, ifadeoutfsi1 = 0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters
    public GameObject GarageView;   // Appearance/Disappearance of Garage
    public static float ReverbXoneMix = 1.03f;   // Reverberation Value for Garage
    public GameObject RoadView;   // Appearance/Disappearance of Road
    public GameObject TunnelView;   // Appearance/Disappearance of Road

    // Yoshi Counters
    public int exhaustDropDown = 0; // 0 Dropdown up, 1 Dropdown down
    public int iexhaust = 0;  // 0 for Stock, 1 for Yoshi,  
                              //    public Button Exhaustbutton;  // Exhaust Option Button

    public GameObject CantstartwithoutExhaustPurNote; // When trying to change exhaust while Blipping,(Can only be changed during idling)
    public GameObject Exhaustbutton;
    public GameObject ExhaustStockbutton;
    public GameObject StockExhaustTick;
    public GameObject ExhaustYoshi1button;
    public GameObject Yoshi1ExhaustTick;

    public GameObject ExhaustBMSRevobutton;
    public GameObject BMSRevoExhaustTick;
    //   public GameObject Akra1CarbonExhaustTick;
    public GameObject ExhaustStockPurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased
                                               //  public GameObject ExhaustBMSRevoPurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased

    public BinSaveLoad BinarySaveLoad;     // Gam Obj to Save Load Consumables in Binary

    //  public GameObject ExhaustTBR1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased
    public GameObject StockMuffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject StockMufflerShield;   // Appearance/Disappearance of Stock Muffler Shield
    public GameObject Yoshi1Muffler;   // Appearance/Disappearance of Stock Muffler

    public GameObject BMSRevoMuffler;   // Appearance/Disappearance of BMSRevo Muffler

    //   public GameObject AkraBlackMuffler;   // Appearance/Disappearance of AkrapovicBlack
    //   public GameObject AkraCarbonMuffler;   // Appearance/Disappearance of AkrapovicCarbon
    public GameObject StockMufflerSoundSource;  // Activate/Deactivate Stock Sound (Interference stop issue at Blipc10000 
    public GameObject Yoshi1MufflerSoundSource;  // Activate/Deactivate Akra1 Sound (Interference stop issue at Blipc10000 
    public GameObject BMSRevoMufflerSoundSource;  // Activate/Deactivate BMSRevo Sound (Interference stop issue at Blipc10000 


    // GamrObject for RPMConsole1 to Enlarge
    public GameObject RPMConsole1;
    public int iRPMConsole = 0;  // 1:Enlarge, 0:Reduce

    // Backfire Section
    public ExhaustPop1Jawa2s AVExhaustPop1;
    public ExhaustPop2Jawa2s AVExhaustPop2;
    public int iPopCrackle2 = 0;  // 0-off, 1-On
    public static float dtpop = 0.75f;  // Delay on Backfire 
                                        //   public GameObject PopsCracklesHeader;  // Main Menu for Pops Crackles
    public GameObject PopsCrackles1;       // Pos Crackles1 Button
    public GameObject PopsCrackles1Tick;       // Pos Crackles1 Button Tick

    // Paid Options
    public AudioScriptNinja400Crackle10000 ACrackle10000;  // Crackle 10000
    public AudioScriptNinja400Crackle8000 ACrackle8000;  // Crackle 10000

    public AudioStartupJawa2s AStartup;
    public AudioStartingVespa2s AStarting;
    public AudioShuttingVespa2s AShutting;

    public AudioIdlingVespa2s AIdling;
    public AudioNBLIPNinja250 ANBLIP;
    public AudioNBLIP2000Ninja250 ANBLIP2000;
    public AudioNBLIP3000Ninja250 ANBLIP3000;
    public AudioNBLIP4000Ninja250 ANBLIP4000;
    public AudioNBLIP5000Ninja250 ANBLIP5000;
    public AudioNBLIP6000Ninja250 ANBLIP6000;
    public AudioNBLIP7000Ninja250 ANBLIP7000;
    public AudioNBLIP10000Ninja250 ANBLIP10000;
    public AudioNBLIP14000Ninja250 ANBLIP14000;

    public AudioNBLIPC3000Ninja250 ANBLIPC3000;
    public AudioNBLIPC4000Ninja250 ANBLIPC4000;
    public AudioNBLIPC5500Ninja250 ANBLIPC5500;
    public AudioNBLIPC6000Ninja250 ANBLIPC6000;
    public AudioNBLIPC7500Ninja250 ANBLIPC7500;
    public AudioNBLIPC10000Ninja250 ANBLIPC10000;
    public AudioNBLIPC14000Ninja250 ANBLIPC14000;
    public AudioNRevlimiterJawa2s ANRevLimiter;
    public AudioNFSR15000Ninja250 ANFSR15000;
    public AudioNFSR7500Ninja250 ANFSR7500;
    public AudioNFSRVespa2s ANFSR;
    public AudioNSLIVespa2s ANSLI;
    public AudioNFSI1Vespa2s ANFSI1;
    public AudioNFSI2Ninja250 ANFSI2;

    //  Akra1

    public AudioStartingNinja250Yoshi1 AStartingYoshi1;
    public AudioIdlingNinja250Yoshi1 AIdlingYoshi1;
    public AudioNSLINinja250Yoshi1 ANSLIYoshi1;
    public AudioNFSI1Ninja250Yoshi1 ANFSI1Yoshi1;
    public AudioNFSRNinja250Yoshi1 ANFSRYoshi1;
    public AudioNRevLimiterNinja250Yoshi1 ANRevLimiterYoshi1;
    //    public AudioNBLIPNinja250Yoshi1 ANBLIPYoshi1;
    //    public AudioNBLIP2000Ninja250Yoshi1 ANBLIP2000Yoshi1;
    //    public AudioNBLIP3000Ninja250Yoshi1 ANBLIP3000Yoshi1;
    //    public AudioNBLIP4000Ninja250Yoshi1 ANBLIP4000Yoshi1;
    //    public AudioNBLIP5000Ninja250Yoshi1 ANBLIP5000Yoshi1;
    //    public AudioNBLIP6000Ninja250Yoshi1 ANBLIP6000Yoshi1;
    //    public AudioNBLIP7000Ninja250Yoshi1 ANBLIP7000Yoshi1;
    //    public AudioNBLIP10000Ninja250Yoshi1 ANBLIP10000Yoshi1;
    //    public AudioNBLIP14000Ninja250Yoshi1 ANBLIP14000Yoshi1;
    //   public AudioNRevLimiterNinja250Yoshi1 ANRevLimiterYoshi1;
    //    public AudioNBLIPC3000Yoshi1 ANBLIPC3000Yoshi1;
    //    public AudioNBLIPC4000Yoshi1 ANBLIPC4000Yoshi1;
    //    public AudioNBLIPC5500Yoshi1 ANBLIPC5500Yoshi1;
    //    public AudioNBLIPC6000Yoshi1 ANBLIPC6000Yoshi1;
    //    public AudioNBLIPC7500Yoshi1 ANBLIPC7500Yoshi1;
    //    public AudioNBLIPC10000Yoshi1 ANBLIPC10000Yoshi1;
    //    public AudioNBLIPC14000Yoshi1 ANBLIPC14000Yoshi1;
    //    
    //    public AudioNFSR15000Yoshi1 ANFSR15000Yoshi1;
    //    public AudioNFSRNinja250Yoshi1 ANFSRAkra1;

    //  BMSRevo

    public AudioStartingNinja250BMSRevo AStartingBMSRevo;
    public AudioIdlingNinja250BMSRevo AIdlingBMSRevo;
    public AudioNSLINinja250BMSRevo ANSLIBMSRevo;
    public AudioNFSI1Ninja250BMSRevo ANFSI1BMSRevo;
    public AudioNFSRNinja250BMSRevo ANFSRBMSRevo;

    //Store Section
    //  public static int Ninja250ExhaustYoshi1 = 1;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs
    public static int Jawa2sExhaustStock = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval2 in Playerprefs
    public static int Jawa2sExhaustXYZ = 1;  // 1 When Purchased at Store, stored as 1 in nonconsumableval2 in Playerprefs
    int istartwithoutexhaustpurchase = 0;  // To notify user to purchase at least 1 Exhaust to start

    // Headlight Object
    public N250Headlight Hlight;
    public N250Taillight Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 

    // Background Music Efects



    int ineedle = 0;
    int[] rpmidlesettleed = { 500, 800, 1000, 1000, 1200, 1500, 1800, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2200, 2200, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2200, 2200, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000 };  // RPM Vlues for RPM settle down


    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No


    [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, RPM0, RPM200, RPM500, RPM800, RPM1000, RPM1100, RPM1500, RPM1800, RPM2000, RPM2100, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;
    public GameObject ConsoleRPMOff, ConsoleRPM0, ConsoleRPM200, ConsoleRPM500, ConsoleRPM800, ConsoleRPM1000, ConsoleRPM1100, ConsoleRPM1500, ConsoleRPM1800, ConsoleRPM2000, ConsoleRPM2200, ConsoleRPM2500, ConsoleRPM2800, ConsoleRPM3000, ConsoleRPM3200, ConsoleRPM3500, ConsoleRPM3800, ConsoleRPM4000, ConsoleRPM4200, ConsoleRPM4500, ConsoleRPM4800, ConsoleRPM5000, ConsoleRPM5200, ConsoleRPM5500, ConsoleRPM6000, ConsoleRPM6500, ConsoleRPM7000, ConsoleRPM7500, ConsoleRPM7700, ConsoleRPM8000, ConsoleRPM8200, ConsoleRPM8500, ConsoleRPM8800, ConsoleRPM9000, ConsoleRPM9200, ConsoleRPM9500, ConsoleRPM9800, ConsoleRPM10000, ConsoleRPM10200, ConsoleRPM10500, ConsoleRPM10800, ConsoleRPM11000, ConsoleRPM11200, ConsoleRPM11500, ConsoleRPM11800, ConsoleRPM12000, ConsoleRPM12200, ConsoleRPM12500, ConsoleRPM12800, ConsoleRPM13000, ConsoleRPM13200, ConsoleRPM13500, ConsoleRPM13800, ConsoleRPM14000, ConsoleRPM14200, ConsoleRPM14500, ConsoleRPM14800, ConsoleRPM15000;

    void Awake()
    {
        Application.targetFrameRate = 1000;
    }
    // Use this for initialization
    void Start()
    {
        // IAP Give the PlayerPrefs some values to send over to the next Scene
        //           PlayerPrefs.SetInt("nonconsumableval11", 0);
        //              BinarySaveLoad.SaveJawa2sStock1(2);   //Commented after Stock made free
        //  Jawa2sExhaustStock = 0;


    }

    // Update is called once per frame
    void Update()
    {
        //       slidervalold = sliderval;
        //      Debug.log(sliderval);

        //      trate = (sliderval - slidervalold);
        //        throttlediff = (sliderval * 150 - rpm1);
        // Starting Module
        //      if (Input.GetKeyDown("space") || istart == 0)
        Jawa2sExhaustStock = 1;   // After made free

        if (Jawa2sExhaustStock == 0) BinarySaveLoad.LoadJawa2sStock1();  // Binary Load Cnsumable Data
        if (Jawa2sExhaustStock == 1)
        {
            StockMufflerSoundSource.SetActive(true);
            istartwithoutexhaustpurchase = 1;  // EnableBlip exhaust tick etc 
                                               //     if (ExhaustStockPurPanel.activeInHierarchy == true)
                                               //   {

            ExhaustStockPurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                    //                   Debug.Log("Aftermarket Exhaust 1 Purchased");

            // }
        }



        // Ingnition On Module
        if (ikeyinfromtouch == 1)
        {
            ikeyinfromtouch = 2;
            //         NeutralButtonOff.enabled = true;

            // Ignition Key Rotate to On Position
            IgnitionKeyOn.enabled = true;
            IgnitionKeyOff.enabled = false;


            RedButtonOn.enabled = true;
            iconsolestartup = 1;  // Console Startup Loop Enabler
            AStartup.Startup();
            NeutralButtonOff.enabled = false;
            NeutralButtonOn.enabled = true;
            StartCoroutine("ConsoleStartup");

            // Fadeout Background Music
            //          StartCoroutine("FadeoutBackgroundMusic");


        }




        //    StartCoroutine("ExhaustSmokeEvents");  //Start Smoke Pulse


        if (((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1) && istartwithoutexhaustpurchase == 0)
        {
            if ((Jawa2sExhaustStock == 0))
            {
                CantstartwithoutExhaustPurNote.SetActive(true);
                //           MustangCustomizeOptions.SetActive(true);
                //           MustangExhaustOptions.SetActive(true);
                //         RoushButton.SetActive(true);
                //          Borla1Button.SetActive(true);
                //           MustangPaintOptions.SetActive(false);
                //       MustangColorPalette.SetActive(false);
                //       MustangCarOptions.SetActive(false);

                //       MustangLocationOptions.SetActive(false);

                //     ExhaustButton.Exhausts();
                StartCoroutine("CantStartwithoutExhhaustPurchaseDeactivate");
                istartwithoutexhaustpurchase = 1;
            }
        }

        // Start Module
        if ((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1)
        {
            if (Jawa2sExhaustStock == 1)
            {
                // Enable Console neutral Buttons
                //        NeutralButtonOff.enabled = false;
                //        NeutralButtonOn.enabled = true;

                //         istart = 1;




                StartCoroutine("Startistart1");  // Starting the Loop for Events


                istartfromtouch = 2;

                gear = 0;
                if (iexhaust == 0) AStarting.Starting();
                else if (iexhaust == 1) AStartingYoshi1.Starting();
                else if (iexhaust == 2) AStartingBMSRevo.Starting();
                // Module for startup rpm change increase and reduce 0-3000-2000
                iRPMIdleSettle = 1; // Idle-Warm Up RPM enabled
                ineedle = 0;
                StartCoroutine("RPMIdleSettle");
                // Module for startup ends

                itrate = 1;  // Enable SLI
                idlingvol = 1;
                Debug.Log("Inside Idling Jawa");
                StartCoroutine("StartIdling");

                // Exhaust Smoke to Start
                //       StartCoroutine("ExhaustSmokeEvents");  //Start Smoke Pulse
                //    StartCoroutine("StopExplode); // Stop Smoke Pulse
                //         irpm = 0;
            }
        }

        // Shuttin Module
        if ((Input.GetKeyDown("space") && istart == 1) || istartfromtouch == 3)
        {
            // Enable Console neutral Buttons
            //       NeutralButtonOff.enabled = true;
            //       NeutralButtonOn.enabled = false;
            AShutting.Shutting();
            istart = 0;
            istartfromtouch = 0;

            itrate = -1;


            // Stop all sounds
            if (iexhaust == 0)
            {
                AStarting.StartingStop();
                ANBLIP.BlipStop();
                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                ANBLIP3000.Blip3000Stop();
                ANBLIP4000.Blip4000Stop();
                ANBLIP5000.Blip5000Stop();
                ANBLIP6000.Blip6000Stop();
                ANBLIP7000.Blip7000Stop();
                ANBLIP10000.Blip10000Stop();
                ANBLIP14000.Blip14000Stop();
                AIdling.IdlingStop(); // Stop Idling
                ANFSR.FSRStop();
                ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();
                ANRevLimiter.RevLimiterStop();



            }
            else if (iexhaust == 1)
            {

                AStartingYoshi1.StartingStop();
                AIdlingYoshi1.IdlingStop(); // Stop Idling
                ANSLIYoshi1.NSLIStop();
                ANFSI1Yoshi1.NFSI1Stop();
                ANFSRYoshi1.FSRStop();
            }
            else if (iexhaust == 2)
            {

                AStartingBMSRevo.StartingStop();
                AIdlingBMSRevo.IdlingStop(); // Stop Idling
                ANSLIBMSRevo.NSLIStop();
            }

            StopCoroutine("StartIdling");
            StopCoroutine("RPMIdleSettle");
            //     StopCoroutine("Startistart1");
            ivibrating = 0; // Stop Vibrating after Rev Limter Stops

            rpm1 = 0;
            RemoveRPMConsole();
            RPM0.enabled = true;
            ConsoleRPM0.SetActive(true);
        }

        // Throttle Spring back Module

        if (istart == 1)
        {
            // Touch Release Module
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
                        //                Debug.Log("Touch Begun");
                        //             message = "Begun ";
                        break;
                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                        mSlider.value = 0.0f;

                        break;
                }
            }
            // Mouse Release Module
            if (Input.GetMouseButtonUp(0))

                //      Debug.Log("Pressed primary button.");
                mSlider.value = 0.0f;
        }


        // Throttle  Events
        if (istart == 1 && gear == 0)
        {
            trate = (sliderval - slidervalold);
            throttlediff = (sliderval * 60 - rpm1 * 1.0f);

            // SLI Module
            //               if(((trate > 0 && throttlediff > 0) && itrate == 1) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 ||itrate == 6006 || itrate == 6005  || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0))  {
            if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 10000))
            {


                Debug.Log("Inside SLI 00000000000000000000");

                StopCoroutine("StartIdling");
                StopCoroutine("FadeinIdling");
                if (iRPMIdleSettle == 1) rpm1 = 2000;
                iRPMIdleSettle = 0; // Idle-Warm Up RPM disabled
                StopCoroutine("RPMIdleSettle");
                if (iexhaust == 0)
                {
                    Debug.Log("Inside SLI");
                    AStarting.StartingStop();
                    ANSLI.NSLI();
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                else if (iexhaust == 1)
                {
                    Debug.Log("Inside SLI  ____2");
                    AStartingYoshi1.StartingStop();
                    ANSLIYoshi1.NSLI();
                    ANFSRYoshi1.FSRStop();
                }

                else if (iexhaust == 2)
                {
                    Debug.Log("Inside SLI  ____2");
                    AStartingBMSRevo.StartingStop();
                    ANSLIBMSRevo.NSLI();
                    //        ANFSRBMSRevo.FSRStop();
                }

                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");




                if (itrate == 5001)
                {
                    //           ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
                    //           StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 2;    // Counter for FSI to start
                irpm = 1;      // Counter for RPM 
            }

            // FSI1 Module
            //            if (((trate > 2 || throttlediff > 2000) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))

            //               if (((trate > 2 || throttlediff > 2000 || rpm1 > 4500) && itrate == 2) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0))
            if (((trate > 5 || throttlediff > 3000 || rpm1 > 20500) && itrate == 2 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
            // above FSI1 starts after rpm > 45000//

            {
                Debug.Log("Inside Blip(FSI1)------------------");
                if (iexhaust == 0)
                {
                    ANFSI1.NFSI1();
                    ANSLI.NSLIStop();
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }

                else if (iexhaust == 1)
                {
                    ANFSI1Yoshi1.NFSI1();
                    ANSLIYoshi1.NSLIStop();
                    ANFSRYoshi1.FSRStop();
                }
                else if (iexhaust == 2)
                {
                    //          ANFSI1BMSRevo.NFSI1();
                    ANSLIBMSRevo.NSLIStop();
                    //         ANFSRBMSRevo.FSRStop();
                }
                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");



                if (itrate == 5001)
                {
                    //        ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
                    //        StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 3;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }

            // Twin specific 3rd Throttle Mode
            if ((trate > 4000 || throttlediff > 12000) && itrate == 3 && throttlediff > 0 && rpm1 < 11000 && rpm1 > 2000 && iblipcanstart == 1 && istart == 1)
            {
                if (iexhaust != 1)  // Not for Yoshi1 Exhaust as no FSI2
                {
                    Debug.Log("Inside Blip(FSI2)------------------");
                    ANFSI2.NFSI2();
                    ANFSI1.NFSI1Stop();
                    ANSLI.NSLIStop();
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();

                    itrate = 4;
                }
            }


            // Blip Module
            //             if ((trate > 10 || throttlediff > 12000) && throttlediff > 0  && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
            if ((trate > 1000000 || throttlediff > 12000000) && throttlediff > 0 && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
            {
                //           Debug.Log("Trate=");
                //           Debug.Log(trate);

                Debug.Log("Inside Blip---");
                startTick = AudioSettings.dspTime;

                iblipcanstart = 0;
                irevlimitercanstart = 1;

                ANBLIP.Blip();
                ANBLIP2000.Blip2000();
                ANBLIP3000.Blip3000();
                ANBLIP4000.Blip4000();
                ANBLIP5000.Blip5000();
                ANBLIP6000.Blip6000();
                ANBLIP7000.Blip7000();
                ANBLIP10000.Blip10000();
                ANBLIP14000.Blip14000();
                //         ANFSR15000.NFSR15000();

                //           ANBLIPC3000.BLIPC3000();

                idlingvol = 0;   // Making Idling vol 0 so no silence upon start and idling
                AIdling.IdlingStop(); // Stop Idling
                StopCoroutine("StartIdling");
                //         ifadeoutidling = 1;
                //         fadeoutidlingtime = 0.01f;
                //         StartCoroutine("FadeoutIdling");
                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                //        ifadeoutsli = 1;
                //        ifadeoutfsi1 = 1;
                //         StartCoroutine("FadeoutNSLI");
                //         StartCoroutine("FadeoutNFSI1");
                //        ifadeoutnfsr = 1;
                //        fadeoutfsrtime = 0.005f;
                //          FadeoutNFSR();



                //           ANFSR.FSRStop();

                //         ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();

                //            NSLIAtrigger.SLIStop();

                //               Debug.Log("Blip ***************************************Blip");
                // Stop SLI
                // Stoo SLR
                itrate = 5000;  // Blip itrate
                                //         irpm = 5000;
                                //           rpmdisplay = rpm1;
            }

            if (((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 2 || itrate == 3 || itrate == 4)) && rpm1 >= 6000)
            //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
            {
                itrate = 10000;  // pre rev limit Trate
                                 //          EnableFSR15000afterRevlimiterStart();
                irevlimitercanstart = 0;
                ivibrating = 1;

                rpm1 = 6000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                if (iexhaust == 0) ANRevLimiter.RevLimiter();
                if (iexhaust == 1) ANRevLimiterYoshi1.RevLimiter();
                //            if (iexhaust == 2) ANRevLimiterBMSRevo.RevLimiter();
                ANBLIP14000.Blip14000Stop();
                ANBLIP.BlipStop();
                ANSLI.NSLIStop();
                ANFSI1.NFSI1Stop();
                // Mobile Vibration
                //            ivibrating = 1;
                //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                //          Handheld.Vibrate();
                StartCoroutine("RevLimiterVibrationOnOff");

            }
            // Deactivated Blip FSR
            //         if (((trate < 0 || throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            if (((trate < -10000 || throttlediff < -100000) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            //              if (((trate < 0 || throttlediff < 0) && itrate == 5000 && rpm1 > 2000) || (itrate == 5000 && rpm1 >= 9000))
            {
                itrate = 5001;  // Counter for BlipC
                                //     itrate = -1;
                                //         iblipcanstop = 1;
                                //     }
                                //     if(iblipcanstop == 1) {

                startTick = AudioSettings.dspTime - startTick;
                //              Debug.Log("Delta Time=");
                //              Debug.Log(startTick);


                //                  InvokeRepeating("FadeoutBLIP", 0f, 0.01f);
                //                 StartCoroutine("Cancelinvoke"); 
                //             ANBLIP.BLIPFadeout();
                //             ifadeoutblip = 1;
                //             StartCoroutine("FadeoutBLIP");
                //          ANBLIP.BlipStop();

                //             if ((rpm1 > 2500 && rpm1 <= 3300) || (rpm1 > 3800 && rpm1 <= 4300) || (rpm1 > 5000 && rpm1 <= 5500) || (rpm1 > 5800 && rpm1 <= 6300) || (rpm1 > 6800 && rpm1 <= 7500))
                //             {

                Debug.Log("Inside trate < 0, rpm=");
                Debug.Log(rpm1);

                if (rpm1 > 10000 && rpm1 <= 15000)
                {
                    itrate = 6100;
                    ANRevLimiter.RevLimiterStop();
                    ivibrating = 0; // Stop Vibrating after Rev Limter Stops

                    ANFSR15000.NFSR15000();
                    ANBLIP.BlipStop();
                    ANBLIP14000.Blip14000Stop();

                }
                if (rpm1 > 6800 && rpm1 <= 10000)
                {
                    itrate = 6008;
                    //            iblipcanstop = 0;


                    ANBLIPC14000.BLIPC14000();
                    ANBLIP.BlipStop();
                    ANBLIP14000.Blip14000Stop();

                    //       iblipcanstop = 0;
                }



                if (rpm1 > 6200 && rpm1 <= 6800)
                {
                    itrate = 6007;
                    //            iblipcanstop = 0;


                    ANBLIPC10000.BLIPC10000();
                    ANBLIP.BlipStop();
                    ANBLIP14000.Blip14000Stop();

                    //       iblipcanstop = 0;
                }



                if (rpm1 > 5600 && rpm1 <= 6200)
                {
                    itrate = 6006;
                    rpmblipconst = rpm1;
                    blipcinterval = 5.347f;
                    ANBLIPC7500.BLIPC7500();
                    //          iblipcanstop = 0;
                    ANBLIP.BlipStop();
                    //                ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();

                    //       iblipcanstop = 0;
                }

                if (rpm1 > 5000 && rpm1 <= 5600)
                {
                    itrate = 6005;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    ANBLIPC6000.BLIPC6000();
                    //         iblipcanstop = 0;
                    ANBLIP.BlipStop();
                    //              ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();

                    //       iblipcanstop = 0;
                }
                if (rpm1 > 4100 && rpm1 <= 5000)

                {

                    //            if ((startTick > 0.214 && startTick < 0.215) || (startTick > 0.221 && startTick < 0.222) || (startTick > 0.227 && startTick < 0.229) || (startTick > 0.234 && startTick < 0.236) || (startTick > 0.24 && startTick < 0.243) || (startTick > 0.247 && startTick < 0.249) || (startTick > 0.252 && startTick < 0.254)) iswitch = 0;
                    //            if (iswitch >= 0)
                    //            {
                    iswitch = 1;
                    itrate = 6004;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.345f;
                    ANBLIPC5500.BLIPC5500();
                    //            iblipcanstop = 0;
                    ANBLIP.BlipStop();

                    //            ANBLIP5000.Blip5000Stop();
                    ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();

                    //       iblipcanstop = 0;
                    //          }
                }
                //                         if (rpm1 > 3200 && rpm1 <= 4000 && ((startTick < 0.171 || startTick > 0.175) || (startTick < 0.161 || startTick > 0.166) || (startTick < 0.171 || startTick > 0.175)))
                if (rpm1 > 3000 && rpm1 <= 4100)
                {

                    //        if ((startTick >= 0.169 && startTick <= 0.17) || (startTick >= 0.178 && startTick <= 0.181) || (startTick >= 0.187 && startTick < 0.189) || (startTick >= 0.196 && startTick <= 0.197) || (startTick >= 0.203 && startTick <= 0.207) || (startTick >= 0.211 && startTick <= 0.215) || (startTick >= 0.217 && startTick <= 0.224)) iswitch = 0;
                    //                  if ((startTick > 0.171 && startTick < 0.175) || (startTick > 0.18 && startTick < 0.185) || (startTick > 0.19 && startTick < 0.192) || (startTick > 0.197 && startTick < 0.2) || (startTick > 0.205 && startTick < 0.209) || (startTick > 0.213 && startTick < 0.215) || (startTick > 0.22 && startTick < 0.223)) iswitch = 1;
                    //       if (iswitch >= 0)
                    //       {
                    iswitch = 1;
                    itrate = 6003;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.53f;
                    ANBLIPC4000.BLIPC4000();

                    //           iblipcanstop = 0;
                    ANBLIP.BlipStop();


                    //               ANBLIP4000.Blip4000Stop();
                    //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                    if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                    ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();


                    //       iblipcanstop = 0;
                    //       }
                }
                //        if (rpm1 > 320 && rpm1 <= 3200 && startTick > 0.0f )
                if (rpm1 > 2000 && rpm1 <= 3000)
                //          if (startTick >= 0.178)
                {
                    //          if ((startTick >= 0.103 && startTick <= 0.107) || (startTick >= 0.111 && startTick <= 0.114) || (startTick >= 0.125 && startTick <= 0.129) || (startTick >= 0.137 && startTick <= 0.142) || (startTick >= 0.148 && startTick <= 0.152)) iswitch = 0;
                    //             if ((startTick > 0.121 && startTick < 0.122) || (startTick > 0.128 && startTick < 0.131) || (startTick > 0.133 && startTick < 0.135) || (startTick > 0.140 && startTick < 0.143) || (startTick > 0.152 && startTick < 0.156) || (startTick > 0.161 && startTick < 0.166)) iswitch = 1;
                    //         if (iswitch == 0) {
                    iswitch = 1;
                    itrate = 6002;
                    rpmblipconst = rpm1;
                    blipcinterval = 0.35f;
                    ANBLIPC3000.BLIPC3000();

                    //      iblipcanstop = 0;
                    ANBLIP.BlipStop();


                    //                 ANBLIP3000.Blip3000Stop();
                    if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000.Blip4000Stop();
                    ANBLIP5000.Blip5000Stop();
                    ANBLIP6000.Blip6000Stop();
                    ANBLIP7000.Blip7000Stop();
                    ANBLIP10000.Blip10000Stop();
                    ANBLIP14000.Blip14000Stop();

                    //      }

                    //       iblipcanstop = 0;
                }




                StartCoroutine("EnableBlip");  // Enable blip after 200 ms of Blip 
                                               //         StartCoroutine("StartFSR");     // Enable FSR after Blipc ends
                                               //          StartCoroutine("StartFSR7500");     // Enable FSR after Blipc ends
                                               //          }
            }

            // FSR after SLI module
            if (((trate < -0.5 && throttlediff < 0) && (itrate == 2 || itrate == 3 || itrate == 4) && rpm1 > 2000) || (throttlediff < 0 && itrate == 10000))
            {
                blipcinterval = 0;
                Debug.Log("Inside FSR-----------------------------");
                StartCoroutine("StartFSR");     // Enable FSR after Blipc ends

            }
            // Restart Idling
            if (rpm1 <= (rpmidling + 20) && (itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999))
            {
                //          Debug.Log("Pre Enter FadeoutSLR1 ***************************************8");

                //            iingearrestartidling = 0;
                //         Debug.Log("Inside RestartIdling----------------------------- Trate=");
                //         Debug.Log(trate);
                itrate = 1;
                irpm = 0;
                //           ANFSR.FSRStop();
                StopCoroutine("FadeinNFSR");
                StopCoroutine("FadeoutNFSR");
                StopCoroutine("FadeinIdling");
                StopCoroutine("FadeoutIdling");

                ifadeoutnfsr = 1;
                fadeoutfsrtime = 0.01f;
                StartCoroutine("FadeoutNFSR");


                if (iexhaust == 0) AIdling.Idling();
                else if (iexhaust == 1) AIdlingYoshi1.Idling();
                else if (iexhaust == 2) AIdlingBMSRevo.Idling();
                ifadeinidling = 1;
                StartCoroutine("FadeinIdling");
                //        StartCoroutine("ExhaustSmokeEvents");  // Restart Idling Exhaust Smoke Pulses
                //      StartCoroutine("StartIdling");
                //          ANFSR.FSRStop();
                rpm1 = rpmidling;
                //                
                //         

                //          ANFSR15000.NFSR15000Stop();
                //          ANBLIPC14000.BLIPC14000Stop();
                //          ANBLIPC7500.BLIPC7500Stop();
                //          ANBLIPC6000.BLIPC6000Stop();
                //          ANBLIPC5500.BLIPC5500Stop();
                //          ANBLIPC4000.BLIPC4000Stop();
                //         ANBLIPC3000.BLIPC3000Stop();
                //     StartCoroutine("Setitrate1");

            }



            if (iRPMIdleSettle != 1)     // Avoid RPM needele to hover at 2000 RPM and fluctuate while initial pick up and settle down enabled when RPMIdlesettled module is done
            {

                // RPM Console Update 
                RemoveRPMConsole();

                // Idling Pulsating Needle

                if (irpm == 0)
                {
                    if (iidleneedlemoved == 0)
                    {
                        int randidle = Random.Range(0, 10);
                        if (randidle >= 5)
                        {
                            RPM2000.enabled = true;
                            RPM2100.enabled = false;
                        }
                        else if (randidle == 0)
                        {
                            iidleneedlemoved = 1;
                            StartCoroutine("EnableIdleRPMNeedleMove");
                            //                   yield return new WaitForSeconds(0.25f);
                            RPM2100.enabled = true;
                            RPM2000.enabled = false;
                        }
                    }
                }


                if (rpm1 == 0) RPM0.enabled = true;
                //          if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
                //          if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
                //          if (rpm1 >= 1500 && rpm1 < 2000) RPM1500.enabled = true;

                if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
                if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
                if (rpm1 >= 1500 && rpm1 <= 2000 && irpm == 0) RPM1500.enabled = true;
                if (rpm1 >= 1500 && rpm1 <= 2000 && irpm != 0) RPM2000.enabled = true;

                if (rpm1 >= 2000 && rpm1 < 2200) RPM2000.enabled = true;
                if (rpm1 >= 2200 && rpm1 < 2500) RPM2200.enabled = true;
                if (rpm1 >= 2500 && rpm1 < 2800) RPM2500.enabled = true;
                if (rpm1 >= 2800 && rpm1 < 3000) RPM2800.enabled = true;
                if (rpm1 >= 3000 && rpm1 < 3200) RPM3000.enabled = true;
                if (rpm1 >= 3200 && rpm1 < 3500) RPM3500.enabled = true;
                if (rpm1 >= 3500 && rpm1 < 3800) RPM3800.enabled = true;
                if (rpm1 >= 3800 && rpm1 < 4000) RPM4000.enabled = true;
                if (rpm1 >= 4000 && rpm1 < 4200) RPM4200.enabled = true;
                if (rpm1 >= 4200 && rpm1 < 4500) RPM4500.enabled = true;
                if (rpm1 >= 4500 && rpm1 < 4800) RPM4800.enabled = true;
                if (rpm1 >= 4800 && rpm1 < 5000) RPM5000.enabled = true;
                if (rpm1 >= 5000 && rpm1 < 5200) RPM5200.enabled = true;
                if (rpm1 >= 5200 && rpm1 < 5500) RPM5500.enabled = true;
                if (rpm1 >= 5500 && rpm1 < 6000) RPM6000.enabled = true;
                if (rpm1 >= 6000 && rpm1 < 6500) RPM6500.enabled = true;
                if (rpm1 >= 6500 && rpm1 < 7000) RPM7000.enabled = true;
                if (rpm1 >= 7000 && rpm1 < 7500) RPM7500.enabled = true;
                if (rpm1 >= 7500 && rpm1 < 8000) RPM8000.enabled = true;
                if (rpm1 >= 8000 && rpm1 < 8500) RPM8500.enabled = true;
                if (rpm1 >= 8500 && rpm1 < 9000) RPM9000.enabled = true;
                if (rpm1 >= 9000 && rpm1 < 9500) RPM9500.enabled = true;
                if (rpm1 >= 9500 && rpm1 < 10000) RPM10000.enabled = true;
                if (rpm1 >= 10000 && rpm1 < 10500) RPM10500.enabled = true;
                if (rpm1 >= 10500 && rpm1 < 11000) RPM11000.enabled = true;
                if (rpm1 >= 11000 && rpm1 < 11500) RPM11500.enabled = true;
                if (rpm1 >= 11500 && rpm1 < 12000) RPM12000.enabled = true;
                if (rpm1 >= 12000 && rpm1 < 12500) RPM12500.enabled = true;
                if (rpm1 >= 12500 && rpm1 < 13000) RPM13000.enabled = true;
                if (rpm1 >= 13000 && rpm1 < 13500) RPM13500.enabled = true;
                if (rpm1 >= 13500 && rpm1 < 14000) RPM14000.enabled = true;
                if (rpm1 >= 14000 && rpm1 < 14500) RPM14500.enabled = true;
                if (rpm1 >= 14500 && rpm1 <= 15000) RPM15000.enabled = true;
                //         if (rpm1 > 14000) RedlineButton.enabled = true;
                else if (rpm1 < 14000) RedlineButton.enabled = false;
            }
            // Console RPM Bars*****************************************************
            if (rpm1 > 1500 && irpm == 0)
            {
                //       ConsoleRPM500.SetActive(true);
                //      ConsoleRPM1000.SetActive(true);
                //      ConsoleRPM1100.SetActive(true);
            }

            if (rpm1 > 1500 && irpm != 0)
            {
                //      ConsoleRPM500.SetActive(true);
                //      ConsoleRPM1000.SetActive(true);
                //      ConsoleRPM1100.SetActive(true);
            }

            if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0)
            {
                //       ConsoleRPM500.SetActive(true);
                ConsoleRPM1000.SetActive(true);
            }
            if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0)
            {
                //             ConsoleRPM500.SetActive(true);
                ConsoleRPM1000.SetActive(true);
            }
            if (rpm1 >= 1500 && rpm1 < 2000) ConsoleRPM1500.SetActive(true);
            if (rpm1 >= 2000 && rpm1 < 2200) ConsoleRPM2000.SetActive(true);
            if (rpm1 >= 2200 && rpm1 < 2500) ConsoleRPM2200.SetActive(true);
            if (rpm1 >= 2500 && rpm1 < 2800) ConsoleRPM2500.SetActive(true);
            if (rpm1 >= 2800 && rpm1 < 3000) ConsoleRPM2800.SetActive(true);
            if (rpm1 >= 3000 && rpm1 < 3200) ConsoleRPM3000.SetActive(true);
            if (rpm1 >= 3200 && rpm1 < 3500) ConsoleRPM3500.SetActive(true);
            if (rpm1 >= 3500 && rpm1 < 3800) ConsoleRPM3800.SetActive(true);
            if (rpm1 >= 3800 && rpm1 < 4000) ConsoleRPM4000.SetActive(true);
            if (rpm1 >= 4000 && rpm1 < 4200) ConsoleRPM4200.SetActive(true);
            if (rpm1 >= 4200 && rpm1 < 4500) ConsoleRPM4500.SetActive(true);
            if (rpm1 >= 4500 && rpm1 < 4800) ConsoleRPM4800.SetActive(true);
            if (rpm1 >= 4800 && rpm1 < 5000) ConsoleRPM5000.SetActive(true);
            if (rpm1 >= 5000 && rpm1 < 5200) ConsoleRPM5200.SetActive(true);
            if (rpm1 >= 5200 && rpm1 < 5500) ConsoleRPM5500.SetActive(true);
            if (rpm1 >= 5500 && rpm1 < 6000) ConsoleRPM6000.SetActive(true);

            if (rpm1 >= 6000 && rpm1 < 6500) ConsoleRPM6500.SetActive(true);
            if (rpm1 >= 6500 && rpm1 < 7000) ConsoleRPM7000.SetActive(true);

            if (rpm1 >= 7000 && rpm1 < 7500) ConsoleRPM7500.SetActive(true);
            //      if(rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM7700.SetActive(true);
            if (rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM8000.SetActive(true);
            if (rpm1 >= 8000 && rpm1 < 8500) ConsoleRPM8500.SetActive(true);
            if (rpm1 >= 8500 && rpm1 < 9000) ConsoleRPM9000.SetActive(true);

            if (rpm1 >= 9000 && rpm1 < 9500) ConsoleRPM9500.SetActive(true);

            if (rpm1 >= 9500 && rpm1 < 10000) ConsoleRPM10000.SetActive(true);


            if (rpm1 >= 10000 && rpm1 < 10500) ConsoleRPM10500.SetActive(true);
            if (rpm1 >= 10500 && rpm1 < 11000) ConsoleRPM11000.SetActive(true);
            if (rpm1 >= 11000 && rpm1 < 11500) ConsoleRPM11500.SetActive(true);

            if (rpm1 >= 11500 && rpm1 < 12000) ConsoleRPM12000.SetActive(true);
            if (rpm1 >= 12000 && rpm1 <= 12500) ConsoleRPM12500.SetActive(true);
            if (rpm1 > 12500 && rpm1 < 13000) ConsoleRPM13000.SetActive(true);
            if (rpm1 >= 13000 && rpm1 < 13500) ConsoleRPM13500.SetActive(true);
            if (rpm1 >= 13500 && rpm1 < 14000) ConsoleRPM14000.SetActive(true);
            if (rpm1 >= 14000 && rpm1 < 14500) ConsoleRPM14500.SetActive(true);
            if (rpm1 >= 14500 && rpm1 <= 15000) ConsoleRPM15000.SetActive(true);


            slidervalold = sliderval;  // Assign Slidervalue to Slidrold
        }







    }
    // Coroutines***************************************

    // RPM Console Effects

    // Idling needle Enable Move
    private IEnumerator EnableIdleRPMNeedleMove()
    {
        yield return new WaitForSeconds(0.5f);
        iidleneedlemoved = 0;
    }

    IEnumerator Startistart1()
    {
        //       yield return new WaitForSeconds(rpmdisplayafterstart);
        yield return new WaitForSeconds(0.5f);
        istart = 1;
        //      rpm1 = rpmidling;
    }

    // Start Idling
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(0.5f); // wait half a secon
        irpm = 0;
        rpm1 = rpmidling;
        //   AIdling.Idling();
        if (iexhaust == 0) AIdling.Idling();
        if (iexhaust == 1) AIdlingYoshi1.Idling();
        if (iexhaust == 2) AIdlingBMSRevo.Idling();



        // Red Console button Off
        RedButtonOn.enabled = false;
    }




    IEnumerator EnableBlip()
    {
        yield return new WaitForSeconds(0.2f);
        iblipcanstart = 1;
        Debug.Log("Inside Enable Blip");
    }

    // Enable Trate for Revlimiter after 0.1s of Revlimiter Start so Reducing waits for 0.1 s
    private IEnumerator EnableFSR15000afterRevlimiterStart()
    {
        yield return new WaitForSeconds(0.1f);
        itrate = 10000;
    }

    IEnumerator StartFSR()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2 || itrate == 3 || itrate == 4 || itrate == 10000)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            ifadeinnfsr = 1;
            ANFSR.FSR();
            //      ANFSRYoshi1.FSR();
            //       ANFSRBMSRevo.FSR();
            StartCoroutine("FadeinNFSR");
            if (iexhaust == 0) ANRevLimiter.RevLimiterStop();
            else if (iexhaust == 1) ANRevLimiterYoshi1.RevLimiterStop();
            //         else if (iexhaust == 2) ANRevLimiteBMSRevo.RevLimiterStop();
            //       Debug.Log("Before POP Crackle Ninja400 RPM=");
            //       Debug.Log(rpm1);
            //       Debug.Log(iPopCrackle2);
            if (iPopCrackle2 == 1 && rpm1 > 7000)
            {
                //    Debug.Log("Inside POP Crackle Ninja400");
                //       ACrackle10000.Crackle10000();
                //         StartCoroutine("Explode");
            }
            else if (iPopCrackle2 == 1 && rpm1 < 7000 && rpm1 > 5000)
            {
                //    Debug.Log("Inside POP Crackle Ninja400 2");
                //     ACrackle8000.Crackle8000();
                //    StartCoroutine("Explode");
            }


            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLI");
            StartCoroutine("FadeoutNFSI1");

            ANFSI2.NFSI2Stop();

            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;




            itrate = 4999;
        }


    }

    IEnumerator StartFSR7500()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            itrate = 4999;
            ANFSR7500.FSR7500();
            ANSLI.NSLIStop();
            //       iblipcanstart = 1;

        }


    }


    void FadeoutBLIP()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        //      while (ifadeoutblip == 1)
        //      {
        //          Debug.Log("Enter FadeoutSLR1***********************************2");
        //          yield return new WaitForSeconds(0.001f); // wait half a second

        ANBLIP.BLIPFadeout();
        //     }
    }

    IEnumerator Cancelinvoke()
    {


        yield return new WaitForSeconds(0.1f); // wait half a second
        CancelInvoke();

    }




    IEnumerator FadeinIdling()
    {
        //***************
        while (ifadeinidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (iexhaust == 0) AIdling.IdlingFadein();
            else if (iexhaust == 1) AIdlingYoshi1.IdlingFadein();
            else if (iexhaust == 2) AIdlingBMSRevo.IdlingFadein();
        }
    }

    IEnumerator FadeoutIdling()
    {
        //***************
        while (ifadeoutidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutidlingtime); // wait half a second
            if (iexhaust == 0) AIdling.IdlingFadeout();
            else if (iexhaust == 1) AIdlingYoshi1.IdlingFadeout();
            else if (iexhaust == 2) AIdlingBMSRevo.IdlingFadeout();
        }
    }

    // Defunct************************
    IEnumerator FadeoutNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutfsrtime); // wait half a second
            if (iexhaust == 0) ANFSR.NFSRFadeout();
            if (iexhaust == 1) ANFSRYoshi1.NFSRFadeout();
            if (iexhaust == 2) ANFSRBMSRevo.NFSRFadeout();
            // Fadeout BlipFSRs
            //       ANFSR15000.BLIPCR15000Fadeout();
            //      ANBLIPC14000.BLIPCR14000Fadeout();
            //      ANBLIPC7500.BLIPCR7500Fadeout();
            //      ANBLIPC6000.BLIPCR6000Fadeout();
            //      ANBLIPC5500.BLIPCR5000Fadeout();
            //      ANBLIPC4000.BLIPCR4000Fadeout();
            //      ANBLIPC3000.BLIPCR3000Fadeout();
        }
    }

    IEnumerator FadeinNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (iexhaust == 0) ANFSR.NFSRFadein();
            else if (iexhaust == 1) ANFSRYoshi1.NFSRFadein();
            else if (iexhaust == 2) ANFSRBMSRevo.NFSRFadein();
        }
    }

    IEnumerator FadeoutNSLI()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if (iexhaust == 0) ANSLI.NSLIFadeout();
            else if (iexhaust == 1) ANSLIYoshi1.NSLIFadeout();
            else if (iexhaust == 2) ANSLIBMSRevo.NSLIFadeout();

        }
    }
    IEnumerator FadeoutNFSI1()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            if (iexhaust == 0) ANFSI1.NFSI1Fadeout();
            else if (iexhaust == 1) ANFSI1Yoshi1.NFSI1Fadeout();
            else if (iexhaust == 2) ANFSI1BMSRevo.NFSI1Fadeout();
        }
    }


    // ***************************************************

    IEnumerator FadeoutBLIPCR7500()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
                                                    //           ANFSR.NFSRFadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
        }
    }

    IEnumerator FadeoutBackgroundMusic()
    {

        //               Debug.Log("Enter FadeoutBackground***********************************2");

        AudioSource audio = GetComponent<AudioSource>();
        float startVolume = audio.volume;

        while (audio.volume > 0)
        {
            audio.volume -= startVolume * Time.deltaTime / 1f;

            yield return null;
        }
        //     audio.volume = 0f;
    }


    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator RevLimiterVibrationOnOff()
    {
        if (ivibrator == 1) //Handheld.Vibrate();
            while (ivibrating == 1)
            {
                yield return new WaitForSeconds(0.5f);
                if (ivibrator == 1)
                {
                    //          Handheld.Vibrate();
                }
            }

    }

    // Keyin from Touch
    public void KeyInFromTouch()
    {
        //      moveSpeed = newSpeed;

        if (ikeyinfromtouch == 0)
        {
            ikeyinfromtouch = 1;
            Hlight.HeadlightOn();
            Rlight.TaillightOn();
            ConsoleRPM0.SetActive(true);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(false);

        }
        else if (ikeyinfromtouch == 2 && istartfromtouch != 0)
        {
            ikeyinfromtouch = 0;
            istartfromtouch = 3;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;
            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.TaillightOff();
            ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
            StartCoroutine("DelayRemoveConsoleRPM0");  //Due to inbility to remove Console RPM0 after directly shutting engine from key

        }
        else if (ikeyinfromtouch == 2)
        {
            ikeyinfromtouch = 0;
            RedButtonOn.enabled = false;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.TaillightOff();
            ConsoleRPM0.SetActive(false);// COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
        }




    }

    private IEnumerator DelayRemoveConsoleRPM0()
    {
        yield return new WaitForSeconds(0.2f);
        ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
    }
    // StartStop from Touch
    public void StartStopFromTouch()
    {
        //      moveSpeed = newSpeed;
        if (ikeyinfromtouch == 2)
        {
            if (istartfromtouch == 0) istartfromtouch = 1;
            else if (istartfromtouch == 2) istartfromtouch = 3;
        }
        // Make StartStopButton Dark
        StartStopButton.enabled = false;
        StartStopButtonPressed.enabled = true;
        //       StartCoroutine("StartStopButtonAppear");
    }
    // ReAppear StartStopButton(Original)


    // ReAppear StartStopButton(Original)
    public void Start1StopButtonAppear()  // 
    {

        StartStopButton.enabled = true;
        StartStopButtonPressed.enabled = false;

    }

    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        ReverbXoneMix = 0.00f;
        RoadView.SetActive(true); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage
        TunnelView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(false);
        HeadlightReflectionL.SetActive(false);

        AIdling.IdlingReverbRoad();
        AStartup.StartupReverbRoad();
        AStarting.StartingReverbRoad();
        AShutting.ShuttingReverbRoad();
        ANRevLimiter.RevLimiterReverbRoad();
        ANFSR.NFSRReverbRoad();
        ANSLI.NSLIReverbRoad();
        ANFSI1.NFSI1ReverbRoad();
        ANFSI2.NFSI2ReverbRoad();

        //       AStartingYoshi1.StartingReverbRoad();
        //       AIdlingYoshi1.IdlingReverbRoad();
        //       ANSLIYoshi1.NSLIReverbRoad();
        //       ANFSI1Yoshi1.NFSI1ReverbRoad();
        //       ANFSRYoshi1.NFSRReverbRoad();
        //       ANRevLimiterYoshi1.RevLimiterReverbRoad();
        //     ANFSI1.NFSI1ReverbRoad();

        //       AStartingBMSRevo.StartingReverbRoad();
        //       AIdlingBMSRevo.IdlingReverbRoad();
        //       ANSLIBMSRevo.NSLIReverbRoad();
        //      ANFSI1BMSRevo.NFSI1ReverbRoad();
        //      ANFSRBMSRevo.NFSRReverbRoad();
        //        ANRevLimiterYoshi1.RevLimiterReverbRoad();


        //        ACrackle10000.Crackle10000ReverbRoad();
        //        ACrackle8000.Crackle8000ReverbRoad();
    }

    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {
        ReverbXoneMix = 1.03f;

        //   TunnelView.SetActive(false); // DisAppearance of Tunnel
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(true); // Disapperance of Garage
        TunnelView.SetActive(false); // Appearance of Road

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        ANFSI2.NFSI2ReverbGarage();

        //       AStartingYoshi1.StartingReverbGarage();
        //       AIdlingYoshi1.IdlingReverbGarage();
        //       ANSLIYoshi1.NSLIReverbGarage();
        //       ANFSI1Yoshi1.NFSI1ReverbGarage();
        //       ANFSRYoshi1.NFSRReverbGarage();
        //       ANRevLimiterYoshi1.RevLimiterReverbGarage();
        //     ANFSI1.NFSI1ReverbGarage();


        //       AStartingBMSRevo.StartingReverbGarage();
        //      AIdlingBMSRevo.IdlingReverbGarage();
        //      ANSLIBMSRevo.NSLIReverbGarage();
        //       ANFSI1BMSRevo.NFSI1ReverbGarage();
        //       ANFSRBMSRevo.NFSRReverbGarage();
        //       ANRevLimiterYoshi1.RevLimiterReverbGarage();

        //       ACrackle10000.Crackle10000ReverbGarage();
        //       ACrackle8000.Crackle8000ReverbGarage();
    }

    public void TunnelAppear()  // 
    {
        ReverbXoneMix = 1.1f;

        //   TunnelView.SetActive(false); // DisAppearance of Tunnel
        TunnelView.SetActive(true); // Appearance of Road
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);

        AIdling.IdlingReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        ANFSI2.NFSI2ReverbGarage();


        //       AStartingYoshi1.StartingReverbGarage();
        //       AIdlingYoshi1.IdlingReverbGarage();
        //       ANSLIYoshi1.NSLIReverbGarage();
        //       ANFSI1Yoshi1.NFSI1ReverbGarage();
        //       ANFSRYoshi1.NFSRReverbGarage();
        //       ANRevLimiterYoshi1.RevLimiterReverbGarage();
        //    ANFSI1.NFSI1ReverbGarage();

        //       AStartingBMSRevo.StartingReverbGarage();
        //       AIdlingBMSRevo.IdlingReverbGarage();
        //       ANSLIBMSRevo.NSLIReverbGarage();
        //       ANFSI1BMSRevo.NFSI1ReverbGarage();
        //       ANFSRBMSRevo.NFSRReverbGarage();
        // /      ANRevLimiterYoshi1.RevLimiterReverbGarage();

        //       ACrackle10000.Crackle10000ReverbGarage();
        //       ACrackle8000.Crackle8000ReverbGarage();
    }

    public void ExhaustStock()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            iexhaust = 0;
            //       exhaustDropDown = 0;
            //       StockMuffler.SetActive(true); // Disapperance of Stovk Muffler
            //     StockMufflerShield.SetActive(true); // Disapperance of Stovk Muffler
            StockExhaustTick.SetActive(true); // Tick on Stock Ex Panel
            StockMufflerSoundSource.SetActive(true);  //Activate Akra1 Sound Sources
                                                      //       Yoshi1ExhaustTick.SetActive(false); // Tick vanish on Akra1 Ex Panel
                                                      //       BMSRevoExhaustTick.SetActive(false); // Tick on Stock Ex Panel
                                                      //       Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel

            //           StockMufflerShield.SetActive(true); // Disapperance of Stovk Muffler Shield
            //      Yoshi1Muffler.SetActive(false); // Disapperance of Akra Carbon Muffler                                   // Akra1Muffler.SetActive(false); // Disapperance of Stovk Muffler
            //       AkraBlackMuffler.SetActive(false); // Disapperance of Stovk Muffler
            //      BMSRevoMuffler.SetActive(false); // Disapperance of Stovk Muffler

            //      Yoshi1MufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
            //      BMSRevoMufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
        }

    }
    public void ExhaustYoshi1()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off
            if (Jawa2sExhaustStock == 1)  // Only Activate when Akra1 Purchased
            {
                iexhaust = 1;
                //     exhaustDropDown = 0;
                Yoshi1Muffler.SetActive(true); // Disapperance of Stovk Muffler
                Yoshi1ExhaustTick.SetActive(true); // Tick on Stock Ex Panel
                                                   //        Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel

                StockExhaustTick.SetActive(false); // Tick on Stock Ex Panel
                BMSRevoExhaustTick.SetActive(false); // Tick on Stock Ex Panel
                ExhaustStockPurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
                StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler                                         //       

                BMSRevoMuffler.SetActive(false); // Disapperance of Stovk Muffler
                                                 //    AkraBlackMuffler.SetActive(true); // Disapperance of Stovk Muffler
                StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
                Yoshi1MufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
                BMSRevoMufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
            }
        }
    }

    public void ExhaustBMSRevo()  // 
    {
        if (istartfromtouch != 2)
        { // Only works when Engine is off


            if (Jawa2sExhaustXYZ == 1)  // Only Activate when Akra1 Purchased
            {
                Debug.Log("INside BMSRevo");
                iexhaust = 2;
                //     exhaustDropDown = 0;
                BMSRevoMuffler.SetActive(true); // Disapperance of Stovk Muffler
                BMSRevoExhaustTick.SetActive(true); // Tick on Stock Ex Panel
                                                    //        Akra1CarbonExhaustTick.SetActive(false); // Tick on Stock Ex Panel

                StockExhaustTick.SetActive(false); // Tick on Stock Ex Panel
                Yoshi1ExhaustTick.SetActive(false); // Tick vanish on Akra1 Ex Panel
                                                    //       ExhaustXYZPurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                StockMuffler.SetActive(false); // Disapperance of Stovk Muffler
                Yoshi1Muffler.SetActive(false); // Disapperance of Akra Carbon Muffler 
                StockMufflerShield.SetActive(false); // Disapperance of Stovk Muffler                                         //       

                //    AkraBlackMuffler.SetActive(true); // Disapperance of Stovk Muffler
                StockMufflerSoundSource.SetActive(false);  //DeActivate Akra1 Sound Sources
                Yoshi1MufflerSoundSource.SetActive(false);  // Deactivate Akra1 Sound Sources
                BMSRevoMufflerSoundSource.SetActive(true);  // Deactivate Akra1 Sound Sources
            }
        }
    }



    public void MoveRPMConsole1()
    {
        if (iRPMConsole == 0)
        {
            RPMConsole1.transform.position = new Vector3(990, 130, 0);
            RPMConsole1.transform.localScale += new Vector3(5, 1.5f, 0);

            //    RPMConsole1.SetActive(false);
            iRPMConsole = 1;
        }
        else if (iRPMConsole == 1)
        {
            //     RPMConsole1.SetActive(true);
            RPMConsole1.transform.localScale += new Vector3(-5, -1.5f, 0);
            RPMConsole1.transform.position = new Vector3(980, 130, 0);
            iRPMConsole = 0;
        }
    }


    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        sliderval = sliderposition;

    }

    // GUI Events Functions
    public void VibratorOnOff()
    {
        if (ivibrator == 0) ivibrator = 1;
        else if (ivibrator == 1) ivibrator = 0;

    }


    // Console Startup Sequence************************************************************
    int rpmi = 1000;   // Counter for rpm change for Console Startup Sequence
                       //   float wait=0.1;
    private IEnumerator ConsoleStartup()
    {
        yield return new WaitForSeconds(1f);
        while (iconsolestartup == 1 || iconsolestartup == -1)
        {
            yield return new WaitForSeconds(0.01f); // wait half a second

            RemoveRPMConsole();
            //     if (rpmi <= 7400)
            //     {
            if (iconsolestartup == 1 && rpmi <= 12500)
            {
                rpmi = rpmi + iconsolestartuprate * 2;
            }
            else iconsolestartup = -1;
            if (iconsolestartup == -1 && rpmi >= 1000)
            {
                rpmi = rpmi - iconsolestartuprate * 2;
                if (rpmi <= 900)
                {
                    //              RPM0.enabled = true;
                    rpmi = 0;
                    iconsolestartup = 0;
                }
            }

            if (rpmi < 900) RPM0.enabled = true;
            if (rpmi >= 900 && rpmi < 1500) RPM1000.enabled = true;
            if (rpmi >= 1500 && rpmi < 2000) RPM1500.enabled = true;
            if (rpmi >= 2000 && rpmi < 2200) RPM2000.enabled = true;
            if (rpmi >= 2200 && rpmi < 2500) RPM2200.enabled = true;
            if (rpmi >= 2500 && rpmi < 2800) RPM2500.enabled = true;
            if (rpmi >= 2800 && rpmi < 3000) RPM2800.enabled = true;
            if (rpmi >= 3000 && rpmi < 3200) RPM3000.enabled = true;
            if (rpmi >= 3200 && rpmi < 3500) RPM3500.enabled = true;
            if (rpmi >= 3500 && rpmi < 3800) RPM3800.enabled = true;
            if (rpmi >= 3800 && rpmi < 4000) RPM4000.enabled = true;
            if (rpmi >= 4000 && rpmi < 4200) RPM4200.enabled = true;
            if (rpmi >= 4200 && rpmi < 4500) RPM4500.enabled = true;
            if (rpmi >= 4500 && rpmi < 4800) RPM4800.enabled = true;
            if (rpmi >= 4800 && rpmi < 5000) RPM5000.enabled = true;
            if (rpmi >= 5000 && rpmi < 5200) RPM5200.enabled = true;
            if (rpmi >= 5200 && rpmi < 5500) RPM5500.enabled = true;
            if (rpmi >= 5500 && rpmi < 6000) RPM6000.enabled = true;
            if (rpmi >= 6000 && rpmi < 6500) RPM6500.enabled = true;
            if (rpmi >= 6500 && rpmi < 7000) RPM7000.enabled = true;
            if (rpmi >= 7000 && rpmi < 7500) RPM7500.enabled = true;
            if (rpmi >= 7500 && rpmi < 8000) RPM8000.enabled = true;
            if (rpmi >= 8000 && rpmi < 8500) RPM8500.enabled = true;
            if (rpmi >= 8500 && rpmi < 9000) RPM9000.enabled = true;
            if (rpmi >= 9000 && rpmi < 9500) RPM9500.enabled = true;
            if (rpmi >= 9500 && rpmi < 10000) RPM10000.enabled = true;
            if (rpmi >= 10000 && rpmi < 10500) RPM10500.enabled = true;
            if (rpmi >= 10500 && rpmi < 11000) RPM11000.enabled = true;
            if (rpmi >= 11000 && rpmi < 11500) RPM11500.enabled = true;
            if (rpmi >= 11500 && rpmi < 12000) RPM12000.enabled = true;
            if (rpmi >= 12000 && rpmi < 12500) RPM12500.enabled = true;
            if (rpmi >= 12500 && rpmi < 13000) RPM13000.enabled = true;
            if (rpmi >= 13000 && rpmi < 13500) RPM13500.enabled = true;
            if (rpmi >= 13500 && rpmi < 14000) RPM14000.enabled = true;
            if (rpmi >= 14000 && rpmi < 14500) RPM14500.enabled = true;
            if (rpmi >= 14500 && rpmi < 15000) RPM15000.enabled = true;

            // Console RPM Bars*****************************************************


            if (rpmi < 900) ConsoleRPM0.SetActive(true);
            if (rpmi >= 900 && rpmi < 1500) ConsoleRPM1000.SetActive(true);
            if (rpmi >= 1500 && rpmi < 2000) ConsoleRPM1500.SetActive(true);

            if (rpmi >= 2000 && rpmi < 2200) ConsoleRPM2000.SetActive(true);
            if (rpmi >= 2200 && rpmi < 2500) ConsoleRPM2200.SetActive(true);
            if (rpmi >= 2500 && rpmi < 2800) ConsoleRPM2500.SetActive(true);
            if (rpmi >= 2800 && rpmi < 3000) ConsoleRPM2800.SetActive(true);
            if (rpmi >= 3000 && rpmi < 3200) ConsoleRPM3000.SetActive(true);
            if (rpmi >= 3200 && rpmi < 3500) ConsoleRPM3500.SetActive(true);
            if (rpmi >= 3500 && rpmi < 3800) ConsoleRPM3800.SetActive(true);
            if (rpmi >= 3800 && rpmi < 4000) ConsoleRPM4000.SetActive(true);
            if (rpmi >= 4000 && rpmi < 4200) ConsoleRPM4200.SetActive(true);
            if (rpmi >= 4200 && rpmi < 4500) ConsoleRPM4500.SetActive(true);
            if (rpmi >= 4500 && rpmi < 4800) ConsoleRPM4800.SetActive(true);
            if (rpmi >= 4800 && rpmi < 5000) ConsoleRPM5000.SetActive(true);
            if (rpmi >= 5000 && rpmi < 5200) ConsoleRPM5200.SetActive(true);
            if (rpmi >= 5200 && rpmi < 5500) ConsoleRPM5500.SetActive(true);
            if (rpmi >= 5500 && rpmi < 6000) ConsoleRPM6000.SetActive(true);

            if (rpmi >= 6000 && rpmi < 6500) ConsoleRPM6500.SetActive(true);
            if (rpmi >= 6500 && rpmi < 7000) ConsoleRPM7000.SetActive(true);

            if (rpmi >= 7000 && rpmi < 7500) ConsoleRPM7500.SetActive(true);
            //      if(rpmi >= 7500 && rpmi < 8000) ConsoleRPM7700.SetActive(true);
            if (rpmi >= 7500 && rpmi < 8000) ConsoleRPM8000.SetActive(true);
            if (rpmi >= 8000 && rpmi < 8500) ConsoleRPM8500.SetActive(true);
            if (rpmi >= 8500 && rpmi < 9000) ConsoleRPM9000.SetActive(true);

            if (rpmi >= 9000 && rpmi < 9500) ConsoleRPM9500.SetActive(true);

            if (rpmi >= 9500 && rpmi < 10000) ConsoleRPM10000.SetActive(true);


            if (rpmi >= 10000 && rpmi < 10500) ConsoleRPM10500.SetActive(true);
            if (rpmi >= 10500 && rpmi < 11000) ConsoleRPM11000.SetActive(true);
            if (rpmi >= 11000 && rpmi < 11500) ConsoleRPM11500.SetActive(true);

            if (rpmi >= 11500 && rpmi < 12000) ConsoleRPM12000.SetActive(true);
            if (rpmi >= 12000 && rpmi < 12500) ConsoleRPM12500.SetActive(true);
            if (rpmi >= 12500 && rpmi < 13000) ConsoleRPM13000.SetActive(true);
            if (rpmi >= 13000 && rpmi < 13500) ConsoleRPM13500.SetActive(true);
            if (rpmi >= 13500 && rpmi < 14000) ConsoleRPM14000.SetActive(true);
            if (rpmi >= 14000 && rpmi < 14500) ConsoleRPM14500.SetActive(true);
            if (rpmi >= 14500 && rpmi < 15000) ConsoleRPM15000.SetActive(true);


        }
        //      newImage = Image.FromFile(@"C:/User Data/VS/Software/Bass/Combined-Project/Blank-Application/Images/" + rpmString + ".png");
    }

    private IEnumerator RPMIdleSettle()
    {
        yield return new WaitForSeconds(0.4f);

        while (iRPMIdleSettle == 1)
        {
            yield return new WaitForSeconds(0.05f);

            ineedle = ineedle + 1;
            rpmi = rpmidlesettleed[ineedle];
            //        Debug.Log("RPMi=");
            //        Debug.Log(rpmi);
            RemoveRPMConsole();
            if (rpmi == 0)
            {
                RPM0.enabled = true;
                Debug.Log("RPM0 Enabled");
            }
            if (rpmi > 0 && rpmi < 300)
            {
                RPM200.enabled = true;
                ConsoleRPM200.SetActive(true);
            }
            if (rpmi >= 300 && rpmi < 600)
            {
                RPM500.enabled = true;
                ConsoleRPM500.SetActive(true);
            }
            if (rpmi >= 600 && rpmi < 900)
            {
                ConsoleRPM800.SetActive(true);
                RPM800.enabled = true;
            }
            if (rpmi >= 900 && rpmi < 1100)
            {
                ConsoleRPM1000.SetActive(true);
                RPM1000.enabled = true;
            }
            if (rpmi >= 1100 && rpmi < 1600)
            {
                RPM1500.enabled = true;
                ConsoleRPM1500.SetActive(true);
            }
            if (rpmi >= 1600 && rpmi < 1900)
            {

                RPM1800.enabled = true;
                ConsoleRPM1800.SetActive(true);
            }
            if (rpmi >= 1900 && rpmi < 2100)
            {
                RPM2000.enabled = true;
                ConsoleRPM2000.SetActive(true);
                //    Debug.Log("RPM2000 Enabled");
            }
            if (rpmi >= 2100 && rpmi < 2300)
            {
                RPM2200.enabled = true;
                ConsoleRPM2200.SetActive(true);
            }
            if (rpmi >= 2300 && rpmi < 2600)
            {
                RPM2500.enabled = true;
                ConsoleRPM2500.SetActive(true);
            }
            if (rpmi >= 2600 && rpmi < 2900)
            {
                RPM2800.enabled = true;
                ConsoleRPM2800.SetActive(true);
            }
            if (rpmi >= 2900 && rpmi < 3100)
            {
                RPM3000.enabled = true;
                ConsoleRPM3000.SetActive(true);
            }
            if (rpmi >= 3100 && rpmi < 3300)
            {
                RPM3200.enabled = true;
                ConsoleRPM3200.SetActive(true);
            }
            if (ineedle >= 53) iRPMIdleSettle = 0;

        }

        //      RemoveRPMConsole();
    }



    // Console RPM Remove
    public void RemoveRPMConsole()
    {
        RPM0.enabled = false;
        RPM200.enabled = false;
        RPM500.enabled = false;
        RPM800.enabled = false;
        RPM1000.enabled = false;
        RPM1100.enabled = false;
        RPM1500.enabled = false;
        RPM1800.enabled = false;
        RPM2000.enabled = false;
        //      RPM2000.enabled = false;
        RPM2200.enabled = false;
        RPM2500.enabled = false;
        RPM2800.enabled = false;
        RPM3000.enabled = false;
        RPM3200.enabled = false;
        RPM3500.enabled = false;
        RPM3800.enabled = false;
        RPM4000.enabled = false;
        RPM4200.enabled = false;
        RPM4500.enabled = false;
        RPM4800.enabled = false;
        RPM5000.enabled = false;
        RPM5200.enabled = false;
        RPM5500.enabled = false;
        RPM6000.enabled = false;
        RPM6500.enabled = false;
        RPM7000.enabled = false;
        RPM7500.enabled = false;

        RPM8000.enabled = false;
        RPM8500.enabled = false;
        RPM9000.enabled = false;
        RPM9500.enabled = false;
        RPM10000.enabled = false;
        RPM10500.enabled = false;
        RPM11000.enabled = false;
        RPM11500.enabled = false;
        RPM12000.enabled = false;
        RPM12500.enabled = false;
        RPM13000.enabled = false;
        RPM13500.enabled = false;
        RPM14000.enabled = false;
        RPM14500.enabled = false;
        RPM15000.enabled = false;

        // RPM Console Section


        //        for (int irpmconsolexxx = 0; irpmconsolexxx < 15000; irpmconsolexxx = irpmconsolexxx + 100)
        //        {
        //            string name = $"ConsoleRPM " + "rpm1";
        //          ConsoleRPM500.name = "name";
        //          ConsoleRPM500.SetActive(false);
        //      }


        // Console View Bars
        //     ConsoleRPMOff.SetActive(false);
        ConsoleRPM0.SetActive(false);
        ConsoleRPM200.SetActive(false);
        ConsoleRPM500.SetActive(false);
        ConsoleRPM800.SetActive(false);
        ConsoleRPM1000.SetActive(false);
        ConsoleRPM1500.SetActive(false);
        ConsoleRPM1800.SetActive(false);
        ConsoleRPM2000.SetActive(false);
        ConsoleRPM2200.SetActive(false);
        ConsoleRPM2500.SetActive(false);
        ConsoleRPM2800.SetActive(false);
        ConsoleRPM3000.SetActive(false);
        ConsoleRPM3200.SetActive(false);
        ConsoleRPM3500.SetActive(false);
        ConsoleRPM3800.SetActive(false);
        ConsoleRPM4000.SetActive(false);
        ConsoleRPM4200.SetActive(false);
        ConsoleRPM4500.SetActive(false);
        ConsoleRPM4800.SetActive(false);
        ConsoleRPM5000.SetActive(false);
        ConsoleRPM5200.SetActive(false);
        ConsoleRPM5500.SetActive(false);
        ConsoleRPM6000.SetActive(false);
        ConsoleRPM6500.SetActive(false);
        ConsoleRPM7000.SetActive(false);
        ConsoleRPM7500.SetActive(false);
        ConsoleRPM7700.SetActive(false);
        ConsoleRPM8000.SetActive(false);
        ConsoleRPM8200.SetActive(false);
        ConsoleRPM8500.SetActive(false);
        ConsoleRPM8800.SetActive(false);
        ConsoleRPM9000.SetActive(false);
        ConsoleRPM9200.SetActive(false);
        ConsoleRPM9500.SetActive(false);
        ConsoleRPM9800.SetActive(false);
        ConsoleRPM10000.SetActive(false);
        ConsoleRPM10200.SetActive(false);
        ConsoleRPM10500.SetActive(false);
        ConsoleRPM10800.SetActive(false);
        ConsoleRPM11000.SetActive(false);
        ConsoleRPM11200.SetActive(false);
        ConsoleRPM11500.SetActive(false);
        ConsoleRPM11800.SetActive(false);
        ConsoleRPM12000.SetActive(false);
        ConsoleRPM12200.SetActive(false);
        ConsoleRPM12500.SetActive(false);
        ConsoleRPM12800.SetActive(false);
        ConsoleRPM13000.SetActive(false);
        ConsoleRPM13200.SetActive(false);
        ConsoleRPM13500.SetActive(false);
        ConsoleRPM13800.SetActive(false);
        ConsoleRPM14000.SetActive(false);
        ConsoleRPM14200.SetActive(false);
        ConsoleRPM14500.SetActive(false);
        ConsoleRPM14800.SetActive(false);
        ConsoleRPM15000.SetActive(false);

    }
    int iexhaustpulse = 0;
    IEnumerator ExhaustSmokeEvents()
    {

        while (itrate == 1 && iexhaustpulse == 0)
        {
            yield return new WaitForSeconds(0.1f);
            //         Debug.Log("INside Exhaust Smoke");
            StartCoroutine("Explode");
            StartCoroutine("StopExplode");
            iexhaustpulse = 1;
        }
    }
    // Exhaust Pop/Backfire
    private IEnumerator Explode()
    {
        yield return new WaitForSeconds(0.2f);
        //      Debug.Log("INside Exhaust POP");
        AVExhaustPop1.ExhaustPop();
        AVExhaustPop2.ExhaustPop();

    }
    // Exhaust Pop/Backfire

    private IEnumerator StopExplode()
    {
        yield return new WaitForSeconds(0.22f);
        //      Debug.Log("INside Exhaust POP Stop");
        AVExhaustPop1.ExhaustPopStop();
        AVExhaustPop2.ExhaustPopStop();
        //   Debug.Log("Testing 2");
        iexhaustpulse = 0;
    }

    public void PopCrackleEnable()  // Enable/Disable Pop Crackles
    {
        if (iPopCrackle2 == 0)
        {
            iPopCrackle2 = 1;
            Debug.Log("Pop Tick******************");
            PopsCrackles1Tick.SetActive(true);


        }
        else if (iPopCrackle2 == 1)
        {
            iPopCrackle2 = 0;
            PopsCrackles1Tick.SetActive(false);
        }
    }

    private IEnumerator CantStartwithoutExhhaustPurchaseDeactivate()
    {
        yield return new WaitForSeconds(3f);
        CantstartwithoutExhaustPurNote.SetActive(false);
    }

}
