﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP10000R35Aft1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        LowPassFilter();
    }

    public void Blip10000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineR35.rpm1 >= 7000 && AudioEngineR35.rpm1 < 8000)
        {
            audio.Play();
        }
        else if (AudioEngineR35.rpm1 >= 6000 && AudioEngineR35.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.016F);
        }
        else if (AudioEngineR35.rpm1 >= 5000 && AudioEngineR35.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.027F);
        }
        else if (AudioEngineR35.rpm1 >= 4000 && AudioEngineR35.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.048F);
        }
        else if (AudioEngineR35.rpm1 >= 3000 && AudioEngineR35.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.099F);
        }
        else if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.164F);
        }


        else if (AudioEngineR35.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.26F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip10000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip10000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip10000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}


