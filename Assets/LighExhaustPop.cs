﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighExhaustPop : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LightOnExhaust()
    {
        var myLight = GetComponent<Light>();
        myLight.enabled = true;
    }
    public void LightOff2Exhaust()
    {
        var myLight = GetComponent<Light>();
        myLight.enabled = false;
    }
}
