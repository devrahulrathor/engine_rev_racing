using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterBullet500 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (SoundEngineBullet500Drag.itrate == 10000)
        {
            //      RPMCal();
            // SoundEngineBullet500Drag.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        //        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (SoundEngineBullet500Drag.ivibrating == 1)
        {

            //          SoundEngineBullet500Drag.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (SoundEngineBullet500Drag.ivibrator == 1)
            //       {
            if (SoundEngineBullet500Drag.rpm1 == 5500) SoundEngineBullet500Drag.rpm1 = 5000;
            else SoundEngineBullet500Drag.rpm1 = 5500;
    //        Debug.Log("RPM =");
    //        Debug.Log(SoundEngineBullet500Drag.rpm1);
            //          else if (SoundEngineBullet500Drag.rpm1 == 14000) SoundEngineBullet500Drag.rpm1 = 15000;
            //          }
        }

    }


    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineBullet500Drag.ReverbXoneMix;
    }

}
