using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterVespa2s : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineVespa2s.itrate == 10000)
        {
            //      RPMCal();
            // AudioEngineVespa2s.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineVespa2s.ivibrating == 1)
        {

            //          AudioEngineVespa2s.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (AudioEngineVespa2s.ivibrator == 1)
            //       {
            if (AudioEngineVespa2s.rpm1 == 6000) AudioEngineVespa2s.rpm1 = 6500;
            else AudioEngineVespa2s.rpm1 = 6000;
            Debug.Log("RPM =");
            Debug.Log(AudioEngineVespa2s.rpm1);
            //          else if (AudioEngineVespa2s.rpm1 == 14000) AudioEngineVespa2s.rpm1 = 15000;
            //          }
        }

    }


    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineVespa2s.ReverbXoneMix;
    }

}


