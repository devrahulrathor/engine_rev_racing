﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNFSR15000R35Aft1 : MonoBehaviour
{
    int[] posrpmarrayfsr15000 = { 7500, 6500, 5500, 4400, 4000, 3500, 3300, 3200, 2800, 2500, 2300, 2200, 2200, 2100, 2100, 2000, 2000, 2000, 2000 };


    double[] rpmposarrayfsr15000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR35.itrate == 6100)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
        LowPassFilter();
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineR35.rpm1 = posrpmarrayfsr15000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineR35.rpm1 <= (AudioEngineR35.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }


    public void NFSR15000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        //       audio.PlayScheduled(AudioSettings.dspTime + 0.504F);

        audio.Play();
        //      audio.Play(44100);
    }

    public void NFSR15000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    double Blip15000volume = 1.0;
    public void BLIPCR15000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip15000volume = Blip15000volume - 0.1;
        if (Blip15000volume > 0) audio.volume = (float)Blip15000volume;
        else
        {

            AudioEngineR35.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
        }
    }

    public void NFSR15000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void NFSR15000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}


