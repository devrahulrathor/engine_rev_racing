﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarBackButton : MonoBehaviour {

    // Use this for initialization
    public void BackButton()
    {
        SceneManager.LoadScene(0);  // Motorbike Options Scene
    }

}
