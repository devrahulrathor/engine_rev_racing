using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighSpeedCrashSound : MonoBehaviour
{
    // Start is called before the first frame update
    public void HighSpeedCrashtPlay()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }
    public void HighSpeedCrashStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}
