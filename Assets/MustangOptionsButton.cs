﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MustangOptionsButton : MonoBehaviour
{
    public GameObject MustangLocationsButton;
    public GameObject MustangVehicleButton;
    public GameObject MustangCustomizeButton;


    public GameObject MustangCameraViewButton;



    public int iMustangoptions = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(MustangVehicleOptions.iMustang == 350)
        {
     //       Debug.Log("INside GT350---------------------------");
            MustangCustomizeButton.SetActive(false);
        }
        if (MustangVehicleOptions.iMustang == 500)
        {
       //     MustangCustomizeButton.SetActive(true);
        }
    }

    public void AllOptionButtonsActive()  // 
    {
        if (iMustangoptions == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            MustangLocationsButton.SetActive(true);
            MustangViews.iMsutangthisLocationbutton = 0;
            MustangVehicleButton.SetActive(true);
            MustangVehicleOptions.iMsutangthisVehiclebutton = 0;
            MustangCustomizeButton.SetActive(true);
            MustangCustomizeOptions.iMsutangthisCustomizebutton = 0;
            //      ZX10ROptionButtonBackground.SetActive(true);
            //      MustangCameraViewButton.SetActive(true);
            iMustangoptions = 1;

        }
        else if (iMustangoptions == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            MustangLocationsButton.SetActive(false);
            MustangViews.iMsutangthisLocationbutton = 1;
            MustangVehicleButton.SetActive(false);
            MustangVehicleOptions.iMsutangthisVehiclebutton = 1;
            MustangCustomizeButton.SetActive(false);
            MustangCustomizeOptions.iMsutangthisCustomizebutton = 1;

            //       ZX10ROptionButtonBackground.SetActive(false);
            //       MustangCameraViewButton.SetActive(false);
            iMustangoptions = 0;

        }

    }


}


