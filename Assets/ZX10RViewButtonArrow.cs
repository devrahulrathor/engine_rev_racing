﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10RViewButtonArrow : MonoBehaviour
{
    // Not Being Used
    public GameObject ZX10RGarageButton;
    public GameObject ZX10RRoadButton;
    public GameObject ZX10RTunnelButton;

    public AudioEngine AEngine;
    public int iZX10Rview = 0;   // Appear/Disapperar Counter
    public int iZX10SwipeCounter = 1;   // Decide the View Option Active: 1 Garage Active, 2 Road Active
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

      
    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iZX10SwipeCounter == 1)
        {
            ZX10RRoadButton.SetActive(true);
            ZX10RGarageButton.SetActive(false);
            iZX10SwipeCounter++;
        }
        else if (iZX10SwipeCounter == 2)
        {
            ZX10RRoadButton.SetActive(false);
            ZX10RTunnelButton.SetActive(true);
            iZX10SwipeCounter++;
        }
        else if (iZX10SwipeCounter == 3)
        {
            ZX10RTunnelButton.SetActive(true);
            ZX10RRoadButton.SetActive(false);
            iZX10SwipeCounter--;
        }

    }
}
