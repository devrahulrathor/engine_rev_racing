﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34Views : MonoBehaviour
{
    public GameObject R34GarageButton;
    public GameObject R34RoadButton;
    public GameObject R34TunnelButton;
    public GameObject R34LocationButton;

    int iR34Location = 0;
    public int iR34view = 0;   // Appear/Disapperar Counter
    public static int iMsutangthisLocationbutton = 0;

    public GameObject R34DriveOptions;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Locations()  // 
    {



        if (iR34Location == 0)
        {
            R34GarageButton.SetActive(true);
            R34RoadButton.SetActive(true);
            R34TunnelButton.SetActive(true);
            //       R34DriveOptions.SetActive(false);
            //          R34ViewOptions.SetActive(false);
            iR34Location = 1;
            R34Customizeoptions.iR34thisCustomizebutton = 0;
        }
        else if (iR34Location == 1)
        {

            R34GarageButton.SetActive(false);
            R34RoadButton.SetActive(false);
            R34TunnelButton.SetActive(false);
            //         R34DriveOptions.SetActive(true);
            //        R34ViewOptions.SetActive(true);
            iR34Location = 0;
        }
    }
    public void LocationsDeactivate()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        R34GarageButton.SetActive(false);
        R34RoadButton.SetActive(false);
        R34TunnelButton.SetActive(false);
        //      R34LocationButton.SetActive(false);

        iR34Location = 0;

    }

    public void LocationsButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        //       Debug.Log("ViewsButton=");
        //       Debug.Log(iMsutangthisLocationbutton);
        if (iMsutangthisLocationbutton == 0)
        {
            //         Debug.Log("deactivating Views");
            R34LocationButton.SetActive(false);
            iMsutangthisLocationbutton = 1;
        }
        else if (iMsutangthisLocationbutton == 1)
        {
            //          Debug.Log("Activating Views");
            R34LocationButton.SetActive(true);
            iMsutangthisLocationbutton = 0;
        }
    }


    public void GarageRoadButtonsActive()  // 
    {
        if (iR34view == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            R34GarageButton.SetActive(true);
            R34RoadButton.SetActive(true);


            iR34view = 1;
        }
        else if (iR34view == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            R34GarageButton.SetActive(false);
            R34RoadButton.SetActive(false);
            iR34view = 0;
        }

    }
}



