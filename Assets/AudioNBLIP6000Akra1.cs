﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000Akra1 : MonoBehaviour {


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngine.itrate == 5005)
        {
            AudioEngine.rpm1 = 6000;
            //           Debug.Log(AudioEngine.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngine.rpm1 >= 5000 && AudioEngine.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngine.rpm1 >= 4000 && AudioEngine.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.028F);
        }
        else if (AudioEngine.rpm1 >= 3000 && AudioEngine.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.073F);
        }
        else if (AudioEngine.rpm1 >= 2000 && AudioEngine.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.114F);
        }
        else if (AudioEngine.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.169F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip6000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
   //     audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}

