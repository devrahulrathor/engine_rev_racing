﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourButtonScript : MonoBehaviour {
    public Color altColor = Color.black;
    public Renderer rend;
    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        //Set the initial color (0f,0f,0f,0f)
        rend.material.color = altColor;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
