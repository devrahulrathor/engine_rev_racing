﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuVehiclesScript : MonoBehaviour
{
    public GameObject MotorbikeMenu, CarMenu, RideRaceMenu;
    public GameObject ZX10R, Ninja400, CB400, Jawa2s, Vespa2s, STriple, DragMenu, RideMenu, RaceMenu;
    public GameObject Huracan, Fiesta, Mustang, GTRR34, GTRR35, Corvette;
    public GameObject BackArrow, ForwardArrow, QuitButton;
    public static int iMainMenuVehicles = 1;   // Motorbikes -1, Cars-1000
    int iMainMenuVehicle = 1;   // 11 Motorbikes 1, 21 Motorbikes 2,  10001 Cars 1, 10001 Cars 10002, 20001 Ride Race-1
    int iVehicleMenu;   // To Check if PLayerpref for iMainMenuVehicle exists(first time Gam is installed
    public Button VehicleButton;
  //  public static int iMainMenubuttoncolor;  // 1 for Activated Colour Green, 2 for Deactivated White

    // Use this for initialization
    void Start()
    {
   //    PlayerPrefs.SetInt("iVehiclesMenu", 1);
        VehiclesActivate();
        ChangeButtonColor(1);
    }

    void Update()
    {
        //     GetComponent<Button>().interactable = false;
        //    TurnRed();

    }
        public void NextButton()
    {
        SceneManager.LoadScene(1);  // Car Options Scene
    }


    public void VehiclesActivate()
    {
     //     iMainMenubuttoncolor = 1;
  
        // Checking First Time Games is installed if Playerpref exists so as to load  on Main Menu iVehicleMenu=1
        iVehicleMenu = PlayerPrefs.GetInt("iVehiclesMenu");
        if (iVehicleMenu == 0)
        {
            //Do stuff on the first time
    //        Debug.Log("first run");
            PlayerPrefs.SetInt("iVehiclesMenu", 1);
            //      PlayerPrefs.SetInt("IsFirst", 1);
        }
        else
        {
            //Do stuff other times
     //       Debug.Log("welcome again!");
        }


        //  PlayerPrefs.GetInt("nonconsumableval1");
        //      GetComponent<Button>().interactable = false;  //Deactivate Vehicle Button when pressed

        iMainMenuVehicle = PlayerPrefs.GetInt("iVehiclesMenu");  
        if (iMainMenuVehicle == 1)   // Vehicles Home
        {
            Vehicles1DeActivate();

            MotorbikeMenu.SetActive(true);
            CarMenu.SetActive(true);
 //           RideRaceMenu.SetActive(true);
            QuitButton.SetActive(true);
            BackArrow.SetActive(false);
            ForwardArrow.SetActive(false);

        }
        if (iMainMenuVehicle == 11)  // Motorbikes 1
        {
            //      Debug.Log("iMainMenuVehicle=");
            //     Debug.Log(iMainMenuVehicles);
            Vehicles1DeActivate();
            ZX10R.SetActive(true);
            Ninja400.SetActive(true);
            BackArrow.SetActive(true);
            ForwardArrow.SetActive(true);

        }
        if (iMainMenuVehicle == 21)  // Motorbikes 1
        {
            //      Debug.Log("iMainMenuVehicle=");
            //     Debug.Log(iMainMenuVehicles);
            Vehicles1DeActivate();
            CB400.SetActive(true);
            Jawa2s.SetActive(true);
      //      Ninja400.SetActive(true);
            BackArrow.SetActive(true);
            ForwardArrow.SetActive(true);

        }
        if (iMainMenuVehicle == 31)  // Motorbikes 1
        {
            //      Debug.Log("iMainMenuVehicle=");
            //     Debug.Log(iMainMenuVehicles);
            Vehicles1DeActivate();
            Vespa2s.SetActive(true);
            STriple.SetActive(true);
      //      YamahaR1.SetActive(true);
            //      Ninja400.SetActive(true);
            BackArrow.SetActive(true);
            //        ForwardArrow.SetActive(true);

        }

        if (iMainMenuVehicle == 10001)  // Cars 1
        {
            //      Debug.Log("Mustang iMainMenuVehicle=");
            //     Debug.Log(iMainMenuVehicles);
            Vehicles1DeActivate();

            Huracan.SetActive(true);
            Mustang.SetActive(true);

            BackArrow.SetActive(true);
            ForwardArrow.SetActive(true);   // Deactivaed for now till Cars are beyond first screen
        }
        if (iMainMenuVehicle == 10011)  // Cars 2
        {
                Debug.Log("iMainMenuVehicle=");
                 Debug.Log(iMainMenuVehicles);
            Vehicles1DeActivate();
   //         Fiesta.SetActive(true);
            GTRR35.SetActive(true);
            GTRR34.SetActive(true);
            ForwardArrow.SetActive(true);
            BackArrow.SetActive(true);
            //       ForwardArrow.SetActive(true);   // Deactivaed for now till Cars are beyond first screen
        }
        if (iMainMenuVehicle == 10021)  // Cars 2
        {
            Debug.Log("iMainMenuVehicle=");
            Debug.Log(iMainMenuVehicles);
            Vehicles1DeActivate();
            Corvette.SetActive(true);
                  Fiesta.SetActive(true);
          

            BackArrow.SetActive(true);
            //       ForwardArrow.SetActive(true);   // Deactivaed for now till Cars are beyond first screen
        }
        if (iMainMenuVehicle == 20001)  // RideRace Opening Screen
        {
                 Debug.Log("iMainMenuVehicle=");
               Debug.Log(iMainMenuVehicle);
            Vehicles1DeActivate();

            DragMenu.SetActive(true);
     //       Fiesta.SetActive(true);
            BackArrow.SetActive(true);
            //       ForwardArrow.SetActive(true);   // Deactivaed for now till Cars are beyond first screen
        }

    }

    public void Vehicles1DeActivate()
    {
        MotorbikeMenu.SetActive(false);
        CarMenu.SetActive(false);
        RideRaceMenu.SetActive(false);
        ZX10R.SetActive(false);
        Ninja400.SetActive(false);
        CB400.SetActive(false);
        Jawa2s.SetActive(false);
        Vespa2s.SetActive(false);
        STriple.SetActive(false);
        Huracan.SetActive(false);
        Fiesta.SetActive(false);
        Corvette.SetActive(false);
        Mustang.SetActive(false);
        GTRR34.SetActive(false);
        GTRR35.SetActive(false);
        DragMenu.SetActive(false);

        BackArrow.SetActive(false);
        ForwardArrow.SetActive(false);
        QuitButton.SetActive(false);
     
        //    GetComponent<Button>().colour = true;  //Activate Vehicle Button when pressed
    }

    public void VehiclesDeActivate()
    {
        MotorbikeMenu.SetActive(false);
        CarMenu.SetActive(false);
        RideRaceMenu.SetActive(false);
        ZX10R.SetActive(true);
        Ninja400.SetActive(true);
    }

    public void MotorBikes1DeActivate()
    {

    //    ZX10R.SetActive(false);
    //    Ninja400.SetActive(false);
    }

    public void BackButton()  // From Successive Vehicle Menus
    {
        iMainMenuVehicle = PlayerPrefs.GetInt("iVehiclesMenu");
        if (iMainMenuVehicle > 0 && iMainMenuVehicle < 10000)
        {
            iMainMenuVehicle = iMainMenuVehicle - 10;

        }
        else if (iMainMenuVehicle == 10001) iMainMenuVehicle = 1;
        else if (iMainMenuVehicle > 10001 && iMainMenuVehicle < 20000)
        {
            iMainMenuVehicle = iMainMenuVehicle - 10;
        }
        //       else if (iMainMenuVehicle > 15000 && iMainMenuVehicle < 20000)
        //       {
        //           iMainMenuVehicle = iMainMenuVehicle - 10000;
        //       }
        else if (iMainMenuVehicle > 20000)
        {

            iMainMenuVehicle = iMainMenuVehicle - 20000;
        }
        PlayerPrefs.SetInt("iVehiclesMenu", iMainMenuVehicle);
        VehiclesActivate();
        Debug.Log("Back Main Menu = ");
        Debug.Log(iMainMenuVehicle);
        //    ZX10R.SetActive(false);
        //    Ninja400.SetActive(false);
    }

    public void ForwardButton()  // From Successive Vehicle Menus
    {
        iMainMenuVehicle = PlayerPrefs.GetInt("iVehiclesMenu");
        if (iMainMenuVehicle < 15000)
        {
            iMainMenuVehicle = iMainMenuVehicle + 10;
     //       Debug.Log("Imainvehicle=");
     //       Debug.Log(iMainMenuVehicle);
        }
        if (iMainMenuVehicle > 15000)
        {
            iMainMenuVehicle = iMainMenuVehicle - 10000;
        }
        PlayerPrefs.SetInt("iVehiclesMenu", iMainMenuVehicle);
        VehiclesActivate();
        Debug.Log("Forw Main Menu = ");
        Debug.Log(iMainMenuVehicle);
        //    ZX10R.SetActive(false);
        //    Ninja400.SetActive(false);
    }

    public void ChangeButtonColor(int a)
    {
    
        ColorBlock colors = VehicleButton.colors;
        //          colors.normalColor = new Color32(0, 219, 213, 255);
        if (a == 1)
        {
            Debug.Log("Change Colours to Green");
            colors.normalColor = new Color32(0, 219, 213, 255);  // Green Colour
            colors.highlightedColor = new Color32(0, 219, 213, 255);
            colors.pressedColor = new Color32(0, 219, 213, 255);
            colors.selectedColor = new Color32(0, 219, 213, 255);
        }
        else if (a == 2)
        {
            Debug.Log("Change Colours to White");
            colors.normalColor = new Color32(225, 225, 225, 255);  //White Colour
            colors.highlightedColor = new Color32(225, 225, 225, 255);
            colors.pressedColor = new Color32(225, 225, 225, 255);
            colors.selectedColor = new Color32(225, 225, 225, 255);
        }
        //   colors.normalColor = new Color(0, 0.2f, 0.5f, 0.8f);
        //      colors.highlightedColor = new Color32(0, 219, 150, 255);
        VehicleButton.colors = colors;
    }


}