﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34PartsOptions : MonoBehaviour
{
    public GameObject R34ExhaustButton;
    public GameObject R34ExhaustButtonAft1;
    public GameObject R34ExhaustButtonTit;
    public GameObject R34TurbochargerButton;

    int iR34Exhausts = 0;

    public void Exhausts()  // 
    {


        Debug.Log("Exhaust");
        if (iR34Exhausts == 0)
        {
            R34ExhaustButton.SetActive(true);
            R34ExhaustButtonAft1.SetActive(true);
            R34ExhaustButtonTit.SetActive(true);
            R34TurbochargerButton.SetActive(true);

            //       R34DriveOptions.SetActive(false);
            //          R34ViewOptions.SetActive(false);
            iR34Exhausts = 1;
            R34Customizeoptions.iR34thisCustomizebutton = 0;
        }
        else if (iR34Exhausts == 1)
        {

            R34ExhaustButton.SetActive(false);
            R34ExhaustButtonAft1.SetActive(false);
            R34ExhaustButtonTit.SetActive(false);
            R34TurbochargerButton.SetActive(false);

            //         R34DriveOptions.SetActive(true);
            //        R34ViewOptions.SetActive(true);
            iR34Exhausts = 0;
        }
    }



    public void ExhaustsDeactivatefromOptions()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        R34ExhaustButton.SetActive(false);
        R34ExhaustButtonAft1.SetActive(false);
        R34ExhaustButtonTit.SetActive(false);
        R34TurbochargerButton.SetActive(false);
        //       R34Akra1CarbonExhaust.SetActive(false);
        //           ZX10RLocationOptions.SetActive(true);

        iR34Exhausts = 0;
    }



}
