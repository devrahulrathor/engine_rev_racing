﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34ExhaustOptions : MonoBehaviour
{
    public GameObject R34BuyStockExhaustButton;
    public GameObject R34RentStockExhaustButton;
   
    public static int iR34ExhaustBuyRentbuttons = 0;
    //   public GameObject R34GT350Button;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void R34StockExhaustBuyRentButtonsActivate()  // 
    {

        if (AudioEngineR34.R34StockPurchased != 1)
        {
            if (iR34ExhaustBuyRentbuttons == 0)
            {
                R34BuyStockExhaustButton.SetActive(true);
                R34RentStockExhaustButton.SetActive(true);

                iR34ExhaustBuyRentbuttons = 1;
            }
            else if (iR34ExhaustBuyRentbuttons == 1)
            {
                R34BuyStockExhaustButton.SetActive(false);
                R34RentStockExhaustButton.SetActive(false);

                iR34ExhaustBuyRentbuttons = 0;
            }
        }

    }


    public void R34StockExhaustBuyRentButtonsDeActivate()  // 
    {
        R34BuyStockExhaustButton.SetActive(false);
        R34RentStockExhaustButton.SetActive(false);
        iR34ExhaustBuyRentbuttons = 0;
    }
    }
