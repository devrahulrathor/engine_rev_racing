﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiestaPopsCracklesMenu : MonoBehaviour
{
    int iFiestaPopsCrackles = 0;
    // Other Game Options Appear/Disappear
//    public GameObject FiestaExhaustOptions;
    public GameObject FiestaLocationOptions;
  //  public GameObject FiestaViewsButton;  // Deactivate Exhaust Button when Console View

    public GameObject PopsCracklesButton1;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void FiestaRPopsCracklesMenu()
    {
        if (iFiestaPopsCrackles == 0)
        {
            PopsCracklesButton1.SetActive(true);
            //        DefaultView.SetActive(true);
            //        ConsoleView.SetActive(true);

   //         FiestaExhaustOptions.SetActive(false);
            FiestaLocationOptions.SetActive(false);
   //         FiestaViewsButton.SetActive(false);
            iFiestaPopsCrackles = 1;
        }
        else if (iFiestaPopsCrackles == 1)
        {
            PopsCracklesButton1.SetActive(false);
            //          DefaultView.SetActive(false);
            //          ConsoleView.SetActive(false);

     //       FiestaExhaustOptions.SetActive(true);
            FiestaLocationOptions.SetActive(true);
      //      FiestaViewsButton.SetActive(true);
            iFiestaPopsCrackles = 0;
        }
    }
    public void FiestaPopsCracklesDropDownDeactivate()  // From Main Options Button on top
    {
        PopsCracklesButton1.SetActive(false);
        iFiestaPopsCrackles = 0;
    }

}
