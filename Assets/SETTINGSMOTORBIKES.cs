using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SETTINGSMOTORBIKES : MonoBehaviour
{

    public GameObject SettingsWindow;
    public GameObject ExhaustPopOnTick, ExhaustPopOffTick;
    public GameObject ArrowKeysTick, TiltControlTick;
     public BikeControllerZX25 BikeController;
   int iSettingsDropDown = 0;  //1- Dropped Down, 0-Up 
    // Settings
    int iTurnControl = 1;  // 1-Arrow Turn COntrol, 2-Tilt Accelerometer Control
    int iPops = 0;  //1-Pops Enabled, 0 Disabled
    // Start is called before the first frame update
    void Start()
    {
       // Settings Reinstate to what 's done in Main Menu Settings
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Race-EngineRevScene")
        {
            Debug.Log("POPS Enabled 1");
            MainControllerDrag.iPopCrackle = PlayerPrefs.GetInt("Pops");
// PopS
            if (PlayerPrefs.GetInt("Pops") == 0) iPops = 1;
            else iPops = 0;
            ExhaustPopsS();
            // Direction COntrol 
       //     PlayerPrefs.SetInt("TurnControl", 1);
            iTurnControl = PlayerPrefs.GetInt("TurnControl");
            if ( PlayerPrefs.GetInt("TurnControl") == 1) iTurnControl = 2 ;  // Accelerometer Tilt Enabled
             else iTurnControl = 1;
            TurnControlSelection();

        }


        //    DontDestroyOnLoad(this.gameObject);
    }

public void SettingsDroopDown()
    {
        if(iSettingsDropDown == 0)
        {
            SettingsWindow.SetActive(true);
            iSettingsDropDown = 1;   
        }
        else if (iSettingsDropDown == 1)
        {
         //   SettingsWindow.SetActive(false);
         //   iSettingsDropDown = 0;
        }
    }
public void SettingsDropDownCancel()
    {
        if (iSettingsDropDown == 1)
        {
          SettingsWindow.SetActive(false);
            iSettingsDropDown = 0;
        }
    }

    // Settings

    // Turn Control
    public void TurnControlSelection()
    {
        if (iTurnControl == 1)
        {
            Debug.Log("Tilt Turn");
            PlayerPrefs.SetInt("TurnControl", 2);  // Accelerometer Tilt Enabled
            TiltControlTick.SetActive(true);
            ArrowKeysTick.SetActive(false);
            iTurnControl = 2;
        }
        else if (iTurnControl == 2)
        {
            Debug.Log("Arrow Turn");
            PlayerPrefs.SetInt("TurnControl", 1);  // Steering Buttons Enabled
            TiltControlTick.SetActive(false);
            ArrowKeysTick.SetActive(true);
            iTurnControl = 1;
            BikeController.iaccelerometeron = 1;
        }
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Race-EngineRevScene") BikeController.SteeringControlToggle();
    }

    // Exhaust POPS
    public void ExhaustPopsS()
    {
        Debug.Log("POPS Enabled -1");
        if (iPops == 0)
        {
            Debug.Log("POPS Enabled 0");
            PlayerPrefs.SetInt("Pops", 1);  // Steering Buttons Enabled
            Scene scene = SceneManager.GetActiveScene();
            if (scene.name == "Race-EngineRevScene")
            {
                Debug.Log("POPS Enabled 1");
                MainControllerDrag.iPopCrackle = 1;
            }
            ExhaustPopOnTick.SetActive(true);
            ExhaustPopOffTick.SetActive(false);

            iPops = 1;
        }
        else if (iPops == 1)
        {
            PlayerPrefs.SetInt("Pops", 0);  // Steering Buttons Enabled
            Scene scene = SceneManager.GetActiveScene();
            if (scene.name == "Race-EngineRevScene")
            {
                Debug.Log("POPS DisEnabled");
                MainControllerDrag.iPopCrackle = 0;
            }
            ExhaustPopOnTick.SetActive(false);
            ExhaustPopOffTick.SetActive(true);
            iPops = 0;
        }

    }

}
