﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScriptNinja400Crackle8000 : MonoBehaviour
{
    //  public static float dtcrackle8000 = 0.75f;  // Time lapse after FSR after which Crackle starts
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Crackle8000()
    {
        //      StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayScheduled(AudioSettings.dspTime + AudioEngineNinja250.dtpop);
        //       audio.Play();
        audio.Play(44100);
    }

    public void Crackle8000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Crackle8000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineS1000RRAkra.ReverbXoneMix;
    }
}
