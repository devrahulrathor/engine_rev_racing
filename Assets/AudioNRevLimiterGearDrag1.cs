﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterGearDrag1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineDrag.itrate == 10000)
        {
            //      RPMCal();
            // AudioEngineDrag.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop Gear");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineDrag.ivibrating == 1)
        {
            //          AudioEngineDrag.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (AudioEngineDrag.ivibrator == 1)
            //       {
            if (AudioEngineDrag.rpm1 == 15000) AudioEngineDrag.rpm1 = 14000;
            else AudioEngineDrag.rpm1 = 15000;
            //          else if (AudioEngineDrag.rpm1 == 14000) AudioEngineDrag.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //      audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineDrag.ReverbXoneMix;
    }
}