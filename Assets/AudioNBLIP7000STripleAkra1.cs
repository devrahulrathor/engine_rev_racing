using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000STripleAkra1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (AudioEngineSTriple.itrate == 5006)
        {
            AudioEngineSTriple.rpm1 = 7000;
            //           Debug.Log(AudioEngineSTriple.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineSTriple.rpm1 >= 6000 && AudioEngineSTriple.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (AudioEngineSTriple.rpm1 >= 5000 && AudioEngineSTriple.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.045F);
        }
        else if (AudioEngineSTriple.rpm1 >= 4000 && AudioEngineSTriple.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.089F);
        }
        else if (AudioEngineSTriple.rpm1 >= 3000 && AudioEngineSTriple.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.089F);
        }
        else if (AudioEngineSTriple.rpm1 >= 2000 && AudioEngineSTriple.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.146F);
        }


        else if (AudioEngineSTriple.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.291F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //      audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}

