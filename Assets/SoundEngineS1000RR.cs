using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;

public class SoundEngineS1000RR : MonoBehaviour
{
    double startTick;
    public MainControllerS1000RR MCS1000RR;
    public SoundMethodsS1000RR SMS1000RR;
    public GameObject HuracanSound;
    public GameObject AventadorSound;

    public static float startingtime= 1.4f;   

    //   public int ikeyin = 0;    // Ignition Key in Counter
    //   public static int ikeyinfromtouch = 0;
    public static int istart = -100;    // Start Counter
 //   public static int istartfromtouch = 0;
 //   public int istartdisable = 0; // Disable for 1.5sec after first start, 0-enabled, 1-disabled
  public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs                                                           
 //   public int iRPMIdleSettle = 0; // Counter for Console RPM Warm up and settle-0-3000-Idling RPM

 //   public Slider mSlider;  // Slider Spring Back Variable


    public static int idlingvol = 1;  // Counter to set idling volume 1 when engine starting 
    public static int idlingvolsport = 0;  // Counter to set idling volume(sport Mode) 1 when engine starting 

  //  public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = 0;
    public static int iblipcanstart = 1;  // Counter for Blip Start
    public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 0;

 //   int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 2000;
    public float rpmdisplayafterstart = 1.4f;  // Time in seconds after which Idling RPM start post Startup
    public static int rpmblipconst;  // TO Stop Blip at different RPMs
    public float blipcinterval = 0;  // Time after which BlipC has to stop
                                     //    public Text RPMText;     // RPMs outputted to Canvas//
    public float sliderval;
    public float slidervalold;

    // Fade in out counters
    public static int ifadeoutstarting = 1;  // Fade out Starting
    public float fadeoutstartingtime = 2f;  // Fade out Duration

    public static int ifadeoutnfsr = 0, ifadeinnfsr = 0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeinidlingtime = 0;  // Fade in Duration
    public float fadeinidlingdelaytime = 0;  // Delay before Fade in Starts(When Starting/IdlingFade this is 2 seconds so fade in ends around 2.5 sec)
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)
    public float fadeoutstatingtime = 0;  // Fade out Duration

    public static int ifadeinidling = 0, ifadeoutidling = 0;
    public static int ifadeinidlingsport = 0, ifadeoutidlingsport = 0;
    public static int ifadeoutsli = 0, ifadeoutfsi1 = 0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters
    
    public static float ReverbXoneMix = 1.09f;   // Reverberation Value
   



    //Backfire Section
    public static float dtcrackle = 0.4f;  // Delay on Backfire dtcrackle8000
    public ExhaustPopHuracan AVExhaustPop;
    public ExhaustPopHuracan1 AVExhaustPop1;
    public ExhaustPopHuracan2 AVExhaustPop2;
    public ExhaustPopHuracan3 AVExhaustPop3;
    public LighExhaustPop LightExhaustPop;
    public TailLightLeftEmission TailLEmission;
    public TailLightRightEmission TailREmission;


    public AudioStartupS1000RRAkra AStartup;
    public AudioStartingS1000RRAkra AStarting;
    public AudioShuttingS1000RRAkra AShutting;

    public AudioIdlingS1000RRAkra AIdling;
    public AudioIdlingSportS1000RRAkra AIdlingSport;
    public AudioNBLIPS1000RRAkra ANBLIP;
    public AudioNBLIP2000S1000RRAkra ANBLIP2000;
    public AudioNBLIP3000S1000RRAkra ANBLIP3000;
    public AudioNBLIP4000S1000RRAkra ANBLIP4000;
    public AudioNBLIP5000S1000RRAkra ANBLIP5000;
    public AudioNBLIP6000S1000RRAkra ANBLIP6000;
    public AudioNBLIP7000S1000RRAkra ANBLIP7000;
    public AudioNBLIP10000S1000RRAkra ANBLIP10000;
    public AudioNBLIP14000S1000RRAkra ANBLIP14000;

    public AudioNBLIPC3000S1000RRAkra ANBLIPC3000;
    public AudioNBLIPC4000S1000RRAkra ANBLIPC4000;
    public AudioNBLIPC5500S1000RRAkra ANBLIPC5500;
    public AudioNBLIPC6000S1000RRAkra ANBLIPC6000;
    public AudioNBLIPC7500S1000RRAkra ANBLIPC7500;
    public AudioNBLIPC10000S1000RRAkra ANBLIPC10000;
    public AudioNBLIPC14000S1000RRAkra ANBLIPC14000;
    public AudioNRevLimiterS1000RRAkra ANRevLimiter;
    public AudioNFSR15000S1000RRAkra ANFSR15000;
    public AudioNFSR7500 ANFSR7500;
    public AudioNFSRS1000RRAkra ANFSR;

    public AudioNSLIS1000RRAkra ANSLI;
    public AudioNFSI1S1000RRAkra ANFSI1;

    //Store Section
  //  public static int HuracanSportMode = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs


    // Headlight Object
    public HeadlightHuracan Hlight;
    public TailLightsHuracan Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 


    // Paid Options
    public AudioScriptCrackle8000S1000RR ACrackle8000;  // Crackle 8000
    public AudioScriptCrackle6000S1000RR ACrackle6000;  // Crackle 6000
    public AudioScriptCrackle4000S1000RR ACrackle4000;  // Crackle 4000
                                                        //  public static int MainControllerS1000RR.icrackle = 0, 

 

    public int icrackleold = 0;                            // Enable Crackle 

    int ineedle = 0;
 //   int[] rpmidlesettleed = { 500, 800, 1000, 1500, 1800, 2000, 2200, 2500, 2800, 3000, 3200, 3000, 2800, 2800, 2500, 2500, 2200, 2200, 2200, 2000, 2000, 2000, 2000 };  // RPM Vlues for RPM settle down

    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No


    //RateUs Variables
    public int irevcountrateus = 0;  // Counter to decide when user rates when the count is greated than irevcountmaxrateus
    public int irevcountmaxrateus = 50;
    int irateusloopdisable = 0;
    int irateusdialogdisable = 0;
  //  [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, ModeSport, ModeNormal, RPM0, RPM200, RPM500, RPM800, RPM1000, RPM1100, RPM1500, RPM1800, RPM2000, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //      Debug.Log("@@@@@@@@@@@@@@@@@@ RPM in Main Controller 1 =");
        //      Debug.Log(rpm1);
        if (MainControllerS1000RR.ivehicle == 1)
        {
            HuracanSound.SetActive(true);
            AventadorSound.SetActive(false);
          //  rpmidling = 2000;
        }
        if (MainControllerS1000RR.ivehicle == 2)
        {
            HuracanSound.SetActive(false);
            AventadorSound.SetActive(true);
        //    rpmidling = 2000;
        }
        // Throttle  Events
        if (istart == 1 && gear == 0)
        {

            trate = (MainControllerS1000RR.sliderval - slidervalold);

        //    throttlediff = (MainControllerS1000RR.sliderval * 250 - MainControllerS1000.rpm1);
            //    trate = (sliderval - slidervalold);
            //              throttlediff = (sliderval * 150 - rpm1);
              throttlediff = (MainControllerS1000RR.sliderval * 250 - (MainControllerS1000RR.rpm1 - 1));
      //      Debug.Log("Trate = ");
      //      Debug.Log(trate);
            // Mode Change Idling Changei

            if ((itrate == 1) && MainControllerS1000RR.icrackle != icrackleold)   //then change idling sound
            {
                idlingvol = 1;
                Debug.Log("Dynamic Idling change--------------------------------------");
                if (MainControllerS1000RR.icrackle == 1)
                {
                    //        AIdling.IdlingStop();
                    ifadeoutidling = 1;
                    StartCoroutine("FadeoutIdlingModeChange");

                    ifadeinidling = 1;
                    idlingvolsport = 0;
                    StartCoroutine("FadeinIdlingModeChange");
                    Debug.Log("fadein Idling 0000000000000000000000000000000");
                    AIdlingSport.Idling();
                }
                else if (MainControllerS1000RR.icrackle == 0)
                {
                    //          AIdlingSport.IdlingStop();
                    ifadeoutidling = 1;
                    StartCoroutine("FadeoutIdlingModeChange");

                    ifadeinidling = 1;
                    idlingvol = 0;
                    StartCoroutine("FadeinIdlingModeChange");
                    AIdling.Idling();
                    Debug.Log("fadein Idling 111111111111111111111111111111");

                }
            }
            if (trate > 0)
            {
                //           Debug.Log("Trate=");
                //           Debug.Log(trate);

                //            Debug.Log("RPM=");
                //            Debug.Log(rpm1);
            }
    //        Debug.Log("@@@@@@@@@@@@@@@@@@ RPM in Main Controller 2 =");
    //        Debug.Log(rpm1);
            // SLI Module
            //               if(((trate > 0 && throttlediff > 0) && itrate == 1) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 ||itrate == 6006 || itrate == 6005  || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0))  {
            //               if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 12000))
            if (((trate > 0 && throttlediff > 0) && itrate == 1 && rpm1 < 12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999) && trate > 0 && throttlediff > 0 && rpm1 < 6000))  // Modified for Car

            {
                //               Debug.Log("Inside SLI______________________________________");
                if (MainControllerS1000RR.ivehicle == 1)
                {
                    ANSLI.NSLI();
                    ANFSR.FSRStop();
                    ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                else if(MainControllerS1000RR.ivehicle == 2)
                {
                    SMS1000RR.SLI();
                }
                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");

                if (itrate == 5001)
                {
                    //           ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
                    //           StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 2;    // Counter for FSI to start
                irpm = 1;      // Counter for RPM 
                fadeinidlingdelaytime = 0f;   // Delay before Fade in Idling Starts is 0 Unlike 2 secs at Starting
            }

            // FSI1 Module
           //    if (((trate > 2 || throttlediff > 2000 || rpm1 > 15500) && itrate == 2 && rpm1 < -12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < -5000))
                if (((trate < -2 || throttlediff > 20000 || rpm1 > 15500) && itrate == 2 && rpm1 < -12000) || ((itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002) && trate > 0 && throttlediff > 0 && rpm1 < -5000))

                // above FSI1 starts after rpm > 45000//

                {
                    Debug.Log("Entering FSI1_________________________________________");
                ANFSI1.NFSI1();
                ANSLI.NSLIStop();
                ANFSR.FSRStop();
                ANFSR15000.NFSR15000Stop();
                ANBLIPC14000.BLIPC14000Stop();
                ANBLIPC10000.BLIPC10000Stop();
                ANBLIPC7500.BLIPC7500Stop();
                ANBLIPC6000.BLIPC6000Stop();
                ANBLIPC5500.BLIPC5500Stop();
                ANBLIPC4000.BLIPC4000Stop();
                ANBLIPC3000.BLIPC3000Stop();

                ifadeoutidling = 1;
                fadeoutidlingtime = 0.01f;
                StartCoroutine("FadeoutIdling");
                if (itrate == 5001)
                {
                    //        ifadeoutnfsr = 1;
                    //             StartCoroutine("FadeoutNFSR");
                    //        StartCoroutine("FadeoutBLIPCR7500");
                }
                //       AIdling.IdlingStop(); // Stop Idling

                itrate = 3;    // Counter for FSI to start
                irpm = 2;      // Counter for RPM 

            }



            // Blip Module
            //    if ((trate > 10 || throttlediff > 12000) && throttlediff > 0  && rpm1 < 11000 && iblipcanstart == 1 && istart == 1)
            if ((trate > 10 || throttlediff > 8000) && throttlediff > 0 && rpm1 < 6000 && iblipcanstart == 1 && istart == 1)
              
                {


                Debug.Log("Inside Blip");
                Debug.Log("iblipcanstart=");
                Debug.Log(iblipcanstart);
                //    Debug.Log("RPM=");
                //    Debug.Log(rpm1);

                startTick = AudioSettings.dspTime;

                iblipcanstart = 0;
                irevlimitercanstart = 1;
                idlingvol = 0;   // Making Idling vol 0 so no silence upon start and idling
                if (MainControllerS1000RR.ivehicle == 1)
                {
                    ANBLIP.Blip();
                    ANBLIP2000.Blip2000();
                    ANBLIP3000.Blip3000();
                    ANBLIP4000.Blip4000();
                    ANBLIP5000.Blip5000();
                    ANBLIP6000.Blip6000();
                    ANBLIP7000.Blip7000();
                    //          ANBLIP10000.Blip10000();
                    //          ANBLIP14000.Blip14000();
                    //         ANFSR15000.NFSR15000();

                    //           ANBLIPC3000.BLIPC3000();

                    
                    AIdling.IdlingStop(); // Stop Idling
                    AIdlingSport.IdlingStop();
                    SMS1000RR.IdlingStop();
                    //         ifadeoutidling = 1;
                    //         fadeoutidlingtime = 0.01f;
                    //         StartCoroutine("FadeoutIdling");
                    ANSLI.NSLIStop();
                    ANFSI1.NFSI1Stop();
                    //        ifadeoutsli = 1;
                    //        ifadeoutfsi1 = 1;
                    //         StartCoroutine("FadeoutNSLI");
                    //         StartCoroutine("FadeoutNFSI1");
                    //        ifadeoutnfsr = 1;
                    //        fadeoutfsrtime = 0.005f;
                    //          FadeoutNFSR();



                    //           ANFSR.FSRStop();

                    //         ANFSR15000.NFSR15000Stop();
                    ANBLIPC14000.BLIPC14000Stop();
                    ANBLIPC10000.BLIPC10000Stop();
                    ANBLIPC7500.BLIPC7500Stop();
                    ANBLIPC6000.BLIPC6000Stop();
                    ANBLIPC5500.BLIPC5500Stop();
                    ANBLIPC4000.BLIPC4000Stop();
                    ANBLIPC3000.BLIPC3000Stop();
                }
                else if (MainControllerS1000RR.ivehicle == 2)
                {
                    SMS1000RR.Blip();
                }

                //            NSLIAtrigger.SLIStop();

                //               Debug.Log("Blip ***************************************Blip");
                // Stop SLI
                // Stoo SLR
               idlingvol = 0;
                StopCoroutine("Fadeoutidling");  // Avoid Idling Fading out upon restartidling when quick blipping  ** New** for Corvette bug at Blip3000
                StopCoroutine("Fadeinidling");  // Avoid Idling Fading out upon restartidling when quick blipping  ** New** for Corvette bug at Blip3000


                itrate = 5000;  // Blip itrate
                fadeinidlingdelaytime = 0f;   // Delay before Fade in Idling Starts is 0 Unlike 2 secs at Starting
                                              //         irpm = 5000;
                                              //           rpmdisplay = rpm1;
            }

            if (((iblipcanstart == 0 && irevlimitercanstart == 1) || (itrate == 2 || itrate == 3)) && rpm1 >= 8000)

            //           if (iblipcanstart == 0 && irevlimitercanstart == 1 && rpm1 >= 14000 )
            {
                itrate = 10000;  // pre rev limit Trate
                                 //          EnableFSR15000afterRevlimiterStart();
                irevlimitercanstart = 0;
                ivibrating = 1;
                rpm1 = 8500;  // Initial RPM before Revlimiter Coroutine to fluctuate
                if (MainControllerS1000RR.ivehicle == 1)
                {
                    ANRevLimiter.RevLimiter();
                    ANBLIP14000.Blip14000Stop();
                    ANBLIP.BlipStop();
                }
                else if (MainControllerS1000RR.ivehicle == 2)
                    {
                    SMS1000RR.RevLimiter();
                    SMS1000RR.SLIStop();
                    }

                // Mobile Vibration
                //            ivibrating = 1;
                //            rpm1 = 15000;  // Initial RPM before Revlimiter Coroutine to fluctuate
                //          Handheld.Vibrate();
                StartCoroutine("RevLimiterVibrationOnOff");

            }

            if (((trate < 0 || throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            //           if (((trate < 0 && throttlediff < 0) && (itrate == 5000 || itrate == 10000) && rpm1 > 2000))
            //    Defunct          if (((trate < 0 || throttlediff < 0) && itrate == 5000 && rpm1 > 2000) || (itrate == 5000 && rpm1 >= 9000))
            {
          //     Debug.Log("Inside BLipCs**********************************************");
                itrate = 5001;  // Counter for BlipC
                                //     itrate = -1;
                                //         iblipcanstop = 1;
                                //     }
                                //     if(iblipcanstop == 1) {

                startTick = AudioSettings.dspTime - startTick;
                //              Debug.Log("Delta Time=");
                //              Debug.Log(startTick);


                //                  InvokeRepeating("FadeoutBLIP", 0f, 0.01f);
                //                 StartCoroutine("Cancelinvoke"); 
                //             ANBLIP.BLIPFadeout();
                //             ifadeoutblip = 1;
                //             StartCoroutine("FadeoutBLIP");
                //          ANBLIP.BlipStop();

                //             if ((rpm1 > 2500 && rpm1 <= 3300) || (rpm1 > 3800 && rpm1 <= 4300) || (rpm1 > 5000 && rpm1 <= 5500) || (rpm1 > 5800 && rpm1 <= 6300) || (rpm1 > 6800 && rpm1 <= 7500))
                //             {

                //            Debug.Log("Inside trate < 0, rpm=");
                //            Debug.Log(rpm1);

                if (MainControllerS1000RR.ivehicle == 1)  // Huracan
                {
                    if (rpm1 >= 8000 && rpm1 <= 15000)
                    {
                        Debug.Log("Inside Revlimiter___++++");
                        itrate = 6100;
                        ANRevLimiter.RevLimiterStop();
                        ivibrating = 0; // Stop Vibrating after Rev Limter Stops

                        ANFSR15000.NFSR15000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP6000.Blip6000Stop();

                        if (MainControllerS1000RR.icrackle == 1)
                        {// Paid up Crackle Option
                         //          Debug.Log("MainControllerS1000RR.icrackle=");
                         //          Debug.Log(MainControllerS1000RR.icrackle);
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                            //      ExplodeinMainController();
                        }

                    }
                    if (rpm1 > 7800 && rpm1 <= -10000)
                    {
                        itrate = 6008;
                        //            iblipcanstop = 0;
                        ANRevLimiter.RevLimiterStop();
                        ivibrating = 0; // Stop Vibrating after Rev Limter Stops

                        ANBLIPC14000.BLIPC14000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();

                        //       iblipcanstop = 0;
                    }



                    if (rpm1 > 7800 && rpm1 <= -7900)
                    {
                        itrate = 6007;
                        //            iblipcanstop = 0;
                        ANRevLimiter.RevLimiterStop();
                        ivibrating = 0; // Stop Vibrating after Rev Limter Stops
                                        //            Debug.Log("Inside 8000-9000");

                        ANBLIPC10000.BLIPC10000();
                        ANBLIP.BlipStop();
                        ANBLIP14000.Blip14000Stop();
                        if (MainControllerS1000RR.icrackle == 1)
                        {// Paid up Crackle Option
                         //          Debug.Log("MainControllerS1000RR.icrackle=");
                         //          Debug.Log(MainControllerS1000RR.icrackle);
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        };

                        //       iblipcanstop = 0;
                    }



                    if (rpm1 > 6000 && rpm1 <= 7200)
                    {
                        //            Debug.Log("Inside 6000-07200");

                        itrate = 6006;

                        //     Debug.Log("itrate=");
                        //       Debug.Log(itrate);
                        ANRevLimiter.RevLimiterStop();
                        ivibrating = 0; // Stop Vibrating after Rev Limter Stops
                        rpmblipconst = rpm1;
                        blipcinterval = 5.347f;

                        ANBLIPC7500.BLIPC7500();


                        //          iblipcanstop = 0;
                        ANBLIP.BlipStop();
                        //                ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        //         ANBLIP7000.Blip7000Stop();
                        ANBLIP14000.Blip14000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP6000.Blip6000Stop();
                        //       iblipcanstop = 0;


                        if (MainControllerS1000RR.icrackle == 1)
                        {// Paid up Crackle Option
                         //        Debug.Log("MainControllerS1000RR.icrackle=");
                         //        Debug.Log(MainControllerS1000RR.icrackle);
                            ACrackle8000.Crackle8000();
                            StartCoroutine("Explode");
                        };
                    }

                    if (rpm1 > 5000 && rpm1 <= 6000)
                    {
                        itrate = 6005;
                        //               Debug.Log("Inside 5000-6000");
                        rpmblipconst = rpm1;
                        blipcinterval = 0.35f;
                        ANBLIPC6000.BLIPC6000();
                        //         iblipcanstop = 0;
                        ANBLIP.BlipStop();
                        //              ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();


                        if (MainControllerS1000RR.icrackle == 1)
                        {// Paid up Crackle Option
                            ACrackle6000.Crackle6000();
                            StartCoroutine("Explode");

                        };

                        //       iblipcanstop = 0;
                    }
                    if (rpm1 > 4100 && rpm1 <= 5000)

                    {

                        //            if ((startTick > 0.214 && startTick < 0.215) || (startTick > 0.221 && startTick < 0.222) || (startTick > 0.227 && startTick < 0.229) || (startTick > 0.234 && startTick < 0.236) || (startTick > 0.24 && startTick < 0.243) || (startTick > 0.247 && startTick < 0.249) || (startTick > 0.252 && startTick < 0.254)) iswitch = 0;
                        //            if (iswitch >= 0)
                        //            {
                        iswitch = 1;
                        itrate = 6004;
                        //              Debug.Log("Inside 4000-5000");
                        rpmblipconst = rpm1;
                        blipcinterval = 0.345f;
                        ANBLIPC5500.BLIPC5500();
                        //            iblipcanstop = 0;
                        ANBLIP.BlipStop();

                        //            ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();


                        if (MainControllerS1000RR.icrackle == 1)
                        {// Paid up Crackle Option
                            ACrackle4000.Crackle4000();
                            StartCoroutine("Explode");
                        };
                        //       iblipcanstop = 0;
                        //          }
                    }
                    //                         if (rpm1 > 3200 && rpm1 <= 4000 && ((startTick < 0.171 || startTick > 0.175) || (startTick < 0.161 || startTick > 0.166) || (startTick < 0.171 || startTick > 0.175)))
                    if (rpm1 > 3000 && rpm1 <= 4100)
                    {

                        //        if ((startTick >= 0.169 && startTick <= 0.17) || (startTick >= 0.178 && startTick <= 0.181) || (startTick >= 0.187 && startTick < 0.189) || (startTick >= 0.196 && startTick <= 0.197) || (startTick >= 0.203 && startTick <= 0.207) || (startTick >= 0.211 && startTick <= 0.215) || (startTick >= 0.217 && startTick <= 0.224)) iswitch = 0;
                        //                  if ((startTick > 0.171 && startTick < 0.175) || (startTick > 0.18 && startTick < 0.185) || (startTick > 0.19 && startTick < 0.192) || (startTick > 0.197 && startTick < 0.2) || (startTick > 0.205 && startTick < 0.209) || (startTick > 0.213 && startTick < 0.215) || (startTick > 0.22 && startTick < 0.223)) iswitch = 1;
                        //       if (iswitch >= 0)
                        //       {
                        iswitch = 1;
                        //              Debug.Log("Inside 3000-4000");
                        itrate = 6003;
                        rpmblipconst = rpm1;
                        blipcinterval = 0.53f;
                        ANBLIPC4000.BLIPC4000();

                        //           iblipcanstop = 0;
                        ANBLIP.BlipStop();


                        //               ANBLIP4000.Blip4000Stop();
                        //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
                        if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();


                        if (MainControllerS1000RR.icrackle == 1)
                        {// Paid up Crackle Option
                            ACrackle4000.Crackle4000();
                            StartCoroutine("Explode");
                        };

                        //       iblipcanstop = 0;
                        //       }
                    }
                    //        if (rpm1 > 320 && rpm1 <= 3200 && startTick > 0.0f )
                    if (rpm1 > 2000 && rpm1 <= 3000)
                    //          if (startTick >= 0.178)
                    {
                        //          if ((startTick >= 0.103 && startTick <= 0.107) || (startTick >= 0.111 && startTick <= 0.114) || (startTick >= 0.125 && startTick <= 0.129) || (startTick >= 0.137 && startTick <= 0.142) || (startTick >= 0.148 && startTick <= 0.152)) iswitch = 0;
                        //             if ((startTick > 0.121 && startTick < 0.122) || (startTick > 0.128 && startTick < 0.131) || (startTick > 0.133 && startTick < 0.135) || (startTick > 0.140 && startTick < 0.143) || (startTick > 0.152 && startTick < 0.156) || (startTick > 0.161 && startTick < 0.166)) iswitch = 1;
                        //         if (iswitch == 0) {
                        iswitch = 1;
                        itrate = 6002;
                        rpmblipconst = rpm1;
                        blipcinterval = 0.35f;
                        ANBLIPC3000.BLIPC3000();

                        //      iblipcanstop = 0;
                        ANBLIP.BlipStop();


                        //                 ANBLIP3000.Blip3000Stop();
                        if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000.Blip4000Stop();
                        ANBLIP5000.Blip5000Stop();
                        ANBLIP6000.Blip6000Stop();
                        ANBLIP7000.Blip7000Stop();
                        ANBLIP10000.Blip10000Stop();
                        ANBLIP14000.Blip14000Stop();

                        //      }

                        //       iblipcanstop = 0;
                    }
                }
                else if (MainControllerS1000RR.ivehicle == 2)   // Aventador
                {
                    if (rpm1 >= 7000 && rpm1 <= 15000)
                    {
                        Debug.Log("Inside BlpC15000------------------");
                        itrate = 6100;
                        SMS1000RR.FSR15000();
                        
                       
                        ivibrating = 0; // Stop Vibrating after Rev Limter Stops
                    }

                    else if (rpm1 >= 7000 && rpm1 < 8000)
                    {
                        Debug.Log("Inside BlpC8000------------------");
                        itrate = 6006;
                        SMS1000RR.FSR8000();
                        

                        //     Debug.Log("itrate=");
                        //       Debug.Log(itrate);
                        ANRevLimiter.RevLimiterStop();
                        ivibrating = 0; // Stop Vibrating after Rev Limter Stops
                        rpmblipconst = rpm1;
                        blipcinterval = 5.347f;
                    }

                    else if (rpm1 > 5000 && rpm1 < 7000)
                    {
                        Debug.Log("Inside BlpC6000------------------");
                        itrate = 6005;
                      SMS1000RR.FSR6000();
                        
                        //               Debug.Log("Inside 5000-6000");
                        rpmblipconst = rpm1;
                        blipcinterval = 0.35f;
                     

                        //       iblipcanstop = 0;
                    }

                   else if (rpm1 > 4100 && rpm1 <= 5000)

                    {
                        Debug.Log("Inside BlpC5000------------------");
                        //            if ((startTick > 0.214 && startTick < 0.215) || (startTick > 0.221 && startTick < 0.222) || (startTick > 0.227 && startTick < 0.229) || (startTick > 0.234 && startTick < 0.236) || (startTick > 0.24 && startTick < 0.243) || (startTick > 0.247 && startTick < 0.249) || (startTick > 0.252 && startTick < 0.254)) iswitch = 0;
                        //            if (iswitch >= 0)
                        //            {
                        iswitch = 1;
                        itrate = 6004;
                      SMS1000RR.FSR5000();
                        //              Debug.Log("Inside 4000-5000");
                        rpmblipconst = rpm1;
                        blipcinterval = 0.345f;
                    
                     
                    }

                    if (rpm1 > 3000 && rpm1 <= 4100)
                    {
                        Debug.Log("Inside BlpC4000------------------");
                        //        if ((startTick >= 0.169 && startTick <= 0.17) || (startTick >= 0.178 && startTick <= 0.181) || (startTick >= 0.187 && startTick < 0.189) || (startTick >= 0.196 && startTick <= 0.197) || (startTick >= 0.203 && startTick <= 0.207) || (startTick >= 0.211 && startTick <= 0.215) || (startTick >= 0.217 && startTick <= 0.224)) iswitch = 0;
                        //                  if ((startTick > 0.171 && startTick < 0.175) || (startTick > 0.18 && startTick < 0.185) || (startTick > 0.19 && startTick < 0.192) || (startTick > 0.197 && startTick < 0.2) || (startTick > 0.205 && startTick < 0.209) || (startTick > 0.213 && startTick < 0.215) || (startTick > 0.22 && startTick < 0.223)) iswitch = 1;
                        //       if (iswitch >= 0)
                        //       {
                        iswitch = 1;
                        //              Debug.Log("Inside 3000-4000");
                        itrate = 6003;
                        SMS1000RR.FSR4000();
                        rpmblipconst = rpm1;
                        blipcinterval = 0.53f;
                        
                        //       iblipcanstop = 0;
                        //       }
                    }
                    if (rpm1 > 2000 && rpm1 <= 3000)
                    //          if (startTick >= 0.178)
                    {
                        Debug.Log("Inside BlpC3000------------------");
                        //          if ((startTick >= 0.103 && startTick <= 0.107) || (startTick >= 0.111 && startTick <= 0.114) || (startTick >= 0.125 && startTick <= 0.129) || (startTick >= 0.137 && startTick <= 0.142) || (startTick >= 0.148 && startTick <= 0.152)) iswitch = 0;
                        //             if ((startTick > 0.121 && startTick < 0.122) || (startTick > 0.128 && startTick < 0.131) || (startTick > 0.133 && startTick < 0.135) || (startTick > 0.140 && startTick < 0.143) || (startTick > 0.152 && startTick < 0.156) || (startTick > 0.161 && startTick < 0.166)) iswitch = 1;
                        //         if (iswitch == 0) {
                        iswitch = 1;
                        itrate = 6002;
                        SMS1000RR.FSR3000();
                        rpmblipconst = rpm1;
                        blipcinterval = 0.35f;

                    
                    }


                }




                StartCoroutine("EnableBlip");  // Enable blip after 200 ms of Blip 
                                               //         StartCoroutine("StartFSR");     // Enable FSR after Blipc ends
                                               //          StartCoroutine("StartFSR7500");     // Enable FSR after Blipc ends
                                               //          }
            }

            // FSR after SLI module
            if ((trate < -0.5 && throttlediff < 0) && (itrate == 2 || itrate == 3) && rpm1 > 2000)
            {
                blipcinterval = 0;
                //           Debug.Log("FSR SLI");
                StartCoroutine("StartFSR");     // Enable FSR after Blipc ends
                //____________________________________
                if (MainControllerS1000RR.icrackle == 1 && rpm1 > 4000)  // Exhaust Pop only beyond RPM 4000
                {// Paid up Crackle Option
                 //          Debug.Log("MainControllerS1000RR.icrackle=");
                 //          Debug.Log(MainControllerS1000RR.icrackle);
                    ACrackle8000.Crackle8000();
                    StartCoroutine("Explode");

               
                }

            }
            // Restart Idling
            if (rpm1 <= (rpmidling + 200) && (itrate == 6100 || itrate == 6009 || itrate == 6008 || itrate == 6007 || itrate == 6006 || itrate == 6005 || itrate == 6004 || itrate == 6003 || itrate == 6002 || itrate == 4999))
            {
                //              Debug.Log("Inside RestartIdling Sound Engine ***************************************8");

                //            iingearrestartidling = 0;
                idlingvol = 0;
                StopCoroutine("Fadeoutidling");  // Avoid Idling Fading out upon restartidling when quick blipping  ** New** for Corvette bug at Blip3000
                StopCoroutine("Fadeinidling");  // Avoid Idling Fading out upon restartidling when quick blipping  ** New** for Corvette bug at Blip3000


                itrate = 1;
                irpm = 0;
                SMS1000RR.Idling();
        //        if (MainControllerS1000RR.ivehicle == 1)
        //        {
        //            AIdling.Idling();
       //             AIdlingSport.Idling();
        //        }
        //        else if(MainControllerS1000RR.ivehicle == 2)
        //        {

        //        }
                ifadeinidling = 1;
                ifadeinidlingsport = 1;
               
                StartCoroutine("FadeinIdling");
                //      StartCoroutine("StartIdling");
              //          ANFSR.FSRStop();
                rpm1 = rpmidling;
          //                     ifadeoutnfsr = 1;
                //             fadeoutfsrtime = 0.01f;
            //           StartCoroutine("FadeoutNFSR");
                //          ANFSR.FSRStop();
                //          ANFSR15000.NFSR15000Stop();
                //          ANBLIPC14000.BLIPC14000Stop();
                //          ANBLIPC7500.BLIPC7500Stop();
                //          ANBLIPC6000.BLIPC6000Stop();
                //          ANBLIPC5500.BLIPC5500Stop();
                //          ANBLIPC4000.BLIPC4000Stop();
                //         ANBLIPC3000.BLIPC3000Stop();
                //     StartCoroutine("Setitrate1");
                Debug.Log("itrate=");
                Debug.Log(itrate);
            }
            

            // RPM Console Update 


            // Idling Pulsating Needle
        }
        //*************************************************************************************
 // Debug.Log("@@@@@@@@@@@@@@@@@@ RPM in Main Controller 3 =");
     
 //       Debug.Log(rpm1);
        slidervalold = MainControllerS1000RR.sliderval;  // Assign Slidervalue to Slidrold
        icrackleold = MainControllerS1000RR.icrackle;
    }

    IEnumerator Startistart1()
    {
        yield return new WaitForSeconds(0.5f);
        istart = 1;
    }


    IEnumerator EnableBlip()
    {
        yield return new WaitForSeconds(0.2f);
        iblipcanstart = 1;
              Debug.Log("Inside Enable Blip");
    }

    // Enable Trate for Revlimiter after 0.1s of Revlimiter Start so Reducing waits for 0.1 s
    private IEnumerator EnableFSR15000afterRevlimiterStart()
    {
        yield return new WaitForSeconds(0.1f);
        itrate = 10000;
    }

    IEnumerator StartFSR()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2 || itrate == 3)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            ifadeinnfsr = 1;
            if(MainControllerS1000RR.ivehicle == 1) ANFSR.FSR();
            else if(MainControllerS1000RR.ivehicle == 2) SMS1000RR.NFSR();
            StartCoroutine("FadeinNFSR");

            //    ANFSR15000.NFSR15000();
            ifadeoutsli = 1;
            ifadeoutfsi1 = 1;
            StartCoroutine("FadeoutNSLI");
            StartCoroutine("FadeoutNFSI1");
            //                ANSLI.NSLIStop();
            //               ANFSI1.NFSI1Stop();
            //       iblipcanstart = 1;

            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option
                if (rpm1 > 2000 && rpm1 < 4000) ACrackle4000.Crackle4000();
                if (rpm1 >= 4000 && rpm1 < 6000) ACrackle6000.Crackle6000();
                if (rpm1 >= 6000) ACrackle8000.Crackle8000();
            };
            itrate = 4999;
            StartCoroutine("EnableBlip");  // Enable blip after 200 ms of Blip New Add in Distinct Sound Module
        }


    }

    IEnumerator StartFSR7500()
    {
        yield return new WaitForSeconds(blipcinterval); // wait half a second
        if (itrate == 5001 || itrate == 2)
        //      if (iblipcanstart == 0 )
        {  // Counter for BlipC
            itrate = 4999;
            ANFSR7500.FSR7500();
            ANSLI.NSLIStop();
            //       iblipcanstart = 1;

        }


    }


    void FadeoutBLIP()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        //      while (ifadeoutblip == 1)
        //      {
        //          Debug.Log("Enter FadeoutSLR1***********************************2");
        //          yield return new WaitForSeconds(0.001f); // wait half a second

        ANBLIP.BLIPFadeout();
        //     }
    }

    IEnumerator Cancelinvoke()
    {


        yield return new WaitForSeconds(0.1f); // wait half a second
        CancelInvoke();

    }


    IEnumerator FadeoutStarting()
    {
        yield return new WaitForSeconds(2f); // wait half a second
        //***************
        while (ifadeoutstarting == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutidlingtime); // wait half a second

            SMS1000RR.StartingFadeout();
        }

    }

    IEnumerator FadeinIdling()
    {
        //***************
        //    Debug.Log("Enter FadeinIdling1***********************************01");
        yield return new WaitForSeconds(fadeinidlingdelaytime); // wait half a second
        while (ifadeinidling == 1)
        {
   //               Debug.Log("Enter FadeinIdling1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            SMS1000RR.IdlingFadein();
      //      if (MainControllerS1000RR.icrackle == 0) AIdling.IdlingFadein();
      //      if (MainControllerS1000RR.icrackle == 1) AIdlingSport.IdlingFadein();
        }
    }

    IEnumerator FadeoutIdling()
    {
        //***************
        while (ifadeoutidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutidlingtime); // wait half a second

            SMS1000RR.IdlingFadeout();
        }
       
    }


    public void FNFadeoutIdlingModeChange()
    {
        //        AIdling.IdlingStop();
        Debug.Log("Sound Engine Crackle 1");
        ifadeoutidling = 1;
        StartCoroutine("FadeoutIdlingModeChange");

        ifadeinidling = 1;
        idlingvolsport = 0;
        StartCoroutine("FadeinIdlingModeChange");
        AIdlingSport.Idling();
    }
    public void FNFadeinIdlingModeChange()
    {
        ifadeoutidling = 1;
        StartCoroutine("FadeoutIdlingModeChange");

        ifadeinidling = 1;
        idlingvol = 0;
        StartCoroutine("FadeinIdlingModeChange");
        AIdling.Idling();
    }

    IEnumerator FadeinIdlingModeChange()
    {
        //***************
        while (ifadeinidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.05f); // wait half a second
            if (MainControllerS1000RR.icrackle == 0) AIdling.IdlingFadein();
            if (MainControllerS1000RR.icrackle == 1) AIdlingSport.IdlingFadein();
        }
    }


    IEnumerator FadeoutIdlingModeChange()
    {
        //***************
        while (ifadeoutidling == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.05f); // wait half a second
            if (MainControllerS1000RR.icrackle == 1) AIdling.IdlingFadeout();
            if (MainControllerS1000RR.icrackle == 0) AIdlingSport.IdlingFadeout();
        }
    }

    // Defunct************************
    IEnumerator FadeoutNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(fadeoutfsrtime); // wait half a second
            if (MainControllerS1000RR.ivehicle == 1)
            {
                ANFSR.NFSRFadeout();
                // Fadeout BlipFSRs
          //      ANFSR15000.BLIPCR15000Fadeout();
          //      ANBLIPC14000.BLIPCR14000Fadeout();
          //      ANBLIPC7500.BLIPCR7500Fadeout();
          //      ANBLIPC6000.BLIPCR6000Fadeout();
          //      ANBLIPC5500.BLIPCR5000Fadeout();
          //      ANBLIPC4000.BLIPCR4000Fadeout();
          //      ANBLIPC3000.BLIPCR3000Fadeout();
            }
            else if(MainControllerS1000RR.ivehicle == 2)
            {
                SMS1000RR.NFSRFadeout();
            }
        }
    }

    IEnumerator FadeinNFSR()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeinnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
            if(MainControllerS1000RR.ivehicle == 1) ANFSR.NFSRFadein();
          else if(MainControllerS1000RR.ivehicle == 2) SMS1000RR.NFSRFadein();
        }
    }

    IEnumerator FadeoutNSLI()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutsli == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
           if (MainControllerS1000RR.ivehicle == 1) ANSLI.NSLIFadeout();
                else if (MainControllerS1000RR.ivehicle == 2) SMS1000RR.NSLIFadeout();

        }
    }
    IEnumerator FadeoutNFSI1()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutfsi1 == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second

            ANFSI1.NFSI1Fadeout();
        }
    }


    // ***************************************************

    IEnumerator FadeoutBLIPCR7500()
    {
        //      Debug.Log("Enter FadeoutSLR1***************************************1");
        while (ifadeoutnfsr == 1)
        {
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.01f); // wait half a second
                                                    //           ANFSR.NFSRFadeout();
            ANBLIPC7500.BLIPCR7500Fadeout();
        }
    }
    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator RevLimiterVibrationOnOff()
    {
        if (ivibrator == 1) //Handheld.Vibrate();
            while (ivibrating == 1)
            {
                yield return new WaitForSeconds(0.5f);
                if (ivibrator == 1)
                {
                    //          Handheld.Vibrate();
                }
            }

    }

    public void StartingfromMainController()
    {
        SMS1000RR.Starting();
        if (MainControllerS1000RR.ivehicle == 2)
        {
            AudioStartingAventador.Startingvolume = 1f;
           idlingvol = 0;
        }
    }
    //     yield return new WaitForSeconds(startertime); // wait half a secon


    public void TriggerIdlingfromMainController()
    {
           if (MainControllerS1000RR.ivehicle == 1) startingtime = 1.5f;

        // else if (MainControllerS1000RR.ivehicle == 2) startingtime = 2.245f;
        else if (MainControllerS1000RR.ivehicle == 2) startingtime = 2.0f;
        StartCoroutine("StartIdling");
        Debug.Log("Before FadeiIdling1");
        if (MainControllerS1000RR.ivehicle == 2)
        {
           Debug.Log("Inside FadeiIdling1");
            ifadeoutstarting = 1;
            StartCoroutine("FadeoutStarting");
           ifadeinidling = 1;
            AudioIdlingAventador.Idlingvolume = 0;
            fadeinidlingdelaytime = 2;
            StartCoroutine("FadeinIdling");
        }
    }
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(startingtime); // wait half a second

        //     yield return new WaitForSeconds(startertime); // wait half a secon


        //     istartfromtouch = 2;

        gear = 0;



        // Start Timer upon engine start to change idling speed
        //     StartTimer = 0.0f;
        //    istarttimer = 1; //Start Timer


     //   AStarting.StartingStop();
     //   AStarting.StartingStop();
        StartCoroutine("Startistart1");  // Starting the Loop for Events
        itrate = 1;  // Enable SLI
        idlingvol = 1;
        irpm = 0;
        MainControllerS1000RR.rpm1 = rpmidling;
        rpm1 = rpmidling;
        SMS1000RR.Idling();
      
        Debug.Log("StartIdling in Sound ENgine");
        //     iblipcanstart = 0;
    }

    public void ShuttingfromMainController()
    {
        AShutting.Shutting();
        itrate = -1;
 //      istarttimer = 0; // Stop Timer
                         // Enable Console neutral Buttons
                         //       NeutralButtonOff.enabled = true;
                         //       NeutralButtonOn.enabled = false;

        istart = 0;
        //       istartfromtouch = 0;


        irpm = -1;  // For stopping RPM at Idling


        // Stop all sounds
        if (MainControllerS1000RR.ivehicle == 1)
        {
            AStarting.StartingStop();
            ANBLIP.BlipStop();
            ANSLI.NSLIStop();
            ANFSI1.NFSI1Stop();
            ANBLIP3000.Blip3000Stop();
            ANBLIP4000.Blip4000Stop();
            ANBLIP5000.Blip5000Stop();
            ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();
            AIdling.IdlingStop(); // Stop Idling
            AIdlingSport.IdlingStop();
            ANFSR.FSRStop();
            ANFSR15000.NFSR15000Stop();
            ANBLIPC14000.BLIPC14000Stop();
            ANBLIPC10000.BLIPC10000Stop();
            ANBLIPC7500.BLIPC7500Stop();
            ANBLIPC6000.BLIPC6000Stop();
            ANBLIPC5500.BLIPC5500Stop();
            ANBLIPC4000.BLIPC4000Stop();
            ANBLIPC3000.BLIPC3000Stop();
            ANRevLimiter.RevLimiterStop();

            AStarting.StartingStop();
        }
        if (MainControllerS1000RR.ivehicle == 2)
        {
            SMS1000RR.Shutting();
        }
            rpm1 = 0;

    }

    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        ReverbXoneMix = 0.00f;
      

        AIdling.IdlingReverbRoad();
        AIdlingSport.IdlingSportReverbRoad();
        AStartup.StartupReverbRoad();
        AStarting.StartingReverbRoad();
        AShutting.ShuttingReverbRoad();
        ANRevLimiter.RevLimiterReverbRoad();

        ANSLI.NSLIReverbRoad();
        ANFSI1.NFSI1ReverbRoad();


        //  ANFSI2.NFSI2ReverbRoad();

        ANBLIP2000.Blip2000ReverbRoad();
        ANBLIP3000.Blip3000ReverbRoad();
        ANBLIP4000.Blip4000ReverbRoad();
        ANBLIP5000.Blip5000ReverbRoad();
        ANBLIP6000.Blip6000ReverbRoad();
        ANBLIP7000.Blip7000ReverbRoad();
        ANBLIP10000.Blip10000ReverbRoad();
        ANBLIP14000.Blip14000ReverbRoad();


        ANBLIPC3000.Blipc3000ReverbRoad();
        ANBLIPC4000.Blipc4000ReverbRoad();
        ANBLIPC5500.Blipc5000ReverbRoad();
        ANBLIPC6000.Blipc6000ReverbRoad();
        ANBLIPC7500.Blipc7500ReverbRoad();
        ANBLIPC10000.Blipc10000ReverbRoad();
        ANBLIPC14000.Blipc14000ReverbRoad();
        ANFSR15000.NFSR15000ReverbRoad();
        ANFSR.NFSRReverbRoad();

        SMS1000RR.RoadAppear();

        ACrackle8000.Crackle8000ReverbRoad();
        ACrackle6000.Crackle6000ReverbRoad();
        ACrackle4000.Crackle4000ReverbRoad();

    }

    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {

        ReverbXoneMix = 1.03f;
      

        AIdling.IdlingReverbGarage();
        AIdlingSport.IdlingSportReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();

        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        // ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000.Blip2000ReverbGarage();
        ANBLIP3000.Blip3000ReverbGarage();
        ANBLIP4000.Blip4000ReverbGarage();
        ANBLIP5000.Blip5000ReverbGarage();
        ANBLIP6000.Blip6000ReverbGarage();
        ANBLIP7000.Blip7000ReverbGarage();
        ANBLIP10000.Blip10000ReverbGarage();
        ANBLIP14000.Blip14000ReverbGarage();


        ANBLIPC3000.Blipc3000ReverbGarage();
        ANBLIPC4000.Blipc4000ReverbGarage();
        ANBLIPC5500.Blipc5000ReverbGarage();
        ANBLIPC6000.Blipc6000ReverbGarage();
        ANBLIPC7500.Blipc7500ReverbGarage();
        ANBLIPC10000.Blipc10000ReverbGarage();
        ANBLIPC14000.Blipc14000ReverbGarage();
        ANFSR15000.NFSR15000ReverbGarage();
        ANFSR.NFSRReverbGarage();

   //     AIdlingAventador.IdlingReverbGarage();
   //     AIdlingSport.IdlingSportReverbGarage();
   //     AStartupAventador.StartupReverbGarage();
   //   AStartingAventador.StartingReverbGarage();
     //   AShutting.ShuttingReverbGarage();
    //    ANRevLimiterAventador.RevLimiterReverbGarage();
    //    ANSLIAventador.NSLIReverbGarage();
        ANFSR.NFSRReverbGarage();
        //    ANSLIAventador.NSLIReverbGarage();
        //     ANFSI1.NFSI1ReverbGarage();
        SMS1000RR.GarageAppear();



        ACrackle8000.Crackle8000ReverbGarage();
        ACrackle6000.Crackle6000ReverbGarage();
        ACrackle4000.Crackle4000ReverbGarage();
    }

    // DisAppearance of Racetrack Road/Garage
    public void TunnelAppear()  // 
    {
        ReverbXoneMix = 1.1f;
      

        AIdling.IdlingReverbGarage();
        AIdlingSport.IdlingSportReverbGarage();
        AStartup.StartupReverbGarage();
        AStarting.StartingReverbGarage();
        AShutting.ShuttingReverbGarage();
        ANRevLimiter.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLI.NSLIReverbGarage();
        ANFSI1.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000.Blip2000ReverbGarage();
        ANBLIP3000.Blip3000ReverbGarage();
        ANBLIP4000.Blip4000ReverbGarage();
        ANBLIP5000.Blip5000ReverbGarage();
        ANBLIP6000.Blip6000ReverbGarage();
        ANBLIP7000.Blip7000ReverbGarage();
        ANBLIP10000.Blip10000ReverbGarage();
        ANBLIP14000.Blip14000ReverbGarage();

        ANBLIPC3000.Blipc3000ReverbGarage();
        ANBLIPC4000.Blipc4000ReverbGarage();
        ANBLIPC5500.Blipc5000ReverbGarage();
        ANBLIPC6000.Blipc6000ReverbGarage();
        ANBLIPC7500.Blipc7500ReverbGarage();
        ANBLIPC10000.Blipc10000ReverbGarage();
        ANBLIPC14000.Blipc14000ReverbGarage();
        ANFSR15000.NFSR15000ReverbGarage();
        ANFSR.NFSRReverbGarage();


        SMS1000RR.GarageAppear();

        ACrackle8000.Crackle8000ReverbGarage();
        ACrackle6000.Crackle6000ReverbGarage();
        ACrackle4000.Crackle4000ReverbGarage();
    }


    public void StartupSound()
       
    {
        Debug.Log("Startup in Sound Engine");
   //     AStartup.Startup();
    }

    float clutchpos = 0;
    public void SliderClutch(float sliderpositionclutch)
    {
        //      moveSpeed = newSpeed;
        clutchpos = sliderpositionclutch;

    }
    public void Explode()  // Triggers Explosion in MainController
    {
        MCS1000RR.Explode();
    }

    


}
