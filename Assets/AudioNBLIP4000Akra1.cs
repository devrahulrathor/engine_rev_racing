﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP4000Akra1 : MonoBehaviour {
    public static float dt4000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip4000()
    {


        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngine.rpm1 >= 3000 && AudioEngine.rpm1 < 4000)
        {

            audio.Play();

        }
        else if (AudioEngine.rpm1 >= 2000 && AudioEngine.rpm1 < 3000)
        {
            dt4000 = 0.055f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }
        else if (AudioEngine.rpm1 < 2000)
        {
            dt4000 = 0.097f;
            audio.PlayScheduled(AudioSettings.dspTime + dt4000);
        }

        audio.volume = 1;
        //     audio.Play();
        //     audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip4000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip4000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip4000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
     //   audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}
