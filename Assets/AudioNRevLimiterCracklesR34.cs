﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterCracklesR34 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineR34.itrate == 7000)
        {
            //      RPMCal();
            // AudioEngineR34.rpm1 = 15000;

        }
        LowPassFilter();
    
    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineR34.ivibrating == 1)
        {
            //          AudioEngineR34.rpm1 = 15000;
            yield return new WaitForSeconds(0.08f);
            //       if (AudioEngineR34.ivibrator == 1)
            //       {
            if (AudioEngineR34.rpm1 == 7500) AudioEngineR34.rpm1 = 7000;
            else AudioEngineR34.rpm1 = 7500;
            //          else if (AudioEngineR34.rpm1 == 14000) AudioEngineR34.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR34.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}