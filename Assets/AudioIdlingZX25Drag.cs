using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingZX25Drag : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX25Drag.irpm == 0)
        {
      //           Debug.Log("Inside Idling Sub irpm = 0, MC RPM =");
      //     Debug.Log(SoundEngineZX25Drag.rpmidling);
            MainControllerDrag.rpm1 = SoundEngineZX25Drag.rpmidling;
            MainControllerDrag.bikespeed = 0;
        }
    }

    public void Idling()
    {
        Debug.Log("Inside Idling************************************************");
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = SoundEngineZX25Drag.idlingvol;
        Debug.Log("Inside Idling************************************************ Idlingvol=");
        Debug.Log(audio.volume);
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       Idlingvolume = 0;


    }

    double Idlingvolume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

           SoundEngineZX25Drag.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingFadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

       
        Idlingvolume = Idlingvolume + 0.1;
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
       
        else
        {
            audio.volume = 1;
           SoundEngineZX25Drag.ifadeinidling = 0;

            //            audio.Stop();
        }
    //    Debug.Log("-------Idling Voil = ");
    //    Debug.Log(Idlingvolume);

    }
    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
        Debug.Log("Inside Idling Reverb Garage, ReverbXone=");
        Debug.Log(SoundEngineZX25Drag.ReverbXoneMix);
    }
}

