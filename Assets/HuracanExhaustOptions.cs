﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuracanExhaustOptions : MonoBehaviour
{
    public GameObject HuracanStockExhaust;
    public GameObject HuracanAkra1Exhaust;

    public GameObject HuracanNormalMode;
    public GameObject HuracanSportMode;

    public GameObject HuracanAkra1CarbonExhaust;
    public GameObject HuracanExhaustForwardArrow;
    public GameObject HuracanExhaustBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject HuracanLocationOptions;
    public GameObject HuracanViewOptions;


    //   public GameObject ZX10RExhaustBackImage;
    public int iHuracanExhaust = 0;   // Appear/Disapperar Counter
    public int iHuracanExhaustSwipeCounter = 1;   // Decide the Exhaust Option Active: 1 Garage Active, 2 Road Active

    public GameObject HuracanMainCameraView, HuracanRConsoleCameraView;  // Camera Objects

    public static int iHuracanExhaust1 = 0;   // Appear/Disapperar Counter


    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Update()
    {

  //      if (HuracanCameraViewChange.iHuracanCameraSwitch == 1)  // Deactivate Button for Exhaust View Change when Console View
   //     {

            //       GetComponent<Button>().interactable = false;
    //    }
   //     else if (ZX10CameraViewChange.iZX10CameraSwitch == 0)
    //    {

            //      GetComponent<Button>().interactable = true;
      //  }
        if (iHuracanExhaust1 == 110)  // Disabled
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iHuracanExhaustSwipeCounter == 2)
                {
                    HuracanExhaustForwardArrow.SetActive(true);
                    HuracanExhaustBackArrow.SetActive(true);
                }
                else if (iHuracanExhaustSwipeCounter == 3)
                {
                    HuracanExhaustForwardArrow.SetActive(false);
                    HuracanExhaustBackArrow.SetActive(true);
                }
                else if (iHuracanExhaustSwipeCounter == 1)
                {
                    HuracanExhaustForwardArrow.SetActive(true);
                    HuracanExhaustBackArrow.SetActive(false);
                }
                if (pos.y > 400 && pos.y < 600 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iHuracanExhaustSwipeCounter == 1 && iswipeenable == 1)
                        {
                            HuracanStockExhaust.SetActive(false);
                            HuracanAkra1Exhaust.SetActive(true);
                            iHuracanExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iHuracanExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            HuracanAkra1Exhaust.SetActive(false);
                            HuracanAkra1CarbonExhaust.SetActive(true);
                            iHuracanExhaustSwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                    if (touchZero.deltaPosition.x > 10)
                    {

                        if (iHuracanExhaustSwipeCounter == 3 && iswipeenable == 1)
                        {
                            //        ZX10RTBR1Exhaust.SetActive(false);
                            HuracanAkra1Exhaust.SetActive(true);
                            HuracanAkra1CarbonExhaust.SetActive(false);
                            //               ZX10RStockExhaust.SetActive(false);
                            iHuracanExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iHuracanExhaustSwipeCounter == 2 && iswipeenable == 1)
                        {
                            HuracanAkra1Exhaust.SetActive(false);
                            HuracanStockExhaust.SetActive(true);
                            iHuracanExhaustSwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }


    public void Exhausts()  // 
    {


        if (iHuracanExhaust1 == 0)
        {
            HuracanStockExhaust.SetActive(true);
            HuracanAkra1Exhaust.SetActive(true);
       //     HuracanAkra1CarbonExhaust.SetActive(true);
            //         ZX10RLocationOptions.SetActive(false);
      //      HuracanViewOptions.SetActive(false);
            HuracanLocationOptions.SetActive(false);

            //       ZX10MainCameraView.SetActive(true);
            //       ZX10RConsoleCameraView.SetActive(false);

            iHuracanExhaust1 = 1;
        }
        else if (iHuracanExhaust1 == 1)
        {
            HuracanStockExhaust.SetActive(false);
            HuracanAkra1Exhaust.SetActive(false);
   //         HuracanAkra1CarbonExhaust.SetActive(false);
            //           ZX10RLocationOptions.SetActive(true);
  //          HuracanViewOptions.SetActive(true);
            HuracanLocationOptions.SetActive(true);
            iHuracanExhaust1 = 0;
        }
    }

    public void ExhaustsDeactivatefromOptions()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        HuracanNormalMode.SetActive(false);
        HuracanSportMode.SetActive(false);
 //       HuracanAkra1CarbonExhaust.SetActive(false);
        //           ZX10RLocationOptions.SetActive(true);

        iHuracanExhaust1 = 0;
    }

    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }


}
