﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuracanCameraViewChange : MonoBehaviour
{
    public static float ConsoleCameraAngle = 0.0f, ConsoleCameraAngleStep = 0.9f;
    public static int iHuracanCameraSwitch = 0; // To Switch Camera from Default to Console 
    int iHuracanCameraSwitch2 = 0; // To Switch Camera from Default to Console
    public GameObject HuracanMainCameraView, HuracanConsoleCameraView;  // Camera Objects
    public GameObject RPMConsole, ConsoleBackground, Image360;   //Partillly take off Console Buttons
    public GameObject DefaultView, ConsoleView;  // GUI Images

    // Other Game Options Appear/Disappear
    public GameObject HuracanExhaustOptions;
    public GameObject HuracanLocationOptions;
    public GameObject ExhaustButton;  // Deactivate Exhaust Button when Console View

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //     ZX10MainCameraView.transform.position = Vector3.Lerp(ZX10MainCameraView.transform.position, ZX10RConsoleCameraView.transform.position, 0.5f * Time.deltaTime);
        //      do {
        //         if (ZX10MainCameraView.transform.position.x >= -10 && ZX10MainCameraView.transform.position.y >= -10)
        //         {
        //            ZX10MainCameraView.transform.Rotate(0, -ConsoleCameraAngleStep, 0);
        //             ConsoleCameraAngle = ConsoleCameraAngle + ConsoleCameraAngleStep;
        //         }
        //     } while (ConsoleCameraAngle <= 90.0f);
    }





    public void HuracanViewChange()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        if (iHuracanCameraSwitch == 0)
        {
            HuracanMainCameraView.SetActive(false);
            HuracanConsoleCameraView.SetActive(true);
            ConsoleView.SetActive(true);
            DefaultView.SetActive(false);
            RPMConsole.SetActive(false);
            ConsoleBackground.SetActive(false);
            Image360.SetActive(false);


            iHuracanCameraSwitch = 1;
        }
        else if (iHuracanCameraSwitch == 1)
        {
            HuracanMainCameraView.SetActive(true);
            HuracanConsoleCameraView.SetActive(false);
            DefaultView.SetActive(true);
            ConsoleView.SetActive(false);
            RPMConsole.SetActive(true);
            ConsoleBackground.SetActive(true);
            Image360.SetActive(true);

            iHuracanCameraSwitch = 0;
        }
    }
    //      do
    //      {
    //          ZX10MainCameraView.transform.position = Vector3.Lerp(ZX10MainCameraView.transform.position, ZX10RConsoleCameraView.transform.position, 0.004f * Time.deltaTime);
    //         ZX10MainCameraView.transform.Rotate(0, -ConsoleCameraAngleStep, 0);
    //        ConsoleCameraAngle = ConsoleCameraAngle + ConsoleCameraAngleStep;
    //    } while (ConsoleCameraAngle <= 9000.0f);



    public void HuracanViewToggle()
    {
        if (iHuracanCameraSwitch2 == 0)
        {
            DefaultView.SetActive(true);
            ConsoleView.SetActive(true);

            HuracanExhaustOptions.SetActive(false);
            HuracanLocationOptions.SetActive(false);

            iHuracanCameraSwitch2 = 1;
        }
        else if (iHuracanCameraSwitch2 == 1)
        {
            DefaultView.SetActive(false);
            ConsoleView.SetActive(false);

            HuracanExhaustOptions.SetActive(true);
            HuracanLocationOptions.SetActive(true);
            iHuracanCameraSwitch2 = 0;
        }


    }

    public void CameraViewsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {
        DefaultView.SetActive(false);
        ConsoleView.SetActive(false);
        iHuracanCameraSwitch2 = 0;
    }

    public void HuracanDefaultView()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);


        HuracanMainCameraView.SetActive(true);
        HuracanConsoleCameraView.SetActive(false);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
        RPMConsole.SetActive(true);
        ConsoleBackground.SetActive(true);

        iHuracanCameraSwitch = 0;

    }

    public void HuracanConsoleView()
    {
        //        ZX10CameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        iHuracanCameraSwitch = 1;
        HuracanMainCameraView.SetActive(false);
        HuracanConsoleCameraView.SetActive(true);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
        RPMConsole.SetActive(false);
        ConsoleBackground.SetActive(false);



    }

}
