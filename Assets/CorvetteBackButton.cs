using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CorvetteBackButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void BackButton()
    {
        if (AudioEngineCorvette.itrate != -1) AudioEngineCorvette.istartfromtouch = 3;
        SceneManager.LoadScene(0); // Car Options1 Main Screen
    }
}
