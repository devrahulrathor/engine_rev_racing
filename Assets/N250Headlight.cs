﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N250Headlight : MonoBehaviour {
    public GameObject HeadLight1;

    // Use this for initialization
    public void HeadlightOn()
    {
        //           HeadLight.enabled = true;
        HeadLight1.SetActive(true);

    }
    public void HeadlightOff()
    {
        //           HeadLight.enabled = true;
        HeadLight1.SetActive(false);

    }
}
