﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N250Taillight : MonoBehaviour {
    public GameObject TailLight1;
    // Use this for initialization
    public void TaillightOn()
    {
        //           HeadLight.enabled = true;
        TailLight1.SetActive(true);

    }
    public void TaillightOff()
    {
        //           HeadLight.enabled = true;
        TailLight1.SetActive(false);

    }
}
