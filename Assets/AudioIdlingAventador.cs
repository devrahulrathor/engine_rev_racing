using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingAventador : MonoBehaviour
{


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineS1000RR.itrate == 1)
        {
            //       Debug.Log("Inside AIdling S10000");
            SoundEngineS1000RR.rpm1 = SoundEngineS1000RR.rpmidling;
        }
    }

    public void IdlingAventador()
    {
        
        AudioSource audio = GetComponent<AudioSource>();
    //    audio.reverbZoneMix = 0.0f;
        audio.volume = SoundEngineS1000RR.idlingvol;
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
      //  audio.volume = 0;
        //       Idlingvolume = 0;


    }

    public static double Idlingvolume = 1.0;
    public void IdlingFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

            SoundEngineS1000RR.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;
            Idlingvolume = 0;
        }
    }
    public static int ivol=0;
    public void IdlingFadein()
    {
        
        AudioSource audio = GetComponent<AudioSource>();

   //     ivol = ivol + 1;
   //     if (ivol == 1)
   //     {
   //        Idlingvolume = 0;
   //     }
        Idlingvolume = Idlingvolume + 0.1;
   //     Debug.Log("Inside IdlingFade In Idlingvol=");
    //    Debug.Log(Idlingvolume);
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
        else
        {
            audio.volume = 1;
            SoundEngineS1000RR.ifadeinidling = 0;
       //    ivol = 0;
            //            audio.Stop();
        }

    }


    public void IdlingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }




}