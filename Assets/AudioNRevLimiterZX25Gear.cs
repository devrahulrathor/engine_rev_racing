using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterZX25Gear : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (SoundEngineZX25Drag.itrate == 10000)
        {
            //      RPMCal();
      //      SoundEngineZX25Drag.rpm1 = 17500;

        }

    }

    public void RevLimiter()
    {
        Debug.Log("Inside RevLimiter Audio Start");
   //    StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop Gear");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (SoundEngineZX25Drag.ivibrating == 1)
        {
            //          SoundEngineZX25Drag.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (SoundEngineZX25Drag.ivibrator == 1)
            //       {
            if (SoundEngineZX25Drag.rpm1 == 15000) SoundEngineZX25Drag.rpm1 = 14000;
            else SoundEngineZX25Drag.rpm1 = 15000;
            //          else if (SoundEngineZX25Drag.rpm1 == 14000) SoundEngineZX25Drag.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //      audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX25Drag.ReverbXoneMix;
    }
}
