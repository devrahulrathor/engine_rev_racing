﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10RBikeTranslate : MonoBehaviour

{
    float rightcorrection = 1, downcorrection=1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Move the object forward along its z axis 1 unit/second.
        transform.Translate(Vector3.down * 350 * Time.deltaTime, Space.Self);
        transform.Translate(Vector3.right * 5*rightcorrection * Time.deltaTime,Space.Self);
      
        rightcorrection = rightcorrection + 0.1f;
                transform.Rotate(0.0f, 0.25f, 0.0f, Space.Self);
        transform.Translate(Vector3.forward * 1.5f*downcorrection  * Time.deltaTime, Space.Self);
    }

}