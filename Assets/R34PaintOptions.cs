﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34PaintOptions : MonoBehaviour
{
    public GameObject thisPaintButton, ColorButtons;
    public static int iR34thisPaintbutton = 0;
    public static int iR34PaintOptionButtons = 0;
    // Start is called before the first frame update


    public void PaintButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        Debug.Log("VehicleButton=");
        //     Debug.Log(iR34thisVehiclebutton);
        if (iR34thisPaintbutton == 0)
        {

            thisPaintButton.SetActive(false);
            iR34thisPaintbutton = 1;
            //       ColorButtons.SetActive(false);
        }
        else if (iR34thisPaintbutton == 1)
        {
            thisPaintButton.SetActive(true);
            iR34thisPaintbutton = 0;
            //      ColorButtons.SetActive(true);
        }
    }


    public void PaintOptionButtonsDeactivate()  // 
    {
        ColorButtons.SetActive(false);
        iR34PaintOptionButtons = 0;
    }

    public void PaintOptionButtons()  // 
    {



        if (iR34PaintOptionButtons == 0)
        {
            ColorButtons.SetActive(true);
            iR34PaintOptionButtons = 1;
            R34Customizeoptions.iR34thisCustomizebutton = 0;
        }
        else if (iR34PaintOptionButtons == 1)
        {
            ColorButtons.SetActive(false);

            iR34PaintOptionButtons = 0;
        }
    }

}

