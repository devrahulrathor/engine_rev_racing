using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraViewChange : MonoBehaviour
{
    public GameObject CorvetteMainCameraView, CorvettConsoleCameraView;  // Camera Objects
    public static int iCorvetteCameraSwitch = 0; // To Switch Camera from Default to Console 
    public void CorvetteDefaultView()
    {
        //        CorvetteCameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);


        CorvetteMainCameraView.SetActive(true);
        CorvettConsoleCameraView.SetActive(false);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
    //    RPMConsole.SetActive(true);
       

        iCorvetteCameraSwitch = 0;

    }

    public void CorvetteConsoleView()
    {
        //        CorvetteCameraView.transform.position = Vector3.Lerp(this.transform.position, new Vector3(0.00512155f, -0.002f, 0.001f), Time.deltaTime);

        iCorvetteCameraSwitch = 1;
        CorvetteMainCameraView.SetActive(false);
        CorvettConsoleCameraView.SetActive(true);
        //  ConsoleView.SetActive(false);
        //  DefaultView.SetActive(true);
     



    }

}
