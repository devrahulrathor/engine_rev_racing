﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScriptCrackle8000R35 : MonoBehaviour
{
    //   public static float dtcrackle8000 = 0.15f;  // Time lapse after FSR after which Crackle starts in AudioEngine
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        LowPassFilter();
    }
    public void Crackle8000()
    {
        //      StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayScheduled(AudioSettings.dspTime + AudioEngineR35.dtcrackle);
        //       audio.Play();
        audio.Play(44100);
    }

    public void Crackle8000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Crackle8000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}

