using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColorCorvette : MonoBehaviour
{
    public Material matbody1;  // Main Body Colour
                               //  public Material matbody2;  // Stripe Colour
    public Color newcol;
    public KeyCode changecolo;
    public float icolorshade = 0.3f;  // Control Color Shade Manually
    public Slider cSlider;  // Slider for Body Colour
    public Slider MetallicSlider;  // Slider for Metallic Prop of Body Color
    public float icolorMetallic = 0.35f;  // Control Color Metallic Shade Manually
    float sliderpositionMat1;
    int icolorcounter = 1;  //Decide which color dynamically updates: 1 Red, 2:Blue, 3:Green, 4:Yellow, 5: Black, 6: White
                            //   public float sliderval;
    public GameObject Stripes;  // Deactivate Stripes
    public GameObject mycar1, mycar2, mycar3;  // My cars for colors
    public GameObject loadmycar1, loadmycar2, loadmycar3;  // My cars for colors
    public GameObject SaveMyCarNote1, SaveMyCarNote2, SaveMyCarNote3;  // Notification when Car Saved

    int iStripestate = 1;  // 1- Stripe On -1-Stripe off

    int icolorcounterstripe = 1;
    int istripestatefilefirst = 1;
    // Use this for initialization
    void Start()
    {

        LoadMyCar(1);
        //   PlayerPrefs.SetInt("CorvetteStripestate1", iStripestate);
        //   ColourRefresh(); //Set Colour to default values 

    }


    // Update is called once per frame
    void Update()
    {
        //     Slidervalue();
    }
    public void ColorRed()
    {
        icolorcounter = 1;
        if (icolorcounter == 1)
        {
            Debug.Log("INside Red Colour");
            ///  matbody1.color = Color.red;
            if (icolorshade >= 0) matbody1.color = Color.Lerp(Color.red, Color.black, icolorshade);
            else if (icolorshade < 0) matbody1.color = Color.Lerp(Color.red, Color.magenta, -1 * icolorshade);
        }//    Debug.Log("Colour Change");
    }
    public void ColorBlue()
    {
        icolorcounter = 2;
        if (icolorcounter == 2)
        {
            //     matbody1.color = Color.blue;
            if (icolorshade >= 0) matbody1.color = Color.Lerp(Color.blue, Color.black, icolorshade);
            else if (icolorshade < 0) matbody1.color = Color.Lerp(Color.blue, Color.green, -1 * icolorshade);
        }
        //      Debug.Log("Colour Change");
    }

    public void ColorGreen()
    {
        icolorcounter = 3;
        if (icolorcounter == 3)
        {
            //   matbody1.color = Color.green;
            matbody1.color = Color.Lerp(Color.green, Color.black, icolorshade);
            // Debug.Log("Colour Change");
        }
    }
    public void ColorYellow()
    {
        icolorcounter = 4;
        if (icolorcounter == 4)
        {
            //    matbody1.color = Color.yellow;
            if (icolorshade >= 0) matbody1.color = Color.Lerp(Color.yellow, Color.black, icolorshade);
            else if (icolorshade < 0) matbody1.color = Color.Lerp(Color.yellow, Color.red, -1 * icolorshade);
            //   Debug.Log("Colour Change");
        }
    }
    public void ColorBlack()
    {
        icolorcounter = 5;
        if (icolorcounter == 5)
        {
            //     matbody1.color = Color.black;
            matbody1.color = Color.Lerp(Color.black, Color.white, icolorshade);
            //     Debug.Log("Colour Change");
        }
    }

    public void ColorWhite()
    {
        icolorcounter = 6;
        if (icolorcounter == 6)
        {
            //   matbody1.color = Color.white;
            matbody1.color = Color.Lerp(Color.white, Color.black, icolorshade);
            //       Debug.Log("Colour Change");
        }
    }

    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        icolorshade = sliderposition;
        ColourRefresh();

        //     Debug.Log("Icolorshade=");
        //     Debug.Log(icolorshade);

    }
    void ColourRefresh()
    {

        if (icolorcounter == 1) ColorRed();
        if (icolorcounter == 2) ColorBlue();
        if (icolorcounter == 3) ColorGreen();
        if (icolorcounter == 4) ColorYellow();
        if (icolorcounter == 5) ColorBlack();
        if (icolorcounter == 6) ColorWhite();

    }
    void ColourStripeRefresh()
    {

        if (icolorcounterstripe == 1) ColorBlackStripe();
        if (icolorcounterstripe == 2) ColorWhiteStripe();
        if (icolorcounterstripe == 3) ColorRedStripe();
        if (icolorcounterstripe == 4) ColorBlueStripe();
        if (icolorcounterstripe == 5) ColorYellowStripe();
        if (icolorcounterstripe == 6) ColorGreenStripe();

    }
    void ColourShadeRefresh()
    {
    }

    public void ColorBlackStripe()
    {
        icolorcounterstripe = 1;
        if (icolorcounterstripe == 1)
        {
            //       matbody2.color = Color.black;
            //    matbody2.color = Color.Lerp(Color.black, Color.white, icolorshade);
            //     Debug.Log("Colour Change");
        }
    }
    public void ColorWhiteStripe()
    {
        icolorcounterstripe = 2;
        if (icolorcounterstripe == 2)
        {
            //      matbody2.color = Color.white;
            //   matbody2.color = Color.Lerp(Color.white, Color.black, icolorshade);
            //       Debug.Log("Colour Change");
        }
    }
    public void ColorRedStripe()
    {
        icolorcounterstripe = 3;
        if (icolorcounterstripe == 3)
        {
            //       matbody2.color = Color.red;
            //     matbody1.color = Color.Lerp(Color.red, Color.black, icolorshade);

        }//    Debug.Log("Colour Change");
    }

    public void ColorBlueStripe()
    {
        icolorcounterstripe = 4;
        if (icolorcounterstripe == 4)
        {
            //        matbody2.color = Color.blue;
            //    matbody1.color = Color.Lerp(Color.blue, Color.black, icolorshade);
        }
        //      Debug.Log("Colour Change");
    }

    public void ColorYellowStripe()
    {
        icolorcounterstripe = 5;
        if (icolorcounterstripe == 5)
        {
            //        matbody2.color = Color.yellow;
            //    matbody1.color = Color.Lerp(Color.blue, Color.black, icolorshade);
        }
        //      Debug.Log("Colour Change");
    }

    public void ColorGreenStripe()
    {
        icolorcounterstripe = 6;
        if (icolorcounterstripe == 6)
        {
            //      matbody2.color = Color.green;
            //    matbody1.color = Color.Lerp(Color.blue, Color.black, icolorshade);
        }
        //      Debug.Log("Colour Change");
    }
    public void StripeOnOff()
    {
        if (iStripestate == 1)
        {
            //         Stripes.SetActive(false);
            iStripestate = -1;
        }
        else if (iStripestate == -1)
        {
            //        Stripes.SetActive(true);
            iStripestate = 1;
        }
    }

    // Metallic Propsery of Body Color
    public void SlidervalueMat(float sliderpositionMat)
    {

        //      moveSpeed = newSpeed;
        sliderpositionMat1 = sliderpositionMat;
        MetallicProperty();
        icolorMetallic = sliderpositionMat1;
        //    icolorshade = sliderposition;
        //   ColourRefresh();



    }
    public void MetallicProperty()
    {
        Debug.Log("Inside Metallic");
        matbody1.SetFloat("_Metallic", sliderpositionMat1);
        //     Debug.Log(" 1    IcolorMetallic=");
        //    Debug.Log(icolorMetallic);
    }

    // Save Car Load Number
    int imycar = 1;
    public void SaveMyCarNumber()
    {
        if (imycar < 3) imycar = imycar + 1;
        else imycar = 1;
        DeactivateCarNumberButtons();
        if (imycar == 1)
        {
            mycar1.SetActive(true);

        }
        else if (imycar == 2)
        {
            mycar2.SetActive(true);

        }
        else if (imycar == 3)
        {
            mycar3.SetActive(true);

        }

    }

    void DeactivateCarNumberButtons()
    {
        mycar1.SetActive(false);
        mycar2.SetActive(false);
        mycar3.SetActive(false);
    }
    private IEnumerator DeactivateCarSaveNote()
    {
        yield return new WaitForSeconds(1f);
        {
            SaveMyCarNote1.SetActive(false);
            SaveMyCarNote2.SetActive(false);
            SaveMyCarNote3.SetActive(false);
        }
    }


    int iloadmycar = 1;
    public void LoadMyCarNumber()
    {
        if (iloadmycar < 3) iloadmycar = iloadmycar + 1;
        else iloadmycar = 1;
        Debug.Log("iloadmycar = ");
        Debug.Log(iloadmycar);
        DeactivateLoadCarNumberButtons();
        if (iloadmycar == 1) loadmycar1.SetActive(true);
        else if (iloadmycar == 2) loadmycar2.SetActive(true);
        else if (iloadmycar == 3) loadmycar3.SetActive(true);
    }

    void DeactivateLoadCarNumberButtons()
    {
        loadmycar1.SetActive(false);
        loadmycar2.SetActive(false);
        loadmycar3.SetActive(false);
    }

    int R35col1, R35col2, R35col3;
    public void SaveMyCar(int icarcolor)
    {
        //    R35col1 = PlayerPrefs.SetFloat("Colour1", icolorshade);
        Debug.Log("Save ColourMetallic=");
        Debug.Log(icolorMetallic);

        if (icarcolor == 1)
        {
            PlayerPrefs.SetInt("CorvetteColour1", icolorcounter);
            PlayerPrefs.SetFloat("CorvetteColourshade1", icolorshade);
            PlayerPrefs.SetFloat("CorvetteColourMetallic1", icolorMetallic);
            PlayerPrefs.SetInt("CorvetteStripecolour1", icolorcounterstripe);

            //        if (iStripestate == 0) iStripestate = 1;
            //      else if (iStripestate == 1) iStripestate = 0;

            PlayerPrefs.SetInt("CorvetteStripestate1", iStripestate);
            SaveMyCarNote1.SetActive(true);
            //      Debug.Log("Save Stripestate2=");
            //      Debug.Log(iStripestate);
        }
        //    Corvettecol1 = PlayerPrefs.SetFloat("Colour1", icolorshade);
        else if (icarcolor == 2)
        {
            PlayerPrefs.SetInt("CorvetteColour2", icolorcounter);
            PlayerPrefs.SetFloat("CorvetteColourshade2", icolorshade);
            PlayerPrefs.SetFloat("CorvetteColourMetallic2", icolorMetallic);
            PlayerPrefs.SetInt("CorvetteStripecolour2", icolorcounterstripe);
            //        if (iStripestate == 0) iStripestate = 1;
            //      else if (iStripestate == 1) iStripestate = 0;
            PlayerPrefs.SetInt("CorvetteStripestate2", iStripestate);
            SaveMyCarNote2.SetActive(true);
            //      Debug.Log("Save Stripestate2=");
            //      Debug.Log(iStripestate);
        }
        else if (icarcolor == 3)
        {
            PlayerPrefs.SetInt("CorvetteColour3", icolorcounter);
            PlayerPrefs.SetFloat("CorvetteColourshade3", icolorshade);
            PlayerPrefs.SetFloat("CorvetteColourMetallic3", icolorMetallic);
            PlayerPrefs.SetInt("CorvetteStripecolour3", icolorcounterstripe);
            //           if (iStripestate == 0) iStripestate = 1;
            //         else if (iStripestate == 1) iStripestate = 0;
            PlayerPrefs.SetInt("CorvetteStripestate3", iStripestate);
            SaveMyCarNote3.SetActive(true);
        }
        StartCoroutine("DeactivateCarSaveNote");

    }

    public void LoadMyCar(int icarcolor)
    {
        //   icolorMetallic = PlayerPrefs.GetInt("ColourMetallic");
        //    matbody1.SetFloat("ColorMetallic", PlayerPrefs.GetInt("ColourMetallic"));


        if (icarcolor == 1)
        {
            Debug.Log("Load ColourMetallic=");
            Debug.Log(PlayerPrefs.GetFloat("CorvetteColourMetallic1"));

            icolorcounter = PlayerPrefs.GetInt("CorvetteColour1");
            //   icolorcounter = PlayerPrefs.GetFloat("Colour1");
            icolorshade = PlayerPrefs.GetFloat("CorvetteColourshade1");
            matbody1.SetFloat("_Metallic", PlayerPrefs.GetFloat("CorvetteColourMetallic1"));
            icolorcounterstripe = PlayerPrefs.GetInt("CorvetteStripecolour1");
            //     iStripestate = PlayerPrefs.GetInt("CorvetteStripestate1");
            //        if (PlayerPrefs.GetInt("CorvetteStripestate1") == 0) Stripes.SetActive(true);
            //        if (PlayerPrefs.GetInt("CorvetteStripestate1") == -1) Stripes.SetActive(false);
            //        else if (PlayerPrefs.GetInt("CorvetteStripestate1") == 1) Stripes.SetActive(true);
            Debug.Log("Load Stripestate1=");
            Debug.Log(iStripestate);

        }
        else if (icarcolor == 2)
        {
            //   Debug.Log(" Load icarcolor 2");
            icolorcounter = PlayerPrefs.GetInt("CorvetteColour2");
            //   icolorcounter = PlayerPrefs.GetFloat("Colour1");
            icolorshade = PlayerPrefs.GetFloat("CorvetteColourshade2");
            matbody1.SetFloat("_Metallic", PlayerPrefs.GetFloat("CorvetteColourMetallic2"));
            icolorcounterstripe = PlayerPrefs.GetInt("CorvetteStripecolour2");
            //   iStripestate = PlayerPrefs.GetInt("CorvetteStripestate2");
            //          if (PlayerPrefs.GetInt("CorvetteStripestate2") == -1) Stripes.SetActive(false);
            //         else if (PlayerPrefs.GetInt("CorvetteStripestate2") == 1) Stripes.SetActive(true);
            Debug.Log("Load Stripestate2=");
            Debug.Log(iStripestate);
        }
        else if (icarcolor == 3)
        {
            //      Debug.Log(" Load icarcolor 2");
            icolorcounter = PlayerPrefs.GetInt("CorvetteColour3");
            //   icolorcounter = PlayerPrefs.GetFloat("Colour1");
            icolorshade = PlayerPrefs.GetFloat("CorvetteColourshade3");
            matbody1.SetFloat("_Metallic", PlayerPrefs.GetFloat("CorvetteColourMetallic3"));
            icolorcounterstripe = PlayerPrefs.GetInt("CorvetteStripecolour3");
            //  iStripestate = PlayerPrefs.GetInt("CorvetteStripestate3");
            //          if (PlayerPrefs.GetInt("CorvetteStripestate3") == -1) Stripes.SetActive(false);
            //          else if (PlayerPrefs.GetInt("CorvetteStripestate3") == 1) Stripes.SetActive(true);

        }

        Debug.Log("Stripestate =");
        Debug.Log(iStripestate);
        //      StripeOnOff();
        ColourRefresh();
        ColourStripeRefresh();


    }

}
