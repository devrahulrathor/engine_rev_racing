﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltoOptionsButton : MonoBehaviour
{
    public GameObject FiestaViewsButton;
    public GameObject FiestaPopsCracklesButton;
    //    public GameObject AltoExhaustButton;
    //    public GameObject Ninj250OptionButtonBackground;
    //   public GameObject AltoCameraViewButton;



    public int iAltooptions = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AllOptionButtonsActive()  // 
    {
        if (iAltooptions == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            FiestaViewsButton.SetActive(true);
            FiestaPopsCracklesButton.SetActive(true);
            //        AltoExhaustButton.SetActive(true);
            //      ZX10ROptionButtonBackground.SetActive(true);
            //      AltoCameraViewButton.SetActive(true);
            iAltooptions = 1;

        }
        else if (iAltooptions == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            FiestaViewsButton.SetActive(false);
            FiestaPopsCracklesButton.SetActive(false);
            //    AltoExhaustButton.SetActive(false);
            //       ZX10ROptionButtonBackground.SetActive(false);
            //    AltoCameraViewButton.SetActive(false);
            iAltooptions = 0;

        }

    }


}

