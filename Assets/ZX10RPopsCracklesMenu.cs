﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10RPopsCracklesMenu : MonoBehaviour
{
    int iZX10PopsCrackles = 0;
    // Other Game Options Appear/Disappear
    public GameObject ZX10RExhaustOptions;
    public GameObject ZX10RLocationOptions;
    public GameObject ZX10RViewsButton;  // Deactivate Exhaust Button when Console View


    public GameObject PopsCracklesButton1;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ZX10PopsCracklesMenu()
    {
        if (iZX10PopsCrackles == 0)
        {
            PopsCracklesButton1.SetActive(true);
            //        DefaultView.SetActive(true);
            //        ConsoleView.SetActive(true);

            ZX10RExhaustOptions.SetActive(false);
            ZX10RLocationOptions.SetActive(false);
            ZX10RViewsButton.SetActive(false);
            iZX10PopsCrackles = 1;
        }
        else if (iZX10PopsCrackles == 1)
        {
            PopsCracklesButton1.SetActive(false);
            //          DefaultView.SetActive(false);
            //          ConsoleView.SetActive(false);

            ZX10RExhaustOptions.SetActive(true);
            ZX10RLocationOptions.SetActive(true);
            ZX10RViewsButton.SetActive(true);
            iZX10PopsCrackles = 0;
        }
    }
        public void ZX10PopsCracklesDropDownDeactivate()  // From Main Options Button on top
        {
       PopsCracklesButton1.SetActive(false);
        iZX10PopsCrackles = 0;
    }
    
}

   
        
