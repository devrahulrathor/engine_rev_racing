﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja250Views2 : MonoBehaviour
{

    public GameObject Ninja250GarageButton;
    public GameObject Ninja250RoadButton;
    public GameObject Ninja250TunnelButton;
    public GameObject Ninja250ViewBackImage;
    public GameObject Ninja250ViewForwardArrow;
    public GameObject Ninja250ViewBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject Ninja250ExhaustOptions;
    public GameObject Ninja250ViewOptions;
    public GameObject Ninja250PopsCracklesButton2;


    public int iNinja250view = 0;   // Appear/Disapperar Counter
    public int iNinja250SwipeCounter = 1;   // Decide the View Option Active: 1 Garage Active, 2 Road Active
    int iNinja250Location = 0;

    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (iNinja250Location == 100)  // Deactivated
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iNinja250SwipeCounter == 2)
                {
                    Ninja250ViewForwardArrow.SetActive(true);
                    Ninja250ViewBackArrow.SetActive(true);
                }
                else if (iNinja250SwipeCounter == 3)
                {
                    Ninja250ViewForwardArrow.SetActive(false);
                    Ninja250ViewBackArrow.SetActive(true);
                }
                else if (iNinja250SwipeCounter == 1)
                {
                    Ninja250ViewForwardArrow.SetActive(true);
                    Ninja250ViewBackArrow.SetActive(false);
                }

                if (pos.y > 600 && pos.y < 800 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iNinja250SwipeCounter == 1 && iswipeenable == 1)
                        {
                            Ninja250RoadButton.SetActive(true);
                            Ninja250GarageButton.SetActive(false);
                            Ninja250TunnelButton.SetActive(false);
                            iNinja250SwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iNinja250SwipeCounter == 2 && iswipeenable == 1)
                        {
                            Ninja250TunnelButton.SetActive(true);
                            Ninja250RoadButton.SetActive(false);
                            iNinja250SwipeCounter++;

                            //                 iswipeenable = 0;
                            //                 StartCoroutine("EnableSwipe");
                        }

                    }
                    if (touchZero.deltaPosition.x > 10)
                    {
                        if (iNinja250SwipeCounter == 3 && iswipeenable == 1)
                        {
                            //                  //        ZX10RTBR1Exhaust.SetActive(false);
                            Ninja250RoadButton.SetActive(true);
                            Ninja250TunnelButton.SetActive(false);
                            iNinja250SwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iNinja250SwipeCounter == 2 && iswipeenable == 1)
                        {
                            Ninja250GarageButton.SetActive(true);
                            Ninja250RoadButton.SetActive(false);
                            //              ZX10RTunnelButton.SetActive(false);
                            iNinja250SwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }

    public void Locations()  // 
    {



        if (iNinja250Location == 0)
        {
            Ninja250GarageButton.SetActive(true);
            Ninja250RoadButton.SetActive(true);
            Ninja250TunnelButton.SetActive(true);
            Ninja250ExhaustOptions.SetActive(false);
           Ninja250PopsCracklesButton2.SetActive(false);
           Ninja250ViewOptions.SetActive(false);
            iNinja250Location = 1;
        }
        else if (iNinja250Location == 1)
        {
            Ninja250GarageButton.SetActive(false);
            Ninja250RoadButton.SetActive(false);
            Ninja250TunnelButton.SetActive(false);
            Ninja250ExhaustOptions.SetActive(true);
            Ninja250PopsCracklesButton2.SetActive(true);
            Ninja250ViewOptions.SetActive(true);
            iNinja250Location = 0;
        }
    }

    public void LocationsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        Ninja250GarageButton.SetActive(false);
        Ninja250RoadButton.SetActive(false);
        Ninja250TunnelButton.SetActive(false);
        Ninja250PopsCracklesButton2.SetActive(false);
        iNinja250Location = 0;

    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iNinja250view == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            //          audio.Play();
            Ninja250GarageButton.SetActive(true);
            Ninja250RoadButton.SetActive(true);
            //        ZX10RViewBackImage.SetActive(true);


            iNinja250view = 1;
        }
        else if (iNinja250view == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            //        audio.Play();
            Ninja250GarageButton.SetActive(true);
            Ninja250RoadButton.SetActive(true);
            //       ZX10RViewBackImage.SetActive(false);
            iNinja250view = 0;
        }

    }


    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
}
