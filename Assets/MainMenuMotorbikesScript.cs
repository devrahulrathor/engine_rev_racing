﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuMotorbikesScript : MonoBehaviour
{

    public GameObject ZX10R, Ninja400, CB400;
    // Start is called before the first frame update
    public void MotorBikes1Activate()
    {
        //  MainMenuVehiclesScript.iMainMenuVehicles = 11;

        PlayerPrefs.SetInt("iVehiclesMenu", 11);
        //       ZX10R.SetActive(true);
        //        Ninja400.SetActive(true);
    }

    public void MotorBikes1DeActivate()
    {

        ZX10R.SetActive(false);
        Ninja400.SetActive(false);
    }
}
