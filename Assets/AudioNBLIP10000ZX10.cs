using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP10000ZX10 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void Blip10000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (SoundEngineZX10.rpm1 >= 7000 && SoundEngineZX10.rpm1 < 8000)
        {
            audio.Play();
        }
        else if (SoundEngineZX10.rpm1 >= 6000 && SoundEngineZX10.rpm1 < 7000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.016F);
        }
        else if (SoundEngineZX10.rpm1 >= 5000 && SoundEngineZX10.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.027F);
        }
        else if (SoundEngineZX10.rpm1 >= 4000 && SoundEngineZX10.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.048F);
        }
        else if (SoundEngineZX10.rpm1 >= 3000 && SoundEngineZX10.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.099F);
        }
        else if (SoundEngineZX10.rpm1 >= 2000 && SoundEngineZX10.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.164F);
        }


        else if (SoundEngineZX10.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.26F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip10000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip10000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip10000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //   audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }
}

