Thank you for purchasing this asset.
The skycrapers package contains 27 models with atlased textures (many buildings share the same texture) sized 1024x1024. The goal is to provide light and simple buildings for your game.
The materials are PBM and all sets contain emmision map for night scenes.
The models are meant to be seen from the distance, and be used as backgound props. 

How to use___________________________
Open the "prefabs" folder and drag and drop the desired model to the scene.
The proportions are consistent and they are real scale.
The object lighting parameters have been adjusted in all cases to "very low resolution".

Contact___________________________
Please email me if you need any assistance at dactilardesign@gmail.com