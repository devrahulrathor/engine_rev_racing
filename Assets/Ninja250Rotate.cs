﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja250Rotate : MonoBehaviour {

    public int ninja250rotate = 0;
    float anglerot = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {


        //    transform.position = MURCIELAGO640.transform.position + offset;
        //    transform.Translate(Vector3.forward * Time.deltaTime);

        //   transform.Translate(Vector3.forward * 100*Time.deltaTime, Camera.main.transform);
        // Move the object forward along its z axis 1 unit/second.
        //    transform.Translate(Vector3.forward * 100* Time.deltaTime);
        //    transform.Translate(0f, 1 * Time.deltaTime, -1 * 1.5f * Input.GetAxis("Horizontal") * 0);
        //        transform.Translate(0f, 0f, 2 * Time.deltaTime);
        //      transform.Translate(0f,  50 * Time.deltaTime, 0f);
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0f, 50 * Time.deltaTime, 0f);

        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0f, -20 * Time.deltaTime, 0f);

        }

        if (Input.GetKey(KeyCode.RightArrow) || ninja250rotate == 1)
        {

            transform.Rotate(0, 30 * Time.deltaTime, 0);
            anglerot = anglerot + 30 * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, -30 * Time.deltaTime, 0);
            anglerot = anglerot - 30 * Time.deltaTime;
        }

        // Stop and Freeze Rotation upon press of Space
        if (Input.GetKey(KeyCode.Space))
        {
            transform.Rotate(0, 30 * Time.deltaTime, 0);

        }
        else
            transform.Translate(0f, 0f, 0f);
        // Touch Module to Rotate Model_____________________________________________________
        if (Input.touchCount == 1)
        {
            if (AudioEngineNinja250.istart != 1)  // Touch Rotate Works only when Vehicle off
            {
                float rotateSpeed = 0.09f;
                Touch touchZero = Input.GetTouch(0);

                Vector2 pos = touchZero.position;
                if (pos.y > 150 && pos.x > 400 && pos.x < 1200)
                {
                    //   pos.x = (pos.x - width) / width;
                    //   pos.y = (pos.y - height) / height;


                    //Rotate the model based on offset
                    //     Vector3 localAngle = activeARfurniture.transform.localEulerAngles;
                    //    localAngle.y -= rotateSpeed * touchZero.deltaPosition.x;
                    //  activeARfurniture.transform.localEulerAngles = localAngle;
       //             transform.Rotate(0, -rotateSpeed * touchZero.deltaPosition.x, 0);
                    anglerot = anglerot - rotateSpeed * touchZero.deltaPosition.x;
                }
            }
        }
    }

    // StartStop Rotate from Touch
    public void RotatefromTouch()
    {
        //      moveSpeed = newSpeed;
      if (ninja250rotate == 0) ninja250rotate = 1;
       else if (ninja250rotate == 1) ninja250rotate = 0;

    }



}
