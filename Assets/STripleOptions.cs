using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class STripleOptions : MonoBehaviour
{
    // Use this for initialization

    public void ZX10RStock()
    {
        SceneManager.LoadScene(2);
    }

    public void ZX10RAkra()
    {
        SceneManager.LoadScene(5);
    }
    public void BackButton()
    {
        if (AudioEngineSTriple.itrate != -1) AudioEngineSTriple.istartfromtouch = 3;
        SceneManager.LoadScene(0);  // Motorbike Options
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
