﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja250OptionsButton : MonoBehaviour
{
    public GameObject Ninja250ViewsButton;
    public GameObject Ninja250ExhaustButton;
    //    public GameObject Ninj250OptionButtonBackground;
    //    public GameObject Ninja250CameraViewButton;
    public GameObject Ninja250CameraViewButton;
    public GameObject Ninja250PopsCracklesButton;



    public int iNinja250options = 0;   // Appear/Disapperar Counter
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AllOptionButtonsActive()  // 
    {
        if (iNinja250options == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Ninja250ViewsButton.SetActive(true);
            Ninja250ExhaustButton.SetActive(true);
            //      ZX10ROptionButtonBackground.SetActive(true);
                    Ninja250CameraViewButton.SetActive(true);
            Ninja250PopsCracklesButton.SetActive(true);
            iNinja250options = 1;

        }
        else if (iNinja250options == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            Ninja250ViewsButton.SetActive(false);
            Ninja250ExhaustButton.SetActive(false);
            Ninja250PopsCracklesButton.SetActive(false);
            //       ZX10ROptionButtonBackground.SetActive(false);
         Ninja250CameraViewButton.SetActive(false);
            iNinja250options = 0;

        }

    }


}
