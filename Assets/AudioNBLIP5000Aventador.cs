using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP5000Aventador : MonoBehaviour
{
    public static float dt5000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineS1000RR.itrate == 5004)
        {
            SoundEngineS1000RR.rpm1 = 5000;
            //           Debug.Log(SoundEngineS1000RR.rpm1);
        }
    }
    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip5000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (SoundEngineS1000RR.rpm1 >= 4000 && SoundEngineS1000RR.rpm1 < 5000)
        {
            audio.Play();
        }
        else if (SoundEngineS1000RR.rpm1 >= 3000 && SoundEngineS1000RR.rpm1 < 4000)
        {
            //       dt5000 = 0.052f;
            dt5000 = 0.043f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (SoundEngineS1000RR.rpm1 >= 2000 && SoundEngineS1000RR.rpm1 < 3000)
        {
            //    dt5000 = 0.096f;
            dt5000 = 0.083f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        else if (SoundEngineS1000RR.rpm1 < 2000)
            //      dt5000 = 0.212f;
            dt5000 = 0.202f;
        audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        audio.volume = 1;

        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip5000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip5000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip5000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}