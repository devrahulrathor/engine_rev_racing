﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPC4000Akra1 : MonoBehaviour {
    int[] posrpmarrayfsr4000 = { 4000, 3500, 3000, 2500, 2500, 2000, 1500, 1300, 1200, 1150, 1100, 1100, 1100, 1100, 1100, 1100 };

    double[] rpmposarrayfsr4000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngine.itrate == 6003)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
    }


    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngine.rpm1 = posrpmarrayfsr4000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngine.rpm1 <= (AudioEngine.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }

    public void BLIPC4000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1;
        audio.Play();
        audio.Play(44100);
    }

    public void BLIPC4000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    double Blip4000volume = 1.0;
    public void BLIPCR4000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip4000volume = Blip4000volume - 0.1;
        if (Blip4000volume > 0) audio.volume = (float)Blip4000volume;
        else
        {

            AudioEngine.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
            Blip4000volume = 1;
        }
    }
    public void Blipc4000Akra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blipc4000Akra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
   //    audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngine.ReverbXoneMix;
    }
}
