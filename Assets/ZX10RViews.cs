﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10RViews : MonoBehaviour
{
    public GameObject ZX10RGarageButton;
    public GameObject ZX10RRoadButton;
    public GameObject ZX10RTunnelButton;
    public GameObject ZX10RViewBackImage;
    public GameObject ZX10RViewForwardArrow;
    public GameObject ZX10RViewBackArrow;

    // Other Game Options Appear/Disappear
    public GameObject ZX10RExhaustOptions;
    public GameObject ZX10RViewOptions;
    public GameObject ZX10RPopsCracklesButton;


    public int iZX10Rview = 0;   // Appear/Disapperar Counter
    public int iZX10SwipeCounter = 1;   // Decide the View Option Active: 1 Garage Active, 2 Road Active
    int iZX10RLocation = 0; 

    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (iZX10RLocation == 100)  // Deactivated
        {
            if (Input.touchCount > 0)
            {
                Touch touchZero = Input.GetTouch(0);
                Vector2 pos = touchZero.position;

                if (iZX10SwipeCounter == 2)
                {
                    ZX10RViewForwardArrow.SetActive(true);
                    ZX10RViewBackArrow.SetActive(true);
                }
                else if (iZX10SwipeCounter == 3)
                {
                    ZX10RViewForwardArrow.SetActive(false);
                    ZX10RViewBackArrow.SetActive(true);
                }
                else if (iZX10SwipeCounter == 1)
                {
                    ZX10RViewForwardArrow.SetActive(true);
                    ZX10RViewBackArrow.SetActive(false);
                }

                if (pos.y > 600 && pos.y < 800 && pos.x > 100 && pos.x < 400)
                {
                    if (touchZero.deltaPosition.x < -10)
                    {
                        if (iZX10SwipeCounter == 1 && iswipeenable == 1)
                        {
                            ZX10RRoadButton.SetActive(true);
                            ZX10RGarageButton.SetActive(false);
                            ZX10RTunnelButton.SetActive(false);
                            iZX10SwipeCounter++;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                        else if (iZX10SwipeCounter == 2 && iswipeenable == 1)
                        {
                            ZX10RTunnelButton.SetActive(true);
                            ZX10RRoadButton.SetActive(false);
                            iZX10SwipeCounter++;

                            //                 iswipeenable = 0;
                            //                 StartCoroutine("EnableSwipe");
                        }

                    }
                    if (touchZero.deltaPosition.x > 10)
                    {
                        if (iZX10SwipeCounter == 3 && iswipeenable == 1)
                        {
                            //                  //        ZX10RTBR1Exhaust.SetActive(false);
                            ZX10RRoadButton.SetActive(true);
                            ZX10RTunnelButton.SetActive(false);
                            iZX10SwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }

                        else if (iZX10SwipeCounter == 2 && iswipeenable == 1)
                        {
                            ZX10RGarageButton.SetActive(true);
                            ZX10RRoadButton.SetActive(false);
                            //              ZX10RTunnelButton.SetActive(false);
                            iZX10SwipeCounter--;
                            iswipeenable = 0;
                            StartCoroutine("EnableSwipe");
                        }
                    }
                }
            }
        }
    }

    public void Locations()  // 
    {

        
        
        if (iZX10RLocation == 0)
        {
            ZX10RGarageButton.SetActive(true);
            ZX10RRoadButton.SetActive(true);
            ZX10RTunnelButton.SetActive(true);
            ZX10RExhaustOptions.SetActive(false);
            ZX10RViewOptions.SetActive(false);
            ZX10RPopsCracklesButton.SetActive(false);
            iZX10RLocation = 1;
        }
        else if (iZX10RLocation == 1)
        {
            ZX10RGarageButton.SetActive(false);
            ZX10RRoadButton.SetActive(false);
            ZX10RTunnelButton.SetActive(false);
            ZX10RExhaustOptions.SetActive(true);
            ZX10RViewOptions.SetActive(true);
            ZX10RPopsCracklesButton.SetActive(true);
            iZX10RLocation = 0;
        }
    }

    public void LocationsDeactivatefromOptions()  // Deactivate Location Menu When Pulling Up Options Tab
    {

        ZX10RGarageButton.SetActive(false);
        ZX10RRoadButton.SetActive(false);
        ZX10RTunnelButton.SetActive(false);
 
        iZX10RLocation = 0;

    }

        public void GarageRoadButtonsActive()  // 
    {
        if (iZX10Rview == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
  //          audio.Play();
            ZX10RGarageButton.SetActive(true);
            ZX10RRoadButton.SetActive(true);
    //        ZX10RViewBackImage.SetActive(true);


            iZX10Rview = 1;
        }
        else if (iZX10Rview == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
    //        audio.Play();
            ZX10RGarageButton.SetActive(true);
            ZX10RRoadButton.SetActive(true);
     //       ZX10RViewBackImage.SetActive(false);
            iZX10Rview = 0;
        }

    }


    // Waiting after one Menu Swipe
    IEnumerator EnableSwipe()
    {
        yield return new WaitForSeconds(0.25f);
        iswipeenable = 1;

    }
}
