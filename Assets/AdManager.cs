﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour
{
    private string APP_ID = "ca-app-pub-3940256099942544~3347511713";
    private BannerView bannerAD;
    private RewardBasedVideoAd rewardVideoAd;
    // Start is called before the first frame update
    public static AdManager instance;
    private void Awake()
    {
        DontDestroyOnLoad(this);
        Debug.LogError("awake admanager");
        instance = this;
    }
    void Start()
    {
        Debug.LogError("start admanager");
        MobileAds.Initialize(APP_ID);
        
        RequestVideoAD();
        RequestBanner();
    }

    void RequestBanner()
    {
        Debug.LogError("RequestBanner admanager");
        string banner_ID = " ca-app-pub-3940256099942544/6300978111";
        bannerAD = new BannerView(banner_ID, AdSize.SmartBanner, AdPosition.Top);
        AdRequest adRequest = new AdRequest.Builder()
        .AddTestDevice("2077ef9a63d2b398840261c8221a0c9b").Build();
        bannerAD.LoadAd(adRequest);
    }
 void RequestVideoAD()
    {
        Debug.LogError("RequestVideoAD admanager");

        string video_ID = "ca-app-pub-3940256099942544/5224354917";
        rewardVideoAd = RewardBasedVideoAd.Instance;
        Debug.LogError("RequestVideoAD admanager");

        // For Real App
        //    AdRequest adRequest = new AdRequest.Builder().Build();

        // For Testing
        Debug.LogError("RequestVideoAD admanager");

        AdRequest adRequest = new AdRequest.Builder().AddTestDevice("2077ef9a63d2b398840261c8221a0c9b").Build();
        rewardVideoAd.LoadAd(adRequest, video_ID);
        Debug.LogError("RequestVideoAD admanager");

    }


    public void Display_Reward_Video()
    {
        Debug.LogError("Display_Reward_Video admanager");

        if (rewardVideoAd.IsLoaded())
        {
        Debug.LogError("Display_Reward_Video admanager");
            rewardVideoAd.Show();
        }
    }
    public void Display_Banner()
    {
        Debug.LogError("Display_Banner admanager");
        bannerAD.Show();
    }

    // HANDLE EVENTS
    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        Debug.LogError("HandleRewardedAdLoaded admanager");

        MonoBehaviour.print("HandleRewardedAdLoaded event received");
        Display_Reward_Video();
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.LogError("HandleRewardedAdFailedToLoad admanager");

        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                            + args.Message);

        //ad failed to load load again
        RequestVideoAD();
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        Debug.LogError("HandleRewardedAdOpening admanager");

        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        Debug.LogError("HandleRewardedAdFailedToShow admanager");

        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdClosed event received");
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardedAdRewarded event received for "
                        + amount.ToString() + " " + type);
    }

    private void OnDestroy()
    {
        Debug.LogError("OnDestroy admanager");

    }
    //Banner Handle
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        //MonoBehaviour.print("HandleAdLoaded event received");
        Display_Banner();
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }


    void HandleVideoAdEvent(bool subscribe)
    {
        // Called when an ad request has successfully loaded.
        rewardVideoAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
  rewardVideoAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
   
        // Called when an ad is shown.
        rewardVideoAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        //        rewardVideoAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
   //       rewardVideoAd.OnUserEarnedReward += HandleUserEarnedReward;
    //    // Called when the ad is closed.
        rewardVideoAd.OnAdClosed += HandleRewardedAdClosed;

    }

    void HandleBannerADEvents(bool subscribe)
    {
        // Called when an ad request has successfully loaded.
        bannerAD.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerAD.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        bannerAD.OnAdOpening += this.HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        bannerAD.OnAdClosed += this.HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerAD.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;

    }
}
