﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeTranslate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Move the object forward along its z axis 1 unit/second.
        transform.Translate(Vector3.forward * 350*Time.deltaTime);
        transform.Translate(Vector3.left * 15 * Time.deltaTime);
        if (transform.position.x <= 1000 && transform.position.x > 500)
        {
                      transform.Rotate(0.0f, 0.0f, -0.1f, Space.Self);
          
            transform.Translate(Vector3.down * 2.0f * Time.deltaTime);
    //        transform.Translate(Vector3.left * 50 * Time.deltaTime, Space.World);
        }
        if (transform.position.x <= 500 && transform.position.x >= 200)
        {
            transform.Translate(Vector3.left * 20 * Time.deltaTime, Space.World);
            transform.Translate(Vector3.down * 1.0f * Time.deltaTime);
        }

        if (transform.position.x < 200 && transform.position.x > -600)
            {
                transform.Translate(Vector3.left * 70 * Time.deltaTime, Space.World);
                transform.Translate(Vector3.down * 1.7f * Time.deltaTime);
            }
     
        if (transform.position.x < -600)
        {
            transform.Translate(Vector3.left * 90 * Time.deltaTime, Space.World);
            transform.Translate(Vector3.down * 12.5f * Time.deltaTime);
        }

        // Move the object upward in world space 1 unit/second.
        //    transform.Translate(Vector3.up * Time.deltaTime, Space.World);
    }
}
