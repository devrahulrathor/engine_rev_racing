using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNFSR15000Corvette : MonoBehaviour
{
//    int[] posrpmarrayfsr15000 = { 5000, 5000, 5000, 4500, 4200, 3800, 3700, 3000, 2200, 2100, 1800, 1500, 1300, 1100 };
    int[] posrpmarrayfsr15000 = { 7000, 6800, 6600, 6400, 6300, 6200, 6100, 6000, 5900, 5800, 5600, 5400, 5200, 5000, 4800, 4600, 4400, 4200, 4000, 3800, 3600, 3400, 3200, 3000, 2800, 2600, 2400, 2200, 2000, 1800, 1600, 1400, 1200, 1100 };


    // double[] rpmposarrayfsr15000 = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    double[] rpmposarrayfsr15000 = { 0.1, 0.1, 0.1, 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.4, 0.4, 0.4, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 1.8, 2.1, 2.1, 2.1, 2.1, 2.1, 2.1, 2.1, 2.1, 2.1, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9, 2.9 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineCorvette.itrate == 6100)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
        LowPassFilter();
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineCorvette.rpm1 = posrpmarrayfsr15000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineCorvette.rpm1 <= (AudioEngineCorvette.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }


    public void NFSR15000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        //       audio.PlayScheduled(AudioSettings.dspTime + 0.504F);

        audio.Play();
        //      audio.Play(44100);
    }

    public void NFSR15000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    double Blip15000volume = 1.0;
    public void BLIPCR15000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip15000volume = Blip15000volume - 0.1;
        if (Blip15000volume > 0) audio.volume = (float)Blip15000volume;
        else
        {

            AudioEngineCorvette.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
        }
    }

    public void NFSR15000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void NFSR15000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineCorvette.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}
