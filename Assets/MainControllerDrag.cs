using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using UnityEngine.SceneManagement;

public class MainControllerDrag : MonoBehaviour
{
    double startTick;
    // Vehicle Selection Section
    public static int ivehicle = 1;     // 1-ZX25R, 2-Bullet500CH

    public UiControllerDrag UI;
    //    public SoundEngineZX10 SEZX10;  // Refernce of Sound Engine
    public SoundEngineZX25Drag SEZX25;  // Refernce of Sound Engine ZX25
    public SoundEngineBullet500Drag SEBullet500;  // Refernce of Sound Engine Bullet 500
    public GUIScript GUI;
  //  public ColorPickerObject CPicker;
  //  public ColorPickerRidingGear CPickerRidingGear;

    public int ikeyin = 0;    // Ignition Key in Counter
    public static int ikeyinfromtouch = 0;
    public int ikeyoffsound = 0; // Counter for activating key-off sound
    public static int istart = -100;    // Start Counter
    public static int istartfromtouch = 0;
    public int iconsolestartup = 0;  // Counter for Console Startup Function RPMs

    public Slider ThrottleSlider;  // Slider Spring Back Variable


    public static int idlingvol = 0;  // Counter to set idling volume 1 when engine starting 

    public int iconsolestartuprate = 250; // Counter for Speed of Startup Needle Sweep
    public static int gear = 0;    // gear Counter
    public static float trate = 0;
    public static float throttlediff = 0;

    public static int itrate = -1;  // var for RPM Sound play mode
    public static int irpm = -1;  // To let RPM be 0 before idling(Clash with AudioIdling Module where irpm =0 activates it
                                  //  public static int iblipcanstart = 1;  // Counter for Blip Start
                                  //  public static int iblipcanstop = 0;  // Blip Stop Counter
    public int iswitch = 1;  // Switch for Blip Stop
    public static int rpm1 = 0;

    int iidleneedlemoved = 0;  // Flag when needle moves(Stop immeidate movement)


    //    public static int irevlimitercanstart = 1;  // Counter for revlimiter to start

    public static int rpmidling = 1100;     // Not user can delete
    //   public float rpmdisplayafterstart = 1.4f;  // Time in seconds after which Idling RPM start post Startup
    //  public static int rpmblipconst;  // TO Stop Blip at different RPMs
    //  public float blipcinterval = 0;  // Time after which BlipC has to stop
    //    public Text RPMText;     // RPMs outputted to Canvas//
    public static float sliderval;
    public static float slidervalold;

    // Fade in out counters
    public static int ifadeoutnfsr = 0, ifadeinnfsr = 0;  // NFSR Fadeout Counter
    public float fadeoutfsrtime = 0;  // Fade out Duration
    public float fadeoutidlingtime = 0;  // Fade out Duration
    public float fadeoutslitime = 0;  // Fade out Duration(Unused)
    public float fadeoutfsi1time = 0;  // Fade out Duration(Unused)

    public static int ifadeinidling = 0, ifadeoutidling = 0;  // Not needed
    public static int ifadeoutsli = 0, ifadeoutfsi1 = 0;   // Fade out SLI/FSI1 after FSR Starts
    public static int ifadeoutblip = 0;  // Fade out Blip

    // View Counters Counters
    public GameObject GarageView;   // Appearance/Disappearance of Garage
    public static float ReverbXoneMix = 1.03f;               //   public static float ReverbXoneMix = 1.03f;   // Reverberation Value for Garage
    public GameObject RoadView;   // Appearance/Disappearance of Road
    public GameObject TunnelView;   // Appearance/Disappearance of Road

    // Akrapovic Counters
    public int exhaustDropDown = 0; // 0 Dropdown up, 1 Dropdown down
    public static int iexhaust = 0;  // 0 for Stock, 1 for Akra1,  
                                     //    public Button Exhaustbutton;  // Exhaust Option Button
    public GameObject Exhaustbutton;
    public GameObject ExhaustStockbutton;
    public GameObject StockExhaustTick;
    public GameObject ExhaustAkra1button;
    public GameObject Akra1ExhaustTick;
    public GameObject Akra1CarbonExhaustTick;
    public GameObject ExhaustAkra1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased
    public GameObject ExhaustTBR1PurPanel;    // Panel on Akra1 GUI to grey out, disappears when purchased

    public BinSaveLoad BinarySaveLoad;     // Gam Obj to Save Load Consumables in Binary

    public GameObject StockMuffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject StockMufflerShield;   // Appearance/Disappearance of Stock Muffler Shield
    public GameObject Akra1Muffler;   // Appearance/Disappearance of Stock Muffler
    public GameObject AkraBlackMuffler;   // Appearance/Disappearance of AkrapovicBlack
    public GameObject AkraCarbonMuffler;   // Appearance/Disappearance of AkrapovicCarbon
    public GameObject StockMufflerSoundSource;  // Activate/Deactivate Stock Sound (Interference stop issue at Blipc10000 
    public GameObject Akra1MufflerSoundSource;  // Activate/Deactivate Akra1 Sound (Interference stop issue at Blipc10000 



    // Backfire Section
    public ExhaustPopZX10R AVExhaustPop;
      public static int iPopCrackle = 0;  // 0-off, 1-On
    public static float dtpop = 0.75f;  // Delay on Backfire 
    public GameObject PopsCracklesHeader;  // Main Menu for Pops Crackles
    public GameObject PopsCrackles1;       // Pos Crackles1 Button
    public GameObject PopsCrackles1Tick;       // Pos Crackles1 Button Tick
                                               // Paid Options
    public AudioScriptCrackle10000 ACrackle10000;  // Crackle 10000
    public AudioScriptCrackle8000 ACrackle8000;  // Crackle 10000

    // Odometer Section
    public static float distance = 0, distancetoFuelEmpty = 5f;
    public static float fuelLevel = 100;
    public static int iFuellowWarning = 0;
    public static int iFuelOverWarning = 0;
    
    //Timer Variables
    float StartTimer = 0;
    int istarttimer = 0;  // 1 at Start and 0 at Stop
    float startertime = 0;  // Time during which Starter operates -Less during warmedup engine
    float warmuptime = 20; // Seconds to warm-up

    public Camera Camera;







    // GEAR SECTION________________________________________________________________________________________________________________
    //   Gear1
    public static float bikespeed = 0;  // Bike SPEED
    public Text SpeedText;
    public Text GearText;




    //Launch Control

    public AudioLaunchC8000 ANLaunch8000;
    public AudioLaunchCConst8000 ANLaunchC8000;
    public AudioLaunchCReduce ANLaunchReduce;

    int revslaunch;   // Revs upto which engine revs
    int revslaunchdrop; // Revs to which rpm drops at SLI
                        //    public int GameMode = 0;  // 0- Race/Ride Mode 1- Drag Mode
    public int iclutchrelease = 0;
    int ilaunchCengaged = 0;  // 1 for 8000 RPM hold, 2 for 4000 RPM hold

    //Store Section
    public static int ZX10ExhaustAkra1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval1 in Playerprefs
    public static int ZX10ExhaustTBR1 = 0;  // 1 When Purchased at Store, stored as 1 in nonconsumableval2 in Playerprefs

    // Bike Position
    public ZX10Rscript Bike;

    // Headlight Object
    public HeadLight Hlight;
    public RearLight Rlight;
    public GameObject HeadlightReflectionR, HeadlightReflectionL;  //Swithch off Headlight Road Reflections 


    // GUI Option Variables
    public static int ivibrator = 0; // Rev Limiter Vibrator on
    public static int ivibrating = 0; // Vibrating yes No

    //RateUs Variables
    public int irevcountrateus = 0;  // Counter to decide when user rates when the count is greated than irevcountmaxrateus
    public int irevcountmaxrateus = 50;
    int irateusloopdisable = 0;
    int irateusdialogdisable = 0;

    // Braking Section
    public static int ibrake = 0; // 1 When Pressed, 0, when not pressed, -1 when released 
    public static int ibrakingloop = 0;  // To enable Brakefromtouch activate once

    // Game Selection Variables
     int itrack=0;

    [SerializeField] private Image StartStopButton, StartStopButtonPressed, RedButtonOn, NeutralButtonOff, NeutralButtonOn, RedlineButton, IgnitionKeyOn, IgnitionKeyOff, RPM0, RPM1000, RPM1100, RPM1500, RPM2000, RPM2200, RPM2500, RPM2800, RPM3000, RPM3200, RPM3500, RPM3800, RPM4000, RPM4200, RPM4500, RPM4800, RPM5000, RPM5200, RPM5500, RPM6000, RPM6500, RPM7000, RPM7500, RPM7700, RPM8000, RPM8200, RPM8500, RPM8800, RPM9000, RPM9200, RPM9500, RPM9800, RPM10000, RPM10200, RPM10500, RPM10800, RPM11000, RPM11200, RPM11500, RPM11800, RPM12000, RPM12200, RPM12500, RPM12800, RPM13000, RPM13200, RPM13500, RPM13800, RPM14000, RPM14200, RPM14500, RPM14800, RPM15000;
    public GameObject ConsoleRPMOff, ConsoleRPM0, ConsoleRPM1000, ConsoleRPM1100, ConsoleRPM1500, ConsoleRPM2000, ConsoleRPM2200, ConsoleRPM2500, ConsoleRPM2800, ConsoleRPM3000, ConsoleRPM3200, ConsoleRPM3500, ConsoleRPM3800, ConsoleRPM4000, ConsoleRPM4200, ConsoleRPM4500, ConsoleRPM4800, ConsoleRPM5000, ConsoleRPM5200, ConsoleRPM5500, ConsoleRPM6000, ConsoleRPM6500, ConsoleRPM7000, ConsoleRPM7500, ConsoleRPM7700, ConsoleRPM8000, ConsoleRPM8200, ConsoleRPM8500, ConsoleRPM8800, ConsoleRPM9000, ConsoleRPM9200, ConsoleRPM9500, ConsoleRPM9800, ConsoleRPM10000, ConsoleRPM10200, ConsoleRPM10500, ConsoleRPM10800, ConsoleRPM11000, ConsoleRPM11200, ConsoleRPM11500, ConsoleRPM11800, ConsoleRPM12000, ConsoleRPM12200, ConsoleRPM12500, ConsoleRPM12800, ConsoleRPM13000, ConsoleRPM13200, ConsoleRPM13500, ConsoleRPM13800, ConsoleRPM14000, ConsoleRPM14200, ConsoleRPM14500, ConsoleRPM14800, ConsoleRPM15000, ConsoleRPM16000, ConsoleRPM17000;





    // Start is called before the first frame update
    void Start()
    {
        //  BinarySaveLoad.SaveZx10Akra1(0);
        //    Debug.Log("INside MainController Drag");
        // Loading Game as Per User Choice

      //  ivehicle = PlayerPrefs.GetInt("Vehicle");  // Vehicle
      //  GUI.VehicleActivate();
      //  itrack = PlayerPrefs.GetInt("Tracks");
      //  GUI.Locationdeactivate();
      //  if (itrack == 1) GUI.HighwayActivate();
      //  else if (itrack == 2) GUI.ModerCityDayActivate();
      //  else if (itrack == 3) GUI.ModerCityNightActivate();
      //  else if (itrack == 4) GUI.GreenLandDayActivate();
    }

    // Update is called once per frame
    void Update()
    {

        //Check for Purchased Exhausts
        if (ZX10ExhaustAkra1 == 0) BinarySaveLoad.LoadZX10Akra1();  // Binary Load Cnsumable Data
        if (ZX10ExhaustAkra1 == 1)
        {
            if (ExhaustAkra1PurPanel.activeInHierarchy == true)
            {
                {
                    ExhaustAkra1PurPanel.SetActive(false);  // Panel to Grey out disappears to highlight Akra1 GUI
                                                            //                   Debug.Log("Aftermarket Exhaust 1 Purchased");
                }
            }
        }


        // Ingnition On Module
        if (ikeyinfromtouch == 1)
        {
            Debug.Log("inside Main Loop Ignition on");
            ikeyinfromtouch = 2;
            ikeyoffsound = 1;  // Activate Key-off sound
            //         NeutralButtonOff.enabled = true;

            // Ignition Key Rotate to On Position
            IgnitionKeyOn.enabled = true;
            IgnitionKeyOff.enabled = false;


            RedButtonOn.enabled = true;
            iconsolestartup = 1;  // Console Startup Loop Enabler
                                  //   AStartup.Startup();
            SEZX25.StartupSound();
            NeutralButtonOff.enabled = false;
            NeutralButtonOn.enabled = true;
            StartCoroutine("ConsoleStartup");  // Lighing up of RPM Console when Key is pressed

            // Headlight on Module
            //      Hlight.HeadlightOn();


        }
        // Key Off Sound 
        else if (ikeyinfromtouch == 2 && ikeyoffsound == 1 && istartfromtouch != 2)
        {
            ikeyoffsound = 0;

            //           AStartdown.Startdown();
        }

        // Start Module
        if ((Input.GetKeyDown("space") && istart == 0) || istartfromtouch == 1)
        {
            // Enable Console neutral Buttons
            //        NeutralButtonOff.enabled = false;
            //        NeutralButtonOn.enabled = true;

            istart = 1;
            //     StartCoroutine("Startistart1");  // Starting the Loop for Events
            istartfromtouch = 2;

            gear = 0;

            if (ivehicle == 1) startertime = 0.65f;  // Ninja ZX
            else if (ivehicle == 2) startertime = 0.65f;  // Bullet 500
            else if (ivehicle == 3) startertime = 0.65f;   // Bullet 500

            //       AStarting.Starting();
            if (ivehicle == 1) SEZX25.StartingfromMainController();  // Call for Staring in Sound Engine
            if (ivehicle == 2)
            {
                Debug.Log("INside Starting Bullet500");
                SEBullet500.StartingfromMainController();  // Call for Staring in Sound Engine
            }
            Debug.Log("inside Maincontroller Idling");
            StartCoroutine("StartIdling");

            // Start Timer upon engine start to change idling speed
            //     StartTimer = 0.0f;
            istarttimer = 1; //Start Timer

            //       irpm = 0;

        }
        //     Debug.Log("RPM=");
        //     Debug.Log(rpm1);
        // Module to change starter time depending upon the time engine has run (Time Increment After Start Engine)
        if (istarttimer == 1) StartTimer += Time.deltaTime;
        //       elseif (istarttimer ==0) Start
        if (StartTimer < warmuptime) startertime = 0.65f;  // Changing Startertime after warm-up
        else startertime = 0.35f;

        // Shuttin Module
        if ((Input.GetKeyDown("space") && istart == 1) || istartfromtouch == 3)
        {
            istarttimer = 0; // Stop Timer
                             // Enable Console neutral Buttons
                             //       NeutralButtonOff.enabled = true;
                             //       NeutralButtonOn.enabled = false;
                             //      AShutting.Shutting();
            if(ivehicle == 1) SEZX25.ShuttingfromMainController();  // Call for Shutting on Sound Engine
            else if(ivehicle == 2) SEBullet500.ShuttingfromMainController();  // Call for Shutting on Sound Engine
          //  bikespeed = 0;
            istart = 0;
            istartfromtouch = 0;

            //       itrate = -1;
            //      SoundEngineZX10.irpm = -1;  // For stopping RPM at Idling

            // Stop all sounds

            ivibrating = 0; // Stop Vibrating after Rev Limter Stops

            //     rpm1 = SoundEngineZX25Drag.rpm11;
            Debug.Log("RPM in Shutting 1");
            Debug.Log(rpm1);
            RemoveRPMConsole();
            RPM0.enabled = true;
            ConsoleRPM0.SetActive(true);
        }
        // Throttle Spring back Module-reverts to zero position when  user leaves throttle

        if (istart == 1)
        {
            // Touch Release Module
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
                        //                Debug.Log("Touch Begun");
                        //             message = "Begun ";
                        break;
                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                        //   if (gear == 0) ThrottleSlider.value = 0.0f;
                        ThrottleSlider.value = 0.0f;
                        break;
                }
            }
            // Mouse Release Module
            if (Input.GetMouseButtonUp(0))
            {

                //      Debug.Log("Pressed primary button.");
                if (gear == 0) ThrottleSlider.value = 0.0f;
            }
        }

        //DIstance Calculation
       distance = distance + MainControllerDrag.bikespeed * (Time.deltaTime/3600);
        fuelLevel = 100 * (1 - (distance / distancetoFuelEmpty));
        Debug.Log("Fuel Level =" + fuelLevel);
        if (fuelLevel < 20 && iFuellowWarning == 0)
        {
            Debug.Log("Fuel Low");
            iFuellowWarning = 1;
            UI.FuelLowWarningNoteActivate();
            


        }
        if (fuelLevel <= 5 && iFuelOverWarning == 0)
        //  if (distance > distancetoFuelEmpty)
        {
            Debug.Log("Fuel Emply");
            // Stopping Physics before refuelling or reload game
              //  Time.timeScale = 0;
              //  AudioListener.pause = true;
            UI.FuelOverWarningNoteActivate();
            iFuelOverWarning = 1;



        }
       Debug.Log("Distance = " + distance);

        //Bikes Speed and Gear Print on Screen
        SpeedText.text = "SPEED: " + bikespeed;
        GearText.text = "G: " + gear;



        if(ivehicle == 1) rpm1 = SoundEngineZX25Drag.rpm1;  // Reading RPM from SoundEngine
         else if(ivehicle == 2) rpm1 = SoundEngineBullet500Drag.rpm1;                              //                          Debug.Log("RPM in MainController");
                                                                                              //                            Debug.Log(rpm1);
            
       

        
        //                            Debug.Log("RPM in MainController2");
       // Testing Throttle below                                                                                       //                            Debug.Log(SoundEngineZX25Drag.rpm1);
      trate = (sliderval - slidervalold);   // Calculating throttle rate (of change) to decide appropriate sound cycle in Sound Engine
                                              //ZX10R     throttlediff = (sliderval * 150 - rpm1); // Calculating throttle difference to decide appropriate sound cycle in Sound Engine
                                              //     if(ivehicle == 1) throttlediff = (sliderval * 160 - rpm1);
                                              //    Debug.Log("Thorottlediff = ");
                                              //    Debug.Log(throttlediff);
                                              //Refresh Throttle Consoles
 
        slidervalold = sliderval;  // Assign Slidervalue to Slidrold
        RemoveRPMConsole();
        RPMConsoleUpdate();
    }


    public void RPMConsoleUpdate()
    {

        // Idling Pulsating Needle
        //     Debug.Log("Inside RPMConsoleUpdate  RPM=");
        //     Debug.Log(rpm1);
        if (irpm == 0)
        {
            if (iidleneedlemoved == 0)
            {
                int randidle = Random.Range(0, 10);
                if (randidle >= 5)
                {
                    RPM1000.enabled = true;
                    RPM1100.enabled = false;
                }
                else if (randidle == 0)
                {
                    iidleneedlemoved = 1;
                    //        StartCoroutine("EnableIdleRPMNeedleMove");
                    //                   yield return new WaitForSeconds(0.25f);
                    RPM1100.enabled = true;
                    RPM1000.enabled = false;
                }
            }
        }
        if (rpm1 == 0) RPM0.enabled = true;
        if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0) RPM1000.enabled = true;
        if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0) RPM1000.enabled = true;
        if (rpm1 >= 1500 && rpm1 < 2000) RPM1500.enabled = true;
        if (rpm1 >= 2000 && rpm1 < 2200) RPM2000.enabled = true;
        if (rpm1 >= 2200 && rpm1 < 2500) RPM2200.enabled = true;
        if (rpm1 >= 2500 && rpm1 < 2800) RPM2500.enabled = true;
        if (rpm1 >= 2800 && rpm1 < 3000) RPM2800.enabled = true;
        if (rpm1 >= 3000 && rpm1 < 3200) RPM3000.enabled = true;
        if (rpm1 >= 3200 && rpm1 < 3500) RPM3500.enabled = true;
        if (rpm1 >= 3500 && rpm1 < 3800) RPM3800.enabled = true;
        if (rpm1 >= 3800 && rpm1 < 4000) RPM4000.enabled = true;
        if (rpm1 >= 4000 && rpm1 < 4200) RPM4200.enabled = true;
        if (rpm1 >= 4200 && rpm1 < 4500) RPM4500.enabled = true;
        if (rpm1 >= 4500 && rpm1 < 4800) RPM4800.enabled = true;
        if (rpm1 >= 4800 && rpm1 < 5000) RPM5000.enabled = true;
        if (rpm1 >= 5000 && rpm1 < 5200) RPM5200.enabled = true;
        if (rpm1 >= 5200 && rpm1 < 5500) RPM5500.enabled = true;
        if (rpm1 >= 5500 && rpm1 < 6000) RPM6000.enabled = true;
        if (rpm1 >= 6000 && rpm1 < 6500) RPM6500.enabled = true;
        if (rpm1 >= 6500 && rpm1 < 7000) RPM7000.enabled = true;
        if (rpm1 >= 7000 && rpm1 < 7500) RPM7500.enabled = true;
        if (rpm1 >= 7500 && rpm1 < 8000) RPM8000.enabled = true;
        if (rpm1 >= 8000 && rpm1 < 8500) RPM8500.enabled = true;
        if (rpm1 >= 8500 && rpm1 < 9000) RPM9000.enabled = true;
        if (rpm1 >= 9000 && rpm1 < 9500) RPM9500.enabled = true;
        if (rpm1 >= 9500 && rpm1 < 10000) RPM10000.enabled = true;
        if (rpm1 >= 10000 && rpm1 < 10500) RPM10500.enabled = true;
        if (rpm1 >= 10500 && rpm1 < 11000) RPM11000.enabled = true;
        if (rpm1 >= 11000 && rpm1 < 11500) RPM11500.enabled = true;
        if (rpm1 >= 11500 && rpm1 < 12000) RPM12000.enabled = true;
        if (rpm1 >= 12000 && rpm1 < 12500) RPM12500.enabled = true;
        if (rpm1 >= 12500 && rpm1 < 13000) RPM13000.enabled = true;
        if (rpm1 >= 13000 && rpm1 < 13500) RPM13500.enabled = true;
        if (rpm1 >= 13500 && rpm1 < 14000) RPM14000.enabled = true;
        if (rpm1 >= 14000 && rpm1 < 14500) RPM14500.enabled = true;
        if (rpm1 >= 14500 && rpm1 <= 15000) RPM15000.enabled = true;
        //         if (rpm1 > 14000) RedlineButton.enabled = true;
        else if (rpm1 < 14000) RedlineButton.enabled = false;


        // Console RPM Bars*****************************************************
        if (rpm1 > 1500 && irpm == 0)
        {
            //       ConsoleRPM500.SetActive(true);
            //      ConsoleRPM1000.SetActive(true);
            //      ConsoleRPM1100.SetActive(true);
        }

        if (rpm1 > 1500 && irpm != 0)
        {
            //      ConsoleRPM500.SetActive(true);
            //      ConsoleRPM1000.SetActive(true);
            //      ConsoleRPM1100.SetActive(true);
        }

        if (rpm1 >= 900 && rpm1 < 1500 && irpm == 0)
        {
            //       ConsoleRPM500.SetActive(true);
            ConsoleRPM1000.SetActive(true);
        }
        if (rpm1 >= 900 && rpm1 < 1500 && irpm != 0)
        {
            //             ConsoleRPM500.SetActive(true);
            ConsoleRPM1000.SetActive(true);
        }
        if (rpm1 == 0 && ikeyinfromtouch != 0) ConsoleRPM0.SetActive(true);
        if (rpm1 >= 1500 && rpm1 < 2000) ConsoleRPM1500.SetActive(true);
        if (rpm1 >= 2000 && rpm1 < 2200) ConsoleRPM2000.SetActive(true);
        if (rpm1 >= 2200 && rpm1 < 2500) ConsoleRPM2200.SetActive(true);
        if (rpm1 >= 2500 && rpm1 < 2800) ConsoleRPM2500.SetActive(true);
        if (rpm1 >= 2800 && rpm1 < 3000) ConsoleRPM2800.SetActive(true);
        if (rpm1 >= 3000 && rpm1 < 3200) ConsoleRPM3000.SetActive(true);
        if (rpm1 >= 3200 && rpm1 < 3500) ConsoleRPM3500.SetActive(true);
        if (rpm1 >= 3500 && rpm1 < 3800) ConsoleRPM3800.SetActive(true);
        if (rpm1 >= 3800 && rpm1 < 4000) ConsoleRPM4000.SetActive(true);
        if (rpm1 >= 4000 && rpm1 < 4200) ConsoleRPM4200.SetActive(true);
        if (rpm1 >= 4200 && rpm1 < 4500) ConsoleRPM4500.SetActive(true);
        if (rpm1 >= 4500 && rpm1 < 4800) ConsoleRPM4800.SetActive(true);
        if (rpm1 >= 4800 && rpm1 < 5000) ConsoleRPM5000.SetActive(true);
        if (rpm1 >= 5000 && rpm1 < 5200) ConsoleRPM5200.SetActive(true);
        if (rpm1 >= 5200 && rpm1 < 5500) ConsoleRPM5500.SetActive(true);
        if (rpm1 >= 5500 && rpm1 < 6000) ConsoleRPM6000.SetActive(true);

        if (rpm1 >= 6000 && rpm1 < 6500) ConsoleRPM6500.SetActive(true);
        if (rpm1 >= 6500 && rpm1 < 7000) ConsoleRPM7000.SetActive(true);

        if (rpm1 >= 7000 && rpm1 < 7500) ConsoleRPM7500.SetActive(true);
        //      if(rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM7700.SetActive(true);
        if (rpm1 >= 7500 && rpm1 < 8000) ConsoleRPM8000.SetActive(true);
        if (rpm1 >= 8000 && rpm1 < 8500) ConsoleRPM8500.SetActive(true);
        if (rpm1 >= 8500 && rpm1 < 9000) ConsoleRPM9000.SetActive(true);

        if (rpm1 >= 9000 && rpm1 < 9500) ConsoleRPM9500.SetActive(true);

        if (rpm1 >= 9500 && rpm1 < 10000) ConsoleRPM10000.SetActive(true);


        if (rpm1 >= 10000 && rpm1 < 10500) ConsoleRPM10500.SetActive(true);
        if (rpm1 >= 10500 && rpm1 < 11000) ConsoleRPM11000.SetActive(true);
        if (rpm1 >= 11000 && rpm1 < 11500) ConsoleRPM11500.SetActive(true);

        if (rpm1 >= 11500 && rpm1 < 12000) ConsoleRPM12000.SetActive(true);
        if (rpm1 >= 12000 && rpm1 < 12500) ConsoleRPM12500.SetActive(true);
        if (rpm1 >= 12500 && rpm1 < 13000) ConsoleRPM13000.SetActive(true);
        if (rpm1 >= 13000 && rpm1 < 13500) ConsoleRPM13500.SetActive(true);
        if (rpm1 >= 13500 && rpm1 < 14000) ConsoleRPM14000.SetActive(true);
        if (rpm1 >= 14000 && rpm1 < 14500) ConsoleRPM14500.SetActive(true);
        if (rpm1 >= 14500 && rpm1 <= 15000) ConsoleRPM15000.SetActive(true);
        if (rpm1 >= 15000 && rpm1 < 16000) ConsoleRPM16000.SetActive(true);
        if (rpm1 >= 16000 && rpm1 <= 18000) ConsoleRPM17000.SetActive(true);

        //*************************************************************************************


    }


    // Keyin from Touch
    public void KeyInFromTouch()
    {
        //      moveSpeed = newSpeed;
        //    Debug.Log("inside Key In MC &&&&&&&&&&&&&&&&&&&&&&&&");
        if (ikeyinfromtouch == 0)
        {
            //        Debug.Log("inside Key In MC2 &&&&&&&&&&&&&&&&&&&&&&&&");
            ikeyinfromtouch = 1;
            // Headlight on Module
            Hlight.HeadlightOn();
            Rlight.RearlightOn();
            ConsoleRPM0.SetActive(true);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(false);
            Debug.Log("Ignition On");
        }
        else if (ikeyinfromtouch == 2 && istartfromtouch != 0)
        {

            //     Debug.Log("Loop1");
            ikeyinfromtouch = 0;
            istartfromtouch = 3;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
            StartCoroutine("DelayRemoveConsoleRPM0");  //Due to inbility to remove Console RPM0 after directly shutting engine from key
            Debug.Log("Ignition Off1");
        }
        else if (ikeyinfromtouch == 2)
        {
            ikeyinfromtouch = 0;
            //        Debug.Log("Loop2");
            RedButtonOn.enabled = false;
            NeutralButtonOff.enabled = true;
            NeutralButtonOn.enabled = false;
            IgnitionKeyOn.enabled = false;
            IgnitionKeyOff.enabled = true;

            // Headlight off Module
            Hlight.HeadlightOff();
            Rlight.RearlightOff();
            ConsoleRPM0.SetActive(false);// COnsole Lights Off after Keyoff
            ConsoleRPMOff.SetActive(true);
            Debug.Log("Ignition Off2");
        }

    }

    private IEnumerator DelayRemoveConsoleRPM0()
    {
        yield return new WaitForSeconds(0.2f);
        ConsoleRPM0.SetActive(false);  // COnsole Lights Off after Keyoff
    }

    public void StartStopFromTouch()
    {
        //      moveSpeed = newSpeed;
        if (ikeyinfromtouch == 2)
        {
            if (istartfromtouch == 0) istartfromtouch = 1;
            else if (istartfromtouch == 2) istartfromtouch = 3;
        }
        // Make StartStopButton Dark
        StartStopButton.enabled = false;
        StartStopButtonPressed.enabled = true;
        //       StartCoroutine("StartStopButtonAppear");
    }
    // ReAppear StartStopButton(Original)


    // ReAppear StartStopButton(Original)
    public void Start1StopButtonAppear()  // 
    {

        StartStopButton.enabled = true;
        StartStopButtonPressed.enabled = false;

    }

    // Braking Sub
    public void BrakeFromTouch()
    {
      Debug.Log("Brake Activated Brake");

        if (ibrakingloop == 0) ibrake = 1;
        Debug.Log("Brakefromtouchloop 2");
        ibrakingloop++;
    }

    public void BrakeReleaseFromTouch()
    {

        Debug.Log("Brake Activated Release");
        ibrake = -1;
        ibrakingloop = 0;
    }
    // Appearance of Racetrack Road
    public void RoadAppear()  // 
    {
        //   SoundEngineZX10.ReverbXoneMix = 0.00f;
        RoadView.SetActive(true); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage
        TunnelView.SetActive(false); // DisAppearance of Tunnel

        HeadlightReflectionR.SetActive(false);
        HeadlightReflectionL.SetActive(false);
        SEZX25.RoadAppear();


    }
    // DisAppearance of Racetrack Road/Garage
    public void TunnelAppear()  // 
    {
        //    SoundEngineZX10.ReverbXoneMix = 1.1f;
        TunnelView.SetActive(true); // Disapperance of Garage
        RoadView.SetActive(false); // Appearance of Road
        GarageView.SetActive(false); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);
        SEZX25.TunnelAppear();

    }


    // DisAppearance of Racetrack Road
    public void GarageAppear()  // 
    {
        //    SoundEngineZX10.ReverbXoneMix = 1.03f;
        RoadView.SetActive(false); // DisAppearance of Road
        TunnelView.SetActive(false); // DisAppearance of Tunnel
        GarageView.SetActive(true); // Disapperance of Garage

        HeadlightReflectionR.SetActive(true);
        HeadlightReflectionL.SetActive(true);
        SEZX25.GarageAppear();

    }

    // Start Idling  
    private IEnumerator StartIdling()
    {
        yield return new WaitForSeconds(startertime); // wait half a secon


        //     AStarting.StartingStop();
        if (ivehicle == 1)
        {
            Debug.Log("INside Starting2 ZX25");
            SEZX25.StartIdlingfromMainController();  // Start Idling sound in Sound Engine
        }
        if (ivehicle == 2)
        {
            Debug.Log("INside Starting2 Bullet500");
            SEBullet500.StartIdlingfromMainController();  // Start Idling sound in Sound Engine
        }
                                                                    //     irpm = 0;
                                                                    //        rpm1 = rpmidling;
                                                                    //       if (MainControllerZX10.iexhaust == 0) AIdling.Idling();
                                                                    //       if (MainControllerZX10.iexhaust == 1) AIdlingAkra1.IdlingAkra1();
                                                                    // Red Console button Off
        RedButtonOn.enabled = false;
    }

    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        sliderval = sliderposition;

    }

    public void ThrottleGearON()  
    {
        if (gear > 0)
        {
      //      Debug.Log("Throttle Gear ON");
      //      trate = 1;
      //      throttlediff = 1;

        }
    }
    public void ThrottleGearOFF()
    {
        if (gear > 0)
        {
        //    Debug.Log("Throttle Gear OFF");
         //   trate = -1;
         //   throttlediff = -1;

        }
    }

    // Start Idling
    //    private IEnumerator StartIdling1()
    //   {
    //       yield return new WaitForSeconds(startertime); // wait half a secon


    //       AStarting.StartingStop();

    //      irpm = 0;
    //     rpm1 = rpmidling;
    //     if (ivehicle == 1)
    //    {
    //         if (iexhaust == 0) AIdling.Idling();
    //         if (iexhaust == 1) AIdlingAkra1.IdlingAkra1();
    //     }
    //     else if (ivehicle == 2)
    //      {
    //           AIdlingJawa.Idling();
    //      }
    //       else if (ivehicle == 3)
    //      {
    //          AIdlingBullet500.Idling();
    //      }
    // Red Console button Off
    //      RedButtonOn.enabled = false;
    //  }






    // Console Startup Sequence************************************************************
    int rpmi = 1000;   // Counter for rpm change for Console Startup Sequence
                       //   float wait=0.1;
    private IEnumerator ConsoleStartup()
    {
        yield return new WaitForSeconds(1f);
        while (iconsolestartup == 1 || iconsolestartup == -1)
        {
            yield return new WaitForSeconds(0.01f); // wait half a second

            RemoveRPMConsole();
            //     if (rpmi <= 7400)
            //     {
            if (iconsolestartup == 1 && rpmi <= 14500)
            {
                rpmi = rpmi + iconsolestartuprate * 2;
            }
            else iconsolestartup = -1;
            if (iconsolestartup == -1 && rpmi >= 1000)
            {
                rpmi = rpmi - iconsolestartuprate * 2;
                if (rpmi <= 900)
                {
                    //              RPM0.enabled = true;
                    rpmi = 0;
                    iconsolestartup = 0;
                }
            }

            if (rpmi < 900) RPM0.enabled = true;
            if (rpmi >= 900 && rpmi < 1500) RPM1000.enabled = true;
            if (rpmi >= 1500 && rpmi < 2000) RPM1500.enabled = true;
            if (rpmi >= 2000 && rpmi < 2200) RPM2000.enabled = true;
            if (rpmi >= 2200 && rpmi < 2500) RPM2200.enabled = true;
            if (rpmi >= 2500 && rpmi < 2800) RPM2500.enabled = true;
            if (rpmi >= 2800 && rpmi < 3000) RPM2800.enabled = true;
            if (rpmi >= 3000 && rpmi < 3200) RPM3000.enabled = true;
            if (rpmi >= 3200 && rpmi < 3500) RPM3500.enabled = true;
            if (rpmi >= 3500 && rpmi < 3800) RPM3800.enabled = true;
            if (rpmi >= 3800 && rpmi < 4000) RPM4000.enabled = true;
            if (rpmi >= 4000 && rpmi < 4200) RPM4200.enabled = true;
            if (rpmi >= 4200 && rpmi < 4500) RPM4500.enabled = true;
            if (rpmi >= 4500 && rpmi < 4800) RPM4800.enabled = true;
            if (rpmi >= 4800 && rpmi < 5000) RPM5000.enabled = true;
            if (rpmi >= 5000 && rpmi < 5200) RPM5200.enabled = true;
            if (rpmi >= 5200 && rpmi < 5500) RPM5500.enabled = true;
            if (rpmi >= 5500 && rpmi < 6000) RPM6000.enabled = true;
            if (rpmi >= 6000 && rpmi < 6500) RPM6500.enabled = true;
            if (rpmi >= 6500 && rpmi < 7000) RPM7000.enabled = true;
            if (rpmi >= 7000 && rpmi < 7500) RPM7500.enabled = true;
            if (rpmi >= 7500 && rpmi < 8000) RPM8000.enabled = true;
            if (rpmi >= 8000 && rpmi < 8500) RPM8500.enabled = true;
            if (rpmi >= 8500 && rpmi < 9000) RPM9000.enabled = true;
            if (rpmi >= 9000 && rpmi < 9500) RPM9500.enabled = true;
            if (rpmi >= 9500 && rpmi < 10000) RPM10000.enabled = true;
            if (rpmi >= 10000 && rpmi < 10500) RPM10500.enabled = true;
            if (rpmi >= 10500 && rpmi < 11000) RPM11000.enabled = true;
            if (rpmi >= 11000 && rpmi < 11500) RPM11500.enabled = true;
            if (rpmi >= 11500 && rpmi < 12000) RPM12000.enabled = true;
            if (rpmi >= 12000 && rpmi < 12500) RPM12500.enabled = true;
            if (rpmi >= 12500 && rpmi < 13000) RPM13000.enabled = true;
            if (rpmi >= 13000 && rpmi < 13500) RPM13500.enabled = true;
            if (rpmi >= 13500 && rpmi < 14000) RPM14000.enabled = true;
            if (rpmi >= 14000 && rpmi < 14500) RPM14500.enabled = true;
            if (rpmi >= 14500 && rpmi < 15000) RPM15000.enabled = true;


            // Console RPM Bars*****************************************************


            if (rpmi < 900)
            {
                //       Debug.Log("Activating Console 0 RPM");
                ConsoleRPM0.SetActive(true);
            }
            if (rpmi >= 900 && rpmi < 1500) ConsoleRPM1000.SetActive(true);
            if (rpmi >= 1500 && rpmi < 2000) ConsoleRPM1500.SetActive(true);

            if (rpmi >= 2000 && rpmi < 2200) ConsoleRPM2000.SetActive(true);
            if (rpmi >= 2200 && rpmi < 2500) ConsoleRPM2200.SetActive(true);
            if (rpmi >= 2500 && rpmi < 2800) ConsoleRPM2500.SetActive(true);
            if (rpmi >= 2800 && rpmi < 3000) ConsoleRPM2800.SetActive(true);
            if (rpmi >= 3000 && rpmi < 3200) ConsoleRPM3000.SetActive(true);
            if (rpmi >= 3200 && rpmi < 3500) ConsoleRPM3500.SetActive(true);
            if (rpmi >= 3500 && rpmi < 3800) ConsoleRPM3800.SetActive(true);
            if (rpmi >= 3800 && rpmi < 4000) ConsoleRPM4000.SetActive(true);
            if (rpmi >= 4000 && rpmi < 4200) ConsoleRPM4200.SetActive(true);
            if (rpmi >= 4200 && rpmi < 4500) ConsoleRPM4500.SetActive(true);
            if (rpmi >= 4500 && rpmi < 4800) ConsoleRPM4800.SetActive(true);
            if (rpmi >= 4800 && rpmi < 5000) ConsoleRPM5000.SetActive(true);
            if (rpmi >= 5000 && rpmi < 5200) ConsoleRPM5200.SetActive(true);
            if (rpmi >= 5200 && rpmi < 5500) ConsoleRPM5500.SetActive(true);
            if (rpmi >= 5500 && rpmi < 6000) ConsoleRPM6000.SetActive(true);

            if (rpmi >= 6000 && rpmi < 6500) ConsoleRPM6500.SetActive(true);
            if (rpmi >= 6500 && rpmi < 7000) ConsoleRPM7000.SetActive(true);

            if (rpmi >= 7000 && rpmi < 7500) ConsoleRPM7500.SetActive(true);
            //      if(rpmi >= 7500 && rpmi < 8000) ConsoleRPM7700.SetActive(true);
            if (rpmi >= 7500 && rpmi < 8000) ConsoleRPM8000.SetActive(true);
            if (rpmi >= 8000 && rpmi < 8500) ConsoleRPM8500.SetActive(true);
            if (rpmi >= 8500 && rpmi < 9000) ConsoleRPM9000.SetActive(true);

            if (rpmi >= 9000 && rpmi < 9500) ConsoleRPM9500.SetActive(true);

            if (rpmi >= 9500 && rpmi < 10000) ConsoleRPM10000.SetActive(true);


            if (rpmi >= 10000 && rpmi < 10500) ConsoleRPM10500.SetActive(true);
            if (rpmi >= 10500 && rpmi < 11000) ConsoleRPM11000.SetActive(true);
            if (rpmi >= 11000 && rpmi < 11500) ConsoleRPM11500.SetActive(true);

            if (rpmi >= 11500 && rpmi < 12000) ConsoleRPM12000.SetActive(true);
            if (rpmi >= 12000 && rpmi < 12500) ConsoleRPM12500.SetActive(true);
            if (rpmi >= 12500 && rpmi < 13000) ConsoleRPM13000.SetActive(true);
            if (rpmi >= 13000 && rpmi < 13500) ConsoleRPM13500.SetActive(true);
            if (rpmi >= 13500 && rpmi < 14000) ConsoleRPM14000.SetActive(true);
            if (rpmi >= 14000 && rpmi < 14500) ConsoleRPM14500.SetActive(true);
            if (rpmi >= 14500 && rpmi < 15000) ConsoleRPM15000.SetActive(true);
            if (rpmi >= 15000 && rpmi < 16000) ConsoleRPM16000.SetActive(true);
            if (rpmi >= 16000 && rpmi < 18000) ConsoleRPM17000.SetActive(true);



        }
        //      newImage = Image.FromFile(@"C:/User Data/VS/Software/Bass/Combined-Project/Blank-Application/Images/" + rpmString + ".png");
    }

    // Console RPM Remove
    public void RemoveRPMConsole()
    {
        RPM0.enabled = false;
        RPM1000.enabled = false;
        RPM1100.enabled = false;
        RPM1500.enabled = false;
        RPM2000.enabled = false;
        RPM2200.enabled = false;
        RPM2500.enabled = false;
        RPM2800.enabled = false;
        RPM3000.enabled = false;
        RPM3200.enabled = false;
        RPM3500.enabled = false;
        RPM3800.enabled = false;
        RPM4000.enabled = false;
        RPM4200.enabled = false;
        RPM4500.enabled = false;
        RPM4800.enabled = false;
        RPM5000.enabled = false;
        RPM5200.enabled = false;
        RPM5500.enabled = false;
        RPM6000.enabled = false;
        RPM6500.enabled = false;
        RPM7000.enabled = false;
        RPM7500.enabled = false;

        RPM8000.enabled = false;
        RPM8500.enabled = false;
        RPM9000.enabled = false;
        RPM9500.enabled = false;
        RPM10000.enabled = false;
        RPM10500.enabled = false;
        RPM11000.enabled = false;
        RPM11500.enabled = false;
        RPM12000.enabled = false;
        RPM12500.enabled = false;
        RPM13000.enabled = false;
        RPM13500.enabled = false;
        RPM14000.enabled = false;
        RPM14500.enabled = false;
        RPM15000.enabled = false;


        // RPM Console Section


        //        for (int irpmconsolexxx = 0; irpmconsolexxx < 15000; irpmconsolexxx = irpmconsolexxx + 100)
        //        {
        //            string name = $"ConsoleRPM " + "rpm1";
        //          ConsoleRPM500.name = "name";
        //          ConsoleRPM500.SetActive(false);
        //      }


        // Console View Bars
        //     ConsoleRPMOff.SetActive(false);

        ConsoleRPM0.SetActive(false);
        //      ConsoleRPM500.SetActive(false);      
        ConsoleRPM1000.SetActive(false);
        ConsoleRPM1500.SetActive(false);
        ConsoleRPM2000.SetActive(false);
        ConsoleRPM2200.SetActive(false);
        ConsoleRPM2500.SetActive(false);
        ConsoleRPM2800.SetActive(false);
        ConsoleRPM3000.SetActive(false);
        ConsoleRPM3200.SetActive(false);
        ConsoleRPM3500.SetActive(false);
        ConsoleRPM3800.SetActive(false);
        ConsoleRPM4000.SetActive(false);
        ConsoleRPM4200.SetActive(false);
        ConsoleRPM4500.SetActive(false);
        ConsoleRPM4800.SetActive(false);
        ConsoleRPM5000.SetActive(false);
        ConsoleRPM5200.SetActive(false);
        ConsoleRPM5500.SetActive(false);
        ConsoleRPM6000.SetActive(false);
        ConsoleRPM6500.SetActive(false);
        ConsoleRPM7000.SetActive(false);
        ConsoleRPM7500.SetActive(false);
        ConsoleRPM7700.SetActive(false);
        ConsoleRPM8000.SetActive(false);
        ConsoleRPM8200.SetActive(false);
        ConsoleRPM8500.SetActive(false);
        ConsoleRPM8800.SetActive(false);
        ConsoleRPM9000.SetActive(false);
        ConsoleRPM9200.SetActive(false);
        ConsoleRPM9500.SetActive(false);
        ConsoleRPM9800.SetActive(false);
        ConsoleRPM10000.SetActive(false);
        ConsoleRPM10200.SetActive(false);
        ConsoleRPM10500.SetActive(false);
        ConsoleRPM10800.SetActive(false);
        ConsoleRPM11000.SetActive(false);
        ConsoleRPM11200.SetActive(false);
        ConsoleRPM11500.SetActive(false);
        ConsoleRPM11800.SetActive(false);
        ConsoleRPM12000.SetActive(false);
        ConsoleRPM12200.SetActive(false);
        ConsoleRPM12500.SetActive(false);
        ConsoleRPM12800.SetActive(false);
        ConsoleRPM13000.SetActive(false);
        ConsoleRPM13200.SetActive(false);
        ConsoleRPM13500.SetActive(false);
        ConsoleRPM13800.SetActive(false);
        ConsoleRPM14000.SetActive(false);
        ConsoleRPM14200.SetActive(false);
        ConsoleRPM14500.SetActive(false);
        ConsoleRPM14800.SetActive(false);
        ConsoleRPM15000.SetActive(false);
        ConsoleRPM16000.SetActive(false);
        ConsoleRPM17000.SetActive(false);






        //     ConsoleRPM1100.SetActive(false);
        //     ConsoleRPM1500.SetActive(false);
        //     ConsoleRPM2000.SetActive(false);
        //     ConsoleRPM2200.SetActive(false);
        //     ConsoleRPM2500.SetActive(false);
        //     ConsoleRPM2800.SetActive(false);
        //     ConsoleRPM3000.SetActive(false);


    }


    //******* In Gear Routines
    // Keyin from Touch
    public void GearUp()
    {
        if ((gear == 0 && rpm1 == rpmidling) || gear > 0)
        {
            
            gear = gear + 1;
            if (ivehicle == 1) SEZX25.GearUpSEZX25();
            else if (ivehicle == 2) SEBullet500.GearUpSEBullet500();
            if (gear == 1)
            {
                GUI.ConsoleCameraActivate();  // Change View to Console View in Gear
            }
            //   if (gear == 1) itrate = 100;    // To enable Gear1 SLI
        }
    }
    public void GearDown()
    {
        if (gear > 0)
        {
            gear = gear - 1;
            if (ivehicle == 1) SEZX25.GearDownSEZX25();
            else if (ivehicle == 2) SEBullet500.GearDownSEBullet500();
        }

    }

    public void PopCrackleEnable()  // Enable/Disable Pop Crackles
    {
        if (iPopCrackle == 0)
        {
            iPopCrackle = 1;
            //     PopsCrackles1Tick.SetActive(true);


        }
        else if (MainControllerDrag.iPopCrackle == 1)
        {
            iPopCrackle = 0;
            //     PopsCrackles1Tick.SetActive(false);
        }
    }

}
