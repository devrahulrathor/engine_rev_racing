﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public GameObject SettingsWindow;
    public static int iSettingWindow = 0;
    public static float lowpasscutoffreq=5000;
    public static float turbovol=1;
    public Slider LowpassSlider;
    public Slider TurboVolSlider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void SettingsWindowOnOff()
    {
        if(iSettingWindow == 0)
        {
            SettingsWindow.SetActive(true);
            iSettingWindow = 1;
        }
        else if (iSettingWindow == 1)
        {
            SettingsWindow.SetActive(false);
            iSettingWindow = 0;
        }


    }


    public void Slidervalue1(float sliderposition)
    {
        Debug.Log("SiderPos=");
 
            Debug.Log(sliderposition);

    lowpasscutoffreq = sliderposition;

    }
    public void Slidervalue2(float sliderposition)
    {
      

        turbovol = sliderposition;

    }
}
