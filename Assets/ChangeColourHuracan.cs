﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColourHuracan : MonoBehaviour {
    public Material matbody1;
    public Color newcol;
    public KeyCode changecolo;
    public float icolorshade=0.3f;  // Control Color Shade Manually
    public Slider cSlider;  // Slider Spring Back Variable
    int icolorcounter=1;  //Decide which color dynamically updates: 1 Red, 2:Blue, 3:Green, 4:Yellow, 5: Black, 6: White
 //   public float sliderval;

    // Use this for initialization
    void Start()
    {

       ColourRefresh(); //Set Colour to default values 
    
    }

    // Update is called once per frame
    void Update()
    {
   //     Slidervalue();
    }
    public void ColorRed()
    {
        icolorcounter = 1;
        if (icolorcounter == 1)
        {
            ///  matbody1.color = Color.red;
            matbody1.color = Color.Lerp(Color.red, Color.black, icolorshade);

        }//    Debug.Log("Colour Change");
    }
    public void ColorBlue()
    {
        icolorcounter = 2;
        if (icolorcounter == 2)
        {
            //     matbody1.color = Color.blue;
            matbody1.color = Color.Lerp(Color.blue, Color.black, icolorshade);
        }
  //      Debug.Log("Colour Change");
    }

    public void ColorGreen()
    {
        icolorcounter = 3;
        if (icolorcounter == 3)
        {
            //   matbody1.color = Color.green;
            matbody1.color = Color.Lerp(Color.green, Color.black, icolorshade);
            // Debug.Log("Colour Change");
        }
    }
    public void ColorYellow()
    {
        icolorcounter = 4;
        if (icolorcounter == 4)
        {
            //    matbody1.color = Color.yellow;
            matbody1.color = Color.Lerp(Color.yellow, Color.black, icolorshade);
            //   Debug.Log("Colour Change");
        }
    }
    public void ColorBlack()
    {
        icolorcounter = 5;
        if (icolorcounter == 5)
        {
            //     matbody1.color = Color.black;
            matbody1.color = Color.Lerp(Color.black, Color.white, icolorshade);
            //     Debug.Log("Colour Change");
        }
    }

    public void ColorWhite()
    {
        icolorcounter = 6;
        if (icolorcounter == 6)
        {
            //   matbody1.color = Color.white;
            matbody1.color = Color.Lerp(Color.white, Color.black, icolorshade);
            //       Debug.Log("Colour Change");
        }
    }

    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        icolorshade = sliderposition;
        ColourRefresh();
     
        //     Debug.Log("Icolorshade=");
        //     Debug.Log(icolorshade);

    }
    void ColourRefresh()
    {
        if (icolorcounter == 1) ColorRed();
        if (icolorcounter == 2) ColorBlue();
        if (icolorcounter == 3) ColorGreen();
        if (icolorcounter == 4) ColorYellow();
        if (icolorcounter == 5) ColorBlack();
        if (icolorcounter == 6) ColorWhite();

    }

}
