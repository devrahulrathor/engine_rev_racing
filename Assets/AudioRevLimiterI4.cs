﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRevLimiterI4 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineI4.itrate == 10000)
        {
            //      RPMCal();
            // AudioEngineI4.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        //      Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineI4.ivibrating == 1)
        {
            //          AudioEngineI4.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (AudioEngineI4.ivibrator == 1)
            //       {
            if (AudioEngineI4.rpm1 == 15000) AudioEngineI4.rpm1 = 14000;
            else AudioEngineI4.rpm1 = 15000;
            //          else if (AudioEngineI4.rpm1 == 14000) AudioEngineI4.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //      audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineI4.ReverbXoneMix;
    }
}
