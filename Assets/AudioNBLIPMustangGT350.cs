﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPMustangGT350 : MonoBehaviour
{
    //  int[] posrpmarrayblip = { 1100, 1100, 1200, 1300, 1400, 1500, 1600, 1600, 1600, 1700, 1700, 1700, 1700, 1700, 1700, 1800, 1900, 1900, 2000, 2000, 2300, 2600, 2900, 3200, 3500, 3800, 4000, 4100, 4200, 4200, 4200, 4300, 4300, 4400, 4500, 4600, 4700, 4700, 4800, 4800, 4900, 5000, 5200, 5400, 5600, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 6900, 7000, 7000, 7100, 7200, 7200, 7300, 7400, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500 };
    int[] posrpmarrayblip = { 1100, 1100, 1200, 1300, 1400, 1500, 1600, 1600, 1700, 1800, 1800, 1900, 1900, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3200, 3400, 3600, 3800, 4000, 4200, 4400, 4600, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5700, 5800, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 6900, 7000, 7000, 7100, 7200, 7200, 7300, 7400, 7500, 7500, 7500, 7500,    7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500, 7500 };

    // int[] posrpmarrayblip = { 2000, 2200, 2400, 2500, 2800, 3000, 3200, 3500, 4000, 4200, 4500, 4700, 4800, 5000, 5200, 5400, 5600, 5800, 6000, 6200, 6500, 6600, 6700, 6800, 7000, 7200, 7400, 7500, 7600, 7800, 8000, 8000, 8000 };

    //   double[] rpmposarrayblip = { 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.07, 0.07, 0.07, 0.07, 0.17, 0.17, 0.17, 0.17, 0.17, 0.20, 0.20, 0.20, 0.20, 0.20, 0.20, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21, 0.22, 0.22, 0.22, 0.22, 0.22, 0.22, 0.23, 0.23, 0.23, 0.23, 0.23, 0.23, 0.23, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.26, 0.26, 0.26, 0.26, 0.28, 0.28, 0.28, 0.28, 0.32, 0.32, 0.32, 0.32, 0.36, 0.36, 0.36, 0.36, 0.37, 0.37, 0.37, 0.37, 0.41, 0.41, 0.41, 0.41, 0.42, 0.42, 0.42, 0.43, 0.43, 0.43, 0.43, 0.44, 0.44, 0.44, 0.44, 0.44, 0.45, 0.45, 0.45, 0.45, 0.47, 0.47, 0.47, 0.47, 0.48, 0.48, 0.48, 0.48, 0.51, 0.51, 0.51, 0.51, 0.53, 0.53, 0.53, 0.53, 0.55, 0.55, 0.55, 0.55, 0.58, 0.58, 0.58, 0.58, 0.61, 0.61, 0.61, 0.61, 0.62, 0.62, 0.62, 0.62, 0.62, 0.62 };
    double[] rpmposarrayblip = { 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.09, 0.09, 0.09, 0.09, 0.09, 0.09, 0.09, 0.09, 0.09, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.21, 0.22, 0.22, 0.22, 0.22, 0.22, 0.22, 0.22, 0.22, 0.22, 0.23, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.28, 0.31, 0.31, 0.31, 0.31, 0.31, 0.31, 0.31, 0.31, 0.31, 0.34, 0.34, 0.34, 0.34, 0.34, 0.34, 0.34, 0.34, 0.34, 0.39, 0.39, 0.39, 0.39, 0.39, 0.39, 0.39, 0.39, 0.39, 0.39, 0.42, 0.42, 0.42, 0.42, 0.42, 0.42, 0.42, 0.42, 0.42, 0.42, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45, 0.45 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineMustang.itrate == 5000)
        {
            RPMCal();
            //           Debug.Log(AudioEngineMustang.rpm1);
        }
    }

    int irpmpos = 0;
    double posinsec;
    public void Blip()
    {

        AudioSource audio = GetComponent<AudioSource>();

        irpmpos = (AudioEngineMustang.rpm1 / 50) - 21;
        //      Debug.Log("RPM  in Borla SLI=");
        //      Debug.Log(AudioEngineMustang.rpm1);
        //      Debug.Log("RPM Pos in Borla SLI=");
        //      Debug.Log(irpmpos);
        if (irpmpos < 1) irpmpos = 1;  // Bug while irpmpos == -21 error
        posinsec = rpmposarrayblip[irpmpos];
        //         textBox3.Text = posinsec.ToString();
        //       var Last10SecondsBytePos = Bass.BASS_ChannelSeconds2Bytes(streamslr1, durationInSeconds - 2);

        audio.time = (float)posinsec;
        //      audio.time = (float)posinsec;
        audio.volume = 0;
        audio.Play();
        audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();
        irpmarrayindex = (int)((audio.time * 10000) / 100);

        //       rpm = posrpmarrayblip[irpmarrayindex];
        //         textBox2.Text = position.ToString();
        //       if (audio.time <= 0.32)
        //         if(AudioEngineMustang.rpm1 <= 7000)
        //       {
        AudioEngineMustang.rpm1 = posrpmarrayblip[irpmarrayindex];
        //       }
        //     else AudioEngineMustang.rpm1 = 0;
        //     AudioEngineMustang.itrate = -1;

    }
    public void BlipStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    // Defunct as Response not good***************
    double Blipvolume = 1.0;
    public void BLIPFadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blipvolume = Blipvolume - 0.1;
        if (Blipvolume > 0) audio.volume = (float)Blipvolume;
        else
        {

            AudioEngineMustang.ifadeoutblip = 0;
            audio.Stop();
            audio.volume = 0;
            Blipvolume = 0;
        }
    }
    // **********************************************


}
