﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10RExhaustOptionsArrow : MonoBehaviour
{
    public GameObject ZX10RStockExhaust;
    public GameObject ZX10RAkra1Exhaust;

    public AudioEngine AEngine;
    public int iZX10Rview = 0;   // Appear/Disapperar Counter
    public int iZX10SwipeCounter = 1;   // Decide the View Option Active: 1 Garage Active, 2 Road Active
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void StockAkra1ButtonsActive()  // 
    {
        if (iZX10SwipeCounter == 1)
        {
            ZX10RStockExhaust.SetActive(true);
            ZX10RAkra1Exhaust.SetActive(false);
            iZX10SwipeCounter++;
        }
        else if (iZX10SwipeCounter == 2)
        {
            ZX10RStockExhaust.SetActive(false);
            ZX10RAkra1Exhaust.SetActive(true);
            iZX10SwipeCounter--;
        }

    }
}

