﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MustangVehicleOptions : MonoBehaviour
{ 
    public GameObject MustangGT500Button, MustangGT500;
    public GameObject MustangGT350Button, MustangGT350;
    public GameObject VehiclesButtonthisobject;
    public GameObject Cantchangecarwhileengineon;
    public static int iMustang = 500;  // GT 500: 500, Gt350: 350

    // Other Game Options Appear/Disappear
    //public GameObject MustangLocationOptions;
    //public GameObject MustangViewOptions;


    //   public GameObject ZX10RExhaustBackImage;
    public static int iMustangVehicle = 0, iMsutangthisVehiclebutton = 0;  // Appear/Disapperar Counter



//public static int iMustangExhaust1 = 0;   // Appear/Disapperar Counter


int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
                        // Start is called before the first frame update
void Update()
{

}


public void Vehicles()  // 
{


    if (iMustangVehicle == 0)
    {
            MustangGT500Button.SetActive(true);
            MustangGT350Button.SetActive(true);
       
        iMustangVehicle = 1;
    }
    else if (iMustangVehicle == 1)
    {
            MustangGT500Button.SetActive(false);
            MustangGT350Button.SetActive(false);
            iMustangVehicle = 0;
    }
}

public void VehicleDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
{
        MustangGT500Button.SetActive(false);
        MustangGT350Button.SetActive(false);
      

        iMustangVehicle = 0;
}
    public void VehicleButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
  //      Debug.Log("VehicleButton=");
  //      Debug.Log(iMsutangthisVehiclebutton);
        if (iMsutangthisVehiclebutton == 0)
        {
          
            VehiclesButtonthisobject.SetActive(false);
            iMsutangthisVehiclebutton = 1;
        }
        else if (iMsutangthisVehiclebutton == 1)
        {
            VehiclesButtonthisobject.SetActive(true);
            iMsutangthisVehiclebutton = 0;
        }
    }


    public void GT350Activate()
    {
        if (AudioEngineMustang.istartfromtouch != 2)
        {
            iMustang = 350;
            //       MustangGT350.SetActive(true);
            //       MustangGT500.SetActive(false);

        }
        else
        {
            Cantchangecarwhileengineon.SetActive(true);
            StartCoroutine("CarcantbestartedNoteOff");
        }
     //   MustangCustomizeOptions.iMsutangthisCustomizebutton = 0;
    }
    public void GT500Activate()
    {
        if (AudioEngineMustang.istartfromtouch != 2)
        { // Only works when Engine is off
            Debug.Log("Mustang GT500 Activat");
            iMustang = 500;
     //       MustangGT350.SetActive(false);
     //       MustangGT500.SetActive(true);
            // MustangCustomizeOptions.iMsutangthisCustomizebutton = 1;
        }
        else
        {
            Cantchangecarwhileengineon.SetActive(true);
            StartCoroutine("CarcantbestartedNoteOff");
        }
    }

    private IEnumerator CarcantbestartedNoteOff()
    {
        yield return new WaitForSeconds(3f);
        Cantchangecarwhileengineon.SetActive(false);
    }
}