using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000R35Tit : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR35.itrate == 5005)
        {
            AudioEngineR35.rpm1 = 6000;
            //           Debug.Log(AudioEngineR35.rpm1);
        }
        LowPassFilter();
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineR35.rpm1 >= 5400 && AudioEngineR35.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineR35.rpm1 >= 4000 && AudioEngineR35.rpm1 < 5400)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.136F);
        }
        else if (AudioEngineR35.rpm1 >= 3000 && AudioEngineR35.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.221F);
        }
        else if (AudioEngineR35.rpm1 >= 2000 && AudioEngineR35.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.273F);
        }
        else if (AudioEngineR35.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.344F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}



