using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenScript : MonoBehaviour
{
    public GameObject SplashScreen, TouchScreenText;
    int iSplash = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("SplashScreen") == 0)
        {
            StartCoroutine("TouchScreenTextAnimation");

        }
        else if (PlayerPrefs.GetInt("SplashScreen") == 1)
        {
            SplashScreen.SetActive(false);
        }
    }



    public void Screen1()
    {
        SplashScreen.SetActive(false);
       
    }
    public void splash_Activate()
    {
        SplashScreen.SetActive(true);

    }

    private IEnumerator TouchScreenTextAnimation()
    {
        PlayerPrefs.SetInt("SplashScreen", 1);  // Play Splash Screen Once pn startup
        while (true)
        {
            
            if (iSplash == 0)
            {
                yield return new WaitForSeconds(0.6f);
                TouchScreenText.SetActive(false);
                iSplash = 1;
            }
            else if (iSplash == 1)
            {
                yield return new WaitForSeconds(0.3f);
                TouchScreenText.SetActive(true);
                iSplash = 0;
            }
        }
    }

}
