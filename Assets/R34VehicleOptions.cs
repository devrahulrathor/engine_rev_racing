﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R34VehicleOptions : MonoBehaviour
{
    public GameObject R34GT500Button;
    public GameObject R34GT350Button;
    public GameObject VehiclesButtonthisobject;

    // Other Game Options Appear/Disappear
    //public GameObject R34LocationOptions;
    //public GameObject R34ViewOptions;


    //   public GameObject ZX10RExhaustBackImage;
    public static int iR34Vehicle = 0, iR34thisVehiclebutton = 0;  // Appear/Disapperar Counter



    //public static int iR34Exhaust1 = 0;   // Appear/Disapperar Counter


    int iswipeenable = 1;   // Counter for enabling/disabling swipe for time lag
                            // Start is called before the first frame update
    void Update()
    {

    }


    public void Vehicles()  // 
    {


        if (iR34Vehicle == 0)
        {
            R34GT500Button.SetActive(true);
            R34GT350Button.SetActive(true);

            iR34Vehicle = 1;
        }
        else if (iR34Vehicle == 1)
        {
            R34GT500Button.SetActive(false);
            R34GT350Button.SetActive(false);
            iR34Vehicle = 0;
        }
    }

    public void VehicleDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
        R34GT500Button.SetActive(false);
        R34GT350Button.SetActive(false);


        iR34Vehicle = 0;
    }
    public void VehicleButtonDeactivate()  // Deactivate Exhaust Menu When Pulling Up Options Tab
    {
             Debug.Log("VehicleButton=");
              Debug.Log(iR34thisVehiclebutton);
        if (iR34thisVehiclebutton == 0)
        {

            VehiclesButtonthisobject.SetActive(false);
            iR34thisVehiclebutton = 1;
        }
        else if (iR34thisVehiclebutton == 1)
        {
            VehiclesButtonthisobject.SetActive(true);
            iR34thisVehiclebutton = 0;
        }
    }




}