using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIScript : MonoBehaviour
{
    public GameObject Highway;
    public GameObject FantasticCityOpen;
    public GameObject ClassicCityDay;
    public GameObject GreenLandDay;
    public GameObject ZX25R, Bullet500;
    public GameObject ZX25GreenBody, ZX25OtherColorBody; // Activating dofferent parts with Green and no green textures to change color
    public GameObject SoundEngineZX25R, SoundEngineBullet500;
    public GameObject ZX25RSound, Bullet500Sound;
    public Material SkyboxMatDay1, SkyboxMatEvening, SkyboxMatEveningLate;

    //  public GameObject GreenLandNight;
    public static int ilocation = 1;  // 1 Highway, 2-ModernCityDay, 3-ModernCityNight, 4 GreenLandDay, 5 GreenLandnight

    public GameObject ConsoleCamera, ProfileCamera, HelmetCamera;

    public static int icameraview = 1; // 1-ConsoleView, 2:Profile View etc
    int ivehicle = 1; // 2-Bullet500, ZX25R

   //  Game Selection Variables
     int itrack = 0;

    // Update is called once per frame


    private void Start()
    {
        RenderSettings.skybox = SkyboxMatDay1;

       if( PlayerPrefs.GetInt("ZX25GreenColor") == 1)
        {
            ZX25GreenBody.SetActive(true);
            ZX25OtherColorBody.SetActive(false);
        }
       else
        {
            ZX25GreenBody.SetActive(false);
            ZX25OtherColorBody.SetActive(true);
        }

       //Pasted from Main Controller Drag restoring Vehicle and tracks
        ivehicle = PlayerPrefs.GetInt("Vehicle");  // Vehicle
        VehicleActivate();
        itrack = PlayerPrefs.GetInt("Tracks");
        Locationdeactivate();
        if (itrack == 1) ClassicCityDayActivate();
        else if (itrack == 2) GreenLandDayActivate();
        else if (itrack == 3) FantasticCityOpenActivate();
        else if (itrack == 4) HighwayActivate();
    }

    void Update()
    {
        
    }

    public void Locationdeactivate()
    {

        Highway.SetActive(false);
        ClassicCityDay.SetActive(false);
        FantasticCityOpen.SetActive(false);
        GreenLandDay.SetActive(false);
    //    GreenLandNight.SetActive(false);
    }

    public void ClassicCityDayActivate()
    {
        ClassicCityDay.SetActive(true);
    //    Physics.gravity = new Vector3(0, -300.0F, 0);
    }
    public void FantasticCityOpenActivate()
    {
        FantasticCityOpen.SetActive(true);
    //    Physics.gravity = new Vector3(0, -300.0F, 0);
    }
    public void GreenLandDayActivate()
    {
        ilocation = 4;
        GreenLandDay.SetActive(true);
    //    Physics.gravity = new Vector3(0, -300.0F, 0);
    }
    public void HighwayActivate()
    {
        ilocation = 1;
        Highway.SetActive(true);
        //    Physics.gravity = new Vector3(0, -300.0F, 0);
    }
    public void CameraViewChange()
    {
        if (MainControllerDrag.gear == 0)
        {
            Debug.Log("Inside Camera View icameravies" + icameraview);
            if (icameraview == 1)
            {
                Debug.Log("Inside Camera View 1");
                icameraview = 2;
                ProfileCamera.SetActive(true);
                ConsoleCamera.SetActive(false);
                HelmetCamera.SetActive(false);
            }

            else if (icameraview == 2)
            {
                Debug.Log("Inside Camera View 2");
                icameraview = 3;
                ConsoleCamera.SetActive(false);
                ProfileCamera.SetActive(false);
                HelmetCamera.SetActive(true);
            }
            else if(icameraview == 3)
            {
                Debug.Log("Inside Camera View 3");
                icameraview = 1;
                HelmetCamera.SetActive(false);
                ConsoleCamera.SetActive(true);
                ProfileCamera.SetActive(false);
                
            }
        }
    }
    public void ConsoleCameraActivate()
    {
        Debug.Log("Inside Console Camera Act");
        icameraview = 1;
        ConsoleCamera.SetActive(true);
        ProfileCamera.SetActive(false);
    }
    public void VehicleActivate()
    {
        ivehicle = PlayerPrefs.GetInt("Vehicle");  // Vehicle
        VehicleDeactivate();
        if (ivehicle == 1)
        {
            ZX25R.SetActive(true);
            SoundEngineZX25R.SetActive(true);
            ZX25RSound.SetActive(true);
            ivehicle = 1;
            MainControllerDrag.ivehicle = ivehicle;
        }
        else if (ivehicle == 2)
        {
            Bullet500.SetActive(true);
            SoundEngineBullet500.SetActive(true);
            Bullet500Sound.SetActive(true);
            ivehicle = 2;
            MainControllerDrag.ivehicle = ivehicle;
        }

    }
    public void VehicleDeactivate()
    {
        ZX25R.SetActive(false);
        SoundEngineZX25R.SetActive(false);
        ZX25RSound.SetActive(false);
        Bullet500.SetActive(false);
        Bullet500.SetActive(false);
        Bullet500Sound.SetActive(false);

    }


}
