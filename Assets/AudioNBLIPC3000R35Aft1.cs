﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIPC3000R35Aft1 : MonoBehaviour
{
    int[] posrpmarrayfsr3000 = { 3000, 2800, 2500, 2400, 2200, 1800, 1500, 1500, 1200, 1200, 1050, 1050, 1050 };

    double[] rpmposarrayfsr3000 = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.9, 0.9, 0.9, 0.9, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 1.4, 1.4, 1.5, 1.5, 1.6, 1.6, 1.6, 1.6, 1.7, 1.7, 1.8, 1.8, 1.9, 1.9, 1.9, 1.9, 2, 2.1, 2.2, 2.2 };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineR35.itrate == 6002)
        {
            RPMCal();
            //         RPMText.text = "RPM:" + rpm.ToString();

            //     Debug.Log(rpm);
        }
        LowPassFilter();
    }

    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();


        irpmarrayindex = (int)((audio.time * 1000) / 100);

        //      textBox2.Text = irpmarrayindex.ToString();
        //     rpm = posrpmarrayfsr[irpmarrayindex];
        AudioEngineR35.rpm1 = posrpmarrayfsr3000[irpmarrayindex];

        //    textBox2.Text = irpmpos.ToString();
        if (AudioEngineR35.rpm1 <= (AudioEngineR35.rpmidling + 50))
        {
            //             MessageBox.Show("Idling 2 ");
            //      isoundmode = 0;
            //      irpm = 1;
            //      iidling = 1;
            //      timer2.Interval = 1;
            //      timer1.Interval = 1;
        }

    }

    public void BLIPC3000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = 1;
        //      audio.PlayScheduled(AudioSettings.dspTime + 0.161F);

        audio.Play();
        audio.Play(44100);
    }

    public void BLIPC3000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    double Blip3000volume = 1.0;
    public void BLIPCR3000Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Blip3000volume = Blip3000volume - 0.1;
        if (Blip3000volume > 0) audio.volume = (float)Blip3000volume;
        else
        {

            AudioEngineR35.ifadeoutnfsr = 0;
            audio.Stop();
            audio.volume = 1;
            Blip3000volume = 1;
        }
    }

    public void Blipc3000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blipc3000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineR35.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}
