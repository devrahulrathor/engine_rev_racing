﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000Drag1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineDrag.itrate == 5005)
        {
            AudioEngineDrag.rpm1 = 6000;
            //           Debug.Log(AudioEngineDrag.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineDrag.rpm1 >= 5000 && AudioEngineDrag.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineDrag.rpm1 >= 4000 && AudioEngineDrag.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.02F);
        }
        else if (AudioEngineDrag.rpm1 >= 3000 && AudioEngineDrag.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.072F);
        }
        else if (AudioEngineDrag.rpm1 >= 2000 && AudioEngineDrag.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.136F);
        }
        else if (AudioEngineDrag.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.232F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineDrag.ReverbXoneMix;
    }
}
