﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP7000I4 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (!audio.isPlaying)
        {
            //         Debug.Log("Blip7000 is Playing***********************");
        }
        if (AudioEngineI4.itrate == 5006)
        {
            AudioEngineI4.rpm1 = 7000;
            //           Debug.Log(AudioEngineI4.rpm1);
        }
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip7000()
    {

        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineI4.rpm1 >= 6000 && AudioEngineI4.rpm1 < 7000)
        {
            audio.Play();
        }
        else if (AudioEngineI4.rpm1 >= 5000 && AudioEngineI4.rpm1 < 6000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.093F);
        }
        else if (AudioEngineI4.rpm1 >= 4000 && AudioEngineI4.rpm1 < 5000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.149F);
        }
        else if (AudioEngineI4.rpm1 >= 3000 && AudioEngineI4.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.189F);
        }
        else if (AudioEngineI4.rpm1 >= 2000 && AudioEngineI4.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.345F);
        }


        else if (AudioEngineI4.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.550F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip7000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip7000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip7000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //  audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineI4.ReverbXoneMix;
    }
}

