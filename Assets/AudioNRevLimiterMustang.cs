﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterMustang : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineMustang.itrate == 7000)
        {
            //      RPMCal();
            // AudioEngineMustang.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineMustang.ivibrating == 1)
        {
            //          AudioEngineMustang.rpm1 = 15000;
            yield return new WaitForSeconds(0.08f);
            //       if (AudioEngineMustang.ivibrator == 1)
            //       {
            if (AudioEngineMustang.rpm1 == 7500) AudioEngineMustang.rpm1 = 7000;
            else AudioEngineMustang.rpm1 = 7500;
            //          else if (AudioEngineMustang.rpm1 == 14000) AudioEngineMustang.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }
}

