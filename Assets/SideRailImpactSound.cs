using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideRailImpactSound : MonoBehaviour
{
    // Start is called before the first frame update

    public void SideRailImpactPlay()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }
    public void SideRailImpactStop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}
