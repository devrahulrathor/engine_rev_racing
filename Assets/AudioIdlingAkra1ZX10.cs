using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioIdlingAkra1ZX10 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (SoundEngineZX10.irpm == 0)
        {
            SoundEngineZX10.rpm1 = SoundEngineZX10.rpmidling;
        }
    }

    public void IdlingAkra1()
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = SoundEngineZX10.idlingvol;
        audio.Play();
        audio.Play(44100);


    }
    public void IdlingAkra1Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        audio.volume = 0;
        //       Idlingvolume = 0;


    }

    double Idlingvolume = 1.0;
    public void IdlingAkra1Fadeout()
    {
        AudioSource audio = GetComponent<AudioSource>();
        Idlingvolume = Idlingvolume - 0.1;
        if (Idlingvolume > 0) audio.volume = (float)Idlingvolume;
        else
        {

            SoundEngineZX10.ifadeoutidling = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void IdlingAkra1Fadein()
    {
        AudioSource audio = GetComponent<AudioSource>();

        Idlingvolume = Idlingvolume + 0.1;
        if (Idlingvolume < 1) audio.volume = (float)Idlingvolume;
        else
        {
            audio.volume = 1;
            SoundEngineZX10.ifadeinidling = 0;

            //            audio.Stop();
        }
    }

    public void IdlingAkra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void IdlingAkra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineZX10.ReverbXoneMix;
    }
}

