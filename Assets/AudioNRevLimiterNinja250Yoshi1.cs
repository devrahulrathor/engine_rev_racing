﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterNinja250Yoshi1 : MonoBehaviour { 
    // Use this for initialization
    void Start () {

}

// Update is called once per frame
void Update()
{

    if (AudioEngineNinja250.itrate == 10000)
    {
        //      RPMCal();
        // AudioEngineNinja250.rpm1 = 15000;

    }

}

public void RevLimiter()
{
    StartCoroutine("RevLimiterRPMFluctuation");
    AudioSource audio = GetComponent<AudioSource>();
    audio.Play();
    audio.Play(44100);
}

public void RevLimiterStop()
{
    Debug.Log("RevLimiterStop");
    AudioSource audio = GetComponent<AudioSource>();
    audio.Stop();
    StopCoroutine("RevLimiterRPMFluctuation");
}


IEnumerator RevLimiterRPMFluctuation()
{

    while (AudioEngineNinja250.ivibrating == 1)
    {
        //          AudioEngineNinja250.rpm1 = 15000;
        yield return new WaitForSeconds(0.05f);
        //       if (AudioEngineNinja250.ivibrator == 1)
        //       {
        if (AudioEngineNinja250.rpm1 == 12500) AudioEngineNinja250.rpm1 = 13000;
        else AudioEngineNinja250.rpm1 = 12500;
        //          else if (AudioEngineNinja250.rpm1 == 14000) AudioEngineNinja250.rpm1 = 15000;
        //          }
    }

}


public void RevLimiterReverbRoad()
{
    AudioSource audio = GetComponent<AudioSource>();
    audio.reverbZoneMix = 0.0f;
}

public void RevLimiterReverbGarage()
{
    AudioSource audio = GetComponent<AudioSource>();
    audio.reverbZoneMix = AudioEngineNinja250.ReverbXoneMix;
}

}
