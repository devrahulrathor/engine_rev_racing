﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class BinSaveLoad : MonoBehaviour
{
    void Sart()
    {

    }

    public static void Save()
    {
        //     BinaryFormatter bf = new BinaryFormatter();
        //      FileStream file = File.Open(Application.persistentDataPath + "/save.dat", FileMode.OpenOrCreate);
        //    SerializableSaveData serializableSaveData = new SerializableSaveData();
        //    bf.Serialize(file, serializableSaveData);
        //    file.Close();
        //Debug.Log (SaveData.currentLevel);
    }

    // ZX10R Section
    int izx10akra1 = 0;
    public void SaveZx10Akra1(int a)
    {
        //
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/zx10akra1.dat")))

        {
            Debug.Log("File Exists ZX10akra1");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/zx10akra1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist ZX10akra1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/zx10akra1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if(a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadZX10Akra1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/zx10akra1.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/zx10akra1.dat"), FileMode.Open);
          //      MainControllerZX10.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
          AudioEngine.ZX10ExhaustAkra1 = (int)bf.Deserialize(file);
            //          Debug.Log("ZX10Akra1=");
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }
    // CB400 Section
    int iCB400BMSR = 0;
    public void SaveCB400Akra1(int a)
    {
        //
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/CB400BMSR.dat")))

        {
    //        Debug.Log("File Exists CB400BMSR.dat");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CB400BMSR.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();

            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist CB400BMSR.dat");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CB400BMSR.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
          
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadCB400Akra1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/CB400BMSR.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CB400BMSR.dat"), FileMode.Open);
            AudioEngineI4.CB400Akra1 = (int)bf.Deserialize(file);

            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }


    }

    // Ninja400 Section
    int ininja250Yoshi1 = 0;
    public void SaveNinja250Yoshi1(int a)
    {
        //
        int nonconsumableval1 = 0;
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/ninja250yoshi1.dat")))

        {
            Debug.Log("File Exists ninja250yoshi1.dat");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/ninja250yoshi1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist ninja250yoshi1.dat");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/ninja250yoshi1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
           
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }
    public void LoadNinja250Yoshi1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/ninja250yoshi1.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/ninja250yoshi1.dat"), FileMode.Open);
            AudioEngineNinja250.Ninja250ExhaustYoshi1 = (int)bf.Deserialize(file);
            //          Debug.Log("ZX10Akra1=");
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    // Huracan Section
    int iHuracanmode = 0;
    public void SaveHuracanMode(int a)
    {
        //
        int nonconsumableval1 = 0;
        Debug.Log("File Exists Huracan Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/huracanmode.dat")))

        {
            Debug.Log("File Exists Huracan Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/huracanmode.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist HuracanMode");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/huracanmode.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
          
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadHuracanMode()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/huracanmode.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/huracanmode.dat"), FileMode.Open);
       //     AudioEngineS1000RRAkra.HuracanSportMode = (int)bf.Deserialize(file);
       MainControllerS1000RR.HuracanSportMode = (int)bf.Deserialize(file);
            //          Debug.Log("ZX10Akra1=");
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }


    // Mustang Section
    int iMustangRoush1 = 0;
    public void SaveMustangRoush1(int a)
    {
        //
        int nonconsumableval1 = 0;
      Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/MustangRoush1.dat")))

        {
            Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/MustangRoush1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
           Debug.Log("File doesn't Exist MustangRoush1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/MustangRoush1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            Debug.Log(" Mustang Roush Exhaust Set to 1 on BinSave");
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadMustangRoush1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/MustangRoush1.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/MustangRoush1.dat"), FileMode.Open);
            AudioEngineMustang.MustangRoushPurchased = (int)bf.Deserialize(file);
      //               Debug.Log("MustangRoush=");
      //              Debug.Log(AudioEngineMustang.MustangRoushPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }


    // Borla Section
    int iMustangBorla1 = 0;
    public void SaveMustangBorla1(int a)
    {
        //
        int nonconsumableval1 = 0;
        Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/MustangBorla1.dat")))

        {
            Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/MustangBorla1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist MustangBorla1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/MustangBorla1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;
            Debug.Log(" Mustang Borla Exhaust Set to 1 on BinSave");
            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }
    public void LoadMustangBorla1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/MustangBorla1.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/MustangBorla1.dat"), FileMode.Open);
            AudioEngineMustang.MustangBorla1Purchased = (int)bf.Deserialize(file);
            //          Debug.Log("ZX10Akra1=");
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }
   
    
    // Jawa2s Section
    int iJawa2sStock = 0;
    public void SaveJawa2sStock1(int a)
    {
        //
        int nonconsumableval1 = 0;
     //   Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/Jawa2sStock1.dat")))

        {
   //         Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/Jawa2sStock1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
     //       Debug.Log("File doesn't Exist MustangRoush1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/Jawa2sStock1.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;

            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadJawa2sStock1()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/Jawa2sStock1.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/Jawa2sStock1.dat"), FileMode.Open);
            AudioEngineJawa2s.Jawa2sExhaustStock = (int)bf.Deserialize(file);
            //          Debug.Log("ZX10Akra1=");
            //          Debug.Log(AudioEngine.ZX10ExhaustAkra1);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    // NISSAN GTR R34 Section
    int iR34StockExhaust = 0;
    public void SaveR34StockExhaust(int a)
    {
        //
        int nonconsumableval1 = 0;
        Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/R34StockExhaust.dat")))

        {
      //      Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R34StockExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist MustangRoush1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R34StockExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;

            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadR34StockExhaust()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/R34StockExhaust.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R34StockExhaust.dat"), FileMode.Open);
            AudioEngineR34.R34StockPurchased = (int)bf.Deserialize(file);
            //               Debug.Log("MustangRoush=");
            //              Debug.Log(AudioEngineMustang.MustangRoushPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    // Corvette Stingray
    int iCorvetteStockExhaust = 0;
    public void SaveCorvetteStockExhaust(int a)
    {
        //
        int nonconsumableval1 = 0;
        Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/CorvetteStockExhaust.dat")))

        {
            //      Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CorvetteStockExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist MustangRoush1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CorvetteStockExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;

            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadCorvetteStockExhaust()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/CorvetteStockExhaust.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CorvetteStockExhaust.dat"), FileMode.Open);
            AudioEngineCorvette.CorvetteStockPurchased = (int)bf.Deserialize(file);
            //               Debug.Log("MustangRoush=");
            //              Debug.Log(AudioEngineMustang.MustangRoushPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }
    public void SaveTryitonceCorvetteStockTitaniumExhaust(int a)
    {
        //


        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CvtStExTrl.dat"), FileMode.OpenOrCreate);
        //SaveData saveData = new SaveData();
        bf.Serialize(file, 1);
        file.Close();
    }
    public void LoadTryitonceCorvetteStockTitaniumExhaust()
    {
        //
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/CvtStExTrl.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/CvtStExTrl.dat"), FileMode.Open);
            //     AudioEngineCorvette.CorvetteStockPurchased = (int)bf.Deserialize(file);
            AudioEngineCorvette.itryitonce = (int)bf.Deserialize(file);
            //               Debug.Log("MustangRoush=");
            //              Debug.Log(AudioEngineMustang.MustangRoushPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            AudioEngineCorvette.itryitonce = 0;
            //      Debug.Log("Binary ZX10akra1 not found");
        }

    }




    // NISSAN GTR R35 Section
    int iR35StockTitaniumExhaust = 0;
    public void SaveR35StockTitaniumExhaust(int a)
    {
        //
        int nonconsumableval1 = 0;
        Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/R35StockTitaniumExhaust.dat")))

        {
            //      Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R35StockTitaniumExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist MustangRoush1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R35StockTitaniumExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;

            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadR35StockTitaniumExhaust()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/R35StockTitaniumExhaust.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R35StockTitaniumExhaust.dat"), FileMode.Open);
            AudioEngineR35.R35StockTitaniumPurchased = (int)bf.Deserialize(file);
            //               Debug.Log("MustangRoush=");
            //              Debug.Log(AudioEngineMustang.MustangRoushPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

    public void SaveTryitonceR35StockTitaniumExhaust(int a)
    {
        //
       
      
        BinaryFormatter bf = new BinaryFormatter();
    
        FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R3SSTE.dat"), FileMode.OpenOrCreate);
        //SaveData saveData = new SaveData();
         bf.Serialize(file, 1);
        file.Close();
    }
    public void LoadTryitonceR35StockTitaniumExhaust()
    {
        //
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/R3SSTE.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/R3SSTE.dat"), FileMode.Open);
            //     AudioEngineR35.R35StockTitaniumPurchased = (int)bf.Deserialize(file);
            AudioEngineR35.itryitonce = (int)bf.Deserialize(file);
          //                Debug.Log("LOad file exists R35 Titanium =");
          //              Debug.Log(AudioEngineR35.R35StockTitaniumPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            AudioEngineR35.itryitonce = 0;
                Debug.Log("Binary R35Stock Titan not found");
        }

    }
    // Triupmh Speed Triple
    int iCSTripleArrowExhaust = 0;
    public void SaveSTripleArrowExhaust(int a)
    {
        //
        int nonconsumableval1 = 0;
        Debug.Log("File Exists Mustang Mode 0");
        BinaryFormatter bf = new BinaryFormatter();
        //  FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.OpenOrCreate);
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/STripleArrowkExhaust.dat")))

        {
            //      Debug.Log("File Exists Mustang Mode");

            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/STripleArrowkExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 0) nonconsumableval1 = 0;
            else if (a == 1) nonconsumableval1 = 1;
            else if (a == 3) nonconsumableval1 = 1;
            if (a != 2)
            {
                bf.Serialize(file, nonconsumableval1);
            }
            file.Close();
        }
        else
        {
            Debug.Log("File doesn't Exist MustangRoush1");
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/STripleArrowkExhaust.dat"), FileMode.OpenOrCreate);
            //SaveData saveData = new SaveData();
            if (a == 3 || a == 1) nonconsumableval1 = 1;

            else nonconsumableval1 = 0;

            bf.Serialize(file, nonconsumableval1);
            file.Close();
        }
    }

    public void LoadSTripleArrowExhaust()
    {
        //     string filename = "/filename.dat";
        //    if (File.Exists(Application.persistentDataPath + "/zx10akra1.dat"))
        if (File.Exists(Path.Combine(Application.persistentDataPath + "/STripleArrowkExhaust.dat")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //       FileStream file = File.Open(Application.persistentDataPath + "/zx10akra1.dat", FileMode.Open);
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath + "/STripleArrowkExhaust.dat"), FileMode.Open);
            AudioEngineSTriple.STripleExhaustArrow = (int)bf.Deserialize(file);
            //               Debug.Log("MustangRoush=");
            //              Debug.Log(AudioEngineMustang.MustangRoushPurchased);
            file.Close();
            //      return true;
        }
        else
        {
            //      Debug.Log("Binary ZX10akra1 not found");
        }
        //  return false;
    }

}
