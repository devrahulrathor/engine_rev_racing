using UnityEngine;
using System.Collections;

public class MotorbikeController : MonoBehaviour
{

    public WheelCollider WColForward;
    public WheelCollider WColBack;

    public Transform wheelF;
    public Transform wheelB;
    public GameObject handles;
    public GameObject RearMudGuard;
    public Vector3 RearMudGuardSusOffset;
    [Tooltip("Lower values mean higher sensitivity")]

    public float preventFallAngle = 40;

    [Tooltip("Experimental Feature : Only for controlled low speeds")]
    public bool canArtificialStoppie = false;
    [Range(0.1f,1f)]
    public float stoppieAmount = 0.3f;
    private float preventFallInfluence = 1;

    public float maxSteerAngle = 45;
    public float maxMotorTorque = 200;
    [Tooltip("Adds more speed. Inaccurate from a physics standpoint. Arcade Feature. Values too high will break the realism of the system and make the bike glitch badly.")]
    public float ArtificialAcceleration = 1000f;
    public float maxForwardBrake = 400;
    public float maxBackBrake = 400;

    public float wheelRadius = 0.7f;

    public float steerSensivity = 30;
    public float controlAngle = 25;
    public float controlOmega = 30;

    public float lowSpeed = 8;
    public float highSpeed = 25;

    private WheelData[] wheels;

    private Transform thisTransform;
    public Vector3 com;

    public class WheelData
    {

        public WheelData(Transform transform, WheelCollider collider)
        {
            wheelTransform = transform;
            wheelCollider = collider;
            wheelStartPos = transform.transform.localPosition;
        }

        public Transform wheelTransform;
        public WheelCollider wheelCollider;
        public Vector3 wheelStartPos;
        public float rotation = 0f;
    }

    public struct MotorbikeInput
    {
        public float steer;
        public float acceleration;
        public float brakeForward;
        public float brakeBack;
    }

    void Start()
    {


        wheels = new WheelData[2];
        wheels[0] = new WheelData(wheelF, WColForward);
        wheels[1] = new WheelData(wheelB, WColBack);

        thisTransform = GetComponent<Transform>();

    }


    void FixedUpdate()
    {
        var input = new MotorbikeInput();

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) input.acceleration = 1;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) input.steer += 1;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) input.steer -= 1;

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            input.brakeBack = 0.3f;
            input.brakeForward = 0.5f;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            input.brakeForward = 1f;
        }

        motoMove(motoControl(input));
        uprightForce();
        updateWheels();
        steerHandles();
        RearMudGuardSuspension();
        if(canArtificialStoppie) // for natural stoppie increase forward brake to 50000 and Front Wheel collider forward friction to 5. Around those values a natural stoppie can be performed.
        CalcStoppie(); //Requires prevent falling

        if (speedVal < 0.01f && thisTransform.up.y < 0.2f && Mathf.Abs(prevOmega) < 0.001f || Input.GetKeyDown(KeyCode.R))
        {
            reset();
        }

        //Bug fix stationary wheel rotates on asphalt
        if(speedVal<0.02f&&!Input.GetKey(KeyCode.W)) // or UP ARROW
        {
            WColBack.brakeTorque = 0.01f;

        }

    }

    private void reset()
    {
        Transform t = GetComponent<Transform>();
        t.position = t.position + new Vector3(0, 0.2f, 0);
        t.rotation = new Quaternion(0, 0, 0, 1);
    }

    private Vector3 prevPos = new Vector3();
    private float prevAngle = 0;
    private float prevOmega = 0;
    private float speedVal = 0;
    private float prevSteer = 0f;

    private MotorbikeInput motoControl(MotorbikeInput input)
    {
        var posNow = thisTransform.position;
        var speed = (posNow - prevPos) / Time.fixedDeltaTime;
        prevPos = posNow;

        speedVal = speed.magnitude;
        var moveForward = speed.normalized;

        var angle = Vector3.Dot(moveForward, Vector3.Cross(thisTransform.up, new Vector3(0, 1, 0)));
        var omega = (angle - prevAngle) / Time.fixedDeltaTime;
        prevAngle = angle;
        prevOmega = omega;


        if (speedVal < lowSpeed)
        {
            float t = speedVal / lowSpeed;
            input.steer *= t * t;
            omega *= t * t;
            angle = angle * (2 - t);
            input.acceleration += Mathf.Abs(angle) * 3 * (1 - t);
        }

        if (speedVal > highSpeed)
        {
            float t = speedVal / highSpeed;
            if (omega * angle < 0f)
            {
                omega *= t;
            }
        }
        input.steer *= (1 - 2.5f * angle * angle);
        input.steer = 1f / (speed.sqrMagnitude + 1f) * (input.steer * steerSensivity + angle * controlAngle + omega * controlOmega);
        float steerDelta = 10 * Time.fixedDeltaTime;
        input.steer = Mathf.Clamp(input.steer, prevSteer - steerDelta, prevSteer + steerDelta);
        prevSteer = input.steer;

        return input;
    }
    private void uprightForce()
    {
        
            var turnAngle = transform.eulerAngles.z;
            if (transform.eulerAngles.z > 180)
                turnAngle = transform.eulerAngles.z - 360;
            com.y = 1.5f;
            if (speedVal < 1)
            {
                if (turnAngle < 50 && turnAngle > -50)
                {
                    var rot = Quaternion.FromToRotation(transform.up, Vector3.up);
                    GetComponent<Rigidbody>().AddTorque(new Vector3(rot.x, rot.y, rot.z) * 150000);
                }
            }
            else if (speedVal > 1)
            {
                if (turnAngle > preventFallAngle || turnAngle < -preventFallAngle)
                {
                    com.y = 0;
                }
            }
            if (turnAngle > preventFallAngle)
                com.x += Time.deltaTime*preventFallInfluence;
            else if (turnAngle < -preventFallAngle)
                com.x -= Time.deltaTime*preventFallInfluence;
            else if (turnAngle < 40 && turnAngle > -40)
                com.x = 0;
            if(Mathf.Abs(turnAngle)>preventFallAngle+5)
            transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,Mathf.Clamp(turnAngle,-preventFallAngle-5,preventFallAngle+5));
            
            
            GetComponent<Rigidbody>().centerOfMass = com;
        

    }

    private void motoMove(MotorbikeInput input)
    {
        if (speedVal > 1)
            WColForward.steerAngle = Mathf.Clamp(input.steer, -1, 1) * maxSteerAngle;
        else
            WColForward.steerAngle = Mathf.Clamp(input.steer, -speedVal, speedVal);


        WColForward.brakeTorque = maxForwardBrake * input.brakeForward;
        WColBack.brakeTorque = maxBackBrake * input.brakeBack;

        WColBack.motorTorque = maxMotorTorque * input.acceleration;
        if(speedVal<highSpeed)
        GetComponent<Rigidbody>().AddForce(transform.forward*ArtificialAcceleration*input.acceleration);

    }

    private void updateWheels()
    {
        float delta = Time.fixedDeltaTime;

        foreach (WheelData w in wheels)
        {
            WheelHit hit;

            Vector3 localPos = w.wheelTransform.localPosition;
            if (w.wheelCollider.GetGroundHit(out hit))
            {
                localPos.y -= Vector3.Dot(w.wheelTransform.position - hit.point, transform.up) - wheelRadius;
                w.wheelTransform.localPosition = localPos;
            }
            else
            {
                localPos.y = w.wheelStartPos.y;
            }

            w.rotation = Mathf.Repeat(w.rotation + delta * w.wheelCollider.rpm * 360.0f / 60.0f, 360f);
            w.wheelTransform.localRotation = Quaternion.Euler(w.rotation, w.wheelCollider.steerAngle, 0);
        }
    }
    private void steerHandles()
    {
        handles.transform.localRotation = Quaternion.Euler(0, WColForward.steerAngle, 0);
    }
    private void RearMudGuardSuspension()
    {
        WheelHit hit;
        if(WColBack.GetGroundHit(out hit))
        RearMudGuard.transform.rotation = Quaternion.LookRotation(transform.position - wheelB.transform.position - RearMudGuardSusOffset, transform.forward);
    }
    private void CalcStoppie()
    {
        var stoppieAngle = transform.eulerAngles.x;
        if (transform.eulerAngles.x > 180)
                stoppieAngle = transform.eulerAngles.x - 360;
        if(Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.DownArrow)||Input.GetKey(KeyCode.Space))
        com.z += (speedVal*Time.deltaTime)/5;
        else
        com.z -= Time.deltaTime*100;
        if(com.z<0||stoppieAngle>5+speedVal)
        {
            com.z=0;
            if(stoppieAngle>50)
            com.z-=stoppieAngle/10;
        }
        
        else if(com.z > stoppieAmount)
        com.z =stoppieAmount;
    }

}
