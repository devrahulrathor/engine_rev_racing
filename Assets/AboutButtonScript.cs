﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AboutButtonScript : MonoBehaviour
{
    public GameObject AboutWindow;
    public Button AboutButton;

    public void AboutButtonActivate()
    {
        AboutWindow.SetActive(true);
        ChangeButtonColor(1);
    }

    public void AboutButtonDeActivate()
    {
        AboutWindow.SetActive(false);
    }


    public void ChangeButtonColor(int a)
    {

        ColorBlock colors = AboutButton.colors;
        //          colors.normalColor = new Color32(0, 219, 213, 255);
        if (a == 1)
        {
            Debug.Log("Change Colours to Green");
            colors.normalColor = new Color32(0, 219, 213, 255);  // Green Colour
            colors.highlightedColor = new Color32(0, 219, 213, 255);
            colors.pressedColor = new Color32(0, 219, 213, 255);
            colors.selectedColor = new Color32(0, 219, 213, 255);
        }
        else if (a == 2)
        {
            Debug.Log("Change Colours to White");
            colors.normalColor = new Color32(225, 225, 225, 255);  //White Colour
            colors.highlightedColor = new Color32(225, 225, 225, 255);
            colors.pressedColor = new Color32(225, 225, 225, 255);
            colors.selectedColor = new Color32(225, 225, 225, 255);
        }
        //   colors.normalColor = new Color(0, 0.2f, 0.5f, 0.8f);
        //      colors.highlightedColor = new Color32(0, 219, 150, 255);
        AboutButton.colors = colors;
    }

}
