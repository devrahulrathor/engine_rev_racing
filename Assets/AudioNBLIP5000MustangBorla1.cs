﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP5000MustangBorla1 : MonoBehaviour
{
    public static float dt5000 = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineMustang.itrate == 5004)
        {
            AudioEngineMustang.rpm1 = 5000;
            //           Debug.Log(AudioEngineMustang.rpm1);
        }
    }
    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip5000()
    {

        AudioSource audio = GetComponent<AudioSource>();
        if (AudioEngineMustang.rpm1 >= 4000 && AudioEngineMustang.rpm1 < 5000)
        {
            audio.Play();
        }
        else if (AudioEngineMustang.rpm1 >= 2000 && AudioEngineMustang.rpm1 < 4000)
        {
            //       dt5000 = 0.052f;
            dt5000 = 0.068f;
            audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        }
        //     else if (AudioEngineMustang.rpm1 >= 2000 && AudioEngineMustang.rpm1 < 3000)
        //     {
        //    dt5000 = 0.096f;
        //         dt5000 = 0.167f;
        //         audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        //     }
        else if (AudioEngineMustang.rpm1 < 2000)
            //      dt5000 = 0.212f;
            dt5000 = 0.269f;
        audio.PlayScheduled(AudioSettings.dspTime + dt5000);
        audio.volume = 1;

        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip5000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }

    public void Blip5000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip5000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineMustang.ReverbXoneMix;
    }
}


