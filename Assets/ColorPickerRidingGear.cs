using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPickerRidingGear : MonoBehaviour
{
    // Riding Gear Section
    public Material matHelmet, matJacket1;
    public float colorshadeRidingGear = 0;
    public float colorshadeRidingGearHelmet = 0;
    public Slider cSliderRidinggear;  // Slider for Body Colour
    public Slider cSliderRidingGearHelmet;  // Slider for Body Colour
    int icolorcounter = 1;   // 1 Green
    int icolorcounterhelmet = 1;   // 1 Green
    public static int iRidingGearRented = 0;  // To Rent Riding Gear after wating an Ad

    private void Start()
    {
        SetInitialColor();
    }
    public void SetInitialColor()
    {
        Debug.Log("Inside Bullet SetColor1 RidingGear");
        matJacket1.color = Color.Lerp(Color.white, Color.black, 0.0f);
        matHelmet.color = Color.Lerp(Color.white, Color.black, 0.2f);
        //   matBullet500.color = Color.Lerp(Color.green, Color.black, 1 * 0.4537f);
    }

    public void ColorGreen()
    {
        Debug.Log("INside Color Green");
        icolorcounter = 1;
      
            if (colorshadeRidingGear >= 0) matJacket1.color = Color.Lerp(Color.green, Color.black, colorshadeRidingGear);
            else if (colorshadeRidingGear < 0) matJacket1.color = Color.Lerp(Color.green, Color.white, -1 * colorshadeRidingGear);
      
      
    }
    public void ColorBlue()
    {
        Debug.Log("INside Color Blue");
        icolorcounter = 2;
       
            if (colorshadeRidingGear >= 0) matJacket1.color = Color.Lerp(Color.blue, Color.black, colorshadeRidingGear);
            else if (colorshadeRidingGear < 0) matJacket1.color = Color.Lerp(Color.blue, Color.white, -1 * colorshadeRidingGear);
      
    }
    public void ColorYellow()
    {
        icolorcounter = 3;
        if (colorshadeRidingGear >= 0) matJacket1.color = Color.Lerp(Color.yellow, Color.black, colorshadeRidingGear);
            else if (colorshadeRidingGear < 0) matJacket1.color = Color.Lerp(Color.yellow, Color.white, -1 * colorshadeRidingGear);
     
    }

    public void ColorRed()
    {
        Debug.Log("INside Color Red");
        icolorcounter = 4;
    
            if (colorshadeRidingGear >= 0) matJacket1.color = Color.Lerp(Color.red, Color.black, colorshadeRidingGear);
            else if (colorshadeRidingGear < 0) matJacket1.color = Color.Lerp(Color.red, Color.white, -1 * colorshadeRidingGear);
      
    }

    void ColourRefresh()
    {

        if (icolorcounter == 1) ColorGreen();
        if (icolorcounter == 2) ColorBlue();
        if (icolorcounter == 3) ColorYellow();
        if (icolorcounter == 4) ColorRed();
        //   if (icolorcounter == 3) ColorGreen();
        //   if (icolorcounter == 4) ColorYellow();
        //   if (icolorcounter == 5) ColorBlack();
        //   if (icolorcounter == 6) ColorWhite();

    }
    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        colorshadeRidingGear = sliderposition;
        Debug.Log("colorshadeRidingGear=" + colorshadeRidingGear);
        ColourRefresh();

        //     Debug.Log("colorshadeRidingGear=");
        //     Debug.Log(colorshadeRidingGear);

    }

    public void ColorGreenHelmet()
    {
        Debug.Log("INside Color Green");
        icolorcounterhelmet = 1;

        if (colorshadeRidingGearHelmet >= 0) matHelmet.color = Color.Lerp(Color.green, Color.black, colorshadeRidingGearHelmet);
        else if (colorshadeRidingGearHelmet < 0) matHelmet.color = Color.Lerp(Color.green, Color.white, -1 * colorshadeRidingGearHelmet);


    }
    public void ColorBlueHelmet()
    {
        Debug.Log("INside Color Blue");
        icolorcounterhelmet = 2;

        if (colorshadeRidingGearHelmet >= 0) matHelmet.color = Color.Lerp(Color.blue, Color.black, colorshadeRidingGearHelmet);
        else if (colorshadeRidingGearHelmet < 0) matHelmet.color = Color.Lerp(Color.blue, Color.white, -1 * colorshadeRidingGearHelmet);

    }
    public void ColorYellowHelmet()
    {
        icolorcounterhelmet = 3;
        if (colorshadeRidingGearHelmet >= 0) matHelmet.color = Color.Lerp(Color.yellow, Color.black, colorshadeRidingGearHelmet);
        else if (colorshadeRidingGearHelmet < 0) matHelmet.color = Color.Lerp(Color.yellow, Color.white, -1 * colorshadeRidingGearHelmet);

    }

    public void ColorRedHelmet()
    {
        Debug.Log("INside Color Red");
        icolorcounterhelmet = 4;

        if (colorshadeRidingGearHelmet >= 0) matHelmet.color = Color.Lerp(Color.red, Color.black, colorshadeRidingGearHelmet);
        else if (colorshadeRidingGearHelmet < 0) matHelmet.color = Color.Lerp(Color.red, Color.white, -1 * colorshadeRidingGearHelmet);

    }


    public void SlidervalueHelmet(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        colorshadeRidingGearHelmet = sliderposition;
        Debug.Log("colorshadeRidingGear=" + colorshadeRidingGear);
        ColourRefreshHelmet();

        //     Debug.Log("colorshadeRidingGear=");
        //     Debug.Log(colorshadeRidingGear);

    }

    void ColourRefreshHelmet()
    {

        if (icolorcounterhelmet == 1) ColorGreenHelmet();
        if (icolorcounterhelmet == 2) ColorBlueHelmet();
        if (icolorcounterhelmet == 3) ColorYellowHelmet();
        if (icolorcounterhelmet == 4) ColorRedHelmet();
        //   if (icolorcounter == 3) ColorGreen();
        //   if (icolorcounter == 4) ColorYellow();
        //   if (icolorcounter == 5) ColorBlack();
        //   if (icolorcounter == 6) ColorWhite();

    }
}
