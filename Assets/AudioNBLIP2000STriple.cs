using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP2000STriple : MonoBehaviour
{
    int[] posrpmarrayblip2000 = { 1150, 1150, 1150, 1150, 1200, 1250, 1300, 1500, 1700, 1850 };
    double[] rpmposarrayblip2000 = { 0.01, 0.01, 0.01, 0.02, 0.05, 0.06, 0.07, 0.08, 0.08, 0.08, 0.08, 0.09, 0.09, 0.09, 0.09 };
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineSTriple.rpm1 <= 2000)
        {
            //       RPMCal();
            //        Debug.Log("Inside 2000Blip irpmindex=");
            //        Debug.Log(irpmarrayindex);

        }
    }


    int irpmpos = 0;
    double posinsec;
    public void Blip2000()
    {
        //   AudioEngineSTriple.itrate = 5001;

        AudioSource audio = GetComponent<AudioSource>();


        //      irpmpos = (AudioEngineSTriple.rpm1 / 50) - 21;

        //      posinsec = rpmposarrayblip2000[irpmpos];
        //         textBox3.Text = posinsec.ToString();
        //       var Last10SecondsBytePos = Bass.BASS_ChannelSeconds2Bytes(streamslr1, durationInSeconds - 2);

        //      audio.time = (float)posinsec;
        //      irpmpos = (AudioEngineSTriple.rpm1 / 50) - 21;

        //      posinsec = rpmposarrayblip[irpmpos];
        //         textBox3.Text = posinsec.ToString();
        //       var Last10SecondsBytePos = Bass.BASS_ChannelSeconds2Bytes(streamslr1, durationInSeconds - 2);

        //      audio.time = (float)posinsec;
        //      audio.time = (float)posinsec;
        if (AudioEngineSTriple.rpm1 < 2000)
        {
            audio.Play();
            audio.volume = 1;

            audio.Play(44100);
        }

        //       RPMmodetext.text = "Blip";
    }


    int irpmarrayindex;
    public void RPMCal()
    {
        AudioSource audio = GetComponent<AudioSource>();
        irpmarrayindex = (int)((audio.time * 10000) / 100);

        //       rpm = posrpmarrayblip[irpmarrayindex];
        //         textBox2.Text = position.ToString();
        //       if (audio.time <= 0.32)
        //         if(AudioEngineSTriple.rpm1 <= 7000)
        //       {
        AudioEngineSTriple.rpm1 = posrpmarrayblip2000[irpmarrayindex];
        //       }
        //     else AudioEngineSTriple.rpm1 = 0;
        //     AudioEngineSTriple.itrate = -1;

    }

    public void Blip2000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip2000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip2000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //     audio.reverbZoneMix = 1.09f;
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}
