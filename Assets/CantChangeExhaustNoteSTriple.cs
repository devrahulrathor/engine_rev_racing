using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CantChangeExhaustNoteSTriple : MonoBehaviour
{
    public GameObject CantswitchExhaustNote;
    // Start is called before the first frame update
    

    public void ExhaustCantChangeNote()
    {
        if (AudioEngineSTriple.istartfromtouch == 2)
        {
            CantswitchExhaustNote.SetActive(true);
            StartCoroutine("ExhaustCantChangeNoteoff");
        }
    }
    private IEnumerator ExhaustCantChangeNoteoff()
    {
        yield return new WaitForSeconds(2f);
        //   image.color = Color.white;
        CantswitchExhaustNote.SetActive(false);

    }

}
