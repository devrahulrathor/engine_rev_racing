using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class BikeControllerZX25 : MonoBehaviour
{
    public GameObject BikeZX10;
    public GameObject BikeZX25Handlebar, BikeBullet500Handlebar;
    public GameObject ProfileCamera;

    public MainControllerDrag MainController;
    public SoundEngineZX25Drag AudioEngineZX25;
    public SoundEngineBullet500Drag AudioEngineBullet500;
    private Rigidbody bikejawa;
    public GameObject JawaforAssetBoxCollider;  // Deactivated when high speed crash so Bike can rest on road
    public GameObject BoxColliderLeft;
    public GameObject BoxColliderRight;
    public GameObject JavaPhysics;
    float anglevehiclerotateoncrash = 0;
    //Store and Reset Vehicle Position History and reset after crash
    Vector3 originalPos;
    Vector3 originalPos1, originalPos2, originalPos3, originalPos4, originalPos5;
    public Quaternion originalRotationValue; // declare this as a Quaternion  Val at Origin
    public Quaternion originalRotationValueZX10; // declare this as a Quaternion Val at Origin
    public Quaternion originalRotationValueHandlebar; // declare this as a Quaternion Val at Origin
                                                      //    public Quaternion originalRotationValueProfileCameraprecrash; // declare this as a Quaternion  Val at Origin

    public Quaternion originalRotationValueprecrash; // declare this as a Quaternion Val just before Crash
    public Quaternion originalRotationValueZX10precrash; // declare this as a Quaternion Val just before crash

    public Quaternion originalRotationValueProfileCamera; // declare this as a Quaternion  Val at Origin

    // Animation of Rider
    // Throttle Hand and Helmet
    public GameObject riderArmZX25, riderForeArm, riderHandZX25, riderHelmetZX25;
    public GameObject riderArmBullet500, riderForeArmBullet500, riderHandBullet500, riderHelmetBullet500;
    float anglerotHand = 0, anglerotForearm = 0, anglerotArm = 0;
    float anglerotArpmrpmx;  // Angle to which Arm can rotate for a specific RPM
    float anglerotRiderHelmetx = 0, anglerotRiderHelmety = 0;
    int iHelmetAnimationtrigger = 0;  // Counter to trigger Helmet Animatio once
    int iHelmetAnimationRandon;  // Randomize Animation 0-10
    int iHelmetAnimationRevLimitertrigger = 0;  // Counter to trigger Helmet Animation RevLimiter once

    public Quaternion originalRotationValueHelmet; // declare this as a Quaternion Val of Helmet in the beginning before RevLimiter starts



    //  public Rigidbody BikeLeftCollider;// Collider for Left Crashes
    public Slider RightTurnSlider;
    public Slider LeftTurnSlider;

    int i = 0;
    int it = 0;
    float vehiclepositionx = 0;  //Tracking Vehhicle Position for Tunnel Sound Change
    float vehiclepositionoldx = 0;
    float vehicledirectionx = 0;  // +/- for Tunnel Sound
    float vehiclepositiony = 0;  //Tracking Vehhicle Position for Tunnel Sound Change
    float vehiclepositionoldy = 0;
    float vehicledirectiony = 0;  // +/- for Tunnel Sound
    int ivehicleintunnel = 0;

    public int isteer = 0;  // -1 Left, 1 Right Constrolled from Touch Arrows
                            //    public WheelCollider WColForward;
                            //    public WheelCollider WColBack;

    public float maxMotorTorque = 400;

    private WheelData[] wheels;
    //  public Transform wheelF;
    //  public Transform wheelB;
    public int iaccelerometerTurnTest = 1;
    public float accelvaltest = 0;
    public float maxleananglemagnitude = 50;
    public Slider AcceltestSlider;


    int igamemode = 1; //  1 for Test, 2 for Mobile accelerometer
    private float forwardspeed = 10;
    public Slider accelpedal;
    public float maxSteeringAngle = 30f; // maximum steer angle the wheel can have // 35 initial value
    float steering;
    float maxleanangle = 40.0f;
    float maxleanangletemp;
    float angletilt = 0;  //Tilt Angle while steering
    float anglelean = 0;   // Lean Angle while cornering
    float danglelean = 50f;  // Delta Lean angle every Update  
    float dangleleanmultiplier = 50; // User control multiplier for lean angle change(high val means rapid leaning -Provide a user setting depending on Bike
    float angleleanHandlebar = 0;
    //  float devicetiltrate = 1; // To lean the bike as per the mobile rotation rate
    float inputaccelxold;  // To caclculate tilte rate
    int rightarrowengaged = 0, leftarrowengaged = 0;
    private int ivehiclecrashed = 0;  // Detect Vehicle Crash mode, 1 when crashed
    private int ivehiclecrashing = 0;  // Detect Vehicle Crash mode starts, 1 when crash starts to put the vehicle back on track after low speed collision
    public static int ivehiclecrashingsound = 0;  // To de Activate Braking sound when Crashing
    int ivehiclereversing = 0;  // 1 when reversing so to avoid clash with main module which assigns 0 velocity when gear 0

    //check the collision with object Strings
    String str = "GeeksforGeeks";
    String substr1 = "r_fence";  // Middle Metallic Divider
    String substr2 = "r_block";   // Side Concerte Wall
    //   String substr2 = "For";
    //Find location of Cebtre Crash Rails/ Side Concrete Boundaries
    int isiderailcollisionexit = 0;  // 1 if Vehicle left Guard Rail mesh Collider Trigget
    int icollisiondirection = 0; // -1 for Left Side Collision and 1 for Right

    // Module to check if vehicle is on road
    int igravityenterroadmodifier = 0;  // Due to vehicle flying off the road and to bring it back
    int igravityexitroadmodifier = 0;  // Due to vehicle flying off the road and to bring it back
    int iroadcollision = 0; // number of collisions of vehicle with road (since 2 roads blocks at same location so 2 collisions)
    bool SideRailPlay = false;              //    bool isgrounded;

    // Tunnel Detect Section
    // City Roads Tunnel 1
    float tunneldotprod1 = 0;   // Dot Product to decide if Vehicle entering tunnelor exiting Entrance 
    float tunneldotprod2 = 0;   // Dot Product to decide if Vehicle entering tunnelor exiting Exit 
    // Greenland Daty Tunnel 1
    float GLtunneldotprod1 = 0;   // Dot Product to decide if Vehicle entering tunnelor exiting Entrance 
    float GLtunneldotprod2 = 0;   // Dot Product to decide if Vehicle entering tunnelor exiting Exit 



    // Game Settings Section
    public int iaccelerometeron = 0;  // 1 Accelerometer on for direction(Mobile tilt), 0 - Direction Arrows


    // Audio Sources
    public SideRailImpactSound SRailImpact;
    public HighSpeedCrashSound HSpeedCrash;

    // using String.Contains() Method
    // Console.WriteLine(str.Contains(substr1));


    public class WheelData
    {

        public WheelData(Transform transform, WheelCollider collider)
        {
            wheelTransform = transform;
            wheelCollider = collider;
            wheelStartPos = transform.transform.localPosition;
        }

        public Transform wheelTransform;
        public WheelCollider wheelCollider;
        public Vector3 wheelStartPos;
        public float rotation = 0f;
    }



    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity = new Vector3(0, -500.0F, 0);
        bikejawa = this.GetComponent<Rigidbody>();

        wheels = new WheelData[2];
        //     wheels[0] = new WheelData(wheelF, WColForward);
        //     wheels[1] = new WheelData(wheelB, WColBack);
        gameObject.tag = "Player";

        originalPos = transform.position;
        originalRotationValue = transform.rotation; // save the initial rotation
        originalRotationValueZX10 = BikeZX10.transform.rotation; // save the initial rotation
                                                                 //   originalRotationValueProfileCamera = ProfileCamera.transform.rotation; // save the initial rotation
        if (MainControllerDrag.ivehicle == 1) originalRotationValueHandlebar = BikeZX25Handlebar.transform.rotation; // save the initial rotation
        if (MainControllerDrag.ivehicle == 2) originalRotationValueHandlebar = BikeBullet500Handlebar.transform.rotation; // save the initial rotation
                                                                                                                          //     originalRotationValueProfileCamera = ProfileCamera.transform.rotation; // save the initial rotation
        StartCoroutine("VehiclePositionHistory");

        if (MainControllerDrag.ivehicle == 1) originalRotationValueHelmet = riderHelmetZX25.transform.rotation;
        else if (MainControllerDrag.ivehicle == 2) originalRotationValueHelmet = riderHelmetBullet500.transform.rotation;
   
    }

    //Collosion Detection
    void OnCollisionEnter(Collision collision)
    {
        //Ouput the Collision to the console
        Debug.Log("Collision : " + collision.gameObject.name);
        if (collision.gameObject.name != "Line001" && collision.gameObject.name != "Line001_2" && collision.gameObject.name != "Road_Track_A_01" && collision.gameObject.name != "Track_C_002" && collision.gameObject.name != "Track_B_Mesh" && Mathf.Abs(bikejawa.velocity.x) > 200)
        {
            //         ivehiclecrashed = 1;
            //       BikeZX10.SetActive(false);

        }
        if (collision.gameObject.name == "r_fence_cv")
        {
            //     Debug.Log("Collision with Ventral Divider");
        }

        //      Collider myCollider = collision.contacts[0].thisCollider;  // Generates Garbage
        //   Collider myCollider = collision.GetContact(0).Collider;  // Need to check this over above method
        //    Debug.Log("Collision at");
        Collider myCollider = collision.GetContact(0).thisCollider;  // Generates Garbage                                                          //    Debug.Log(myCollider);

        if (myCollider.name == "Bike_Collider_Left" || myCollider.name == "Bike_Collider_Right")
        {
            if (myCollider.name == "Bike_Collider_Left") icollisiondirection = -1;
            else if (myCollider.name == "Bike_Collider_Right") icollisiondirection = 1;
            //      if (collision.contacts[0].otherCollider.transform.gameObject.name == "Bike_Collider_Left") { 

            if (MainControllerDrag.bikespeed < 100)
            {     // Crash Recover Module only when Bikespeed is less than certain vel: 100 KNMH
                MainController.BrakeFromTouch();  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                                                  //       else if (MainControllerDrag.ivehicle == 2) MainController.BrakeFromTouch();  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

                StartCoroutine("BrakeRelease"); //Brake Release to reduce speed to say 20 KMPh
                                                //   ivehiclecrashing = 1;
                if (Mathf.Abs(MainControllerDrag.bikespeed) > 0 && Mathf.Abs(MainControllerDrag.bikespeed) < 100)
                {
                    StopCoroutine("SlowSpeedSideRailCrash");
                    if (icollisiondirection == -1) transform.Rotate(0, 10, 0);  // Steering Parent bikejawa
                    else if (icollisiondirection == 1) transform.Rotate(0, -10, 0);  // Steering Parent bikejawa
                    StartCoroutine("SlowSpeedSideRailCrash");
                }
                Debug.Log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Bike Left Collider ");
                if (SideRailPlay == false)
                {
                    Debug.Log("Siderail Crash Sound PLay");
                    SRailImpact.SideRailImpactPlay();  // Playing Metal Impact Sound
                    SideRailPlay = true;
                }
            }

            else  // High Speed Crash Module
            {
                Debug.Log("Entered HighSpeed Crash Yes");
                // calculate force vector
                Vector3 dir2 = transform.position - collision.transform.position;
                // normalize force vector to get direction only and trim magnitude
                dir2.Normalize();
                //     bikejawa.AddForce(dir2* 1000);
                //        bikejawa.AddForce( dir2, ForceMode.Impulse);
                //     BikeZX10.transform.Rotate(1000 * Time.deltaTime, 0, 0);   // Leaning Bike

                MainController.BrakeFromTouch();  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                HSpeedCrash.HighSpeedCrashtPlay();  // PLaying Crash Sound
                if (Mathf.Abs(MainControllerDrag.bikespeed) > 0 && Mathf.Abs(MainControllerDrag.bikespeed) >= 100)
                {
                    ivehiclecrashingsound = 1;
                    //    StopCoroutine("SlowSpeedSideRailCrash");
                    Debug.Log("Inside High Speed Crash icollsiondire =" + icollisiondirection);
                    //         if (icollisiondirection == -1) transform.Rotate(0, 50, 0);  // Steering Parent bikejawa
                    //         else if (icollisiondirection == 1) transform.Rotate(0, -50, 0);  // Steering Parent bikejawa

                }
                //       bikejawa.AddTorque(0, 10000, 0, ForceMode.Impulse);
                //     JawaforAssetBoxCollider.SetActive(false);
                //    JawaforAssetBoxCollider.GetComponent<BoxCollider>().enabled = false;
                BoxColliderLeft.SetActive(false);
                BoxColliderRight.SetActive(false);
                StartCoroutine("HighSpeedCrash");
                ivehiclecrashed = 1;
            }
        }



        if (collision.gameObject.name.Contains(substr1) || collision.gameObject.name.Contains(substr2))
        {
            //   var force = transform.position - collision.transform.position;
            // normalize force vector to get direction only and trim magnitude
            //   force.Normalize();
            //   bikejawa.AddForce(force * 50000);
            //    AudioEngineZX25.BrakeFromTouch();

            Vector3 targetDirection = (transform.position - collision.transform.position).normalized;
            //  Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 10f, 0.0f);
            //bikejawa.transform.rotation = Quaternion.LookRotation(-newDirection);

            //output to log the position change
            //      Debug.Log("Bike Position=");
            //        Debug.Log(transform.position);
            //        Debug.Log("SideRail Position=");
            //        Debug.Log(collision.transform.position);
            if ((bikejawa.transform.position.z - collision.transform.position.z) < 0)
            {
                //         Debug.Log("RIGHT Turn");
            }

            else
            {
                //          Debug.Log("LEFT Turn");
            }
            //    Debug.Log("Target Direction=");
            //    Debug.Log(targetDirection);

            //          if (Vector3.Dot(transform.position, collision.transform.position) > 0)
            //             {
            //                 Debug.Log("RIGHT Turn");
            //             }

            // else
            // {
            //     Debug.Log("LEFT Turn");
            // }



        }
        if (collision.gameObject.name.Contains(substr1) || collision.gameObject.name.Contains(substr2))
        {
            //     if (isteer == 1) Debug.Log("___________________Right Crash");
            //      else if (isteer == -1) Debug.Log("___________________Left Crash");

            //     AudioEngineZX25.BrakeFromTouch();
            //   Debug.Log("Collision with Ventral Divider" + collision.gameObject.name);
            //    bikejawa.angularVelocity = Vector3.zero;
            //  bikejawa.transform.position.z = 
            //      bikejawa.transform.position = new Vector3(1, 1, 1);
            //   ivehiclecrashing = 1;
            if (Mathf.Abs(MainControllerDrag.bikespeed) > 0 && Mathf.Abs(MainControllerDrag.bikespeed) < 100)
            {
                //       StopCoroutine("SlowSpeedSideRailCrash");
                //   transform.Rotate(0, 10, 0);  // Steering Parent bikejawa
                //        StartCoroutine("SlowSpeedSideRailCrash");
            }
        }


        // force is how forcefully we will push the player away from the enemy.
        //    float force = -1000;

        // If the object we hit is the enemy
        //     if (collision.gameObject.tag == "enemy")
        //     {
        // Calculate Angle Between the collision point and the player
        // how much the character should be knocked back
        //    var magnitude = 50000;
        // calculate force vector
        Vector3 dir = transform.position - collision.transform.position;
        // normalize force vector to get direction only and trim magnitude
        // dir.Normalize();
        //   bikejawa.AddForce(dir* magnitude);
        //     }


        // Gravity Modifier when Bike flies out of road so as to drop it back
        if (collision.gameObject.name.Contains("turn") || collision.gameObject.name.Contains("round"))
        {

            //       Physics.gravity = new Vector3(0, -500.0F, 0);

        }
        if (collision.gameObject.name.Contains("road"))
        {
            //      Physics.gravity = new Vector3(0, -300.0F, 0);
        }

    }
    void OnCollisionExit(Collision collision)
    {
        //    Debug.Log("Exit Collision : " + collision.gameObject.name);
        //   if (collision.gameObject.name.Contains("road") || collision.gameObject.name.Contains("RU") || collision.gameObject.name.Contains("turn"))
        //   {
        //    iroadcollision = iroadcollision - 1;
        //    Debug.Log("Exit Road Collision : " + collision.gameObject.name);
        //    Debug.Log("Enter Road Collision, iroadcollision= : " + iroadcollision);
        //    if (iroadcollision == 0)
        //    {
        //         igravityexitroadmodifier = 1;
        //         igravityenterroadmodifier = 0;
        //    }
        //  }
        isiderailcollisionexit = 1;
        Debug.Log("Exit Siderail Collision : " + collision.gameObject.name);
        SRailImpact.SideRailImpactStop();  // Stop Playing Metal Impact Sound
        SideRailPlay = false;
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        vehiclepositionoldx = vehiclepositionx;

        // Disabled Gravity Modifier to bring vehicle back to road when it leaves road
        if (igravityexitroadmodifier == 1)
        {
            //         Physics.gravity = new Vector3(0, -2500.0F, 0);
            //      Debug.Log("Collision Gravity Now 2500");
        }
        else
        {
            //       Physics.gravity = new Vector3(0, -250.0F, 0);
            //  Debug.Log("Collision Gravity Now 250");
        }
        //      Debug.Log("Rider Animation 0 itrate =" + SoundEngineZX25Drag.itrate);
        // Animation for Blipping
        BlipAnimate();
        if (GUIScript.icameraview == 2 && iHelmetAnimationtrigger == 0)
        {
            Debug.Log("INside Helmet Animate 1");
            iHelmetAnimationtrigger = 1;
            StartCoroutine("HelmetAnimate");
            StopCoroutine("HelmetAnimateRevLimiter");
            StopCoroutine("HelmetAnimateRevLimiterEnd");
        }
        if ((GUIScript.icameraview == 2 && iHelmetAnimationRevLimitertrigger == 0) && (SoundEngineZX25Drag.itrate == 10000 || SoundEngineBullet500Drag.itrate == 10000))
        {

            Debug.Log("AnimateRevLimiter0");
            StopCoroutine("HelmetAnimate");
            StopCoroutine("HelmetAnimateRevLimiterEnd");

            StartCoroutine("HelmetAnimateRevLimiter");

            iHelmetAnimationtrigger = 1;

        }
        Debug.Log(" HelmetAnimate iHelmetAnimationRevLimitertrigger=" + iHelmetAnimationRevLimitertrigger);
        if (GUIScript.icameraview == 2 && iHelmetAnimationRevLimitertrigger == 2)
        {
            Debug.Log("AnimateRevLimiterEnd00");
            iHelmetAnimationRevLimitertrigger = 3;  // Avoid another pass
            if (SoundEngineZX25Drag.itrate != 10000)
            {
                Debug.Log("AnimateRevLimiterEnd0");
                StopCoroutine("HelmetAnimateRevLimiter");
                StartCoroutine("HelmetAnimateRevLimiterEnd");
            }

            else if (SoundEngineBullet500Drag.itrate != 10000)
            {
                //       Debug.Log("AnimateRevLimiterEnd0");
                StopCoroutine("HelmetAnimateRevLimiter");
                StopCoroutine("HelmetAnimate");
                StartCoroutine("HelmetAnimateRevLimiterEnd");
            }
        }



        //   else if (GUIScript.icameraview == 1) iHelmetAnimationtrigger = 0;
        // 


        // Store Position of Vehicle to reset after crash
        //   originalPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        //   if(ivehiclecrashed == 0) originalPos = transform.position;
        //    danglelean = maxleananglemagnitude * (1 + Mathf.Abs((Input.acceleration.x - inputaccelxold) / Time.deltaTime));

        //      detectCollision();   // Avoid Bouncing off and tipping over on collision
        //      steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        //     Debug.Log("Steering=");
        //     Debug.Log(steering);
        //           bikejawa.velocity = accelpedal.value *  (forwardspeed* Vector3.left + (steering/100) * Vector3.forward);  // Forward


        //        Debug.Log("bikejawa Velocity=");
        //            Debug.Log(bikejawa.velocity.x);

        //Detect when there is a collision starting
        //       AccelerometerTurnTest();
        //    ivehiclecrashed = 1;
        // Testing Vehicle Direction for Collision Sequence
        //     Debug.Log("Velocity x=");
        //     Debug.Log(bikejawa.velocity.x);
        //     Debug.Log("Velocity z=");
        //     Debug.Log(bikejawa.velocity.z);
        //output to log the position change
        //    Debug.Log("Bike Position=");
        //    Debug.Log(bikejawa.transform.position);



        //Module for Mobile Accelerometer
        //     danglelean = dangleleanmultiplier * (1 + Mathf.Abs((Input.acceleration.x - inputaccelxold) / Time.deltaTime));  //Or// 50 for maxleananglemagnitude calibrated

        // Test1      if (Mathf.Abs(Input.acceleration.x) > 0.005f) danglelean = dangleleanmultiplier * (1 + Mathf.Abs(((Input.acceleration.x - Mathf.Abs(0.005f)) - inputaccelxold) / Time.deltaTime));  // 50 for maxleananglemagnitude calibrated
        // Test1      else danglelean = 0;

        //Test 2 start

        if (iaccelerometeron == 1)
        {
            Debug.Log("Entered steering iaccelerometeron=" + iaccelerometeron);
            //      igamemode = 2; 
            if (Mathf.Abs(MainControllerDrag.bikespeed) < 50) dangleleanmultiplier = 30f;  // test 2
            else dangleleanmultiplier = 70f;                                        // Test 2
            danglelean = dangleleanmultiplier * Mathf.Abs((Input.acceleration.x - inputaccelxold) / Time.deltaTime);  //Test2 50 for maxleananglemagnitude calibrated

            //Test 2 end     
            maxleanangle = Mathf.Abs(Input.acceleration.x * maxleananglemagnitude);
            if (MainControllerDrag.gear != 0)   // Avoid tilting bike at Gear =0
            {
                if (Input.acceleration.x < -0.005) isteer = -1;
                else if (Input.acceleration.x > 0.005) isteer = 1;
            }

        }
        //Module for Accelerometer over

        if (ivehiclecrashed == 0 && ivehiclecrashing == 0 && MainControllerDrag.gear >= 0 && ivehiclereversing == 0)
        {
            //        bikejawa.velocity = accelpedal.value * (transform.right * -forwardspeed + transform.forward * (steering / 100));
            if (MainControllerDrag.gear != 0) bikejawa.velocity = transform.right * MainControllerDrag.bikespeed * 0.5f * -5;

            //      Debug.Log("Bike Bullet500 Vel=");
            //      Debug.Log(MainControllerDrag.bikespeed);
            //    bikejawa.AddForce( 10000, 0, 0, ForceMode.Impulse);

            //       updateWheels();
            //      wheelB.Rotate(0f, WColBack.rpm / 60 * 360 * Time.deltaTime, 0f);
            //       WColBack.motorTorque = maxMotorTorque * 1;

            //               Debug.Log("bikejawa Velocity=");
            //               Debug.Log(bikejawa.velocity);

            //     maxleanangle = maxleanangletemp;
            // BikeTilting while steering
            //Right
            //        if (Input.GetAxis("Horizontal") < 1 && Input.GetAxis("Horizontal") > 0)
            //   if (Input.acceleration.x > 0) AccelerometerTurnVal();

            if (isteer == 1)
            //     if (Input.GetKey(KeyCode.RightArrow))
            {
                //            Debug.Log("Inside iaccelerometeron Right1");
                //   steering = maxSteeringAngle * 0.1f;
                //          transform.Rotate((-bikejawa.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);

                //      Debug.Log("Right********************************Angle Lean=");
                //      Debug.Log(anglelean);
                //        Debug.Log("maxleanangle=");
                //      Debug.Log(-maxleanangle);
                if (anglelean > -maxleanangle)  //
                {
                    //              Debug.Log("Inside iaccelerometeron Right2");
                    if (Mathf.Abs(bikejawa.velocity.x) > 4 || Mathf.Abs(bikejawa.velocity.z) > 4)
                    {  // Lean and Steering starts after 4 KMph so at gear 1 at start no leaning
                       //    danglelean = 50 * (1+Mathf.Abs((Input.acceleration.x - inputaccelxold) / inputaccelxold));
                        anglelean = anglelean + (-danglelean * Time.deltaTime);
                        //    BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                        BikeZX10.transform.Rotate(danglelean * Time.deltaTime, 0, 0);   // Leaning Bike
                                                                                        //    if(MainControllerDrag.ivehicle == 1)  BikeZX25Handlebar.transform.Rotate( 0, 0.25f, 0);   // Handlebar Rotate
                                                                                        //     if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate( 0, 0.25f, 0);   // Handlebar Rotate
                                                                                        //     angleleanHandlebar = angleleanHandlebar  - 0.25f;

                        if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, 0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                        if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0, 0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                        angleleanHandlebar = angleleanHandlebar - 0.25f * danglelean * Time.deltaTime;
                        //      if (Mathf.Abs(bikejawa.velocity.x) > 30) BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                        //     else BikeZX10.transform.Rotate(0, 0, (-danglelean/1.5f) * Time.deltaTime);   // Leaning Bike
                        //     anglelean = anglelean + (maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);
                        //     BikeZX10.transform.Rotate(0, 0, maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);   // Leaning Bike
                    }
                    //     Debug.Log("Right Angle Lean=");
                    //     Debug.Log(anglelean);
                    //     steering = maxSteeringAngle * 30 * Time.deltaTime;
                }
                steering = (maxSteeringAngle * -anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                if (MainControllerDrag.bikespeed == 0) steering = 0;
                //   if(Mathf.Abs(bikejawa.velocity.x) < 30) steering = (maxSteeringAngle*1.5f * -anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);  // New Line
                //              Debug.Log("Right Steering Angle =");
                //              Debug.Log(steering);
                //              Debug.Log("Lean Angle =");
                //              Debug.Log(anglelean);
                transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa  /If Condition so as to stop Vehicle Stering at Standstill in neutral and Gear1

                angletilt = angletilt + 1f * steering * Time.deltaTime;

                //           Debug.Log("ZX25Steering Right steering=" + -steering * 3 / 100);

                //      rightarrowengaged = 1;
                //       Debug.Log("Negativ eAngle Lean=");
                //       Debug.Log(anglelean);
            }

            //Left
            //     else if (Input.GetAxis("Horizontal") > -1 && Input.GetAxis("Horizontal") < 0)
            else if (isteer == -1)
            //     else if (Input.GetKey(KeyCode.LeftArrow))
            {
                //   steering = maxSteeringAngle * -0.1f;
                //          transform.Rotate((-bikejawa.velocity.x / 500f) * steering * Time.deltaTime, steering / 100, 0);


                if (anglelean < maxleanangle)  //
                {
                    //       Debug.Log("Left********************************Angle Lean=");
                    //       Debug.Log(anglelean);
                    if (Mathf.Abs(bikejawa.velocity.x) > 4 || Mathf.Abs(bikejawa.velocity.z) > 4)
                    {  // Lean and Steering starts after 4 KMph so at gear 1 at start no leaning

                        anglelean = anglelean + (danglelean * Time.deltaTime);
                        //    BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                        BikeZX10.transform.Rotate(-danglelean * Time.deltaTime, 0, 0);   // Leaning Bike
                                                                                         //     if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, -0.25f, 0);   // Handlebar Rotate
                                                                                         //     if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0,-0.25f, 0);   // Handlebar Rotate
                                                                                         //     angleleanHandlebar = angleleanHandlebar + 0.25f;

                        if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, -0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                        if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0, -0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                        angleleanHandlebar = angleleanHandlebar - 0.25f * danglelean * Time.deltaTime;
                        //   if (Mathf.Abs(bikejawa.velocity.x) > 30)  BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                        //       else BikeZX10.transform.Rotate(0, 0, (danglelean/1.5f) * Time.deltaTime);   // Test Line
                        //     anglelean = anglelean + (maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);
                        //     BikeZX10.transform.Rotate(0, 0, maxleananglemagnitude * -Input.acceleration.x * Time.deltaTime);   // Leaning Bike
                    }
                }
                steering = -(maxSteeringAngle * anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                if (MainControllerDrag.bikespeed == 0) steering = 0;
                //    if (Mathf.Abs(bikejawa.velocity.x) < 30) steering = -(maxSteeringAngle* 1.5f * anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);  // Test Line
                //              Debug.Log("Left Steering Angle =");
                //              Debug.Log(steering);
                //              Debug.Log("Lean Angle =");
                //               Debug.Log(anglelean);

                transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa  //If Condition so as to stop Vehicle Stering at Standstill in neutral and Gear1

                angletilt = angletilt - 1f * steering * Time.deltaTime;

                //                Debug.Log("ZX25Steering Left steering=" + steering * 3 / 100);



                //    leftarrowengaged = 1;
                //      Debug.Log("Positive Angle Lean=");
                //      Debug.Log(anglelean);
            }
            //    }


            //    else
            //            else if(isteer == -10000 || MainControllerDrag.gear == 0)  // Mobile Accelerometer Module
            else if (iaccelerometeron == 0)
            {
                //                      Debug.Log("Entered recover steering iaccelerometeron=" + iaccelerometeron);
                if (anglelean >= 5)
                {
                    //               Debug.Log("Entered steering left");
                    //      steering = -maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent bikejawa
                                                             //   BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                    BikeZX10.transform.Rotate(danglelean * Time.deltaTime, 0, 0);   // Leaning Bike

                    anglelean = anglelean - (danglelean * Time.deltaTime);

                    //    if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, 0.25f, 0);   // Handlebar Rotate
                    //    if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0, 0.25f, 0);   // Handlebar Rotate
                    //    angleleanHandlebar = angleleanHandlebar - 0.25f;

                    if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, 0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                    if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0, 0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                    angleleanHandlebar = angleleanHandlebar - 0.25f * danglelean * Time.deltaTime;
                    //New
                    steering = -(maxSteeringAngle * anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                    transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa
                }
                if (anglelean <= -5)
                {
                    //     steering = maxSteeringAngle * 10 * Time.deltaTime;
                    transform.Rotate(0, steering / 100, 0);  // Steering Parent bikejawa
                                                             //    BikeZX10.transform.Rotate(0, 0, danglelean * Time.deltaTime);   // Leaning Bike
                    BikeZX10.transform.Rotate(-danglelean * Time.deltaTime, 0, 0);   // Leaning Bike

                    anglelean = anglelean + (danglelean * Time.deltaTime);

                    //  if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, -0.25f, 0);   // Handlebar Rotate
                    //  if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0, -0.25f, 0);   // Handlebar Rotate
                    //  angleleanHandlebar = angleleanHandlebar + 0.25f;

                    if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.Rotate(0, -0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                    if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.Rotate(0, -0.25f * danglelean * Time.deltaTime, 0);   // Handlebar Rotate
                    angleleanHandlebar = angleleanHandlebar + danglelean * Time.deltaTime;
                    //New
                    steering = (maxSteeringAngle * -anglelean * Time.deltaTime) * (1 - Mathf.Abs(bikejawa.velocity.x) / 1500);
                    transform.Rotate(0, steering * 3 / 100, 0);  // Steering Parent bikejawa
                }
                //            Debug.Log("Recover Steering Angle =");
                //            Debug.Log(steering);
                //            Debug.Log("Recover Lean Angle =");
                //            Debug.Log(anglelean);
            }

            if (angletilt > 0 && rightarrowengaged != 1)
            {
                //        transform.Rotate(-0.5f * steering * Time.deltaTime, 0, 0);
            }
            if (angletilt < 0 && rightarrowengaged != 1)
            {
                //      transform.Rotate(0.5f * steering * Time.deltaTime, 0, 0);
            }
            //    bikejawa.AddForce(accelpedal.value * forwardspeed * (Vector3.forward + Vector3.back / 2));

            //     bikejawa.velocity = accelpedal.value * forwardspeed * (Vector3.left);  // Forward

            //     bikejawa.velocity = accelpedal.value * forwardspeed * (Vector3.left + steering* Vector3.forward);  // Forward
        } // Crashed Brace


        //     if(MainControllerDrag.gear == 0) ReorientRollatNeutral(); // Set Vehicle Upright after Vehicle comes to neutral

        //Detect Tunnels and Change Sound
        TunnelDetectFadeSound();

        inputaccelxold = Input.acceleration.x;
    }

    public void TunnelDetectFadeSound()
    {

        // Detect Tunnels
        //Detect Direction
        vehiclepositionx = bikejawa.transform.position.x;
        vehicledirectionx = vehiclepositionx - vehiclepositionoldx;
        //  Debug.Log("Vehicle Direction=");
        //  Debug.Log(vehicledirection);
        //      if (bikejawa.transform.position == new Vector3(1, 1, 1))


        //     else if (vehicledirectionx > 0 && ivehicleintunnel == 1) it = 0;

        // City Roads Tunnel 1
        if (GUIScript.ilocation == 1)
        {

            if (tunneldotprod1 > 0 && ivehicleintunnel == 1) it = 0;
            if (tunneldotprod1 < 0 && ivehicleintunnel == 0) it = 0;
            else if (tunneldotprod1 < 0 && ivehicleintunnel == 1) it = 1;
            else if (tunneldotprod1 > 0 && ivehicleintunnel == 0) it = 1;
            // Tunnel Entrance 1
            Vector3 Tunnel1Pos1 = new Vector3(-12707, 0.968f, -585);
            Vector3 Tunnel1Pos2 = new Vector3(-12703, -0.83f, 36);
            Vector3 Tunnel1Pos3 = new Vector3(-12703, 70, 36);
            Vector3 Dir11 = Tunnel1Pos1 - Tunnel1Pos2;
            Vector3 Dir12 = Tunnel1Pos3 - Tunnel1Pos2;
            Vector3 TunnelDir1 = Vector3.Cross(Dir11, Dir12).normalized;
            //   Vector3 TunnelVehicleDot = Vector3.Dot(Dir11, Dir11);
            tunneldotprod1 = Vector3.Dot(TunnelDir1, bikejawa.velocity);
            //        Debug.Log("Tunnel Dot Prod1 = " + tunneldotprod1);

            // Tunnel Exit
            Vector3 Tunnel2Pos1 = new Vector3(-14864, 0.831f, -582);
            Vector3 Tunnel2Pos2 = new Vector3(-14806, -0.83f, 30);
            Vector3 Tunnel2Pos3 = new Vector3(-14826, 70, 30);
            Vector3 Dir21 = Tunnel1Pos1 - Tunnel1Pos2;
            Vector3 Dir22 = Tunnel1Pos3 - Tunnel1Pos2;
            Vector3 TunnelDir2 = Vector3.Cross(Dir11, Dir12).normalized;
            //   Vector3 TunnelVehicleDot = Vector3.Dot(Dir11, Dir11);
            tunneldotprod2 = Vector3.Dot(TunnelDir1, bikejawa.velocity);
            //     Debug.Log("Tunnel Dot Prod2 = " + tunneldotprod2);
        }

        if (GUIScript.ilocation == 4)
        {
            if (GLtunneldotprod1 > 0 && ivehicleintunnel == 1) it = 0;
            if (GLtunneldotprod1 < 0 && ivehicleintunnel == 0) it = 0;
            else if (GLtunneldotprod1 < 0 && ivehicleintunnel == 1) it = 1;
            else if (GLtunneldotprod1 > 0 && ivehicleintunnel == 0) it = 1;
            // Greenland DayTunnel 1

            // Tunnel Entrance 1
            Vector3 GLTunnel1Pos1 = new Vector3(-12707, 0.968f, -585);
            Vector3 GLTunnel1Pos2 = new Vector3(-12703, -0.83f, 36);
            Vector3 GLTunnel1Pos3 = new Vector3(-12703, 70, 36);
            Vector3 GLDir11 = GLTunnel1Pos1 - GLTunnel1Pos2;
            Vector3 GLDir12 = GLTunnel1Pos3 - GLTunnel1Pos2;
            Vector3 GLTunnelDir1 = Vector3.Cross(GLDir11, GLDir12).normalized;
            //   Vector3 TunnelVehicleDot = Vector3.Dot(Dir11, Dir11);
            GLtunneldotprod1 = Vector3.Dot(GLTunnelDir1, bikejawa.velocity);
            //     Debug.Log("GL Tunnel Dot Prod1 = " + GLtunneldotprod1);


            // Tunnel Exit
            Vector3 GLTunnel2Pos1 = new Vector3(-56328, 1355, -11473);
            Vector3 GLTunnel2Pos2 = new Vector3(-56691, 1373, -11498);
            Vector3 GLTunnel2Pos3 = new Vector3(-56691, 1500, -11498);
            Vector3 GLDir21 = GLTunnel2Pos1 - GLTunnel2Pos2;
            Vector3 GLDir22 = GLTunnel2Pos3 - GLTunnel2Pos2;
            Vector3 GLTunnelDir2 = Vector3.Cross(GLDir21, GLDir22).normalized;
            //   Vector3 TunnelVehicleDot = Vector3.Dot(Dir11, Dir11);
            GLtunneldotprod2 = Vector3.Dot(GLTunnelDir2, bikejawa.velocity);
            //     Debug.Log("Tunnel Dot Prod2 = " + GLtunneldotprod2);
        }

        //     if (Vector3.Dot(TunnelDir1, bikejawa.velocity) < 1.2 && Vector3.Dot(TunnelDir1, bikejawa.velocity) > 0.8)
        //    {

        //        Debug.Log("Positive Vel, Dot Prod = " + Vector3.Dot(TunnelDir1, bikejawa.velocity));

        //    }
        //    else
        //    {
        //        Debug.Log("Negative Vel, Dot Prod = " + Vector3.Dot(TunnelDir1, bikejawa.velocity));
        //    }
        // if (Vector3.Dot(forward, toOther) < 0)
        //      var perp: Vector3 = Vector3.Cross(side1, side2);
        //   Vector3 dir = bluePos - redPos;
        //   float distance = Vector3.Distance(redPos, bluePos);
        //   Vector3 oneThird = redPos + dir * (distance * 0.3f);
        // Testing end

        if (GUIScript.ilocation == 1)
        {
            if (bikejawa.transform.position.x <= -12400 && bikejawa.transform.position.x > -13000 && bikejawa.transform.position.z > -620 && bikejawa.transform.position.z < 55 && it == 0)
            {
                Debug.Log("Inside Tunnel 1");
                if (tunneldotprod1 < 0)
                {
                    StartCoroutine("FadeinTunnelSound");
                    ivehicleintunnel = 1;
                }
                else if (tunneldotprod1 > 0)
                {
                    StartCoroutine("FadeoutTunnelSound");
                    ivehicleintunnel = 0;
                }
                it++;
            }
            if (bikejawa.transform.position.x <= -14502 && bikejawa.transform.position.x > -15400 && bikejawa.transform.position.z > -582 && bikejawa.transform.position.z < 30 && it == 1)
            {
                Debug.Log("Inside Tunnel 2");
                if (tunneldotprod2 < 0)
                {
                    StartCoroutine("FadeoutTunnelSound");
                    ivehicleintunnel = 0;
                }
                else if (tunneldotprod2 > 0)
                {
                    StartCoroutine("FadeinTunnelSound");
                    ivehicleintunnel = 1;
                }
                it--;

            }
        }
        //   Debug.Log("FaeinTunnel Vehicledirection = " + vehicledirectionx);
        //   Debug.Log("FaeinTunnel ivehicleintunnel = " + ivehicleintunnel);
        //     Debug.Log("FaeinTunnel it = " + it);
        if (GUIScript.ilocation == 4)  // GreenLand Day 
        {
            //     if (bikejawa.transform.position.x <= -20915 && bikejawa.transform.position.x > -22000 && bikejawa.transform.position.z > 1270 && bikejawa.transform.position.z <1561 && it == 0)
            if (bikejawa.transform.position.x <= -20939 && bikejawa.transform.position.x > -21123 && bikejawa.transform.position.z > 1251 && bikejawa.transform.position.z < 1593 && it == 0)

            {
                Debug.Log("Inside Tunnel Greenland");
                if (GLtunneldotprod1 < 0)
                {
                    //     Debug.Log("FaeinTunnel Vehicledirection = " + vehicledirection);
                    StartCoroutine("FadeinTunnelSound");
                    ivehicleintunnel = 1;
                }
                else if (GLtunneldotprod1 > 0)
                {
                    Debug.Log("FaeoutTunnel Vehicledirection = " + vehicledirectionx);
                    StartCoroutine("FadeoutTunnelSound");
                    ivehicleintunnel = 0;
                }
                it++;
            }
            //  Debug.Log("Greenland x " + bikejawa.transform.position.x);    
            //  Debug.Log("Greenland z " + bikejawa.transform.position.z);
            //  Debug.Log("Greenland it=" + it);

            //     if (bikejawa.transform.position.x <= -56327 && bikejawa.transform.position.x > -56664 && bikejawa.transform.position.z <= -11394 && bikejawa.transform.position.z > -11631 && it == 1)
            if (bikejawa.transform.position.x <= -56327 && bikejawa.transform.position.x > -56660 && bikejawa.transform.position.z <= -11394 && bikejawa.transform.position.z > -11500 && it == 1)

            {
                Debug.Log("Outside Tunnel Greenland");
                if (GLtunneldotprod2 < 0)
                {
                    StartCoroutine("FadeoutTunnelSound");
                    ivehicleintunnel = 0;
                }
                else if (GLtunneldotprod2 > 0)
                {
                    StartCoroutine("FadeinTunnelSound");
                    ivehicleintunnel = 1;
                }
                it--;

            }
        }



        //  if (vehicledirection > 0) it = 1;
        //  else it = 0;

        if (bikejawa.transform.position.x <= -12400 && bikejawa.transform.position.x > -14502 && it == 0)
        {
            //     it++;  // Counter to decide if bike in tunnel , 1-in Tunnel, 0 out
            //     bikejawa.transform.position = new Vector3(1, 1, 1);
            //      AudioEngineZX25Drag.ReverbXoneMix = 1.1f;
            //   AudioEngineZX25.TunnelAppear();
            //   Debug.Log("Tunnelin");
            //   StartCoroutine("FadeinTunnelSound");
        }
        if ((bikejawa.transform.position.x <= -14502 || bikejawa.transform.position.x > -12400) && it == 1)
        {
            //      it--;
            //     bikejawa.transform.position = new Vector3(1, 1, 1);
            //      AudioEngineZX25Drag.ReverbXoneMix = 1.1f;
            //   AudioEngineZX25.RoadAppear();
            //    Debug.Log("Tunnelout");
            //    Debug.Log("IT=");
            //    Debug.Log(it);
            //    StartCoroutine("FadeoutTunnelSound");
        }
    }
    IEnumerator FadeinTunnelSound()
    {
        i = 0;
        if (MainControllerDrag.ivehicle == 1) SoundEngineZX25Drag.ReverbXoneMix = 0;
        if (MainControllerDrag.ivehicle == 2) SoundEngineBullet500Drag.ReverbXoneMix = 0;

        //    Debug.Log("Tunnel Out SOund***************************************1");
        Debug.Log("TunnelReverboneMix=***************************************1");
        Debug.Log(SoundEngineZX25Drag.ReverbXoneMix);
        while (i <= 11)
        {

            yield return new WaitForSeconds(0.05f); // wait half a second
            if (MainControllerDrag.ivehicle == 1)
            {
                SoundEngineZX25Drag.ReverbXoneMix = SoundEngineZX25Drag.ReverbXoneMix + 0.1f;
                AudioEngineZX25.TunnelAppear();  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            }
            else if (MainControllerDrag.ivehicle == 2)
            {
                SoundEngineBullet500Drag.ReverbXoneMix = SoundEngineBullet500Drag.ReverbXoneMix + 0.1f;
                AudioEngineBullet500.TunnelAppear();  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            }
            Debug.Log("TunnelReverboneMix=***************************************2");
            Debug.Log(SoundEngineZX25Drag.ReverbXoneMix);
            i++;
            //       else if (iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
        if (MainControllerDrag.ivehicle == 1) SoundEngineZX25Drag.ReverbXoneMix = 1.1f;
        else if (MainControllerDrag.ivehicle == 2) SoundEngineBullet500Drag.ReverbXoneMix = 1.1f;
    }



    IEnumerator FadeoutTunnelSound()
    {
        i = 0;

        // Debug.Log("Tunnel In SOund***************************************1");
        // Debug.Log("TunnelReverboneMix=***************************************1");
        // Debug.Log(AudioEngineZX25Drag.ReverbXoneMix);
        while (i <= 11)
        {

            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.05f); // wait half a second
            if (MainControllerDrag.ivehicle == 1)
            {
                SoundEngineZX25Drag.ReverbXoneMix = SoundEngineZX25Drag.ReverbXoneMix - 0.1f;
                AudioEngineZX25.TunnelAppear();       //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


            }
            else if (MainControllerDrag.ivehicle == 2)
            {
                SoundEngineBullet500Drag.ReverbXoneMix = SoundEngineBullet500Drag.ReverbXoneMix - 0.1f;
                AudioEngineBullet500.TunnelAppear();       //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            }

            Debug.Log("TunnelReverboneMix=***************************************2");
            Debug.Log(SoundEngineZX25Drag.ReverbXoneMix);
            i++;
            //       else if (iexhaust == 1) ANFSRAkra1.NFSRFadein();
        }
        if (MainControllerDrag.ivehicle == 1) SoundEngineZX25Drag.ReverbXoneMix = 0f;
        else if (MainControllerDrag.ivehicle == 2) SoundEngineBullet500Drag.ReverbXoneMix = 0f;
    }


    private void updateWheels()
    {
        float delta = Time.fixedDeltaTime;

        foreach (WheelData w in wheels)
        {
            WheelHit hit;

            //    Vector3 localPos = w.wheelTransform.localPosition;
            if (w.wheelCollider.GetGroundHit(out hit))
            {
                //      localPos.y -= Vector3.Dot(w.wheelTransform.position - hit.point, transform.up) - wheelRadius;
                //      w.wheelTransform.localPosition = localPos;
            }
            else
            {
                //     localPos.y = w.wheelStartPos.y;
            }

            w.rotation = Mathf.Repeat(w.rotation + delta * w.wheelCollider.rpm * 360.0f / 60.0f, 360f);
            //     WColForward.steerAngle = Mathf.Clamp(1, -1, 1) * maxSteeringAngle;
            w.wheelTransform.localRotation = Quaternion.Euler(w.rotation, 0, 0);
            //      w.wheelTransform.rotation = Quaternion.Euler(w.rotation, 0, 0); ;
        }
    }

    public void LeftSteeron()
    {

        if (iaccelerometeron == 0)
        {
            Debug.Log("Inside Left Steeron");
            isteer = -1;
        }

    }
    public void LeftSteeroff()
    {
        if (iaccelerometeron == 0) isteer = 0;
    }
    public void RightSteeron()
    {
        if (iaccelerometeron == 0)
        {
            Debug.Log("Inside Right Steeron");
            isteer = 1;
        }
    }
    public void RightSteeroff()
    {
        if (iaccelerometeron == 0) isteer = 0;
    }

    public void RightTurnVal(float sliderposition)
    {
        if (MainControllerDrag.gear != 0)   // Avoid tilting bike at Gear =0
        {
            if (sliderposition > 0) isteer = 1;
            else if (sliderposition < 0) isteer = -1;
            maxleanangle = sliderposition * isteer;

        }


    }
    public void AccelerometerTurnVal()
    {
        Debug.Log("Accelerometer");
        if (Input.acceleration.x > 0) isteer = 1;
        else if (Input.acceleration.y < 0) isteer = -1;
        if (Input.acceleration.x > 40f)
        {
            maxleanangle = 40f;
        }
        else maxleanangle = Input.acceleration.x * 100 * isteer;





    }
    public void LeftTurnVal(float sliderposition)
    {
        isteer = -1;
        //   maxleanangle = sliderposition;
        accelvaltest = sliderposition;


    }
    public void AccelerometerTurnTest()
    {
        if (iaccelerometerTurnTest == 1 && ivehiclecrashed == 1)
        {
            BikeZX10.transform.Rotate(0, 0, Input.acceleration.x * maxleananglemagnitude * Time.deltaTime);   // Leaning Bike
                                                                                                              //     BikeZX10.transform.Rotate(0, 0, maxleananglemagnitude*accelvaltest * Time.deltaTime);   // Leaning Bike
        }
        else if (iaccelerometerTurnTest == 2)
        {
            //    BikeZX10.transform.Rotate(0, 0,  Input.acceleration.y * maxleananglemagnitude * Time.deltaTime);   // Leaning Bike

        }
        else if (iaccelerometerTurnTest == 3)
        {
            //    BikeZX10.transform.Rotate(0, 0, Input.acceleration.z * maxleananglemagnitude * Time.deltaTime);   // Leaning Bike
        }

    }
    public void AccelerometerTrunTestActivate()
    {
        if (ivehiclecrashed == 1) ivehiclecrashed = 0;
        else ivehiclecrashed = 1;
    }

    public void acceltestval1()
    {
        iaccelerometerTurnTest = 1;
    }
    public void acceltestval2()
    {
        iaccelerometerTurnTest = 2;
    }
    public void acceltestval3()
    {
        iaccelerometerTurnTest = 3;
    }
    public void AcceltestTurnVal(float sliderposition)
    {
        //       dangleleanmultiplier = sliderposition;
        //    maxleananglemagnitude = sliderposition;


    }

    public void QuitGame()
    {
        Application.Quit();

    }
    public void MainMenuBack()  // Defunct
    {
        SceneManager.LoadScene(0);  // Track1 Scene
                                    //   Application.Quit();
    }

    int islscrash = 0;
    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator SlowSpeedSideRailCrash()
    {
        //     if (ivibrator == 1) //Handheld.Vibrate();
        //         while (ivibrating == 1)
        Debug.Log(" Entered Low SPeed Crash Routine");
        //   if(anglelean > 0)  BikeZX10.transform.Rotate(0, 0, -anglelean);   // Leaning Bike
        for (i = 1; i < 10; i++)
        {
            yield return new WaitForSeconds(0.05f);
            if (islscrash == 1)
            {
                transform.Rotate(0, 0.00f, 0);  // Steering Parent bikejawa
                                                //     BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
                if (icollisiondirection == -1) BikeZX10.transform.Rotate(0, 0, -1);   // Leaning Bike
                else if (icollisiondirection == 1) BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
                islscrash = 0;
            }
            else if (islscrash == 0)
            {
                //    transform.Rotate(0, 0.05f, 0);  // Steering Parent bikejawa
                //    BikeZX10.transform.Rotate(0, 0,  danglelean * Time.deltaTime);   // Leaning Bike
                if (icollisiondirection == -1) BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
                else if (icollisiondirection == 1) BikeZX10.transform.Rotate(0, 0, -1);   // Leaning Bike
                islscrash = 1;
            }
        }
        //      BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
        //    ivehiclecrashing = 0;
        //    transform.Rotate(0, 10, 0);  // Steering Parent bikejawa
        //     yield return new WaitForSeconds(0.05f);
        //     if(isiderailcollisionexit == 1)
        //     {
        //         transform.Rotate(0, -20, 0);  // Steering Parent bikejawa
        //         isiderailcollisionexit = 0;
        //     }

    }

    // Vibrating on Off upon Hitting Rev Limiter
    IEnumerator HighSpeedCrash()
    {
        //     if (ivibrator == 1) //Handheld.Vibrate();
        //         while (ivibrating == 1)
        Debug.Log(" EntereHigh SPeed Crash Routine");
        //   if(anglelean > 0)  BikeZX10.transform.Rotate(0, 0, -anglelean);   // Leaning Bike
        //  Physics.gravity = new Vector3(0, -2000.0F, 0);
        //     bikejawa.AddTorque(Vector3.Cross(lastPos, initPos) * 1000, orceMode.Impulse);

        //    BikeZX10.transform.Rotate(1000 * Time.deltaTime, 0, 0);   // Leaning Bike

        //     if (icollisiondirection == -1)
        // Test for Crash Tunn Routine()()()()))*)) 
        //   BikeZX10.transform.Rotate(90f, 0, 0);   // Bike Rotates by 90 degree upon crash
        transform.position = originalPos1;  // Location 2 time steps before so Bike doesn;t stick to railings and climbs on them after crash
                                            //   transform.Translate(0, 500, 0);    // Testing for Bike to be above ground after crash so that underground isnt visible
        int crashcount = UnityEngine.Random.Range(0, 10);

        if (anglelean >= 0)
        {
            Debug.Log("CrashAnglelean =" + anglelean);
            BikeZX10.transform.Rotate(-(70 - anglelean), 0, 0);   // Bike Rotates by 90 degree upon crash
        }
        else
        {
            Debug.Log("CrashAnglelean2 =" + anglelean);
            BikeZX10.transform.Rotate((70 + anglelean), 0, 0);   // Bike Rotates by 90 degree upon crash
        }
        //    if(crashcount <= 5 ) BikeZX10.transform.Rotate(90f, 0, 0);   // Bike Rotates by 90 degree upon crash
        //    else BikeZX10.transform.Rotate(-90f, 0, 0);   // Bike Rotates by 90 degree upon crash

        //    else if(icollisiondirection == 1) JavaPhysics.transform.Rotate(-90f, 0, 0);   // Leaning Bike
        anglevehiclerotateoncrash = anglevehiclerotateoncrash + 90f;
        //    StartCoroutine("HighSpeedCrashSpeedReduce");
        for (i = 1; i < 2; i++)
        {
            yield return new WaitForSeconds(2.02f);
            Debug.Log("Java Physics Inverse Rotate");
            //     JavaPhysics.transform.Translate(0, 0, 40);   // Leaning Bike
            //     if (icollisiondirection == -1) JavaPhysics.transform.Rotate(90f, 0, 0);   // Leaning Bike
            //      else if(icollisiondirection == 1) JavaPhysics.transform.Rotate(-90f, 0, 0);   // Leaning Bike
            //     BoxColliderLeft.SetActive(true);
            //     BoxColliderRight.SetActive(true);
            yield return new WaitForSeconds(2.02f);
            Debug.Log("Java Phyics ivehiclecrshed 0");
            //      transform.position = originalPos2;
            //      transform.rotation = Quaternion.Slerp(transform.rotation, originalRotationValue, 1f);
            //   BikeZX10.transform.rotation = originalRotationValueZX10precrash;
            //  anglelean = 0;   // Stop recovery of lean after reorientation to vertical pos
            ReorientAfterHighSpeedCrash();

            BoxColliderLeft.SetActive(true);
            BoxColliderRight.SetActive(true);

            ivehiclecrashed = 0;
            ivehiclecrashingsound = 0;
            StartCoroutine("VehiclePositionHistory");
            //     JavaPhysics.transform.Rotate(100 * Time.deltaTime, 0, 0);   // Leaning Bike
            //   BikeZX10.transform.Rotate(100 * Time.deltaTime, 0, 0);   // Leaning Bike
            //     BikeZX10.transform.Rotate(0, 100 * Time.deltaTime, 0);   // Leaning Bike 
        }
        for (i = 1; i < 50; i++)
        {
            yield return new WaitForSeconds(0.05f);
            //         if (islscrash == 1)
            //         {
            // transform.Rotate(0, 0.00f, 0);  // Steering Parent bikejawa
            //      BikeZX10.transform.Rotate(100 * Time.deltaTime, 0, 0);   // Leaning Bike
            //        BikeZX10.transform.Rotate(0,500 * Time.deltaTime, 0);   // Leaning Bike 
            //        BikeZX10.transform.Rotate(0, 0, 20 * Time.deltaTime);   // Leaning Bike
            //     BikeZX10.transform.Rotate(0, 0, -danglelean * Time.deltaTime);   // Leaning Bike
            //     if (icollisiondirection == -1) BikeZX10.transform.Rotate(0, 0, -1);   // Leaning Bike
            //     else if (icollisiondirection == 1) BikeZX10.transform.Rotate(0, 0, 1);   // Leaning Bike
            islscrash = 0;
            //      }
        }
    }
    float bikerzx10crashrotateangle = 0;
    private IEnumerator HighSpeedCrashSpeedReduce()
    {
        Debug.Log("HighSpeedCrashSpeed1");
        //    SoundEngineZX25Drag.itrate = -10000;
        while (bikerzx10crashrotateangle < 90)
        {

            yield return new WaitForSeconds(0.05f);
            Debug.Log("HighSpeedCrashSpeed2");
            //    MainControllerDrag.bikespeed = MainControllerDrag.bikespeed - 10;   //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            bikerzx10crashrotateangle = bikerzx10crashrotateangle + 5;
            Debug.Log("BikeZX10RotateAngle=" + bikerzx10crashrotateangle);
            BikeZX10.transform.Rotate(bikerzx10crashrotateangle, 0, 0);   // Leaning Bike
        }
        bikerzx10crashrotateangle = 0;
        //    MainControllerDrag.bikespeed = 0;
    }

    private IEnumerator VehiclePositionHistory()
    {
        Debug.Log("Storing Veicle Position1 ivehiclecrashed= " + ivehiclecrashed);
        while (ivehiclecrashed == 0)
        {
            //     while (ivehiclecrashed == 0  && originalPos2 != originalPos1) {
            yield return new WaitForSeconds(1.0f);
            //       Debug.Log("Storing Veicle Position2 ");
            originalPos5 = originalPos4;
            originalPos4 = originalPos3;
            originalPos3 = originalPos2;
            originalPos2 = originalPos1;
            originalPos1 = transform.position;
            //  originalRotationValue = transform.rotation; // save the initial rotation
        }
    }


    public void VehicleBackupStart()
    {
        StartCoroutine("VehicleBackUp1");
        ivehiclebackup = 0;
    }
    public void VehicleBackupStop()
    {
        StopCoroutine("VehicleBackUp1");
    }

    int ivehiclebackup = 0;
    private IEnumerator VehicleBackUp1()  // Preselected Positions
    {
        while (ivehiclebackup <= 5)
        {
            yield return new WaitForSeconds(1.0f);
            Debug.Log("ivehiclebackup=" + ivehiclebackup);
            if (ivehiclebackup == 1) transform.position = originalPos1;
            else if (ivehiclebackup == 2) transform.position = originalPos2;
            else if (ivehiclebackup == 3) transform.position = originalPos3;
            else if (ivehiclebackup == 4) transform.position = originalPos4;
            else if (ivehiclebackup == 5) transform.position = originalPos5;
            originalRotationValueprecrash = transform.rotation;
            originalRotationValueZX10precrash = BikeZX10.transform.rotation;
            //     originalRotationValueProfileCameraprecrash = ProfileCamera.transform.rotation; // save the initial rotation
            ivehiclebackup++;
        }
    }

    public void VehicleReverse()  // Just pull vehicle back through negative pull
    {
           if (MainControllerDrag.gear == 0)
           {
        Debug.Log("Vehicle Reverse");
        //      transform.Translate(Vector3.right *  5);
        bikejawa.velocity = transform.right * -50 * 0.5f * -5;
        ivehiclereversing = 1;
        StartCoroutine("VehicleReverseCoroutine");
          }
    }

    private IEnumerator VehicleReverseCoroutine()
    {
        while (ivehiclereversing == 1)
        {
            yield return new WaitForSeconds(0.05f);
            bikejawa.velocity = transform.right * -20 * 0.5f * -5;
        }


    }

    public void VehicleReverseStop()
    {
        ivehiclereversing = 0;
    }


    private IEnumerator AnimationIdleBlipping()
    {
        while (SoundEngineZX25Drag.itrate == 5000)
        {
            yield return new WaitForSeconds(0.05f);
            riderArmZX25.transform.Rotate(10, 0, 0);
            riderForeArm.transform.Rotate(5, 0, 0);
            riderHandZX25.transform.Rotate(5, 0, 0);
        }


    }
    private IEnumerator BrakeRelease()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.BrakeReleaseFromTouch();     //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    }

    public void ReorientTest()
    {
        //   transform.position = originalPos;  Ride Start Location
        transform.position = originalPos2;  // Location 2 time steps before
        //  transform.rotation = Quaternion.Slerp(transform.rotation, originalRotationValue, 1f);
        transform.rotation = originalRotationValue;
        BikeZX10.transform.rotation = originalRotationValueZX10;
        //    ProfileCamera.transform.rotation = originalRotationValueProfileCamera;
        //   ProfileCamera.transform.rotation = originalRotationValueProfileCamera;
        anglelean = 0;

    }

    public void ReorientAfterHighSpeedCrash()
    {
        transform.position = originalPos2;
        //  transform.rotation = Quaternion.Slerp(transform.rotation, originalRotationValue, 1f);
        transform.rotation = originalRotationValueprecrash;
        BikeZX10.transform.rotation = originalRotationValueZX10;
        if (MainControllerDrag.ivehicle == 1) BikeZX25Handlebar.transform.rotation = originalRotationValueHandlebar;
        if (MainControllerDrag.ivehicle == 2) BikeBullet500Handlebar.transform.rotation = originalRotationValueHandlebar;
        //  ProfileCamera.transform.rotation = originalRotationValueProfileCameraprecrash;
        anglelean = 0;
        angleleanHandlebar = 0;

    }

    public void SteeringControlToggle()   // Toggle from Mobile tilt to Arrows
    {
        //   Debug.Log("Inside SteeringControlToggle");
        if (iaccelerometeron == 0) iaccelerometeron = 1;
        else if (iaccelerometeron == 1)

        {
            iaccelerometeron = 0;
            maxleanangle = 40f;
            danglelean = 50;
            anglelean = 0;
            angleleanHandlebar = 0;
            isteer = 0;
        }


    }


    void BlipAnimate()
    {
        if (MainControllerDrag.bikespeed == 0 && SoundEngineZX25Drag.itrate != 1 && SoundEngineZX25Drag.itrate != 10000)  // itrate 1 at Idling, 10000 at Revlimiter
        {
            //         Debug.Log("Rider Animation 1");
            //     StartCoroutine("AnimationIdleBlipping");
            //  if(SoundEngineZX25Drag.rpm1 )
            if (MainControllerDrag.ivehicle == 1) anglerotArpmrpmx = 15f * (16000f / (16000f + (16000f - SoundEngineZX25Drag.rpm1)));
            else if (MainControllerDrag.ivehicle == 2) anglerotArpmrpmx = 15f * (5000f / (5000f + (5000f - SoundEngineBullet500Drag.rpm1)));
            if (SoundEngineZX25Drag.itrate == 5000 || SoundEngineZX25Drag.itrate == 2 || SoundEngineBullet500Drag.itrate == 5000 || SoundEngineBullet500Drag.itrate == 2)
            {
                Debug.Log("Rider Animation 2");
                //    anglerotHand = (100000 / (17000 - SoundEngineZX25Drag.rpm1)) * 0.20f;
                //   anglerotForearm = (100000 / (17000 - SoundEngineZX25Drag.rpm1)) * 0.8f;
                //   anglerotArm = (100000 / (17000 - SoundEngineZX25Drag.rpm1)) * 0.15f;
                if (Mathf.Abs(anglerotArm) < anglerotArpmrpmx)
                {
                    Debug.Log("Rider Animation 3");
                    anglerotArm = anglerotArm + 0.15f;
                    anglerotForearm = anglerotHand + 0.5f;
                    anglerotHand = anglerotHand - 1.0f;
                    if (MainControllerDrag.ivehicle == 1)
                    {
                        Debug.Log("Rider Animation 4");
                        riderArmZX25.transform.Rotate(0.15f, 0, 0);
                        //       riderForeArm.transform.Rotate(anglerotForearm, 0, 0);
                        riderHandZX25.transform.Rotate(-1f, 0, 0);
                    }
                    else if (MainControllerDrag.ivehicle == 2)
                    {
                        Debug.Log("Rider Animation 5");
                        riderArmBullet500.transform.Rotate(0.15f, 0, 0);
                        //       riderForeArm.transform.Rotate(anglerotForearm, 0, 0);
                        riderHandBullet500.transform.Rotate(-1f, 0, 0);
                    }
                }
                //  else anglerotArm = 0;
            }

            //     else if (SoundEngineZX25Drag.itrate == 6100 || SoundEngineZX25Drag.itrate == 6009 || SoundEngineZX25Drag.itrate == 6008 || SoundEngineZX25Drag.itrate == 6007 || SoundEngineZX25Drag.itrate == 6006 || SoundEngineZX25Drag.itrate == 6005 || SoundEngineZX25Drag.itrate == 6004 || SoundEngineZX25Drag.itrate == 6003 || SoundEngineZX25Drag.itrate == 6002)
            else if ((SoundEngineZX25Drag.itrate >= 6002 && SoundEngineZX25Drag.itrate <= 6100) || (SoundEngineBullet500Drag.itrate == 4999))
            {
                //   Debug.Log("BikeController Itrate =" + SoundEngineZX25Drag.itrate);
                //   anglerotHand = -(100000 / (17000 - SoundEngineZX25Drag.rpm1)) * 0.20f;
                //   anglerotForearm = -(100000 / (17000 - SoundEngineZX25Drag.rpm1)) * 0.8f;
                //   anglerotArm = -(100000 / (17000 - SoundEngineZX25Drag.rpm1)) * 0.15f;
                if (anglerotArm > 0)
                {
                    anglerotArm = anglerotArm - 0.15f;
                    anglerotForearm = anglerotForearm - 0.5f;
                    anglerotHand = anglerotHand + 1f;
                    if (MainControllerDrag.ivehicle == 1)
                    {
                        Debug.Log("Rider Animation 6");
                        riderArmZX25.transform.Rotate(-0.15f, 0, 0);
                        //       riderForeArm.transform.Rotate(anglerotForearm, 0, 0);
                        riderHandZX25.transform.Rotate(1f, 0, 0);
                    }
                    else if (MainControllerDrag.ivehicle == 2)
                    {
                        Debug.Log("Rider Animation 7");
                        riderArmBullet500.transform.Rotate(-0.15f, 0, 0);
                        //       riderForeArm.transform.Rotate(anglerotForearm, 0, 0);
                        riderHandBullet500.transform.Rotate(1f, 0, 0);
                    }
                }
                //   else anglerotArm = 0;
            }
            //     Debug.Log("AnglerotArm=" + anglerotArm);
            //     Debug.Log("AngleroForearm=" + anglerotForearm);
            //      Debug.Log("AngleroArm=" + anglerotHand);



        }
    }

    private IEnumerator HelmetAnimate()
    {
        while (GUIScript.icameraview == 2)
        {
            //    yield return new WaitForSeconds(2f);


            // Animation 1, From Center to RIght
            iHelmetAnimationRandon = UnityEngine.Random.Range(0, 10);
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(1.5f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(3f);
            //   iHelmetAnimationtrigger = 1;
            //       Debug.Log("INside Helmet Animate 2 anglerot =" + anglerotRiderHelmetx);
            //   while (anglerotRiderHelmety <= 40)
            while (anglerotRiderHelmety <= UnityEngine.Random.Range(40, 80))
            {
                //            Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety + 2f;
                anglerotRiderHelmetx = anglerotRiderHelmetx + 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(0.3f, 2f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(0.3f, 2f, 0);
            }
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(2f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(2.5f);
            while (anglerotRiderHelmety >= 0)
            {
                //          Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety - 2f;
                anglerotRiderHelmetx = anglerotRiderHelmetx - 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(-0.3f, -2f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(-0.3f, -2f, 0);
            }

            // Animation 2, From Center to Left
            iHelmetAnimationRandon = UnityEngine.Random.Range(0, 10);
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(2.5f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(3f);
            //   iHelmetAnimationtrigger = 1;
            Debug.Log("INside Helmet Animate 2 anglerot =" + anglerotRiderHelmetx);
            //   while (anglerotRiderHelmety <= 40)
            while (anglerotRiderHelmety >= UnityEngine.Random.Range(-25, -80))
            {
                Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety - 2f;
                anglerotRiderHelmetx = anglerotRiderHelmetx - 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(-0.3f, -2f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(-0.3f, -2f, 0);
            }
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(2f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(3f);
            while (anglerotRiderHelmety <= 0)
            {
                //            Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety + 2f;
                anglerotRiderHelmetx = anglerotRiderHelmetx + 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(0.3f, 2f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(0.3f, 2f, 0);
            }

            //Animation Left and Up
            iHelmetAnimationRandon = UnityEngine.Random.Range(0, 10);
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(1.0f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(2f);
            //   iHelmetAnimationtrigger = 1;
            Debug.Log("INside Helmet Animate 2 anglerot =" + anglerotRiderHelmetx);
            //   while (anglerotRiderHelmety <= 40)
            while (anglerotRiderHelmetx >= UnityEngine.Random.Range(-15, -25))
            {
                //            Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety - 0.5f;
                anglerotRiderHelmetx = anglerotRiderHelmetx - 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(-0.3f, -0.5f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(-0.3f, -0.5f, 0);
            }
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(1.5f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(2f);
            while (anglerotRiderHelmetx <= 0)
            {
                Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety + 0.5f;
                anglerotRiderHelmetx = anglerotRiderHelmetx + 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(0.3f, 0.5f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(0.3f, 0.5f, 0);
            }




        }
        iHelmetAnimationtrigger = 0;
        iHelmetAnimationRevLimitertrigger = 0;  // Counter to trigger Helmet Animatio once
    }

    private IEnumerator HelmetAnimateRevLimiter()
    {

        if (anglerotRiderHelmetx > 0)
        {
            while (anglerotRiderHelmetx > 0)
            {
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(-0.3f, 0, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(-0.3f, 0, 0);
                anglerotRiderHelmetx = anglerotRiderHelmetx - 0.3f;
            }
        }
        while (GUIScript.icameraview == 2)
        {
            //    yield return new WaitForSeconds(2f);


            // Animation 1, From Center to RIght
            iHelmetAnimationRandon = UnityEngine.Random.Range(0, 10);
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(0.5f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(1f);
            //   iHelmetAnimationtrigger = 1;
            //       Debug.Log("INside Helmet Animate 2 anglerot =" + anglerotRiderHelmetx);
            //   while (anglerotRiderHelmety <= 40)
            while (anglerotRiderHelmety <= UnityEngine.Random.Range(60, 70))
            {
                Debug.Log("INside Helmet AnimateRevLimiter");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety + 1f;
                anglerotRiderHelmetx = anglerotRiderHelmetx + 0.3f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(0.3f, 1f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(0.3f, 1f, 0);
            }
            iHelmetAnimationRevLimitertrigger = 2;
        }

    }

    private IEnumerator HelmetAnimateRevLimiterEnd()
    {
        while (GUIScript.icameraview == 2)
        {
            if (iHelmetAnimationRandon < 5) yield return new WaitForSeconds(2f);

            else if (iHelmetAnimationRandon >= 5 && iHelmetAnimationRandon <= 10) yield return new WaitForSeconds(2.5f);
            while (anglerotRiderHelmety >= 0 && (SoundEngineZX25Drag.itrate != 10000 || SoundEngineBullet500Drag.itrate != 10000))
            {
                //          Debug.Log("INside Helmet Animate 3");
                yield return new WaitForSeconds(0.01f);

                anglerotRiderHelmety = anglerotRiderHelmety - 2f;
                anglerotRiderHelmetx = anglerotRiderHelmetx - 0.5f;
                if (MainControllerDrag.ivehicle == 1) riderHelmetZX25.transform.Rotate(-0.5f, -2f, 0);
                else if (MainControllerDrag.ivehicle == 2) riderHelmetBullet500.transform.Rotate(-0.5f, -2f, 0);
            }

            iHelmetAnimationtrigger = 0;  // Counter to trigger Helmet Animatio once
            iHelmetAnimationRevLimitertrigger = 0;  // Counter to trigger Helmet Animatio once
        }
    }

   
}