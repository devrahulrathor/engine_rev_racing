﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiestaViews : MonoBehaviour
{
    public GameObject FiestaGarageButton;
    public GameObject FiestaRoadButton;
    public int iFiestaview = 0;   // Appear/Disapperar Counter

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GarageRoadButtonsActive()  // 
    {
        if (iFiestaview == 0)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            FiestaGarageButton.SetActive(true);
            FiestaRoadButton.SetActive(true);


            iFiestaview = 1;
        }
        else if (iFiestaview == 1)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            FiestaGarageButton.SetActive(false);
            FiestaRoadButton.SetActive(false);
            iFiestaview = 0;
        }

    }
}
