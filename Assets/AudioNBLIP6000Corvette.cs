using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNBLIP6000Corvette : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AudioEngineCorvette.itrate == 5005)
        {
            AudioEngineCorvette.rpm1 = 6000;
            //           Debug.Log(AudioEngineCorvette.rpm1);
        }
        LowPassFilter();
    }

    //  int irpmpos = 0;
    //  double posinsec;
    public void Blip6000()
    {


        AudioSource audio = GetComponent<AudioSource>();

        if (AudioEngineCorvette.rpm1 >= 5400 && AudioEngineCorvette.rpm1 < 6000)
        {
            audio.Play();
        }
        else if (AudioEngineCorvette.rpm1 >= 4000 && AudioEngineCorvette.rpm1 < 5400)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.04F);
        }
        else if (AudioEngineCorvette.rpm1 >= 3000 && AudioEngineCorvette.rpm1 < 4000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.073F);
        }
        else if (AudioEngineCorvette.rpm1 >= 2000 && AudioEngineCorvette.rpm1 < 3000)
        {
            audio.PlayScheduled(AudioSettings.dspTime + 0.246F);
        }
        else if (AudioEngineCorvette.rpm1 < 2000)
            audio.PlayScheduled(AudioSettings.dspTime + 0.332F);
        audio.volume = 1;
        //    audio.Play();
        //    audio.Play(44100);

        //       RPMmodetext.text = "Blip";
    }

    public void Blip6000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
    public void Blip6000ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void Blip6000ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineCorvette.ReverbXoneMix;
    }
    public void LowPassFilter()
    {
        //    AudioSource audio = GetComponent<AudioSource>();
        //   audio.cutoffFrequency = (Mathf.Sin(Time.time) * 11010 + 11000);
        //    Debug.Log("Cutoff Freq = ");

        //  Debug.Log(Settings.lowpasscutoffreq);
        GetComponent<AudioLowPassFilter>().cutoffFrequency = Settings.lowpasscutoffreq;

    }
}



