using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPickerObject : MonoBehaviour
{
    public List<ColorPicker> colorPickers;
//    public Slider shineSlider;
    public Color currentColor;
    public Material matZX25, matBullet500;
    public GameObject ZX25GreenBody, ZX25OtherColorBody; // Activating dofferent parts with Green and no green textures to change color
    public float icolorshade = 0;
    public Slider cSlider;  // Slider for Body Colour
    public Slider MetallicSlider;  // Slider for Metallic Prop of Body Color
    int icolorcounter = 1;   // 1 Green

    // Riding Gear Section
    public Material Helmet, Jacket1;
    public float icolorshadeRidinggear = 0;
    public float icolorshadeHelmet = 0;
    public Slider cSliderRidinggear;  // Slider for Body Colour

    void Start()
    {
        Debug.Log("Inside Bullet SetColor0");
        SetInitialColor();
        
    }
    public void SetData()
    {
        for (int i = 0; i < colorPickers.Count; i++)
        {
        //    colorPickers[i].mat = mat;
        }

    //   currentColor = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a);
   //     shineSlider.onValueChanged.AddListener(delegate (float arg0) { ValueChangeCheck(); });
    }
    public void SetInitialColor()
    {
        Debug.Log("Inside Bullet SetColor1");
        ZX25GreenBody.SetActive(true);
        PlayerPrefs.SetInt("ZX25GreenColor", 1);  // 1-for Green Color and 0 for Non Greens
        ZX25OtherColorBody.SetActive(false);
        matBullet500.color = Color.Lerp(Color.blue, Color.green, 0.4537f);
     //   matBullet500.color = Color.Lerp(Color.green, Color.black, 1 * 0.4537f);
    }

    public void ColorGreen()
    {
        Debug.Log("INside Color Green");
        icolorcounter = 1;
        if (MainMenuScript.ivehicle == 1)
        {
            //      if (icolorshade >= 0) matZX25.color = Color.Lerp(Color.green, Color.black, icolorshade);
            //     else if (icolorshade < 0) matZX25.color = Color.Lerp(Color.green, Color.white, -1 * icolorshade);
            ZX25GreenBody.SetActive(true);
            ZX25OtherColorBody.SetActive(false);
            PlayerPrefs.SetInt("ZX25GreenColor", 1);  // 1-for Green Color and 0 for Non Greens
        }
        else if(MainMenuScript.ivehicle == 2)
        {
         if (icolorshade >= 0) matBullet500.color = Color.Lerp(Color.green, Color.black, 1 * icolorshade);
            if (icolorshade < 0) matBullet500.color = Color.Lerp(Color.green, Color.white, -1 * icolorshade);
        }
    }
    public void ColorBlue()
    {
        Debug.Log("INside Color Blue");
        icolorcounter = 2;
        if (MainMenuScript.ivehicle == 1)
        {
           
            ZX25GreenBody.SetActive(false);
            ZX25OtherColorBody.SetActive(true);
                if (icolorshade >= 0) matZX25.color = Color.Lerp(Color.blue, Color.green, icolorshade);
                  else if (icolorshade < 0) matZX25.color = Color.Lerp(Color.blue, Color.white, -1 * icolorshade);
            PlayerPrefs.SetInt("ZX25GreenColor", 0);  // 1-for Green Color and 0 for Non Greens

        }
        else if (MainMenuScript.ivehicle == 2)
        {
            if (icolorshade >= 0) matBullet500.color = Color.Lerp(Color.blue, Color.green, icolorshade);
            else if (icolorshade < 0) matBullet500.color = Color.Lerp(Color.blue, Color.white, -1 * icolorshade);
        }
    }
    public void ColorYellow()
    {
        icolorcounter = 3;
        if (MainMenuScript.ivehicle == 1)
        {
            ZX25GreenBody.SetActive(false);
            ZX25OtherColorBody.SetActive(true);
           if (icolorshade >= 0) matZX25.color = Color.Lerp(Color.yellow, Color.black, icolorshade);
           else if (icolorshade < 0) matZX25.color = Color.Lerp(Color.yellow, Color.white, -1 * icolorshade);
            PlayerPrefs.SetInt("ZX25GreenColor", 0);  // 1-for Green Color and 0 for Non Greens
        }
        else if (MainMenuScript.ivehicle == 2)
        {
            if (icolorshade >= 0) matBullet500.color = Color.Lerp(Color.yellow, Color.black, icolorshade);
            else if (icolorshade < 0) matBullet500.color = Color.Lerp(Color.yellow, Color.white, -1 * icolorshade);
        }
    }

    public void ColorRed()
    {
        Debug.Log("INside Color Red");
        icolorcounter = 4;
        if (MainMenuScript.ivehicle == 1)
        {
       
            ZX25GreenBody.SetActive(false);
            ZX25OtherColorBody.SetActive(true);
               if (icolorshade >= 0) matZX25.color = Color.Lerp(Color.red, Color.black, icolorshade);
             else if (icolorshade < 0) matZX25.color = Color.Lerp(Color.red, Color.white, -1 * icolorshade);
            PlayerPrefs.SetInt("ZX25GreenColor", 0);  // 1-for Green Color and 0 for Non Greens
        }
        else if (MainMenuScript.ivehicle == 2)
        {
            if (icolorshade >= 0) matBullet500.color = Color.Lerp(Color.red, Color.black, icolorshade);
            else if (icolorshade < 0) matBullet500.color = Color.Lerp(Color.red, Color.white, -1 * icolorshade);
        }
    }
    public void ColorOrange()
    {
        Debug.Log("INside Color Red");
        icolorcounter = 5;
        if (MainMenuScript.ivehicle == 1)
        {

            ZX25GreenBody.SetActive(false);
            ZX25OtherColorBody.SetActive(true);
            if (icolorshade >= 0) matZX25.color = Color.Lerp(Color.red, Color.yellow, (icolorshade+0.2f));
            else if (icolorshade < 0) matZX25.color = Color.Lerp(Color.red, Color.white, -1 * icolorshade);
            PlayerPrefs.SetInt("ZX25GreenColor", 0);  // 1-for Green Color and 0 for Non Greens
        }
        else if (MainMenuScript.ivehicle == 2)
        {
            if (icolorshade >= 0) matBullet500.color = Color.Lerp(Color.red, Color.black, icolorshade);
            else if (icolorshade < 0) matBullet500.color = Color.Lerp(Color.red, Color.white, -1 * icolorshade);
        }
    }


    public void Slidervalue(float sliderposition)
    {
        //      moveSpeed = newSpeed;
        icolorshade = sliderposition;
        Debug.Log("Icolorshade=" + icolorshade);
       ColourRefresh();

        //     Debug.Log("Icolorshade=");
        //     Debug.Log(icolorshade);

    }


    void ColourRefresh()
    {

        if (icolorcounter == 1) ColorGreen();
        if (icolorcounter == 2) ColorBlue();
        if (icolorcounter == 3) ColorYellow();
        if (icolorcounter == 4) ColorRed();
        if (icolorcounter == 5) ColorOrange();
        //   if (icolorcounter == 3) ColorGreen();
        //   if (icolorcounter == 4) ColorYellow();
        //   if (icolorcounter == 5) ColorBlack();
        //   if (icolorcounter == 6) ColorWhite();

    }
    private void ValueChangeCheck()
    {
      //  mat.color = Color.Lerp(currentColor, Color.black, shineSlider.value);
    }
}


