﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLaunchCConst8000 : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {

    }

    public void LaunchConst8000()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.PlayScheduled(AudioSettings.dspTime + 2.3);
        audio.Play(44100);



    }
    public void LaunchConst8000Stop()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
    }
}