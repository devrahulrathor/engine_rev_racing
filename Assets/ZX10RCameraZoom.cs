﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZX10RCameraZoom : MonoBehaviour
{
    
    

    public float perspectiveZoomSpeed = 0.35f;        // The rate of change of the field of view in perspective mode.
    public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.
    public GameObject ZX10RGameObject;

    private float rotatespeed = 0.2f;

    float finalrotspeed = 0;  // Final Speed of Rotation before touch is left
    float finalrotspeed1 = 0, finalrotspeed2 = 0, finalrotspeed3 = 0, finalrotspeedAvg = 0;


    private Vector3 point, ZX10pos = new Vector3(0, -15, 0);  // Point to Rotate Camera Around Object
    public float pitch, yaw;
    float speedH = 10.0f;
    float speedV = 10.0f;
    public float anglerotcamera = 0.0f, anglerotcameray = 0.0f, anglerotcamerafinal = 0.0f;
    float anglerotcameraold;
    public int irotatecamera = 0;  //Vounter to ensure Rotated Camera View is always of exhaust
    float deltaangle = 0f;   //Varible angle to slowly rotate Bike to show Exhaust
    float Camerafieldofview;
    int ExhaustViewRotateon = 0;  // Block Exhaust Camera Rotating                                                                
    int icamerazoomon = 0;   //Varaibale to decide if camera is zooming 1 Yes, 0 not: to disable Rotate when zooming
    int irotateon = 0;  // Counter for whether rotate touch is on: As to activate disspiating zooming

    int il = 10; // Used for Looping

    // Start is called before the first frame update
    void Start()
    {
        point = ZX10RGameObject.transform.position;

        //            transform.LookAt(point);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {

            // If there are two touches on the device...
            if (Input.touchCount == 1) icamerazoomon = 0;
            else if (Input.touchCount == 2) icamerazoomon = 1;
            //       else if (Input.touchCount == 0 && icamerazoomon == 0)  // Decide Dissipative Rotating
            //      {

            //    }

            Touch touchZero = Input.GetTouch(0);
            Vector2 pos = touchZero.position;
            if (pos.y > 120 && pos.x > 400 && pos.x < 1500 && AudioEngine.rpm1 < 3000)
            {
                //   Touch touchOne = Input.GetTouch(1);
                // Rotate the Entire Camera around the relevant Object
                //               transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 20 * Time.deltaTime * rotatespeed);
                //          point = ZX10RGameObject.transform.position;
                //      transform.LookAt(ZX10pos);
                //             var targetRotation = Quaternion.LookRotation(ZX10RGameObject.transform.position - transform.position);

                if (icamerazoomon == 0 && Input.touchCount == 1)
                {
                    anglerotcameraold = anglerotcamera;
                    //     transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), -1.0f * touchZero.deltaPosition.x * rotatespeed); transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), -1.0f * touchZero.deltaPosition.x * rotatespeed);  //horiz rotation
                    finalrotspeed = -2.0f * touchZero.deltaPosition.x * rotatespeed;
                    //               transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), finalrotspeed);
                    StartCoroutine("DissipativeRotation2");


                    //              anglerotcamera = anglerotcamera - 1.0f * touchZero.deltaPosition.x * rotatespeed;

                    finalrotspeed3 = finalrotspeed2;
                    finalrotspeed2 = finalrotspeed1;
                    finalrotspeed1 = finalrotspeed;
                    finalrotspeedAvg = (finalrotspeed1 + finalrotspeed2 + finalrotspeed3) / 3;

            //        Debug.Log("RotSpeedAvg=");
            //        Debug.Log(finalrotspeedAvg);

              //      Debug.Log("Final RotSpeeds 3, 2, 1=");
               //     Debug.Log(finalrotspeed3);
               //     Debug.Log(finalrotspeed2);
               //     Debug.Log(finalrotspeed1);

                    irotateon = 1;  // Rotation is on : for Disspiative Rotation after touch is zero
                }

                //     Debug.Log("AngleRotC=");
                //     Debug.Log(anglerotcamera);
                //              Debug.Log("AngleRotCY=");
                //                      Debug.Log(anglerotcameray);

                if (anglerotcameray <= 70.0f && anglerotcameray >= 0.0f)
                {


                    //                                transform.RotateAround(point, new Vector3((1.0f - (anglerotcamera / 45.0f)), 0.0f, (anglerotcamera / 45.0f)), 1.0f * touchZero.deltaPosition.y * rotatespeed);  // Vertical rotation
                    //                point = ZX10RGameObject.transform.position;

                    //          anglerotcameray = anglerotcameray - 0.1f * touchZero.deltaPosition.y;
                    //          transform.RotateAround(ZX10pos, new Vector3(-1.0f, 0.0f, 0.0f), 1.0f * touchZero.deltaPosition.y * rotatespeed);  // Vertical rotation
                    //      transform.LookAt(ZX10pos);
                    //               transform.Rotate(Input.mousePosition.x, Input.mousePosition.y, 0);  // Vertical rotation

                }

                //                        transform.RotateAround(point, new Vector3(0.0f, 0.0f, -1.0f), -0.1f * touchZero.deltaPosition.y * rotatespeed);
                //      transform.Rotate(0, -rotatespeed * touchZero.deltaPosition.x, 0);


                // If there are two touches on the device...
                if (Input.touchCount == 2 && icamerazoomon == 1)
                {

                    // Store both touches.
                    //           Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    // Find the position in the previous frame of each touch.
                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    // Find the magnitude of the vector (the distance) between the touches in each frame.
                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    // Find the difference in the distances between each frame.
                    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                    // If the camera is orthographic...
                    //         if (camera.isOrthoGraphic)
                    //        {
                    // ... change the orthographic size based on the change in distance between the touches.
                    //        camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

                    // Make sure the orthographic size never drops below zero.
                    //      camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
                    //      }
                    //       else
                    //       {
                    // Otherwise change the field of view based on the change in distance between the touches.
                    Camera.main.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed * 0.1f;

                    // Clamp the field of view to make sure it's between 40 and 70degrees.
                    Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 35.0f, 70.0f);
                    //     }
                }

            }
        }
        // Camera Rotate Section
        else if (Input.touchCount == 1)
        {
            //   Touch touch = Input.GetTouch(0);
            //        Vector2 touchPos = touch.position - touch.deltaPosition;


            yaw += speedH * Input.GetAxis("Mouse X");
            pitch -= speedV * Input.GetAxis("Mouse Y");
            //     transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            //               transform.eulerAngles = new Vector3(0.0f, touchPos.x, 0.0f);
        }
        else if (Input.touchCount == 0 && irotateon == 1)
        {
            il = il - 1;
            StartCoroutine("DissipativeRotation2");
            //        Debug.Log("Inside Disspiate Rotate***********************************************");
            //        transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 1.0f * (finalrotspeed1 / 10) * il);
            if (il <= 1)
            {
                //          irotateon = 0;
                il = 10;
            }
        }





    }
    public void ExhaustViewCamera()  // De-Rotate Camera to Exhaust View based on Exhaust Options Button
    {
        if (ExhaustViewRotateon == 0)
        {
            ExhaustViewRotateon = 1;
            if (irotatecamera == 0)
            {
                anglerotcamerafinal = 180 + anglerotcamera * 1.0f;
                irotatecamera = 1;

            }
            else if (irotatecamera == 1)
            {
                anglerotcamerafinal = anglerotcamera * 1.0f;
                //       irotatecamera = 0;
            }
            Debug.Log("AnglerotcameraFinal=");
            Debug.Log(anglerotcamerafinal);

            //         transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), anglerotcamerafinal);
            deltaangle = 0f;
            //       Debug.Log("Anglerotcamera=");
            //       Debug.Log(anglerotcamerafinal);
            //========================
            if (anglerotcamerafinal > 360f && anglerotcamerafinal < 720f) anglerotcamerafinal -= 360;
            else if (anglerotcamerafinal > 720f && anglerotcamerafinal < 1080f) anglerotcamerafinal -= 720;
            else if (anglerotcamerafinal < -360f && anglerotcamerafinal > -720f) anglerotcamerafinal += 360;
            else if (anglerotcamerafinal < -720f && anglerotcamerafinal > -1080f) anglerotcamerafinal += 720;
            //        if (anglerotcamerafinal > 180f && anglerotcamerafinal < 360f) anglerotcamerafinal = 180 -(anglerotcamerafinal- 180);
            //        else if (anglerotcamerafinal < -180f && anglerotcamerafinal > -360f) anglerotcamerafinal = -(180 + anglerotcamerafinal) ;
            //=======================
           if( ZX10CameraViewChange.iZX10CameraSwitch == 0) StartCoroutine("ExhaustViewCameraSlowRotate");
            anglerotcamera = 0;

        }

    }
    private IEnumerator ExhaustViewCameraSlowRotate()  // De-Rotate Camera to Exhaust View based on Exhaust Options Button
    {


        deltaangle = anglerotcamerafinal / 20.0f;


        for (int i = 1; i <= 20; i++)
        {

            yield return new WaitForSeconds(0.001f);
            transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), deltaangle);
            Camerafieldofview = Camera.main.fieldOfView;
            if (Camerafieldofview < 40)
            {
                Camera.main.fieldOfView += 1.5f;
            }
            else if (Camerafieldofview > 40)
            {
                Camera.main.fieldOfView -= 1.5f;
            }

        }
        Camera.main.fieldOfView = 40f;

        ExhaustViewRotateon = 0;  // Activate Exhaust Camera Rotation after Rotation ended
    }

    IEnumerator DissipativeRotation()
    {
        //***************
        int i = 10;
        while (i >= 0)
        {
            i = i - 1;
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.1f); // wait half a second
            //                                               transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 1.0f * (rotatespeed/10)*i);  //horiz rotation
            transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 1.0f * (finalrotspeedAvg / 1000) * i);
            //  transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), -2.0f * touchZero.deltaPosition.x * rotatespeed);
        }
        irotateon = 0;

    }

    IEnumerator DissipativeRotation2()
    {
        //***************

        int i = 7;
        while (i >= 1)
        {
            i = i - 1;
            //          Debug.Log("Enter FadeoutSLR1***********************************2");
            yield return new WaitForSeconds(0.001f); // wait half a second
            transform.RotateAround(point, new Vector3(0.0f, -1.0f, 0.0f), 1.0f * (finalrotspeedAvg * 1) / 20);
            anglerotcamera = anglerotcamera - 1f * (finalrotspeedAvg * 1) / 20;
     //       Debug.Log("Anglerotcamera=");
     //       Debug.Log(anglerotcamera);
        }
        irotateon = 0;

    }

}