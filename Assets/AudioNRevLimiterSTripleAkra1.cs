using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNRevLimiterSTripleAkra1 : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (AudioEngineSTriple.itrate == 10000)
        {
            //      RPMCal();
            // AudioEngineSTriple.rpm1 = 15000;

        }

    }

    public void RevLimiter()
    {
        StartCoroutine("RevLimiterRPMFluctuation");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

    public void RevLimiterStop()
    {
        //     Debug.Log("RevLimiterStop");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        StopCoroutine("RevLimiterRPMFluctuation");
    }


    IEnumerator RevLimiterRPMFluctuation()
    {

        while (AudioEngineSTriple.ivibrating == 1)
        {
            //          AudioEngineSTriple.rpm1 = 15000;
            yield return new WaitForSeconds(0.05f);
            //       if (AudioEngineSTriple.ivibrator == 1)
            //       {
            if (AudioEngineSTriple.rpm1 == 12000) AudioEngineSTriple.rpm1 = 11000;
            else AudioEngineSTriple.rpm1 = 12000;
            //          else if (AudioEngineSTriple.rpm1 == 14000) AudioEngineSTriple.rpm1 = 15000;
            //          }
        }

    }
    public void RevLimiterAkra1ReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void RevLimiterAkra1ReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = AudioEngineSTriple.ReverbXoneMix;
    }
}
