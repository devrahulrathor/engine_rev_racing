using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;

public class SoundMethodsS1000RR : MonoBehaviour
{
    public MainControllerS1000RR MS1000RR;

    public AudioStartupS1000RRAkra AStartup;
    public AudioStartingS1000RRAkra AStarting;
    public AudioShuttingS1000RRAkra AShutting;

    public AudioIdlingS1000RRAkra AIdling;
    public AudioIdlingSportS1000RRAkra AIdlingSport;
    public AudioNBLIPS1000RRAkra ANBLIP;
    public AudioNBLIP2000S1000RRAkra ANBLIP2000;
    public AudioNBLIP3000S1000RRAkra ANBLIP3000;
    public AudioNBLIP4000S1000RRAkra ANBLIP4000;
    public AudioNBLIP5000S1000RRAkra ANBLIP5000;
    public AudioNBLIP6000S1000RRAkra ANBLIP6000;
    public AudioNBLIP7000S1000RRAkra ANBLIP7000;
    public AudioNBLIP10000S1000RRAkra ANBLIP10000;
    public AudioNBLIP14000S1000RRAkra ANBLIP14000;

    public AudioNBLIPC3000S1000RRAkra ANBLIPC3000;
    public AudioNBLIPC4000S1000RRAkra ANBLIPC4000;
    public AudioNBLIPC5500S1000RRAkra ANBLIPC5500;
    public AudioNBLIPC6000S1000RRAkra ANBLIPC6000;
    public AudioNBLIPC7500S1000RRAkra ANBLIPC7500;
    public AudioNBLIPC10000S1000RRAkra ANBLIPC10000;
    public AudioNBLIPC14000S1000RRAkra ANBLIPC14000;
    public AudioNRevLimiterS1000RRAkra ANRevLimiter;
    public AudioNFSR15000S1000RRAkra ANFSR15000;
    public AudioNFSR7500 ANFSR7500;
    public AudioNFSRS1000RRAkra ANFSR;

    public AudioNSLIS1000RRAkra ANSLI;
    public AudioNFSI1S1000RRAkra ANFSI1;

   //     public AudioStartupAventador AStartup;
        public AudioStartingAventador AStartingAventador;
    //    public AudioShuttingAventador AShutting;

      public AudioIdlingAventador AIdlingAventador;
    //    public AudioIdlingSportAventador AIdlingSport;
        public AudioNBLIPAventador ANBLIPAventador;
        public AudioNBLIP2000Aventador ANBLIP2000Aventador;
        public AudioNBLIP3000Aventador ANBLIP3000Aventador;
        public AudioNBLIP4000Aventador ANBLIP4000Aventador;
       public AudioNBLIP5000Aventador ANBLIP5000Aventador;
       public AudioNBLIP6000Aventador ANBLIP6000Aventador;
        public AudioNBLIP7000Aventador ANBLIP7000Aventador;
        public AudioNBLIP10000Aventador ANBLIP10000Aventador;
        public AudioNBLIP14000Aventador ANBLIP14000Aventador;

       public AudioNBLIPC3000Aventador ANBLIPC3000Aventador;
       public AudioNBLIPC4000Aventador ANBLIPC4000Aventador;
        public AudioNBLIPC5500Aventador ANBLIPC5500Aventador;
         public AudioNBLIPC6000Aventador ANBLIPC6000Aventador;
        public AudioNBLIPC7500Aventador ANBLIPC7500Aventador;
        public AudioNBLIPC10000Aventador ANBLIPC10000Aventador;
    //     public AudioNBLIPC14000Aventador ANBLIPC14000Aventador;
        public AudioNRevLimiterAventador ANRevLimiterAventador;
        public AudioNFSR15000Aventador ANFSR15000Aventador;
    //    public AudioNFSR7500 ANFSR7500;
        public AudioNFSRAventador ANFSRAventador;

        public AudioNSLIAventador ANSLIAventador;
    //       public AudioNFSI1Aventador ANFSI1Aventador;
     public AudioScriptCrackle8000Aventador ACrackle8000Aventador;  // Crackle 8000

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Starting()
    {
        if(MainControllerS1000RR.ivehicle == 1)
        {
            Debug.Log("Sound Method Starting");
            AStarting.Starting();
        }
        else if(MainControllerS1000RR.ivehicle == 2)
        {
            AStartingAventador.StartingAventador();
        }
    }
    public void StartingFadeout()
    {
    if(MainControllerS1000RR.ivehicle == 2)
        {
            AStartingAventador.StartingFadeout();
        }
    }
    public void Shutting()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {

        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            AStartingAventador.StartingStop();
            //   ANBLIP.BlipStop();
            ANSLIAventador.NSLIStop();
            //    ANFSI1.NFSI1Stop();
            //    ANBLIP3000.Blip3000Stop();
            //    ANBLIP4000.Blip4000Stop();
            //    ANBLIP5000.Blip5000Stop();
            //    ANBLIP6000.Blip6000Stop();
            //    ANBLIP7000.Blip7000Stop();
            //    ANBLIP10000.Blip10000Stop();
            //    ANBLIP14000.Blip14000Stop();
            AIdlingAventador.IdlingStop(); // Stop Idling
                                           //   AIdlingSport.IdlingStop();
            ANFSRAventador.FSRStop();
            //   ANFSR15000.NFSR15000Stop();
            //    ANBLIPC14000.BLIPC14000Stop();
            //    ANBLIPC10000.BLIPC10000Stop();
            //    ANBLIPC7500.BLIPC7500Stop();
            ///    ANBLIPC6000.BLIPC6000Stop();
            //    ANBLIPC5500.BLIPC5500Stop();
            //    ANBLIPC4000.BLIPC4000Stop();
            //    ANBLIPC3000.BLIPC3000Stop();
            ANRevLimiterAventador.RevLimiterStop();

            AStartingAventador.StartingStop();
        }
    }



    public void Idling()
        {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            if (MainControllerS1000RR.icrackle == 0) AIdling.Idling();


            if (MainControllerS1000RR.icrackle == 1) AIdlingSport.Idling();
        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            SoundEngineS1000RR.idlingvol = 0;
            if (MainControllerS1000RR.icrackle == 0) AIdlingAventador.IdlingAventador();


           if (MainControllerS1000RR.icrackle == 1) AIdlingAventador.IdlingAventador();
        }
    }
    public void IdlingFadein()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            if (MainControllerS1000RR.icrackle == 0) AIdling.IdlingFadein();
            if (MainControllerS1000RR.icrackle == 1) AIdlingSport.IdlingFadein();
        }

        if (MainControllerS1000RR.ivehicle == 2)
        {
             AIdlingAventador.IdlingFadein();
        }
        
        }
    public void IdlingStop()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            if (MainControllerS1000RR.icrackle == 0) AIdling.IdlingStop();
            if (MainControllerS1000RR.icrackle == 1) AIdlingSport.IdlingStop();
        }

        if (MainControllerS1000RR.ivehicle == 2)
        {
            AIdlingAventador.IdlingStop();
        }

    }
    public void IdlingFadeout()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            if (MainControllerS1000RR.icrackle == 0) AIdling.IdlingFadeout();
            if (MainControllerS1000RR.icrackle == 1) AIdlingSport.IdlingFadeout();
        }
        if (MainControllerS1000RR.ivehicle == 2)
        {
            AIdlingAventador.IdlingFadeout();
        }
    }
    public void SLI()
    {
        if (MainControllerS1000RR.ivehicle == 2)
        {
            ANSLIAventador.NSLI();
            ANFSRAventador.FSRStop();
     //       ANFSR15000.NFSR15000Stop();
     //       ANBLIPC14000.BLIPC14000Stop();
     //     ANBLIPC10000.BLIPC10000Stop();
     //       ANBLIPC7500.BLIPC7500Stop();
     //       ANBLIPC6000.BLIPC6000Stop();
     //       ANBLIPC5500.BLIPC5500Stop();
     //       ANBLIPC4000.BLIPC4000Stop();
     //       ANBLIPC3000.BLIPC3000Stop();
        }
    }

    public void SLIStop()
    {
        ANSLIAventador.NSLIStop();
    }
        public void RevLimiter()
    {
        if(MainControllerS1000RR.ivehicle == 1)
        {

        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            Debug.Log("inside Aventador Revlimiter");
            ANRevLimiterAventador.RevLimiter();
       //     ANBLIP14000.Blip14000Stop();
              ANBLIPAventador.BlipStop();

            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option

              MS1000RR.Explode();
          //      ACrackle8000Aventador.Crackle8000();
                //   StartCoroutine("Explode");
                //      ExplodeinMainController();
            }

        }

    }
    public void Blip()
    {
        if(MainControllerS1000RR.ivehicle == 2)
        {
            ANBLIPAventador.Blip();
            ANBLIP2000Aventador.Blip2000();
            ANBLIP3000Aventador.Blip3000();
       //     ANBLIP4000Aventador.Blip4000();
            ANBLIP5000Aventador.Blip5000();
            ANBLIP6000Aventador.Blip6000();
            ANBLIP7000Aventador.Blip7000();
            //          ANBLIP10000.Blip10000();
            //          ANBLIP14000.Blip14000();
            //         ANFSR15000.NFSR15000();

            //           ANBLIPC3000.BLIPC3000();


            AIdlingAventador.IdlingStop(); // Stop Idling
                                           //   AIdlingSport.IdlingStop();
                                           //  SMS1000RR.IdlingStop();
                                           //         ifadeoutidling = 1;
                                           //         fadeoutidlingtime = 0.01f;
                                           //         StartCoroutine("FadeoutIdling");
            ANSLIAventador.NSLIStop();
            //      ANFSI1Aventador.NFSI1Stop();
            //        ifadeoutsli = 1;
            //        ifadeoutfsi1 = 1;
            //         StartCoroutine("FadeoutNSLI");
            //         StartCoroutine("FadeoutNFSI1");
            //        ifadeoutnfsr = 1;
            //        fadeoutfsrtime = 0.005f;
            //          FadeoutNFSR();



            //           ANFSR.FSRStop();

            //         ANFSR15000.NFSR15000Stop();
            //     ANBLIPC14000.BLIPC14000Stop();
            //      ANBLIPC10000.BLIPC10000Stop();
            ANBLIPC7500Aventador.BLIPC7500Stop();
            ANBLIPC6000Aventador.BLIPC6000Stop();
            ANBLIPC5500Aventador.BLIPC5500Stop();
            ANBLIPC4000Aventador.BLIPC4000Stop();
            ANBLIPC3000Aventador.BLIPC3000Stop();
        }
    }

    public void FSR15000()
    {

        if (MainControllerS1000RR.ivehicle == 1)
        {
            ANRevLimiter.RevLimiterStop();


            ANFSR15000.NFSR15000();
            ANBLIP.BlipStop();
            ANBLIP14000.Blip14000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP6000.Blip6000Stop();
        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            ANRevLimiterAventador.RevLimiterStop();


            ANFSR15000Aventador.NFSR15000();
            //      ANBLIP.BlipStop();
            //      ANBLIP14000.Blip14000Stop();
            //      ANBLIP7000.Blip7000Stop();
            //      ANBLIP6000.Blip6000Stop();
            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option

          //      MS1000RR.Explode();
          //      ACrackle8000Aventador.Crackle8000();
             //   StartCoroutine("Explode");
                //      ExplodeinMainController();
            }
        }

    }

    public void FSR8000()
    {

        if (MainControllerS1000RR.ivehicle == 1)
        {
          

          
          

            ANBLIPC7500.BLIPC7500();


            //          iblipcanstop = 0;
            ANBLIP.BlipStop();
            //                ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            //         ANBLIP7000.Blip7000Stop();
            ANBLIP14000.Blip14000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP6000.Blip6000Stop();
            //       iblipcanstop = 0;


            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option
             //        Debug.Log("MainControllerS1000RR.icrackle=");
             //        Debug.Log(MainControllerS1000RR.icrackle);
            //    ACrackle8000.Crackle8000();
            //    StartCoroutine("Explode");
            }
        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            ANRevLimiterAventador.RevLimiterStop();



            ANBLIPC7500Aventador.BLIPC7500();


            //          iblipcanstop = 0;
            ANBLIPAventador.BlipStop();
            //                ANBLIP7000.Blip7000Stop();
            ANBLIP10000Aventador.Blip10000Stop();
            //         ANBLIP7000.Blip7000Stop();
            ANBLIP14000Aventador.Blip14000Stop();
            ANBLIP7000Aventador.Blip7000Stop();
            ANBLIP6000Aventador.Blip6000Stop();
            //       iblipcanstop = 0;

        }

    }

    public void FSR6000()
    {

        if (MainControllerS1000RR.ivehicle == 1)
        {

            ANBLIPC6000.BLIPC6000();
            //         iblipcanstop = 0;
            ANBLIP.BlipStop();
            //              ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();


            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option
          //      ACrackle6000.Crackle6000();
                StartCoroutine("Explode");

            };
        }
        
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            ANRevLimiterAventador.RevLimiterStop();



            ANBLIPC6000Aventador.BLIPC6000();


            //          iblipcanstop = 0;
            ANBLIPAventador.BlipStop();
            //                ANBLIP7000.Blip7000Stop();
            ANBLIP10000Aventador.Blip10000Stop();
            //         ANBLIP7000.Blip7000Stop();
            ANBLIP14000Aventador.Blip14000Stop();
            ANBLIP7000Aventador.Blip7000Stop();
            ANBLIP6000Aventador.Blip6000Stop();
            //       iblipcanstop = 0;

        }

    }

    public void FSR5000()
    {

        if (MainControllerS1000RR.ivehicle == 1)
        {
            ANBLIPC5500.BLIPC5500();
            //            iblipcanstop = 0;
            ANBLIP.BlipStop();

            //            ANBLIP5000.Blip5000Stop();
            ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();


            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option
             //    ACrackle4000.Crackle4000();
                StartCoroutine("Explode");

            }
        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            ANBLIPC5500Aventador.BLIPC5500();
            //            iblipcanstop = 0;
            ANBLIPAventador.BlipStop();

            //            ANBLIP5000.Blip5000Stop();
            ANBLIP6000Aventador.Blip6000Stop();
            ANBLIP7000Aventador.Blip7000Stop();
            ANBLIP10000Aventador.Blip10000Stop();
            ANBLIP14000Aventador.Blip14000Stop();

        }

    }
    public void FSR4000()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            //    if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
            ANBLIPC4000.BLIPC4000();
            ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();


            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option
            //    ACrackle4000.Crackle4000();
                StartCoroutine("Explode");
            }

        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {
            //      if (startTick < (AudioNBLIP5000.dt5000 - 0.02f)) ANBLIP5000.Blip5000Stop();
            ANBLIPC4000Aventador.BLIPC4000();
            ANBLIP6000Aventador.Blip6000Stop();
            ANBLIP7000Aventador.Blip7000Stop();
            ANBLIP10000Aventador.Blip10000Stop();
            ANBLIP14000Aventador.Blip14000Stop();

            ANBLIPAventador.BlipStop();
            if (MainControllerS1000RR.icrackle == 1)
            {// Paid up Crackle Option
           //     ACrackle4000.Crackle4000();
                StartCoroutine("Explode");
            };


        }

       


        //               ANBLIP4000.Blip4000Stop();
        //          if (startTick < 0.192) ANBLIP5000.Blip5000Stop();
      
    }

    public void FSR3000()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            ANBLIPC3000.BLIPC3000();

            //      iblipcanstop = 0;
            ANBLIP.BlipStop();


            //                 ANBLIP3000.Blip3000Stop();
          //  if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000.Blip4000Stop();
            ANBLIP5000.Blip5000Stop();
            ANBLIP6000.Blip6000Stop();
            ANBLIP7000.Blip7000Stop();
            ANBLIP10000.Blip10000Stop();
            ANBLIP14000.Blip14000Stop();


        }
        else if (MainControllerS1000RR.ivehicle == 2)
        {

            ANBLIPC3000Aventador.BLIPC3000();

            //      iblipcanstop = 0;
            ANBLIPAventador.BlipStop();


            //                 ANBLIP3000.Blip3000Stop();
            //  if (startTick < (AudioNBLIP4000.dt4000 - 0.02f)) ANBLIP4000.Blip4000Stop();
            ANBLIP5000Aventador.Blip5000Stop();
            ANBLIP6000Aventador.Blip6000Stop();
            ANBLIP7000Aventador.Blip7000Stop();
            ANBLIP10000Aventador.Blip10000Stop();
            ANBLIP14000Aventador.Blip14000Stop();
        }

      

    }


    public void NFSRFadein()
    {
        if (MainControllerS1000RR.ivehicle == 2) ANFSRAventador.NFSRFadein();
    }
    public void NFSRFadeout()
    {
        if (MainControllerS1000RR.ivehicle == 2) ANFSRAventador.NFSRFadeout();
    }
    public void NSLIFadeout()
    {
        ANSLIAventador.NSLIFadeout();
    }

    public void NFSR()
    {
        ANFSRAventador.FSR();
    }

    public void FSRStop()
    {
        if (MainControllerS1000RR.ivehicle == 1)
        {
            if (MainControllerS1000RR.icrackle == 0) ANFSR.FSRStop();
       //    if (MainControllerS1000RR.icrackle == 1) ANSFRSport.FSRStop();
        }
        if (MainControllerS1000RR.ivehicle == 2)
        {
            ANFSRAventador.FSRStop();
        }
    }

    public void GarageAppear()
    {
        AIdlingAventador.IdlingReverbGarage();
        ANFSRAventador.NFSRReverbGarage();

       
  //      AIdlingSport.IdlingSportReverbGarage();
    //    AStartup.StartupReverbGarage();
        AStartingAventador.StartingReverbGarage();
    //    AShuttingAventador.ShuttingReverbGarage();
        ANRevLimiterAventador.RevLimiterReverbGarage();

        ANSLIAventador.NSLIReverbGarage();
     //   ANFSI1Aventador.NFSI1ReverbGarage();
        // ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000Aventador.Blip2000ReverbGarage();
        ANBLIP3000Aventador.Blip3000ReverbGarage();
        ANBLIP4000Aventador.Blip4000ReverbGarage();
        ANBLIP5000Aventador.Blip5000ReverbGarage();
        ANBLIP6000Aventador.Blip6000ReverbGarage();
        ANBLIP7000Aventador.Blip7000ReverbGarage();
        ANBLIP10000Aventador.Blip10000ReverbGarage();
        ANBLIP14000Aventador.Blip14000ReverbGarage();


        ANBLIPC3000Aventador.Blipc3000ReverbGarage();
        ANBLIPC4000Aventador.Blipc4000ReverbGarage();
        ANBLIPC5500Aventador.Blipc5000ReverbGarage();
        ANBLIPC6000Aventador.Blipc6000ReverbGarage();
        ANBLIPC7500Aventador.Blipc7500ReverbGarage();
        ANBLIPC10000Aventador.Blipc10000ReverbGarage();
    //    ANBLIPC14000Aventador.Blipc14000ReverbGarage();
        ANFSR15000Aventador.NFSR15000ReverbGarage();
        ANFSRAventador.NFSRReverbGarage();

    }
    public void RoadAppear()
    {
        AIdlingAventador.IdlingReverbRoad();
        ANFSRAventador.NFSRReverbRoad();
   //     AIdlingSport.IdlingSportReverbRoad();
     //   AStartup.StartupReverbRoad();
        AStartingAventador.StartingReverbRoad();
   //     AShuttingAventador.ShuttingReverbRoad();
        ANRevLimiterAventador.RevLimiterReverbRoad();

        ANSLIAventador.NSLIReverbRoad();
        //   ANFSI1Aventador.NFSI1ReverbRoad();


        //  ANFSI2.NFSI2ReverbRoad();

        ANBLIP2000Aventador.Blip2000ReverbRoad();
        ANBLIP3000Aventador.Blip3000ReverbRoad();
        ANBLIP4000Aventador.Blip4000ReverbRoad();
        ANBLIP5000Aventador.Blip5000ReverbRoad();
        ANBLIP6000Aventador.Blip6000ReverbRoad();
        ANBLIP7000Aventador.Blip7000ReverbRoad();
        ANBLIP10000Aventador.Blip10000ReverbRoad();
        ANBLIP14000Aventador.Blip14000ReverbRoad();


        ANBLIPC3000Aventador.Blipc3000ReverbRoad();
        ANBLIPC4000Aventador.Blipc4000ReverbRoad();
        ANBLIPC5500Aventador.Blipc5000ReverbRoad();
        ANBLIPC6000Aventador.Blipc6000ReverbRoad();
        ANBLIPC7500Aventador.Blipc7500ReverbRoad();
        ANBLIPC10000Aventador.Blipc10000ReverbRoad();
    //    ANBLIPC14000Aventador.Blipc14000ReverbRoad();
        ANFSR15000Aventador.NFSR15000ReverbRoad();
        ANFSRAventador.NFSRReverbRoad();
    }
    public void TunnelAppear()
    {
        AIdlingAventador.IdlingReverbGarage();
        ANFSRAventador.NFSRReverbGarage();

     //   AStartupAventador.StartupReverbGarage();
        AStartingAventador.StartingReverbGarage();
        //   AShuttingAventador.ShuttingReverbGarage();
        ANRevLimiterAventador.RevLimiterReverbGarage();
        // ANFSR.NFSRReverbGarage();
        ANSLIAventador.NSLIReverbGarage();
        //    ANFSI1Aventador.NFSI1ReverbGarage();
        //   ANFSI2.NFSI2ReverbGarage();

        ANBLIP2000Aventador.Blip2000ReverbGarage();
        ANBLIP3000Aventador.Blip3000ReverbGarage();
        ANBLIP4000Aventador.Blip4000ReverbGarage();
        ANBLIP5000Aventador.Blip5000ReverbGarage();
        ANBLIP6000Aventador.Blip6000ReverbGarage();
        ANBLIP7000Aventador.Blip7000ReverbGarage();
        ANBLIP10000Aventador.Blip10000ReverbGarage();
        ANBLIP14000Aventador.Blip14000ReverbGarage();

        ANBLIPC3000Aventador.Blipc3000ReverbGarage();
        ANBLIPC4000Aventador.Blipc4000ReverbGarage();
        ANBLIPC5500Aventador.Blipc5000ReverbGarage();
        ANBLIPC6000Aventador.Blipc6000ReverbGarage();
        ANBLIPC7500Aventador.Blipc7500ReverbGarage();
        ANBLIPC10000Aventador.Blipc10000ReverbGarage();
  //      ANBLIPC14000Aventador.Blipc14000ReverbGarage();
        ANFSR15000Aventador.NFSR15000ReverbGarage();
        ANFSRAventador.NFSRReverbGarage();


    }

}
