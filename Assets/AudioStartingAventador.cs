using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStartingAventador : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void StartingAventador()
    {

        AudioSource audio = GetComponent<AudioSource>();
       audio.volume = 1;
        audio.Play();
        audio.Play(44100);


    }

    public void StartingStop()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.Stop();
    }

    public static float Startingvolume = 1.0f;
    public void StartingFadeout()
    {
        Debug.Log("INside StartingFadeout Audio Source--------------------------------------");
        AudioSource audio = GetComponent<AudioSource>();
        Startingvolume = Startingvolume - 0.1f;
        if (Startingvolume > 0) audio.volume = (float)Startingvolume;
        else
        {

            SoundEngineS1000RR.ifadeoutstarting = 0;
            audio.Stop();
            audio.volume = 0;

        }
    }

    public void StartingReverbRoad()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = 0.0f;
    }

    public void StartingReverbGarage()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.reverbZoneMix = SoundEngineS1000RR.ReverbXoneMix;
    }
}
