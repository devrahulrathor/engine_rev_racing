# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    artifactId: "androidx.arch.core.core-runtime-2.0.1"
  }
  digests {
    sha256: "\005\'p6\202\360o:\372\203\003\312{\374X\004\343\320\345C-\364%\254b\320\214N\223\314\005\323"
  }
}
library {
  maven_library {
    artifactId: "androidx.asynclayoutinflater.asynclayoutinflater-1.0.0"
  }
  digests {
    sha256: "\367\352\266\fW\255\335\224\273\006\'X2\376v\000a\033\352\252\341\241\354Y|#\031V\372\371l\213"
  }
}
library {
  maven_library {
    artifactId: "androidx.browser.browser-1.0.0"
  }
  digests {
    sha256: "\033\037\305\303\261\354\214\376A\ro4_\347\2634 cKx7\251\206\365\377\304t\340d9\204\321"
  }
}
library {
  maven_library {
    artifactId: "androidx.coordinatorlayout.coordinatorlayout-1.0.0"
  }
  digests {
    sha256: "\345\b\306\225H\224\2237M\224+\367\264\356\002\253\367W\035%\252\304\306\"\345}l\325\315)\353s"
  }
}
library {
  maven_library {
    artifactId: "androidx.core.core-1.2.0"
  }
  digests {
    sha256: "RK\213\210\316\266\247J~D\346\265g\2415f\017!\027\231\220L\262\030\277\356[\341\026h \262"
  }
}
library {
  maven_library {
    artifactId: "androidx.cursoradapter.cursoradapter-1.0.0"
  }
  digests {
    sha256: "\250\034\217\347\210\025\372G\337[t\235\353Rrz\321\037\223\227\332X\261`\027\364\353,\021\342\205d"
  }
}
library {
  maven_library {
    artifactId: "androidx.customview.customview-1.0.0"
  }
  digests {
    sha256: " \345\270\366Rj4YZ`OVq\215\250\021g\300\264\nz\224\245}\2525Vc\362YM\362"
  }
}
library {
  maven_library {
    artifactId: "androidx.documentfile.documentfile-1.0.0"
  }
  digests {
    sha256: "\206Z\006\036\362\372\321e\"\370C56\270\324r\b\304o\367\307tQ\227\337\241\356\264\201\206\224\207"
  }
}
library {
  maven_library {
    artifactId: "androidx.drawerlayout.drawerlayout-1.0.0"
  }
  digests {
    sha256: "\224\002D,\334ZC\317b\373\024\370\317\230\3063B\324\331\331\270\005\310\003<l\367\350\002t\232\301"
  }
}
library {
  maven_library {
    artifactId: "androidx.fragment.fragment-1.0.0"
  }
  digests {
    sha256: "e\3352\327\037\346Z2\347y\211\246\317\261\255\t0p8\222\177\202\247@\307a\021b\320\265\030\370"
  }
}
library {
  maven_library {
    artifactId: "androidx.interpolator.interpolator-1.0.0"
  }
  digests {
    sha256: "3\03115\246O\342\037\242\303^\354f\210\361\247nQ&\006\300\374\203\334\033h\2367\255\327s*"
  }
}
library {
  maven_library {
    artifactId: "androidx.legacy.legacy-support-core-ui-1.0.0"
  }
  digests {
    sha256: "\r\022`\306\347\346\2437\370u\337q\265\026\223\036p?qn\220\210\230\027\315: \372Z\303\331G"
  }
}
library {
  maven_library {
    artifactId: "androidx.legacy.legacy-support-core-utils-1.0.0"
  }
  digests {
    sha256: "\247\355\317\001\325\265+04\a0\'\274Gu\267\212Gd\273b\002\273\221\326\034\202\232\335\215\321\307"
  }
}
library {
  maven_library {
    artifactId: "androidx.lifecycle.lifecycle-livedata-2.0.0"
  }
  digests {
    sha256: "\310&\t\316\330\304\230\360\247\001\243\017\266w\033\267H\b`\332\356\204\330.\n\201\356\206\355\367\2729"
  }
}
library {
  maven_library {
    artifactId: "androidx.lifecycle.lifecycle-livedata-core-2.0.0"
  }
  digests {
    sha256: "\375\3434\354~\"tL\017[\376|\257\032\204\311\327\0272pD@\005w\275\371\275\222\036\304\367\274"
  }
}
library {
  maven_library {
    artifactId: "androidx.lifecycle.lifecycle-runtime-2.0.0"
  }
  digests {
    sha256: "\344\257\311\3466\030?o>\016\337\034\364a!\244\222\377\322\306s\a[\260\177U\307\251\235\324<\373"
  }
}
library {
  maven_library {
    artifactId: "androidx.lifecycle.lifecycle-service-2.0.0"
  }
  digests {
    sha256: "\313+\025\273\f\361A4\351S\355\216\255\226\371Be\001\206C\365\0316}Q\375\203\177s\021\351\370"
  }
}
library {
  maven_library {
    artifactId: "androidx.lifecycle.lifecycle-viewmodel-2.0.0"
  }
  digests {
    sha256: "\326F\n\352\033k\255\200\253\024\317\210)~\236C\277\336\215\207\303\345\302\217,P\2023\377\274\300b"
  }
}
library {
  maven_library {
    artifactId: "androidx.loader.loader-1.0.0"
  }
  digests {
    sha256: "\021\3675\313;U\304X\324p\276\331\342RT7[Q\213K\033\255i&x:p&\333\017P%"
  }
}
library {
  maven_library {
    artifactId: "androidx.localbroadcastmanager.localbroadcastmanager-1.0.0"
  }
  digests {
    sha256: "\347\0342\214\356\365\304\247\327o-\206\337\033e\326_\342\254\370h\261\244\357\330J?43a\206\330"
  }
}
library {
  maven_library {
    artifactId: "androidx.print.print-1.0.0"
  }
  digests {
    sha256: "\035\\\17715\241\273\246a\3747?\327.\021\353\nJ\333\2639g\207\202m\330\344\031\r]\236\335"
  }
}
library {
  maven_library {
    artifactId: "androidx.room.room-runtime-2.1.0"
  }
  digests {
    sha256: "\027^\375\nr(\020\' \230((\031\353\325-m\017\240\252_\233\373%\254T&\216\035\252\004\036"
  }
}
library {
  maven_library {
    artifactId: "androidx.slidingpanelayout.slidingpanelayout-1.0.0"
  }
  digests {
    sha256: "v\277\373|\357\277x\a\224\330\201p\002\332\321V/>\'\300\251\367F\326$\001\310\355\263\n\356\336"
  }
}
library {
  maven_library {
    artifactId: "androidx.sqlite.sqlite-2.0.1"
  }
  digests {
    sha256: "\315\374\353\217n\306J\254!}\305\244\231\245\355\266\326\020\371\326\245u\373\247\276\240k+\372\363\352\177"
  }
}
library {
  maven_library {
    artifactId: "androidx.sqlite.sqlite-framework-2.0.1"
  }
  digests {
    sha256: "\207\016;\3163\255!\357\003C2\260\203\262\222Q\005\276\317\327=&.:\022p\220\022)\344Pv"
  }
}
library {
  maven_library {
    artifactId: "androidx.swiperefreshlayout.swiperefreshlayout-1.0.0"
  }
  digests {
    sha256: "\227a\263\250\t\311\260\223\375\006\243\304\273\306Eum\354\016\225\265\311\332A\233\311\362\243\363\002n\215"
  }
}
library {
  maven_library {
    artifactId: "androidx.versionedparcelable.versionedparcelable-1.1.0"
  }
  digests {
    sha256: "\232\035w\024\n\302\"\267\206kPT\356}\025\233\301\200\t\207\355-F\335j\375\321E\253\267\020\301"
  }
}
library {
  maven_library {
    artifactId: "androidx.viewpager.viewpager-1.0.0"
  }
  digests {
    sha256: "\024z\364\341J\031\204\001\r\217\025^^\031\327\201\360<\035p\337\355\002\250\340\321\204(\270\374\206\202"
  }
}
library {
  maven_library {
    artifactId: "androidx.work.work-runtime-2.1.0"
  }
  digests {
    sha256: "\275\035\035 \346\t\264~\272P?\022}p:\276;sg\342\006n{;D\311\272PD\266 \262"
  }
}
library {
  maven_library {
    artifactId: "billing-3.0.3"
  }
  digests {
    sha256: "\201\346\200\375\353\275\235~\330\2473\017\260\202?(\333h\353\3343\020\235\207$\257](\037.\231N"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-ads-19.5.0"
  }
  digests {
    sha256: "\353\006\346\245\271Q\203\230\335\245.\353\327b\247F\275\342cs\2265+E\330\303\324_\313e4\377"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-ads-base-19.5.0"
  }
  digests {
    sha256: "\370\276\341\316\362\236\353A\207A\276{\336\n\200qG\330\025\313\323SC}M\333\314l\337\346\r\000"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-ads-identifier-17.0.0"
  }
  digests {
    sha256: "\260\034@\214&[:\241q\321\233\350m{\215O4T\363\bQi\247ui\nj\205\365z\273s"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-ads-lite-19.5.0"
  }
  digests {
    sha256: "\373\360\237\275\r\275\355zh\226\036\205\251~}\270\365\371\352\216\321p\255a\367R\224\333\203\237\241\v"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-basement-17.3.0"
  }
  digests {
    sha256: "\222\\jh,\262Xy\327\314\r\vq[\001W\362f\226\314\037\001\310\351(O\340\204\252\025\'/"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-gass-19.5.0"
  }
  digests {
    sha256: "\361\354i\020}\\@\004\336\r(\354J\230+\301\301\343\0236\0314\245Mm\f^\274Jy\363\311"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-measurement-base-17.2.0"
  }
  digests {
    sha256: "\200\262\357a\032\017\003YJ\306\2027\254!\205\002EA\b*\037B\000NlX<\303\257\204\241\324"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-measurement-sdk-api-17.2.0"
  }
  digests {
    sha256: "%\323\357\374T\'\346\222#\b\204\016\376Q\257\327\371\320\322\326\307\v\336\033\247|M\345\314\307\203\202"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-tasks-17.1.0"
  }
  digests {
    sha256: "\313.\251\211\r\202>\342I\317\242\251D)\223~\305\253\vR!q\225S\267%\035\242.\376\314\260"
  }
}
library {
  maven_library {
    artifactId: "common"
  }
  digests {
    sha256: "i\006\370Ya1@5 DuV\"\334\3655\214\367\262U\252\360h\313\021\207\260\265!G\350\243"
  }
}
library {
  maven_library {
    artifactId: "googlemobileads-unity"
  }
  digests {
    sha256: "6\227\016\020\375Q\372\312\235~~}\211\276\371bH\250\b\325.\221N\376\223\320\225\224\023fz\301"
  }
}
library {
  maven_library {
    artifactId: "UnityAds"
  }
  digests {
    sha256: "\n\311sV\2426n\253\331\204+\274\021S\342\n\"\271H\344\236\336\033*8>\220\323T0\036\366"
  }
}
library {
  maven_library {
    artifactId: "UnityAdsAndroidPlugin"
  }
  digests {
    sha256: "i\363\366\217\371\355R\t\aX9V\333v\326E\242\005\352\252e\335\374\306yl>8I\250a\335"
  }
}
library {
  digests {
    sha256: "\323\215c\355\263\017\024g\201\215P\252\360_\212i-\352\21319*\004\233\372\231\033\025\232\325\266\222"
  }
}
library {
  digests {
    sha256: "{\255z\030\210\004\255\352o\241\363]^\371\233p_ \275\223\354\255\336HG`\377\206\2655\376\374"
  }
}
library {
  digests {
    sha256: "\027AZ\307@]8*a\214\207\340`\307\256\256\257\204l\331w\367\vU\337\203\3021\327\265*\352"
  }
}
library {
  digests {
    sha256: "\344\255v\a\345\300G|o\211\016\362jI\313\215\033\264\337\373e\v\253E\002\257\356ddN0i"
  }
}
library {
  digests {
    sha256: "8\2153\377P=\214=\001\n]\327\354\376\016D\273\031\257\314(\375\036c6u]\000\357z\323\306"
  }
}
library {
  digests {
    sha256: "\234\215\021{\\+\301 \241\315\376\270W\340[I[\026\303`\023W\003r\247\b\367\202~:\311\371"
  }
}
library {
  digests {
    sha256: "\3471j\204\270\231\353*\373\025QxN\230\a\373d\275\374\301\005co\340U\034\3206\200\037\227\310"
  }
}
library_dependencies {
}
library_dependencies {
  library_index: 1
}
library_dependencies {
  library_index: 2
}
library_dependencies {
  library_index: 3
}
library_dependencies {
  library_index: 4
}
library_dependencies {
  library_index: 5
}
library_dependencies {
  library_index: 6
}
library_dependencies {
  library_index: 7
}
library_dependencies {
  library_index: 8
}
library_dependencies {
  library_index: 9
}
library_dependencies {
  library_index: 10
}
library_dependencies {
  library_index: 11
}
library_dependencies {
  library_index: 12
}
library_dependencies {
  library_index: 13
}
library_dependencies {
  library_index: 14
}
library_dependencies {
  library_index: 15
}
library_dependencies {
  library_index: 16
}
library_dependencies {
  library_index: 17
}
library_dependencies {
  library_index: 18
}
library_dependencies {
  library_index: 19
}
library_dependencies {
  library_index: 20
}
library_dependencies {
  library_index: 21
}
library_dependencies {
  library_index: 22
}
library_dependencies {
  library_index: 23
}
library_dependencies {
  library_index: 24
}
library_dependencies {
  library_index: 25
}
library_dependencies {
  library_index: 26
}
library_dependencies {
  library_index: 27
}
library_dependencies {
  library_index: 28
}
library_dependencies {
  library_index: 29
}
library_dependencies {
  library_index: 30
}
library_dependencies {
  library_index: 31
}
library_dependencies {
  library_index: 32
}
library_dependencies {
  library_index: 33
}
library_dependencies {
  library_index: 34
}
library_dependencies {
  library_index: 35
}
library_dependencies {
  library_index: 36
}
library_dependencies {
  library_index: 37
}
library_dependencies {
  library_index: 38
}
library_dependencies {
  library_index: 39
}
library_dependencies {
  library_index: 40
}
library_dependencies {
  library_index: 41
}
library_dependencies {
  library_index: 42
}
library_dependencies {
  library_index: 43
}
library_dependencies {
  library_index: 44
}
library_dependencies {
  library_index: 45
}
library_dependencies {
  library_index: 46
}
library_dependencies {
  library_index: 47
}
library_dependencies {
  library_index: 48
}
library_dependencies {
  library_index: 49
}
module_dependencies {
  module_name: "base"
  dependency_index: 48
  dependency_index: 49
  dependency_index: 43
  dependency_index: 44
  dependency_index: 45
  dependency_index: 46
  dependency_index: 47
}
